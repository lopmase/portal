module.exports = {
  apps: [
    {
      name: 'auctioneer',
      script: './server.js',
      watch: true,
      env: {
        NODE_ENV: 'production',
      },
    },
  ],
};
