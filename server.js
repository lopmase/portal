const { CronJob } = require('cron');
const express = require('express');
const path = require('path');

const app = express();

app.use(express.static(path.join(__dirname, 'build')));
app.get('/*', function handler(req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});
const job = new CronJob(
  '0 */1 * * * *',
  async function test() {
    console.log('runnnn');
  },
  null,
  true,
  'Asia/Ho_Chi_Minh',
);
// Use this if the 4th param is default value(false)
job.start();
app.listen(6000);
