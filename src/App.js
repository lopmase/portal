import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';

import { toast } from 'react-toastify';
import configureStore from './store/configStore';

import './App.scss';
import 'react-toastify/dist/ReactToastify.css';
import 'react-placeholder/lib/reactPlaceholder.css';
import 'react-day-picker/lib/style.css';
import { ROUTES } from './constants/ROUTES';
import HouseContainer from './container/HouseContainer/Container';
import Shell from './container/ShellContainner/Shell/Shell';
import Login from './container/AuthContainer/Login/Login';
import NotFound from './container/Common/NotFound';
import GlobalErrorModal from './container/Common/GlobalErrorModal/GlobalErrorModal';
import DashboardContainer from './container/DashboardContainer/Container';
import { isAuthenticated } from './apis/authApi';
import { loadUserProfile } from './container/AuthContainer/Profile/action';
import NewCustomer from './container/CustomerConntainer/NewCustomer';
import ProjectContainer from './container/ProjectContainer/Container';
import CustomerContainer from './container/CustomerConntainer/Container';
import ConfirmDialog from './container/ShellContainner/ConfirmDialog/ConfirmDialog';
import Profile from './container/AuthContainer/Profile/Profile';
import BlogContainer from './container/BlogContainer/Container';
import SocialContainer from './container/SocialContainer/Container';
import AdminContainer from './container/AdminContainer/Container';
import BussinessContainer from './container/BussinessContainer/Container';
import EmployeeDiagram from './container/EmployeeDiagram/Container';
import ChangePassword from './container/AuthContainer/ChangePassword';
import Alonhadat from './container/AuthContainer/Alonhadat/index';
import RequestContainer from './container/RequestContainer/Container';
import { trackUserInfo } from './container/AuthContainer/AuthAction';
import PostManager from './container/PostManager/Container';
import KnowledgeContainer from './container/KnowledgeContainer/Container';
import AccountContainer from './container/AccountContainer/Container';
import { PrivateRoute } from './components';

const history = createBrowserHistory();
const store = configureStore(history);

toast.configure({
  position: 'bottom-right',
  toastClassName: 'my-toast',
  autoClose: 3000,
});

const App = () => {
  if (isAuthenticated()) {
    store.dispatch(loadUserProfile());
    trackUserInfo();
  }
  return (
    <ConnectedRouter history={history}>
      <div>
        <Switch>
          <Route exact path={ROUTES.LOGIN} component={Login} />
          <Route>
            <Shell>
              <Switch>
                <Route path={ROUTES.DASHBOARD} component={DashboardContainer} />
                <Route path={ROUTES.ACCOUNT} component={AccountContainer} />
                <Route path={ROUTES.BLOG_ROOT} component={BlogContainer} />
                <Route path={ROUTES.HOUSE_ROOT} component={HouseContainer} />
                <Route path={ROUTES.PROJECT_ROOT} component={ProjectContainer} />
                <Route path={ROUTES.CUSTOMER_ROOT} component={CustomerContainer} />
                <Route path={ROUTES.SOCIAL_ROOT} component={SocialContainer} />
                <Route path={ROUTES.OVERVIEW_BUSINESS_ROOT} component={BussinessContainer} />
                <Route path={ROUTES.ADMIN_ROOT} component={AdminContainer} />
                <PrivateRoute path={ROUTES.EMPLOYMENT_DIAGRAM} component={EmployeeDiagram} />
                {/* <Route path={ROUTES.CHAT} component={ChatShell} /> */}
                <Route path={ROUTES.POST_MANAGER} component={PostManager} />
                <Route path={ROUTES.REQUEST_ROOT} component={RequestContainer} />
                <Route path={ROUTES.KNOWLEDGE_ROOT} component={KnowledgeContainer} />
                <Redirect path="/" to={ROUTES.DASHBOARD} />
                <Route component={NotFound} />
              </Switch>
            </Shell>
          </Route>
        </Switch>
        <NewCustomer />
        <ChangePassword />
        <Alonhadat />
        <GlobalErrorModal />
        <Profile />
        <ConfirmDialog />
      </div>
    </ConnectedRouter>
  );
};

const ConnectedApp = () => (
  <Provider store={store}>
    {/* <AppFireBase /> */}
    <App />
  </Provider>
);

export default ConnectedApp;
