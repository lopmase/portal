import * as types from './types';
import { apiSerializer } from '../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../constants/MESSAGE';

export function refreshingAction(loading) {
  return {
    type: types.REFRESHING_USERS,
    loading,
  };
}

export function errorAction(message) {
  return {
    type: types.UPDATE_USERS_ERROR,
    message,
  };
}

export function loadingAction(loading) {
  return {
    type: types.LOADING_USERS,
    loading,
  };
}

export function updateUsersAction(users) {
  return {
    type: types.UPDATE_USERS,
    users,
  };
}

export const listUsers = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { initialized } = getState().user;
    if (initialized) {
      dispatch(refreshingAction(true));
    } else {
      dispatch(loadingAction(true));
    }
    const response = await thunkDependencies.user.list();
    const result = apiSerializer(response);
    dispatch(updateUsersAction(result));
    return { data: result };
  } catch (error) {
    dispatch(errorAction(error.message || UNKNOWN_ERROR));
    return { error: error.message || UNKNOWN_ERROR };
  }
};
