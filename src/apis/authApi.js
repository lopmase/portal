import { USER_TOKEN } from '../constants/STORAGE_KEYS';
import { request } from './request';

export const login = (email, password) => {
  return request('api/auth/signin/email', {
    method: 'post',
    data: {
      email,
      password,
    },
  });
};

export const isAuthenticated = () => {
  return localStorage.getItem(USER_TOKEN);
};

export default {
  login,
  isAuthenticated,
};
