import { requestAuth } from './request';
import { apiSerializer } from '../helpers/apiSerializer';
import { Blog } from '../models/Blog';

export const list = ({ page = 1, size = 10 }) => {
  return requestAuth('api/blog', {
    method: 'get',
    params: { page, size },
  }).then(response => {
    const result = apiSerializer(response);
    return result.map(blog => new Blog(blog));
  });
};
export const option = () => {
  return requestAuth('api/house/option', {
    method: 'get',
  });
};
export const detail = blogId => {
  return requestAuth(`api/blog/detail?blog_id=${blogId}`, {
    method: 'get',
  });
};

export const remove = blogId => {
  return requestAuth('api/blog/detail', {
    method: 'delete',
    data: {
      blog_id: blogId,
    },
  });
};

export const newBlog = data => {
  return requestAuth('api/blog/detail', {
    method: 'post',
    data,
  });
};
export const update = data => {
  return requestAuth('api/blog/detail', {
    method: 'put',
    data,
  });
};
export const upload = file => {
  const form = new FormData();
  form.append('image', file, file.name);
  return requestAuth('api/house/image', {
    method: 'post',
    data: form,
  });
};

export default {
  list,
  remove,
  update,
  newBlog,
  option,
  upload,
  detail,
};
