import { requestAuth } from './request';

export const getAllStatictis = () => {
  return requestAuth('api/business/accumulation/get-all', {
    method: 'get',
  });
};
export const createStatictis = data => {
  return requestAuth('/api/business/accumulation/add', {
    method: 'post',
    data,
  });
};

export const createTransaction = data => {
  return requestAuth('/api/business/transaction/add', {
    method: 'post',
    data,
  });
};

export const createTransactionV2 = data => {
  return requestAuth('/api/business/v2/transaction/add', {
    method: 'post',
    data,
  });
};
export const updateTransaction = data => {
  return requestAuth('/api/business/transaction/update', {
    method: 'put',
    data,
  });
};

export const updateTransactionV2 = data => {
  return requestAuth('/api/business/v2/transaction/update', {
    method: 'put',
    data,
  });
};

export const getTransactionByHouseIdV2 = id => {
  return requestAuth(`/api/business/v2/transaction/${id}`, {
    method: 'get',
  });
};

export const updateStatistic = data => {
  return requestAuth('/api/business/accumulation/update', {
    method: 'put',
    data,
  });
};
export const getAllTransaction = data => {
  return requestAuth('api/business/transaction/get-all', {
    method: 'get',
    params: data,
  });
};
export const getAllTransactionV2 = data => {
  return requestAuth('api/business/v2/transaction', {
    method: 'get',
    params: data,
  });
};
export const getAllStatisticByCondition = data => {
  return requestAuth('api/business/accumulation/get-by', {
    method: 'get',
    params: data,
  });
};
export const getAllStatisticForReport = data => {
  return requestAuth('api/business/accumulation/report', {
    method: 'get',
    params: data,
  });
};
export const getHouse = data => {
  return requestAuth('api/house/list-by-condition', {
    method: 'post',
    data,
  });
};
export const countCurrentHouseQuantity = data => {
  return requestAuth('api/house/count-by-condition', {
    method: 'post',
    data,
  });
};

export const getHouseSold = () => {
  return requestAuth('api/house/house-sold/get-all', {
    method: 'get',
  });
};

export const createFirstValue = data => {
  return requestAuth('/api/business/first-value/add', {
    method: 'post',
    data,
  });
};

export const getFirstValue = () => {
  return requestAuth('api/business/first-value/get-all', {
    method: 'get',
  });
};

export const statisticcHouse = () => {
  return requestAuth('api/house/statistic', {
    method: 'get',
  });
};
export const getStatisticHouse = data => {
  return requestAuth('api/house/statistic/detail', {
    method: 'get',
    params: data,
  });
};
export const getUser = () => {
  return requestAuth('api/user/list', {
    method: 'get',
  });
};

export const createTarget = data => {
  return requestAuth('/api/business/target/create', {
    method: 'post',
    data,
  });
};

export const getTarget = data => {
  return requestAuth('/api/business/target/get', {
    method: 'post',
    data,
  });
};
export const updateTarget = data => {
  return requestAuth('/api/business/target/update', {
    method: 'put',
    data,
  });
};
//todo

export const createReceipts = data => {
  return requestAuth('/api/business/v2/receipt/add', {
    method: 'post',
    data,
  });
};

export const updateReceipts = data => {
  return requestAuth('/api/business/v2/receipt/update', {
    method: 'put',
    data,
  });
};

export const getReceipts = data => {
  return requestAuth('/api/business/v2/receipt', {
    method: 'get',
    // eslint-disable-next-line no-undef
    params: data,
  });
};
