import { requestAuth } from './request';

export const list = ({ role, page, size, search, sort, order }) => {
  return requestAuth('api/customer/list', {
    method: 'get',
    params: {
      role: role || undefined,
      page: page || 1,
      size: size || 100,
      search: search || undefined,
      sort,
      order,
    },
  });
};

export const getCustomerByUserId = userId => {
  return requestAuth(`api/customer/userId/${userId}`, {
    method: 'get',
  });
};

export const newCustomer = data => {
  return requestAuth('api/customer/detail', {
    method: 'post',
    data,
  });
};
export const removeCustomer = customerId => {
  return requestAuth('api/customer/detail', {
    method: 'delete',
    data: {
      customer_id: customerId,
    },
  });
};
export const detailCustomer = customerId => {
  return requestAuth(`api/customer/detail?customer_id=${customerId}`, {
    method: 'get',
  });
};
export const updateCustomer = data => {
  return requestAuth('api/customer/detail', {
    method: 'put',
    data,
  });
};
export const dashboard = () => {
  return requestAuth('api/dashboard/data', {
    method: 'get',
  });
};

export const createHouseRecommendStatus = data => {
  return requestAuth('api/customer/house/recommend', {
    method: 'post',
    data,
  });
};

export const updatePriority = data => {
  return requestAuth('api/customer/house/recommend/update-priority', {
    method: 'put',
    data,
  });
};

export const getHouseRecommendHistory = data => {
  return requestAuth('api/customer/house/recommend/history', {
    method: 'get',
    params: {
      request_id: data.request_id,
    },
  });
};

export default {
  list,
  new: newCustomer,
  removeCustomer,
  detailCustomer,
  updateCustomer,
  dashboard,
  getCustomerByUserId,
};
