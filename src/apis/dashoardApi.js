import { requestAuth } from './request';
import { apiSerializer } from '../helpers/apiSerializer';

export const overall = () => {
  return requestAuth('api/dashboard/data', {
    method: 'get',
  }).then(resp => apiSerializer(resp));
};

export const topViewedHouses = size => {
  return requestAuth('api/house/top/viewed', {
    method: 'get',
    params: { size },
  }).then(resp => apiSerializer(resp));
};
export const getHouseTopPost = () => {
  return requestAuth('api/user/get-post', {
    method: 'get',
  }).then(resp => apiSerializer(resp));
};

export default {
  overall,
  topViewedHouses,
};
