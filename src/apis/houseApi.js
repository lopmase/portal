/* eslint-disable camelcase */
import { requestAuth } from './request';
import { HOUSE_STATUS } from '../constants/COMMON';
import { apiSerializer } from '../helpers/apiSerializer';
import { House } from '../models/House';

export const list = ({
  role,
  page = 1,
  size = 10,
  column,
  direction = 'asc',
  project_id,
  mode = 'public',
  status = HOUSE_STATUS[0].value,
  search,
  filters = [],
  tagSearch,
}) => {
  return requestAuth('api/house/list', {
    method: 'post',
    params: {
      page,
      size,
    },
    data: {
      role: role || undefined,
      search: search || undefined,
      column: column || undefined,
      direction,
      project_id,
      mode,
      status,
      filters,
      tagSearch,
    },
  }).then(resp => {
    const rs = apiSerializer(resp);
    return { ...rs, data: rs.data.map(item => new House(item)) };
  });
};
export const listV2 = ({
  role,
  page = 1,
  size = 10,
  column,
  direction = 'asc',
  project_id,
  mode = 'public',
  status = HOUSE_STATUS[0].value,
  search,
  filters = [],
  tagSearch,
  fetchType,
}) => {
  return requestAuth('api/house/v2/list', {
    method: 'post',
    params: {
      page,
      size,
    },
    data: {
      role: role || undefined,
      search: search || undefined,
      column: column || undefined,
      direction,
      project_id,
      mode,
      status,
      filters,
      tagSearch,
      fetchType,
    },
  });
};
export const getTag = () => {
  return requestAuth('api/house/tag', {
    method: 'get',
  });
};

export const createTag = data => {
  return requestAuth('api/house/tag', {
    method: 'post',
    data,
  });
};
export const deleteTag = data => {
  return requestAuth('api/house/tag', {
    method: 'delete',
    data,
  });
};

export const updateTag = data => {
  return requestAuth('api/house/tag', {
    method: 'put',
    data,
  });
};

export const updateHouseTag = data => {
  return requestAuth('api/house/tag/house', {
    method: 'put',
    data,
  });
};
export const detail = house_id => {
  return requestAuth('api/house/detail', {
    method: 'get',
    params: {
      house_id,
    },
  });
};

export const comments = ({ page = 1, size = 10 }) => {
  return requestAuth('api/house/comments', {
    method: 'get',
    params: { page, size },
  });
};

export const requestByHouse = house_id => {
  return requestAuth('api/match/house/request', {
    method: 'get',
    params: {
      house_id,
    },
  });
};
export const houseByRequest = (ids = []) => {
  return requestAuth('api/match/request/house', {
    method: 'get',
    params: {
      request_ids: ids.join(','),
    },
  });
};

export const remove = house_id => {
  return requestAuth('api/house/detail', {
    method: 'delete',
    data: {
      house_id,
    },
  });
};

export const newHouse = data => {
  return requestAuth('api/house/detail', {
    method: 'post',
    data,
  });
};
export const addSubRecommendHouse = data => {
  return requestAuth('api/house/sub-recommend', {
    method: 'post',
    data,
  });
};
export const getSubRecommendHouse = data => {
  return requestAuth('api/match/get-sub-recommend', {
    method: 'post',
    data,
  });
};
export const update = data => {
  return requestAuth('api/house/detail', {
    method: 'put',
    data,
  });
};
export const upload = (file, rotation, newName) => {
  const form = new FormData();
  form.append('image', file, file.name);
  form.append('rotation', rotation);
  form.append('newName', newName);
  return requestAuth('api/house/image', {
    method: 'post',
    data: form,
  });
};

export const rotateImage = (id, rotation) => {
  return requestAuth('api/house/image/rotate', {
    method: 'post',
    data: { id, rotation },
  });
};

export const uploadWeb = data => {
  return requestAuth('api/house/preview', {
    method: 'post',
    data,
  });
};
export const reviews = ({ url, page = 1, size = 10 }) => {
  return requestAuth(
    url || 'api/house/preview',
    {
      method: 'get',
      params: {
        page,
        size,
      },
    },
    url,
  );
};

export const pendingPublicHouses = ({ page = 1, size = 10 }) => {
  return requestAuth('api/house/public/pending', {
    method: 'get',
    params: {
      page,
      size,
    },
  });
};

export const approvedHouses = (page = 1) => {
  return requestAuth('api/house/preview/approved', {
    method: 'get',
    params: {
      page,
      size: 10,
    },
  });
};

export const approve = data => {
  return requestAuth('api/house/preview/approve', {
    method: 'post',
    data,
  });
};

export const approvePublic = data => {
  return requestAuth('api/house/public/approve', {
    method: 'post',
    data,
  });
};

export const reject = data => {
  return requestAuth('api/house/preview/reject', {
    method: 'post',
    data,
  });
};

export const rejectPublic = data => {
  return requestAuth('api/house/public/reject', {
    method: 'post',
    data,
  });
};

export const distinctValues = column => {
  return requestAuth(`api/house/column/values`, {
    method: 'get',
    params: {
      column,
    },
  });
};

export const getCommentByHouseId = id => {
  return requestAuth(`api/house/${id}/comment`, {
    method: 'get',
    params: {},
  });
};

export const history = (house_id, sortCol = 'created_at', order = 'DESC') => {
  return requestAuth('api/history', {
    method: 'post',
    data: { filter: { model_id: house_id, model: 'House' }, sort: { [sortCol]: order } },
  });
};

export const downloadAllImageByHouseId = id => {
  return requestAuth(`api/house/${id}/image/zip`, {
    method: 'get',
    data: {
      id,
    },
  });
};
export const addAlonhadat = data => {
  return requestAuth('api/house/alonhadat', {
    method: 'post',
    data,
  });
};
export const updateAlonhadat = data => {
  return requestAuth('api/house/alonhadat', {
    method: 'put',
    data,
  });
};
export const approvalAlonhadat = data => {
  return requestAuth('api/house/alonhadat/approved', {
    method: 'put',
    data,
  });
};
export const addBatdongsan = data => {
  return requestAuth('api/house/batdongsan', {
    method: 'post',
    data,
  });
};

export const updateBatdongsan = data => {
  return requestAuth('api/house/batdongsan', {
    method: 'put',
    data,
  });
};

export const getAlonhadat = () => {
  return requestAuth('api/house/alonhadat', {
    method: 'get',
  });
};
export const getBatdongsan = () => {
  return requestAuth('api/house/batdongsan', {
    method: 'get',
  });
};

export const getDetailAlonhadat = id => {
  return requestAuth(`api/house/alonhadat/${id}`, {
    method: 'get',
  });
};
export const getDetailBatdongsan = id => {
  return requestAuth(`api/house/batdongsan/${id}`, {
    method: 'get',
  });
};
export const deleteAlonhadat = id => {
  return requestAuth(`api/house/alonhadat/${id}`, {
    method: 'delete',
  });
};
export const deleteBatdongsan = id => {
  return requestAuth(`api/house/batdongsan/${id}`, {
    method: 'delete',
  });
};

// todo
export const addQuestion = data => {
  return requestAuth('api/question', {
    method: 'post',
    data,
  });
};

export const getQuestionByHouseId = id => {
  return requestAuth(`api/question/${id}`, {
    method: 'get',
  });
};

export default {
  list,
  getTag,
  remove,
  update,
  new: newHouse,
  upload,
  rotateImage,
  approvedHouses,
  uploadWeb,
  reviews,
  approve,
  reject,
  detail,
  comments,
  requestByHouse,
  houseByRequest,
  distinctValues,
  pendingPublicHouses,
  approvePublic,
  rejectPublic,
  getCommentByHouseId,
  history,
  downloadAllImageByHouseId,
  addAlonhadat,
  getAlonhadat,
  addBatdongsan,
  approvalAlonhadat,
  getDetailAlonhadat,
  updateAlonhadat,
  updateBatdongsan,
  getBatdongsan,
  getDetailBatdongsan,
  deleteAlonhadat,
  deleteBatdongsan,
  listV2,
  addQuestion
};
