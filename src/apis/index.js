import auth from './authApi';
import user from './userApi';
import house from './houseApi';
import knowledge from './knowLedgeApi';
import metadata from './metadataApi';
import customer from './customerApi';
import project from './projectApi';
import blog from './blogApi';
import dashboard from './dashoardApi';
import request from './requestApi';
import zalo from './zaloApi';

const thunkDependencies = {
  auth,
  user,
  house,
  metadata,
  customer,
  blog,
  project,
  dashboard,
  request,
  knowledge,
  zalo,
};

export default thunkDependencies;
