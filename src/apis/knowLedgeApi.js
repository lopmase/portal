import { requestAuth } from './request';

export const upload = (file, rotation, newName) => {
  const form = new FormData();
  form.append('image', file, file.name);
  form.append('rotation', rotation);
  form.append('newName', newName);
  return requestAuth('api/house/image', {
    method: 'post',
    data: form,
  });
};

export const rotateImage = (id, rotation) => {
  return requestAuth('api/house/image/rotate', {
    method: 'post',
    data: { id, rotation },
  });
};

export const addKnowledge = data => {
  return requestAuth('api/knowledge', {
    method: 'post',
    data,
  });
};

export const updateKnowledge = data => {
  return requestAuth(`api/knowledge`, {
    method: 'put',
    data,
  });
};

export const addType = data => {
  return requestAuth('api/knowledge/type', {
    method: 'post',
    data,
  });
};

export const getType = () => {
  return requestAuth('api/knowledge/type', {
    method: 'get',
  });
};

export const getKnowledge = () => {
  return requestAuth('api/knowledge', {
    method: 'get',
  });
};

export const getKnowledgeByTypeId = id => {
  return requestAuth(`api/knowledge/by/${id}/waiting`, {
    method: 'get',
  });
};

export const deleteKnowledge = id => {
  return requestAuth(`api/knowledge/${id}`, {
    method: 'delete',
  });
};

export const getKnowledgeWithPage = (id, page) => {
  return requestAuth(`api/knowledge/by/${id}`, {
    method: 'get',
    params: {
      page,
    },
  });
};

export const comment = (id, content) => {
  return requestAuth(`api/knowledge/${id}/comment`, {
    method: 'post',
    data: { content },
  });
};

export const getCommentByKnowledgeId = id => {
  return requestAuth(`api/knowledge/${id}/comment`, {
    method: 'get',
    params: {},
  });
};

export const approve = id => {
  return requestAuth(`api/knowledge/approve`, {
    method: 'put',
    data: {
      id,
    },
  });
};

export const reject = data => {
  return requestAuth(`api/knowledge/notApprove`, {
    method: 'put',
    data,
  });
};

export const getListNotApprove = () => {
  return requestAuth(`api/knowledge/notApproveByUserId`, {
    method: 'get',
  });
};

export const updateListPostChangeColumn = data => {
  return requestAuth(`api/knowledge/changeColumn`, {
    method: 'put',
    data,
  });
};

export default {
  upload,
  rotateImage,
  addKnowledge,
  addType,
  getType,
  getKnowledge,
  comment,
  getCommentByKnowledgeId,
  updateKnowledge,
  getKnowledgeByTypeId,
  approve,
  reject,
  getListNotApprove,
  getKnowledgeWithPage,
  deleteKnowledge,
  updateListPostChangeColumn,
};
