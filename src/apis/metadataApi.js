/* eslint-disable camelcase */
import { requestAuth } from './request';

export const streets = () => {
  return requestAuth('api/house/street', {
    method: 'get',
  });
};

export const options = () => {
  return requestAuth('api/house/option', {
    method: 'get',
  });
};

export const getDictionary = (type, district_id) => {
  return requestAuth(`api/dictionary/${type}/list`, {
    method: 'get',
    params: {
      district_id,
    },
  });
};
export const getProvince = () => {
  return requestAuth(`api/dictionary/province/list`, {
    method: 'get',
  });
};

export const newDictionary = (type, data, district_id) => {
  return requestAuth(`api/dictionary/${type}/detail`, {
    method: 'post',
    data: { value: data, district_id },
  });
};
export const updateDictionary = (type, id, value, district_id) => {
  return requestAuth(`api/dictionary/${type}/detail`, {
    method: 'put',
    data: { id, value, district_id },
  });
};
export const deleteDictionary = (type, id) => {
  return requestAuth(`api/dictionary/${type}/detail`, {
    method: 'delete',
    data: {
      id,
    },
  });
};

export default {
  streets,
  options,
  getDictionary,
  newDictionary,
  updateDictionary,
  deleteDictionary,
};
