/* eslint-disable camelcase */
import { requestAuth } from './request';

export const list = ({ page = 1, size = 10 }) => {
  return requestAuth('api/project/list', {
    method: 'get',
    params: {
      page,
      size,
    },
  });
};

export const searchByName = (key = '') => {
  return requestAuth('api/project/search', {
    method: 'get',
    params: {
      key,
    },
  });
};

export const option = () => {
  return requestAuth('api/house/option', {
    method: 'get',
  });
};
export const detail = project_id => {
  return requestAuth(`api/project/detail?project_id=${project_id}`, {
    method: 'get',
  });
};

export const remove = project_id => {
  return requestAuth('api/project/detail', {
    method: 'delete',
    data: {
      project_id,
    },
  });
};

export const newProject = data => {
  return requestAuth('api/project/detail', {
    method: 'post',
    data,
  });
};
export const update = data => {
  return requestAuth('api/project/detail', {
    method: 'put',
    data,
  });
};
export const upload = file => {
  const form = new FormData();
  form.append('image', file, file.name);
  return requestAuth('api/house/image', {
    method: 'post',
    data: form,
  });
};

export default {
  list,
  remove,
  update,
  newProject,
  option,
  upload,
  detail,
  searchByName,
};
