import axios from 'axios';
import { USER_TOKEN } from '../constants/STORAGE_KEYS';
import { handleError } from '../helpers/errorHandler';

axios.defaults.baseURL = 'https://tgl.vjitp.com/'; // process.env.REACT_APP_API_URL;

const defaultOptions = {
  headers: {
    'Content-Type': 'application/json',
  },
};

export const request = async (url, options) => {
  const requestOptions = {
    ...defaultOptions,
    ...options,
  };

  try {
    const response = await axios({
      url,
      ...requestOptions,
    });
    return response.data;
  } catch (e) {
    const { status } = e.response;
    const { data } = e.response;

    return handleError(status, data);
  }
};

export const getAuthToken = () => {
  return localStorage.getItem(USER_TOKEN) || undefined;
};

export const requestAuth = (url, options, replace) => {
  const token = getAuthToken();
  const authOption = {
    headers: {
      ...defaultOptions.headers,
      Authorization: `Bearer ${token}`,
    },
    ...options,
  };
  return request(url, authOption, replace);
};

export const requestAuthBlob = async (url, options, headers) => {
  const token = getAuthToken();
  const authOption = {
    headers: {
      ...defaultOptions.headers,
      Authorization: `Bearer ${token}`,
      ...headers,
    },
    ...options,
    responseType: 'blob',
  };
  return request(url, authOption);
};
