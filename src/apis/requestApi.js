import { requestAuth } from './request';
import { REQUEST_STATUS } from '../constants/COMMON';

export const list = ({ mode = 'public', status = REQUEST_STATUS[0].value }) => {
  return requestAuth('api/customer/request/list', {
    method: 'get',
    params: {
      mode,
      status,
      size: 100,
    },
  });
};

export const remove = requestId => {
  return requestAuth('api/customer/request/detail', {
    method: 'delete',
    data: {
      request_id: requestId,
    },
  });
};

export const newRequest = data => {
  const input = data;
  input.min_price =
    typeof data.min_price === 'string' ? Number(data.min_price.replace(/,/gi, '')) : data.min_price;
  input.max_price =
    typeof data.max_price === 'string' ? Number(data.max_price.replace(/,/gi, '')) : data.max_price;
  return requestAuth('api/customer/request/detail', {
    method: 'post',
    data: input,
  });
};
export const update = data => {
  const params = { ...data };
  if (params.min_price || params.max_price) {
    params.min_price =
      typeof params.min_price === 'string'
        ? Number(params.min_price.replace(/,/gi, ''))
        : params.min_price;
    params.max_price =
      typeof params.max_price === 'string'
        ? Number(params.max_price.replace(/,/gi, ''))
        : params.max_price;
  }
  return requestAuth('api/customer/request/detail', {
    method: 'put',
    data: params,
  });
};
export const upload = file => {
  const form = new FormData();
  form.append('image', file, file.name);
  return requestAuth('api/house/image', {
    method: 'post',
    data: form,
  });
};

export const detail = requestId => {
  return requestAuth(`api/customer/request/detail?request_id=${requestId}`, {
    method: 'get',
  });
};

export default {
  list,
  remove,
  update,
  new: newRequest,
  upload,
  detail,
};
