/* eslint-disable camelcase */
import axios from 'axios';
import { requestAuth, requestAuthBlob } from './request';
import { apiSerializer } from '../helpers/apiSerializer';
import { MAP_GEOCODE } from '../constants/APP_CONST';

export const list = ({ role, page, size, search }) => {
  return requestAuth('api/user/list', {
    method: 'get',
    params: {
      role: role || undefined,
      page: page || 1,
      size: size || 10000,
      search,
    },
  });
};

export const getUserListByManagerId = () => {
  return requestAuth('api/user/list-by-condition', {
    method: 'get',
  });
};
export const getAll = () => {
  return requestAuth('api/user/get-all', {
    method: 'get',
  });
};

export const removeUser = user_id => {
  return requestAuth('api/user/detail', {
    method: 'delete',
    data: {
      user_id,
    },
  });
};

export const newUser = data => {
  return requestAuth('api/user/detail', {
    method: 'post',
    data,
  });
};

export const changePassword = (old_password, new_password) => {
  return requestAuth('api/auth/password/change', {
    method: 'post',
    data: {
      old_password,
      new_password,
    },
  });
};

export const alonhadatUpdate = (alonhadat_name, alonhadat_password) => {
  return requestAuth('api/user/alonhadat', {
    method: 'put',
    data: {
      alonhadat_name,
      alonhadat_password,
    },
  });
};

export const updateUser = data => {
  return requestAuth('api/user/detail', {
    method: 'put',
    data,
  });
};
export const updateAvatar = imageId => {
  return requestAuth('api/user/avatar', {
    method: 'put',
    data: {
      profile_picture: imageId,
    },
  });
};
export const detail = user_id => {
  return requestAuth(`api/user/detail?user_id=${user_id}`, {
    method: 'get',
  });
};
export const loadMe = () => {
  return requestAuth('api/user/detail', {
    method: 'get',
  });
};

export const notifications = (size = 10, page) => {
  return requestAuth('api/notification/all', {
    method: 'get',
    params: {
      page: page || 1,
      size: size || 100,
    },
  });
};

export const comment = (id, content, image) => {
  return requestAuth(`api/house/${id}/comment`, {
    method: 'post',
    data: { content, image },
  });
};

export const likeHouse = id => {
  return requestAuth(`api/house/${id}/like`, {
    method: 'post',
  });
};

export const toggleCommentStatus = id => {
  return requestAuth(`api/house/${id}/status`, {
    method: 'post',
  });
};

export const comments = id => {
  return requestAuth(`api/notification/${id}/comment`, {
    method: 'get',
  });
};

export const readNotification = id => {
  return requestAuth(`api/notification/seen/${id}`, {
    method: 'post',
  });
};

export const resetPassword = user_id => {
  return requestAuth('api/user/reset/password', {
    method: 'put',
    data: {
      user_id,
    },
  });
};

export const exportExcel = (type, data) => {
  return requestAuthBlob(
    `api/export/${type}`,
    {
      method: 'post',
      data,
    },
    {},
  );
};

export const getLastLocation = () => {
  return requestAuth(
    `api/user/location/last`,
    {
      method: 'get',
    },
    {},
  ).then(data => apiSerializer(data));
};

export const getAllUserLocations = ({ size = 10, page }) => {
  return requestAuth(
    `api/user/location/track`,
    {
      method: 'get',
      params: { size, page },
    },
    {},
  ).then(data => apiSerializer(data));
};

export const getLocationsById = ({ user_id, size = 10, page }) => {
  return requestAuth(
    `api/user/location/track/${user_id}`,
    {
      method: 'get',
      params: { size, page },
    },
    {},
  ).then(data => apiSerializer(data));
};

export const getIpAddress = async () => {
  const response = await axios({
    url: `https://ipinfo.io?token=048aec3dc105b1`,
    headers: {
      'Content-Type': 'application/json',
    },
  });
  return response.data;
};

export const getGeoAddressFromCoordinate = async (lat, long) => {
  // const tempAPI = 'AIzaSyCjkxqm-PlCFyI3IeyZVs_duxs7H7_R8Qc';
  const tempAPI2 = 'AIzaSyDQ-rVF-Ulpp5LAWk6a_WXYuTgfVA50jSY';
  // process.env.REACT_APP_GOOGLE_API
  const response = await axios({
    url: `${MAP_GEOCODE}?latlng=${lat},${long}&key=${tempAPI2}`,
    headers: {
      'Content-Type': 'application/json',
    },
  });
  if (response.data) {
    const { results } = response.data;
    if (results && results.length > 0) {
      return results[0].formatted_address;
    }
  }
  return null;
};

export const trackLocation = location => {
  return requestAuth(
    `api/user/location/track`,
    {
      method: 'post',
      data: location,
    },
    {},
  );
};
export const getPostManager = data => {
  return requestAuth(
    `api/user/post-manager-condition`,
    {
      method: 'post',
      data,
    },
    {},
  );
};

export const getPostManagerWaitingList = data => {
  return requestAuth(
    `api/user/post-manager-waiting`,
    {
      method: 'post',
      data,
    },
    {},
  );
};

export const getPostManagerErrorList = data => {
  return requestAuth(
    `api/user/post-manager-error`,
    {
      method: 'post',
      data,
    },
    {},
  );
};
export const getHouse = () => {
  return requestAuth(
    `api/user/post-manager-house`,
    {
      method: 'get',
    },
    {},
  );
};
export const createPostManager = data => {
  return requestAuth(
    `api/user/post-manager`,
    {
      method: 'post',
      data,
    },
    {},
  );
};
export const updatePostManager = data => {
  return requestAuth(
    `api/user/post-manager`,
    {
      method: 'put',
      data,
    },
    {},
  );
};
export const deletePostManager = data => {
  return requestAuth(
    `api/user/post-manager`,
    {
      method: 'delete',
      data,
    },
    {},
  );
};
export const createPostAddress = data => {
  return requestAuth(
    `api/user/post-address`,
    {
      method: 'post',
      data,
    },
    {},
  );
};
export const updatePostAddress = data => {
  return requestAuth(
    `api/user/post-address`,
    {
      method: 'put',
      data,
    },
    {},
  );
};

export const getPostAddress = data => {
  return requestAuth(
    `api/user/post-address`,
    {
      method: 'get',
      data,
    },
    {},
  );
};
export const getPostHistory = data => {
  return requestAuth(
    `api/user/post-manager/history`,
    {
      method: 'post',
      data,
    },
    {},
  );
};
export const getPostAddressActive = data => {
  return requestAuth(
    `api/user/post-address-active`,
    {
      method: 'get',
      data,
    },
    {},
  );
};
export const getPostAddressStatus = data => {
  return requestAuth(
    `api/user/post-address/status`,
    {
      method: 'get',
      data,
    },
    {},
  );
};
export const getPostAddressStatusActive = data => {
  return requestAuth(
    `api/user/post-address/status-active`,
    {
      method: 'get',
      data,
    },
    {},
  );
};
export const updatePostAddressStatus = data => {
  return requestAuth(
    `api/user/post-address/status`,
    {
      method: 'put',
      data,
    },
    {},
  );
};
export const getUserById = data => {
  return requestAuth(
    `api/user/list-by-id`,
    {
      method: 'post',
      data,
    },
    {},
  );
};
export const deleteAlonhadatPosted = data => {
  return requestAuth(`api/user/post/alonhadat/delete`, {
    method: 'post',
    data,
  });
};
export default {
  getUserListByManagerId,
  list,
  removeUser,
  newUser,
  updateUser,
  detail,
  loadMe,
  changePassword,
  resetPassword,
  notifications,
  readNotification,
  updateAvatar,
  likeHouse,
  comment,
  comments,
  toggleCommentStatus,
  exportExcel,
  getLastLocation,
  trackLocation,
  alonhadatUpdate,
  deleteAlonhadatPosted,
  getPostManager,
  getPostManagerWaitingList,
  getPostManagerErrorList,
};
