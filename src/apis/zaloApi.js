/* eslint-disable camelcase */
import { requestAuth } from './request';

export const getAuthUrl = (callbackUrl = '', param = '') =>
  `https://oauth.zaloapp.com/v3/auth?app_id=${
    process.env.REACT_APP_ZALO_APP_ID
  }&redirect_uri=${process.env.REACT_APP_WEB_URL + callbackUrl}&state=${param}`;

export const updateZaloToken = code => {
  return requestAuth('api/zalo/token', {
    method: 'put',
    data: {
      code,
    },
  });
};

export const postZalo = ({ house_id, message }) => {
  return requestAuth('api/zalo/feed', {
    method: 'post',
    data: {
      house_id,
      message,
    },
  });
};

export const getFriendList = () => {
  return requestAuth('api/zalo/friends', {
    method: 'get',
    param: {},
  });
};

export const getZaloProfile = () => {
  return requestAuth('api/zalo/me', {
    method: 'get',
    param: {},
  });
};

export default {
  updateZaloToken,
  getFriendList,
  getZaloProfile,
  getAuthUrl,
  postZalo,
};
