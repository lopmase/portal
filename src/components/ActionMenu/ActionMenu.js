import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './ActionMenu.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons';

const ActionMenu = props => {
  const [active, setActive] = useState(false);
  const { onSelect, items, containerStyle, menuStyle, render } = props;

  const toggle = () => {
    setActive(!active);
  };

  const select = item => {
    setActive(false);
    onSelect(item);
  };

  return (
    <div className={classNames('action-menu', { active })}>
      <div
        className={classNames('main', {
          active,
        })}
        style={{ ...containerStyle }}
        onClick={toggle}
      >
        {render ? (
          render(active)
        ) : (
          <div
            className={classNames('btn-holder', {
              active,
            })}
          >
            <FontAwesomeIcon icon={faEllipsisV} />
          </div>
        )}
      </div>
      <div
        className={classNames('menu-content', {
          active,
        })}
        style={{ ...menuStyle }}
      >
        {items.map(item => (
          <div onClick={() => select(item)} key={item.value} className="item">
            <div>
              {item.icon && <FontAwesomeIcon icon={item.icon} style={{ marginRight: 10 }} />}
              {item.text}
            </div>
            {item.meta && <div className="meta">{item.meta}</div>}
          </div>
        ))}
      </div>
    </div>
  );
};

ActionMenu.propTypes = {
  onSelect: PropTypes.func,
  render: PropTypes.func,
  items: PropTypes.arrayOf(PropTypes.object),
  containerStyle: PropTypes.object,
  menuStyle: PropTypes.object,
};

ActionMenu.defaultProps = {
  onSelect: () => {},
  render: null,
  items: [],
  containerStyle: {},
  menuStyle: {},
};

export default ActionMenu;
