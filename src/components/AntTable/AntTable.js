/* eslint-disable react/default-props-match-prop-types */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/jsx-indent */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';

const AntTable = ({
  data,
  rowClick,
  onChanged,
  columns,
  width,
  loading,
  height,
  autoSize,
  rowClassName,
  components,
  size = 'small',
  pagination,
}) => {
  const [prevSorting, setPrevSorting] = useState(null);
  const handleTableChange = (_pagination, filters, sorter) => {
    onChanged({
      sortField: sorter.field,
      sortOrder: sorter.order,
      sortChanged: (sorter.field || prevSorting) && sorter.field + sorter.order !== prevSorting,
      filters,
    });
    if (sorter.field) {
      setPrevSorting(sorter.field + sorter.order);
    }
  };

  return (
    <Table
      components={components}
      columns={columns}
      className="tg-table-custom"
      useFixedHeader
      size={size}
      scroll={autoSize ? undefined : { x: width, y: height }}
      rowKey={record => record.id}
      dataSource={data}
      onRow={(record, rowIndex) => {
        return {
          onClick: () => rowClick({ index: rowIndex, rowData: record }),
        };
      }}
      pagination={pagination}
      loading={loading}
      onChange={handleTableChange}
      rowClassName={rowClassName}
      filterResetToDefaultFilteredValue
    />
  );
};

AntTable.propTypes = {
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  rowClick: PropTypes.func,
  onChanged: PropTypes.func,
  loading: PropTypes.bool,
  width: PropTypes.number,
  height: PropTypes.number,
  autoSize: PropTypes.bool,
  rowClassName: PropTypes.func,
  components: PropTypes.object,
  size: PropTypes.string,
  pagination: PropTypes.bool,
};

AntTable.defaultProps = {
  loading: false,
  height: 600,
  width: 3500,
  autoSize: false,
  style: {},
  rowClick: () => {},
  onChanged: () => {},
  rowClassName: () => {},
  components: {},
  size: '',
  pagination: false,
};

export default AntTable;
