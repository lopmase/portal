/* eslint-disable react/no-array-index-key */
/* eslint-disable react/jsx-indent */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AntTable from './AntTable';
import ResizeableTitle from './ResizeableTitle';
import { get, set, remove } from '../../storage/browserStorage';
import { LOCAL_STORAGE_HOUSE_TABLE_KEY } from '../../constants/STORAGE_KEYS';
import 'antd/dist/antd.css';

const DEFAULT_MIN_WIDTH = 70;

class ResizeableTable extends Component {
  constructor(props) {
    super(props);
    const { columns } = this.props;
    this.state = { columns };
  }

  componentWillReceiveProps(props) {
    const { reset } = this.props;
    if (props.reset !== reset) {
      remove(LOCAL_STORAGE_HOUSE_TABLE_KEY);
      this.setState({ columns: props.columns });
    }
  }

  componentDidUpdate(prevProps) {
    const { columns } = this.props;
    if (columns.length !== prevProps.columns.length) {
      this.setState({ columns });
    }
  }

  storeLocal = (columnId, storeObj) => {
    const storedColumns = get(LOCAL_STORAGE_HOUSE_TABLE_KEY) || {};
    storedColumns[columnId] = storeObj;
    set(LOCAL_STORAGE_HOUSE_TABLE_KEY, storedColumns);
  };

  handleResize = index => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      this.storeLocal(nextColumns[index].dataIndex, { width: size.width });
      return { columns: nextColumns };
    });
  };

  render() {
    const {
      data,
      rowClick,
      onChanged,
      width,
      loading,
      height,
      autoSize,
      style,
      rowClassName,
    } = this.props;

    const { columns } = this.state;
    const components = {
      header: {
        cell: ResizeableTitle,
      },
    };
    const storedColumns = get(LOCAL_STORAGE_HOUSE_TABLE_KEY) || {};
    const renderColumns = columns.map((col, index) => {
      const storedColumn = storedColumns[col.dataIndex];
      const renderWidth = storedColumn ? storedColumn.width : col.width;
      return {
        ...col,
        width: renderWidth,
        onHeaderCell: column => ({
          width: column.width || DEFAULT_MIN_WIDTH,
          onResize: this.handleResize(index),
          minWidth: DEFAULT_MIN_WIDTH,
        }),
      };
    });

    return (
      <AntTable
        components={components}
        columns={renderColumns}
        rowClick={rowClick}
        height={height}
        onChanged={onChanged}
        loading={loading}
        rowClassName={rowClassName}
        data={data}
        width={width}
        style={style}
        autoSize={autoSize}
      />
    );
  }
}

ResizeableTable.propTypes = {
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  // renderCell: PropTypes.func,
  rowClick: PropTypes.func,
  onChanged: PropTypes.func,
  // onHeaderClick: PropTypes.func,
  loading: PropTypes.bool,
  width: PropTypes.number,
  height: PropTypes.number,
  style: PropTypes.object,
  autoSize: PropTypes.bool,
  rowClassName: PropTypes.func,
  reset: PropTypes.bool,
  // sortColumn: PropTypes.string,
  // sortOrder: PropTypes.string,
};

ResizeableTable.defaultProps = {
  // renderCell: undefined,
  // selectable: false,
  loading: false,
  height: 600,
  width: 3200,
  autoSize: false,
  style: {},
  rowClick: () => {},
  // onHeaderClick: () => {},
  onChanged: () => {},
  // sortColumn: '',
  // sortOrder: 'asc',
  rowClassName: () => {},
  reset: false,
};

export default ResizeableTable;
