/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Autocomplete.scss';

const Autocomplete = props => {
  const [active, setActive] = useState(false);
  const { value, onChange, onSelect, items, placeholder, containerStyle } = props;

  const select = item => {
    setActive(false);
    onSelect({ item });
  };

  return (
    <div className="autocomplete">
      <div
        className={`main${active ? ' active' : ''}${value ? '' : ' placeholder'}`}
        style={{ ...containerStyle }}
        onClick={() => setActive(!active)}
      >
        <input
          value={value}
          onChange={event => onChange(event.target.value)}
          placeholder={placeholder}
        />
      </div>
      <div
        className={classNames('autocomplete-content', {
          active,
        })}
      >
        {items.map(item => (
          <div onClick={() => select(item)} key={item.value} className="item">
            <div>{item.text}</div>
            {item.meta && <div className="meta">{item.meta}</div>}
          </div>
        ))}
      </div>
    </div>
  );
};

Autocomplete.propTypes = {
  onChange: PropTypes.func,
  onSelect: PropTypes.func,
  value: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(PropTypes.object),
  placeholder: PropTypes.string,
  containerStyle: PropTypes.object,
};

Autocomplete.defaultProps = {
  onChange: () => {},
  onSelect: () => {},
  items: [],
  placeholder: undefined,
  containerStyle: undefined,
};

export default Autocomplete;
