/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState } from 'react';
import PropTypes from 'prop-types';

import './Avatar.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';

const Avatar = ({
  image,
  id,
  onImageAdded,
  onImageRemoved,
  containerStyle,
  label,
  removeIconVisible,
}) => {
  const [loading, setLoading] = useState(false);

  const readImage = file => {
    if (!file) return;

    setLoading(true);
    if (file.size > 5000000) {
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      setLoading(false);

      onImageAdded({ data: reader.result, file });
    };
    reader.onerror = () => {
      setLoading(false);
    };
  };

  const onImageChange = event => {
    const file = event.target.files.item(0);

    readImage(file);
  };

  return (
    <div className="avatar" style={{ ...containerStyle }}>
      <label htmlFor={`avatar${id}`}>
        {image ? (
          <div className="photo">
            <img alt="p" src={image.data || image.url} />
            {removeIconVisible && (
              <div className="delete" onClick={() => onImageRemoved()}>
                <FontAwesomeIcon icon={faTrashAlt} color="white" />
              </div>
            )}
          </div>
        ) : (
          <div className="placeholder" />
        )}

        <div className="text">{label || 'Chọn ảnh'}</div>
      </label>
      <input
        type="file"
        accept="image/*"
        disabled={loading}
        id={`avatar${id}`}
        onChange={event => onImageChange(event)}
      />
    </div>
  );
};

Avatar.propTypes = {
  onImageAdded: PropTypes.func,
  onImageRemoved: PropTypes.func,
  id: PropTypes.string,
  label: PropTypes.string,
  removeIconVisible: PropTypes.bool,
  image: PropTypes.object,
  containerStyle: PropTypes.object,
};

Avatar.defaultProps = {
  onImageAdded: () => {},
  onImageRemoved: () => {},
  id: new Date().getTime().toString(),
  label: null,
  removeIconVisible: true,
  image: null,
  containerStyle: undefined,
};

export default Avatar;
