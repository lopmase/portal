import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from '../../styles/_Colors.scss';

import './Button.scss';

const Button = props => {
  const {
    text,
    onClick,
    loading,
    backgroundColor,
    textColor,
    containerStyle,
    disabled,
    clear,
    icon,
    iconColor,
    iconOnly = false,
    small,
    border,
    ortherClass,
  } = props;
  return (
    <button
      type="button"
      disabled={loading || disabled}
      className={classNames('btn', {
        clear,
        border,
        small,
        ortherClass,
      })}
      style={{
        opacity: loading || disabled ? 0.5 : 1,
        backgroundColor: backgroundColor || styles.primaryColor,
        color: textColor || 'white',
        ...containerStyle,
      }}
      onClick={onClick}
    >
      {icon && (
        <FontAwesomeIcon
          width={12}
          style={{ marginRight: iconOnly ? 0 : 10 }}
          height={12}
          color={iconColor}
          icon={icon}
        />
      )}
      {loading ? 'Loading' : text}
    </button>
  );
};

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
  loading: PropTypes.bool,
  clear: PropTypes.bool,
  backgroundColor: PropTypes.string,
  textColor: PropTypes.string,
  iconColor: PropTypes.string,
  containerStyle: PropTypes.object,
  disabled: PropTypes.bool,
  icon: PropTypes.any,
  iconOnly: PropTypes.bool,
  border: PropTypes.bool,
  small: PropTypes.bool,
  ortherClass: PropTypes.any,
};

Button.defaultProps = {
  text: '',
  loading: false,
  iconColor: 'white',
  clear: false,
  backgroundColor: undefined,
  textColor: undefined,
  disabled: false,
  containerStyle: undefined,
  icon: undefined,
  iconOnly: false,
  onClick: undefined,
  border: false,
  small: false,
  ortherClass: '',
};

export default Button;
