import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './Card.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

const Card = props => {
  const onKeyPressHandler = () => {};

  const {
    minWidth,
    hoverable,
    containerStyle,
    noPadding,
    shadow,
    key,
    loading,
    children,
    onClick,
  } = props;
  return (
    <div
      key={key}
      className={classNames('card', {
        shadow,
        padding: !noPadding,
        hoverable,
      })}
      onKeyPress={onKeyPressHandler}
      onClick={onClick}
      role="button"
      tabIndex="0"
      style={{ minWidth: minWidth || 'auto', ...containerStyle }}
    >
      {children}
      {loading && (
        <div className="loading">
          <FontAwesomeIcon icon={faSpinner} size="3x" spin color="white" />
        </div>
      )}
    </div>
  );
};

Card.propTypes = {
  minWidth: PropTypes.number,
  containerStyle: PropTypes.object,
  noPadding: PropTypes.bool,
  onClick: PropTypes.func,
  shadow: PropTypes.bool,
  hoverable: PropTypes.bool,
  loading: PropTypes.bool,
  key: PropTypes.string,
  children: PropTypes.node.isRequired,
};

Card.defaultProps = {
  minWidth: undefined,
  containerStyle: {},
  shadow: false,
  loading: false,
  hoverable: false,
  key: undefined,
  onClick: () => {},
  noPadding: false,
};

export default Card;
