/* eslint-disable jsx-a11y/label-has-for */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import '../../styles/_Colors.scss';
import './Checkbox.scss';

import Flexbox from '../Flexbox/Flexbox';

class Checkbox extends Component {
  static propTypes = {
    onChange: PropTypes.func,
    containerStyle: PropTypes.object,
    label: PropTypes.string,
    checked: PropTypes.bool,
  };

  static defaultProps = {
    onChange: () => {},
    containerStyle: {},
    label: '',
    checked: false,
  };

  check = () => {
    const { onChange, checked } = this.props;
    if (onChange) {
      onChange(!checked);
    }
  };

  render() {
    const { containerStyle, label, checked } = this.props;

    return (
      <div
        role="button"
        tabIndex="0"
        onClick={this.check}
        className="checkbox"
        style={{ ...containerStyle }}
      >
        <Flexbox row flexStart>
          <div className={`main${checked ? ' checked' : ''}`}>
            {checked && <div className="checkmark" />}
          </div>
          {label && <label htmlFor="label">{label}</label>}
        </Flexbox>
      </div>
    );
  }
}

export default Checkbox;
