import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Checkbox from '../Checkbox/Checkbox';

class ControlledCheckbox extends Component {
  static propTypes = {
    onChange: PropTypes.func,
    containerStyle: PropTypes.object,
    label: PropTypes.string,
    checked: PropTypes.bool,
  };

  static defaultProps = {
    onChange: () => {},
    containerStyle: {},
    label: '',
    checked: false,
  };

  check = () => {
    const { onChange, checked } = this.props;
    onChange && onChange(!checked);
  };

  render() {
    const { containerStyle, label, checked } = this.props;

    return (
      <Checkbox
        containerStyle={containerStyle}
        label={label}
        checked={checked}
        onChange={this.check}
      />
    );
  }
}

export default ControlledCheckbox;
