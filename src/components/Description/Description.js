import React from 'react';
import PropTypes from 'prop-types';
import './Decription.scss';
import { Card, FormInput } from '..';

const Description = ({ form, handleChange }) => {
  return (
    <Card shadow containerStyle={{ flex: 1, minWidth: 300, height: '40%' }}>
      <FormInput form={form} name="key_word" label="Key Word" handleChange={handleChange} />
      <FormInput
        maxLength={160}
        form={form}
        name="descriptions"
        label="Description"
        handleChange={handleChange}
      />
    </Card>
  );
};

Description.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
};

Description.defaultProps = {};

export default Description;
