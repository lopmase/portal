import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './Dialog.scss';

class Dialog extends Component {
  check = () => {
    const { onChange, checked } = this.props;
    if (onChange) {
      onChange(!checked);
    }
  };

  render() {
    const { containerStyle, header, renderContent } = this.props;

    return (
      <div className="dialog" style={{ ...containerStyle }}>
        <div className="header">
          {header}
          <div className="close">
            <FontAwesomeIcon icon={faTimes} color="white" />
          </div>
        </div>
        <div className="content">{renderContent ? renderContent() : <div>Content</div>}</div>
      </div>
    );
  }
}

Dialog.propTypes = {
  onChange: PropTypes.func,
  containerStyle: PropTypes.object,
  header: PropTypes.node.isRequired,
  renderContent: PropTypes.oneOfType([PropTypes.node, PropTypes.func]),
  checked: PropTypes.bool,
};

Dialog.defaultProps = {
  onChange: () => {},
  containerStyle: {},
  renderContent: undefined,
  checked: false,
};

export default Dialog;
