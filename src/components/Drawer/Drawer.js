/* eslint-disable react/no-did-update-set-state */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faHome,
  faUser,
  faBuilding,
  faUserFriends,
  faFolderOpen,
  faArrowLeft,
  faArrowRight,
  faCommentDollar,
  faNewspaper,
  faAlignLeft,
  faSms,
} from '@fortawesome/free-solid-svg-icons';
import { ReactComponent as ArrowUp } from '../../assets/images/arrow-up-right-dots-solid-logo2.svg';
import { ReactComponent as ListCheck } from '../../assets/images/list-check-solid6.svg';
import { ReactComponent as Dollar } from '../../assets/images/sack-dollar-solid4.svg';
import { logout } from '../../container/AuthContainer/AuthAction';

import './Drawer.scss';
import 'font-awesome/css/font-awesome.min.css';
import { ROUTES } from '../../constants/ROUTES';

import Text from '../Text/Text';
import Button from '../Button/Button';
import { isAdmin } from '../../helpers/roleUtil';
import { REDUCERS } from '../../constants/COMMON';
import { generateModalActions } from '../../helpers/modalAction';
import Flexbox from '../Flexbox/Flexbox';
import avatar from '../../assets/images/avt2.png';
import { formatImageUrl } from '../../helpers/common';

const { openModal } = generateModalActions(REDUCERS.USER_PROFILE_MODAL);

export const menuItems = [
  {
    route: ROUTES.DASHBOARD,
    name: 'Trang Chủ',
    icon: faHome,
  },
  {
    route: ROUTES.HOUSES,
    name: 'BẤT ĐỘNG SẢN',
    icon: faBuilding,
  },
  {
    route: ROUTES.CUSTOMERS,
    name: 'Khách hàng',
    icon: faUserFriends,
    staffPermission: 1,
  },
  {
    route: ROUTES.REQUESTS,
    name: 'Nhu cầu',
    icon: <Dollar />,
    fontawesome: true,
    staffPermission: 1,
  },
  {
    route: ROUTES.PROJECTS,
    name: 'Dự án',
    icon: faFolderOpen,
    requireAdmin: true,
    staffPermission: 1,
  },
  {
    route: ROUTES.BLOGS,
    name: 'Tin tức',
    icon: faNewspaper,
    requireAdmin: true,
    staffPermission: 1,
  },
  {
    route: ROUTES.ADMIN_ROOT,
    name: 'Quản trị',
    icon: faUser,
    requireAdmin: true,
    staffPermission: 1,
  },
  {
    route: ROUTES.EMPLOYMENT_DIAGRAM,
    name: 'Sơ đồ nhân sự',
    icon: faAlignLeft,
    requireAdmin: true,
    staffPermission: 1,
  },
  {
    route: ROUTES.CHAT,
    name: 'Chat',
    icon: faCommentDollar,
    requireAdmin: true,
    staffPermission: 1,
  },
  {
    route: ROUTES.POST_MANAGER,
    name: 'Quản lý tin đăng',
    icon: <ListCheck />,
    fontawesome: true,
    staffPermission: 1,
  },
  {
    route: ROUTES.OVERVIEW_BUSINESS_ROOT,
    name: 'Tổng quan',
    icon: <ArrowUp />,
    fontawesome: true,
    requireAdmin: false,
  },
  {
    route: ROUTES.KNOWLEDGE_ROOT,
    name: 'kiến thức',
    icon: <ArrowUp />,
    fontawesome: true,
    requireAdmin: false,
  },
];

class Drawer extends Component {
  static propTypes = {
    history: PropTypes.object,
    logout: PropTypes.func.isRequired,
    openProfile: PropTypes.func.isRequired,
    location: PropTypes.any,
    user: PropTypes.any,
    loading: PropTypes.bool,
    compact: PropTypes.bool,
    onModeChanged: PropTypes.func.isRequired,
    status: PropTypes.object,
  };

  static defaultProps = {
    history: {},
    location: {},
    user: {},
    loading: true,
    compact: false,
    status: {},
  };

  state = {
    newMessage: false,
    newMessageLength: 0,
  };

  componentDidMount() {
    const { status } = this.props;
    if (status && status.length > 0) {
      const message = status.filter(st => st.read === 0);
      message.length > 0 && this.setState({ newMessage: true });
    }
  }

  componentDidUpdate(previousProps, previousState) {
    const { status } = this.props;
    if (previousProps.status !== this.props.status && status && status.length > 0) {
      const message = status.filter(st => st.read === 0);
      message.length > 0 && this.setState({ newMessage: true, newMessageLength: message.length });
      message.length === 0 && this.setState({ newMessage: false, newMessageLength: 0 });
    }
  }

  handleLogout = () => {
    const { history } = this.props;
    history.push(ROUTES.LOGIN);
    this.props.logout();
  };

  render() {
    const { location, loading, user, compact, onModeChanged, openProfile } = this.props;
    const { newMessage, newMessageLength } = this.state;

    return (
      <div className={classNames('drawer-container', { compact })}>
        <Flexbox spaceBetween containerStyle={{ flexDirection: 'column', height: '100%' }}>
          <div style={{ width: '100%' }}>
            <div className="logo">THỊNH GIA</div>
            <div className="meta">Gia đình thịnh vượng</div>
            <div className="avatar-container" onClick={openProfile}>
              <img
                src={user && user.avatar ? formatImageUrl(user.avatar.thumbnail) : avatar}
                alt=""
              />
              <div className="name">
                <Text
                  containerStyle={{ textAlign: 'center' }}
                  loaderStyle={{ display: 'inline-block' }}
                  text={user ? user.name : ''}
                  loading={loading}
                />
              </div>
            </div>
            <div className="menu">
              {menuItems
                .filter(
                  item =>
                    !item.requireAdmin ||
                    (user && isAdmin(user)) ||
                    (user && user.chat_permission === item.staffPermission),
                )
                .map(item => (
                  <Link key={item.route} to={item.route}>
                    <div className={`item${location.pathname === item.route ? ' active' : ''}`}>
                      {item.fontawesome ? item.icon : <FontAwesomeIcon icon={item.icon} />}
                      <div className="text">{`${item.name}`}</div>
                      {newMessage && item.route === ROUTES.CHAT && (
                        <>
                          <div className="text">
                            <FontAwesomeIcon icon={faSms} />
                          </div>
                          <div className="text">{newMessageLength}</div>
                        </>
                      )}
                    </div>
                  </Link>
                ))}
            </div>
          </div>
          <div style={{ textAlign: 'right', width: '100%' }}>
            <Button
              onClick={onModeChanged}
              clear
              containerStyle={{ marginRight: compact ? 10 : 20, marginBottom: 20 }}
              icon={compact ? faArrowRight : faArrowLeft}
              iconOnly
              iconColor="white"
            />
          </div>
        </Flexbox>
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    user: state.auth.user.data,
    loading: state.auth.user.fetching,
    message: state.chat.data.message,
    status: state.chat.data.status,
    phoneNumber: state.chat.data.phoneNumber,
    state,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      logout,
      openProfile: openModal,
    },
    dispatch,
  );
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Drawer));
