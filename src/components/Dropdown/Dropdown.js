/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-nested-ternary */
import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './Dropdown.scss';
import Flexbox from '../Flexbox/Flexbox';
import { escapeUnicode } from '../../helpers/formatter';
import useOutsideAlerter from '../../hooks/useOutsiderAlerter';

const Dropdown = props => {
  const wrapperRef = useRef(null);

  const [active, setActive] = useState(false);
  const [searchValue, setSearch] = useState('');
  const {
    selected,
    items,
    placeholder,
    containerStyle,
    search,
    onSearch,
    clear,
    contentHeight,
  } = props;
  let mainRef = null;
  let handleRef = null;
  let inputRef = null;

  console.log('item', items);
  console.log('selected', selected);

  const select = item => {
    setActive(false);
    if (search) {
      setSearch('');
    }
    const { onChanged } = props;
    onChanged && onChanged({ item });
  };

  const onSearchChanged = event => {
    setSearch(event.target.value);
    if (onSearch) {
      onSearch(event.target.value);
    }
  };

  const handleClick = event => {
    if (!active) {
      setActive(true);
      inputRef.focus();
    } else if (event.target === handleRef) {
      setActive(false);
    }

    if (event.target === mainRef) {
      inputRef.focus();
    }
  };

  const handleBlur = () => {
    setActive(false);
  };

  const filterItem = item => {
    if (search) {
      if (!onSearch) {
        return (
          escapeUnicode(item.text)
            .toUpperCase()
            .includes(searchValue.toUpperCase()) ||
          item.text.toUpperCase().includes(searchValue.toUpperCase())
        );
      }
    }
    return true;
  };

  useOutsideAlerter(wrapperRef, handleBlur);

  const selectedItem = items.filter(i => i.value === selected)[0];

  return (
    <div className={classNames('dropdown', { clear })} ref={wrapperRef}>
      <div
        className={classNames('main', {
          active,
          placeholder: !selected,
        })}
        ref={ele => {
          mainRef = ele;
        }}
        style={{ ...containerStyle }}
        onClick={handleClick}
      >
        <Flexbox row spaceBetween>
          {search ? (
            <div style={{ flex: 1 }}>
              <input
                placeholder={active && selectedItem ? selectedItem.text : placeholder}
                ref={ele => {
                  inputRef = ele;
                }}
                value={active ? searchValue : selectedItem ? selectedItem.text : ''}
                onChange={onSearchChanged}
              />
            </div>
          ) : (
            <div
              ref={ele => {
                inputRef = ele;
              }}
            >
              {selectedItem ? selectedItem.text : placeholder}
            </div>
          )}
          <div className="handle-container">
            <div
              ref={ele => {
                handleRef = ele;
              }}
              className={`handle${active ? ' active' : ''}`}
            />
          </div>
        </Flexbox>
      </div>
      <div
        className={classNames('dropdown-content', {
          active,
        })}
        style={{ maxHeight: contentHeight }}
      >
        {items.filter(filterItem).map(item => (
          <div onClick={() => select(item)} key={item.value} className="item">
            <div>{item.text}</div>
            {item.meta && <div className="meta">{item.meta}</div>}
          </div>
        ))}
      </div>
    </div>
  );
};

Dropdown.propTypes = {
  onChanged: PropTypes.func,
  onSearch: PropTypes.func,
  selected: PropTypes.any,
  items: PropTypes.arrayOf(PropTypes.object),
  placeholder: PropTypes.string,
  contentHeight: PropTypes.number,
  search: PropTypes.bool,
  clear: PropTypes.bool,
  containerStyle: PropTypes.object,
};

Dropdown.defaultProps = {
  onChanged: () => {},
  onSearch: undefined,
  selected: null,
  search: false,
  contentHeight: 350,
  clear: false,
  items: [],
  placeholder: undefined,
  containerStyle: undefined,
};

export default Dropdown;
