/* eslint-disable react/no-array-index-key */
/* eslint-disable react/jsx-indent */
/* eslint no-param-reassign: ["error", { "props": false }] */

import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import AntTable from '../AntTable/AntTable';
import ResizeableTitle from './ResizeableTitle';
import TableSetting from './TableSetting';
import { get, set, remove } from '../../storage/browserStorage';
import { DEFAULT_MIN_WIDTH, DEFAULT_FIXED_COLUMN_SIDE, ANT_TABLE_FIXED_PROP } from './CONSTANT';
import 'antd/dist/antd.css';

class ExpensiveTable extends Component {
  defaultColumns = [];

  localStorageKey = '';

  constructor(props) {
    super(props);
    this.fnSettingSaved = this.fnSettingSaved.bind(this);
    this.handleResize = this.handleResize.bind(this);
    this.state = { columns: [] };
  }

  componentDidUpdate(prevProps) {
    const { columns } = this.props;
    if (!_.isEmpty(columns) && _.isEmpty(this.defaultColumns)) {
      this.defaultColumns = [...columns];
    }
    if (!_.isEqual(this.props, prevProps)) {
      this.setColumns(columns);
    }
  }

  getLocalStorageKey = () => {
    if (_.isEmpty(this.localStorageKey)) {
      const { id } = this.props;
      this.localStorageKey = id.replace(/ /g, '').concat('_expensive_table');
    }
    return this.localStorageKey;
  };

  storeLocal = (columnId, storeObj) => {
    const { columns } = this.state;
    const storedColumns = get(this.getLocalStorageKey()) || [];
    if (_.isEmpty(storedColumns)) {
      this.storeColumnConfigs2Local(columns);
      return;
    }
    const { width } = storeObj;
    const storedIndex = _.findIndex(storedColumns, column => {
      return column.dataIndex === columnId;
    });

    if (storedColumns[storedIndex]) {
      storedColumns[storedIndex].width = width;
    }

    set(this.getLocalStorageKey(), storedColumns);
  };

  handleResize = columnId => (e, { size }) => {
    this.setState(({ columns }) => {
      const nextColumns = [...columns];
      const index = _.findIndex(columns, column => {
        return column.dataIndex === columnId;
      });
      nextColumns[index] = {
        ...nextColumns[index],
        width: size.width,
      };
      this.storeLocal(nextColumns[index].dataIndex, { width: size.width });
      return { columns: nextColumns };
    });
  };

  fnSettingSaved = settings => {
    const { columns, fixedColumns } = settings;

    const filteredFixedColumns = fixedColumns.map(fixedColumn => {
      fixedColumn[ANT_TABLE_FIXED_PROP] = DEFAULT_FIXED_COLUMN_SIDE;
      return fixedColumn;
    });
    const filteredColumns = columns.map(column => {
      column[ANT_TABLE_FIXED_PROP] = false;
      return column;
    });
    const processedColumns = filteredFixedColumns.concat(filteredColumns);
    this.storeColumnConfigs2Local(processedColumns);
    return this.setState({
      columns: processedColumns,
    });
  };

  storeColumnConfigs2Local = processedColumns => {
    const { columns } = this.state;
    const filterColumns = [];
    // processedColumns = processedColumns || columns;
    _.forEach(processedColumns || columns, column => {
      const { dataIndex, width, visible, fixed } = column;
      filterColumns.push({
        dataIndex,
        width,
        visible,
        fixed,
      });
    });
    set(this.getLocalStorageKey(), filterColumns);
  };

  setColumns = rawColumns => {
    this.setState({ columns: this.applyColumnSettings(rawColumns) });
  };

  applyColumnSettings = rawColumns => {
    const storedColumns = get(this.getLocalStorageKey()) || [];
    try {
      if (_.isEmpty(storedColumns) || _.isEmpty(rawColumns)) {
        return rawColumns;
      }
      if (storedColumns.length !== rawColumns.length) {
        remove(this.getLocalStorageKey());
        return rawColumns;
      }

      const appliedColumns = [];
      _.forEach(storedColumns, storedColumn => {
        const matchedColumn = _.find(rawColumns, column => {
          return column.dataIndex === storedColumn.dataIndex;
        });
        appliedColumns.push({ ...matchedColumn, ...storedColumn });
      });
      return appliedColumns;
    } catch (e) {
      remove(this.getLocalStorageKey());
    }
    return rawColumns;
  };

  render() {
    const {
      data,
      rowClick,
      onChanged,
      width,
      loading,
      height,
      autoSize,
      style,
      rowClassName,
      toggleSetting,
    } = this.props;
    const { columns } = this.state;
    const components = {
      header: {
        cell: ResizeableTitle,
      },
    };

    const renderColumns = columns
      .filter(col => col.visible || col.channel)
      .map(col => {
        // const storedColumn = storedColumns[col.dataIndex];
        const renderWidth = col.width;
        return {
          ...col,
          width: renderWidth,
          onHeaderCell: column => ({
            width: column.width || DEFAULT_MIN_WIDTH,
            onResize: this.handleResize(col.dataIndex),
            minWidth: DEFAULT_MIN_WIDTH,
          }),
        };
      });
    return (
      <div>
        <AntTable
          components={components}
          columns={renderColumns}
          rowClick={rowClick}
          height={height}
          onChanged={onChanged}
          loading={loading}
          rowClassName={rowClassName}
          data={data}
          width={width}
          style={style}
          autoSize={autoSize}
        />
        <TableSetting
          visible={toggleSetting}
          columns={columns}
          defaultColumns={this.defaultColumns}
          fnSetting={this.fnSettingSaved}
        />
      </div>
    );
  }
}

ExpensiveTable.propTypes = {
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  id: PropTypes.string.isRequired,
  // renderCell: PropTypes.func,
  rowClick: PropTypes.func,
  onChanged: PropTypes.func,
  // onHeaderClick: PropTypes.func,
  loading: PropTypes.bool,
  width: PropTypes.number,
  height: PropTypes.number,
  style: PropTypes.object,
  autoSize: PropTypes.bool,
  rowClassName: PropTypes.func,
  toggleSetting: PropTypes.bool,
  // sortColumn: PropTypes.string,
  // sortOrder: PropTypes.string,
};

ExpensiveTable.defaultProps = {
  // renderCell: undefined,
  // selectable: false,
  loading: false,
  height: 600,
  width: 3200,
  autoSize: false,
  style: {},
  rowClick: () => {},
  // onHeaderClick: () => {},
  onChanged: () => {},
  // sortColumn: '',
  // sortOrder: 'asc',
  rowClassName: () => {},
  toggleSetting: false,
};

export default ExpensiveTable;
