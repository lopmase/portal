import React from 'react';
import 'antd/dist/antd.css';
import { Resizable } from 'react-resizable';
import PropTypes from 'prop-types';
import './ResizeableTitle.scss';

const DEFAULT_MIN_WIDTH = 70;

const ResizeableTitle = props => {
  const { minWidth, onResize, width, ...restProps } = props;

  return (
    <Resizable
      minConstraints={[minWidth, 0]}
      width={width}
      height={0}
      handle={resizeHandle => (
        <span
          className={`react-resizable-handle react-resizable-handle-${resizeHandle}`}
          onClick={e => {
            e.stopPropagation();
          }}
        />
      )}
      onResize={onResize}
      draggableOpts={{ enableUserSelectHack: false }}
    >
      <th {...restProps} />
    </Resizable>
  );
};

ResizeableTitle.propTypes = {
  width: PropTypes.number.isRequired,
  onResize: PropTypes.func.isRequired,
  minWidth: PropTypes.number,
};
ResizeableTitle.defaultProps = {
  minWidth: DEFAULT_MIN_WIDTH,
};

export default ResizeableTitle;
