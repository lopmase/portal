import React, { Component } from 'react';
import _ from 'lodash';
import { ReactSortable } from 'react-sortablejs';
import { faArrowsAltV, faRedo } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Switch, Modal } from 'antd';
import PropTypes from 'prop-types';
import { DEFAULT_FIXED_COLUMN_SIDE, ANT_TABLE_FIXED_PROP } from './CONSTANT';
import Button from '../Button/Button';

import './TableSetting.scss';

class TableSetting extends Component {
  constructor(props) {
    super(props);

    this.state = {
      columns: [],
      visible: false,
      fixedColumns: [],
    };
    this.restoreFactory = this.restoreFactory.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { visible, columns } = this.props;
    if (prevProps.visible !== visible) {
      this.showModal();
    }
    if (!_.isEqual(columns, prevProps.columns)) {
      this.groupColumns(columns);
    }
  }

  groupColumns = rawColumns => {
    const columns = [];
    const fixedColumns = [];

    if (!_.isEmpty(rawColumns)) {
      rawColumns.forEach(column => {
        if (column && column[ANT_TABLE_FIXED_PROP] === DEFAULT_FIXED_COLUMN_SIDE) {
          fixedColumns.push(column);
        } else {
          columns.push(column);
        }
      });
    }
    return this.setState({
      columns,
      fixedColumns,
    });
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    const { fnSetting } = this.props;
    fnSetting(this.state);
    this.setState({
      visible: false,
    });
  };

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };

  toggleColumnVisibility = (index, fixed) => {
    this.setState(prevState => {
      if (fixed) {
        const { fixedColumns } = prevState;
        fixedColumns[index].visible = !fixedColumns[index].visible;
        return { fixedColumns };
      }
      const { columns } = prevState;
      columns[index].visible = !columns[index].visible;
      return { columns };
    });
  };

  restoreFactory = () => {
    const { defaultColumns } = this.props;
    // remove(LOCAL_STORAGE_HOUSE_TABLE_KEY);
    this.groupColumns(defaultColumns);
    // toast.success('Khôi phục bảng thành công');
  };

  render() {
    const { columns, visible, fixedColumns } = this.state;
    return (
      <div>
        <Modal
          title="Tuỳ chỉnh bảng"
          visible={visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <div className="column-list">
            <div>
              <h1 style={{ marginRight: 10, marginBottom: 0 }}>Danh sách cột</h1>
            </div>
            <div className="column-list-content">
              <div>
                <ReactSortable
                  group="shared"
                  list={fixedColumns}
                  setList={newState => this.setState({ fixedColumns: newState })}
                  animation="150"
                >
                  {fixedColumns.map((item, index) => (
                    <div className="list-group-item moveable" key={item.dataIndex}>
                      <FontAwesomeIcon color="#ff6c00" icon={faArrowsAltV} title={item.title} />
                      <span className="title">{item.title}</span>
                      <Switch
                        className="switch"
                        checked={item.visible}
                        onChange={() => {
                          this.toggleColumnVisibility(index, true);
                        }}
                      />
                    </div>
                  ))}
                </ReactSortable>
              </div>
              <hr className="list-separator" />
              <div>
                <ReactSortable
                  group="shared"
                  list={columns}
                  setList={newState => this.setState({ columns: newState })}
                  animation="150"
                >
                  {columns.map((item, index) => (
                    <div className="list-group-item moveable">
                      <FontAwesomeIcon color="#ff6c00" icon={faArrowsAltV} title={item.title} />
                      <span className="title">{item.title}</span>
                      <Switch
                        className="switch"
                        checked={item.visible}
                        onChange={() => {
                          this.toggleColumnVisibility(index, false);
                        }}
                      />
                    </div>
                  ))}
                </ReactSortable>
              </div>
            </div>
          </div>
          <div style={{ marginTop: 20 }}>
            <Button
              containerStyle={{ width: 150 }}
              iconColor="gray"
              icon={faRedo}
              text="Khôi phục"
              backgroundColor="white"
              textColor="gray"
              small
              onClick={() => {
                this.restoreFactory();
              }}
            />
          </div>
        </Modal>
      </div>
    );
  }
}
TableSetting.propTypes = {
  columns: PropTypes.array.isRequired,
  defaultColumns: PropTypes.array.isRequired,
  visible: PropTypes.bool.isRequired,
  fnSetting: PropTypes.func.isRequired,
};
TableSetting.defaultProps = {};

export default TableSetting;
