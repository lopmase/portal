import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Flexbox.scss';

class Flexbox extends Component {
  static propTypes = {
    row: PropTypes.bool,
    spaceBetween: PropTypes.bool,
    padding: PropTypes.number,
    containerStyle: PropTypes.object,
    flexStart: PropTypes.bool,
    alignItems: PropTypes.string,
    children: PropTypes.node.isRequired,
  };

  static defaultProps = {
    row: true,
    spaceBetween: false,
    flexStart: false,
    padding: 0,
    containerStyle: {},
    alignItems: 'center',
  };

  componentDidMount() {}

  render() {
    const {
      row,
      spaceBetween,
      padding,
      containerStyle,
      flexStart,
      children,
      alignItems,
    } = this.props;
    return (
      <div
        className="flexbox"
        style={{
          display: 'flex',
          flexDirection: !row ? 'column' : 'row',
          // eslint-disable-next-line no-nested-ternary
          justifyContent: spaceBetween ? 'space-between' : flexStart ? 'flex-start' : 'center',
          alignItems,
          padding: padding || 0,
          flexWrap: 'wrap',
          ...containerStyle,
        }}
      >
        {children}
      </div>
    );
  }
}

export default Flexbox;
