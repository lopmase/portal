/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import { Select } from 'antd';
import PropTypes from 'prop-types';
import FormField from './FormField';
import { capitalize } from '../../helpers/common';
import FormLabel from './FormLabel';

const FormAntDropdown = props => {
  const {
    form,
    name,
    handleChange,
    items,
    onSearch,
    search,
    require,
    placeholder,
    ...rest
  } = props;
  let { label } = props;
  if (!label) {
    label = capitalize(name);
  }
  return (
    <FormField
      form={form}
      name={name}
      label={label}
      render={({ value }) => (
        <div>
          <FormLabel require={require} text={label} />
          <Select
            // items={items}
            {...{ onSearch, ...rest }}
            showSearch={search}
            style={{ width: '100%' }}
            placeholder={placeholder || `Chọn ${label}`}
            value={value}
            onChange={selectedValue => handleChange(name, selectedValue || null)}
          >
            {items.map(item => (
              <Select.Option key={item.value} value={item.value}>
                {item.text}
              </Select.Option>
            ))}
          </Select>
        </div>
      )}
    />
  );
};

FormAntDropdown.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  search: PropTypes.bool,
  require: PropTypes.bool,
  onSearch: PropTypes.func,
};

FormAntDropdown.defaultProps = {
  label: '',
  placeholder: '',
  search: false,
  require: false,
  onSearch: undefined,
};

export default FormAntDropdown;
