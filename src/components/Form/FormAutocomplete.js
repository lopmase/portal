/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import PropTypes from 'prop-types';
import FormField from './FormField';
import { capitalize } from '../../helpers/common';
import FormLabel from './FormLabel';
import Autocomplete from '../Autocomplete/Autocomplete';

const FormAutocomplete = props => {
  const { form, name, handleChange, handleSelect, items } = props;
  let { label, placeholder } = props;
  if (!label) {
    label = capitalize(name);
  }
  if (!placeholder) {
    placeholder = `Tìm ${label}`;
  }
  return (
    <FormField
      form={form}
      name={name}
      render={({ value }) => (
        <div>
          <FormLabel text={label} />
          <Autocomplete
            items={items}
            value={value}
            onSelect={handleSelect}
            placeholder={placeholder}
            onChange={newValue => handleChange(name, newValue)}
          />
        </div>
      )}
    />
  );
};

FormAutocomplete.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleSelect: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string,
};

FormAutocomplete.defaultProps = {
  label: '',
  placeholder: '',
};

export default FormAutocomplete;
