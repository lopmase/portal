import React from 'react';
import PropTypes from 'prop-types';
import FormField from './FormField';
import { capitalize } from '../../helpers/common';
import Input from '../Input/Input';

const FormAvatar = props => {
  const { form, name, handleChange } = props;
  let { label } = props;
  if (!label) {
    label = name
      .split('_')
      .map(s => capitalize(s))
      .join(' ');
  }
  return (
    <FormField
      form={form}
      name={name}
      label={label}
      render={compProps => <Input {...compProps} onChange={handleChange} />}
    />
  );
};

FormAvatar.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
};

FormAvatar.defaultProps = {
  label: null,
};

export default FormAvatar;
