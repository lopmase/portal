/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import PropTypes from 'prop-types';
import FormField from './FormField';
import { capitalize } from '../../helpers/common';
import Dropdown from '../Dropdown/Dropdown';
import FormLabel from './FormLabel';

const FormDropdown = props => {
  const {
    form,
    name,
    handleChange,
    items,
    onSearch,
    search,
    require,
    placeholder,
    ...rest
  } = props;
  let { label } = props;
  if (!label) {
    label = capitalize(name);
  }
  return (
    <FormField
      form={form}
      name={name}
      label={label}
      render={({ value }) => (
        <div>
          <FormLabel require={require} text={label} />
          <Dropdown
            items={items}
            {...{ onSearch, search, ...rest }}
            placeholder={placeholder || `Chọn ${label}`}
            selected={value}
            onChanged={({ item }) => handleChange(name, item.value)}
          />
        </div>
      )}
    />
  );
};

FormDropdown.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  search: PropTypes.bool,
  require: PropTypes.bool,
  onSearch: PropTypes.func,
};

FormDropdown.defaultProps = {
  label: '',
  placeholder: '',
  search: false,
  require: false,
  onSearch: undefined,
};

export default FormDropdown;
