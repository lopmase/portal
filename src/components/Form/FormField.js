import React from 'react';
import PropTypes from 'prop-types';
import styles from '../../styles/_Colors.scss';

const checkIfEmpty = value => {
  if (value !== undefined && value !== null) return value;
  return undefined;
};

const FormField = props => {
  const { form, name, label, render } = props;
  const { touched, error, data } = form;
  return (
    <div style={{ flex: 1 }}>
      <div style={{ width: '100%' }}>
        {render({ name, value: checkIfEmpty(data[name]), label })}
      </div>
      {touched[name] && error[name] && (
        <div style={{ color: styles.redColor, fontSize: 12, fontWeight: 'bold' }}>
          {error[name]}
        </div>
      )}
    </div>
  );
};

FormField.propTypes = {
  form: PropTypes.object.isRequired,
  render: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
};

FormField.defaultProps = {
  label: '',
};

export default FormField;
