import React from 'react';
import PropTypes from 'prop-types';
import FormField from './FormField';
import { capitalize } from '../../helpers/common';
import Input from '../Input/Input';

const FormInput = props => {
  const { style, form, name, handleChange, multiline, inputProps, maxLength, ...rest } = props;

  let { label } = props;
  if (!label) {
    label = name
      .split('_')
      .map(s => capitalize(s))
      .join(' ');
  }
  return (
    <FormField
      form={form}
      name={name}
      label={label}
      maxLength={maxLength}
      render={compProps => (
        <Input
          {...compProps}
          {...inputProps}
          {...rest}
          style={style}
          multiline={multiline}
          onChange={handleChange}
          maxLength={maxLength}
        />
      )}
    />
  );
};

FormInput.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  multiline: PropTypes.bool,
  inputProps: PropTypes.object,
  style: PropTypes.any,
  maxLength: PropTypes.string,
};

FormInput.defaultProps = {
  label: null,
  multiline: false,
  inputProps: {},
  style: '',
  maxLength: '',
};

export default FormInput;
