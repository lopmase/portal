import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { InputNumber } from 'antd';
import FormField from './FormField';
import Input from '../Input/Input';
import { capitalize } from '../../helpers/common';

const FormInputNumber = props => {
  const { form, name, handleChange, multiline, inputProps, maxLength, ...rest } = props;
  let { label } = props;
  if (!label) {
    label = name
      .split('_')
      .map(s => capitalize(s))
      .join(' ');
  }
  return (
    <>
      <FormField
        form={form}
        name={name}
        label={label}
        render={compProps => (
          <Input
            {...compProps}
            {...inputProps}
            {...rest}
            min={0}
            onChange={handleChange}
            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
            style={{ width: '100%' }}
          />
        )}
      />
    </>
  );
};

FormInputNumber.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  multiline: PropTypes.bool,
  inputProps: PropTypes.object,
};

FormInputNumber.defaultProps = {
  label: null,
  multiline: false,
  inputProps: {},
};

export default FormInputNumber;
