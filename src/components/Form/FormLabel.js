/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import PropTypes from 'prop-types';
import './FormLabel.scss';

const FormLabel = props => {
  const { text, require } = props;
  return (
    <div className="form-label">
      <label>
        {text}
        <span className="require">{require ? '*' : ''}</span>
      </label>
    </div>
  );
};

FormLabel.propTypes = {
  text: PropTypes.string.isRequired,
  require: PropTypes.bool,
};

FormLabel.defaultProps = {
  require: false,
};

export default FormLabel;
