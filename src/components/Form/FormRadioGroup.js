import React from 'react';
import PropTypes from 'prop-types';
import FormField from './FormField';
import { capitalize } from '../../helpers/common';
import Flexbox from '../Flexbox/Flexbox';
import Checkbox from '../Checkbox/Checkbox';
import FormLabel from './FormLabel';

const FormRadioGroup = props => {
  const { form, name, handleChange, options, itemStyles } = props;
  let { label } = props;
  if (!label) {
    label = name
      .split('_')
      .map(s => capitalize(s))
      .join(' ');
  }

  return (
    <FormField
      form={form}
      name={name}
      label={label}
      render={({ value }) => (
        <div>
          <FormLabel text={label} />
          <Flexbox row>
            {options.map(option => (
              <div style={{ flex: 1, ...itemStyles }} key={option.value}>
                <Checkbox
                  onChange={checked => handleChange(name, checked ? option.value : null)}
                  label={option.text}
                  checked={value === option.value}
                />
              </div>
            ))}
          </Flexbox>
        </div>
      )}
    />
  );
};

FormRadioGroup.propTypes = {
  form: PropTypes.object.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape({ value: PropTypes.string, text: PropTypes.string }))
    .isRequired,
  handleChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  itemStyles: PropTypes.object,
};

FormRadioGroup.defaultProps = {
  label: null,
  itemStyles: {},
};

export default FormRadioGroup;
