/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import PropTypes from 'prop-types';
import { Select } from 'antd';
import FormField from './FormField';
import { capitalize } from '../../helpers/common';
import FormLabel from './FormLabel';

const FormSelect = props => {
  const { form, name, handleChange, items, multiple, require } = props;
  let { label } = props;
  if (!label) {
    label = capitalize(name);
  }
  return (
    <FormField
      form={form}
      name={name}
      label={label}
      render={({ value }) => (
        <div>
          <FormLabel require={require} text={label} />
          <Select
            mode={multiple ? 'multiple' : 'single'}
            style={{ width: '100%' }}
            placeholder={`Chọn ${label}`}
            value={value || undefined}
            clearIcon
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 ||
              option.children.toUpperCase().indexOf(input.toUpperCase()) >= 0
            }
            onChange={v => handleChange(name, v)}
          >
            {Array.isArray(items) &&
              items &&
              items.length > 0 &&
              items.map(item => (
                <Select.Option value={item && item.value}>{item && item.text}</Select.Option>
              ))}
          </Select>
        </div>
      )}
    />
  );
};

FormSelect.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired,
  label: PropTypes.string,
  multiple: PropTypes.bool,
  require: PropTypes.bool,
};

FormSelect.defaultProps = {
  label: '',
  multiple: false,
  require: false,
};

export default FormSelect;
