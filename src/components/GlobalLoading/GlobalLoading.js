import React from 'react';
import PropTypes from 'prop-types';
// import { Link } from 'react-router-dom';

// import '../styles/Header.scss';

const GlobalLoading = ({ loading }) => {
  return loading && <div>Loading...</div>;
};

GlobalLoading.propTypes = {
  loading: PropTypes.bool,
};

GlobalLoading.defaultProps = {
  loading: false,
};

export default GlobalLoading;
