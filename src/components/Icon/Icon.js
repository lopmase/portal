/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import PropTypes from 'prop-types';

import './Icon.scss';

const Icon = props => {
  const { name, width, height, containerStyle, onClick } = props;
  return (
    <div
      role="button"
      tabIndex="0"
      className="icon-container"
      style={containerStyle || {}}
      onClick={onClick}
    >
      <img
        alt=""
        width={width || 40}
        height={height || 40}
        src={`${process.env.PUBLIC_URL}/icons/${name}.png`}
      />
    </div>
  );
};

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  containerStyle: PropTypes.object,
  width: PropTypes.number,
  height: PropTypes.number,
  onClick: PropTypes.func,
};

Icon.defaultProps = {
  containerStyle: undefined,
  width: 40,
  height: 40,
  onClick: () => {},
};

export default Icon;
