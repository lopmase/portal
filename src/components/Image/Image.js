import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Image extends Component {
  constructor(props) {
    super(props);

    this.state = {
      src: props.src,
      errored: false,
    };
  }

  onError = () => {
    const { errored } = this.state;
    const { fallbackSrc } = this.props;
    if (!errored) {
      this.setState({
        src: fallbackSrc,
        errored: true,
      });
    }
  };

  render() {
    const { src } = this.state;
    const { src: _1, fallbackSrc: _2, ...props } = this.props;

    return <img src={src} alt="handler" onError={this.onError} {...props} />;
  }
}

Image.propTypes = {
  src: PropTypes.string.isRequired,
  fallbackSrc: PropTypes.string.isRequired,
};

Image.defaultProps = {};

export default Image;
