/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable no-nested-ternary */
import React from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import classNames from 'classnames';

import './Input.scss';
import Icon from '../Icon/Icon';

const Input = ({
  value,
  label,
  placeholder,
  require,
  password,
  containerStyle,
  icon,
  onFocus,
  numberOnly,
  iconRight,
  iconWidth,
  suffix,
  iconHeight,
  editable,
  thousandSeparator,
  multiline,
  suggestions,
  onSuggestionClick,
  onKeyPress,
  onChange,
  name,
  onBlur,
  onValidate,
}) => {
  const handleChange = values => {
    onChange(name, values);
  };

  const empty = () => {};

  const handleBlur = event => {
    if (onBlur) {
      onBlur(event);
    }
    if (!onValidate || !name) {
      return;
    }
    if (require && !value) {
      onValidate(name, false);
    } else {
      onValidate(name, true);
    }
  };

  return (
    <div
      className={classNames('input-container', { disabled: !editable })}
      style={{ ...containerStyle }}
    >
      {label && (
        <label>
          {label}
          <span className="require">{require ? '*' : ''}</span>
        </label>
      )}
      <div style={{ position: 'relative', display: 'flex' }}>
        {icon && (
          <Icon
            width={iconWidth || 15}
            height={iconHeight || 15}
            containerStyle={{
              position: 'absolute',
              [iconRight ? 'right' : 'left']: 10,
              top: 12 - (iconHeight - 15) / 2,
            }}
            name={icon}
          />
        )}
        {suggestions && (
          <div className="suggestions">
            {suggestions.map(suggest => (
              <div onClick={() => onSuggestionClick(suggest)} className="suggestion-item">
                {suggest}
              </div>
            ))}
          </div>
        )}
        {multiline ? (
          <textarea
            disabled={!editable}
            onFocus={onFocus || empty}
            onBlur={handleBlur}
            style={{ paddingLeft: icon && !iconRight ? 30 : 10, flex: 1 }}
            type="text"
            value={value}
            placeholder={placeholder}
            onChange={event => handleChange(event.target.value)}
          />
        ) : numberOnly ? (
          <NumberFormat
            inputMode="decimal"
            suffix={suffix}
            onFocus={onFocus || empty}
            onBlur={handleBlur}
            disabled={!editable}
            type={password ? 'password' : 'text'}
            value={value}
            isNumericString
            placeholder={placeholder}
            onValueChange={values => handleChange(values.floatValue)}
            style={{ flex: 1, paddingLeft: icon && !iconRight ? 30 : 10 }}
            thousandSeparator={thousandSeparator}
          />
        ) : (
          <input
            onKeyPress={onKeyPress}
            disabled={!editable}
            onFocus={onFocus || empty}
            onBlur={handleBlur}
            style={{ paddingLeft: icon && !iconRight ? 30 : 10, flex: 1 }}
            type={password ? 'password' : 'text'}
            value={value}
            placeholder={placeholder}
            onChange={event => handleChange(event.target.value)}
          />
        )}
        {/* {password && (
          <div>
            <FontAwesomeIcon icon={faEye} />
          </div>
        )} */}
      </div>
    </div>
  );
};

Input.propTypes = {
  name: PropTypes.string,
  require: PropTypes.bool,
  onBlur: PropTypes.func,
  onValidate: PropTypes.func,
  placeholder: PropTypes.string,
  password: PropTypes.bool,
  multiline: PropTypes.bool,
  thousandSeparator: PropTypes.string,
  containerStyle: PropTypes.object,
  icon: PropTypes.string,
  onFocus: PropTypes.func,
  iconRight: PropTypes.bool,
  iconWidth: PropTypes.number,
  iconHeight: PropTypes.number,
  editable: PropTypes.bool,
  value: PropTypes.any,
  label: PropTypes.string,
  onChange: PropTypes.func,
  numberOnly: PropTypes.bool,
  suffix: PropTypes.string,
  suggestions: PropTypes.array,
  onSuggestionClick: PropTypes.func,
};

Input.defaultProps = {
  value: '',
  label: '',
  onChange: () => {},
  suffix: '',
  name: undefined,
  require: false,
  thousandSeparator: ',',
  numberOnly: false,
  onBlur: () => {},
  onValidate: () => {},
  placeholder: undefined,
  password: false,
  containerStyle: {},
  icon: undefined,
  onFocus: () => {},
  onSuggestionClick: () => {},
  iconRight: undefined,
  iconWidth: 10,
  iconHeight: 10,
  editable: true,
  multiline: false,
  suggestions: null,
};

export default Input;
