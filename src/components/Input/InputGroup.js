/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import { Input, Select } from 'antd';

const { Option } = Select;
const InputGroup = props => {
  const { form, name, handleChange, items, multiple, require, label } = props;
  return (
    <>
      {label && (
        <label>
          {label}
          <span className="require">{require ? '*' : ''}</span>
        </label>
      )}
      <Input.Group compact>
        <Input style={{ width: 100, textAlign: 'center' }} placeholder="Minimum" />
        <Input
          className="site-input-split"
          style={{
            width: 30,
            borderLeft: 0,
            borderRight: 0,
            pointerEvents: 'none',
          }}
          placeholder="~"
          disabled
        />
        <Input
          className="site-input-right"
          style={{
            width: 100,
            textAlign: 'center',
          }}
          placeholder="Maximum"
        />
      </Input.Group>
    </>
  );
};
export default InputGroup;
