import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './KeyWord.scss';

const keyWord = () => {
  return (
    <div className="div" style={{ flex: 1 }}>
      <h3>Key Word</h3>
      <textarea name="key_word" className="key_word" />
    </div>
  );
};
export default keyWord;
