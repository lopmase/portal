import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from '../../styles/_Colors.scss';

import './Label.scss';

class Label extends Component {
  static propTypes = {
    containerStyle: PropTypes.object,
    text: PropTypes.string,
    background: PropTypes.string,
    color: PropTypes.string,
    dot: PropTypes.bool,
    size: PropTypes.oneOf(['medium', 'small']),
  };

  static defaultProps = {
    containerStyle: {},
    text: '',
    size: 'medium',
    color: undefined,
    background: undefined,
    dot: false,
  };

  componentDidMount() {}

  render() {
    const { containerStyle, text, background, color, dot, size = 'medium' } = this.props;
    const textSize = size === 'small' ? 10 : 12;
    return (
      <div
        className="label"
        style={{
          display: 'inline-block',
          backgroundColor: background || styles.grayColor,
          // eslint-disable-next-line no-nested-ternary
          height: dot ? 0 : size === 'small' ? 20 : 22,
          ...containerStyle,
        }}
      >
        <div
          className="text"
          style={{ fontSize: textSize, color: color || 'white', lineHeight: `${textSize}px` }}
        >
          {text}
        </div>
      </div>
    );
  }
}

export default Label;
