/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';

export default function ImageLightbox({ images, isOpen, onClose, selectedIndex }) {
  const [photoIndex, setPhotoIndex] = useState(0);

  useEffect(() => {
    selectedIndex ? setPhotoIndex(selectedIndex) : setPhotoIndex(0);
  }, [selectedIndex]);
  return (
    <div>
      {isOpen && (
        <Lightbox
          mainSrc={images[photoIndex]}
          nextSrc={images[(photoIndex + 1) % images.length]}
          prevSrc={images[(photoIndex + images.length - 1) % images.length]}
          onCloseRequest={() => onClose()}
          onMovePrevRequest={() => setPhotoIndex((photoIndex + images.length - 1) % images.length)}
          onMoveNextRequest={() => setPhotoIndex((photoIndex + 1) % images.length)}
        />
      )}
    </div>
  );
}
