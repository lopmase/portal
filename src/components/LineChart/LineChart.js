import React from 'react';
import Chart from 'react-apexcharts';
import PropTypes from 'prop-types';
import { capitalize } from '../../helpers/common';

const LineChart = props => {
  const { colors, timeKey, keys, data, id } = props;
  const series = keys.map(key => {
    return {
      name: key
        .split('_')
        .map(s => capitalize(s))
        .join(' '),
      data: data ? data.map(d => d[key]) : [],
    };
  });
  return (
    <Chart
      options={{
        chart: {
          id: `line-chart${id}`,
          toolbar: {
            show: false,
          },
          brush: {
            enabled: false,
          },
        },
        legend: {
          show: false,
        },
        markers: {
          size: 5,
          strokeColor: colors,
          colors: colors.map(() => 'white'),
          strokeWidth: 3,
          hover: {
            sizeOffset: 2,
          },
        },
        colors,
        stroke: {
          width: 3,
        },
        xaxis: {
          categories: data ? data.map(d => d[timeKey]) : [],
          // axisBorder: {
          //   show: false,
          // },
          // labels: {
          //   show: false,
          // },
          // axisTicks: {
          //   show: false,
          // },
        },
        // yaxis: {
        //   axisBorder: {
        //     show: false,
        //   },
        //   labels: {
        //     show: false,
        //   },
        //   axisTicks: {
        //     show: false,
        //   },
        // },
      }}
      series={series}
      type="line"
      height={300}
    />
  );
};

LineChart.propTypes = {
  // dot: PropTypes.number,
  timeKey: PropTypes.string,
  id: PropTypes.string.isRequired,
  keys: PropTypes.array,
  colors: PropTypes.array,
  data: PropTypes.arrayOf({}).isRequired,
};

LineChart.defaultProps = {
  // dot: 5,
  timeKey: 'name',
  keys: ['uv', 'pv'],
  colors: ['#0a6ef3', '#65ce00'],
};

export default LineChart;
