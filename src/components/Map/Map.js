import React from 'react';
import GoogleMapReact from 'google-map-react';
import PropTypes from 'prop-types';
import Marker from '../Marker/Marker';

const Map = ({ marker, center, onMapClick, zoom, onAPILoaded }) => {
  const loadMapAPI = ({ maps }) => {
    onAPILoaded(maps);
  };

  return (
    <div style={{ height: 500, width: '100%' }}>
      <GoogleMapReact
        onClick={onMapClick}
        options={{
          mapTypeControl: true,
        }}
        zoom={16}
        center={marker}
        bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_API, libraries: ['places'] }}
        defaultCenter={center}
        defaultZoom={zoom}
        yesIWantToUseGoogleMapApiInternals
        onGoogleApiLoaded={loadMapAPI}
      >
        {marker && marker.lat !== 0 && <Marker lat={marker.lat} lng={marker.lng} />}
      </GoogleMapReact>
    </div>
  );
};

Map.propTypes = {
  center: PropTypes.shape({
    lat: PropTypes.number,
    lng: PropTypes.number,
  }),
  zoom: PropTypes.number,
  onAPILoaded: PropTypes.func,
  marker: PropTypes.shape({
    lat: PropTypes.number,
    lng: PropTypes.number,
  }),
  onMapClick: PropTypes.func,
};

Map.defaultProps = {
  center: {
    lat: 59.95,
    lng: 30.33,
  },
  zoom: 11,
  onAPILoaded: () => {},
  marker: {
    lat: 59.95,
    lng: 30.33,
  },
  onMapClick: () => {},
};

export default Map;
