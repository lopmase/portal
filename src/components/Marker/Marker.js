import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Marker.scss';

class Marker extends Component {
  static propTypes = {
    // text: PropTypes.string,
    containerStyle: PropTypes.object,
  };

  static defaultProps = {
    // text: undefined,
    containerStyle: {},
  };

  componentDidMount() {}

  render() {
    const { containerStyle } = this.props;
    return (
      <div className="marker" style={{ ...containerStyle }}>
        <div className="inner" />
      </div>
    );
  }
}

export default Marker;
