import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import './Modal.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner, faTimes } from '@fortawesome/free-solid-svg-icons';

const Modal = ({
  containerStyle,
  closeOnBackdrop,
  renderModal,
  visible,
  onClose,
  loading,
  closeBtn,
  classModal,
}) => {
  let modalRef = null;

  const onBackgroundClick = event => {
    if (event.target === modalRef && !loading && closeOnBackdrop) {
      onClose();
    }
  };
  return (
    <div
      className={classNames(
        'modal-container',
        {
          visible,
        },
        classModal,
      )}
      ref={ele => {
        modalRef = ele;
      }}
      onClick={onBackgroundClick}
    >
      <div
        className={classNames('modal', {
          visible,
          loading,
        })}
        style={{ ...containerStyle }}
      >
        {closeBtn && (
          <div className="close-btn" onClick={onClose}>
            <FontAwesomeIcon icon={faTimes} />
          </div>
        )}
        {loading && (
          <div className="loading">
            <FontAwesomeIcon icon={faSpinner} size="3x" spin color="white" />
          </div>
        )}
        {renderModal ? renderModal() : <div>Here</div>}
      </div>
    </div>
  );
};

Modal.propTypes = {
  visible: PropTypes.bool.isRequired,
  loading: PropTypes.bool,
  closeOnBackdrop: PropTypes.bool,
  containerStyle: PropTypes.object,
  classModal: PropTypes.string,
  renderModal: PropTypes.func,
  onClose: PropTypes.func,
  closeBtn: PropTypes.bool,
};

Modal.defaultProps = {
  containerStyle: {},
  classModal: '',
  renderModal: () => {},
  closeOnBackdrop: true,
  loading: false,
  closeBtn: false,
  onClose: () => {},
};

export default Modal;
