import React from 'react';
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';
import './Paginator.scss';
import Flexbox from '../Flexbox/Flexbox';
import Button from '../Button/Button';

const Paginator = ({ containerStyle, onNext, onPrev, pageNumbers, currentPage }) => {
  return (
    <div className="pagination" style={{ ...containerStyle }}>
      <Flexbox row>
        <Button
          containerStyle={{
            marginRight: 10,
            paddingRight: 10,
            paddingLeft: 10,
            paddingTop: 6,
            paddingBottom: 6,
          }}
          onClick={onPrev}
          icon={faChevronLeft}
          disabled={currentPage === 1}
          iconColor="gray"
          iconOnly
          backgroundColor="white"
        />
        <div className="text">{`${currentPage} / ${pageNumbers}`}</div>
        <Button
          containerStyle={{
            marginLeft: 10,
            paddingRight: 10,
            paddingLeft: 10,
            paddingTop: 6,
            paddingBottom: 6,
          }}
          icon={faChevronRight}
          onClick={onNext}
          disabled={currentPage === pageNumbers}
          iconColor="gray"
          iconOnly
          backgroundColor="white"
        />
      </Flexbox>
    </div>
  );
};

Paginator.propTypes = {
  containerStyle: PropTypes.object,
  onNext: PropTypes.func,
  pageNumbers: PropTypes.number,
  currentPage: PropTypes.number,
  onPrev: PropTypes.func,
};

Paginator.defaultProps = {
  containerStyle: {},
  onNext: () => {},
  pageNumbers: 1,
  currentPage: 1,
  onPrev: () => {},
};

export default Paginator;
