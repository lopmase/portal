import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { bindActionCreators } from 'redux';
// import Flexbox from './Flexbox';
import './Popover.scss';
// import moment from 'moment';

// TODO: refactor to pure comp
class Popover extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    containerStyle: PropTypes.object,
    renderPopover: PropTypes.func,
  };

  static defaultProps = {
    containerStyle: {},
    renderPopover: () => {},
  };

  state = {
    visible: false,
  };

  componentDidMount() {}

  toggle = () => {
    const { visible } = this.state;
    this.setState({ visible: !visible });
  };

  render() {
    const { children, containerStyle, renderPopover } = this.props;
    const { visible } = this.state;
    return (
      <div className="popover-container" style={{ ...containerStyle }}>
        <div
          onKeyPress={this.onKeyPressHandler}
          role="button"
          tabIndex="0"
          className="content"
          onClick={this.toggle}
        >
          {children}
        </div>
        <div
          className={classNames('popover', {
            visible,
          })}
        >
          {renderPopover ? renderPopover() : <div>Here We Are</div>}
        </div>
      </div>
    );
  }
}

export function mapStateToProps() {
  return {};
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Popover);
