import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route } from 'react-router-dom';
import { isAuthenticated } from '../../apis/authApi';
import { ROUTES } from '../../constants/ROUTES';

const PrivateRoute = ({ component: Component, className, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated() ? (
          <Component {...props} className={className} />
        ) : (
          <Redirect
            to={{
              pathname: ROUTES.LOGIN,
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};

PrivateRoute.propTypes = {
  component: PropTypes.oneOfType([PropTypes.node, PropTypes.element, PropTypes.func]).isRequired,
  className: PropTypes.string,
  location: PropTypes.object,
};

PrivateRoute.defaultProps = {
  className: '',
  location: undefined,
};

export default PrivateRoute;
