/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import './Slider.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronCircleRight, faChevronCircleLeft } from '@fortawesome/free-solid-svg-icons';

const Slider = ({ images, height, id }) => {
  const [index, setIndex] = useState(0);
  useEffect(() => {
    setIndex(0);
  }, [id]);
  return (
    <div className="slider" style={{ height }}>
      {images.map((image, i) => (
        <img src={image} alt="slider" className={classNames('image', { active: index === i })} />
      ))}
      <div className="next" onClick={() => setIndex(index < images.length - 1 ? index + 1 : index)}>
        <FontAwesomeIcon icon={faChevronCircleRight} size="2x" color="whitesmoke" />
      </div>
      <div className="back" onClick={() => setIndex(index > 0 ? index - 1 : index)}>
        <FontAwesomeIcon icon={faChevronCircleLeft} size="2x" color="whitesmoke" />
      </div>
    </div>
  );
};

Slider.propTypes = {
  images: PropTypes.array,
  height: PropTypes.number,
  id: PropTypes.any.isRequired,
};

Slider.defaultProps = {
  images: [],
  height: 300,
};

export default Slider;
