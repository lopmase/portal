import React from 'react';
import PropTypes from 'prop-types';

import Reponsive from '../Reponsive/Reponsive';

const Spacer = ({ width, height }) => {
  return (
    <>
      <Reponsive.Default>
        <div style={{ width, height }} />
      </Reponsive.Default>
      <Reponsive.Mobile>
        <div style={{ width, height }} />
      </Reponsive.Mobile>
    </>
  );
};

Spacer.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
};

Spacer.defaultProps = {
  width: 10,
  height: 10,
};

export default Spacer;
