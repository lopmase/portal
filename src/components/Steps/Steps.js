import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './Steps.scss';
import Flexbox from '../Flexbox/Flexbox';
import Button from '../Button/Button';
import Reponsive from '../Reponsive/Reponsive';

const Steps = props => {
  const [currentStep, setStep] = useState(0);
  const { steps, containerStyle } = props;
  return (
    <div className="steps" style={{ ...containerStyle }}>
      <Reponsive.Default>
        <Flexbox row spaceBetween>
          {steps.map((step, index) => (
            <div
              className={classNames('header', {
                active: index === currentStep,
              })}
              key={`step-${step.key}`}
            >
              <div className="circle">{index + 1}</div>
              <div className="label">{step.label}</div>
            </div>
          ))}
        </Flexbox>
      </Reponsive.Default>
      {steps.map((step, index) => (
        <div>
          {index === currentStep && (
            <div
              className={classNames('body', {
                visible: index === currentStep,
              })}
              key={step.key}
            >
              <Reponsive.Default>
                <div className="title">{step.title}</div>
              </Reponsive.Default>
              <Reponsive.Mobile>
                <div className="title mobile">{step.title}</div>
              </Reponsive.Mobile>
              <div>{step.render()}</div>
            </div>
          )}
        </div>
      ))}
      <div>
        <Flexbox row spaceBetween containerStyle={{}}>
          <Button
            disabled={currentStep === 0}
            clear
            text="Quay lại"
            onClick={() => setStep(currentStep - 1)}
          />
          <Button
            disabled={currentStep === steps.length - 1}
            text="Tiếp"
            onClick={() => setStep(currentStep + 1)}
          />
        </Flexbox>
      </div>
    </div>
  );
};

Steps.propTypes = {
  steps: PropTypes.array.isRequired,
  containerStyle: PropTypes.object,
};

Steps.defaultProps = {
  containerStyle: {},
};

export default Steps;
