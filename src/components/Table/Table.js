/* eslint-disable react/jsx-indent */
/* eslint-disable react/no-array-index-key */
import React, { Component } from 'react';
import classNames from 'classnames';
import ReactPlaceholder from 'react-placeholder';
import PropTypes from 'prop-types';
import './Table.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSortDown, faSortUp } from '@fortawesome/free-solid-svg-icons';

const loadingData = [{}, {}, {}];

const LoadingRow = ({ columns }) => {
  return (
    <tr>
      {columns.map(col => (
        <td key={col.key}>
          <ReactPlaceholder
            showLoadingAnimation
            type="text"
            ready={false}
            color="#E0E0E0"
            style={{ maxWidth: 100 }}
          >
            <div />
          </ReactPlaceholder>
        </td>
      ))}
    </tr>
  );
};

LoadingRow.propTypes = {
  columns: PropTypes.array.isRequired,
};

class Table extends Component {
  static propTypes = {
    columns: PropTypes.array.isRequired,
    data: PropTypes.array.isRequired,
    renderCell: PropTypes.func,
    rowClick: PropTypes.func,
    rowDoubleClick: PropTypes.func,
    onHeaderClick: PropTypes.func,
    loading: PropTypes.bool,
    selectable: PropTypes.bool,
    sortColumn: PropTypes.string,
    sortOrder: PropTypes.string,
  };

  static defaultProps = {
    renderCell: undefined,
    selectable: false,
    loading: false,
    rowClick: () => {},
    rowDoubleClick: () => {},
    onHeaderClick: () => {},
    sortColumn: '',
    sortOrder: 'asc',
  };

  state = {
    selectedIndex: -1,
  };

  componentDidMount() {}

  onKeyPressHandler = () => {};

  check = () => {};

  onRowSelect = (index, item) => {
    const { rowClick } = this.props;
    const { selectedIndex } = this.state;
    let newIndex = index;
    if (index === selectedIndex) {
      newIndex = -1;
    }
    this.setState({ selectedIndex: newIndex });
    rowClick({ index: newIndex, rowData: item });
  };

  onRowDoubleClickSelect = (index, item) => {
    const { rowDoubleClick } = this.props;
    const { selectedIndex } = this.state;
    let newIndex = index;
    if (index === selectedIndex) {
      newIndex = -1;
    }
    this.setState({ selectedIndex: newIndex });
    rowDoubleClick({ index: newIndex, rowData: item });
  };

  render() {
    const {
      columns,
      data,
      renderCell,
      selectable,
      loading,
      onHeaderClick,
      sortColumn,
      sortOrder,
    } = this.props;
    const { selectedIndex } = this.state;
    return (
      <div className="table-container">
        <table>
          <thead>
            <tr className="header">
              {columns.map((col, index) => (
                <th onClick={() => onHeaderClick(col, index)} key={`th_${index}`}>
                  <span>{col.header}</span>
                  {sortColumn === col.key && (
                    <FontAwesomeIcon
                      style={{ marginLeft: 5 }}
                      icon={sortOrder === 'asc' ? faSortDown : faSortUp}
                    />
                  )}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {loading
              ? loadingData.map((item, index) => (
                  <LoadingRow key={`loading_row_${index}`} item={item} columns={columns} />
                ))
              : data.map((item, ind) => (
                  <tr
                    key={`tr_${ind}`}
                    className={classNames('row', {
                      selected: selectable && ind === selectedIndex,
                    })}
                    onClick={() => this.onRowSelect(ind, item)}
                    onDoubleClick={() => this.onRowDoubleClickSelect(ind, item)}
                    onKeyPress={this.onKeyPressHandler}
                    role="button"
                    tabIndex="0"
                  >
                    {columns.map((col, colIndex) => {
                      if (renderCell) {
                        return renderCell({ item, column: col });
                      }
                      return <td key={`td_${item.id}_${colIndex}`}>{item[col.key].toString()}</td>;
                    })}
                  </tr>
                ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Table;
