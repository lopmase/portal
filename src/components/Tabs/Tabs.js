/* eslint-disable react/require-default-props */
import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './Tabs.scss';

const TabRouter = ({ tabs, className }) => {
  return (
    <div className={`tabs ${className}`}>
      {tabs.map(tab => (
        <Link key={tab.route} to={tab.route}>
          <div
            className={classNames('item', {
              active: window.location.pathname === tab.route,
            })}
          >
            {tab.label}
          </div>
        </Link>
      ))}
    </div>
  );
};

TabRouter.propTypes = {
  tabs: PropTypes.array.isRequired,
  className: PropTypes.string,
  // tabSelect: PropTypes.func,
};

TabRouter.defaultProps = {
  // tabSelect: () => {},
};

export default TabRouter;
