/* eslint-disable react/jsx-indent */
/* eslint-disable no-nested-ternary */
import React from 'react';
import PropTypes from 'prop-types';

import './Text.scss';
import ReactPlaceholder from 'react-placeholder';

const Text = props => {
  const renderElement = (type, text) => {
    const value = text && text.toString ? text.toString() : text;
    switch (type) {
      case 'h1':
        return <h1>{value}</h1>;
      case 'h2':
        return <h2>{value}</h2>;
      case 'h3':
        return <h3>{value}</h3>;
      case 'p':
        return <p>{value}</p>;
      default:
        return <div>{value}</div>;
    }
  };

  const { rows, containerStyle, loaderStyle, loading, type, text, width } = props;
  const isH1 = type === 'h1';
  const isH2 = type === 'h2';
  return (
    <div className="text-box" style={{ ...containerStyle }}>
      <ReactPlaceholder
        showLoadingAnimation
        type="text"
        ready={!loading}
        style={{ width, marginBottom: 10, fontSize: isH1 ? 30 : isH2 ? 25 : 18, ...loaderStyle }}
        rows={rows}
        color="#E0E0E0"
      >
        <div>{renderElement(type, text)}</div>
      </ReactPlaceholder>
    </div>
  );
};

Text.propTypes = {
  type: PropTypes.string,
  text: PropTypes.string.isRequired,
  containerStyle: PropTypes.object,
  loaderStyle: PropTypes.object,
  loading: PropTypes.bool,
  rows: PropTypes.number,
  width: PropTypes.number,
};

Text.defaultProps = {
  type: 'div',
  containerStyle: {},
  loaderStyle: {},
  rows: 1,
  width: 100,
  loading: false,
};

export default Text;
