import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import TimeKeeper from 'react-timekeeper';

import './TimePicker.scss';

class TimePicker extends Component {
  static propTypes = {
    onTimeChanged: PropTypes.func,
    containerStyle: PropTypes.object,
    time: PropTypes.any,
    visible: PropTypes.bool,
  };

  static defaultProps = {
    visible: true,
    onTimeChanged: undefined,
    time: undefined,
    containerStyle: {},
  };

  componentDidMount() {}

  render() {
    const { containerStyle, visible, onTimeChanged, time } = this.props;
    return (
      <div
        className={classNames('time-picker', {
          visible,
        })}
        style={{ ...containerStyle }}
      >
        <TimeKeeper switchToMinuteOnHourSelect time={time} onDoneClick={onTimeChanged} />
      </div>
    );
  }
}

export default TimePicker;
