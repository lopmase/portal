/* eslint-disable no-param-reassign */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable no-nested-ternary */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { faTimes, faImage, faUndo } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Uploader.scss';
import Button from '../Button/Button';
import ImageLightbox from '../LightBox/Lightbox';

const Uploader = props => {
  const [loading, setLoading] = useState(false);
  const [imageIndex, setImageIndex] = useState(0);
  const [lightboxVisible, setLightboxVisible] = useState(false);
  const [rotate, setRotate] = useState({});
  const {
    single,
    id,
    images,
    onImageAdded,
    onImageRemoved,
    onImageRotated,
    containerStyle,
    copyUrl,
    rotateVisible,
  } = props;
  console.log('single', single);
  useEffect(() => {
    console.log('images', images);
  }, [images]);
  const readImages = files => {
    images.newName = '';
    setLoading(true);
    const imageJobs = files.map(
      file =>
        new Promise(resolve => {
          if (file.size > 5000000) {
            resolve({
              error: 'Over limited',
              file,
            });
            return;
          }
          const reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = () => {
            resolve({ data: reader.result, file });
          };
          reader.onerror = error => {
            resolve({ error, file });
          };
        }),
    );
    Promise.all(imageJobs)
      .then(results => {
        const uploadedImages = results.filter(r => !r.error);
        setLoading(false);
        onImageAdded(uploadedImages);
      })
      .catch(() => {
        setLoading(false);
      });
  };

  const createArray = files => {
    let array = [];
    if (!files.length) {
      return [];
    }
    if (single) {
      array = [files.item(0)];
    } else {
      // eslint-disable-next-line no-plusplus
      for (let index = 0; index < files.length; index++) {
        array.push(files.item(index));
      }
    }
    return array;
  };

  const onImageChange = event => {
    const files = createArray(event.target.files);
    readImages(files);
  };

  const copyImageToClipboard = image => {
    const el = document.createElement('textarea');
    el.value = image.url || image.data;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
  };

  return (
    <div className="uploader" style={containerStyle}>
      {images.length > 0 &&
        images.map((image, index) => (
          <div
            className={classNames('photo', {
              [`degree${rotate[index]}`]: rotate[index],
            })}
          >
            <img
              alt="p"
              onClick={() => {
                setImageIndex(index);
                setLightboxVisible(true);
              }}
              src={image.data || image.url}
            />
            {copyUrl && (
              <div className="copy">
                <Button text="Copy URL" onClick={() => copyImageToClipboard(image)} />
              </div>
            )}
            {rotateVisible && (
              <div className="rotate">
                <Button
                  icon={faUndo}
                  iconOnly
                  onClick={() => {
                    const currentRotation = rotate[index];
                    const newRotation = currentRotation
                      ? currentRotation === 270
                        ? 0
                        : currentRotation + 90
                      : 90;
                    setRotate({
                      ...rotate,
                      [index]: newRotation,
                    });
                    onImageRotated(index, newRotation);
                  }}
                />
              </div>
            )}
            <div className="delete" onClick={() => onImageRemoved(index)}>
              <FontAwesomeIcon icon={faTimes} color="white" />
            </div>
          </div>
        ))}
      <label htmlFor={`multi${id}`}>
        <FontAwesomeIcon icon={faImage} size="2x" style={{ marginTop: 20, color: '#b3b3b3' }} />
        <div className="text">Chọn</div>
      </label>
      {single ? (
        <input
          type="file"
          accept="image/*"
          id={`multi${id}`}
          onChange={event => onImageChange(event)}
        />
      ) : (
        <input
          type="file"
          accept="image/*"
          id={`multi${id}`}
          onChange={event => onImageChange(event)}
          multiple
        />
      )}
      {loading && <div />}
      <ImageLightbox
        selectedIndex={imageIndex}
        isOpen={lightboxVisible}
        onClose={() => setLightboxVisible(false)}
        images={images.map(img => (img.main ? img.main : img.data))}
      />
    </div>
  );
};

Uploader.propTypes = {
  onImageAdded: PropTypes.func,
  onImageRotated: PropTypes.func,
  onImageRemoved: PropTypes.func,
  images: PropTypes.array.isRequired,
  single: PropTypes.bool,
  rotateVisible: PropTypes.bool,
  id: PropTypes.string.isRequired,
  containerStyle: PropTypes.object,
  copyUrl: PropTypes.bool,
};

Uploader.defaultProps = {
  onImageAdded: () => {},
  onImageRotated: () => {},
  onImageRemoved: () => {},
  single: false,
  rotateVisible: false,
  copyUrl: false,
  containerStyle: undefined,
};

export default Uploader;
