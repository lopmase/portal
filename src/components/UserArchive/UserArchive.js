/* eslint-disable react/prop-types */
import React from 'react';
import { Select } from 'antd';

const { Option, OptGroup } = Select;

const UserArchive = ({ users, onChange, mode, className }) => {
  return (
    <>
      {users && (
        <Select
          mode={mode}
          className={className}
          onChange={onChange}
          placeholder="Personal"
          maxTagCount="responsive"
          showSearch
          filterOption={(input, option) => {
            return (
              (option.name && option.name.toUpperCase().indexOf(input.toLowerCase()) >= 0) ||
              (option.name && option.name.toLowerCase().indexOf(input.toLowerCase()) >= 0)
            );
          }}
        >
          <Option key={0}>Myself</Option>
          {users.admin.length > 0 && (
            <OptGroup label="Admin">
              {users.admin.map(user => (
                <Option key={user.id} name={user.name}>
                  {user.name}
                </Option>
              ))}
              ;
            </OptGroup>
          )}
          {users.saleAdmin.length > 0 && (
            <OptGroup label="Sale Manager">
              {users.saleAdmin.map(user => (
                <Option key={user.id} name={user.name}>
                  {user.name}
                </Option>
              ))}
              ;
            </OptGroup>
          )}
          {users.seniorManager.length > 0 && (
            <OptGroup label="Senior Manager">
              {users.seniorManager.map(user => (
                <Option key={user.id} name={user.name}>
                  {user.name}
                </Option>
              ))}
              ;
            </OptGroup>
          )}
          {users.manager.length > 0 && (
            <OptGroup label="Manager">
              {users.manager.map(user => (
                <Option key={user.id} name={user.name}>
                  {user.name}
                </Option>
              ))}
              ;
            </OptGroup>
          )}
          {users.teamLeader.length > 0 && (
            <OptGroup label="Team Leader">
              {users.teamLeader.map(user => (
                <Option key={user.id} name={user.name}>
                  {user.name}
                </Option>
              ))}
            </OptGroup>
          )}
          {users.staffs.length > 0 && (
            <OptGroup label="Staff">
              {users.staffs.map(user => (
                <Option key={user.id} name={user.name}>
                  {user.name}
                </Option>
              ))}
            </OptGroup>
          )}
        </Select>
      )}
    </>
  );
};

export default UserArchive;
