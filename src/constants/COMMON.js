/* eslint-disable consistent-return */
import _ from 'lodash';
import { formatMoney } from '../helpers/common';
import moment from 'moment';

export const REDUCERS = {
  ANOTHER_ACCOUNT: 'ANOTHER_ACCOUNT',
  ALONHADAT: 'ALONHADAT_INFO',
  POST_STATE: 'POST_STATE',
  POST_DETAIL_DATA: 'POST_DETAIL_DATA',
  LOGIN: 'LOGIN',
  AWAITING: 'AWAITING',
  UPCOMING: 'UPCOMING',
  COMPLETED: 'COMPLETED',
  AUCTION_DETAIL: 'AUCTION_DETAIL',
  LOTS: 'LOTS',
  NEW_LOT: 'NEW_LOT',
  NEW_HOUSE: 'NEW_HOUSE',
  NEW_POST: 'NEW_POST',
  DASHBOARD: 'DASHBOARD',
  DASHBOARD_CHART: 'DASHBOARD_CHART',
  DASHBOARD_NUMBERS: 'DASHBOARD_NUMBERS',
  ANALYTICS: 'ANALYTICS',
  ANALYTICS_AUCTIONS: 'ANALYTICS_AUCTIONS',
  ANALYTICS_OVERALL: 'ANALYTICS_OVERALL',
  MAKE: 'MAKE',
  MODEL: 'MODEL',
  SUBCATEGORY: 'SUBCATEGORY',
  UPLOAD_JOBS: 'UPLOAD_JOBS',
  NEW_USER_MODAL: 'NEW_USER_MODAL',
  NEW_USER: 'NEW_USER',
  OPTIONS: 'OPTIONS',
  COMMENTS: 'COMMENTS',
  AUCTION_TYPE: 'AUCTION_TYPE',
  GLOBAL_ERROR_MODAL: 'GLOBAL_ERROR_MODAL',
  HOUSES: 'HOUSES',
  HOUSES_GROUPING: 'HOUSES_GROUPING',
  USER_PROFILE: 'USER_PROFILE',
  USER_PROFILE_MODAL: 'USER_PROFILE_MODAL',
  NOTIFICATION: 'NOTIFICATION',
  NOTI_POPUP: 'NOTI_POPUP',
  CUSTOMERS: 'CUSTOMERS',
  NEW_CUSTOMER: 'NEW_CUSTOMER',
  EDIT_CUSTOMER: 'EDIT_CUSTOMER',
  QUICK_NEW_CUSTOMER_MODAL: 'QUICK_NEW_CUSTOMER_MODAL',
  QUICK_NEW_HOUSE_MODAL: 'QUICK_NEW_HOUSE_MODAL',
  SHARE_HOUSE_ZALO_MODAL: 'SHARE_HOUSE_ZALO_MODAL',
  NEW_CUSTOMER_MODAL: 'NEW_CUSTOMER_MODAL',
  EDIT_CUSTOMER_MODAL: 'EDIT_CUSTOMER_MODAL',
  STREETS: 'STREETS',
  USERS_FILTER: 'USERS_FILTER',
  USERS: 'USERS',
  EDIT_USER_MODAL: 'EDIT_USER_MODAL',
  EDIT_USER: 'EDIT_USER',
  GLOBAL_LOADING: 'GLOBAL_LOADING',
  CONFIRM_DIALOG: 'CONFIRM_DIALOG',
  PROJECTS: 'PROJECTS',
  PROJECT_DETAIL: 'PROJECT_DETAIL',
  NEW_PROJECT: 'NEW_PROJECT',
  NEW_PROJECT_MODAL: 'NEW_PROJECT_MODAL',
  EDIT_PROJECT_MODAL: 'EDIT_PROJECT_MODAL',
  HOUSE_DETAIL: 'HOUSE_DETAIL',
  HOUSE_DETAIL_DATA: 'HOUSE_DETAIL_DATA',
  HOUSE_FILTER: 'HOUSE_FILTER',
  BLOGS: 'BLOGS',
  NEW_BLOG: 'NEW_BLOG',
  BLOG_DETAIL: 'BLOG_DETAIL',
  ZALO_TOKEN: 'ZALO_TOKEN',
  REVIEWS: 'REVIEWS',
  REVIEWS_PUBLIC: 'REVIEWS_PUBLIC',
  CHANGE_PASSWORD_MODAL: 'CHANGE_PASSWORD_MODAL',
  CHANGE_PASSWORD: 'CHANGE_PASSWORD',
  REQUESTS: 'REQUESTS',
  NEW_REQUEST: 'NEW_REQUEST',
  TOP_VIEWED_HOUSES: 'TOP_VIEWED_HOUSES',
  REQUEST_DETAIL: 'REQUEST_DETAIL',
  REQUEST_GROUPING: 'REQUEST_GROUPING',
  NEW_REQUEST_MODAL: 'NEW_REQUEST_MODAL',
  EDIT_REQUEST_MODAL: 'EDIT_REQUEST_MODAL',
  CUSTOMER_DETAIL_DATA: 'CUSTOMER_DETAIL_DATA',
  FULL_LIST_NOTIFICATION: 'FULL_LIST_NOTIFICATION',
  TABLE_SETTING: 'TABLE_SETTING',
  TRACKING: 'TRACKING',
  CREATE_TRANSACTION: 'CREATE_TRANSACTION',
  TRANSACTION: 'TRANSACTION',
  CREATE_STATISTIC: 'CREATE_STATISTIC',
  GET_USER: 'GET_USER',
  GET_TAG: 'GET_TAG',
  PROJECT_INFO: 'PROJECT_INFO',
  // to do :
  QUESTION: 'QUESTION',
  TYPE: 'TYPE',
  TYPE_CREATE: 'TYPE_CREATE',
  KNOWLEDGE: 'KNOWLEDGE',
  KNOWLEDGE_DETAIL: 'KNOWLEDGE_DETAIL',
  EDIT_KNOWLEDGE: 'EDIT_KNOWLEDGE',
  TYPE_VISIBLE: 'TYPE_VISIBLE',
  KNOWLEDGE_LIST: 'KNOWLEDGE_LIST',
  RESET_DATA_KNOWLEDGE: 'RESET_DATA_KNOWLEDGE',
  KNOWLEDGE_NOT_APPROVE: 'KNOWLEDGE_NOT_APPROVE',
  KNOWLEDGE_EDIT_FROM_NOT_APPROVE: 'KNOWLEDGE_EDIT_FROM_NOT_APPROVE',
  FETCH_WITH_PAGE: 'FETCH_WITH_PAGE',
  LIST_READY_UPDATE: 'LIST_READY_UPDATE',
  RESET_LIST_UPDATE: 'RESET_LIST_UPDATE'

};

export const ROLE_NAMES = {
  1: 'Super Admin',
  2: 'Admin',
  3: 'Staff',
  5: 'Marketer',
};
export const JOB_POSITION = {
  1: 'Super Admin',
  2: 'Admin',
  3: 'Sale Admin',
  4: 'Senior Manager', // trưởng phòng kinh doanh
  5: 'Manager', // phó phòng kinh doanh
  6: 'Team Leader', // tổ trưởng
  7: 'Staff', // nhân viên kinh doanh
  8: 'Accountant',
};
export const POSITION_NAME = {
  1: 'Giám đốc',
  2: 'Trưởng Phòng',
  3: 'Nhân viên',
};

export const CUSTOMER_TYPES = {
  1: 'Nhà đầu tư',
  2: 'Khách đã mua',
  3: 'Khách tiềm năng',
  4: 'không mua nữa',
};

export const NOTIFICATION_TYPES = {
  1: ' đã tạo bất động sản mới',
  2: ' đã chia sẻ bất động sản lên cộng đồng',
  3: ' đã tạo bất động sản mới',
  4: ' đã bình luận BĐS của bạn',
};

export const DICTIONARY_TYPE = {
  house_type: 'Loại BĐS',
  house_class: 'Loại Nhà',
  street_type: 'Loại Đường',
  customer: 'Khách hàng',
  ownership: 'Pháp lý',
  direction: 'Hướng',
  province: 'Quận/Huyện',
  district: 'Phường/Xã',
  street: 'Đường',
};
export const select = [
  { key: 1, value: 1, label: 'Năm' },
  { key: 2, value: 2, label: 'Quý' },
  { key: 3, value: 3, label: 'Tháng' },
];
export const selectMonth = [
  { key: 1, label: 'Tháng 1' },
  { key: 2, label: 'Tháng 2' },
  { key: 3, label: 'Tháng 3' },
  { key: 4, label: 'Tháng 4' },
  { key: 5, label: 'Tháng 5' },
  { key: 6, label: 'Tháng 6' },
  { key: 7, label: 'Tháng 7' },
  { key: 8, label: 'Tháng 8' },
  { key: 9, label: 'Tháng 9' },
  { key: 10, label: 'Tháng 10' },
  { key: 11, label: 'Tháng 11' },
  { key: 12, label: 'Tháng 12' },
];
export const transactionType = [
  { 1: 'Khách hàng' },
  { 2: 'Chủ' },
  { 3: 'Kết nối' },
  { 4: 'Chính khách - Chính chủ' },
];
export const selectSeason = [
  { key: 1, label: 'Quý 1' },
  { key: 2, label: 'Quý 2' },
  { key: 3, label: 'Quý 3' },
  { key: 4, label: 'Quý 4' },
];
export const OverviewColumns = [
  {
    title: 'Tên',
    dataIndex: 'name',
    key: 'name',
    align: 'left',
  },
  {
    title: 'ĐVT',
    dataIndex: 'unit',
    key: 'unit',
    align: 'center',
  },
  {
    title: 'Kỳ trước',
    dataIndex: 'preSeason',
    key: 'preSeason',
    align: 'center',
  },
  {
    title: 'Kỳ này',
    dataIndex: 'currentSeason',
    key: 'currentSeason',
    align: 'center',
  },
  {
    title: 'Tiêu chuẩn',
    dataIndex: 'standard',
    key: 'standard',
    align: 'center',
  },
];
export const accumulationField = [
  {
    label: 'Lượng sản phẩm đã được tôi marketing hôm nay là',
    name: 'customerPhoneQuantity',
    type: 'Input',
    rules: [
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
      {
        required: true,
        message: 'Vui lòng nhập Số sản phẩm maketing hôm nay',
      },
    ],
  },
  {
    label: 'Số lượng khách hàng xem mới',
    name: 'customerSeen',
    type: 'Input',
    rules: [
      {
        pattern: new RegExp(/^[-]?[0-9]+$/),
        message: 'Phải là số',
      },
      {
        required: true,
        message: 'Vui lòng nhập Số lượng khách hàng xem mới',
      },
    ],
  },
  {
    label: 'Số lượng khách hàng tương tác',
    name: 'customerInteraction', // số lượng khách hàng tương tác
    type: 'Input',
    rules: [
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
      {
        required: true,
        message: 'Vui lòng nhập Số lượng khách hàng tương tác',
      },
    ],
  },
  {
    label: 'Số lượng khách hàng xem 2 lần trở lên',
    name: 'customerSeen2nd',
    type: 'Input',
    rules: [
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
      {
        required: true,
        message: 'Vui lòng nhập Số lượng khách hàng xem 2 lần trở lên',
      },
    ],
  },
  {
    label: 'Số lượng sản phẩm mới',
    name: 'newProductQuantity',
    type: 'Input',
    rules: [
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
      {
        required: true,
        message: 'Vui lòng nhập Số lượng sản phẩm mới',
      },
    ],
  },
  {
    label: 'Số lượng sản phẩm kết nối dịch vụ',
    name: 'productConnection',
    type: 'Input',
    rules: [
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
      {
        required: true,
        message: 'Vui lòng nhập Số lượng sản phẩm kết nối',
      },
    ],
  },
];
export const saleAccumulationField = [
  {
    label: 'Số lượng nhu cầu bán mới',
    name: 'saleQuantity',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Số lượng nhu cầu bán mới',
      },
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
    ],
  },
  {
    label: 'Số khách hàng tư vấn',
    name: 'saleAdviseCustomer',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Số khách hàng tư vấn',
      },
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
    ],
  },
  {
    label: 'Nhà bán có khách xem mới trong quý',
    name: 'saleHouseSeenQuantity',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Nhà bán có khách xem mới',
      },
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
    ],
  },
  {
    label: 'Số lượng khách xem',
    name: 'saleCustomerSeenQuantity',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Số lượng khách xem',
      },
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
    ],
  },
  {
    label: 'Số lượng đàm phán mới',
    name: 'newSaleNegotiationQuantity',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Số lượng đàm phán mới',
      },
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
    ],
  },
];

export const purchaseAccumulationField = [
  {
    label: 'Số lượng nhu cầu mua mới',
    name: 'purchaseQuantity',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Số lượng nhu cầu mua mới',
      },
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
    ],
  },
  {
    label: 'Số khách hàng tư vấn',
    name: 'purchaseAdviseCustomer',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Số khách hàng tư vấn',
      },
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
    ],
  },
  {
    label: 'Số lượng khách đi xem mới trong quý',
    name: 'purchaseCustomerSeenQuantity',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Số lượng khách đi xem mới quý',
      },
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
    ],
  },
  {
    label: 'Số lượt dẫn khách đi xem nhà',
    name: 'purchaseToTakeCustomerSeenHouse',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Số lượt dẫn khách đi xem nhà',
      },
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
    ],
  },
  {
    label: 'Số lượng sản phẩm khách xem',
    name: 'purchaseProductSeenQuantity',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Số lượng sản phẩm khách xem',
      },
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
    ],
  },
  {
    label: 'Số lượng khách hàng xem lần 2 mới',
    name: 'purchaseCustomerSeen2ndQuantity',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Số lượng khách hàng xem lần 2 mới',
      },
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
    ],
  },
  {
    label: 'Số lượng đàm phán mới',
    name: 'newPurchaseNegotiationQuantity',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Số lượng đàm phán mới',
      },
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
    ],
  },
];
export const connectionAccumulationField = [
  {
    label: 'Số lượng nhu cầu kết nối mới',
    name: 'connectionQuantity',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Số lượng nhu cầu kết nối mới',
      },
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
    ],
  },
  {
    label: 'Số lượng khách đi xem mới',
    name: 'connectionCustomerSeenQuantity',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Số lượng khách đi xem mới',
      },
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
    ],
  },
  {
    label: 'Số lượng sản phẩm phù hợp',
    name: 'suitableConnectionProductQuantity',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Số lượng sản phẩm phù hợp',
      },
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
    ],
  },
  {
    label: 'Số lượng sản phẩm khách xem',
    name: 'connectionProductSeenQuantity',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Số lượng sản phẩm khách xem',
      },
      {
        pattern: new RegExp(/^[0-9]+$/),
        message: 'Phải là số',
      },
    ],
  },
  {
    label: 'Số lượng đàm phán mới',
    name: 'newConnectionNegotiationQuantity',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Số lượng đàm phán mới',
      },
    ],
  },
];

export const transactionField = [
  {
    label: 'Giá chốt',
    name: 'price',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Giá chốt',
      },
    ],
  },
  {
    label: 'Hoa hồng thực lãnh',
    name: 'brokerageFee',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Hoa hồng thực lãnh',
      },
    ],
  },
  {
    label: ' Hoa hồng đã nhận',
    name: 'brokerageFeeReceived',
    rules: [
      ({ getFieldValue }) => ({
        validator(current, value) {
          if (getFieldValue('brokerageFee') < value) {
            return Promise.reject(new Error('Không thể lớn hơn hoa hồng thực lãnh'));
          }

          return Promise.resolve();
        },
      }),
    ],
  },
  {
    label: 'Trạng thái hoa hồng',
    name: 'brokerageFeeStatus',
    items: [{ 1: 'Chưa thu tiền' }, { 2: 'Nhận 1 phần' }, { 3: 'Thu tiền ngay' }],
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Trạng thái hoa hồng',
      },
      ({ getFieldValue }) => ({
        validator(current, value) {
          if (Number(value) === 1 && getFieldValue('brokerageFeeReceived') !== 0) {
            return Promise.reject(new Error('Trạng thái không hợp lệ'));
          }
          if (
            Number(value) === 3 &&
            getFieldValue('brokerageFee') !== getFieldValue('brokerageFeeReceived')
          ) {
            return Promise.reject(new Error('Trạng thái không hợp lệ'));
          }
          if (
            Number(value) === 2 &&
            (getFieldValue('brokerageFeeReceived') >= getFieldValue('brokerageFee') ||
              getFieldValue('brokerageFeeReceived') === 0)
          ) {
            return Promise.reject(new Error('Trạng thái không hợp lệ'));
          }
          return Promise.resolve();
        },
      }),
    ],
  },
  {
    label: 'Địa chỉ',
    name: 'address',
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Địa chỉ',
      },
    ],
  },
  {
    label: 'Loại giao dịch',
    name: 'transactionType',
    items: [
      { 1: 'Khách hàng' },
      { 2: 'Chủ' },
      { 3: 'Trung gian' },
      { 4: 'Chính khách - Chính chủ' },
    ],
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Loại giao dịch',
      },
    ],
  },
  {
    label: 'Trạng thái',
    name: 'status',
    items: [
      { 1: 'Giữ chổ' },
      { 2: 'Đặt cọc' },
      { 3: 'Công chứng hợp đồng mua bán' },
      { 4: 'Đăng bộ sang tên' },
    ],
    rules: [
      {
        required: true,
        message: 'Vui lòng nhập Trạng thái giao dịch',
      },
    ],
  },
  {
    label: 'Ghi chú/Cần hỗ trợ',
    name: 'note',
  },
];

export const FLOOR_OPTIONS = _.range(0, 1000).map(number => ({
  key: number,
  text: number,
  value: number,
}));

export const HOUSE_STATUS = [
  { key: 0, text: 'Mở bán', value: 0 },
  { key: 1, text: 'Ngưng bán', value: 1 },
];

export const HOUSE_PERMISSION = [
  { key: 0, text: 'Cộng đồng', value: 0 },
  { key: 1, text: 'Cá nhân', value: 1 },
];

export const REQUEST_STATUS = [
  { key: 0, text: 'Chưa giải quyết', value: 0 },
  { key: 1, text: 'Đã giải quyết', value: 1 },
];

export const NUMERIC_COLUMNS = [
  'area',
  'floor_area',
  'width',
  'length',
  'wide_street',
  'brokerage_rate',
  'number_bedroom',
  'number_wc',
];

export const REDUX_STORE_VERSION = 5;

export const BLOG_TYPE = [
  { key: 1, text: 'Tin tức', value: 1 },
  { key: 2, text: 'Tips', value: 2 },
  { key: 3, text: 'Phong thủy', value: 3 },
];
export const accumulationColumns = [
  {
    title: 'Nhân viên',
    dataIndex: 'name',
    key: 'name',
    width: 150,
    align: 'center',
    fixed: 'left',
    sorter: (a, b) => {
      return `${a.name}`.localeCompare(b.name);
    },
    // responsive: ['xs', 'sm', 'md'],
  },
  {
    title: 'Ngày',
    dataIndex: 'date',
    key: 'date',
    width: 150,
    align: 'center',
    sorter: (a, b) => a.date - b.date,
    // responsive: ['xs', 'sm', 'md'],
  },
  {
    title: 'Sản phẩm marketing trong ngày',
    dataIndex: 'marketingQuantity',
    key: 'marketingQuantity',
    width: 100,
    align: 'center',
    // responsive: ['xs', 'sm'],
  },
  {
    title: 'Lượng sản phẩm hiện có',
    dataIndex: 'house_remain',
    key: 'house_remain',
    width: 100,
    align: 'center',
    // responsive: ['xs', 'sm'],
  },
  {
    title: 'Nhu cầu bán',
    children: [
      {
        title: 'Sản phẩm mới', // Số lượng nhu cầu bán mới
        dataIndex: 'saleQuantity',
        key: 'saleQuantity',
        align: 'center',
        width: 100,
      },
      {
        title: 'Số khách hàng tư vấn',
        dataIndex: 'saleAdviseCustomer',
        key: 'saleAdviseCustomer',
        align: 'center',
        width: 100,
      },
      {
        title: 'Sản phẩm được khách xem mới',
        dataIndex: 'saleHouseSeenQuantity',
        key: 'saleHouseSeenQuantity',
        align: 'center',
        width: 100,
      },
      {
        title: 'Lượt khách xem',
        dataIndex: 'saleCustomerSeenQuantity',
        key: 'saleCustomerSeenQuantity',
        align: 'center',
        width: 100,
      },
      {
        title: 'Đàm phán mới',
        dataIndex: 'newSaleNegotiationQuantity',
        key: 'newSaleNegotiationQuantity',
        align: 'center',
        width: 100,
      },
    ],
    responsive: ['xs', 'sm'],
  },
  {
    title: 'Nhu cầu mua',
    children: [
      {
        title: 'Khách hàng mới',
        dataIndex: 'purchaseQuantity',
        key: 'purchaseQuantity',
        align: 'center',
        width: 100,
      },
      {
        title: 'Số khách hàng tư vấn',
        dataIndex: 'purchaseAdviseCustomer',
        key: 'purchaseAdviseCustomer',
        align: 'center',
        width: 100,
      },
      {
        title: 'Khách xem lần đầu tiên trong quý',
        dataIndex: 'purchaseCustomerSeenQuantity',
        key: 'purchaseCustomerSeenQuantity',
        align: 'center',
        width: 100,
      },
      {
        title: 'Lượt khách đi xem nhà',
        dataIndex: 'purchaseToTakeCustomerSeenHouse',
        key: 'purchaseToTakeCustomerSeenHouse',
        align: 'center',
        width: 100,
      },
      {
        title: 'Lượng sản phẩm khách xem',
        dataIndex: 'purchaseProductSeenQuantity',
        key: 'purchaseProductSeenQuantity',
        align: 'center',
        width: 100,
      },
      {
        title: 'Khách hàng xem lần 2 mới',
        dataIndex: 'purchaseCustomerSeen2ndQuantity',
        key: 'purchaseCustomerSeen2ndQuantity',
        align: 'center',
        width: 100,
      },
      {
        title: 'Đàm phán mới',
        dataIndex: 'newPurchaseNegotiationQuantity',
        key: 'newPurchaseNegotiationQuantity',
        align: 'center',
        width: 100,
      },
    ],
    // responsive: ['xs', 'sm'],
  },
  {
    title: 'Nhu cầu kết nối',
    children: [
      {
        title: 'Nhu cầu kết nối mới',
        dataIndex: 'connectionQuantity',
        key: 'connectionQuantity',
        align: 'center',
        width: 100,
      },
      {
        title: 'Khách đi xem mới',
        dataIndex: 'connectionCustomerSeenQuantity',
        key: 'connectionCustomerSeenQuantity',
        align: 'center',
        width: 100,
      },
      {
        title: 'Lượng sản phẩm phù hợp',
        dataIndex: 'suitableConnectionProductQuantity',
        key: 'suitableConnectionProductQuantity',
        align: 'center',
        width: 100,
      },
      {
        title: 'Lượng sản phẩm khách xem',
        dataIndex: 'connectionProductSeenQuantity',
        key: 'connectionProductSeenQuantity',
        align: 'center',
        width: 100,
      },
      {
        title: 'Đàm phán mới',
        dataIndex: 'newConnectionNegotiationQuantity',
        key: 'newConnectionNegotiationQuantity',
        align: 'center',
        width: 100,
      },
    ],
    // responsive: ['xs', 'sm'],
  },
];

export const targetAccumulationColumns = [
  {
    title: 'Đầu kỳ mua',
    dataIndex: 'purchase',
    key: 'purchase',
    // width: 50,
    align: 'center',
    responsive: ['xs', 'sm'],
  },
  {
    title: 'Đầu kỳ kết nối', // Số lượng nhu cầu bán mới
    dataIndex: 'connection',
    key: 'connection',
    align: 'center',
    // width: 50,
    responsive: ['xs', 'sm'],
  },
];

export const postActionType = {
  create: 1,
  update: 2,
  history: 3,
  create_post_address: 4,
  set_option: 5,
  delete: 6,
  address_setting: 7,
  address_setting_modal: 8,
  load_more: 9,
  post_list: 10,
  update_post: 11,
  delete_post: 12,
  update_posted: 13,
  delete_posted: 14,
  detail_post: 15,
  confirm: 16,
};

export const postType = {
  auto: 1,
  manual: 2,
  confirm: 3,
};

export const alonhadatReducerType = {
  UPDATE_ADDRESS_UPDATE: 'UPDATE_ADDRESS_UPDATE',
  GET: 'GET',
  UPDATE: 'UPDATE',
  LOADING: 'LOADING',
  LOADING_DONE: 'LOADING_DONE',
  OPEN_MODAL: 'OPEN_MODAL',
  CLOSE_MODAL: 'CLOSE_MODAL',
  SET_ID: 'SET_ID',
  SET_TYPE: 'SET_TYPE',
  SET_PREV_TYPE: 'SET_PREV_TYPE',
  SET_POST_TYPE: 'SET_POST_TYPE',
  SET_DEFAULT_TYPE: 'SET_DEFAULT_TYPE',
  SET_PREV_ID: 'SET_PREV_ID',
};

export const postReducerType = {
  POSTED_SET_DATA: 'POSTED_SET_DATA',
  WAIT_FOR_POSTING_SET_DATA: 'POST_WAITING_SET_DATA',
  ERROR_POST_SET_DATA: 'ERROR_POST_SET_DATA',
  WAITING: 'POST/WAITING',
  DONE: 'POST/DONE',
  LAST_PAGE: 'LAST_PAGE',
  SET_POST_QUANTITY: 'SET_POST_QUANTITY',
  SET_WAITING_QUANTITY: 'SET_WAITING_QUANTITY',
  SET_ERROR_QUANTITY: 'SET_ERROR_QUANTITY',
  CURRENT_PAGE: 'CURRENT_PAGE',
};
export const postManagerHistoryColumns = [
  {
    title: 'Cột',
    dataIndex: 'column',
    width: 120,
    sorter: true,
    render: value => (value === 'into_money' ? 'Giá' : 'Trạng thái'),
  },
  {
    title: 'Nhân viên',
    dataIndex: 'user_name',
    width: 120,
  },
  {
    title: 'Giá trị cũ',
    dataIndex: 'old_value',
    width: 100,
    render: (value, row) => {
      if (row.column === 'into_money') {
        return formatMoney(value);
      }
      if (row.column === 'status' && Number(value) === 0) {
        return 'Mở bán';
      }
      if (row.column === 'status' && Number(value) === 1) {
        return 'Ngưng bán';
      }
    },
  },
  {
    title: 'Giá trị mới',
    dataIndex: 'new_value',
    width: 100,
    render: (value, row) => {
      if (row.column === 'into_money') {
        return formatMoney(value);
      }
      if (row.column === 'status' && Number(value) === 0) {
        return 'Mở bán';
      }
      if (row.column === 'status' && Number(value) === 1) {
        return 'Ngưng bán';
      }
    },
  },
  {
    title: 'Thời điểm',
    dataIndex: 'created_at',
    sorter: true,
    width: 150,
    render: value => new Date(value * 1000).toLocaleString(),
  },
];
export const transactionSuccessColumns = [
  {
    title: 'Nhân viên',
    dataIndex: 'staff',
    key: 'staff',
    align: 'center',
    fixed: 'left',
    width: 130,
    render: item => item && item.name,
  },
  {
    title: 'Mã giao dịch',
    dataIndex: 'transactionCode',
    key: 'transactionCode',
    align: 'center',
    // render: item => item && item.transactionCode,
  },
  {
    title: 'Ngày cọc',
    dataIndex: 'date',
    key: 'date',
    align: 'center',
    width: 100,

    render: value => value && moment(value).format('YYYY/MM/DD'),
  },
  {
    title: 'Giá chốt',
    dataIndex: 'price',
    key: 'price',
    align: 'center',

    render: value => formatMoney(Number(value)),
  },
  {
    title: 'Loại giao dịch',
    dataIndex: 'transaction_type',
    key: 'transactionType',
    align: 'center',

    // render: item => item && item.transactionType,
  },

  {
    title: 'Hoa hồng thực lãnh',
    dataIndex: 'brokerageFeeUser',
    key: 'brokerageFee',
    align: 'center',

    render: value => formatMoney(Number(value)),
  },
  {
    title: 'Hoa hồng đã nhận',
    dataIndex: 'brokerageFeeReceived',
    key: 'brokerageFeeReceived',
    align: 'center',
    render: value => formatMoney(Number(value)),
    // witdh: 80,
  },
  {
    title: 'Hoa hồng phải thu',
    dataIndex: 'brokerageFeeReceivable',
    key: 'brokerageFeeReceivable',
    align: 'center',

    render: value => formatMoney(Number(value)),
  },
  {
    title: 'Ngày nhận hoa hồng',
    dataIndex: 'brokerageReceiveDate',
    key: 'brokerageReceiveDate',
    align: 'center',
    width: 100,

    render: value => value && moment(value).format('YYYY/MM/DD'),
  },
  {
    title: 'Trạng thái hoa hồng',
    dataIndex: 'brokerageFeeStatus',
    key: 'brokerageFeeStatus',
    align: 'center',

    render: value => value && moment(value).format('YYYY/MM/DD'),
  },
  {
    title: 'Tỷ lệ hoa hồng',
    dataIndex: 'brokerage_rate',
    key: 'brokerageRate',
    align: 'center',

    // render: item => item && item.brokerageRate,
  },
  {
    title: 'Ngày công chứng',
    dataIndex: 'notarizedDate',
    key: 'notarizedDate',
    align: 'center',
    width: 100,

    render: value => value && moment(value).format('YYYY/MM/DD'),
  },
  {
    title: 'Trạng thái',
    dataIndex: 'status',
    key: 'status',
    align: 'center',

    // render: item => item && item.status,
  },
  {
    title: 'Ghi chú/Cần hỗ trợ',
    dataIndex: 'note',
    key: 'note',
    align: 'center',
  },
  {
    title: 'Khách hàng',
    dataIndex: 'customer',
    key: 'customer',
    align: 'center',
    // width: 130,

    render: item => item && item.name,
  },
  {
    title: 'Địa chỉ',
    dataIndex: 'address',
    key: 'address',
    align: 'center',
    width: 130,

    // render: item => item && `${item.house_number} ${item.house_address} ${item.district}`,
  },
];

export const transactionSuccessColumnsOld = [
  {
    title: 'Nhân viên',
    dataIndex: 'name',
    key: 'name',
    align: 'center',
    fixed: 'left',
    // width: 130,
    // render: item => item && item.name,
  },
  {
    title: 'Ngày cọc',
    dataIndex: 'date',
    key: 'date',
    align: 'center',
    width: 120,
    // render: value => value && moment(value).format('YYYY/MM/DD'),
  },
  {
    title: 'Giá chốt',
    dataIndex: 'price',
    key: 'price',
    align: 'center',

    // render: value => formatMoney(Number(value)),
  },
  {
    title: 'Loại giao dịch',
    dataIndex: 'transactionType',
    key: 'transactionType',
    align: 'center',

    // render: item => item && item.transactionType,
  },

  {
    title: 'Hoa hồng thực lãnh',
    dataIndex: 'brokerageFee',
    key: 'brokerageFee',
    align: 'center',

    // render: value => formatMoney(Number(value)),
  },
  {
    title: 'Hoa hồng đã nhận',
    dataIndex: 'brokerageFeeReceived',
    key: 'brokerageFeeReceived',
    align: 'center',
    // render: value => formatMoney(Number(value)),
    // witdh: 80,
  },
  {
    title: 'Hoa hồng phải thu',
    dataIndex: 'brokerageFeeReceivable',
    key: 'brokerageFeeReceivable',
    align: 'center',

    // render: value => formatMoney(Number(value)),
  },
  {
    title: 'Ngày nhận hoa hồng',
    dataIndex: 'brokerageReceiveDay',
    key: 'brokerageReceiveDay',
    align: 'center',
    width: 100,

    // render: value => value && moment(value).format('YYYY/MM/DD'),
  },
  {
    title: 'Trạng thái hoa hồng',
    dataIndex: 'brokerageFeeStatus',
    key: 'brokerageFeeStatus',
    align: 'center',

    // render: value => value && moment(value).format('YYYY/MM/DD'),
  },
  {
    title: 'Ngày công chứng',
    dataIndex: 'notarizedDay',
    key: 'notarizedDay',
    align: 'center',
    // width: 100,

    // render: value => value && moment(value).format('YYYY/MM/DD'),
  },
  {
    title: 'Trạng thái',
    dataIndex: 'status',
    key: 'status',
    align: 'center',

    // render: item => item && item.status,
  },
  {
    title: 'Ghi chú/Cần hỗ trợ',
    dataIndex: 'note',
    key: 'note',
    align: 'center',
  },
  {
    title: 'Địa chỉ',
    dataIndex: 'address',
    key: 'address',
    align: 'center',
    // width: 130,

    // render: item => item && `${item.house_number} ${item.house_address} ${item.district}`,
  },
];
export const statisticColumns = [
  {
    title: 'Nhân viên',
    dataIndex: 'user',
    key: 'user',
    width: 150,
    align: 'center',
    fixed: 'left',
    sorter: (a, b) => {
      return `${a.name}`.localeCompare(b.name);
    },
  },
  {
    title: 'Nhu cầu bán',
    children: [
      {
        title: 'Tổng số sản phẩm',
        children: [
          {
            title: 'Đầu kỳ', // Số lượng nhu cầu bán mới
            dataIndex: 'house_remain',
            key: 'house_remain',
            align: 'center',
            width: 100,
          },
          {
            title: 'Có thêm',
            dataIndex: 'new_house_quantity',
            key: 'new_house_quantity',
            align: 'center',
            width: 100,
          },
          {
            title: 'Hiện có',
            dataIndex: 'house_quantity',
            key: 'house_quantity ',
            align: 'center',
            width: 100,
          },
        ],
      },
      {
        title: 'Tỷ lệ mua',
        dataIndex: 'potentialBuyingOfSale', // số lượng giao dịch/ hiện có
        key: 'potentialBuyingOfSale',
        align: 'center',
        render: item => (item ? `${Number(item).toFixed(3)}%` : '0%'),
        width: 100,
      },
      {
        title: 'Số lượng giao dịch',
        dataIndex: 'transaction',
        key: 'transaction',
        align: 'center',
        render: item => (item ? item.saleTransactionQuantity : 0),
        width: 100,
      },
      {
        title: 'Hoa hồng trung bình',
        dataIndex: 'averageSaleBrokerageFee',
        key: 'averageSaleBrokerageFee',
        align: 'center',
        // render: item => (item ? formatMoney(item) : 0),
        width: 100,
      },
      {
        title: 'Tổng hoa hồng',
        // dataIndex: 'saleBrokerageFee',
        // key: 'saleBrokerageFee',
        dataIndex: 'transaction',
        key: 'transaction',
        align: 'center',
        render: item => (item ? formatMoney(Number(item.saleBrokerageFee)) : 0),
        width: 100,
      },
    ],
    responsive: ['xs', 'sm'],
  },
  {
    title: 'Nhu cầu mua',
    children: [
      {
        title: 'Tổng khách mua',
        children: [
          {
            title: 'Đầu kỳ', // Số lượng nhu cầu bán mới
            dataIndex: 'purchase_first_value',
            key: 'purchase',
            align: 'center',
            width: 100,
          },
          {
            title: 'Có thêm',
            dataIndex: 'current_purchase_quantity',
            key: 'current_purchase_quantity ', // purchase transaction quantity
            align: 'center',
            width: 100,
          },
          {
            title: 'Hiện có',
            dataIndex: 'new_purchase_quantity',
            key: 'new_purchase_quantity', // total target + purchase transaction quantity
            align: 'center',
            width: 100,
          },
        ],
      },

      {
        title: 'Tỷ lệ xem',
        dataIndex: 'potentialSeenOfPurchase',
        key: 'potentialSeenOfPurchase',
        align: 'center',
        render: item => (item ? `${(Number(item) * 100).toFixed(1)}%` : '0%'),
        width: 100,
      },
      {
        title: 'Số lượng khách xem',
        dataIndex: 'purchaseCustomerSeenQuantity',
        key: 'purchaseCustomerSeenQuantity',
        align: 'center',
        render: item => item || 0,
        width: 100,
      },
      {
        title: 'Tỷ lệ mua',
        dataIndex: 'potentialBuyingOfPurchase',
        key: 'potentialBuyingOfPurchase',
        align: 'center',
        render: item => (item ? `${(Number(item) * 100).toFixed(1)}%` : '0%'),
        width: 100,
      },
      {
        title: 'Số lượng giao dịch',
        dataIndex: 'transaction',
        key: 'transaction',
        render: item => (item ? item.purchaseTransactionQuantity : 0),
        align: 'center',
        width: 100,
      },
      {
        title: 'Hoa hồng trung bình',
        dataIndex: 'averagePurchaseBrokerageFee',
        key: 'averagePurchaseBrokerageFee',
        align: 'center',
        render: item => (item ? formatMoney(Number(item)) : 0),
        width: 100,
      },
      {
        title: 'Tổng hoa hồng',
        dataIndex: 'transaction',
        key: 'transaction',
        align: 'center',
        render: item =>
          item && item.purchaseBrokerageFee ? formatMoney(Number(item.purchaseBrokerageFee)) : 0,
        width: 100,
      },
    ],
    responsive: ['xs', 'sm'],
  },
  {
    title: 'Nhu cầu kết nối',
    children: [
      {
        title: 'Tổng nhu cầu kết nối',
        children: [
          {
            title: 'Đầu kỳ', // Số lượng nhu cầu bán mới
            dataIndex: 'connection_first_value',
            key: 'connection_first_value',
            align: 'center',
            width: 100,
          },
          {
            title: 'Có thêm',
            dataIndex: 'current_connection_quantity',
            key: 'current_connection_quantity ',
            align: 'center',
            width: 100,
          },
          {
            title: 'Hiện có',
            dataIndex: 'new_connection_quantity',
            key: 'new_connection_quantity',
            align: 'center',
            width: 100,
          },
        ],
      },

      {
        title: 'Tỷ lệ xem',
        dataIndex: 'potentialSeenOfConnection',
        key: 'potentialSeenOfConnection',
        align: 'center',
        render: item => (item ? `${(Number(item) * 100).toFixed(1)}%` : '0%'),
        width: 100,
      },
      {
        title: 'Số lượng khách xem',
        dataIndex: 'connectionCustomerSeenQuantity',
        key: 'connectionCustomerSeenQuantity',
        align: 'center',
        width: 100,
      },
      {
        title: 'Tỷ lệ mua',
        dataIndex: 'potentialBuyingOfConnection',
        key: 'potentialBuyingOfConnection',
        align: 'center',
        render: item => (item ? `${(Number(item) * 100).toFixed(1)}%` : '0%'),
        width: 100,
      },
      {
        title: 'Số lượng giao dịch',
        dataIndex: 'transaction',
        key: 'transaction',
        render: item => (item ? item.connectionTransactionQuantity : 0),
        align: 'center',
        width: 100,
      },
      {
        title: 'Hoa hồng trung bình',
        dataIndex: 'averageConnectionBrokerageFee',
        key: 'averageConnectionBrokerageFee',
        align: 'center',
        render: item => (item ? formatMoney(Number(item)) : 0),
        width: 100,
      },
      {
        title: 'Tổng hoa hồng',
        dataIndex: 'transaction', // cho nay ne
        key: 'transaction',
        align: 'center',
        render: item =>
          item && item.connectionBrokerageFee
            ? formatMoney(Number(item.connectionBrokerageFee))
            : 0,
        width: 100,
      },
    ],
    responsive: ['xs', 'sm'],
  },
];

export const overViewMainCard = {
  flex: 1,
  padding: 20,
  minWidth: '100%',
  minHeight: ' 210px',
};

export const fiveWay = [
  {
    key: 1,
    label: 'Tổng hợp',
  },
  {
    key: 2,
    label: 'Nhu Cầu Bán',
  },
  {
    key: 3,
    label: 'Nhu Cầu Mua',
  },
  {
    key: 4,
    label: 'Nhu Cầu Kết Nối',
  },
];

export const defaultTag = [
  {
    tag_id: 1,
    color: 'magenta',
  },
  {
    tag_id: 2,
    color: 'red',
  },
  {
    tag_id: 3,
    color: 'volcano',
  },
  {
    tag_id: 4,
    color: 'orange',
  },
  {
    tag_id: 5,
    color: 'gold',
  },
  {
    tag_id: 6,
    color: 'lime',
  },
  {
    tag_id: 7,
    color: 'green',
  },
  {
    tag_id: 8,
    color: 'cyan',
  },
  {
    tag_id: 9,
    color: 'blue',
  },
  {
    tag_id: 10,
    color: 'geekblue',
  },
];

export const REQUESTCONSTANT = [
  {
    name: 'min_price',
    text: 'Giá tối thiểu',
  },
  {
    name: 'max_price',
    text: 'Giá tối đa',
  },
  {
    name: 'house_type',
    text: 'Loại BĐS',
  },
  {
    name: 'province',
    text: 'Quận/Huyện',
  },
  {
    name: 'area',
    text: 'Diện tích',
  },
  {
    name: 'floors',
    text: 'Số lầu',
  },
  {
    name: 'number_bedroom',
    text: 'Số PN',
  },
  {
    name: 'min_width',
    text: 'Chiều ngang tối thiểu',
  },
  {
    name: 'districts',
    text: 'Phường',
  },
  {
    name: 'road_area',
    text: 'Đường rộng tối thiểu',
  },
];

// phiếu thu
export const receiptsColumns = [
  {
    title: 'Người tạo',
    dataIndex: 'user',
    key: 'user',
    width: 130,
    align: 'center',
    fixed: 'left',
    render: item => item && item.name,
    sorter: (a, b) => {
      return `${a.name}`.localeCompare(b.name);
    },
  },
  {
    title: 'Mã giao dịch',
    dataIndex: 'transactionCode',
    key: 'transactionCode',
    align: 'center',
    width: 100,
    // render: item => item && item.transactionCode,
  },
  {
    title: 'Nhân viên',
    dataIndex: 'staff',
    key: 'staff',
    align: 'center',
    width: 130,
    render: item => item && item.name,
  },
  {
    title: 'Khách hàng',
    dataIndex: 'customer',
    key: 'customer',
    align: 'center',
    width: 130,
    render: item => item && item.name,
  },
  {
    title: 'Loại giao dịch',
    dataIndex: 'transaction_type',
    key: 'transactionType',
    align: 'center',
    width: 100,
    // render: item => item && item.transactionType,
  },
  {
    title: 'Ngày đặt cọc',
    dataIndex: 'date',
    key: 'date',
    align: 'center',
    width: 100,
    render: value => value && moment(value).format('YYYY/MM/DD'),
  },
  {
    title: 'Ngày công chứng',
    dataIndex: 'notarizedDate',
    key: 'notarizedDate',
    align: 'center',
    width: 100,
    render: value => value && moment(value).format('YYYY/MM/DD'),
  },
  {
    title: 'Ngày nhận hoa hồng',
    dataIndex: 'brokerageReceiveDate',
    key: 'brokerageReceiveDate',
    align: 'center',
    width: 100,
    render: value => value && moment(value).format('YYYY/MM/DD'),
  },
  {
    title: 'Ngày giữ chổ',
    dataIndex: 'reservationDate',
    key: 'reservationDate',
    align: 'center',
    width: 100,
    render: value => value && moment(value).format('YYYY/MM/DD'),
  },
  {
    title: 'Giá chốt',
    dataIndex: 'price',
    key: 'price',
    align: 'center',
    width: 100,
    render: value => formatMoney(Number(value)),
  },
  {
    title: 'Hoa hồng thực lãnh',
    dataIndex: 'brokerageFee',
    key: 'brokerageFee',
    align: 'center',
    width: 100,
    render: value => formatMoney(Number(value)),
  },
  {
    title: 'Địa chỉ',
    dataIndex: 'address',
    key: 'address',
    align: 'center',
    width: 150,
    // render: item => item && `${item.house_number} ${item.house_address} ${item.district}`,
  },
];

export const transactionBillColumns = [
  {
    title: 'Người tạo',
    dataIndex: 'user',
    key: 'user',
    width: 150,
    align: 'center',
    fixed: 'left',
    render: item => item && item.name,
    sorter: (a, b) => {
      return `${a.name}`.localeCompare(b.name);
    },
  },
  {
    title: 'Mã phiếu',
    dataIndex: 'feeCode',
    key: 'feeCode',
    align: 'center',
    width: 100,
  },
  {
    title: 'Nhân viên',
    dataIndex: 'detail',
    key: 'staff',
    align: 'center',
    width: 100,
    render: item => item && item.name,
  },
  {
    title: 'Ngày tạo',
    dataIndex: 'dateCollect',
    key: 'dateCollect',
    align: 'center',
    width: 100,
  },
  {
    title: 'Khách hàng',
    dataIndex: 'detail',
    key: 'customer',
    align: 'center',
    width: 100,
    render: item => item && item.customer_name,
  },
  {
    title: 'Phí phải thu',
    dataIndex: 'feePayable',
    key: 'feePayable',
    align: 'center',
    width: 100,
    render: value => formatMoney(Number(value)),
  },
  {
    title: 'Phí đã thu',
    dataIndex: 'feeCollect',
    key: 'feeCollect',
    align: 'center',
    width: 100,
    render: value => formatMoney(Number(value)),
  },
  {
    title: 'Phí chưa thu',
    dataIndex: 'uncollectedFees',
    key: 'uncollectedFees',
    align: 'center',
    width: 100,
    render: value => formatMoney(Number(value)),
  },
  {
    title: 'Địa chỉ',
    dataIndex: 'house',
    key: 'house',
    align: 'center',
    width: 100,
    render: item => item && `${item.house_number} ${item.house_address} ${item.district}`,
  },
  {
    title: 'Ghi chú',
    dataIndex: 'note',
    key: 'note',
    align: 'center',
    width: 100,
    // render: value => formatMoney(Number(value)),
  },
];
