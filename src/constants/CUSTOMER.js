export const CUSTOMER_TYPES = [
  { key: 1, text: 'Nhà đầu tư', value: 1 },
  { key: 2, text: 'Khách đã mua', value: 2 },
  { key: 3, text: 'Khách tiềm năng', value: 3 },
  { key: 4, text: 'không mua nữa', value: 4 },
];
