import moment from 'moment';
import _ from 'lodash';

export const FIELDS = [
  {
    icon: 'info',
    label: 'Tiêu đề',
    property: 'title',
    default: true,
    hide: true,
    filter: { fetchValue: true },
  },
  {
    icon: 'house',
    label: 'Số nhà',
    property: 'house_number',
    filter: { fetchValue: true },
    default: true,
    hide: true,
    pattern: {
      test: [/(Địa chỉ|Đ\/c)/gi, /(\d+)(\/\d+)*\s[^,.]+/gi],
      query: {
        regEx: /.*\s((\d+)(\/\d+)*)\s.*/gi,
        replace: '$1',
      },
    },
    continue: true,
  },
  {
    icon: 'house',
    label: 'Đường',
    property: 'house_address',
    filter: { fetchValue: true },
    hide: true,
    default: true,
    pattern: {
      test: [/(Địa chỉ|Đ\/c)/gi, /(\d+)(\/\d+)*\s[^,.]+/gi],
      query: {
        regEx: /.*\s((\d+)(\/\d+)*)\s+([^,.]+).*/gi,
        replace: '$4',
      },
    },
    continue: true,
  },
  {
    icon: 'building',
    label: 'Loại BĐS',
    property: 'property_type',
    hide: true,
    default: true,
    filter: {
      fetchOption: options => _.values(options.type) || [],
      toValue: (value, options) => _.entries(options.type).filter(t => t[1] === value)[0][0],
    },
    format: (house, options) =>
      options && options.type ? options.type[house.property_type] : 'Chưa xác định',
  },
  {
    icon: 'building',
    label: 'Pháp Lý',
    property: 'ownership',
    default: true,
    filter: {
      fetchOption: options => _.values(options.ownership) || [],
      toValue: (value, options) => _.entries(options.ownership).filter(t => t[1] === value)[0][0],
    },
    format: (house, options) =>
      options && options.ownership ? options.ownership[house.ownership] : 'Chưa xác định',
  },
  {
    icon: 'house',
    label: 'Loại nhà',
    property: 'house_type',
    hide: true,
    default: true,
    filter: {
      fetchOption: options => (options.class ? _.values(options.class) : []),
      toValue: (value, options) => _.entries(options.class).filter(t => t[1] === value)[0][0],
    },
    format: (house, options) =>
      options && options.class ? options.class[house.house_type] : 'Chưa xác định',
    extract: line => {
      const array = ['Đất', 'Nhà tạm', 'Nhà cấp 4', 'Nhà cấp 3', 'Nhà cao tầng', 'Biệt thự'];
      const regex = /^.*(Đất|Nhà\stạm|Nhà\scấp\s4|Nhà\scấp\s3|Nhà\scao\stầng|Biệt\sthự).*$/gi;
      if (line.match(regex)) {
        const match = line.replace(regex, '$1');
        const index = array.map(v => v.toUpperCase()).indexOf(match.toUpperCase());
        return (index + 1).toString();
      }
      return false;
    },
    continue: true,
  },
  {
    icon: 'info',
    label: 'Số lầu',
    property: 'floors',
    default: true,
    filter: {
      suffix: 'lầu',
      operation: 'between',
      values: [0, 30],
    },
    extract: line => {
      const regex = /^.*(\d+)\s?((l|L)(ầ|a|â)u).*$/gi;
      if (line.match(regex)) {
        return Number(line.replace(regex, '$1'));
      }
      return false;
    },
  },
  {
    icon: 'info',
    label: 'Ngang',
    property: 'width',
    hide: true,
    format: house => `${house.width || 0}m`,
    filter: {
      suffix: 'm',
      operation: 'between',
      values: [0, 1000],
    },
    pattern: {
      test: [/((\d+)((,)(\d+))?)\s?m\s?x\s?((\d+)((,)(\d+))?)\s?m/gi],
      query: {
        regEx: /.*\s((\d+)((,)(\d+))?)\s?m\s?x\s?((\d+)((,)(\d+))?)\s?m.*/gi,
        replace: '$1',
      },
    },
    continue: true,
  },
  {
    icon: 'info',
    label: 'Dài',
    property: 'length',
    hide: true,
    filter: {
      suffix: 'm',
      operation: 'between',
      values: [0, 1000],
    },
    format: house => `${house.length || 0}m`,
    pattern: {
      test: [/((\d+)((,)(\d+))?)\s?m\s?x\s?((\d+)((,)(\d+))?)\s?m/gi],
      query: {
        regEx: /.*\s((\d+)((,)(\d+))?)\s?m\s?x\s?((\d+)((,)(\d+))?)\s?m.*/gi,
        replace: '$6',
      },
    },
  },
  {
    icon: 'info',
    label: 'Diện tích',
    hide: true,
    filter: {
      suffix: 'm2',
      operation: 'between',
      values: [0, 10000],
    },
    property: 'area',
    default: true,
    format: house => `${house.area || 0}m2`,
    pattern: {
      test: [/((D|d)i(ệ|e)n (t|T)(í|i)ch)/gi, /(\d+)\s?(m2)/gi],
      query: {
        regEx: /.*\s(\d+)((,|.)(\d+))?\s?(m2).*/gi,
        replace: '$1$2',
      },
    },
  },
  {
    icon: 'info',
    label: 'Giá',
    property: 'into_money',
    hide: true,
    filter: {
      suffix: 'tỷ',
      operation: 'between',
      values: [0, 1000],
      toValue: value => value * 1000000000,
    },
    default: true,
    extract: line => {
      const regex1 = /((G|g)(í|i)(a|á))/gi;
      const regex2 = /(\d+)\s?(t(ỷ|ỉ|y|i|r|riệu|rieu))/gi;
      if (line.match(regex1) && line.match(regex2)) {
        const string = line.replace(
          /.*\s(\d+)((,|.)(\d+))?\s?(t(ỷ|ỉ|y|i|r|riệu|rieu)).*/gi,
          '$1$2-$5',
        );
        const parts = string.split('-');
        if (parts.length > 1 && parts[1].length) {
          if (parts[1] === 'tỷ' || parts[1] === 'tỉ' || parts[1] === 'ty') {
            return Number(parts[0].replace(',', '.')) * 1000000000;
          }
          if (parts[1] === 'tr' || parts[1] === 'triệu') {
            return Number(parts[0].replace(',', '.')) * 1000000;
          }
          return parts[0];
        }
        return false;
      }
      return false;
    },
  },
  {
    icon: 'info',
    label: 'Hướng',
    property: 'house_direction',
    filter: {
      fetchOption: options => options.direction || [],
    },
    default: true,
    itemKey: 'direction',
    format: house =>
      house.house_direction ? house.house_direction.map(d => d.direction).join(', ') : '',
    extract: line => {
      const regex = /^.*((H|h)(ư|u|ứ)(ớ|ơ|o)ng)\s*:?\s*(Đông|Tây|Nam|Bắc|Đông\sNam|Đông\sBắc|Tây\sNam|Tây\sBắc)\s?\.?$/gi;
      if (line.match(regex)) {
        return line.replace(regex, '$5');
      }
      return false;
    },
  },
  {
    icon: 'info',
    label: 'Mục đích',
    property: 'purpose',
    default: true,
    format: house => (house.purpose === 0 ? 'Bán' : 'Cho Thuê'),
  },
  {
    icon: 'info',
    label: 'Nhân viên',
    property: 'user_id',
    default: true,
    format: house => (house.user ? house.user.name : ''),
  },
  {
    icon: 'info',
    label: 'Dự án',
    property: 'project_id',
    default: true,
    format: house => (house.project ? house.project.name : 'Không có'),
  },
  { icon: 'info', label: 'Phường', property: 'district', hide: true },
  { icon: 'info', label: 'Huyện', property: 'province', hide: true },
  {
    icon: 'info',
    label: 'Số phòng ngủ',
    property: 'number_bedroom',
    pattern: {
      test: [/(\d+)\s?((p|P)h(ò|o)ng\s(n|N)g(ủ|u)|pn)/gi],
      query: {
        regEx: /^.*(\d+)\s?((p|P)h(ò|o)ng\s(n|N)g(ủ|u)|pn).*$/gi,
        replace: '$1',
      },
    },
  },
  {
    icon: 'info',
    label: 'Số phòng WC',
    property: 'number_wc',
    pattern: {
      test: [/(\d+)\s?((p|P)h(ò|o)ng\s(n|N)g(ủ|u)|pn)/gi],
      query: {
        regEx: /^.*(\d+)\s?((p|P)h(ò|o)ng\s(n|N)g(ủ|u)|pn).*$/gi,
        replace: '$1',
      },
    },
  },
  {
    icon: 'info',
    label: 'Loại đường',
    property: 'street_type',
    format: (house, options) =>
      options && options.street_type && house.street_type
        ? options.street_type[house.street_type]
        : 'Chưa xác định',
  },
  {
    icon: 'info',
    label: 'Đường rộng (m)',
    property: 'wide_street',
    format: house => `${house.wide_street}m`,
    pattern: {
      test: [/((Đ|d|đ|D)(ư|u)(ờ|o|ơ)ng)/gi, /(\d+)\s?m/gi],
      query: {
        regEx: /.*?(\d+)\s?m.*/gi,
        replace: '$1',
      },
    },
  },
  {
    icon: 'info',
    label: 'Diện tích sàn',
    property: 'floor_area',
    format: house => `${house.floor_area || 'Chưa xác định '}m2`,
  },
  {
    icon: 'info',
    label: 'Đăng ký ngày',
    property: 'created_at',
    format: house => moment(house.created_at * 1000).format('DD/MM/YYYY HH:mm:ss'),
  },
  {
    label: 'NGÀY CẬP NHẬT',
    property: 'updated_at',
    format: house => moment(house.updated_at * 1000).format('DD/MM/YYYY HH:mm:ss'),
  },
];
