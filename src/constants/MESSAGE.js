export const UNKNOWN_ERROR = 'Something went wrong. Please try again later!';
export const DELETE_HOUSE_CONFIRM = 'Bạn có chắc muốn xoá bất động sản?';
export const DELETE_CUSTOMER_CONFIRM = 'Bạn có chắc muốn xoá khách hàng?';
export const RESET_USER_CONFIRM = 'Bạn có chắc muốn đặt lại mật khẩu người dùng này?';
export const DISABLE_USER_CONFIRM = 'Bạn có chắc muốn khoá người dùng này?';
export const ENABLE_USER_CONFIRM = 'Bạn có chắc muốn mở khoá người dùng này?';
export const DELETE_USER_CONFIRM = 'Bạn có chắc muốn xoá người dùng này?';
export const DELETE_BLOG_CONFIRM = 'Bạn có chắc muốn xoá tin tức này?';
export const INVALID_INFO_INPUT =
  'Thông tin nhập không đúng. Vui lòng kiểm tra lại thông tin yêu cầu';
export const DELETE_REQUEST_CONFIRM = 'Bạn có chắc muốn xoá yêu cầu này?';
export const DELETE_PROJECT_CONFIRM = 'Bạn có chắc muốn xoá dự án này?';

export const DELETE_DICTIONARY_CONFIRM = 'Bạn có chắc muốn xoá từ điển này?';
export const CONFIRM_DELETE_POST = 'Bạn có chắc muốn xóa bài đăng?';
