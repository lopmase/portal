export const MODAL_ACTION_TYPES = {
  OPEN: 'MODAL/OPEN',
  CLOSE: 'MODAL/CLOSE',
  TOGGLE: 'MODAL/TOGGLE',
  WAITING: 'WAITING',
  WAITING_DONE: 'WAITING_DONE',
};
