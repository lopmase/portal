export const REQUEST_TYPES = [
  { key: 1, text: 'Mua', value: 1 },
  { key: 2, text: 'Thuê', value: 2 },
];

export const WARD_TYPES = [{ key: 1, text: 'Phường 1', value: 1 }];

export const CUSTOMER_TYPES = [{ key: 1, text: 'Tạo mới', value: 1 }];

export const DIRECTION_TYPES = [
  { key: 1, text: 'Đông', value: 'Đông' },
  { key: 2, text: 'Tây', value: 'Tây' },
  { key: 3, text: 'Nam', value: 'Nam' },
  { key: 4, text: 'Bắc', value: 'Bắc' },
  { key: 5, text: 'Đông Nam', value: 'Đông Nam' },
  { key: 6, text: 'Đông Bắc', value: 'Đông Bắc' },
  { key: 7, text: 'Tây Nam', value: 'Tây Nam' },
  { key: 8, text: 'Tây Bắc', value: 'Tây Bắc' },
];

export const DISTRICT_TYPES = [
  { key: 1, text: 'Vũng Tàu', value: 1 },
  { key: 2, text: 'Bà Rịa', value: 2 },
  { key: 3, text: 'Châu Đức', value: 3 },
  { key: 4, text: 'Xuyên Mộc', value: 4 },
  { key: 5, text: 'Tân Thành', value: 5 },
  { key: 6, text: 'Long Điền', value: 6 },
  { key: 7, text: 'Đất Đỏ', value: 7 },
  { key: 8, text: 'Côn Đảo', value: 8 },
];

export const ESTATE_TYPES = [
  { key: 1, text: 'Căn hộ chung cư', value: 1 },
  { key: 2, text: 'Nhà ở riêng lẻ', value: 2 },
  { key: 3, text: 'Nhà mặt tiền', value: 3 },
  { key: 4, text: 'Biệt thự - Villa', value: 4 },
  { key: 5, text: 'Nhà nghỉ - Khách sạn', value: 5 },
  { key: 6, text: 'Phòng trọ', value: 6 },
  { key: 7, text: 'Kho xưởng', value: 7 },
  { key: 8, text: 'Trang trại', value: 8 },
  { key: 9, text: 'Loại khác', value: 9 },
];

export const HOUSE_TYPES = [
  { key: 1, text: 'Đất', value: 1 },
  { key: 2, text: 'Nhà tạm', value: 2 },
  { key: 3, text: 'Nhà cấp 4', value: 3 },
  { key: 4, text: 'Nhà cao tầng', value: 4 },
  { key: 5, text: 'Biệt thự', value: 5 },
  { key: 6, text: 'Nhà nghỉ - Khách sạn', value: 6 },
  { key: 7, text: 'Kho xưởng', value: 7 },
  { key: 8, text: 'Trang trại', value: 8 },
  { key: 9, text: 'Phòng trọ', value: 9 },
  { key: 10, text: 'Loại khác', value: 10 },
];
