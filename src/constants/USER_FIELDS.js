export const userFields = [
  'phone_number',
  'name',
  'email',
  'password',
  'date_of_birth',
  'profile_picture',
  'social_id',
  'provided_date',
  'address',
  'role',
  'id',
  'achievement',
  'introduction',
  'introduction_status',
  'position',
  'manager',
  'job_position',
  'maximum_price',
  'minimum_price',
  'project',
  'province',
  'create_product_permission',
  'update_product_permission',
  'chat_permission',
  'alonhadat_name',
  'alonhadat_password',
  'batdongsan_name',
  'batdongsan_password',
];

export const anotherAccountFields = [
  'alonhadat_name',
  'alonhadat_password',
  'batdongsan_name',
  'batdongsan_password',
];

export const userFormFields = [
  [
    {
      label: 'Tên',
      name: 'name',
    },
    {
      label: 'Số CMND',
      name: 'social_id',
    },
  ],
  [
    {
      label: 'Email',
      name: 'email',
    },
    {
      label: 'Ngày sinh',
      name: 'date_of_birth',
    },
  ],

  [
    {
      label: 'Địa chỉ',
      name: 'address',
    },
    {
      label: 'Chức vụ',
      name: 'position',
    },
  ],
  [
    {
      label: 'Thành tích',
      name: 'achievement',
    },
    {
      label: 'Giới thiệu',
      name: 'introduction',
    },
  ],

  [
    {
      label: 'Số điện thoại',
      name: 'phone_number',
      editable: false,
    },
    {
      label: 'Ngày đăng ký',
      name: 'provided_date',
      editable: false,
    },
  ],
  [
    {
      label: 'Phân quyền',
      name: 'role',
      editable: false,
    },
    {
      label: 'Vị trí',
      name: 'job_position',
      editable: false,
    },
  ],
  [
    {
      label: 'Cấp trên',
      name: 'manager',
      editable: false,
    },
    {
      label: 'Giá bất động sản cao nhất',
      name: 'maximum_price',
      editable: false,
    },
  ],
  [
    {
      label: 'Giá bất động sản thấp nhất',
      name: 'minimum_price',
      editable: false,
    },
    {
      label: 'Giới hạn dự án',
      name: 'project',
      editable: false,
    },
  ],
];
