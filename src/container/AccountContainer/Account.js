import React, { useEffect } from 'react';
import { compose } from 'recompose';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Modal as AntModal, Divider, Spin } from 'antd';
import _ from 'lodash';

import * as action from './action';
import { userFormFields } from '../../constants/USER_FIELDS';
import { loadUserProfile, handleUpdateAvatar } from '../AuthContainer/Profile/action';
import { Flexbox, Card, FormInput, Spacer, Button } from '../../components';

const alonhadatFieldProps = [
  {
    label: 'Tên tài khoản',
    name: 'alonhadat_name',
  },
  {
    label: 'Mật khẩu',
    name: 'alonhadat_password',
  },
];
const batdongsanFieldProps = [
  {
    label: 'Tên tài khoản',
    name: 'batdongsan_name',
  },
  {
    label: 'Mật khẩu',
    name: 'batdongsan_password',
  },
];
const { confirm } = AntModal;

const Account = ({ form, fetch, mappingUserProfile, handleChange, updateUserProfile, waiting }) => {
  const { submitting, error } = form;
  const isValid = _.values(error).filter(i => i.length).length === 0;
  const fetchData = async () => {
    await fetch();
    mappingUserProfile();
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <Spin tip="Loading..." spinning={waiting} delay={0}>
        <Flexbox spaceBetween row containerStyle={{ alignItems: 'stretch' }}>
          <Card minWidth="50%">
            <Divider orientation="center">Thông tin tài khoản cá nhân</Divider>
            {userFormFields.map(element => (
              <>
                <Flexbox spaceBetween flexStart row containerStyle={{ alignItems: 'stretch' }}>
                  <FormInput form={form} handleChange={handleChange} {...element[0]} />
                  <Spacer />
                  <FormInput form={form} handleChange={handleChange} {...element[1]} />
                </Flexbox>
              </>
            ))}
          </Card>
          <Card minWidth="50%">
            <Divider orientation="center">Alonhadat.com.vn</Divider>
            <Flexbox spaceBetween flexStart row containerStyle={{ alignItems: 'stretch' }}>
              <FormInput form={form} handleChange={handleChange} {...alonhadatFieldProps[0]} />
              <Spacer />
              <FormInput form={form} handleChange={handleChange} {...alonhadatFieldProps[1]} />
            </Flexbox>
            <Spacer />
            <Divider orientation="center">Batdongsan.com.vn</Divider>
            <Spacer height={25} />
            <Flexbox spaceBetween flexStart row containerStyle={{ alignItems: 'stretch' }}>
              <FormInput form={form} handleChange={handleChange} {...batdongsanFieldProps[0]} />
              <Spacer />
              <FormInput form={form} handleChange={handleChange} {...batdongsanFieldProps[1]} />
            </Flexbox>
          </Card>
        </Flexbox>
        <Card>
          <Button
            text="Cập nhật"
            disabled={!isValid}
            onClick={() => {
              updateUserProfile();
            }}
          />
        </Card>
      </Spin>
    </div>
  );
};

Account.propTypes = {
  form: PropTypes.object,
  fetch: PropTypes.func,
  mappingUserProfile: PropTypes.func,
  handleChange: PropTypes.func,
  user: PropTypes.object,
  updateAvatar: PropTypes.func,
  updateUserProfile: PropTypes.func,
  waiting: PropTypes.bool,
};
Account.defaultProps = {
  form: {},
  fetch: () => {},
  mappingUserProfile: () => {},
  handleChange: () => {},
  updateAvatar: () => {},
  user: {},
  updateUserProfile: () => {},
  waiting: false,
};
export function mapStateToProps(state) {
  return {
    user: state.auth.user.data,
    form: state.anotherAccount.data,
    waiting: state.anotherAccount.data.submitting,
    loading: state.auth.user.fetching,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetch: loadUserProfile,
      updateAvatar: handleUpdateAvatar,
      ...action,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Account);
