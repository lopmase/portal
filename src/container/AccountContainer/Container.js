import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';

import { ROUTES } from '../../constants/ROUTES';
import { PrivateRoute } from '../../components';
import NotFound from '../Common/NotFound';
import AccountContainer from './Account';

const Container = props => {
  return (
    <Switch>
      <PrivateRoute exact path={ROUTES.ACCOUNT} component={AccountContainer} />
      <Route component={NotFound} />
    </Switch>
  );
};

Container.propTypes = {};

export default Container;
