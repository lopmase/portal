import { toast } from 'react-toastify';

import { REDUCERS, ROLE_NAMES, JOB_POSITION } from '../../constants/COMMON';
import { userFields } from '../../constants/USER_FIELDS';
import { generateFormActions } from '../../helpers/formAction';
import { UNKNOWN_ERROR } from '../../constants/MESSAGE';
import { showMessage } from '../Common/GlobalErrorModal/action';

const {
  handleFieldChanged,
  submitDone,
  submitError,
  submitting,
  validateAll,
  waiting,
  waitingDone,
} = generateFormActions(REDUCERS.ANOTHER_ACCOUNT);

const notUpdateField = [
  'provided_date',
  'project',
  'province',
  'minimum_price',
  'maximum_price',
  'create_product_permission',
  'phone_number',
  'role',
  'job_position',
  'manager',
];
export const handleChange = (name, value) => async dispatch => {
  dispatch(handleFieldChanged(name, value));
};
export const mappingUserProfile = () => async (dispatch, getState) => {
  try {
    const response = getState().auth.user.data;
    userFields.forEach(field => {
      if (field === 'id') {
        dispatch(handleFieldChanged('user_id', response[field]));
      } else if (field === 'role') {
        dispatch(handleFieldChanged('role', ROLE_NAMES[response[field]]));
      } else if (field === 'job_position') {
        dispatch(handleFieldChanged('job_position', JOB_POSITION[response[field]]));
      } else if (field === 'minimum_price') {
        if (response.minimum_price === 0 || !response.minimum_price) {
          dispatch(handleFieldChanged('minimum_price', 'Không giới hạn'));
        }
        if (response.maximum_price === 0 || !response.minimum_price) {
          dispatch(handleFieldChanged('maximum_price', 'Không giới hạn'));
        }
        if (
          (response.minimum_price && response.maximum_price !== 0) ||
          (response.maximum_price && response.maximum_price !== 0)
        ) {
          dispatch(handleFieldChanged('maximum_price', response[field]));
        }
      } else {
        dispatch(handleFieldChanged(field, response[field]));
      }
    });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const updateUserProfile = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { data } = getState().anotherAccount.data;
    notUpdateField.map(field => delete data[field]);
    dispatch(submitting());
    const updateProfile = await thunkDependencies.user.updateUser(data);
    dispatch(submitDone(updateProfile.data));
    toast.success('Cập nhật người dùng thành công!');
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};
