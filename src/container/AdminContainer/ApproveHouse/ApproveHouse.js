/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import { compose, lifecycle } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { faCheck, faTimes, faEdit } from '@fortawesome/free-solid-svg-icons';
import { push } from 'connected-react-router';
import moment from 'moment';
import { Modal } from 'antd';

import { Flexbox, Button, Card, Input } from '../../../components';
import './ApproveHouse.scss';
import { REDUCERS } from '../../../constants/COMMON';
import * as actions from './action';
import * as publicActions from './publicActions';

import DataList from '../../Common/DataList/DataList';
import FilterByType from './FilterByType';
import { ROUTES } from '../../../constants/ROUTES';
import HtmlDescription from '../../Common/HtmlDescription/HtmlDescription';

const ApproveHouse = ({
  dataSource,
  publicDataSource,
  webAPI,
  publicAPI,
  pushRoute,
  fetchType
}) => {
  const [visible, setVisible] = useState(false);
  const [condition, setCondition] = useState();
  const [rejectItem, setRejectItem] = useState();
  // const [type, setType] = useState('public');

  const { total } = fetchType === 'public' ? publicDataSource : dataSource;
  const { approve, reject, fetch, setFetchType } = fetchType === 'public' ? publicAPI : webAPI;

  useEffect(() => {
    fetch();
  }, [fetchType]);

  const changeMode = value => {
    setFetchType(value);
  };
  const editCondition = (name, value) => {
    setCondition(value);
  };
  return (
    <div className="approve-page" style={{}}>
      <div className="toolbar">
        <Flexbox
          row
          spaceBetween
          containerStyle={{ marginBottom: 20, marginTop: 20, alignItems: 'center' }}
        >
          <Flexbox>
            <div className="total">{`${total} BĐS`}</div>
          </Flexbox>
          <Flexbox>
            <FilterByType selected={fetchType} onChanged={changeMode} />
          </Flexbox>
        </Flexbox>
      </div>
      <div style={{ marginLeft: 10, marginRight: 10 }}>
        <DataList
          selector={state =>
            state.admin[fetchType === 'public' ? REDUCERS.REVIEWS_PUBLIC : REDUCERS.REVIEWS]
          }
          emptyMessage="Không có bất động sản nào cần duyệt"
          renderItems={item => (
            <Card shadow containerStyle={{ marginBottom: 10 }}>
              <Flexbox row spaceBetween>
                <div className="info">
                  <div className="id">{`ID: ${fetchType === 'public' ? item.id : item.house.id}`}</div>
                  <div>{fetchType === 'public' ? item.title : item.house.title}</div>
                  <div>
                    Ngày cập nhật:
                    {` ${
                      fetchType === 'public'
                        ? moment(item.updated_at * 1000).format('DD/MM/YYYY HH:mm:ss')
                        : moment(item.house.updated_at * 1000).format('DD/MM/YYYY HH:mm:ss')
                    }`}
                  </div>
                  <HtmlDescription
                    description={fetchType === 'public' ? item.description : item.house.description}
                  />
                </div>
                <Flexbox row>
                  <Button
                    icon={faEdit}
                    iconColor="white"
                    backgroundColor="lightgray"
                    onClick={() =>
                      pushRoute(
                        `${ROUTES.HOUSE_ROOT}/edit/${fetchType === 'public' ? item.id : item.house.id}`,
                        { goBackApprove: true },
                      )
                    }
                    iconOnly
                    containerStyle={{ marginRight: 10 }}
                  />
                  <Button
                    icon={faCheck}
                    iconColor="white"
                    backgroundColor="green"
                    onClick={() => approve(item)}
                    iconOnly
                    containerStyle={{ marginRight: 10 }}
                  />
                  <Button
                    icon={faTimes}
                    iconColor="white"
                    backgroundColor="red"
                    onClick={() => {
                      setVisible(true);
                      setRejectItem(item);
                    }}
                    iconOnly
                  />
                </Flexbox>
              </Flexbox>
            </Card>
          )}
        />
      </div>
      <Modal
        title="Lý do"
        visible={visible}
        onOk={() => {
          reject({ id: rejectItem, condition });
          setVisible(false);
          setCondition(null);
        }}
        onCancel={() => setVisible(false)}
      >
        <Input multiline value={condition} onChange={editCondition} />
      </Modal>
    </div>
  );
};

export const mapStateToProps = state => ({
  dataSource: state.admin[REDUCERS.REVIEWS],
  publicDataSource: state.admin[REDUCERS.REVIEWS_PUBLIC],
  fetchType: state.admin.fetchTypeHouseApprove.type,
});

export function mapDispatchToProps(dispatch) {
  return {
    publicAPI: bindActionCreators(
      {
        ...publicActions,
      },
      dispatch,
    ),
    webAPI: bindActionCreators(
      {
        ...actions,
      },
      dispatch,
    ),
    pushRoute: bindActionCreators(push, dispatch),
  };
}

export const useLifeCycle = lifecycle({
  componentDidMount() {
    // this.props.fetch();
  },
});

const connetToRedux = connect(mapStateToProps, mapDispatchToProps);
export default compose(connetToRedux, useLifeCycle, withRouter)(ApproveHouse);
