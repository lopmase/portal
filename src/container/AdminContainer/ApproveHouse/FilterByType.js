import React from 'react';
import PropTypes from 'prop-types';

import { Dropdown, Flexbox } from '../../../components';

const TYPES = [
  {
    value: 'public',
    text: 'Cộng đồng',
  },
  {
    value: 'web',
    text: 'Web',
  },
];

const styleProps = {
  containerStyle: {
    minWidth: 120,
    borderRadius: 18,
    paddingTop: 8,
    paddingBottom: 8,
  },
};

const FilterByType = ({ selected, onChanged }) => (
  <div className="page-size" style={{ marginRight: 10 }}>
    <Flexbox row>
      <Dropdown
        {...styleProps}
        items={TYPES}
        selected={selected}
        onChanged={({ item }) => onChanged(item.value)}
      />
    </Flexbox>
  </div>
);

FilterByType.propTypes = {
  selected: PropTypes.any.isRequired,
  onChanged: PropTypes.func.isRequired,
};

FilterByType.defaultProps = {};

export default FilterByType;
