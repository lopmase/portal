import { toast } from 'react-toastify';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { generateModalActions } from '../../../helpers/modalAction';
import { generateDatalistActions } from '../../../helpers/dataListAction';

const { fetchDone, fetching, fetchError } = generateDatalistActions(REDUCERS.REVIEWS);

const { openModal, closeModal } = generateModalActions(REDUCERS.GLOBAL_LOADING);

export const fetch = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { page } = getState().blog[REDUCERS.BLOGS];
    dispatch(fetching());
    const response = await thunkDependencies.house.reviews({
      page,
      size: 100,
    });
    const result = apiSerializer(response);

    dispatch(fetchDone(result));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;

    dispatch(fetchError(message));
  }
};

export const approve = ({ id }) => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(openModal());
    const response = await thunkDependencies.house.approve({ preview_id: id });
    apiSerializer(response);
    dispatch(closeModal());
    dispatch(fetch());
    toast.success('BĐS được upload lên web thành công!');
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());

    dispatch(showMessage(message));
  }
};

export const reject = ({ id, condition }) => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(openModal());
    const response = await thunkDependencies.house.reject({ preview_id: id, condition });
    apiSerializer(response);
    dispatch(closeModal());
    dispatch(fetch());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());

    dispatch(showMessage(message));
  }
};

export const setFetchType = type => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch({ type: 'SET_TYPE_FETCH_APPROVE', payload: type });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};
