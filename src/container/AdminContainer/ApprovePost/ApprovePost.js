/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import { compose, lifecycle } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { faCheck, faTimes, faEdit } from '@fortawesome/free-solid-svg-icons';
import { push } from 'connected-react-router';
import moment from 'moment';
import { Modal } from 'antd';

import { Flexbox, Button, Card, Input } from '../../../components';
// import './ApproveHouse.scss';
import { REDUCERS } from '../../../constants/COMMON';
import * as action from './action';
import * as knowLedgeAction from '../../KnowledgeContainer/Knowledge/components/action';
import ModalCreate from '../../KnowledgeContainer/Knowledge/components/Modal';
import DataList from '../../Common/DataList/DataList';
import FilterByType from './FilterByType';
import { ROUTES } from '../../../constants/ROUTES';
import HtmlDescription from '../../Common/HtmlDescription/HtmlDescription';

const ApprovePost = ({
  total,
  pushRoute,
  typeKnowledge,
  knowledgeList,
  fetch,
  fetchKnowledgeById,
  selected,
  setFetchType,
  approve,
  reject,
  setType,
  type,
  setTypeCreate,
  typeCreate,
  create,
  setDataDetail,
  setEditMode,
}) => {
  const [visible, setVisible] = useState(false);
  const [reason, setReason] = useState();
  const [rejectItem, setRejectItem] = useState();
  const [isModalVisible, setIsModalVisible] = useState(false);

  useEffect(() => {
    fetch();
  }, []);

  useEffect(() => {
    if (selected !== null) {
      fetchKnowledgeById(selected);
    }
  }, [selected]);

  const changeMode = value => {
    setFetchType(value);
  };
  const editReason = (name, value) => {
    console.log('reason', value);
    setReason(value);
  };
  const showModal = (e, id) => {
    setTypeCreate(id);
    setType(e.target.name);
    setIsModalVisible(true);
  };

  const editPost = item => {
    setEditMode('post', 'waitingApprove');
    setDataDetail(item);
    setIsModalVisible(true);
  };

  return (
    <div className="approve-page" style={{}}>
      <div className="toolbar">
        <button className='btn-hover color-green' name="column" onClick={e => showModal(e, null)}>
          Tạo cột
        </button>
        <Flexbox
          row
          spaceBetween
          containerStyle={{ marginBottom: 20, marginTop: 20, alignItems: 'center' }}
        >
          <Flexbox>
            <div className="total">{`${total} bài viết`}</div>
          </Flexbox>
          <Flexbox>
            <FilterByType type={typeKnowledge} selected={selected} onChanged={changeMode} />
          </Flexbox>
        </Flexbox>
      </div>
      <div style={{ marginLeft: 10, marginRight: 10 }}>
        <DataList
          selector={state => state.admin.fetchTypePost}
          emptyMessage="Không có bài viết nào cần duyệt"
          renderItems={item => (
            <Card shadow containerStyle={{ marginBottom: 10 }}>
              <Flexbox row spaceBetween>
                <div className="info">
                  <div className="id">{`ID: ${item.id}`}</div>
                  <div>Người đăng: {item.user.name}</div>
                  <div>Tiêu đề: {item.title}</div>
                  {/* <div>
                    Ngày cập nhật:
                    {` ${
                      fetchType === 'public'
                        ? moment(item.updated_at * 1000).format('DD/MM/YYYY HH:mm:ss')
                        : moment(item.house.updated_at * 1000).format('DD/MM/YYYY HH:mm:ss')
                    }`}
                  </div> */}
                  {/* <HtmlDescription
                    description={fetchType === 'public' ? item.description : item.house.description}
                  /> */}
                </div>
                <Flexbox row>
                  <Button
                    icon={faEdit}
                    iconColor="white"
                    backgroundColor="lightgray"
                    onClick={() => editPost(item)}
                    iconOnly
                    containerStyle={{ marginRight: 10 }}
                  />
                  <Button
                    icon={faCheck}
                    iconColor="white"
                    backgroundColor="green"
                    onClick={() => approve(item.id)}
                    iconOnly
                    containerStyle={{ marginRight: 10 }}
                  />
                  <Button
                    icon={faTimes}
                    iconColor="white"
                    backgroundColor="red"
                    onClick={() => {
                      setVisible(true);
                      setRejectItem(item);
                    }}
                    iconOnly
                  />
                </Flexbox>
              </Flexbox>
            </Card>
          )}
        />
      </div>
      <Modal
        title="Lý do"
        visible={visible}
        onOk={() => {
          reject({ id: rejectItem.id, reason_reject: reason });
          setVisible(false);
          setReason(null);
        }}
        onCancel={() => setVisible(false)}
      >
        <Input multiline value={reason} onChange={editReason} />
      </Modal>

      <ModalCreate
        typeCreate={typeCreate}
        type={type}
        setIsModalVisible={setIsModalVisible}
        isModalVisible={isModalVisible}
        setType={setType}
      />
    </div>
  );
};

export function mapStateToProps(state) {
  return {
    selected: state.admin.fetchTypePost.fetchType,
    total: state.admin.fetchTypePost.total,
    typeKnowledge: state.Knowledge.typeKnowledge.typeList,
    knowledgeList: state.admin.fetchTypePost.list,
    type: state.Knowledge.knowledgeList.type,
    typeCreate: state.Knowledge.typeKnowledge.typeCreate,
    state,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...action,
      ...knowLedgeAction,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(ApprovePost);
