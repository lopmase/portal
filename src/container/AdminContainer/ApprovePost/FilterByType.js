import React from 'react';
import PropTypes from 'prop-types';

import { Dropdown, Flexbox } from '../../../components';

const styleProps = {
  containerStyle: {
    minWidth: 120,
    borderRadius: 18,
    paddingTop: 8,
    paddingBottom: 8,
  },
};

const FilterByType = ({ selected, onChanged, type }) => {
  console.log('selectedFilter', selected);
  return (
    <div className="page-size" style={{ marginRight: 10 }}>
      <Flexbox row>
        <Dropdown
          {...styleProps}
          items={
            type.length
              ? type.map(item => ({
                  value: item.id,
                  text: item.type_name,
                }))
              : []
          }
          selected={selected}
          onChanged={({ item }) => onChanged(item.value)}
        />
      </Flexbox>
    </div>
  );
};

FilterByType.propTypes = {
  selected: PropTypes.any.isRequired,
  onChanged: PropTypes.func.isRequired,
};

FilterByType.defaultProps = {
  // eslint-disable-next-line react/default-props-match-prop-types
  type: [],
};

export default FilterByType;
