import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { toast } from 'react-toastify';
import { fetchKnowledge } from '../../KnowledgeContainer/Knowledge/components/action';
export const fetch = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const res = await thunkDependencies.knowledge.getType();
    dispatch({
      type: REDUCERS.TYPE,
      payload: res.data,
    });
    dispatch({
      type: 'SET_FETCH_TYPE_POST',
      payload: res.data[0].id,
    });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const fetchKnowledgeById = id => async (dispatch, getState, thunkDependencies) => {
  try {
    const res = await thunkDependencies.knowledge.getKnowledgeByTypeId(id);
    dispatch({
      type: 'SET_LIST_POST',
      list: res.data.data,
      total: res.data.total,
    });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const setFetchType = type => async (dispatch, getState, thunkDependencies) => {
  try {
    // const res = await thunkDependencies.knowledge.getKnowledge();
    dispatch({
      type: 'SET_FETCH_TYPE_POST',
      payload: type,
    });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const approve = id => async (dispatch, getState, thunkDependencies) => {
  const fetchId = getState().admin.fetchTypePost.fetchType;
  try {
    const res = await thunkDependencies.knowledge.approve(id);
    // dispatch({
    //   type: 'SET_FETCH_TYPE_POST',
    //   payload: type,
    // });
    toast.success('Bài viết đã được đăng');
    dispatch(fetchKnowledgeById(fetchId));
    dispatch(fetchKnowledge());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const reject = data => async (dispatch, getState, thunkDependencies) => {
  const fetchId = getState().admin.fetchTypePost.fetchType;
  try {
    const res = await thunkDependencies.knowledge.reject(data);
    // dispatch({
    //   type: 'SET_FETCH_TYPE_POST',
    //   payload: type,
    // });
    toast.success('Đã cập nhập');
    dispatch(fetchKnowledgeById(fetchId));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};
