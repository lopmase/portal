const initialFetchType = {
  fetchType: null,
  list: [],
  total: 0,
  fetching: false,
  message: '',
};

export const fetchTypePostReducer = (state = initialFetchType, action) => {
  switch (action.type) {
    case 'SET_FETCH_TYPE_POST':
      return {
        ...state,
        fetchType: action.payload,
      };
    case 'SET_LIST_POST':
      return {
        ...state,
        list: action.list,
        total: action.total,
      };
    default:
      return state;
  }
};
