/* eslint-disable react/no-did-update-set-state */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { ROUTES } from '../../constants/ROUTES';
import UserContainer from '../UserConntainer/Container';
import { Flexbox } from '../../components';
import Notification from '../ShellContainner/Header/Notification';
import './Style.scss';
import ApproveHouse from './ApproveHouse/ApproveHouse';
import ApprovePost from './ApprovePost/ApprovePost';
import TabRouter from '../../components/Tabs/Tabs';
import Layout from '../ShellContainner/Layout/Layout';
import Dictionary from './Dictionary';
import Tracking from './Tracking/Tracking';
import { isSuperAdmin } from '../../helpers/roleUtil';

const TAB_ROUTES = [
  {
    label: 'Người dùng',
    route: ROUTES.USERS,
  },
  {
    label: 'Từ điển',
    route: ROUTES.DICTIONARY,
  },
  {
    label: 'Duyệt BĐS',
    route: ROUTES.APPROVE_HOUSE,
  },
  {
    label: 'Duyệt bài viết',
    route: ROUTES.APPROVE_POST,
  },
];

class AdminContainer extends Component {
  state = {
    tabs: [...TAB_ROUTES],
  };

  componentDidMount() {
    const { user } = this.props;
    if (user && isSuperAdmin(user)) {
      this.setState({ tabs: [...TAB_ROUTES, { label: 'Bảo mật', route: ROUTES.TRACKING }] });
    }
  }

  componentDidUpdate(prevProp) {
    const { user } = this.props;
    const { tabs } = this.state;

    if (user && prevProp.user !== user && isSuperAdmin(user)) {
      this.setState({ tabs: [...tabs, { label: 'Bảo mật', route: ROUTES.TRACKING }] });
    }
  }

  render() {
    const { tabs } = this.state;

    return (
      <div className="admin-container">
        <Layout
          renderHeader={() => (
            <>
              <Flexbox row spaceBetween>
                <h1 style={{ marginBottom: 10 }}>Quản Trị</h1>
                <Notification />
              </Flexbox>
              <Flexbox row spaceBetween>
                <TabRouter tabs={tabs} />
                <div />
              </Flexbox>
            </>
          )}
        >
          <Switch>
            <Route path={ROUTES.USER_ROOT} component={UserContainer} />
            <Route path={ROUTES.DICTIONARY} component={Dictionary} />
            <Route exact path={ROUTES.APPROVE_POST} component={ApprovePost} />
            <Route path={ROUTES.APPROVE_HOUSE} component={ApproveHouse} />
            <Route path={ROUTES.TRACKING} component={Tracking} />
            <Redirect to={ROUTES.USERS} />
          </Switch>
        </Layout>
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    user: state.auth.user.data,
  };
}
export default connect(mapStateToProps, {})(AdminContainer);
