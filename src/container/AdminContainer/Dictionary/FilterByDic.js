import React from 'react';
import PropTypes from 'prop-types';

import { Dropdown, Flexbox } from '../../../components';
import { DICTIONARY_OPTIONS } from '../constants';

const styleProps = {
  // clear: true,
  containerStyle: {
    minWidth: 120,
    borderRadius: 18,
    paddingTop: 8,
    paddingBottom: 8,
  },
};

const RoleNameDropDown = ({ selected, onChanged }) => (
  <div className="page-size" style={{ marginRight: 10 }}>
    <Flexbox row>
      <div style={{ fontSize: 13, fontWeight: 'bold', marginRight: 10 }}>Loại từ điển:</div>
      <Dropdown
        {...styleProps}
        items={DICTIONARY_OPTIONS}
        selected={selected}
        onChanged={({ item }) => onChanged(item.value)}
      />
    </Flexbox>
  </div>
);

RoleNameDropDown.propTypes = {
  selected: PropTypes.any.isRequired,
  onChanged: PropTypes.func.isRequired,
};

RoleNameDropDown.defaultProps = {};

export default RoleNameDropDown;
