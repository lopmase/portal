import React from 'react';
import PropTypes from 'prop-types';

import { Dropdown, Flexbox } from '../../../components';
import MetadataContainer from './MetadataContainer';

const styleProps = {
  // clear: true,
  containerStyle: {
    minWidth: 120,
    borderRadius: 18,
    paddingTop: 8,
    paddingBottom: 8,
  },
};

const RoleNameDropDown = ({ selected, onChanged }) => (
  <div className="page-size" style={{ marginRight: 10 }}>
    <Flexbox row>
      <div style={{ fontSize: 13, fontWeight: 'bold', marginRight: 10 }}>Chọn Quận/Huyện:</div>
      <MetadataContainer
        render={options => {
          const { province } = options;
          return (
            <Dropdown
              {...styleProps}
              items={province.map((text, index) => ({ value: index + 1, text }))}
              selected={selected.value}
              onChanged={({ item }) => onChanged(item)}
            />
          );
        }}
      />
    </Flexbox>
  </div>
);

RoleNameDropDown.propTypes = {
  selected: PropTypes.any.isRequired,
  onChanged: PropTypes.func.isRequired,
};

RoleNameDropDown.defaultProps = {};

export default RoleNameDropDown;
