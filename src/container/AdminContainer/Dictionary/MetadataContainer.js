import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { UserSelector } from '../Selector';

const MetadataContainer = ({ options, render }) => {
  return <div>{options ? render(options) : 'Loading...'}</div>;
};

MetadataContainer.propTypes = {
  options: PropTypes.object.isRequired,
  render: PropTypes.func.isRequired,
};

MetadataContainer.defaultProps = {};

export function mapStateToProps(state) {
  return {
    options: UserSelector.options(state).data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(MetadataContainer);
