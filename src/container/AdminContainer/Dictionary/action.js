import { toast } from 'react-toastify';
import { message } from 'antd';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { generateModalActions } from '../../../helpers/modalAction';
import { generateDatalistActions } from '../../../helpers/dataListAction';
import { UserSelector } from '../Selector';

const { fetchDone, fetching, fetchError } = generateDatalistActions(REDUCERS.DICTIONARY);
const { openModal: openGlobalLoading, closeModal: closeGlobalLoading } = generateModalActions(
  REDUCERS.GLOBAL_LOADING,
);

export const fetch = (type, districtId) => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const response = await thunkDependencies.metadata.getDictionary(type, districtId);
    const result = apiSerializer(response);
    dispatch(fetchDone(result));
  } catch (error) {
    const errorMessage = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(errorMessage));
  }
};

const findDictionaryByValue = (value, state, id) => {
  const dictionaryItems = UserSelector.dictionary(state).list;
  if (id) {
    return dictionaryItems.find(item => item.value === value && item.id !== id);
  }
  return dictionaryItems.find(item => item.value === value);
};

export const createDictionary = (type, data, districtId) => async (
  dispatch,
  getState,
  thunkDependencies,
) => {
  try {
    if (findDictionaryByValue(data, getState())) {
      message.error('Từ điển này đã tồn tại. Vui lòng tạo tên khác');
      return false;
    }
    dispatch(openGlobalLoading());
    const response = await thunkDependencies.metadata.newDictionary(type, data, districtId);
    const result = apiSerializer(response);
    dispatch(closeGlobalLoading(result));
    dispatch(fetch(type, districtId));
    toast.success('Tạo từ điển thành công!');
    return true;
  } catch (error) {
    const errorMessage = error.message || UNKNOWN_ERROR;
    dispatch(closeGlobalLoading());
    dispatch(showMessage(errorMessage));
    return false;
  }
};
export const deleteDictionary = (type, id, districtId) => async (
  dispatch,
  getState,
  thunkDependencies,
) => {
  try {
    dispatch(openGlobalLoading());
    await thunkDependencies.metadata.deleteDictionary(type, id);
    dispatch(closeGlobalLoading());
    dispatch(fetch(type, districtId));
    toast.success('Xoá từ điển thành công');
  } catch (error) {
    const errorMessage = error.message || UNKNOWN_ERROR;
    dispatch(closeGlobalLoading());
    dispatch(showMessage(errorMessage));
  }
};

export const updateDictionary = (type, id, value, districtId) => async (
  dispatch,
  getState,
  thunkDependencies,
) => {
  try {
    const existedItem = findDictionaryByValue(value, getState(), id);
    if (existedItem) {
      message.error('Từ điển này đã tồn tại. Vui lòng tạo tên khác');
      return false;
    }
    dispatch(openGlobalLoading());
    await thunkDependencies.metadata.updateDictionary(type, id, value, districtId);
    dispatch(closeGlobalLoading());
    dispatch(fetch(type, districtId));
    toast.success('Cập nhật từ điển thành công');
    return true;
  } catch (error) {
    const errorMessage = error.message || UNKNOWN_ERROR;
    dispatch(closeGlobalLoading());
    dispatch(showMessage(errorMessage));
    return false;
  }
};
