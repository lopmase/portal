import moment from 'moment';

export const columns = [
  {
    header: '#',
    key: 'id',
  },
  {
    header: 'GIÁ TRỊ',
    key: 'value',
  },
  {
    header: 'NGÀY TẠO',
    key: 'created_at',
    accessor: housetype => moment(housetype.created_at * 1000).format('DD/MM/YYYY'),
  },
  {
    header: 'NGÀY CẬP NHẬT',
    key: 'updated_at',
    accessor: housetype => moment(housetype.updated_at * 1000).format('DD/MM/YYYY'),
  },
  {
    header: 'SỬA/XÓA',
    key: 'action',
  },
];
