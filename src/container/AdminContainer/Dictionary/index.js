/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Modal, Menu, Dropdown } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faPenAlt, faPlus } from '@fortawesome/free-solid-svg-icons';
import { Flexbox, Table, Input, Button } from '../../../components';
import './Dictionary.scss';
import * as actions from './action';
import FilterByDic from './FilterByDic';
import FilterByDistrict from './FilterByDistrict';
import { columns } from './columns';
import { confirm } from '../../ShellContainner/ConfirmDialog/action';
import { UserSelector } from '../Selector';
import { DELETE_DICTIONARY_CONFIRM } from '../../../constants/MESSAGE';
import { DICTIONARY_OPTIONS } from '../constants';
import { DICTIONARY_TYPE, REDUCERS } from '../../../constants/COMMON';
import { getOptions } from '../../HouseContainer/Metadata/optionsAction';

const ActionMenu = ({ onClick }) => (
  <Menu onClick={onClick}>
    <Menu.Item key="edit">
      <FontAwesomeIcon icon={faPenAlt} style={{ marginRight: 10 }} />
      Sửa
    </Menu.Item>
    <Menu.Item key="del">
      <FontAwesomeIcon style={{ marginRight: 10 }} icon={faTrashAlt} />
      Xoá
    </Menu.Item>
  </Menu>
);

const Dictionary = ({
  dataSource: { list, fetching, total },
  openConfirmDialog,
  createDictionary,
  deleteDictionary,
  updateDictionary,
  fetch,
  fetchOptions,
}) => {
  const [dicType, setDicType] = useState(DICTIONARY_OPTIONS[0].value);
  const [visible, setVisible] = useState(false);
  const [editing, setEditing] = useState(false);
  const [itemEditId, setItemEditId] = useState('');
  const [newDictValue, setNewDictValue] = useState('');
  const [editedValue, setEditedValue] = useState('');
  const [selectedDistrict, setSelectedDistrict] = useState({ value: 1, text: 'Vũng Tàu' });
  const label = DICTIONARY_TYPE[dicType];
  const onAdd = () => {
    setVisible(true);
  };

  const onUserInput = (name, value) => {
    !editing ? setNewDictValue(value) : setEditedValue(value);
  };
  const onDelete = id => {
    openConfirmDialog(DELETE_DICTIONARY_CONFIRM, result => {
      if (result) {
        deleteDictionary(dicType, id, selectedDistrict.value);
      }
    });
  };
  const onOK = async () => {
    if (!editing) {
      const success = await createDictionary(dicType, newDictValue, selectedDistrict.value);
      if (success) {
        setNewDictValue('');
        setVisible(false);
      }
    } else {
      const success = await updateDictionary(
        dicType,
        itemEditId,
        editedValue,
        selectedDistrict.value,
      );
      if (success) {
        setEditing(false);
        setEditedValue('');
        setItemEditId('');
      }
      setVisible(false);
    }
  };
  const onCancel = () => {
    setVisible(false);
    setEditing(false);
    setNewDictValue('');
    setEditedValue('');
  };
  const onActionClick = (key, rowData) => {
    if (key === 'del') {
      onDelete(rowData.id);
    } else if (key === 'edit') {
      setVisible(true);
      setEditing(true);
      setItemEditId(rowData.id);
      setEditedValue(rowData.value);
    }
  };

  const renderCell = ({ item, column }) => {
    const cell = item[column.key];
    let value = '';
    if (column.key === 'action') {
      return (
        <td>
          <Dropdown
            trigger="click"
            overlay={<ActionMenu onClick={({ key }) => onActionClick(key, item)} />}
          >
            <Button text="Chọn" clear />
          </Dropdown>
        </td>
      );
    }
    if (column.accessor) {
      value = column.accessor(item, {});
    } else {
      value = cell ? cell.toString() : '';
    }
    return <td className={column.key === 'id' ? 'bold-text' : ''}>{value}</td>;
  };

  useEffect(() => {
    fetch(dicType, selectedDistrict.value);
    if (dicType === 'district') {
      fetchOptions();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dicType]);

  useEffect(() => {
    fetch(dicType, selectedDistrict.value);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedDistrict]);

  return (
    <div className="users-page" style={{}}>
      <div className="toolbar">
        <Flexbox
          row
          spaceBetween
          containerStyle={{ marginBottom: 20, marginTop: 20, alignItems: 'center' }}
        >
          <div className="total">{`Tổng cộng: ${total} loại ${label}`}</div>
          <Flexbox>
            <FilterByDic selected={dicType} onChanged={setDicType} />
            {dicType === 'district' && (
              <FilterByDistrict selected={selectedDistrict} onChanged={setSelectedDistrict} />
            )}
            <Button
              containerStyle={{ paddingRight: 14, paddingLeft: 14 }}
              icon={faPlus}
              iconColor="white"
              iconOnly
              onClick={onAdd}
            />
          </Flexbox>
        </Flexbox>
      </div>
      <Table loading={fetching} selectable data={list} columns={columns} renderCell={renderCell} />
      <Modal
        title={
          !editing
            ? `Thêm ${label} ${
                dicType === 'district' ? ` cho Quận/Huyện: ${selectedDistrict.text}` : ''
              }`
            : `Sửa ${label} ${
                dicType === 'district' ? ` cho Quận/Huyện: ${selectedDistrict.text}` : ''
              }`
        }
        visible={visible}
        onOk={onOK}
        onCancel={onCancel}
      >
        <Input value={!editing ? newDictValue : editedValue} onChange={onUserInput} />
      </Modal>
    </div>
  );
};

export const mapStateToProps = state => ({
  dataSource: UserSelector.dictionary(state),
  options: state.metadata[REDUCERS.OPTIONS].data,
});

const mapDispatchToProps = {
  ...actions,
  fetchOptions: getOptions,
  openConfirmDialog: confirm,
};

const connetToRedux = connect(mapStateToProps, mapDispatchToProps);
export default compose(connetToRedux, withRouter)(Dictionary);
