import { combineReducers } from 'redux';
import createDatalistReducer from '../../helpers/createDataListReducer';
import { REDUCERS } from '../../constants/COMMON';
import { fetchTypePostReducer } from './ApprovePost/reducer';

const initialFetchType = {
  type: 'public',
};

export const fetchTypeReducer = (state = initialFetchType, action) => {
  switch (action.type) {
    case 'SET_TYPE_FETCH_APPROVE':
      console.log('action', action.payload);
      return {
        ...state,
        type: action.payload,
      };
    default:
      return state;
  }
};

export default combineReducers({
  [REDUCERS.REVIEWS]: createDatalistReducer(REDUCERS.REVIEWS, { pageSize: 20 }),
  [REDUCERS.REVIEWS_PUBLIC]: createDatalistReducer(REDUCERS.REVIEWS_PUBLIC, { pageSize: 20 }),
  [REDUCERS.DICTIONARY]: createDatalistReducer(REDUCERS.DICTIONARY, { pageSize: 1000 }),
  [REDUCERS.TRACKING]: createDatalistReducer(REDUCERS.TRACKING, { pageSize: 100 }),
  fetchTypeHouseApprove: fetchTypeReducer,
  // fetchTypePost: createDatalistReducer(REDUCERS.KNOWLEDGE_LIST, { pageSize: 20 }),
  fetchTypePost: fetchTypePostReducer,
});
