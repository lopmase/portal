import { REDUCERS } from '../../constants/COMMON';

export const UserSelector = {
  users: state => state.user[REDUCERS.USERS],
  friends: state => state.social.zalo.friends,
  options: state => state.metadata[REDUCERS.OPTIONS],
  dictionary: state => state.admin[REDUCERS.DICTIONARY],
};
