/* eslint-disable react/no-array-index-key */
/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table } from 'antd';

import { Flexbox } from '../../../components';
import './Tracking.scss';
import { REDUCERS } from '../../../constants/COMMON';
import * as actions from './action';
import { columns } from './columns';

const Tracking = ({ dataSource, fetch }) => {
  const { total, list, fetching } = dataSource;

  useEffect(() => {
    fetch();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="approve-page" style={{}}>
      <div className="toolbar">
        <Flexbox
          row
          spaceBetween
          containerStyle={{ marginBottom: 20, marginTop: 20, alignItems: 'center' }}
        >
          <Flexbox>
            <div className="total">{`${total} Lịch sử`}</div>
          </Flexbox>
          <Flexbox>{/* <FilterByType selected={type} onChanged={changeMode} /> */}</Flexbox>
        </Flexbox>
      </div>
      <div style={{ marginLeft: 10, marginRight: 10 }}>
        <Table
          size="small"
          pagination={false}
          rowKey={item => item.user_id}
          loading={fetching}
          columns={columns}
          dataSource={list}
        />
      </div>
    </div>
  );
};

export const mapStateToProps = state => ({
  dataSource: state.admin[REDUCERS.TRACKING],
});

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
    },
    dispatch,
  );
}

const connetToRedux = connect(mapStateToProps, mapDispatchToProps);
export default compose(connetToRedux, withRouter)(Tracking);
