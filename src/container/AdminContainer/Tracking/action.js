import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateDatalistActions } from '../../../helpers/dataListAction';
import { getAllUserLocations } from '../../../apis/userApi';

const { fetchDone, fetching, fetchError } = generateDatalistActions(REDUCERS.TRACKING);

export const fetch = () => async (dispatch, getState) => {
  try {
    const { page, pageSize } = getState().admin[REDUCERS.TRACKING];
    dispatch(fetching());
    const result = await getAllUserLocations({
      page,
      size: pageSize,
    });

    dispatch(fetchDone(result));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;

    dispatch(fetchError(message));
  }
};
