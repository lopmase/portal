import React from 'react';
import moment from 'moment';
import { Avatar } from 'antd';
import { formatImageUrl } from '../../../helpers/common';
import UserTrackingDetail from './components/UserTrackingDetail';
import LocationCell from './components/LocationCell';

export const columns = [
  {
    title: 'Nhân viên',
    dataIndex: 'user',
    render: user => (
      <div>
        {user && user.avatar && (
          <Avatar
            style={{ marginRight: 10 }}
            size="small"
            src={formatImageUrl(user.avatar.thumbnail)}
          />
        )}
        {user && user.name}
      </div>
    ),
  },
  {
    title: 'Số lần',
    dataIndex: 'location_count',
    render: value => `${value} lần`,
  },
  {
    title: 'IP Address',
    dataIndex: 'ip_address',
    render: (value, item) => item.latest_info.ip_address,
  },
  {
    title: 'Địa điểm',
    dataIndex: 'location',
    render: (value, item) => <LocationCell {...item.latest_info.location} />,
  },
  {
    title: 'Thiết bị',
    dataIndex: 'device_type',
    render: (value, item) => item.latest_info.device_type,
  },
  {
    title: 'Lần đăng nhập gần nhất',
    dataIndex: 'last_login',
    render: value => moment(value * 1000).format('DD-MM-YYYY hh:mm:ssa'),
  },
  {
    title: 'Thêm',
    dataIndex: 'action',
    render: (value, item) => <UserTrackingDetail user={item.user} />,
  },
];
