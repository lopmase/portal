import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';
import { useDispatch } from 'react-redux';
import { showMessage } from '../../../Common/GlobalErrorModal/action';
import { getAddressFromCoordinate } from '../../../../helpers/location';

const LocationCell = ({ latitude, longitude, address }) => {
  const [loading, setLoading] = React.useState(false);
  const [data, setData] = React.useState('');
  const dispatch = useDispatch();
  const noAddress = !data && !address && latitude;

  const getDetail = async () => {
    try {
      setLoading(true);
      const rs = await getAddressFromCoordinate(latitude, longitude);
      if (rs) {
        setData(rs);
      }
    } catch (error) {
      dispatch(showMessage(error.message || error));
    } finally {
      setLoading(false);
    }
  };

  return (
    <div>
      <span>
        {data ||
          address ||
          (latitude ? (
            `[lat: ${latitude}, long: ${longitude}]`
          ) : (
            <span className="gray-text">Chưa cấp quyền</span>
          ))}
      </span>
      {noAddress && <Button icon="search" size="small" onClick={getDetail} loading={loading} />}
    </div>
  );
};

LocationCell.propTypes = {
  latitude: PropTypes.string.isRequired,
  longitude: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
};

export default LocationCell;
