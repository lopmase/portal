import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { Button, Modal, Table, Pagination } from 'antd';
import { useDispatch } from 'react-redux';
import { showMessage } from '../../../Common/GlobalErrorModal/action';
import { getLocationsById } from '../../../../apis/userApi';
import LocationCell from './LocationCell';

export const columns = [
  {
    title: '#',
    dataIndex: 'id',
  },
  {
    title: 'IP Address',
    dataIndex: 'ip_address',
  },
  {
    title: 'Địa điểm',
    dataIndex: 'location',
    render: (value, item) => <LocationCell {...item.location} />,
  },
  {
    title: 'Thiết bị',
    dataIndex: 'device_type',
    render: (value, item) => item.device_type,
  },
  {
    title: 'Thời gian',
    dataIndex: 'created_at',
    render: value => moment(value * 1000).format('DD-MM-YYYY hh:mm:ssa'),
  },
];

const PAGE_SIZE = 10;

const UserTrackingDetail = ({ user }) => {
  const [visible, setVisible] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [data, setData] = React.useState([]);
  const [count, setCount] = React.useState(0);
  const [page, setPage] = React.useState(1);
  const dispatch = useDispatch();

  const getDetail = async () => {
    try {
      setLoading(true);
      const rs = await getLocationsById({ user_id: user.id, page, size: PAGE_SIZE });
      if (rs) {
        setData(rs.data);
        setCount(rs.total);
      }
    } catch (error) {
      dispatch(showMessage(error.message || error));
    } finally {
      setLoading(false);
    }
  };

  React.useEffect(() => {
    if (visible) {
      getDetail();
    }

    return () => {
      setPage(1);
    };
  }, [visible]);

  React.useEffect(() => {
    if (data.length > 0) {
      getDetail();
    }
  }, [page]);

  return (
    <div>
      <Button type="link" onClick={() => setVisible(true)}>
        Chi tiết
      </Button>
      <Modal
        width={900}
        onCancel={() => setVisible(false)}
        visible={visible}
        title={`Lịch sử đăng nhập của ${user && user.name}`}
      >
        {/* <Row>
          <Col xs={24} md={16}> */}
        <h3>{`Tổng ${count}`}</h3>
        <Table
          loading={loading}
          pagination={false}
          columns={columns}
          dataSource={data}
          size="small"
        />
        <Pagination
          style={{ marginTop: 10 }}
          total={count}
          pageSize={PAGE_SIZE}
          current={page}
          onChange={newPage => {
            setPage(newPage);
          }}
        />
        {/* </Col>
          <Col xs={0} md={8}>
            map
          </Col>
        </Row> */}
      </Modal>
    </div>
  );
};

UserTrackingDetail.propTypes = {
  user: PropTypes.object.isRequired,
};

export default UserTrackingDetail;
