import { DICTIONARY_TYPE } from '../../constants/COMMON';

export const DICTIONARY_OPTIONS = Object.keys(DICTIONARY_TYPE).map(key => ({
  value: key,
  text: DICTIONARY_TYPE[key],
}));
