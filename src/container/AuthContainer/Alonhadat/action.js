/* eslint-disable camelcase */
import _ from 'lodash';
import { toast } from 'react-toastify';
import { INVALID_INFO_INPUT, UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateModalActions } from '../../../helpers/modalAction';
import { generateFormActions } from '../../../helpers/formAction';
import { showMessage } from '../../Common/GlobalErrorModal/action';

const {
  handleFieldChanged,
  reset,
  validateAll,
  submitting,
  submitDone,
  submitError,
} = generateFormActions(REDUCERS.ALONHADAT);
const { closeModal } = generateModalActions(REDUCERS.ALONHADAT);

export const handleChange = (name, value) => async dispatch => {
  dispatch(handleFieldChanged(name, value));
};

export const resetForm = () => async dispatch => {
  dispatch(reset());
};

const showErrorMessage = (dispatch, error) => {
  const message = error.message || INVALID_INFO_INPUT;
  dispatch(showMessage(message));
};

export const updateInfo = () => async (dispatch, getState, thunkDependencies) => {
  dispatch(validateAll());
  const { error } = getState().auth[REDUCERS.ALONHADAT].form;
  const isValid = _.values(error).filter(i => i.length).length === 0;
  const { alonhadat_name, alonhadat_password } = getState().auth[REDUCERS.ALONHADAT].form.data;
  if (isValid) {
    try {
      dispatch(submitting());
      await thunkDependencies.user.alonhadatUpdate(alonhadat_name, alonhadat_password);
      dispatch(submitDone());
      dispatch(closeModal());
      toast.success('Cập nhật thành công');
    } catch (err) {
      showErrorMessage(dispatch, err);
      dispatch(submitError(err.message || UNKNOWN_ERROR));
    }
  } else {
    showErrorMessage(dispatch, error);
  }
  dispatch(reset());
};
