import React from 'react';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from './action';
import { FormInput, Button, Modal, Flexbox } from '../../../components';
import { REDUCERS } from '../../../constants/COMMON';
import { generateModalActions } from '../../../helpers/modalAction';
import './ChangePassword.scss';

const { closeModal } = generateModalActions(REDUCERS.ALONHADAT);

const Alonhadat = props => {
  const { handleChange, updateInfo, closeAlonhadatModal, form, modal, resetForm } = props;
  const { visible } = modal;
  const commonInputProps = { width: 200 };
  const alonhdatName = {
    label: 'Tên đăng nhập alonhdat.com.vn',
    name: 'alonhadat_name',
    inputProps: commonInputProps,
  };
  const alonhdatPassword = {
    label: 'Mật khẩu đăng nhập alonhdat.com.vn',
    name: 'alonhadat_password',
    inputProps: commonInputProps,
  };

  return (
    <Modal
      // onClose={closeChangePasswordModal}
      containerStyle={{ maxWidth: 350 }}
      visible={visible}
      reponsive
      renderModal={() => (
        <div className="change_pw_dialog">
          <h2>Đổi mật khẩu</h2>
          <FormInput form={form} handleChange={handleChange} {...alonhdatName} />
          <FormInput form={form} handleChange={handleChange} {...alonhdatPassword} />
          <div className="field" style={{ textAlign: 'center', marginTop: 10 }}>
            <Flexbox row spaceBetween>
              <Button text="Cập nhật" onClick={updateInfo} />
              <Button
                text="Đóng"
                onClick={() => {
                  resetForm();
                  closeAlonhadatModal();
                }}
              />
            </Flexbox>
          </div>
        </div>
      )}
    />
  );
};

Alonhadat.propTypes = {
  handleChange: PropTypes.any.isRequired,
  updateInfo: PropTypes.any.isRequired,
  closeAlonhadatModal: PropTypes.any.isRequired,
  form: PropTypes.any.isRequired,
  modal: PropTypes.any.isRequired,
  resetForm: PropTypes.any.isRequired,
};

Alonhadat.defaultProps = {};

export function mapStateToProps(state) {
  return {
    modal: state.auth[REDUCERS.ALONHADAT].modal,
    form: state.auth[REDUCERS.ALONHADAT].form,
  };
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      closeAlonhadatModal: closeModal,
    },
    dispatch,
  );
}

const connetToRedux = connect(mapStateToProps, mapDispatchToProps);
export default compose(connetToRedux)(Alonhadat);
