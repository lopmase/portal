import { combineReducers } from 'redux';
import createFormReducer from '../../../helpers/createFormReducer';
import createModalReducer from '../../../helpers/createModalReducer';
import { REDUCERS } from '../../../constants/COMMON';

const initialValues = {
  alonhadat_name: '',
  alonhadat_password: '',
};

const commonValidation = [['required', 'Thông tin bắt buộc']];

export default combineReducers({
  modal: createModalReducer(REDUCERS.ALONHADAT),
  form: createFormReducer(REDUCERS.ALONHADAT, initialValues, {
    alonhadat_name: commonValidation,
    alonhadat_password: commonValidation,
  }),
});
