import { push } from 'connected-react-router';
import { apiSerializer } from '../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../constants/MESSAGE';
import { USER_TOKEN, USER_ID } from '../../constants/STORAGE_KEYS';
import { generateFormActions } from '../../helpers/formAction';
import { REDUCERS } from '../../constants/COMMON';
import { generateDataActions } from '../../helpers/dataAction';
import { generateModalActions } from '../../helpers/modalAction';
import { ROUTES } from '../../constants/ROUTES';
import { loadUserProfile } from './Profile/action';
import { getLastLocation, trackLocation } from '../../apis/userApi';
import { isInfoChanged, getUserInfo, getAddressFromCoordinate } from '../../helpers/location';
import { showMessage } from '../Common/GlobalErrorModal/action';

const { handleFieldChanged, submitting, submitDone, submitError } = generateFormActions(
  REDUCERS.LOGIN,
);
const { fetchDone, fetchError, fetching } = generateDataActions(REDUCERS.USER_PROFILE);
const { openModal: openChangePasswordAction } = generateModalActions(
  REDUCERS.CHANGE_PASSWORD_MODAL,
);
const { openModal: openAlonhadatdAction } = generateModalActions(REDUCERS.ALONHADAT);

export const handleChange = (name, value) => async dispatch => {
  dispatch(handleFieldChanged(name, value));
};

export const allNotifications = () => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const response = await thunkDependencies.user.notifications();
    const result = apiSerializer(response);
    dispatch(fetchDone(result));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};

export const logout = () => async dispatch => {
  try {
    localStorage.removeItem(USER_TOKEN);
    localStorage.removeItem(USER_ID);
    dispatch(push(ROUTES.LOGIN));
    return { data: true };
  } catch (error) {
    return { error: error.message || UNKNOWN_ERROR };
  }
};

export const trackUserInfo = () => async dispatch => {
  try {
    let needToTrack = false;
    const info = await getLastLocation();
    const currentInfo = await getUserInfo();

    if (info) {
      if (isInfoChanged(info, currentInfo)) {
        needToTrack = true;
      }
    } else {
      needToTrack = true;
    }

    if (needToTrack) {
      if (currentInfo.location && currentInfo.location.latitude) {
        const geocode = await getAddressFromCoordinate(
          currentInfo.location.latitude,
          currentInfo.location.longitude,
        );
        currentInfo.location.address = geocode;
      }
      const tracking = await trackLocation(currentInfo);
      if (tracking.data.location.notAuthorized) {
        dispatch(logout());
        dispatch(
          showMessage('Bạn cần bật cung cấp vị trí cho trang web để có thể đăng nhập'),
          // confirm('Bạn cần bật cung cấp vị trí cho trang web để có thể đăng nhập', result => {
          //   console.log('re', result);
          //   dispatch(logout());
          // }),
        );
      }
    }
  } catch (error) {
    console.log('error', error);
    // dispatch(showMessage('Something went wrong!'));
  }
};

export const login = () => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(submitting());
    const { email, password } = getState().auth.login.data;
    const response = await thunkDependencies.auth.login(email, password);
    const result = apiSerializer(response);
    const { token, id } = result;
    localStorage.setItem(USER_TOKEN, token);
    localStorage.setItem(USER_ID, id);
    dispatch(submitDone(result));
    dispatch(loadUserProfile());
    dispatch(trackUserInfo());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(submitError(message));
  }
};

export const openChangePasswordModal = () => async dispatch => {
  dispatch(openChangePasswordAction());
};
export const openAlonhadatModal = () => async dispatch => {
  dispatch(openAlonhadatdAction());
};
