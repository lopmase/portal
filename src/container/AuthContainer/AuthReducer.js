import { combineReducers } from 'redux';
import loginReducer from './Login/reducer';
import { REDUCERS } from '../../constants/COMMON';
import createDataReducer from '../../helpers/createDataReducer';
import changePasswordReducer from './ChangePassword/reducer';
import Alonhdat from './Alonhadat/reducer';

export default combineReducers({
  login: loginReducer,
  user: createDataReducer(REDUCERS.USER_PROFILE),
  [REDUCERS.CHANGE_PASSWORD]: changePasswordReducer,
  [REDUCERS.ALONHADAT]: Alonhdat,
});
