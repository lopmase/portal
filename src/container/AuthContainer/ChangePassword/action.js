import _ from 'lodash';
import { toast } from 'react-toastify';
import { INVALID_INFO_INPUT, UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateModalActions } from '../../../helpers/modalAction';
import { generateFormActions } from '../../../helpers/formAction';
import { showMessage } from '../../Common/GlobalErrorModal/action';

import { logout } from '../AuthAction';

const {
  handleFieldChanged,
  reset,
  validateAll,
  submitting,
  submitDone,
  submitError,
} = generateFormActions(REDUCERS.CHANGE_PASSWORD);
const { closeModal } = generateModalActions(REDUCERS.CHANGE_PASSWORD_MODAL);

export const handleChange = (name, value) => async dispatch => {
  dispatch(handleFieldChanged(name, value));
};

export const resetForm = () => async dispatch => {
  dispatch(reset());
};

const showErrorMessage = (dispatch, error) => {
  const message = error.message || INVALID_INFO_INPUT;
  dispatch(showMessage(message));
};

export const changePassword = () => async (dispatch, getState, thunkDependencies) => {
  dispatch(validateAll());
  let { error } = getState().auth[REDUCERS.CHANGE_PASSWORD].form;
  let isValid = _.values(error).filter(i => i.length).length === 0;
  const { currentPassword, newPassword, confirmPassword } = getState().auth[
    REDUCERS.CHANGE_PASSWORD
  ].form.data;
  if (isValid && newPassword !== confirmPassword) {
    isValid = false;
    error = { message: 'Mật khẩu xác nhận không trùng khớp. Vui lòng kiểm tra lại' };
  }
  if (isValid) {
    try {
      dispatch(submitting());
      await thunkDependencies.user.changePassword(currentPassword, newPassword);
      dispatch(submitDone());
      dispatch(closeModal());
      dispatch(logout());
      toast.success('Đổi mật khẩu thành công. Vui lòng đăng nhập lại');
    } catch (err) {
      showErrorMessage(dispatch, err);
      dispatch(submitError(err.message || UNKNOWN_ERROR));
    }
  } else {
    showErrorMessage(dispatch, error);
  }
  dispatch(reset());
};
