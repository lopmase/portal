import React from 'react';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from './action';
import { FormInput, Button, Modal, Flexbox } from '../../../components';
import { REDUCERS } from '../../../constants/COMMON';
import { generateModalActions } from '../../../helpers/modalAction';
import './ChangePassword.scss';

const { closeModal } = generateModalActions(REDUCERS.CHANGE_PASSWORD_MODAL);

const ChangePassword = props => {
  const { handleChange, changePassword, closeChangePasswordModal, form, modal, resetForm } = props;
  const { visible } = modal;
  const commonInputProps = { width: 200 };
  const currentPassword = {
    label: 'Mật khẩu hiện tại',
    name: 'currentPassword',
    inputProps: commonInputProps,
  };
  const newPassword = {
    label: 'Mật khẩu mới',
    name: 'newPassword',
    inputProps: commonInputProps,
  };
  const confirmPassword = {
    label: 'Nhập lại mật khẩu mới',
    name: 'confirmPassword',
    inputProps: commonInputProps,
  };

  return (
    <Modal
      // onClose={closeChangePasswordModal}
      containerStyle={{ maxWidth: 350 }}
      visible={visible}
      reponsive
      renderModal={() => (
        <div className="change_pw_dialog">
          <h2>Đổi mật khẩu</h2>
          <FormInput form={form} handleChange={handleChange} {...currentPassword} />
          <FormInput form={form} handleChange={handleChange} {...newPassword} />
          <FormInput form={form} handleChange={handleChange} {...confirmPassword} />
          <div className="field" style={{ textAlign: 'center', marginTop: 10 }}>
            <Flexbox row spaceBetween>
              <Button text="Đổi mật khẩu" onClick={changePassword} />
              <Button
                text="Đóng"
                onClick={() => {
                  resetForm();
                  closeChangePasswordModal();
                }}
              />
            </Flexbox>
          </div>
        </div>
      )}
    />
  );
};

ChangePassword.propTypes = {
  handleChange: PropTypes.any.isRequired,
  changePassword: PropTypes.any.isRequired,
  closeChangePasswordModal: PropTypes.any.isRequired,
  form: PropTypes.any.isRequired,
  modal: PropTypes.any.isRequired,
  resetForm: PropTypes.any.isRequired,
};

ChangePassword.defaultProps = {};

export function mapStateToProps(state) {
  return {
    modal: state.auth[REDUCERS.CHANGE_PASSWORD].modal,
    form: state.auth[REDUCERS.CHANGE_PASSWORD].form,
  };
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      closeChangePasswordModal: closeModal,
    },
    dispatch,
  );
}

const connetToRedux = connect(mapStateToProps, mapDispatchToProps);
export default compose(connetToRedux)(ChangePassword);
