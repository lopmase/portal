import { combineReducers } from 'redux';
import createFormReducer from '../../../helpers/createFormReducer';
import createModalReducer from '../../../helpers/createModalReducer';
import { REDUCERS } from '../../../constants/COMMON';
import { PASSWORD_LENGHT_MIN } from '../../../constants/APP_CONST';

const initialValues = {
  currentPassword: '',
  newPassword: '',
  confirmPassword: '',
  // alonhadat_name: '',
  // alonhadat_password: '',
};

const commonValidation = [
  ['required', 'Thông tin bắt buộc'],
  [
    value => value.match(new RegExp(`^.{${PASSWORD_LENGHT_MIN},}$`)),
    `Mật khẩu ít nhất ${PASSWORD_LENGHT_MIN} ký tự`,
  ],
];

export default combineReducers({
  modal: createModalReducer(REDUCERS.CHANGE_PASSWORD_MODAL),
  form: createFormReducer(REDUCERS.CHANGE_PASSWORD, initialValues, {
    currentPassword: commonValidation,
    newPassword: commonValidation,
    confirmPassword: commonValidation,
  }),
});
