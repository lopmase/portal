/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { compose } from 'recompose';
import { withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { Button, Input, Card, Flexbox } from '../../../components';
import * as authAction from '../AuthAction';
import styles from '../../../styles/_Colors.scss';
import { ROUTES } from '../../../constants/ROUTES';
import { isAuthenticated } from '../../../apis/authApi';
import './Login.scss';

class Login extends Component {
  static propTypes = {};

  static defaultProps = {};

  state = {};

  render() {
    const { handleChange, login, submitting, error, form, message } = this.props;
    const { email, password } = form;
    const isValid = _.values(error).filter(e => e.length).length === 0;
    return (
      <div className="login-page" style={{ paddingTop: 100, height: '100vh' }}>
        <h1 style={{ textAlign: 'center', color: 'white', marginTop: 40, fontWeight: 900 }}>
          THịnh Gia Land
        </h1>
        <Flexbox padding={40}>
          <Card minWidth={300} containerStyle={{ padding: 30 }}>
            <Input value={email} name="email" onChange={handleChange} label="EMAIL ADDRESS" />
            <Input
              password
              value={password}
              name="password"
              onChange={handleChange}
              label="PASSWORD"
            />
            {message.length > 0 && (
              <div
                style={{
                  color: styles.redColor,
                  marginTop: 10,
                  fontSize: 13,
                  opacity: 0.7,
                  fontWeight: 700,
                }}
              >
                {message}
              </div>
            )}
            <Flexbox containerStyle={{ marginTop: 20 }} row spaceBetween>
              <a href="#" onClick={() => {}}>
                Forgot Password?
              </a>
              <Button disabled={!isValid} loading={submitting} onClick={login} text="LOGIN" />
            </Flexbox>
          </Card>
        </Flexbox>
        {isAuthenticated() && <Redirect to={ROUTES.DASHBOARD} />}
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    form: state.auth.login.data,
    loading: state.auth.login.submitting,
    touched: state.auth.login.touched,
    error: state.auth.login.error,
    message: state.auth.login.message,
    state,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...authAction,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Login);
