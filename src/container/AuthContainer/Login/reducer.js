import createFormReducer from '../../../helpers/createFormReducer';
import { REDUCERS } from '../../../constants/COMMON';

const initialValues = {
  email: '',
  password: '',
};

const validator = (name, value) => {
  return value.length > 0;
};

const loginReducer = createFormReducer(REDUCERS.LOGIN, initialValues, validator);

export default loginReducer;
