/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { compose } from 'recompose';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Modal as AntModal } from 'antd';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { Modal, Avatar, Flexbox, Button, Spacer } from '../../../components';
import * as actions from './action';
import styles from '../../../styles/_Colors.scss';
import './Profile.scss';
import { REDUCERS, ROLE_NAMES } from '../../../constants/COMMON';
import { generateModalActions } from '../../../helpers/modalAction';
import { logout } from '../AuthAction';
import { formatImageUrl } from '../../../helpers/common';
import { ROUTES } from '../../../constants/ROUTES';

const { confirm } = AntModal;

const { openModal } = generateModalActions(REDUCERS.CHANGE_PASSWORD_MODAL);
const { closeModal } = generateModalActions(REDUCERS.USER_PROFILE_MODAL);
const { openModal: openAlonhadat } = generateModalActions(REDUCERS.ALONHADAT);

const Profile = props => {
  const {
    user,
    loading,
    visible,
    close,
    doLogout,
    openChangePasswordModal,
    handleUpdateAvatar,
    openAlonhadatModal,
  } = props;

  const changeAvatar = newImage => {
    confirm({
      title: 'Đổi ảnh đại diện',
      content: 'Bạn có muốn đổi ảnh đại diện mới không',
      onOk: () => {
        handleUpdateAvatar(newImage);
      },
    });
  };

  return (
    <Modal
      visible={visible}
      onClose={close}
      containerStyle={{ width: 350 }}
      reponsive
      renderModal={() => (
        <div className="profile-page">
          {loading ? (
            <div>loading...</div>
          ) : (
            user && (
              <div>
                <Avatar
                  id="user-profile"
                  containerStyle={{ marginTop: 20 }}
                  onImageAdded={changeAvatar}
                  image={user.avatar ? { url: formatImageUrl(user.avatar.thumbnail) } : null}
                  removeIconVisible={false}
                />
                <h2>{user.name}</h2>
                <div className="field">
                  <Flexbox row spaceBetween>
                    <label>SĐT</label>
                    <span>{user.phone_number}</span>
                  </Flexbox>
                </div>
                <div className="field">
                  <Flexbox row spaceBetween>
                    <label>Quyền</label>
                    <span>{ROLE_NAMES[user.role]}</span>
                  </Flexbox>
                </div>
                <div className="field">
                  <Flexbox row spaceBetween>
                    <label>Ngày sinh</label>
                    <span>{user.date_of_birth}</span>
                  </Flexbox>
                </div>
                <div className="field">
                  <Flexbox row spaceBetween>
                    <label>Tài khoản Alonhadat</label>
                    <span>{user.alonhadat_name}</span>
                  </Flexbox>
                </div>
                <Flexbox>
                  <div style={{ textAlign: 'center', marginTop: 10 }}>
                    <Link key={ROUTES.ACCOUNT} to={ROUTES.ACCOUNT}>
                      <Button
                        text="Alonhadat.com"
                        onClick={() => {
                          close();
                        }}
                      />
                    </Link>
                  </div>
                  <Spacer />
                  <div style={{ textAlign: 'center', marginTop: 10 }}>
                    <Button
                      text="Đổi mật khẩu"
                      onClick={() => {
                        openChangePasswordModal();
                        close();
                      }}
                    />
                  </div>
                </Flexbox>
                <Flexbox containerStyle={{ marginTop: 20 }} row spaceBetween>
                  <div />
                  <Button
                    icon={faSignOutAlt}
                    iconColor={styles.primaryColor}
                    clear
                    onClick={() => {
                      doLogout();
                      close();
                    }}
                    text="Đăng xuất"
                  />
                </Flexbox>
              </div>
            )
          )}
        </div>
      )}
    />
  );
};

export function mapStateToProps(state) {
  return {
    user: state.auth.user.data,
    loading: state.auth.user.fetching,
    visible: state.modal[REDUCERS.USER_PROFILE_MODAL].visible,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      close: closeModal,
      doLogout: logout,
      openChangePasswordModal: openModal,
      openAlonhadatModal: openAlonhadat,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Profile);
