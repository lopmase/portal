import _ from 'lodash';
import { toast } from 'react-toastify';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateDataActions } from '../../../helpers/dataAction';
import { generateModalActions } from '../../../helpers/modalAction';
import { uploadImages } from '../../HouseContainer/UploadJobs/action';
import { showMessage } from '../../Common/GlobalErrorModal/action';

const { fetchDone, fetchError, fetching } = generateDataActions(REDUCERS.USER_PROFILE);
const { openModal, closeModal } = generateModalActions(REDUCERS.GLOBAL_LOADING);

export const loadUserProfile = () => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const response = await thunkDependencies.user.loadMe();
    const result = apiSerializer(response);
    dispatch(fetchDone(result));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};

export const handleUpdateAvatar = image => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(openModal());
    await dispatch(uploadImages([image]));
    const { jobs } = getState().house[REDUCERS.UPLOAD_JOBS];
    const imageId = _.values(jobs.results)
      .filter(r => r.id)
      .map(r => r.id)[0];
    if (imageId) {
      await thunkDependencies.user.updateAvatar(imageId);
      dispatch(loadUserProfile());
    }
    dispatch(closeModal());
    toast.success('Thay đổi ảnh đại diện thành công');
  } catch (error) {
    dispatch(closeModal());
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};
