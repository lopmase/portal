import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { faTrashAlt, faPenAlt } from '@fortawesome/free-solid-svg-icons';

import { Card, Flexbox, ActionMenu } from '../../../components';
import styles from '../../../styles/_Colors.scss';

const actions = [
  { text: 'Xoá', value: 'del', icon: faTrashAlt },
  { text: 'Sửa', value: 'edit', icon: faPenAlt },
];

const Blog = ({ blog, onDelete, onEdit }) => {
  const onActionClick = item => {
    if (item.value === 'del') {
      onDelete();
    } else {
      onEdit();
    }
  };
  return (
    <Card shadow noPadding containerStyle={{ marginTop: 20 }}>
      <img
        style={{
          width: '100%',
          height: 180,
          objectFit: 'cover',
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10,
        }}
        alt="cover blog"
        src={blog.photo}
      />
      <div style={{ padding: 10 }}>
        <div style={{ fontSize: 11, color: styles.primaryColor }}>
          {`Tạo bởi ${blog.author} lúc ${moment().format('DD-MM-YYYY')}`}
        </div>
        <div style={{ fontSize: 12, color: 'blue' }}>{`Loại blog: ${blog.blogType}`}</div>
        <h4 style={{ marginBottom: 0, marginTop: 10 }}>{blog.title || 'Tiêu đề'}</h4>
        <p
          style={{
            whiteSpace: 'pre-line',
            fontSize: 12,
            fontWeight: 'bold',
            color: styles.grayColor,
            margin: 0,
            maxHeight: 140,
            overflow: 'hidden',
          }}
        >
          {blog.textContent || 'Nội dung tin tức'}
        </p>
        <Flexbox row spaceBetween>
          <div />
          <ActionMenu onSelect={item => onActionClick(item)} items={actions} />
        </Flexbox>
      </div>
    </Card>
  );
};

Blog.propTypes = {
  blog: PropTypes.any.isRequired,
  onDelete: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
};

Blog.defaultProps = {};

export default Blog;
