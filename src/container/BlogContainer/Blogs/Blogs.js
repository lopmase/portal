/* eslint-disable react/no-array-index-key */
/* eslint-disable react/prop-types */
import React from 'react';
import { compose, lifecycle } from 'recompose';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import { push } from 'connected-react-router';
import { Flexbox, Button } from '../../../components';
import './Blogs.scss';
import { REDUCERS } from '../../../constants/COMMON';
import * as actions from './action';

import { confirm } from '../../ShellContainner/ConfirmDialog/action';
import { DELETE_BLOG_CONFIRM } from '../../../constants/MESSAGE';
import Blog from './Blog';
import { ROUTES } from '../../../constants/ROUTES';

const Blogs = ({ dataSource: { list, fetching, total }, openConfirmDialog, remove, pushRoute }) => {
  const onDelete = id => {
    openConfirmDialog(DELETE_BLOG_CONFIRM, result => {
      if (result) {
        remove(id);
      }
    });
  };

  const onEdit = blog => {
    pushRoute(`${ROUTES.BLOG_ROOT}/edit/${blog.id}`);
  };

  return (
    <div className="blogs-page" style={{}}>
      <div className="toolbar">
        <Flexbox
          row
          spaceBetween
          containerStyle={{ marginBottom: 20, marginTop: 20, alignItems: 'center' }}
        >
          <Flexbox>
            <div className="total">{`${total} Tin tức`}</div>
          </Flexbox>
          <Flexbox>
            <Link to={ROUTES.NEW_BLOG}>
              <Button text="+ Tạo mới" />
            </Link>
          </Flexbox>
        </Flexbox>
      </div>
      {fetching ? (
        <div>Đang tải....</div>
      ) : (
        _.chunk(list, 4).map((row, index) => (
          <Flexbox key={`row${index}`} row flexStart alignItems="flex-start">
            {row.map(blog => (
              <div
                key={blog.id}
                style={{ flex: 1, minWidth: 350, marginLeft: 10, marginRight: 10 }}
              >
                <Blog blog={blog} onDelete={() => onDelete(blog.id)} onEdit={() => onEdit(blog)} />
              </div>
            ))}
          </Flexbox>
        ))
      )}
    </div>
  );
};

export const mapStateToProps = state => ({
  dataSource: state.blog[REDUCERS.BLOGS],
});

const mapDispatchToProps = {
  ...actions,
  openConfirmDialog: confirm,
  pushRoute: push,
};

export const useLifeCycle = lifecycle({
  componentDidMount() {
    this.props.fetch();
  },
});

const connetToRedux = connect(mapStateToProps, mapDispatchToProps);
export default compose(connetToRedux, useLifeCycle, withRouter)(Blogs);
