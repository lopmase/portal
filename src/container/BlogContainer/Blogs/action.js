import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { generateModalActions } from '../../../helpers/modalAction';
import { generateDatalistActions } from '../../../helpers/dataListAction';
import { generateFormActions } from '../../../helpers/formAction';
import { UPDATE_ROLE_FILTER } from './types';

const {
  fetchDone,
  fetching,
  fetchError,
  nextPage,
  previousPage,
  updatePageSize,
} = generateDatalistActions(REDUCERS.BLOGS);

const { openModal, closeModal } = generateModalActions(REDUCERS.GLOBAL_LOADING);
const { openModal: openNewUserAction } = generateModalActions(REDUCERS.NEW_USER_MODAL);
const { handleFieldChanged, reset } = generateFormActions(REDUCERS.NEW_USER);

export const filterByRole = roleId => ({
  type: UPDATE_ROLE_FILTER,
  roleId,
});

export const fetch = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { page, pageSize } = getState().blog[REDUCERS.BLOGS];
    dispatch(fetching());
    const result = await thunkDependencies.blog.list({
      page,
      size: pageSize,
    });
    dispatch(fetchDone(result));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;

    dispatch(fetchError(message));
  }
};

export const goNextPage = () => async dispatch => {
  dispatch(nextPage());
  dispatch(fetch());
};

export const goPrevPage = () => async dispatch => {
  dispatch(previousPage());
  dispatch(fetch());
};

export const changePageSize = size => async dispatch => {
  dispatch(updatePageSize(size));
  dispatch(fetch());
};

export const remove = id => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(openModal());
    await thunkDependencies.blog.remove(id);
    dispatch(closeModal());
    dispatch(fetch());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};

export const openNewUserModal = () => async (dispatch, getState) => {
  const user = getState().auth.user.data;
  if (user) {
    dispatch(handleFieldChanged('user_id', user.id));
  }

  dispatch(reset());
  dispatch(openNewUserAction());
};

export const filterRole = role => async dispatch => {
  dispatch(filterByRole(role));
  dispatch(fetch());
};
