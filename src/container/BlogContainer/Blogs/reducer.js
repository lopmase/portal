import { UPDATE_ROLE_FILTER } from './types';
import { ROLE_NAMES } from '../../../constants/COMMON';

const ROLE_NAME_SELECTIONS = Object.keys(ROLE_NAMES).map(key => ({
  value: key,
  text: ROLE_NAMES[key],
}));

ROLE_NAME_SELECTIONS.unshift({ value: 0, text: 'Tất cả' });

export const ROLE_OPTIONS = ROLE_NAME_SELECTIONS;

const initialValue = {
  role: 0,
};

export const userFilterReducer = (state = initialValue, action) => {
  switch (action.type) {
    case UPDATE_ROLE_FILTER:
      return {
        ...state,
        role: action.roleId,
      };

    default:
      return state;
  }
};
