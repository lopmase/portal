import React, { Component } from 'react';
import { PrivateRoute } from '../../components';
import { ROUTES } from '../../constants/ROUTES';
import Blogs from './Blogs/Blogs';
import NewBlog from './NewBlog';

class BlogContainer extends Component {
  state = {};

  render() {
    return (
      <div className="blog-container">
        <PrivateRoute path={ROUTES.BLOGS} component={Blogs} />
        <PrivateRoute path={ROUTES.NEW_BLOG} component={NewBlog} />
        <PrivateRoute
          path={ROUTES.EDIT_BLOG}
          component={props => <NewBlog {...props} editMode />}
        />
      </div>
    );
  }
}

export default BlogContainer;
