import _ from 'lodash';
import { push } from 'connected-react-router';
import { toast } from 'react-toastify';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateFormActions } from '../../../helpers/formAction';
import { generateDataActions } from '../../../helpers/dataAction';
import { generateModalActions } from '../../../helpers/modalAction';
import Selector from '../Selector';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { uploadImages } from '../../HouseContainer/UploadJobs/action';
import { ROUTES } from '../../../constants/ROUTES';
import { formatImageUrl } from '../../../helpers/common';

const { openModal, closeModal } = generateModalActions(REDUCERS.GLOBAL_LOADING);

const {
  handleFieldChanged,
  submitDone,
  submitError,
  submitting,
  validateAll,
  reset,
} = generateFormActions(REDUCERS.NEW_BLOG);

export const handleChange = (name, value) => async dispatch => {
  dispatch(handleFieldChanged(name, value));
};

export const create = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { data } = getState().blog[REDUCERS.NEW_BLOG];
    let imageId = null;
    dispatch(submitting());
    if (data.cover) {
      await dispatch(uploadImages([data.cover]));
      const uploadJobs = getState().house[REDUCERS.UPLOAD_JOBS].jobs;
      imageId = uploadJobs.results[0] ? uploadJobs.results[0].id : null;
    }
    const form = { ...data, cover: imageId || undefined };
    const response = await thunkDependencies.blog.newBlog(form);
    const result = apiSerializer(response);
    dispatch(submitDone(result));
    dispatch(reset());
    toast.success('Tạo tin tức thành công!');
    dispatch(push(ROUTES.BLOGS));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
    dispatch(submitError(message));
  }
};

export const update = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { data } = getState().blog[REDUCERS.NEW_BLOG];
    let imageId = null;
    dispatch(submitting());
    if (data.cover && data.cover.file) {
      await dispatch(uploadImages([data.cover]));
      const uploadJobs = getState().house[REDUCERS.UPLOAD_JOBS].jobs;
      imageId = uploadJobs.results[0] ? uploadJobs.results[0].id : null;
    } else {
      imageId = data.cover.id;
    }
    const form = { ...data, blog_id: data.id, id: undefined, cover: imageId || undefined };
    const response = await thunkDependencies.blog.update(form);
    const result = apiSerializer(response);
    dispatch(submitDone(result));
    dispatch(reset());
    toast.success('Cập nhật tin tức thành công!');
    dispatch(push(ROUTES.BLOGS));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
    dispatch(submitError(message));
  }
};

export const submit = editMode => async (dispatch, getState) => {
  dispatch(validateAll());
  const { error } = getState().blog[REDUCERS.NEW_BLOG];
  const isValid = _.values(error).filter(i => i.length).length === 0;

  if (isValid) {
    if (editMode) {
      dispatch(update());
    } else {
      dispatch(create());
    }
  }
};

const blogFields = ['id', 'title', 'content', 'tag', 'status', 'cover', 'type'];

const { fetchDone, fetching, fetchError } = generateDataActions(REDUCERS.BLOG_DETAIL);

export const fetch = id => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const response = await thunkDependencies.blog.detail(id);
    const result = apiSerializer(response);
    dispatch(fetchDone(result));
    return result;
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
    return false;
  }
};

export const fetchBlogDetail = id => async (dispatch, getState) => {
  dispatch(openModal());
  const rs = await dispatch(fetch(id));
  const blog = Selector.blog(getState()).data;
  dispatch(closeModal());
  blogFields.forEach(field => {
    if (field === 'cover') {
      const cover =
        blog.cover && blog.cover.main
          ? { id: blog.cover.id, url: formatImageUrl(blog.cover.main) }
          : null;
      dispatch(handleChange(field, cover));
    } else {
      dispatch(handleChange(field, blog[field]));
    }
  });
  return rs;
};
