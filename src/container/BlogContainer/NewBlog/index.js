/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { goBack } from 'connected-react-router';
import { bindActionCreators } from 'redux';
import { Editor } from 'react-draft-wysiwyg';
import { convertToRaw, EditorState, convertFromRaw } from 'draft-js';
import moment from 'moment';
import { faSave, faTimes } from '@fortawesome/free-solid-svg-icons';
import 'draft-js/dist/Draft.css';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import _ from 'lodash';
import './NewBlog.scss';
import styles from '../../../styles/_Colors.scss';
import { REDUCERS, BLOG_TYPE } from '../../../constants/COMMON';
import * as actions from './action';
import { generateModalActions } from '../../../helpers/modalAction';
import {
  Flexbox,
  FormInput,
  FormLabel,
  Card,
  Avatar,
  Button,
  FormField,
  FormDropdown,
} from '../../../components';
import coverImage from '../../../assets/images/left.jpg';

const { toggle } = generateModalActions(REDUCERS.NEW_USER_MODAL);

class NewBlog extends Component {
  static propTypes = {};

  static defaultProps = {};

  state = {
    editorState: EditorState.createEmpty(),
  };

  componentDidMount() {
    const { match } = this.props;
    const { id } = match.params;
    this.loadBlogData(id);
  }

  loadBlogData = async id => {
    const { editMode, fetchBlogDetail } = this.props;

    if (editMode) {
      const rs = await fetchBlogDetail(id);
      if (rs) {
        try {
          const { form } = this.props;
          const jsonContent = JSON.parse(form.data.content);
          const editorState = EditorState.createWithContent(convertFromRaw(jsonContent));
          this.setState({ editorState });
        } catch (error) {
          // eslint-disable-next-line no-console
          console.log(error);
        }
      }
    }
  };

  onChange = editorState => {
    const { handleChange } = this.props;
    const rawContent = convertToRaw(editorState.getCurrentContent());
    this.setState({ editorState });
    handleChange('content', JSON.stringify(rawContent));
  };

  render() {
    const { form, handleChange, submit, user, editMode, back } = this.props;
    const { submitting, error, data } = form;
    const { editorState } = this.state;
    const isValid = _.values(error).filter(i => i.length).length === 0;
    const textContent = editorState.getCurrentContent().getPlainText('\n');
    return (
      <div className="new-blog-page">
        <Flexbox row alignItems="flex-start">
          <div style={{ flex: 3, marginLeft: 10, marginRight: 10 }}>
            <div style={{ display: 'flex' }}>
              <h2>{editMode ? 'Cập nhật blog' : 'Tạo blog'}</h2>
              <Button
                ortherClass
                iconOnly
                spaceBetween
                icon={faTimes}
                onClick={() => back()}
                containerStyle={{ paddingLeft: 15, paddingRight: 15 }}
              />
            </div>
            <Card shadow containerStyle={{ padding: 20, marginTop: 10 }}>
              <Avatar
                onImageAdded={image => handleChange('cover', image)}
                onImageRemoved={() => handleChange('cover', null)}
                id="blog-cover-new"
                image={data.cover}
                label="Ảnh bìa"
                square
              />
              <FormInput form={form} handleChange={handleChange} name="title" label="Tiêu đề" />
              <FormDropdown
                name="type"
                label="Loại Blog"
                form={form}
                handleChange={handleChange}
                items={BLOG_TYPE}
              />
              <FormField
                name="content"
                form={form}
                handleChange={handleChange}
                render={() => (
                  <>
                    <FormLabel text="Nội dung" />
                    <Editor
                      editorState={editorState}
                      wrapperStyle={{
                        borderWidth: 1,
                        borderRadius: 5,
                        borderColor: 'rgba(34,36,38,.15)',
                        borderStyle: 'solid',
                        backgroundColor: 'white',
                      }}
                      handlePastedText={() => false}
                      editorStyle={{ padding: 10 }}
                      onEditorStateChange={this.onChange}
                    />
                  </>
                )}
              />
            </Card>
          </div>
          <div style={{ flex: 1, maxWidth: 400, minWidth: 200, marginLeft: 10, marginRight: 10 }}>
            <h3 style={{ margin: 0, fontSize: 16, color: styles.grayColor }}>Xem trước</h3>
            <Card shadow noPadding containerStyle={{ overflow: 'hidden', marginTop: 15 }}>
              <img
                style={{ width: '100%', height: 150, objectFit: 'cover' }}
                alt="cover blog"
                src={data.cover ? data.cover.data || data.cover.url : coverImage}
              />
              <div style={{ padding: 10 }}>
                <div style={{ fontSize: 11, color: styles.primaryColor }}>
                  {`Tạo bởi ${user ? user.name : ''} lúc ${moment().format('DD-MM-YYYY')}`}
                </div>
                <h4 style={{ marginBottom: 0, marginTop: 10 }}>{data.title || 'Tiêu đề'}</h4>
                <p
                  style={{
                    whiteSpace: 'pre-line',
                    fontSize: 12,
                    fontWeight: 'bold',
                    color: styles.grayColor,
                    margin: 0,
                    maxHeight: 145,
                  }}
                >
                  {textContent || 'Nội dung tin tức'}
                </p>
              </div>
            </Card>
            <div style={{ marginTop: 30 }}>
              <Button
                text={editMode ? 'Cập nhật' : 'Tạo'}
                onClick={() => submit(editMode)}
                icon={faSave}
                loading={submitting}
                disabled={!isValid}
                containerStyle={{ width: '100%' }}
              />
            </div>
          </div>
        </Flexbox>
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    form: state.blog[REDUCERS.NEW_BLOG],
    user: state.auth.user.data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      toggleModal: toggle,
      back: goBack,
    },
    dispatch,
  );
}
export default compose(connect(mapStateToProps, mapDispatchToProps))(NewBlog);
