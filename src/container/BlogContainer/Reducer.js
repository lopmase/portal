import { combineReducers } from 'redux';
import createDatalistReducer from '../../helpers/createDataListReducer';
import { REDUCERS } from '../../constants/COMMON';
import createFormReducer from '../../helpers/createFormReducer';
import createDataReducer from '../../helpers/createDataReducer';

export default combineReducers({
  [REDUCERS.BLOGS]: createDatalistReducer(REDUCERS.BLOGS),
  [REDUCERS.NEW_BLOG]: createFormReducer(
    REDUCERS.NEW_BLOG,
    {},
    {
      cover: [['required', 'Ảnh bìa bắt buộc']],
      type: [['required', 'Loại blog bắt buộc']],
      title: [['required', 'Tiêu đề bắt buộc']],
      content: [['required', 'Nội dung bắt buộc']],
    },
  ),
  [REDUCERS.BLOG_DETAIL]: createDataReducer(REDUCERS.BLOG_DETAIL),
});
