import { REDUCERS } from '../../constants/COMMON';

export default {
  blog: state => state.blog[REDUCERS.BLOG_DETAIL],
};
