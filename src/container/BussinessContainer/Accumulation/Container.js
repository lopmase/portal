/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-tag-spacing */
/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable prettier/prettier */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-param-reassign */
/* eslint-disable array-callback-return */
import { Pagination, Table } from 'antd';
import React, { useEffect, useState, useRef } from 'react';
import { connect, useDispatch } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { getAllStatisticByCondition } from '../../../apis/businessApi';
import Condition from '../Condition/Condition';
import {
  connectionAccumulationField,
  purchaseAccumulationField,
  saleAccumulationField,
  accumulationColumns,
} from '../../../constants/COMMON';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import ModalUpdate from '../Modal/update';
import Target from './Target';

const Accumulation = ({ accumulation, condition, type }) => {
  const [totalAccumulation, setTotalAccumulation] = useState();
  const [dataUpdate, setDataUpdate] = useState();
  const [loading, setLoading] = useState(true);
  const [createTarget, setIsCreateTarget] = useState(false);
  const firstUpdate = useRef(true);
  const NEW_ACCUMULATION = 'NEW_ACCUMULATION';
  const dispatch = useDispatch();
  const fetchData = async condition => {
    let data = await getAllStatisticByCondition({ ...condition, paginate: true });
    setTotalAccumulation(data.data.total);
    data = data.data.data;
    dispatch({
      type: NEW_ACCUMULATION,
      payload: data,
    });
    setLoading(false);
  };
  useEffect(() => {
    accumulation &&
      accumulation.sort((a, b) => {
        return new Date(b.date) - new Date(a.date);
      });
  }, [accumulation]);

  const accumulationField = [
    {
      label: 'Sản phẩm marketing trong ngày',
      name: 'marketingQuantity',
      rules: [
        {
          required: true,
          message: 'Vui lòng nhập Sản phẩm marketing trong ngày',
        },
        {
          pattern: new RegExp(/^[0-9]+$/),
          message: 'Phải là số',
        },
      ],
    },
    ...saleAccumulationField,
    ...purchaseAccumulationField,
    ...connectionAccumulationField,
  ];
  useEffect(() => {
    try {
      if (firstUpdate.current && type !== 'DEFAULT_CONDITION') {
        firstUpdate.current = false;
        return;
      }
      condition && fetchData({ ...condition, page: 1 });
    } catch (err) {
      dispatch(showMessage(err));
    }
  }, [condition]);
  const selectPage = page => {
    fetchData({ ...condition, page });
  };
  return (
    <>
      <Condition />
      <Table
        dataSource={accumulation}
        loading={loading}
        bordered
        columns={accumulationColumns}
        scroll={{ x: 'calc(800px + 50%)', y: 450 }}
        pagination={false}
        onRow={record => {
          return {
            onDoubleClick: () => {
              setDataUpdate({ ...record });
            },
          };
        }}
      />
      {totalAccumulation !== null && totalAccumulation > 0 && (
        <Pagination
          onChange={selectPage}
          pageSize={20}
          defaultCurrent={1}
          total={totalAccumulation}
        />
      )}
      {dataUpdate && (
        <ModalUpdate field={dataUpdate} columns={accumulationField} type="accumulation" />
      )}
      {createTarget && <Target visible={createTarget} />}
    </>
  );
};

export function mapStateToProps(state) {
  return {
    accumulation: state.accumulation.data,
    type: state.condition.type,
    me: state.auth.user.data,
    condition: state.condition.data.condition,
    state,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({});
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Accumulation);
