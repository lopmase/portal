/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { Modal, Form, Table, Tabs } from 'antd';
import { Button } from '../../../components';
import { selectSeason, targetAccumulationColumns } from '../../../constants/COMMON';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { getCondition } from '../../../helpers/getCondition';
import { createTarget, getTarget, updateTarget } from '../../../apis/businessApi';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import TargetForm from './TargetForm';

const { TabPane } = Tabs;
const Target = () => {
  const [visible, setVisible] = useState(false);
  const [condition, setCondition] = useState();
  const [type, setType] = useState();
  const [form] = Form.useForm();
  const [updateTargetValue, setUpdateTargetValue] = useState();
  const [target, setTarget] = useState([]);
  const dispatch = useDispatch();
  const handleChange = value => {
    const cond = getCondition(2, value)[0];
    setCondition({ ...cond, quarter: value });
  };
  const onFinishFailed = value => {
    // dispatch(showMessage('err'));
    console.log('err', value);
  };
  const onFinish = async value => {
    try {
      if (type === 1) {
        if (!condition || !condition.startAt || !condition.endAt) {
          setVisible(false);
          dispatch(showMessage('Vui lòng chọn Quý'));
          return;
        }
        createTarget({ ...value, ...condition })
          .then(() => {
            form.resetFields();
            setVisible(false);
            toast.success('Create Successfull');
          })
          .catch(error => {
            setVisible(false);
            dispatch(showMessage(error.message));
          });
      }
      if (type === 3) {
        updateTarget({
          ...value,
          quarter: updateTargetValue.quarter,
          id: updateTargetValue.id,
        })
          .then(() => {
            form.resetFields();
            setVisible(false);
            toast.success('Update Successfull');
          })
          .catch(error => {
            dispatch(showMessage(error));
          });
      }
    } catch (error) {
      dispatch(showMessage(error.message || UNKNOWN_ERROR));
    }
  };
  const getData = async () => {
    const startAt = `${new Date().getFullYear()}/01/01 00:00:00`;
    const endAt = `${new Date().getFullYear()}/12/31 23:59:59`;
    const res = await getTarget({ startAt, endAt });
    if (res) {
      res.data = res.data.sort((a, b) => a.quarter - b.quarter);
      setTarget(res.data);
    }
  };

  return (
    <div>
      <Button
        text="+Nhập đầu kỳ"
        onClick={() => {
          setType(1);
          setVisible(true);
        }}
      />
      <Button
        text="Xem đầu kỳ"
        containerStyle={{ margin: '10px' }}
        onClick={() => {
          setType(2);
          getData();
          setVisible(true);
        }}
      />
      {visible && (
        <Modal
          title={type === 1 ? 'Nhập đầu kỳ' : 'Đầu kỳ'}
          visible={visible}
          onOk={form.submit}
          okButtonProps={type === 2 && { style: { display: 'none' } }}
          width={type === 2 ? 800 : 416}
          onCancel={() => {
            setVisible(false);
            setCondition(null);
          }}
        >
          {type !== 2 && (
            <TargetForm
              selectSeason={selectSeason}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              handleChange={handleChange}
              updateValues={type === 3 ? updateTargetValue : undefined}
              form={form}
            />
          )}

          {type === 2 && (
            <Tabs defaultActiveKey="1" onChange={() => {}}>
              {target.length > 0 &&
                target.map(value => (
                  <TabPane tab={`Quý ${value.quarter}`} key={value.quarter}>
                    <Button
                      text="Update"
                      onClick={() => {
                        setVisible(true);
                        setType(3);
                        setUpdateTargetValue(value);
                      }}
                    />
                    <Table
                      bordered
                      columns={targetAccumulationColumns}
                      // scroll={{ x: 'calc(800px + 50%)', y: 450 }}
                      dataSource={[value]}
                      pagination={false}
                    />
                  </TabPane>
                ))}
            </Tabs>
          )}
        </Modal>
      )}
    </div>
  );
};

export default Target;
