/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { Form, Select, Input } from 'antd';
import { Spacer } from '../../../components';

const { Option } = Select;

const TargetForm = ({
  selectSeason,
  onFinish,
  onFinishFailed,
  handleChange,
  form,
  updateValues,
}) => {
  useEffect(() => {
    form.setFieldsValue({
      ...updateValues,
    });
  }, [updateValues]);
  return (
    <>
      <p>Vui lòng chọn quý</p>
      <Select
        defaultValue={updateValues ? updateValues.quarter : undefined}
        disabled={!!updateValues}
        onChange={handleChange}
        style={{ minWidth: 120 }}
      >
        {selectSeason.map(value => (
          <Option key={value.key} value={value.key}>
            {value.label}
          </Option>
        ))}
      </Select>
      <Spacer />
      <Form
        name="basic"
        labelCol={{ span: 16 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        layout="vertical"
        form={form}
      >
        <Form.Item
          label="Đầu kỳ mua"
          name="purchase"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập Đầu kỳ mua!',
            },
            { pattern: new RegExp(/^[0-9]+$/), message: 'Phải là số' },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Đầu kỳ kết nối"
          name="connection"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập Đầu kỳ bán!',
            },
            { pattern: new RegExp(/^[0-9]+$/), message: 'Phải là số' },
          ]}
        >
          <Input />
        </Form.Item>
      </Form>
    </>
  );
};

export default TargetForm;
