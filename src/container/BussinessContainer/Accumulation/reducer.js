/* eslint-disable array-callback-return */
/* eslint-disable no-param-reassign */
const ADD_ACCUMULATION = 'ADD_ACCUMULATION';
const NEW_ACCUMULATION = 'NEW_ACCUMULATION';
const ACCUMULATION = 'ACCUMULATION';
const initialState = [];
export const accumulationReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACCUMULATION:
      state.data.map((value, index) => {
        if (Number(value.id) === Number(action.payload.id)) {
          state.data[index] = action.payload;
        }
      });
      return {
        type: ACCUMULATION,
        data: [...state.data],
      };
    case ADD_ACCUMULATION:
      return {
        type: ADD_ACCUMULATION,
        data: [...state.data, action.payload],
      };
    case NEW_ACCUMULATION:
      return {
        type: NEW_ACCUMULATION,
        data: action.payload,
      };
    default:
      return state;
  }
};
