import {
  getAllStatisticByCondition,
  getAllTransaction,
  getTarget,
  countCurrentHouseQuantity,
  getStatisticHouse,
} from '../../apis/businessApi';
import { showMessage } from '../Common/GlobalErrorModal/action';

const NEW_TRANSACTION = 'NEW_TRANSACTION';
const NEW_ACCUMULATION = 'NEW_ACCUMULATION';
const FETCH_TARGET = 'FETCH_TARGET';
const FETCH_CURRENT_HOUSE_QUANTITY = 'FETCH_CURRENT_HOUSE_QUANTITY';
const FETCH_REMAIN_HOUSE = 'FETCH_REMAIN_HOUSE';

export const fetchAccumulation = (condition, pagination) => async dispatch => {
  const data = await getAllStatisticByCondition({ ...condition, pagiante: pagination });
  try {
    dispatch({
      type: NEW_ACCUMULATION,
      payload: data.data,
    });
  } catch (err) {
    dispatch(showMessage(err));
  }
};

export const fetchTransaction = condition => async dispatch => {
  getAllTransaction(condition)
    .then(values => {
      dispatch({
        type: NEW_TRANSACTION,
        payload: values.data,
      });
    })
    .catch(err => dispatch(showMessage(err)));
};
export const fetchStatisticHouse = condition => dispatch => {
  getStatisticHouse(condition).then(resolve => {
    dispatch({
      type: FETCH_REMAIN_HOUSE,
      payload: resolve.data,
    });
  });
};
export const fetchHouseQuantity = condition => dispatch => {
  countCurrentHouseQuantity(condition).then(resolve => {
    dispatch({
      type: FETCH_CURRENT_HOUSE_QUANTITY,
      payload: resolve.data,
    });
  });
};

export const fetchTarget = condition => async dispatch => {
  getTarget(condition).then(resolve => {
    dispatch({
      type: FETCH_TARGET,
      payload: resolve.data,
    });
  });
};
