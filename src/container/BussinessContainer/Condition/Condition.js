/* eslint-disable no-bitwise */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-shadow */
import { Button, Checkbox, DatePicker, Select } from 'antd';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { connect, useDispatch } from 'react-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { getUserListByManagerId } from '../../../apis/userApi';
import { Card } from '../../../components';
import { select, selectMonth, selectSeason } from '../../../constants/COMMON';
import { getCondition } from '../../../helpers/getCondition';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import './Condition.scss';

const { Option, OptGroup } = Select;
const { RangePicker } = DatePicker;

const mainCard = {
  flex: 1,
  padding: 20,
  minWidth: '100%',
};

const Condition = props => {
  const { message, checkRangePicker } = props;
  const ADD_CONDITION = 'ADD_CONDITION';
  const DEFAULT_CONDITION = 'DEFAULT_CONDITION';
  const dispatch = useDispatch();
  const [checkBox, setCheckBox] = useState({
    fixed: true,
    range: false,
  });
  const [personal, setPersonal] = useState(false);
  const [team, setTeam] = useState(true);
  const [users, setUser] = useState([]);
  const [selectRevenueOption, setSelectRevenueOption] = useState();
  const [selectRevenue, setSelectRevenue] = useState(3);
  const [staff, setStaff] = useState([0]);
  const [condition, setCondition] = useState();
  const [preCondition, setPreCondition] = useState();
  const [manager, setManager] = useState([]);
  const [seniorManager, setSeniorManager] = useState();
  const [saleAdmin, setSaleAdmin] = useState();
  const [admin, setAdmin] = useState();
  const [teamLeader, setTeamLeader] = useState([]);
  const [staffs, setStaffs] = useState([]);
  const [clearStaff, setClearStaff] = useState();
  const [clearDepartment, setClearDepartment] = useState();
  const [rangePickerTime, setRangePickerTime] = useState();
  const date = new Date();
  const currentMonth = date.getMonth();

  const mappingUser = lstUser => {
    const admin = lstUser.data.filter(value => value.job_position === 2);
    const saleAdmin = lstUser.data.filter(value => value.job_position === 3);
    const seniorManager = lstUser.data.filter(value => value.job_position === 4);
    const manager = lstUser.data.filter(value => value.job_position === 5);
    const teamLeader = lstUser.data.filter(value => value.job_position === 6);
    const staffs = lstUser.data.filter(value => value.job_position === 7);
    setAdmin(admin);
    setSaleAdmin(saleAdmin);
    setSeniorManager(seniorManager);
    setManager(manager);
    setTeamLeader(teamLeader);
    setStaffs(staffs);
    setUser(lstUser.data);
  };
  useEffect(() => {
    getUserListByManagerId()
      .then(lstUser => {
        mappingUser(lstUser);
      })
      .catch(() => toast.error(message));
    dispatch({
      type: DEFAULT_CONDITION,
    });
    return () => {
      dispatch({
        type: DEFAULT_CONDITION,
      });
    };
  }, []);

  useEffect(() => {
    try {
      if (checkBox.fixed) {
        const [condition, preCondition] = getCondition(selectRevenue, selectRevenueOption, staff);
        setCondition(condition);
        setPreCondition(preCondition);
      }
    } catch (err) {
      dispatch(showMessage(err));
    }
  }, [selectRevenueOption, selectRevenue === 1, checkBox]); // staff
  const search = () => {
    if (!condition.startAt || !preCondition.startAt || !condition.endAt || !preCondition.endAt) {
      dispatch({
        type: DEFAULT_CONDITION,
      });
    }
    if (condition) {
      dispatch({
        type: ADD_CONDITION,
        payload: {
          condition: { ...condition, team, personal },
          preCondition: { ...preCondition, team, personal },
        },
      });
    }
  };
  function handleChange(value) {
    setSelectRevenue(value);
  }

  const handleChangeSelectRevenueOption = value => {
    setSelectRevenueOption(value);
  };
  const selectStaff = value => {
    setStaff(value);
    setClearStaff(value);
  };
  const selectManagerOnchange = value => {
    setStaff(Array(value));
    setClearDepartment(Array(value));
  };
  const fixedOnChange = value => {
    const checkBox = {
      fixed: value.target.checked,
      range: !value.target.checked,
    };
    setCheckBox(checkBox);
  };
  const rangeDateOnChange = value => {
    if (rangePickerTime) {
      setCondition(rangePickerTime.condition);
      setPreCondition(rangePickerTime.preCondition);
    }
    const checkBox = {
      fixed: !value.target.checked,
      range: value.target.checked,
    };
    setCheckBox(checkBox);
  };
  const typeGetOnchange = value => {
    setPersonal(value.target.checked);
    setTeam(!value.target.checked);
    setClearStaff([]);
  };
  const teamOnchange = value => {
    setClearDepartment([]);
    setPersonal(!value.target.checked);
    setTeam(value.target.checked);
  };
  const rangePickerOnChange = value => {
    if (!value) {
      setCondition({ ...condition, startAt: null, endAt: null });
      setPreCondition({ ...preCondition, startAt: null, endAt: null });
      dispatch(showMessage('Vui lòng chọn ngày'));
    } else {
      const from = value[0];
      const to = value[1];
      const condition = {
        startAt: moment(from)
          .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
          .format(),
        endAt: moment(to)
          .set({ hour: 23, minute: 59, second: 59, millisecond: 59 })
          .format(),
        startQuatter: moment(from).quarter(),
        endQuarter: moment(to).quarter(),
        staff,
      };
      const preCondition = {
        startAt: moment(from)
          .subtract(1, 'months')
          .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
          .format(),
        endAt: moment(to)
          .subtract(1, 'months')
          .set({ hour: 23, minute: 59, second: 59, millisecond: 59 })
          .format(),
        staff,
      };
      setRangePickerTime({ condition, preCondition });
      setCondition(condition);
      setPreCondition(preCondition);
    }
  };
  useEffect(() => {
    setCondition({ ...condition, staff });
    setPreCondition({ ...preCondition, staff });
  }, [staff]);
  return (
    <div>
      <Card shadow containerStyle={mainCard}>
        <div className="menu">
          <div className="dateTime">
            <Checkbox
              onChange={fixedOnChange}
              defaultChecked={checkBox.fixed}
              checked={checkBox.fixed}
            />
            <Select
              disabled={!checkBox.fixed}
              defaultValue={select[2].label}
              className="select"
              // eslint-disable-next-line react/jsx-no-bind
              onChange={handleChange}
            >
              {select.map(value => (
                <Option key={value.key} value={value.key}>
                  {value.label}
                </Option>
              ))}
            </Select>
            {selectRevenue === 3 && (
              <Select
                disabled={!checkBox.fixed}
                defaultValue={currentMonth + 1}
                className="select"
                onChange={handleChangeSelectRevenueOption}
              >
                {selectMonth.map(value => (
                  <Option key={value.key} value={value.key}>
                    {value.label}
                  </Option>
                ))}
              </Select>
            )}
            {selectRevenue === 2 && (
              <Select
                disabled={!checkBox.fixed}
                defaultValue={selectSeason[3].label}
                className="select"
                onChange={handleChangeSelectRevenueOption}
              >
                {selectSeason.map(value => (
                  <Option key={value.key} value={value.key}>
                    {value.label}
                  </Option>
                ))}
              </Select>
            )}
          </div>
          <div className="menuStaff">
            <div className="cmbRangPicker">
              <Checkbox
                onChange={rangeDateOnChange}
                defaultChecked={checkBox.range}
                checked={checkBox.range}
                disabled={checkRangePicker}
              />
              <RangePicker
                onChange={rangePickerOnChange}
                disabled={checkRangePicker || !checkBox.range}
                className="rangPicker"
              />
            </div>
            {users.length > 0 && (
              <div className="department">
                {users.length > 0 && (
                  <Checkbox
                    onChange={teamOnchange}
                    defaultChecked={team}
                    checked={team}
                    className="checkBox"
                  />
                )}
                {users.length > 0 && (
                  <Select
                    disabled={!team}
                    className="selectUser"
                    onChange={selectManagerOnchange}
                    placeholder="Department"
                    value={clearDepartment}
                    showSearch
                    filterOption={(input, option) => {
                      return (
                        (option.name &&
                          option.name.toUpperCase().indexOf(input.toLowerCase()) >= 0) ||
                        (option.name && option.name.toLowerCase().indexOf(input.toLowerCase()) >= 0)
                      );
                    }}
                  >
                    <Option key={0}>Mysefl</Option>
                    {admin.length > 0 && (
                      <OptGroup label="Admin">
                        {admin.map(user => (
                          <Option key={user.id} name={user.name}>
                            {user.name}
                          </Option>
                        ))}
                        ;
                      </OptGroup>
                    )}
                    {saleAdmin.length > 0 && (
                      <OptGroup label="Sale Admin">
                        {saleAdmin.map(user => (
                          <Option key={user.id} name={user.name}>
                            {user.name}
                          </Option>
                        ))}
                        ;
                      </OptGroup>
                    )}
                    {seniorManager.length > 0 && (
                      <OptGroup label="Senior Manager">
                        {seniorManager.map(user => (
                          <Option key={user.id} name={user.name}>
                            {user.name}
                          </Option>
                        ))}
                        ;
                      </OptGroup>
                    )}
                    {manager.length > 0 && (
                      <OptGroup label="Manager">
                        {manager.map(user => (
                          <Option key={user.id} name={user.name}>
                            {user.name}
                          </Option>
                        ))}
                        ;
                      </OptGroup>
                    )}
                    {teamLeader.length > 0 && (
                      <OptGroup label="Team Leader">
                        {teamLeader.map(user => (
                          <Option key={user.id} name={user.name}>
                            {user.name}
                          </Option>
                        ))}
                      </OptGroup>
                    )}
                  </Select>
                )}
              </div>
            )}
            {users.length > 0 && (
              <div className="staff">
                {users.length > 0 && (
                  <Checkbox
                    onChange={typeGetOnchange}
                    defaultChecked={personal}
                    checked={personal}
                    className="checkBox"
                  />
                )}
                {users.length > 0 && (
                  <Select
                    mode="multiple"
                    className="selectUser"
                    disabled={!personal}
                    onChange={selectStaff}
                    placeholder="Personal"
                    value={clearStaff}
                    maxTagCount="responsive"
                    filterOption={(input, option) => {
                      return (
                        (option.name &&
                          option.name.toUpperCase().indexOf(input.toLowerCase()) >= 0) ||
                        (option.name && option.name.toLowerCase().indexOf(input.toLowerCase()) >= 0)
                      );
                    }}
                  >
                    <Option key={0}>Myself</Option>
                    {admin.length > 0 && (
                      <OptGroup label="Admin">
                        {admin.map(user => (
                          <Option key={user.id} name={user.name}>
                            {user.name}
                          </Option>
                        ))}
                        ;
                      </OptGroup>
                    )}
                    {saleAdmin.length > 0 && (
                      <OptGroup label="Sale Manager">
                        {saleAdmin.map(user => (
                          <Option key={user.id} name={user.name}>
                            {user.name}
                          </Option>
                        ))}
                        ;
                      </OptGroup>
                    )}
                    {seniorManager.length > 0 && (
                      <OptGroup label="Senior Manager">
                        {seniorManager.map(user => (
                          <Option key={user.id} name={user.name}>
                            {user.name}
                          </Option>
                        ))}
                        ;
                      </OptGroup>
                    )}
                    {manager.length > 0 && (
                      <OptGroup label="Manager">
                        {manager.map(user => (
                          <Option key={user.id} name={user.name}>
                            {user.name}
                          </Option>
                        ))}
                        ;
                      </OptGroup>
                    )}
                    {teamLeader.length > 0 && (
                      <OptGroup label="Team Leader">
                        {teamLeader.map(user => (
                          <Option key={user.id} name={user.name}>
                            {user.name}
                          </Option>
                        ))}
                      </OptGroup>
                    )}
                    {staffs.length > 0 && (
                      <OptGroup label="Staff">
                        {staffs.map(user => (
                          <Option key={user.id} name={user.name}>
                            {user.name}
                          </Option>
                        ))}
                      </OptGroup>
                    )}
                  </Select>
                )}
              </div>
            )}
            <Button
              className="search"
              type="primary"
              // icon={<SearchOutlined />}
              onClick={() => search()}
            >
              Search
            </Button>
          </div>
        </div>
      </Card>
    </div>
  );
};

Condition.propTypes = {
  message: PropTypes.string,
  checkRangePicker: PropTypes.bool,
};

Condition.defaultProps = {
  message: {
    error: 'Get User Faild!',
  },
  checkRangePicker: false,
};

export function mapStateToProps(state) {
  return {
    me: state.auth.user.data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default React.memo(
  compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Condition),
);
