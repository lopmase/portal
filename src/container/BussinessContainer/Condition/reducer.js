const ADD_CONDITION = 'ADD_CONDITION';
const ADD_PRE_CONDITION = 'ADD_PRE_CONDITION';
const GET_CONDITION = 'GET_CONDITION';
const DEFAULT_CONDITION = 'DEFAULT_CONDITION';
const date = new Date();
const startAt = new Date(date.getFullYear(), date.getMonth(), 1);
const quarter = Math.floor(startAt.getMonth() / 3) + 1;
const endAt = new Date(date.getFullYear(), date.getMonth() + 1, 0, 23, 59, 59);

const preStartAt = new Date(date.getFullYear(), date.getMonth() - 1, 1);
const preEndAt = new Date(date.getFullYear(), date.getMonth(), 0, 23, 59, 59);
const initialState = {
  type: DEFAULT_CONDITION,
  data: {
    condition: { endAt, startAt, team: true, personal: false, staff: [0], quarter },
    preCondition: { startAt: preStartAt, endAt: preEndAt, team: true, personal: false, staff: [0] },
  },
};
// .startAt && action.payload.endAt?action.payload:{...action.payload,  condition.startAt: startAt, condition.endAt:endAt, preCondition.startAt: preStartAt, preCondition.endAt:preEndAt }

export const conditionReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_CONDITION:
      return {
        type: ADD_CONDITION,
        data:
          action.payload.condition.startAt && action.payload.endAt
            ? action.payload
            : {
                condition: { startAt, endAt, ...action.payload.condition },
                preCondition: {
                  startAt: preStartAt,
                  endAt: preEndAt,
                  ...action.payload.preCondition,
                },
              },
      };
    case GET_CONDITION:
      return {
        type: GET_CONDITION,
        data: state.data,
      };
    case DEFAULT_CONDITION:
      return {
        type: DEFAULT_CONDITION,
        data: initialState.data,
      };
    case ADD_PRE_CONDITION:
      return {
        type: ADD_PRE_CONDITION,
        data: state.data,
      };
    default:
      return state;
  }
};
