/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { Flexbox } from '../../components';
import TabRouter from '../../components/Tabs/Tabs';
import {
  connectionAccumulationField,
  purchaseAccumulationField,
  saleAccumulationField,
  transactionField,
} from '../../constants/COMMON';
import { ROUTES } from '../../constants/ROUTES';
import Notification from '../ShellContainner/Header/Notification';
import Layout from '../ShellContainner/Layout/Layout';
// eslint-disable-next-line import/extensions, import/no-unresolved
import NewModal from './Modal/newTransaction.js';
// eslint-disable-next-line import/extensions
import NewTransactionBill from './Modal/newTransactionBill.js';

import NewStatisticModel from './Modal/newStatistic';
import Overview from './OverviewContainer/Container';
import accumulation from './Accumulation/Container';
import transaction from './TransactionSuccess/Container';
import statisticAccumulation from './StatisticAccumulation/Container';
import Target from './Accumulation/Target';
import report from './Report/Container';
import './style.scss';
import { isAdmin } from '../../helpers/roleUtil';

class BussinessContainer extends Component {
  state = {
    tabs: [
      {
        label: 'Tổng quan',
        route: ROUTES.OVERVIEW_BUSINESS_ROOT,
      },
      {
        label: 'Chi tiết Thành công tích lũy',
        route: ROUTES.ACCUMULATION,
      },

      {
        label: 'Giao dịch thành công',
        route: ROUTES.SUCCESSFUL_TRANSACTION,
      },
      {
        label: 'Thống kê nhập Thành công tích lũy',
        route: ROUTES.REPORT_ACCUMULATION,
      },
    ],
  };

  render() {
    const { tabs } = this.state;
    const { me, transactionBill, updateBill, condition } = this.props;

    // eslint-disable-next-line no-undef
    return (
      <div>
        <Layout
          renderHeader={() => (
            <>
              <Flexbox row spaceBetween>
                <h1 style={{ marginBottom: 10 }}>Tổng Quan</h1>
                <Notification />
              </Flexbox>
              <Flexbox row spaceBetween>
                <TabRouter
                  className="tabsAdding"
                  tabs={
                    isAdmin(me)
                      ? [
                          ...tabs,
                          {
                            label: 'Tổng hợp Thành công tích lũy',
                            route: ROUTES.STATISTIC_ACCUMULATION,
                          },
                        ]
                      : tabs
                  }
                />
                {window.location.pathname === '/bussiness/transaction' && (
                  <div className="newTransactionBill">
                    <NewModal
                      field={transactionField}
                      title="Giao dịch thành công"
                      me={me}
                      condition={condition}
                    />
                    <NewTransactionBill
                      type={updateBill}
                      transactionBill={transactionBill}
                      title="Phiếu thu"
                      me={me}
                      condition={condition}
                    />
                  </div>
                )}
                <Flexbox row spaceBetween>
                  {window.location.pathname === '/bussiness/accumulation' && (
                    <Target
                      sale={saleAccumulationField}
                      purchase={purchaseAccumulationField}
                      connection={connectionAccumulationField}
                    />
                  )}
                  {window.location.pathname === '/bussiness/accumulation' && (
                    <NewStatisticModel
                      sale={saleAccumulationField}
                      purchase={purchaseAccumulationField}
                      connection={connectionAccumulationField}
                      title="Thành công tích lũy"
                    />
                  )}
                </Flexbox>
              </Flexbox>
            </>
          )}
        >
          <Switch>
            <Route path={ROUTES.ACCUMULATION} component={accumulation} />
            {isAdmin(me) && (
              <Route path={ROUTES.STATISTIC_ACCUMULATION} component={statisticAccumulation} />
            )}
            <Route path={ROUTES.SUCCESSFUL_TRANSACTION} component={transaction} />
            <Route path={ROUTES.REPORT_ACCUMULATION} component={report} />
            <Route to={ROUTES.OVERVIEW_BUSINESS_ROOT} component={Overview} />
          </Switch>
        </Layout>
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    me: state.auth.user.data,
    transactionBill: state.transactionBill.data,
    updateBill: state.transactionBill.type,
    condition: state.condition.data.condition,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(BussinessContainer);
