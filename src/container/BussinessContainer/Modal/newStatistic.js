/* eslint-disable prefer-destructuring */
/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable no-useless-escape */
/* eslint-disable react/jsx-indent */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-param-reassign */
import { DatePicker, Form, Input, Modal } from 'antd';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { createStatictis } from '../../../apis/businessApi';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { Button } from '../../../components';

const ADD_STATICTIS = 'ADD_STATICTIS';
const NewStatisticModal = props => {
  const dispatch = useDispatch();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [form] = Form.useForm();
  const url = window.location.pathname;
  const {
    backgroundColor,
    name,
    label,
    urlFirstValue,
    sale,
    purchase,
    connection,
    title,
    message,
  } = props;
  const showModal = () => {
    setIsModalVisible(true);
  };
  const handleCancel = () => {
    setIsModalVisible(false);
    form.resetFields();
  };
  const onFinishFailed = errorInfo => {
    toast.error(errorInfo);
  };
  const onFinish = values => {
    values.date = values.date.format();

    createStatictis(values)
      .then(respone => {
        toast.success(message.success);
        values.date = values.date.split('T')[0];
        values.id = respone.data.id;
        values.user_id = respone.data.user_id;
        dispatch({
          type: ADD_STATICTIS,
          payload: values,
        });
        form.resetFields();
      })
      .catch(err => {
        dispatch(showMessage(err.message));
      });
    setIsModalVisible(false);
  };
  const disabledDate = current => {
    return current && current > moment().endOf('day');
  };
  return (
    <div>
      {url !== urlFirstValue && (
        <Button backgroundColor={backgroundColor} text="+ Tạo mới" onClick={showModal} />
      )}
      <Modal
        title={title}
        visible={isModalVisible}
        onCancel={handleCancel}
        onOk={form.submit}
        okButtonProps={{ style: { backgroundColor: '#ff8731' } }}
      >
        <div>
          <Form
            name="basic"
            labelCol={{ span: 16 }}
            wrapperCol={{ span: 16 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
            form={form}
          >
            <Form.Item
              label={label.date}
              name={name.date}
              rules={[
                {
                  required: true,
                  message: 'Ngày',
                },
              ]}
            >
              <DatePicker
                defaulValue={moment().format('L')}
                style={{ width: '100%' }}
                disabledDate={disabledDate}
              />
            </Form.Item>
            <Form.Item
              label={label.marketingQuantity}
              name={name.marketingQuantity}
              rules={[
                { required: true, message: 'Vui lòng nhập Số lượng sản phẩm marketing hôm nay!' },
                { pattern: new RegExp(/^[0-9]+$/), message: 'Phải là số' },
              ]}
            >
              <Input />
            </Form.Item>

            <h3>Nhu cầu bán</h3>
            {sale.map(value => (
              <Form.Item
                label={value.label}
                name={value.name}
                rules={value.rules}
                help={value.help}
                validateStatus={value.validateStatus}
              >
                <Input />
              </Form.Item>
            ))}
            <h3>Nhu cầu mua</h3>
            {purchase.map(value => (
              <Form.Item
                label={value.label}
                name={value.name}
                rules={value.rules}
                help={value.help}
                validateStatus={value.validateStatus}
              >
                <Input />
              </Form.Item>
            ))}
            <h3>Nhu cầu kết nối</h3>
            {connection.map(value => (
              <Form.Item
                label={value.label}
                name={value.name}
                rules={value.rules}
                help={value.help}
                validateStatus={value.validateStatus}
              >
                <Input />
              </Form.Item>
            ))}
          </Form>
        </div>
      </Modal>
    </div>
  );
};
NewStatisticModal.propTypes = {
  title: PropTypes.string,
  sale: PropTypes.object,
  purchase: PropTypes.object,
  connection: PropTypes.object,
  backgroundColor: PropTypes.string,
  name: PropTypes.object,
  urlFirstValue: PropTypes.string,
  label: PropTypes.object,
  message: PropTypes.object,
};

NewStatisticModal.defaultProps = {
  title: ' ',
  sale: {},
  purchase: {},
  connection: {},
  backgroundColor: '#ff8731',
  urlFirstValue: '/bussiness/first-value',
  name: {
    date: 'date',
    notarizedDay: 'notarizedDay',
    brokerageReceiveDay: 'brokerageReceiveDay',
    brokerageFeeReceived: 'brokerageFeeReceived',
    brokerageFee: 'brokerageFee',
    price: 'price',
    marketingQuantity: 'marketingQuantity',
  },
  label: {
    date: 'Ngày',
    brokerageReceiveDay: 'Ngày nhận hoa hồng',
    notarizedDay: 'Ngày công chứng',
    marketingQuantity: 'Lượng sản phẩm đã maketing hôm nay',
  },
  message: {
    notarizedDay: 'Vui lòng nhập Ngày công chứng',
    brokerageReceiveDay: 'Vui lòng nhập Ngày nhận hoa hồng',
    success: 'Tạo mới thành công',
    error: 'Someting was wrong, please try agian!',
  },
};
// export default NewStatisticModal;
export default NewStatisticModal;
