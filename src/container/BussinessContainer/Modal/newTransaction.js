/* eslint-disable react/prop-types */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-undef */
/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable no-useless-escape */
/* eslint-disable react/jsx-indent */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-param-reassign */
import { DatePicker, Form, Input, InputNumber, Modal, Select, Spin } from 'antd';
import moment from 'moment';
import PropTypes from 'prop-types';
import { useDispatch, connect } from 'react-redux';
import React, { useState, useEffect } from 'react';
import { toast } from 'react-toastify';
import _ from 'lodash';
import { MinusCircleOutlined } from '@ant-design/icons';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { createTransactionV2 } from '../../../apis/businessApi';
import { getCustomerByUserId } from '../../../apis/customerApi';
import { getHouse, getUserListByManagerId } from '../../../apis/userApi';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { Button, Flexbox, Dropdown } from '../../../components';
import QuickNewCustomer from '../../CustomerConntainer/QuickNewCustomer/QuickNewCustomer';
import { REDUCERS } from '../../../constants/COMMON';
import { generateModalActions } from '../../../helpers/modalAction';
import { openNewCustomerModal } from '../../CustomerConntainer/NewCustomer/action';
import QuickNewHouse from '../../HouseContainer/QuickNewHouse/QuickNewHouse';
import { getTransactionV2 } from '../TransactionSuccess/action';

const { openModal } = generateModalActions(REDUCERS.QUICK_NEW_CUSTOMER_MODAL);
const openModalCreateHouse = generateModalActions(REDUCERS.QUICK_NEW_HOUSE_MODAL).openModal;

const { Option } = Select;
const ADD_TRANSACTION = 'ADD_TRANSACTION';
// eslint-disable-next-line react/prop-types
const NewModal = ({ backgroundColor, name, label, message, field, title, me, condition }) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const [houseSelected, setHouseSelected] = useState();
  const [houses, setHouses] = useState([]);
  const [users, setUsers] = useState([]);
  const [customers, setCustomer] = useState([]);
  const [customerIdForCreateHouse, setCustomerIdForCreateHouse] = useState([]);
  const [userIdForCreateCustomer, setUserIdForCreateCustomer] = useState({
    userId: null,
    index: null,
  });
  const [usersSelected, setUsersSelected] = useState([]);
  const [loading, setLoading] = useState(false);

  const dispatch = useDispatch();
  const dateFormat = 'YYYY-MM-DD';
  const [form] = Form.useForm();
  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    form.resetFields();
    setIsModalVisible(false);
  };

  const getHouseAddress = async () => {
    const [houseList, userList] = await Promise.all([getHouse(), getUserListByManagerId()]);
    houseList.data.map(value => {
      value.house_address = `${value.house_number} ${value.house_address}, ${value.district}, ${value.province}`;
      return value;
    });
    setUsers(userList.data);
    setHouses(houseList.data);
  };
  useEffect(() => {
    getHouseAddress();
  }, []);
  useEffect(() => {
    console.log('houses', houses);
  }, [houses]);
  const convert = values => {
    !_.isNumber(values.brokerageFee) &&
      (values.brokerageFee = parseInt(values.brokerageFee.replace(/\D/g, ''), 10));
    !_.isNumber(values.price) && (values.price = parseInt(values.price.replace(/\D/g, ''), 10));
    !_.isNumber(values.brokerageFeeReceived) &&
      (values.brokerageFeeReceived = parseInt(values.brokerageFeeReceived.replace(/\D/g, ''), 10));
    Number.isNaN(Number(values.status)) &&
      (values.status = Object.keys(
        optionStatus.find(value => Object.values(value).toString() === values.status),
      )[0]);
    Number.isNaN(Number(values.transactionType)) &&
      (values.transactionType = Object.keys(
        transactionType.find(value => Object.values(value).toString() === values.transactionType),
      )[0]);
    values.brokerageFeeReceivable = parseInt(values.brokerageFee - values.brokerageFeeReceived, 10);
    // values.brokerageFeeStatus = data.brokerageFeeStatus;
    values.date = values.date.format(dateFormat);
    values.notarizedDay = values.notarizedDay.format(dateFormat);
    values.brokerageReceiveDate = values.brokerageReceiveDate.format(dateFormat);
    return values;
  };
  const disabledDate = current => {
    return current && current > moment().endOf('day');
  };
  const onQuickCustomerCreated = newCustomer => {
    const customerFilter = customers.find(
      customer => customer.index === userIdForCreateCustomer.index,
    );
    if (customerFilter) {
      const newCustomers = [...customers];
      newCustomer[customerFilter.index] = [
        customerFilter.customers.push({
          id: newCustomer.id,
          user_id: newCustomer.user_id,
          name: newCustomer.name,
        }),
      ];
      setCustomer(newCustomers);
    } else {
      setCustomer([
        ...customers,
        {
          index: customerFilter.index,
          customers: [
            ...customerFilter.customers,
            { id: newCustomer.id, user_id: newCustomer.user_id, name: newCustomer.name },
          ],
        },
      ]);
    }
    setIsModalVisible(true);
  };
  const onQuickHouseCreated = newHouse => {
    newHouse.house_address = `${newHouse.house_number} ${newHouse.house_address}, ${newHouse.district}, ${newHouse.province}`;
    setHouses([...houses, newHouse]);
    setIsModalVisible(true);
  };
  const onFinish = values => {
    values.brokerageReceiveDate = values.brokerageReceiveDate.format();
    values.date = values.date.format();
    values.notarizedDate = values.notarizedDate.format();
    values.reservationDate = values.reservationDate.format();
    Number(values.brokerageRate);
    createTransactionV2(values)
      .then(response => {
        // values.id = response.data.id;
        // values.user_id = response.data.user_id;
        toast.success('Create SuccessFull');
        // const convertValue = convert(values);
        // dispatch({
        //   type: ADD_TRANSACTION,
        //   payload: response,
        // });
      })
      .catch(err => dispatch(showMessage(err)));
    dispatch(getTransactionV2(condition));
    form.resetFields();
    setIsModalVisible(false);
  };
  const selectUser = async (userId, index) => {
    if (userId) {
      const existedUser = usersSelected.filter(user => user.index === index);
      if (existedUser) {
        setUsersSelected([
          ...usersSelected.filter(user => user.index !== index),
          { index, userId },
        ]);
      } else {
        setUsersSelected(prev => [...prev, { index, userId }]);
      }
      setLoading(true);
      const res = await getCustomerByUserId(userId);
      const existed = customers.filter(customer => customer.index === index);
      if (existed) {
        setCustomer([
          ...customers.filter(customer => customer.index !== index),
          { index, customers: res.data },
        ]);
      } else {
        setCustomer(prev => [...prev, { index, customers: res.data }]);
      }
      setLoading(false);
    }
  };
  const selectCustomer = async (customerId, index) => {
    if (customerId) {
      const existedCustomer = customerIdForCreateHouse.find(customer => customer.index === index);
      if (existedCustomer) {
        setCustomerIdForCreateHouse([
          ...customerIdForCreateHouse.filter(customer => customer.index !== index),
          { index, id: customerId },
        ]);
      } else {
        setCustomerIdForCreateHouse([...customerIdForCreateHouse, { index, id: customerId }]);
      }
    }
  };
  return (
    <div style={{ margin: 10 }}>
      {me && (Number(me.role) === 2 || Number(me.role) === 1 || Number(me.job_position) === 8) && (
        <Button backgroundColor={backgroundColor} text="+ Tạo giao dịch" onClick={showModal} />
      )}
      <Modal
        title={title}
        visible={isModalVisible}
        onCancel={handleCancel}
        onOk={form.submit}
        okButtonProps={{ style: { backgroundColor: '#ff8731' } }}
      >
        <div>
          <Form
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            layout="vertical"
            form={form}
          >
            <Flexbox row spaceBetween>
              <Form.Item
                label={label.date}
                name={name.date}
                rules={[
                  {
                    required: true,
                    message: 'Ngày đặt cọc',
                  },
                ]}
              >
                <DatePicker disabledDate={disabledDate} style={{ width: '100%' }} />
              </Form.Item>

              <Form.Item
                label={label.notarizedDate}
                name={name.notarizedDate}
                rules={[
                  {
                    required: true,
                    message: message.notarizedDate,
                  },
                  ({ getFieldValue }) => ({
                    validator(current, value) {
                      if (value < getFieldValue('date')) {
                        return Promise.reject(new Error('Ngày cọc phải trước ngày công chứng'));
                      }

                      return Promise.resolve();
                    },
                  }),
                ]}
              >
                <DatePicker style={{ width: '100%' }} />
              </Form.Item>
              <Form.Item
                label={label.brokerageReceiveDate}
                name={name.brokerageReceiveDate}
                rules={[
                  {
                    required: true,
                    message: message.brokerageReceiveDate,
                  },
                  ({ getFieldValue }) => ({
                    validator(current, value) {
                      if (value < getFieldValue('date')) {
                        return Promise.reject(new Error('Ngày cọc phải trước Ngày nhận hoa hồng'));
                      }

                      return Promise.resolve();
                    },
                  }),
                ]}
              >
                <DatePicker style={{ width: '100%' }} />
              </Form.Item>
            </Flexbox>
            <Flexbox row spaceBetween>
              <Form.Item
                label="Giá chốt"
                name="price"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập Giá chốt',
                  },
                ]}
              >
                <InputNumber
                  min={0}
                  formatter={val => `VNĐ ${val}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  parser={val => val.replace(/\VNĐ\s?|(,*)/g, '')}
                  required
                  style={{ width: '100%' }}
                />
              </Form.Item>

              <Form.Item
                label="Hoa hồng thực lãnh"
                name="brokerageFee"
                rules={[
                  ({ getFieldValue }) => ({
                    validator(current, value) {
                      if (getFieldValue('brokerageFee') < value) {
                        return Promise.reject(new Error('Không thể lớn hơn hoa hồng thực lãnh'));
                      }

                      return Promise.resolve();
                    },
                  }),
                ]}
              >
                <InputNumber
                  min={0}
                  formatter={val => `VNĐ ${val}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  parser={val => val.replace(/\VNĐ\s?|(,*)/g, '')}
                  required
                  style={{ width: '100%', marginRight: 55 }}
                />
              </Form.Item>
            </Flexbox>
            <Flexbox row spaceBetween>
              <Form.Item label="Ngày giữ chổ" name="reservationDate">
                <DatePicker style={{ width: '100%' }} />
              </Form.Item>
              <Flexbox>
                <Form.Item
                  label="Địa chỉ"
                  name="house_id"
                  // rules={[{ required: true, message: 'Bất động sản không thể trống' }]}
                >
                  <Select
                    placeholder="Chọn bất động sản"
                    style={{ width: 180 }}
                    allowClear
                    showSearch
                    filterOption={(input, option) => {
                      return (
                        (option.children &&
                          option.children.toUpperCase().indexOf(input.toUpperCase()) >= 0) ||
                        (option.children &&
                          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0)
                      );
                    }}
                  >
                    {houses &&
                      houses.map(val => (
                        <Option key={val.id} value={val.id}>
                          {val.house_address}
                        </Option>
                      ))}
                  </Select>
                </Form.Item>
                <Button
                  containerStyle={{ marginTop: 10 }}
                  text="+ Tạo"
                  clear
                  onClick={() => {
                    setIsModalVisible(false);
                    dispatch(openModalCreateHouse());
                  }}
                />
              </Flexbox>
            </Flexbox>

            <Form.List name="users_group">
              {(fields, { add, remove }) => (
                <>
                  {fields.map((f, index) => (
                    <>
                      <MinusCircleOutlined onClick={() => remove(f.name)} />
                      <Flexbox row spaceBetween>
                        <Form.Item
                          label="Tên nhân viên"
                          name={[index, 'user_id']}
                          rules={[{ required: true, message: 'Tên nhân viên không thể trống' }]}
                        >
                          <Select
                            placeholder="Chọn nhân viên"
                            style={{ width: 180 }}
                            allowClear
                            showSearch
                            onChange={value => selectUser(value, index)}
                            filterOption={(input, option) => {
                              return (
                                (option.children &&
                                  option.children.toUpperCase().indexOf(input.toUpperCase()) >=
                                    0) ||
                                (option.children &&
                                  option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0)
                              );
                            }}
                          >
                            {users &&
                              users.map(val => (
                                <Option key={val.id} value={val.id}>
                                  {val.name}
                                </Option>
                              ))}
                          </Select>
                        </Form.Item>
                        <Flexbox>
                          <Form.Item
                            label="Tên khách hàng"
                            name={[index, 'customer_id']}
                            rules={[{ required: true, message: 'Tên khách hàng không thể trống' }]}
                          >
                            <Select
                              placeholder="Chọn khách hàng"
                              style={{ width: 180 }}
                              allowClear
                              showSearch
                              onChange={value => selectCustomer(value, index)}
                              filterOption={(input, option) => {
                                return (
                                  (option.children &&
                                    option.children.toUpperCase().indexOf(input.toUpperCase()) >=
                                      0) ||
                                  (option.children &&
                                    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0)
                                );
                              }}
                            >
                              {customers[index] &&
                                customers[index].customers.map(val => (
                                  <Option key={val.id} value={val.id}>
                                    {val.name}
                                  </Option>
                                ))}
                            </Select>
                          </Form.Item>
                          <Form.Item>
                            <Button
                              containerStyle={{ marginTop: 10 }}
                              text="+ Tạo"
                              clear
                              onClick={() => {
                                setUserIdForCreateCustomer(
                                  usersSelected.find(user => user.index === index),
                                );
                                setIsModalVisible(false);
                                dispatch(openModal());
                              }}
                            />
                          </Form.Item>
                        </Flexbox>
                      </Flexbox>
                      <Flexbox row spaceBetween>
                        <Form.Item label="Loại giao dịch" name={[index, 'transactionType']}>
                          <Select
                            style={{ width: 180 }}
                            placeholder="Chọn Loại giao dịch"
                            allowClear
                          >
                            {[
                              { 1: 'Khách hàng' },
                              { 2: 'Chủ' },
                              { 3: 'Trung gian' },
                              { 4: 'Chính khách - Chính chủ' },
                            ].map(val => (
                              <Option key={Object.keys(val)[0]} value={Object.keys(val)[0]}>
                                {Object.values(val)[0]}
                              </Option>
                            ))}
                          </Select>
                        </Form.Item>
                        <Form.Item
                          label="Tỷ lệ hoa hồng"
                          name={[index, 'brokerageRate']}
                          rules={[{ required: true, message: 'Tỷ lệ hoa hồng không thể trống' }]}
                        >
                          <Input
                            style={{ width: 180, marginRight: 60 }}
                            placeholder="Tỷ lệ hoa hồng"
                          />
                        </Form.Item>
                        <Form.Item
                          label="Hoa hồng thực lãnh"
                          name={[index, 'brokerageFeeUser']}
                          rules={[
                            { required: true, message: 'Hoa hồng thực lãnh không thể trống' },
                          ]}
                        >
                          <InputNumber
                            min={0}
                            formatter={val => `VNĐ ${val}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            parser={val => val.replace(/\VNĐ\s?|(,*)/g, '')}
                            required
                            style={{ width: '100%' }}
                          />
                        </Form.Item>
                        <Form.Item
                          label="Mã giao dịch"
                          name={[index, 'transactionCode']}
                          rules={[
                            {
                              required: true,
                              message: 'Vui lòng nhập Mã giao dịch',
                            },
                          ]}
                        >
                          <Input style={{ width: 180, marginRight: 60 }} />
                        </Form.Item>
                        <Form.Item label="Trạng thái giao dịch" name={[index, 'status']}>
                          <Select
                            style={{ width: 180, marginRight: 60 }}
                            placeholder="Trạng thái giao dịch"
                            allowClear
                          >
                            {[
                              { 1: 'Giữ chổ' },
                              { 2: 'Đặt cọc' },
                              { 3: 'Công chứng hợp đồng mua bán' },
                              { 4: 'Đăng bộ sang tên' },
                            ].map(val => (
                              <Option key={Object.keys(val)[0]} value={Object.keys(val)[0]}>
                                {Object.values(val)[0]}
                              </Option>
                            ))}
                          </Select>
                        </Form.Item>
                      </Flexbox>
                    </>
                  ))}
                  <Form.Item>
                    <Button icon={faPlusCircle} onClick={() => add()} text="Thêm nhân viên">
                      Thêm nhân viên
                    </Button>
                  </Form.Item>
                </>
              )}
            </Form.List>
          </Form>
        </div>
      </Modal>
      <QuickNewCustomer
        userId={userIdForCreateCustomer.userId}
        onAfterCreated={newCustomer => {
          if (newCustomer) {
            onQuickCustomerCreated(newCustomer);
          }
        }}
      />
      <QuickNewHouse
        onAfterCreated={newCustomer => {
          if (newCustomer) {
            console.log('onafter');
            onQuickHouseCreated(newCustomer);
          }
        }}
      />
    </div>
  );
};
NewModal.propTypes = {
  title: PropTypes.string,
  field: PropTypes.object,
  backgroundColor: PropTypes.string,
  name: PropTypes.object,
  label: PropTypes.object,
  message: PropTypes.object,
};
NewModal.defaultProps = {
  title: ' ',
  field: {},
  backgroundColor: '#ff8731',
  name: {
    date: 'date',
    notarizedDate: 'notarizedDate',
    brokerageReceiveDate: 'brokerageReceiveDate',
    brokerageFeeReceived: 'brokerageFeeReceived',
    brokerageFee: 'brokerageFee',
    price: 'price',
  },
  label: {
    brokerageReceiveDate: 'Ngày nhận hoa hồng',
    notarizedDate: 'Ngày công chứng',
    date: 'Ngày đặt cọc',
  },
  message: {
    notarizedDate: 'Vui lòng nhập Ngày công chứng',
    brokerageReceiveDate: 'Vui lòng nhập Ngày nhận hoa hồng',
    success: 'Tạo mới thành công',
    error: 'Someting was wrong, please try agian!',
  },
};

export default NewModal;
