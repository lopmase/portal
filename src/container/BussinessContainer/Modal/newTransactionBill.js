/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
import React, { useState, useEffect, useCallBack } from 'react';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import PropTypes from 'prop-types';
import { useDispatch, connect } from 'react-redux';
import { DatePicker, Form, Input, InputNumber, Modal, Select, Spin } from 'antd';
import moment from 'moment';
import * as action from '../TransactionSuccess/action';
import { createReceipts, updateReceipts } from '../../../apis/businessApi';
import { REDUCERS } from '../../../constants/COMMON';
import { generateModalActions } from '../../../helpers/modalAction';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { Button, Flexbox, Dropdown } from '../../../components';

// eslint-disable-next-line react/prop-types
const NewTransactionBill = ({
  backgroundColor,
  label,
  name,
  title,
  transactionBill,
  getReceipt,
  type,
  dataUpdate,
  getTransactionV2,
  me,
  condition,
}) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [isModalVisible, setIsModalVisible] = useState(
    type === 'UPDATE_BILL' && me && (me.role === 2 || me.role === 1 || me.job_position === 8),
  );

  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const disabledDate = current => {
    return current && current > moment().endOf('day');
  };
  useEffect(() => {
    setIsModalVisible(
      type === 'UPDATE_BILL' && me && (me.role === 2 || me.role === 1 || me.job_position === 8),
    );
  }, [type]);

  useEffect(() => {
    if (dataUpdate) {
      form.setFieldsValue({
        basic_code: dataUpdate && dataUpdate.feeCode,
        basic_date: dataUpdate && moment(dataUpdate.dateCollect, 'YYYY-MM-DD'),
        price: dataUpdate && dataUpdate.feePayable,
        priceReceived: dataUpdate && dataUpdate.feeCollect,
        customer: dataUpdate && dataUpdate.detail.customer_name,
        staff_id: dataUpdate && dataUpdate.detail.name,
        address:
          dataUpdate &&
          `${dataUpdate.house.house_number} ${dataUpdate.house.house_address} ${dataUpdate.house.district}`,
      });
    }
  }, [dataUpdate]);

  const showModal = () => {
    setIsModalVisible(true);
    form.setFieldsValue({
      customer: transactionBill.length > 0 ? transactionBill[0].customer.name : '',
      staff_id: transactionBill.length > 0 ? transactionBill[0].staff.name : '',
      address: transactionBill.length > 0 ? transactionBill[0].address : '',
      price: transactionBill.length > 0 ? transactionBill[0].brokerageFeeUser : '',
    });
  };

  const handleCancel = () => {
    form.resetFields();
    setIsModalVisible(false);
    dispatch({ type: 'RESET_TYPE' });
  };
  const onFinish = values => {
    values.basic_date = values.basic_date.format().split('T')[0];
    // eslint-disable-next-line no-param-reassign
    console.log('values', values);
    // eslint-disable-next-line no-param-reassign
    // values.transaction_id = transactionBill[0].id;
    if (type === 'UPDATE_BILL') {
      values.id = dataUpdate.id;
      values.staff_id = dataUpdate.staff_id;
      values.customer = dataUpdate.customer_id;
      values.address = dataUpdate.house_id;
      values.user_id = dataUpdate.user_id;
      values.transaction_id = dataUpdate.transaction_id;

      updateReceipts(values)
        .then(response => {
          toast.success('Update SuccessFull');
        })
        .catch(err => dispatch(showMessage(err)));
    } else {
      values.staff_id = transactionBill[0].staff_id;
      values.customer = transactionBill[0].customer_id;
      values.address = transactionBill[0].house_id;
      values.user_id = transactionBill[0].user.id;
      values.transaction_id = transactionBill[0].id;

      createReceipts(values)
        .then(response => {
          toast.success('Create SuccessFull');
        })
        .catch(err => dispatch(showMessage(err)));
    }
    form.resetFields();
    setIsModalVisible(false);
    getReceipt();
    getTransactionV2(condition);
  };
  return (
    <div style={{ margin: 10 }}>
      {me && (me.role === 2 || me.role === 1 || me.job_position === 8) && (
        <Button backgroundColor={backgroundColor} text="+ Tạo phiếu thu" onClick={showModal} />
      )}

      <Modal
        title={title}
        visible={isModalVisible}
        onCancel={handleCancel}
        onOk={form.submit}
        okButtonProps={{ style: { backgroundColor: '#ff8731' } }}
      >
        <div>
          <Form
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            layout="vertical"
            form={form}
          >
            <Flexbox row spaceBetween>
              <Form.Item
                style={{ width: '45%' }}
                // eslint-disable-next-line react/prop-types
                label={label.date}
                // eslint-disable-next-line react/prop-types
                name={name.date}
                rules={[
                  {
                    required: true,
                    message: 'Ngày thu',
                  },
                ]}
              >
                <DatePicker style={{ width: '100%' }} />
              </Form.Item>
              <Form.Item
                style={{ width: '45%' }}
                label="Mã phiếu"
                // eslint-disable-next-line react/prop-types
                name={name.code}
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng mã phiếu thu',
                  },
                ]}
              >
                <Input required style={{ width: '100%' }} />
              </Form.Item>
            </Flexbox>
            <Flexbox row spaceBetween>
              <Form.Item
                style={{ width: '45%' }}
                label="Phí phải thu"
                name="price"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập phí phải thu',
                  },
                ]}
              >
                <InputNumber
                  min={0}
                  formatter={val => `VNĐ ${val}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  parser={val => val.replace(/\VNĐ\s?|(,*)/g, '')}
                  required
                  disabled
                  style={{ width: '100%' }}
                />
              </Form.Item>
              <Form.Item
                style={{ width: '45%' }}
                label="Phí đã thu "
                name="priceReceived"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập phí đã thu',
                  },
                  ({ getFieldValue }) => ({
                    validator(current, value) {
                      if (value > getFieldValue('price')) {
                        return Promise.reject(new Error('Phải nhỏ hơn phí phải thu'));
                      }

                      return Promise.resolve();
                    },
                  }),
                ]}
              >
                <InputNumber
                  min={0}
                  formatter={val => `VNĐ ${val}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  parser={val => val.replace(/\VNĐ\s?|(,*)/g, '')}
                  required
                  style={{ width: '100%' }}
                />
              </Form.Item>
            </Flexbox>

            <Flexbox row spaceBetween>
              <Form.Item
                style={{ width: '30%' }}
                label="Nhân viên"
                // eslint-disable-next-line react/prop-types
                name="staff_id"
                // rules={[
                //   {
                //     required: true,
                //     message: 'Vui lòng chọn nhân viên',
                //   },
                // ]}
              >
                <Input
                  // eslint-disable-next-line react/prop-types
                  // defaultValue={dataForm.staff}
                  required
                  style={{ width: '100%' }}
                  disabled
                />
              </Form.Item>
              <Form.Item
                style={{ width: '30%' }}
                // eslint-disable-next-line react/prop-types
                label="Khách hàng"
                // eslint-disable-next-line react/prop-types
                name="customer"
                // rules={[
                //   {
                //     required: true,
                //     message: 'Vui lòng chọn khách hàng',
                //   },
                // ]}
              >
                <Input
                  // eslint-disable-next-line react/prop-types
                  // defaultValue={dataForm.customer}
                  required
                  style={{ width: '100%' }}
                  disabled
                />
              </Form.Item>

              <Form.Item
                style={{ width: '30%' }}
                label="Địa chỉ"
                // eslint-disable-next-line react/prop-types
                name="address"
                // rules={[
                //   {
                //     required: true,
                //     message: 'Vui lòng chọn địa chỉ',
                //   },
                // ]}
              >
                <Input
                  // eslint-disable-next-line react/prop-types
                  // defaultValue={dataForm.address}
                  required
                  style={{ width: '100%' }}
                  disabled
                />
              </Form.Item>
            </Flexbox>
          </Form>
        </div>
      </Modal>
    </div>
  );
};
NewTransactionBill.defaultProps = {
  title: ' ',
  field: {},
  backgroundColor: '#ff8731',
  name: {
    date: 'basic_date',
    notarizedDate: 'notarizedDate',
    brokerageReceiveDate: 'brokerageReceiveDate',
    brokerageFeeReceived: 'brokerageFeeReceived',
    brokerageFee: 'brokerageFee',
    price: 'price',
    code: 'basic_code',
  },
  label: {
    brokerageReceiveDate: 'Ngày nhận hoa hồng',
    notarizedDate: 'Ngày công chứng',
    date: 'Ngày thu',
  },
  message: {
    notarizedDate: 'Vui lòng nhập Ngày công chứng',
    brokerageReceiveDate: 'Vui lòng nhập Ngày nhận hoa hồng',
    success: 'Tạo mới thành công',
    error: 'Someting was wrong, please try agian!',
  },
};
export function mapStateToProps(state) {
  return {
    // transactionBill: state.transactionBill.data,
    dataUpdate: state.transactionBill.dataUpdate,
    state,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...action,
    },
    dispatch,
  );
}
export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(NewTransactionBill);
