/* eslint-disable no-param-reassign */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-useless-escape */
import { DatePicker, Form, Input, InputNumber, Modal, Select } from 'antd';
import PropTypes from 'prop-types';
import _ from 'lodash';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { updateTransaction, updateStatistic } from '../../../apis/businessApi';
import { transactionType } from '../../../constants/COMMON';
import { showMessage } from '../../Common/GlobalErrorModal/action';

const { Option } = Select;

const TRANSACTION = 'TRANSACTION';
const ACCUMULATION = 'ACCUMULATION';
const UpdateModal = props => {
  const dispatch = useDispatch();
  const dateFormat = 'YYYY-MM-DD';
  const { columns } = props;
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [data, setData] = useState(props.field);
  const [type, setType] = useState({});
  const [form] = Form.useForm();
  const { message, name, label, optionStatus } = props;
  useEffect(() => {
    setType(props.type);
    form.resetFields();
    props.field.date = moment(props.field.date, dateFormat);
    setData(props.field);
    setIsModalVisible(true);
  }, [props.field, data, props.type, form]);
  const handleCancel = () => {
    setIsModalVisible(false);
    form.resetFields();
  };

  const onFinish = values => {
    try {
      if (type === name.transaction) {
        /* eslint-disable no-param-reassign */
        !_.isNumber(values.brokerageFee) &&
          (values.brokerageFee = parseInt(values.brokerageFee.replace(/\D/g, ''), 10));
        !_.isNumber(values.price) && (values.price = parseInt(values.price.replace(/\D/g, ''), 10));
        !_.isNumber(values.brokerageFeeReceived) &&
          (values.brokerageFeeReceived = parseInt(
            values.brokerageFeeReceived.replace(/\D/g, ''),
            10,
          ));
        Number.isNaN(Number(values.status)) &&
          (values.status = Object.keys(
            optionStatus.find(value => Object.values(value).toString() === values.status),
          )[0]);
        Number.isNaN(Number(values.transactionType)) &&
          (values.transactionType = Object.keys(
            transactionType.find(
              value => Object.values(value).toString() === values.transactionType,
            ),
          )[0]);
        values.id = data.id;
        values.user_id = data.user_id;
        values.brokerageFeeReceivable = parseInt(
          values.brokerageFee - values.brokerageFeeReceived,
          10,
        );
        // values.brokerageFeeStatus = data.brokerageFeeStatus;
        values.date = values.date.format(dateFormat);
        values.notarizedDay = values.notarizedDay.format(dateFormat);
        values.brokerageReceiveDay = values.brokerageReceiveDay.format(dateFormat);
        updateTransaction(values)
          .then(() => {
            toast.success('Update thành công');

            dispatch({
              type: TRANSACTION,
              payload: {
                ...values,
                brokerageFeeStatus: data.brokerageFeeStatus,
                status: values.status || data.status,
              },
            });
          })
          .catch(() => toast.error('Something was wrong'));
      }
      if (type === name.accumulation) {
        values.id = data.id;
        values.user_id = data.user_id;
        values.date = values.date.format(dateFormat);
        updateStatistic(values)
          .then(() => {
            toast.success('Update thành công');
            dispatch({
              type: ACCUMULATION,
              payload: values,
            });
          })
          .catch(err =>
            err.message ? dispatch(showMessage(err.message)) : toast.error('Something wrong'),
          );
      }
      /* eslint-disable no-param-reassign */
      setIsModalVisible(false);
    } catch (err) {
      toast.error('Something was wrong. Please try agian');
    }
  };
  const disabledDate = current => {
    return current && current > moment().endOf('day');
  };
  const onFinishFailed = errorInfo => {
    toast.error(errorInfo);
  };
  const onChange = allValue => {
    allValue.date != null &&
      allValue.notarizedDay != null &&
      allValue.date > allValue.notarizedDay &&
      onFinishFailed('Ngày cọc phải trước ngày công chứng');
  };

  return (
    <>
      <Modal
        title={label.update}
        visible={isModalVisible}
        onCancel={handleCancel}
        onOk={form.submit}
        okButtonProps={{ style: { backgroundColor: '#ff8731' } }}
      >
        <div>
          <Form
            name="basic"
            labelCol={{ span: 16 }}
            wrapperCol={{ span: 16 }}
            onFinish={onFinish}
            onValuesChange={onChange}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
            form={form}
            initialValues={
              (type === name.transaction && {
                brokerageFee: data.brokerageFee,
                price: data.price,
                address: data.address,
                note: data.note,
                brokerageFeeReceived: data.brokerageFeeReceived,
                date: moment(data.date, dateFormat),
                brokerageReceiveDay: moment(data.brokerageReceiveDay, dateFormat),
                notarizedDay: moment(data.notarizedDay, dateFormat),
                transactionType: data.detail.transactionType,
                status: data.status,
                customer_id: data.detail.customer_name,
                user_id: data.detail.name,
                remember: true,
                brokerageRate: data.detail.brokerageRate,
              }) ||
              (type === name.accumulation && {
                ...data,
                remember: true,
              })
            }
          >
            <Form.Item
              label={type === name.transaction ? 'Ngày cọc' : 'Ngày'}
              rules={[
                {
                  required: true,
                  message: message.date,
                },
              ]}
              name="date"
            >
              <DatePicker
                disabledDate={disabledDate}
                format={dateFormat}
                style={{ width: '100%' }}
              />
            </Form.Item>
            {type === name.transaction && (
              <>
                <Form.Item
                  label={label.notarizedDay}
                  rules={[
                    {
                      required: true,
                      message: message.notarizedDay,
                    },
                  ]}
                  name={name.notarizedDay}
                >
                  <DatePicker style={{ width: '100%' }} />
                </Form.Item>
                <Form.Item
                  label={label.brokerageReceiveDay}
                  rules={[
                    {
                      required: true,
                      message: message.brokerageReceiveDay,
                    },
                  ]}
                  name={name.brokerageReceiveDay}
                >
                  <DatePicker format={dateFormat} style={{ width: '100%' }} />
                </Form.Item>
                <Form.Item label={label.price} name={name.price}>
                  <InputNumber
                    min={0}
                    formatter={value => `VNĐ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                    parser={value => value.replace(/\VNĐ\s?|(,*)/g, '')}
                    required
                    style={{ width: '100%' }}
                  />
                </Form.Item>
                <Form.Item label={label.brokerageFee} name={name.brokerageFee}>
                  <InputNumber
                    min={0}
                    formatter={value => `VNĐ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                    parser={value => value.replace(/\VNĐ\s?|(,*)/g, '')}
                    required
                    style={{ width: '100%' }}
                  />
                </Form.Item>
                <Form.Item label={label.brokerageFeeReceived} name={name.brokerageFeeReceived}>
                  <InputNumber
                    min={0}
                    formatter={value => `VNĐ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                    parser={value => value.replace(/\VNĐ\s?|(,*)/g, '')}
                    required
                    style={{ width: '100%' }}
                  />
                </Form.Item>
                <Form.Item
                  rules={[
                    {
                      required: true,
                      message: message.required,
                    },
                  ]}
                  label={label.address}
                  name={name.address}
                >
                  <Input />
                </Form.Item>
                <Form.Item label={label.transactionType} name={name.transactionType}>
                  <Select>
                    {transactionType.map(val => (
                      // eslint-disable-next-line react/jsx-one-expression-per-line
                      <Option value={Object.keys(val)[0]}> {Object.values(val)[0]} </Option>
                    ))}
                  </Select>
                </Form.Item>
                <Form.Item label={label.status} name={name.status}>
                  <Select>
                    {optionStatus.map(val => (
                      // eslint-disable-next-line react/jsx-one-expression-per-line
                      <Option value={Object.keys(val)[0]}> {Object.values(val)[0]} </Option>
                    ))}
                  </Select>
                </Form.Item>
                <Form.Item label={label.note} name="note">
                  <Input />
                </Form.Item>
              </>
            )}
            {type === name.accumulation &&
              columns.map(value => (
                <Form.Item
                  rules={[
                    {
                      pattern: new RegExp(/^[0-9]+$/),
                      message: 'Phải là số',
                    },
                    {
                      required: true,
                      message: 'Vui lòng nhập Số lượng',
                    },
                  ]}
                  label={value.label}
                  name={value.name}
                >
                  <InputNumber min={0} style={{ width: '100%' }} />
                </Form.Item>
              ))}
          </Form>
        </div>
      </Modal>
      ;
    </>
  );
};
UpdateModal.propTypes = {
  columns: PropTypes.any,
  field: PropTypes.any.isRequired,
  type: PropTypes.string.isRequired,
  label: PropTypes.object,
  message: PropTypes.object,
  name: PropTypes.object,
  optionStatus: PropTypes.array,
};

UpdateModal.defaultProps = {
  columns: {},
  label: {
    update: 'Update',
    notarizedDay: 'Ngày công chứng',
    brokerageReceiveDay: 'Ngày nhận hoa hồng',
    price: 'Giá chốt',
    brokerageFee: 'Hoa hồng thực lãnh',
    brokerageFeeReceived: 'Hoa hồng đã nhận',
    address: 'Địa chỉ',
    transactionType: 'Loại giao dịch',
    status: 'Trạng thái giao dịch',
    note: 'Ghi chú',
  },
  name: {
    notarizedDay: 'notarizedDay',
    brokerageReceiveDay: 'brokerageReceiveDay',
    price: 'price',
    brokerageFee: 'brokerageFee',
    brokerageFeeReceived: 'brokerageFeeReceived',
    address: 'address',
    transactionType: 'transactionType',
    status: 'status',
    note: 'note',
    accumulation: 'accumulation',
    transaction: 'transaction',
  },
  message: {
    date: 'Vui lòng nhập Ngày',
    notarizedDay: 'Vui lòng nhập Ngày công chứng',
    brokerageReceiveDay: 'Vui lòng nhập Ngày nhận hoa hồng',
  },
  optionStatus: [
    { 1: 'Giữ chổ' },
    { 2: 'Đặt cọc' },
    { 3: 'Công chứng hợp đồng mua bán' },
    { 4: 'Đăng bộ sang tên' },
  ],
};
export default UpdateModal;
