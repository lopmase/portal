import { DatePicker, Form, Input, InputNumber, Modal, Select } from 'antd';
import PropTypes from 'prop-types';
import _ from 'lodash';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { updateTransaction, updateStatistic, updateTransactionV2 } from '../../../apis/businessApi';
import { transactionType } from '../../../constants/COMMON';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { Button, Flexbox, Dropdown } from '../../../components';
import { getHouse, getUserListByManagerId } from '../../../apis/userApi';
import { getCustomerByUserId } from '../../../apis/customerApi';
import './Modal.scss';
import { getTransactionV2 } from '../TransactionSuccess/action';

const { Option } = Select;
const TRANSACTION = 'TRANSACTION';
const ACCUMULATION = 'ACCUMULATION';
const UpdateTransaction = props => {
  const dispatch = useDispatch();
  const dateFormat = 'YYYY-MM-DD';
  const { columns } = props;
  const [usersSelected, setUsersSelected] = useState();
  const [customers, setCustomer] = useState([]);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [data, setData] = useState(props.field);
  const [type, setType] = useState({});
  const [houses, setHouses] = useState([]);
  const [users, setUsers] = useState([]);
  const [form] = Form.useForm();
  const { message, name, label, optionStatus, condition } = props;
  useEffect(() => {
    setType(props.type);
    form.resetFields();
    props.field.date = moment(props.field.date, dateFormat);
    setData(props.field);
    setIsModalVisible(true);
  }, [props.field, props.type, form]);
  const handleCancel = () => {
    setIsModalVisible(false);
    form.resetFields();
  };

  useEffect(() => {
    form.setFieldsValue({
      brokerageFee: props.field.brokerageFee,
      price: props.field.price,
      house_id: props.field.address,
      // note: props.field.note,
      brokerageFeeReceived: props.field.brokerageFeeReceived,
      date: moment(props.field.date, dateFormat),
      brokerageReceiveDate: moment(props.field.brokerageReceiveDate, dateFormat),
      notarizedDate: moment(props.field.notarizedDate, dateFormat),
      reservationDate: moment(props.field.reservationDate, dateFormat),
      transactionType: props.field.transaction_type,
      status: props.field.status,
      staff_id: props.field.staff.name,
      transactionCode: props.field.transactionCode,
      customer_id: props.field.customer.name,
      brokerageRate: props.field.brokerage_rate,
      brokerageFeeUser: props.field.brokerageFeeUser,
      // remember: true
    });
  }, [form, props.field]);

  useEffect(() => {
    const getHouseAddress = async () => {
      const [houseList, userList, customerList] = await Promise.all([
        getHouse(),
        getUserListByManagerId(),
        getCustomerByUserId(props.field.staff.id),
      ]);
      houseList.data.map(value => {
        // eslint-disable-next-line no-param-reassign
        value.house_address = `${value.house_number} ${value.house_address}, ${value.district}, ${value.province}`;
        return value;
      });
      setCustomer(customerList.data);
      setUsers(userList.data);
      setHouses(houseList.data);
    };
    getHouseAddress();
  }, [props.field.staff.id]);
  console.log('dataUpdate', data);

  const selectUser = async userId => {
    setUsersSelected(userId);
    const res = await getCustomerByUserId(userId);
    // form.setFieldsValue({
    //   customer_id: res.data[0].name,
    // });
    setCustomer(res.data);
  };

  // const onFinish = values => {
  //   try {
  //     if (type === name.transaction) {
  //       /* eslint-disable no-param-reassign */
  //       !_.isNumber(values.brokerageFee) &&
  //         (values.brokerageFee = parseInt(values.brokerageFee.replace(/\D/g, ''), 10));
  //       !_.isNumber(values.price) && (values.price = parseInt(values.price.replace(/\D/g, ''), 10));
  //       !_.isNumber(values.brokerageFeeReceived) &&
  //         (values.brokerageFeeReceived = parseInt(
  //           values.brokerageFeeReceived.replace(/\D/g, ''),
  //           10,
  //         ));
  //       Number.isNaN(Number(values.status)) &&
  //         (values.status = Object.keys(
  //           optionStatus.find(value => Object.values(value).toString() === values.status),
  //         )[0]);
  //       Number.isNaN(Number(values.transactionType)) &&
  //         (values.transactionType = Object.keys(
  //           transactionType.find(
  //             value => Object.values(value).toString() === values.transactionType,
  //           ),
  //         )[0]);
  //       values.id = data.id;
  //       values.user_id = data.user_id;
  //       values.brokerageFeeReceivable = parseInt(
  //         values.brokerageFee - values.brokerageFeeReceived,
  //         10,
  //       );
  //       // values.brokerageFeeStatus = data.brokerageFeeStatus;
  //       values.date = values.date.format(dateFormat);
  //       values.notarizedDay = values.notarizedDay.format(dateFormat);
  //       values.brokerageReceiveDay = values.brokerageReceiveDay.format(dateFormat);
  //       updateTransaction(values)
  //         .then(() => {
  //           toast.success('Update thành công');

  //           dispatch({
  //             type: TRANSACTION,
  //             payload: {
  //               ...values,
  //               brokerageFeeStatus: data.brokerageFeeStatus,
  //               status: values.status || data.status,
  //             },
  //           });
  //         })
  //         .catch(() => toast.error('Something was wrong'));
  //     }

  //     /* eslint-disable no-param-reassign */
  //     setIsModalVisible(false);
  //   } catch (err) {
  //     toast.error('Something was wrong. Please try agian');
  //   }
  // };
  const onFinish = values => {
    try {
      _.isNumber(values.brokerageFee) &&
        // eslint-disable-next-line no-param-reassign
        (values.brokerageFee = parseInt(values.brokerageFee, 10));
      _.isNumber(values.price) && (values.price = parseInt(values.price, 10));
      _.isNumber(values.brokerageFeeReceived) &&
        (values.brokerageFeeReceived = parseInt(values.brokerageFeeReceived, 10));
      Number.isNaN(Number(values.status)) &&
        (values.status = Object.keys(
          optionStatus.find(value => Object.values(value).toString() === values.status),
        )[0]);
      Number.isNaN(Number(values.transactionType)) &&
        (values.transactionType = Object.keys(
          transactionType.find(value => Object.values(value).toString() === values.transactionType),
        )[0]);
      values.id = data.id;
      if (values.staff_id === data.staff.name) {
        values.staff_id = data.staff_id;
      }
      if (values.customer_id === data.customer.name) {
        values.customer_id = data.customer_id;
      }
      if (values.house_id === data.address) {
        values.house_id = data.house_id;
      }
      // values.staff_id = data.staff_id;
      // values.customer_id = data.customer_id;

      // values.brokerageFeeReceivable = parseInt(
      //   values.brokerageFee - values.brokerageFeeReceived,
      //   10,
      // );
      values.reservationDate = values.reservationDate
        ? values.reservationDate.format(dateFormat).split("T")[0]
        : 0;
      values.brokerageFeeUser = parseInt(values.brokerageFeeUser, 10);

      // values.brokerageFeeStatus = data.brokerageFeeStatus;
      values.date = values.date.format(dateFormat).split("T")[0];
      values.notarizedDate = values.notarizedDate.format(dateFormat).split("T")[0];
      // eslint-disable-next-line no-param-reassign
      values.brokerageReceiveDate = values.brokerageReceiveDate.format(dateFormat).split("T")[0];
      updateTransactionV2(values)
        .then(() => {
          toast.success('Update thành công');
        })
        .catch(() => toast.error('Something was wrong'));
    } catch (error) {
      console.log(error);
    }
    dispatch(getTransactionV2(condition));
    setIsModalVisible(false);
  };
  const disabledDate = current => {
    return current && current > moment().endOf('day');
  };
  const onFinishFailed = errorInfo => {
    toast.error(errorInfo);
  };
  const onChange = allValue => {
    allValue.date != null &&
      allValue.notarizedDay != null &&
      allValue.date > allValue.notarizedDay &&
      onFinishFailed('Ngày cọc phải trước ngày công chứng');
  };
  return (
    <>
      <Modal
        title={label.update}
        visible={isModalVisible}
        onCancel={handleCancel}
        onOk={form.submit}
        okButtonProps={{ style: { backgroundColor: '#ff8731' } }}
      >
        <div>
          <Form
            name="basic"
            // labelCol={{ span: 16 }}
            // wrapperCol={{ span: 16 }}
            onFinish={onFinish}
            onValuesChange={onChange}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
            form={form}
          >
            <Flexbox row spaceBetween>
              <Form.Item
                label="Ngày đặt cọc"
                name="date"
                rules={[
                  {
                    required: true,
                    message: 'Ngày đặt cọc',
                  },
                ]}
              >
                <DatePicker disabledDate={disabledDate} style={{ width: '100%' }} />
              </Form.Item>

              <Form.Item
                label="Ngày công chứng"
                name="notarizedDate"
                rules={[
                  {
                    required: true,
                    message: message.notarizedDate,
                  },
                  ({ getFieldValue }) => ({
                    validator(current, value) {
                      if (value < getFieldValue('date')) {
                        return Promise.reject(new Error('Ngày cọc phải trước ngày công chứng'));
                      }

                      return Promise.resolve();
                    },
                  }),
                ]}
              >
                <DatePicker style={{ width: '100%' }} />
              </Form.Item>
              <Form.Item
                label="Ngày nhận hoa hồng"
                name="brokerageReceiveDate"
                rules={[
                  {
                    required: true,
                    message: message.brokerageReceiveDate,
                  },
                  ({ getFieldValue }) => ({
                    validator(current, value) {
                      if (value < getFieldValue('date')) {
                        return Promise.reject(new Error('Ngày cọc phải trước Ngày nhận hoa hồng'));
                      }

                      return Promise.resolve();
                    },
                  }),
                ]}
              >
                <DatePicker style={{ width: '100%' }} />
              </Form.Item>
            </Flexbox>
            <Flexbox row spaceBetween>
              <Form.Item
                label="Giá chốt"
                name="price"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập Giá chốt',
                  },
                ]}
              >
                <InputNumber
                  min={0}
                  formatter={val => `VNĐ ${val}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  parser={val => val.replace(/\VNĐ\s?|(,*)/g, '')}
                  required
                  style={{ width: '100%' }}
                />
              </Form.Item>

              <Form.Item
                label="Hoa hồng thực lãnh"
                name="brokerageFee"
                rules={[
                  ({ getFieldValue }) => ({
                    validator(current, value) {
                      if (getFieldValue('brokerageFee') < value) {
                        return Promise.reject(new Error('Không thể lớn hơn hoa hồng thực lãnh'));
                      }

                      return Promise.resolve();
                    },
                  }),
                ]}
              >
                <InputNumber
                  min={0}
                  formatter={val => `VNĐ ${val}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  parser={val => val.replace(/\VNĐ\s?|(,*)/g, '')}
                  required
                  style={{ width: '100%', marginRight: 55 }}
                />
              </Form.Item>
            </Flexbox>
            <Flexbox row spaceBetween>
              <Form.Item label="Ngày giữ chổ" name="reservationDate">
                <DatePicker style={{ width: '100%' }} />
              </Form.Item>
              <Flexbox>
                <Form.Item
                  label="Địa chỉ"
                  name="house_id"
                  rules={[{ required: true, message: 'Bất động sản không thể trống' }]}
                >
                  <Select
                    placeholder="Chọn bất động sản"
                    style={{ width: 200 }}
                    allowClear
                    showSearch
                    filterOption={(input, option) => {
                      return (
                        (option.children &&
                          option.children.toUpperCase().indexOf(input.toUpperCase()) >= 0) ||
                        (option.children &&
                          option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0)
                      );
                    }}
                  >
                    {houses.length > 0 &&
                      houses.map(val => (
                        <Option key={val.id} value={val.id}>
                          {val.house_address}
                        </Option>
                      ))}
                  </Select>
                </Form.Item>
              </Flexbox>
            </Flexbox>

            {/* <Form.List name="users_group"> */}
            {data && (
              //   data.users_group.map((value, index) => (
              <>
                <div className="lineUpdate" />
                <Flexbox row spaceBetween>
                  <Form.Item
                    label="Tên nhân viên"
                    name="staff_id"
                    rules={[{ required: true, message: 'Tên nhân viên không thể trống' }]}
                  >
                    <Select
                      placeholder="Chọn nhân viên"
                      style={{ width: 200 }}
                      allowClear
                      showSearch
                      onChange={value => selectUser(value)}
                      filterOption={(input, option) => {
                        return (
                          (option.children &&
                            option.children.toUpperCase().indexOf(input.toUpperCase()) >= 0) ||
                          (option.children &&
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0)
                        );
                      }}
                    >
                      {users.length > 0 &&
                        users.map(val => (
                          <Option key={val.id} value={val.id}>
                            {val.name}
                          </Option>
                        ))}
                    </Select>
                  </Form.Item>
                  <Flexbox>
                    <Form.Item
                      label="Tên khách hàng"
                      name="customer_id"
                      rules={[{ required: true, message: 'Tên khách hàng không thể trống' }]}
                    >
                      <Select
                        placeholder="Chọn khách hàng"
                        style={{ width: 200, marginRight: 55 }}
                        allowClear
                        showSearch
                        // onChange={value => selectCustomer(value, index)}
                        filterOption={(input, option) => {
                          return (
                            (option.children &&
                              option.children.toUpperCase().indexOf(input.toUpperCase()) >= 0) ||
                            (option.children &&
                              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0)
                          );
                        }}
                      >
                        {customers.length > 0 &&
                          customers.map(val => (
                            <Option key={val.id} value={val.id}>
                              {val.name}
                            </Option>
                          ))}
                      </Select>
                    </Form.Item>
                  </Flexbox>
                </Flexbox>
                <Flexbox row spaceBetween>
                  <Form.Item label="Loại giao dịch" name="transactionType">
                    <Select style={{ width: 200 }} placeholder="Chọn Loại giao dịch" allowClear>
                      {[
                        { 1: 'Khách hàng' },
                        { 2: 'Chủ' },
                        { 3: 'Trung gian' },
                        { 4: 'Chính khách - Chính chủ' },
                      ].map(val => (
                        <Option key={Object.keys(val)[0]} value={Object.keys(val)[0]}>
                          {Object.values(val)[0]}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    label="Tỷ lệ hoa hồng"
                    name="brokerageRate"
                    rules={[{ required: true, message: 'Tỷ lệ hoa hồng không thể trống' }]}
                  >
                    <Input style={{ width: 200, marginRight: 55 }} placeholder="Tỷ lệ hoa hồng" />
                  </Form.Item>
                  <Form.Item
                    label="Hoa hồng thực lãnh"
                    name="brokerageFeeUser"
                    rules={[{ required: true, message: 'Hoa hồng thực lãnh không thể trống' }]}
                  >
                    <InputNumber
                      min={0}
                      formatter={val => `VNĐ ${val}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                      parser={val => val.replace(/\VNĐ\s?|(,*)/g, '')}
                      required
                      style={{ width: '100%' }}
                    />
                  </Form.Item>
                  <Form.Item
                    label="Mã giao dịch"
                    name="transactionCode"
                    rules={[
                      {
                        required: true,
                        message: 'Vui lòng nhập Mã giao dịch',
                      },
                    ]}
                  >
                    <Input style={{ width: 200, marginRight: 55 }} />
                  </Form.Item>
                  <Form.Item label="Trạng thái giao dịch" name="status">
                    <Select
                      style={{ width: 200, marginRight: 55 }}
                      placeholder="Trạng thái giao dịch"
                      allowClear
                    >
                      {[
                        { 1: 'Giữ chổ' },
                        { 2: 'Đặt cọc' },
                        { 3: 'Công chứng hợp đồng mua bán' },
                        { 4: 'Đăng bộ sang tên' },
                      ].map(val => (
                        <Option key={Object.keys(val)[0]} value={Object.keys(val)[0]}>
                          {Object.values(val)[0]}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Flexbox>
              </>
            )}
            {/* </Form.List> */}
          </Form>
        </div>
      </Modal>
      ;
    </>
  );
};
UpdateTransaction.propTypes = {
  columns: PropTypes.any,
  field: PropTypes.any.isRequired,
  type: PropTypes.string.isRequired,
  label: PropTypes.object,
  message: PropTypes.object,
  name: PropTypes.object,
  optionStatus: PropTypes.array,
};

UpdateTransaction.defaultProps = {
  columns: {},
  label: {
    update: 'Update',
    notarizedDay: 'Ngày công chứng',
    brokerageReceiveDay: 'Ngày nhận hoa hồng',
    price: 'Giá chốt',
    brokerageFee: 'Hoa hồng thực lãnh',
    brokerageFeeReceived: 'Hoa hồng đã nhận',
    address: 'Địa chỉ',
    transactionType: 'Loại giao dịch',
    status: 'Trạng thái giao dịch',
    note: 'Ghi chú',
  },
  name: {
    notarizedDay: 'notarizedDay',
    brokerageReceiveDate: 'brokerageReceiveDate',
    price: 'price',
    brokerageFee: 'brokerageFee',
    brokerageFeeReceived: 'brokerageFeeReceived',
    address: 'address',
    transactionType: 'transactionType',
    status: 'status',
    note: 'note',
    accumulation: 'accumulation',
    transaction: 'transaction',
  },
  message: {
    notarizedDate: 'Vui lòng nhập Ngày công chứng',
    brokerageReceiveDate: 'Vui lòng nhập Ngày nhận hoa hồng',
    success: 'Tạo mới thành công',
    error: 'Someting was wrong, please try agian!',
  },
  optionStatus: [
    { 1: 'Giữ chổ' },
    { 2: 'Đặt cọc' },
    { 3: 'Công chứng hợp đồng mua bán' },
    { 4: 'Đăng bộ sang tên' },
  ],
};
export default UpdateTransaction;
