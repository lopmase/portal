/* eslint-disable react/prop-types */
/* eslint-disable no-bitwise */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-shadow */
import { Col, Collapse, Row, Select, Table } from 'antd';
import React, { useEffect, useState, useRef } from 'react';
import { connect, useDispatch } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { Card } from '../../../components';
import { OverviewColumns, overViewMainCard, fiveWay } from '../../../constants/COMMON';
import Condition from '../Condition/Condition';
import HouseChartByTime from './HouseChartByTime';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { fetchDataStatistic } from '../helper';
import './OverViewStyle.scss';

const { Panel } = Collapse;
const { Option } = Select;

const cardStyle = {
  flex: 1,
  padding: 20,
  minWidth: '50%',
};

const Overview = ({ condition, preCondition, type }) => {
  const dispatch = useDispatch();
  const firstUpdate = useRef(true);
  const [loading, setLoading] = useState(true);
  const [statistic, setStatistic] = useState({});
  const [selectFiveWay, setSelectFiveWay] = useState(1);
  const [fiveWays, setFiveWays] = useState({});
  useEffect(() => {
    if (condition && preCondition) {
      if (firstUpdate.current && type !== 'DEFAULT_CONDITION') {
        firstUpdate.current = false;
        return;
      }
      setLoading(true);
      fetchDataStatistic(condition, preCondition).then(resolve => {
        const { fiveWays, statistic } = resolve;
        setFiveWays(fiveWays);
        setStatistic(statistic);
        setLoading(false);
      });
    }
  }, [condition]);

  useEffect(() => {
    try {
      statistic.fiveWaySynthetic !== undefined &&
        selectFiveWay === 1 &&
        setFiveWays(statistic.fiveWaySynthetic);
      selectFiveWay === 2 && setFiveWays(statistic.fiveWaySale);
      selectFiveWay === 3 && setFiveWays(statistic.fiveWayPurchase);
      selectFiveWay === 4 && setFiveWays(statistic.fiveWayConnection);
    } catch (err) {
      dispatch(showMessage(err));
    }
  }, [selectFiveWay]);

  const fiveWayOnchange = value => {
    setSelectFiveWay(value);
  };

  const sale = [
    {
      name: 'Số lượng nhu cầu bán mới',
      unit: 'SL',
      preSeason: statistic.preSaleDemand,
      currentSeason: statistic.fiveWaySale != null && statistic.fiveWaySale.demand,
      standard: 'Chuẩn',
    },
    {
      name: 'Tỷ lệ chuyển đổi',
      unit: '%',
      preSeason:
        statistic.fiveWaySale != null &&
        ~~statistic.fiveWaySale.prePotentialCustomerPercentNewCustomerInteraction,
      currentSeason:
        statistic.fiveWaySale != null &&
        ~~statistic.fiveWaySale.potentialCustomerPercentNewCustomerInteraction,
      standard: 'Chuẩn',
    },
    {
      name: 'Nhà bán có khách xem mới trong quý',
      unit: 'SP',
      preSeason: statistic.preTotalSaleHouseSeenQuantity,
      currentSeason: statistic.totalSaleHouseSeenQuantity,
      standard: 'Chuẩn',
    },
    {
      name: 'Số lượng khách xem',
      unit: 'SL',
      preSeason: statistic.preTotalSaleCustomerSeenQuantity,
      currentSeason: statistic.totalSaleCustomerSeenQuantity,
      standard: 'Chuẩn',
    },
    {
      name: 'Số lượng đàm phán mới',
      unit: 'SL',
      preSeason: statistic.preTotalNewSaleNegotiationQuantity,
      currentSeason: statistic.totalNewSaleNegotiationQuantity,
      standard: 'Chuẩn',
    },
    {
      name: 'Tỷ lệ chuyển đổi',
      unit: '%',
      preSeason:
        statistic.fiveWaySale != null &&
        ~~statistic.fiveWaySale.preTransactionQuantityPercentPotentialCustomer,
      currentSeason:
        statistic.fiveWaySale != null &&
        ~~statistic.fiveWaySale.transactionQuantityPercentPotentialCustomer,
      standard: 'Chuẩn',
    },
    {
      name: 'Số lượng giao dịch thành công',
      unit: 'SL',
      preSeason: statistic.preSaleTransactionQuantity || 0,
      currentSeason: statistic.saleTransactionQuantity || 0,
      standard: 'Chuẩn',
    },
    {
      name: 'Hoa hồng trung bình',
      unit: 'Triệu',
      preSeason: statistic.preAverageBrokerageFeeBySale || 0,
      currentSeason: statistic.averageBrokerageFeeBySale,
      standard: 'Chuẩn',
    },
    {
      name: 'Tổng hoa hồng',
      unit: 'Triệu',
      preSeason: statistic.preTotalBrokerageFeeBySale,
      currentSeason: statistic.totalBrokerageFeeBySale,
      standard: 'Chuẩn',
    },
  ];
  const purchase = [
    {
      name: 'Số lượng nhu cầu mua mới',
      unit: 'Nhu cầu',
      preSeason: statistic.prePurchaseDemand,
      currentSeason: statistic.fiveWayPurchase != null && ~~statistic.fiveWayPurchase.demand,
      standard: 'Chuẩn',
    },
    {
      name: 'Tỷ lệ chuyển đổi',
      unit: '%',
      preSeason:
        statistic.fiveWayPurchase != null &&
        ~~statistic.fiveWayPurchase.prePotentialCustomerPercentNewCustomerInteraction,
      currentSeason:
        statistic.fiveWayPurchase != null &&
        ~~statistic.fiveWayPurchase.potentialCustomerPercentNewCustomerInteraction,
      standard: 'Chuẩn',
    },
    {
      name: 'Số lượng khách đi xem mới',
      unit: 'KH',
      preSeason: statistic.preTotalPurchaseCustomerSeenQuantity,
      currentSeason: statistic.totalPurchaseCustomerSeenQuantity,
      standard: 'Chuẩn',
    },
    {
      name: 'Số lượt dẫn khách đi xem nhà',
      unit: 'Lượt',
      preSeason: statistic.preTotalPurchaseToTakeCustomerSeenHouse,
      currentSeason: statistic.totalPurchaseToTakeCustomerSeenHouse,
      standard: 'Chuẩn',
    },
    {
      name: 'Số lượng sản phẩm khách xem',
      unit: 'SP',
      preSeason: statistic.preTotalPurchaseProductSeenQuantity,
      currentSeason: statistic.totalPurchaseProductSeenQuantity,
      standard: 'Chuẩn',
    },
    {
      name: 'Số lượng khách hàng xem lần 2 mới',
      unit: 'KH',
      preSeason: statistic.preTotalPurchaseCustomerSeen2ndQuantity,
      currentSeason: statistic.totalPurchaseCustomerSeen2ndQuantity,
      standard: 'Chuẩn',
    },
    {
      name: 'Số lượng đàm phán mới',
      unit: 'SL',
      preSeason: statistic.preTotalNewPurchaseNegotiationQuantity,
      currentSeason: statistic.totalNewPurchaseNegotiationQuantity,
      standard: 'Chuẩn',
    },
    {
      name: 'Tỷ lệ chuyển đổi',
      unit: '%',
      preSeason:
        statistic.fiveWayPurchase != null &&
        ~~statistic.fiveWayPurchase.preTransactionQuantityPercentPotentialCustomer,
      currentSeason:
        statistic.fiveWayPurchase != null &&
        ~~statistic.fiveWayPurchase.transactionQuantityPercentPotentialCustomer,
      standard: 'Chuẩn',
    },
    {
      name: 'Số lượng giao dịch thành công',
      unit: 'GD',
      preSeason: statistic.prePurchaseTransactionQuantity || 0,
      currentSeason: statistic.purchaseTransactionQuantity,
      standard: 'Chuẩn',
    },
    {
      name: 'Hoa hồng trung bình',
      unit: 'Triệu',
      preSeason: statistic.preAverageBrokerageFeeByPurchase || 0,
      currentSeason: statistic.averageBrokerageFeeByPurchase,
      standard: 'Chuẩn',
    },
    {
      name: 'Tổng hoa hồng',
      unit: 'Triệu',
      preSeason: statistic.preTotalBrokerageFeeByPurchase,
      currentSeason: statistic.totalBrokerageFeeByPurchase,
      standard: 'Chuẩn',
    },
  ];
  const connection = [
    {
      name: 'Số lượng nhu cầu kết nối mới',
      unit: 'Nhu cầu',
      preSeason: statistic.preConnectDemand,
      currentSeason: statistic.fiveWayConnection != null && ~~statistic.fiveWayConnection.demand,
      standard: 'Chuẩn',
    },
    {
      name: 'Tỷ lệ chuyển đổi',
      unit: '%',
      preSeason:
        statistic.fiveWayConnection != null &&
        ~~statistic.fiveWayConnection.prePotentialCustomerPercentNewCustomerInteraction,
      currentSeason:
        statistic.fiveWayConnection != null &&
        ~~statistic.fiveWayConnection.potentialCustomerPercentNewCustomerInteraction,
      standard: 'Chuẩn',
    },
    {
      name: 'Số lượng khách đi xem mới',
      unit: 'KH',
      preSeason: statistic.preTotalConnectionCustomerSeenQuantity,
      currentSeason: statistic.totalConnectionCustomerSeenQuantity,
      standard: 'Chuẩn',
    },
    {
      name: 'Số lượng sản phẩm phù hợp',
      unit: 'SP',
      preSeason: statistic.preTotalSuitableConnectionProductQuantity,
      currentSeason: statistic.totalSuitableConnectionProductQuantity,
      standard: 'Chuẩn',
    },
    {
      name: 'Số lượng sản phẩm khách xem',
      unit: 'SP',
      preSeason: statistic.preTotalConnectionProductSeenQuantity,
      currentSeason: statistic.totalConnectionProductSeenQuantity,
      standard: 'Chuẩn',
    },
    {
      name: 'Số lượng đàm phán mới',
      unit: 'SP',
      preSeason: statistic.preTotalNewConnectionNegotiationQuantity,
      currentSeason: statistic.totalNewConnectionNegotiationQuantity,
      standard: 'Chuẩn',
    },
    {
      name: 'Tỷ lệ chuyển đổi',
      unit: '%',
      preSeason:
        statistic.fiveWayConnection != null &&
        ~~statistic.fiveWayConnection.preTransactionQuantityPercentPotentialCustomer,
      currentSeason:
        statistic.fiveWayConnection != null &&
        ~~statistic.fiveWayConnection.transactionQuantityPercentPotentialCustomer,
      standard: 'Chuẩn',
    },
    {
      name: 'Số lượng giao dịch thành công',
      unit: 'GD',
      preSeason: statistic.preConnectTransactionQuantity,
      currentSeason: statistic.connectTransactionQuantity,
      standard: 'Chuẩn',
    },
    {
      name: 'Hoa hồng trung bình',
      unit: 'Triệu',
      preSeason: ~~statistic.preAverageBrokerageFeeByConnect,
      currentSeason: statistic.averageBrokerageFeeByConnect,
      standard: 'Chuẩn',
    },
    {
      name: 'Tổng hoa hồng',
      unit: 'Triệu',
      preSeason: ~~statistic.preTotalBrokerageFeeByConnect,
      currentSeason: statistic.totalBrokerageFeeByConnect,
      standard: 'Chuẩn',
    },
  ];
  return (
    <div>
      <Condition />
      <div className="main">
        <div className="left">
          <div className="child">
            <Row gutter={[20, 20]} className="row">
              <Col xs={24}>
                <Card shadow containerStyle={cardStyle} loading={loading}>
                  <div style={{ display: 'flex' }}>
                    <div className="div1">
                      <Row>
                        <Col span={16}>
                          <h3>Hoa Hồng Doanh Thu</h3>
                        </Col>
                        <Col span={8}>
                          <b className="value">Triệu</b>
                        </Col>
                      </Row>
                      <Row wrap>
                        <Col span={16}>
                          <p>Doanh Thu</p>
                        </Col>
                        <Col span={8}>
                          <p className="value">{statistic.brokerageFeeRevenue}</p>
                        </Col>
                      </Row>
                      <Row>
                        <Col span={16}>
                          <p className="revenue">Giao dịch khách hàng:</p>
                        </Col>
                        <Col span={8}>
                          <p className="value">{statistic.totalBrokerageFeeByPurchase}</p>
                        </Col>
                      </Row>
                      <Row>
                        <Col span={16}>
                          <p className="revenue">Giao dịch chủ: </p>
                        </Col>
                        <Col span={8}>
                          <p className="value">{statistic.totalBrokerageFeeBySale}</p>
                        </Col>
                      </Row>
                      <Row>
                        <Col span={16}>
                          <p className="revenue">Giao dịch kết nối: </p>
                        </Col>
                        <Col span={8}>
                          <p className="value">{statistic.totalBrokerageFeeByConnect}</p>
                        </Col>
                      </Row>
                      <Row>
                        <Col span={16}>
                          <p>Phải Thu</p>
                        </Col>
                        <Col span={8}>
                          <p className="value">{statistic.totalBrokerageFeeReceivable}</p>
                        </Col>
                      </Row>
                    </div>
                    <div className="div2">
                      <div className="title">
                        <h3>Hiệu Suất Kinh Doanh</h3>
                      </div>
                      <Row>
                        <Col span={16}>
                          <p>Giao dịch chủ</p>
                        </Col>
                        <Col span={8}>
                          <p className="value">{statistic.saleTransactionQuantity}</p>
                        </Col>
                      </Row>
                      <Row>
                        <Col span={16}>
                          <p>Giao dịch khách hàng</p>
                        </Col>
                        <Col span={8}>
                          <p className="value">{statistic.purchaseTransactionQuantity}</p>
                        </Col>
                      </Row>
                      <Row>
                        <Col span={16}>
                          <p>Giao dịch kết nối</p>
                        </Col>
                        <Col span={8}>
                          <p className="value">{statistic.connectTransactionQuantity}</p>
                        </Col>
                      </Row>
                    </div>
                  </div>
                </Card>
              </Col>
            </Row>
          </div>
          <div className="child ">
            <Row gutter={[20, 20]} className="row">
              <Col xs={24}>
                <Card shadow containerStyle={cardStyle}>
                  <h1 className="FiveWay">FiveWay</h1>
                  <Select
                    className="selectFiveWay"
                    defaultValue={fiveWay[0].label}
                    onChange={fiveWayOnchange}
                  >
                    {fiveWay.map(value => (
                      <Option key={value.key} value={value.key}>
                        {value.label}
                      </Option>
                    ))}
                  </Select>

                  <div>
                    <Row>
                      <Col span={14}>
                        <p>Số lượng tương tác mới</p>
                      </Col>
                      <Col span={7}>
                        <b className="value">{fiveWays.demand}</b>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={14}>
                        <p>Tỷ lệ chuyện đổi</p>
                      </Col>
                      <Col span={7}>
                        <p className="value">
                          {`${fiveWays.potentialCustomerPercentNewCustomerInteraction || 0}%`}
                        </p>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={14}>
                        <p>Số lượng khách hàng tiềm năng </p>
                      </Col>
                      <Col span={7}>
                        <p className="value">{fiveWays.potentialCustomerQuantity}</p>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={14}>
                        <p>Tỷ lệ chuyển đổi </p>
                      </Col>
                      <Col span={7}>
                        <p className="value">
                          {`${fiveWays.transactionQuantityPercentPotentialCustomer || 0} %`}
                        </p>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={14}>
                        <p>
                          <b>Số lượng giao dịch</b>
                        </p>
                      </Col>
                      <Col span={7}>
                        <p className="value">{fiveWays.transactionQuantity}</p>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={14}>
                        <p>Tổng hoa hồng</p>
                      </Col>
                      <Col span={7}>
                        <p className="value">{fiveWays.totalBrokerageFee}</p>
                      </Col>
                    </Row>
                    <Row>
                      <Col span={14}>
                        <p>Hoa hồng trung bình</p>
                      </Col>
                      <Col span={7}>
                        <p className="value">{fiveWays.averageBrokerageFee}</p>
                      </Col>
                    </Row>
                  </div>
                </Card>
              </Col>
            </Row>
          </div>
          <div className="child ">
            <Row gutter={[20, 20]} className="row">
              <Col xs={24}>
                <Card shadow containerStyle={cardStyle}>
                  <h1>Doanh Thu</h1>
                  <h2>ĐVT: Triệu</h2>
                  <h4>
                    Tổng:
                    {statistic.brokerageFeeRevenue}
                  </h4>
                  <HouseChartByTime
                    data={statistic.dataChart || []}
                    keys={['total']}
                    timeKey="date"
                    selectRevenue={3}
                  />
                </Card>
              </Col>
            </Row>
          </div>
        </div>
        <div className="right">
          <Card shadow containerStyle={overViewMainCard} loading={loading}>
            <h3 style={{ minWidth: '70%' }}>Tình Hình Sức Khỏe Kinh Doanh</h3>
            <Row>
              <Col span={12}>
                <p>Lượng sản phẩm hiện có</p>
              </Col>
              <Col span={12}>
                <p>{statistic.houseQuantity}</p>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <p>Lượng sản phẩm đã được tôi marketing là</p>
              </Col>
              <Col span={12}>
                <p>{statistic.totalMarketingQuantity}</p>
              </Col>
            </Row>

            <Collapse
              bordered={false}
              defaultActiveKey={['1']}
              className="site-collapse-custom-collapse"
            >
              <Panel header={<b>Nhu cầu bán</b>} key="1" className="site-collapse-custom-panel">
                <Table
                  columns={OverviewColumns}
                  dataSource={sale}
                  pagination={false}
                  rowKey="uid"
                  scroll={{ x: 400 }}
                />
              </Panel>
              <Panel header={<b>Nhu cầu mua</b>} key="2" className="site-collapse-custom-panel">
                <Table
                  columns={OverviewColumns}
                  dataSource={purchase}
                  pagination={false}
                  rowKey="uid"
                  scroll={{ x: 400 }}
                />
              </Panel>

              <Panel header={<b>Nhu cầu kết nối</b>} key="3" className="site-collapse-custom-panel">
                <Table
                  columns={OverviewColumns}
                  dataSource={connection}
                  pagination={false}
                  rowKey="uid"
                  scroll={{ x: 400 }}
                />
              </Panel>
            </Collapse>
          </Card>
        </div>
      </div>
    </div>
  );
};

export function mapStateToProps(state) {
  return {
    user: state.transaction.user,
    transaction: state.transaction.data,
    type: state.condition.type,
    me: state.auth.user.data,
    condition: state.condition.data.condition,
    preCondition: state.condition.data.preCondition,
    state,
  };
}
export default compose(withRouter, connect(mapStateToProps))(Overview);
