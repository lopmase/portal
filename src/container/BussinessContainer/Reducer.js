import { combineReducers } from 'redux';
import {
  transactionReducer,
  transactionBillReducer,
  transactionOldReducer,
} from './TransactionSuccess/reducer';
import { accumulationReducer } from './Accumulation/reducer';
import { conditionReducer } from './Condition/reducer';
import {
  statisticReducer,
  targetReducer,
  houseQuantityReducer,
} from './StatisticAccumulation/reducer';

export default combineReducers({
  transaction: transactionReducer,
  transactionOld: transactionOldReducer,
  accumulation: accumulationReducer,
  condition: conditionReducer,
  statistic: statisticReducer,
  target: targetReducer,
  countHouse: houseQuantityReducer,
  transactionBill: transactionBillReducer,
});
