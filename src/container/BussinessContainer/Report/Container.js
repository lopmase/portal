/* eslint-disable react/prop-types */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-return-assign */
/* eslint-disable prefer-destructuring */
/* eslint-disable array-callback-return */
/* eslint-disable no-param-reassign */
/* eslint-disable no-shadow */
import { Table, Typography } from 'antd';
import React, { useEffect, useState } from 'react';
import { connect, useDispatch } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { getAllStatisticByCondition } from '../../../apis/businessApi';
import { getUserById } from '../../../apis/userApi';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import Condition from '../Condition/Condition';
import { getDaysWithCondition } from '../helper';

const { Text } = Typography;
const Report = props => {
  const { message, condition } = props;
  const dispatch = useDispatch();
  const [statistic, setSatistic] = useState();
  const [users, setUsers] = useState([]);
  const [coloumn, setColumn] = useState([]);
  const [daysOfMonth, setDayOfMonth] = useState();
  const fetchData = async condition => {
    try {
      const [statisticData, userLst] = await Promise.all([
        getAllStatisticByCondition(condition),
        getUserById(condition),
      ]);
      statisticData.data.map(value => (value.date = value.date.split(' ')[0]));
      setSatistic(statisticData.data);
      setUsers(userLst.data);
    } catch (err) {
      dispatch(showMessage(message));
    }
  };
  useEffect(() => {
    if (
      users &&
      users.length > 0 &&
      daysOfMonth &&
      daysOfMonth.length > 0 &&
      statistic &&
      Array.isArray(statistic)
    ) {
      const startTime = performance.now();
      users.map(user => {
        daysOfMonth.map(day => {
          if (new Date(day).getDay() !== 0) {
            user[day] =
              statistic.filter(val => val.date === day && val.user_id === user.id).length === 0 ? (
                <Text type="danger">Chưa nhập</Text>
              ) : (
                <Text>Đã nhập</Text>
              );
          } else {
            user[day] = <Text type="success">Nghỉ</Text>;
          }
          user.key = user.id;
        });
      });
      const endTime = performance.now();
      console.log(`Excution mapping users Time: ${endTime - startTime} milliseconds`);
    }
  }, [users]);
  useEffect(() => {
    try {
      const startTime = performance.now();
      if (condition) {
        const { dayOfMonth, columns } = getDaysWithCondition(condition.startAt, condition.endAt);
        setDayOfMonth(dayOfMonth);
        setColumn(columns);
        condition && fetchData(condition);
      }
      const endTime = performance.now();
      console.log(`Excution Time: ${endTime - startTime} milliseconds`);
    } catch (err) {
      dispatch(showMessage(err));
    }
  }, [condition]);
  return (
    <>
      <Condition />
      <Table
        bordered
        columns={coloumn}
        dataSource={users}
        size="middle"
        scroll={{ x: 'calc(1800px + 100%)', y: 850 }}
        pagination
      />
    </>
  );
};

export function mapStateToProps(state) {
  return {
    me: state.auth.user.data,
    condition: state.condition.data.condition,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Report);
