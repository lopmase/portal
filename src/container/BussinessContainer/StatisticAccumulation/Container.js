/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable camelcase */
/* eslint-disable no-sequences */
/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable no-shadow */
/* eslint-disable no-param-reassign */
/* eslint-disable no-return-assign */
/* eslint-disable react/prop-types */
import React, { useState, useEffect, useRef } from 'react';
import { connect, useDispatch } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { Table } from 'antd';
import * as Action from '../Action';
import Condition from '../Condition/Condition';
import { statisticColumns } from '../../../constants/COMMON';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { calcLeftDays } from '../../../helpers/common';
import { mappingStatisticAccumulation } from '../helper';

const StatisticAccumulation = ({
  fetchAccumulation,
  fetchTransaction,
  fetchTarget,
  fetchHouseQuantity,
  fetchStatisticHouse,
  condition,
  accumulation,
  transaction,
  target,
  currentHouse,
  houseRemain,
  type,
}) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();
  const firstUpdate = useRef(true);
  const date = new Date();
  const startAt = new Date(date.getFullYear(), date.getMonth(), 1);
  const quarter = Math.floor(startAt.getMonth() / 3) + 1;

  useEffect(() => {
    try {
      if (firstUpdate.current && type !== 'DEFAULT_CONDITION') {
        firstUpdate.current = false;
        return;
      }
      if (!condition.quarter) {
        fetchTarget({ ...condition, quarter });
      } else {
        fetchTarget(condition);
      }
      fetchAccumulation(condition, false);
      fetchTransaction(condition);

      fetchHouseQuantity(condition);
      fetchStatisticHouse(condition);
    } catch (err) {
      dispatch(showMessage('Something went wrong. Please try again!'));
    }
  }, [condition]);
  useEffect(() => {
    if (accumulation && transaction && currentHouse && houseRemain && target) {
      const quarterCondition = condition.quarter || quarter;
      const leftDays = calcLeftDays({ quarter: quarterCondition, currentQuarter: quarter });
      try {
        const accumulationGrouped = mappingStatisticAccumulation(
          accumulation,
          transaction,
          target,
          currentHouse,
          houseRemain,
          leftDays,
        );
        setData(accumulationGrouped);
        setLoading(false);
      } catch (err) {
        dispatch(showMessage('Something went wrong. Please try again!'));
      }
    }
  }, [accumulation, transaction, target, currentHouse, houseRemain]);
  return (
    <>
      <Condition checkRangePicker />
      <Table
        dataSource={data}
        loading={loading}
        bordered
        columns={statisticColumns}
        scroll={{ x: 'calc(700px + 50%)', y: 450 }}
        pagination={false}
      />
    </>
  );
};

export function mapStateToProps(state) {
  return {
    transaction: state.transaction.data,
    condition: state.condition.data.condition,
    type: state.condition.type,
    accumulation: state.accumulation.data,
    target: state.target.data,
    currentHouse: state.countHouse.currentHouse,
    houseRemain: state.countHouse.houseRemain,
    state,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...Action,
    },
    dispatch,
  );
}
export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(StatisticAccumulation);
