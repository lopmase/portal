/* eslint-disable array-callback-return */
/* eslint-disable no-param-reassign */
/* eslint-disable no-fallthrough */

const initialState = [];
const FETCH_TARGET = 'FETCH_TARGET';
const GET_ACCUMULATION = 'GET_ACCUMULATION';
const FETCH_CURRENT_HOUSE_QUANTITY = 'FETCH_CURRENT_HOUSE_QUANTITY';
const FETCH_REMAIN_HOUSE = 'FETCH_REMAIN_HOUSE';
export const statisticReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ACCUMULATION:
      return { type: GET_ACCUMULATION, data: action.payload };
    default:
      return state;
  }
};
export const houseQuantityReducer = (state = { currentHouse: 0, houseRemain: 0 }, action) => {
  switch (action.type) {
    case FETCH_CURRENT_HOUSE_QUANTITY:
      return { ...state, currentHouse: action.payload };
    case FETCH_REMAIN_HOUSE:
      return { ...state, houseRemain: action.payload };
    default:
      return state;
  }
};
export const targetReducer = (state = [], action) => {
  switch (action.type) {
    case FETCH_TARGET:
      return {
        type: FETCH_TARGET,
        data: action.payload,
      };
    default:
      return state;
  }
};
