import { Table, Tabs } from 'antd';
import React, { useEffect, useState, useRef } from 'react';
import { connect, useDispatch } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';
import './TransactionSuccess.scss';
import { getAllTransaction, getAllTransactionV2 } from '../../../apis/businessApi';
import {
  transactionSuccessColumns,
  receiptsColumns,
  transactionBillColumns,
  transactionSuccessColumnsOld,
} from '../../../constants/COMMON';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import Condition from '../Condition/Condition';
import ModalUpdateTransaction from '../Modal/updateTransaction';
import * as action from './action';
import * as Action from '../Action';
import { convertData, convertDataV2 } from '../helper';
import { formatMoney } from '../../../helpers/common';

const { TabPane } = Tabs;

// eslint-disable-next-line react/prop-types
const Transaction = ({
  transaction,
  me,
  condition,
  type,
  listReceipt,
  getReceipt,
  transactionOld,
}) => {
  const dispatch = useDispatch();
  const [data, setData] = useState();
  const [dataOld, setDataOld] = useState([]);
  const [total, setTotal] = useState({});

  const [selectionType, setSelectionType] = useState('radio');
  const [dataTransactionByAccountant, setDataTransactionByAccountant] = useState();
  const [subColumns, setSubColumns] = useState([]);

  const [dataUpdate, setDataUpdate] = useState();
  const [dataUpdateBill, setDataUpdateBill] = useState();

  const [loading, setLoading] = useState(true);
  const NEW_TRANSACTION = 'NEW_TRANSACTION';
  const firstUpdate = useRef(true);

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      dispatch({
        type: 'ADD_TRANSACTIONBILL',
        payload: selectedRows,
      });
    },
    getCheckboxProps: record => ({
      disabled: record.brokerageFeeUser === record.brokerageFeeReceived,
      // Column configuration not to be checked
      name: record.name,
    }),
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const fetchData = async conditionFetching => {
    try {
      const [transactionV1, transactionV2] = await Promise.all([
        getAllTransaction(conditionFetching),
        getAllTransactionV2(conditionFetching),
      ]);
      // transactionV1.data.map(value => {
      //   value.brokerageReceiveDate = value.brokerageReceiveDay;
      //   value.notarizedDate = value.notarizedDay;
      //   value.detail = value.user;
      //   value.detail.transactionType = value.transactionType;
      // });
      // const newData2 = transactionV2.data.map(value => {
      //   return value.users_group.map(item => {
      //     return {
      //       ...value,
      //       key: uuidv4(),
      //       detail: item,
      //     };
      //   });
      // });
      dispatch({
        type: 'GET_TRANSACTION_OLD',
        // payload: values.data,
        payload: transactionV1.data,
      });

      // const newData = [];
      // newData2.map(item => item.map(value => newData.push(value)));
      dispatch({
        type: NEW_TRANSACTION,
        // payload: values.data,
        payload: [
          // ...transactionV1.data,
          ...transactionV2.data.map(item => {
            return {
              ...item,
              key: uuidv4(),
            };
          }),
        ],
      });

      return [...transactionV1.data, ...transactionV2.data];
    } catch (error) {
      dispatch(showMessage(error.message));
      return false;
    }

    // getAllTransactionV2(conditionFetching)
    //   .then(values => {
    //     const dataDetail = values.data.map(value => {
    //       return value.users_group.map(item => {
    //         return {
    //           ...value,
    //           key: uuidv4(),
    //           detail: item,
    //         };
    //       });
    //     });
    //     const newData = [];
    //     dataDetail.map(item => item.map(value => newData.push(value)));
    //     dispatch({
    //       type: NEW_TRANSACTION,
    //       // payload: values.data,
    //       payload: newData,
    //     });
    //   })
    //   .catch(err => {
    //     dispatch(showMessage(err));
    //   });
  };

  useEffect(() => {
    if (firstUpdate.current && type !== 'DEFAULT_CONDITION') {
      firstUpdate.current = false;
      return;
    }
    getReceipt();
    condition && fetchData(condition);
  }, [condition, fetchData, getReceipt, type]);

  // useEffect(() => {
  //   const fetch = async () => {
  //     const res = await getAllTransactionV2();
  //     console.log('res', res.data);
  //     setDataTransactionByAccountant(res.data);
  //     const dataDetail = res.data.map(value => {
  //       return value.users_group.map(item => {
  //         return {
  //           ...value,
  //           key: uuidv4(),
  //           detail: item,
  //         };
  //       });
  //     });
  //     const newData = [];
  //     dataDetail.map(item => item.map(value => newData.push(value)));
  //     console.log('newData',newData)
  //     setDataUser(newData);
  //   };
  //   fetch();
  // }, []);
  useEffect(() => {
    if (transaction && me) {
      try {
        const convered = convertDataV2(transaction, me);

        // eslint-disable-next-line no-shadow
        // const total = convered.reduce(
        //   (initialValue, current) => {
        //     // eslint-disable-next-line no-param-reassign
        //     initialValue.totalBrokerageFee += current.brokerageFee;
        //     // eslint-disable-next-line no-param-reassign
        //     initialValue.totalBrokerageFeeReceived += current.brokerageFeeReceived;
        //     // eslint-disable-next-line no-param-reassign
        //     initialValue.totalBrokerageFeeReceivable += current.brokerageFeeReceivable;
        //     return initialValue;
        //   },
        //   {
        //     totalBrokerageFee: 0,
        //     totalBrokerageFeeReceived: 0,
        //     totalBrokerageFeeReceivable: 0,
        //   },
        // );
        // setTotal(total);
        setData(convered);
        setLoading(false);
      } catch (err) {
        dispatch(showMessage(err));
      }
    }
    // eslint-disable-next-line react/prop-types
    if (transactionOld.length > 0 && me) {
      try {
        const converedOld = convertData(transactionOld, me);
        setLoading(false);
        setDataOld(converedOld);
      } catch (err) {
        dispatch(showMessage(err));
      }
    }
  }, [transaction, me, dispatch, transactionOld]);
  return (
    <>
      <Condition />
      <Tabs defaultActiveKey="1">
        <TabPane tab="Giao dịch" key="1">
          <Table
            loading={loading}
            dataSource={data}
            bordered
            columns={[...receiptsColumns, ...subColumns]}
            size="middle"
            scroll={{ x: 'calc(800px + 50%)', y: 450 }}
            pagination={{ pageSize: 10 }}
            onRow={record => {
              return {
                onDoubleClick: () => {
                  console.log('record', record);
                  setDataUpdate({ ...record });
                },
              };
            }}
          />
          {/* <div className="total_transaction">
            <p>
              Tổng hoa hồng thực lãnh:
              {formatMoney(Number(`${total.totalBrokerageFee}`))}
            </p>
            <p>
              Tổng hoa hồng đã nhận:
              {formatMoney(Number(`${total.totalBrokerageFeeReceived}`))}
            </p>
            <p>
              Tổng hoa hồng phải thu:
              {formatMoney(Number(`${total.totalBrokerageFeeReceivable}`))}
            </p>
          </div> */}
        </TabPane>

        <TabPane tab="Chi tiết giao dịch" key="2">
          <Table
            loading={loading}
            dataSource={data}
            bordered
            columns={transactionSuccessColumns}
            size="middle"
            scroll={{ x: 'calc(800px + 50%)', y: 450 }}
            pagination={{ pageSize: 10 }}
            rowSelection={{
              type: selectionType,
              ...rowSelection,
            }}
          />
        </TabPane>
        <TabPane tab="Phiếu thu" key="3">
          <Table
            loading={loading}
            dataSource={listReceipt}
            bordered
            columns={transactionBillColumns}
            size="middle"
            scroll={{ x: 'calc(800px + 50%)', y: 450 }}
            onRow={record => {
              return {
                onDoubleClick: () => {
                  return dispatch({ type: 'UPDATE_BILL', payload: { ...record } });
                },
              };
            }}
            pagination={{ pageSize: 10 }}
          />
        </TabPane>
        <TabPane tab="Giao dịch cũ" key="4">
          <Table
            loading={loading}
            dataSource={dataOld}
            bordered
            columns={transactionSuccessColumnsOld}
            size="middle"
            scroll={{ x: 'calc(800px + 50%)', y: 450 }}
            pagination={{ pageSize: 10 }}
          />
        </TabPane>
      </Tabs>
      {dataUpdate &&
        (Number(me.job_position) === 8 || Number(me.role) === 1 || Number(me.role) === 2) && (
          <ModalUpdateTransaction condition={condition} field={dataUpdate} type="transaction" />
        )}
    </>
  );
};
Transaction.propTypes = {
  transaction: PropTypes.array,
  me: PropTypes.object,
  condition: PropTypes.object,
  type: PropTypes.string,
  // eslint-disable-next-line react/require-default-props
  listReceipt: PropTypes.array,
};

Transaction.defaultProps = {
  transaction: [],
  me: {},
  condition: {},
  type: '',
};
export function mapStateToProps(state) {
  return {
    user: state.transaction.user,
    transaction: state.transaction.data,
    transactionOld: state.transactionOld.listTransactionOld,
    me: state.auth.user.data,
    condition: state.condition.data.condition,
    type: state.condition.type,
    listReceipt: state.transactionBill.listData,
    state,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...action,
      ...Action,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Transaction);
