import { v4 as uuidv4 } from 'uuid';
import { generateDatalistActions } from '../../../helpers/dataListAction';
import { getUserListByManagerId } from '../../../apis/userApi';
import { getReceipts, getAllTransactionV2, getAllTransaction } from '../../../apis/businessApi';
import { convertDataV2 } from '../helper';

const GET_USER = 'GET_USER';
const GET_LIST_RECEIPT = 'GET_LIST_RECEIPT';

const { fetchDone, fetching } = generateDatalistActions(GET_USER);

export const getUser = async dispatch => {
  dispatch(fetching());
  const user = await getUserListByManagerId();
  dispatch(fetchDone(user, null, null));
};

export const getReceipt = condition => async dispatch => {
  // dispatch(fetching());
  const listReceipt = await getReceipts({ ...condition });
  listReceipt.data.map(value => {
    // eslint-disable-next-line prefer-destructuring, no-param-reassign
    value.detail = value.users_group[0];
    // value.uncollectedFees = value.feePayable - value.feeCollect;
    return value;
  });
  dispatch({
    type: 'TRANSACTIONBILL',
    payload: listReceipt.data,
  });
  // dispatch(fetchDone(listReceipt, null, null));
};

export const getTransactionV2 = condition => async dispatch => {
  // dispatch(fetching());
  // const [transactionV1, transactionV2] = await Promise.all([
  //   getAllTransaction(condition),
  //   getAllTransactionV2(condition),
  // ]);
  const transactionV2 = await getAllTransactionV2(condition);
  dispatch({
    type: 'NEW_TRANSACTION',
    payload: [
      ...transactionV2.data.map(item => {
        return {
          ...item,
          key: uuidv4(),
        };
      }),
    ],
  });
  // dispatch(fetchDone(listReceipt, null, null));
};
