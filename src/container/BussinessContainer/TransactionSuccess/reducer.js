/* eslint-disable array-callback-return */
/* eslint-disable no-param-reassign */
/* eslint-disable no-fallthrough */

const initialState = [];
const initialStateOld = {
  type: '',
  listTransactionOld: [],
};

const initialStateBill = {
  type: '',
  data: [],
  listData: [],
  dataUpdate: null,
};

const GET_USER = 'GET_USER';
const TRANSACTION = 'TRANSACTION';
const NEW_TRANSACTION = 'NEW_TRANSACTION';
const ADD_TRANSACTION = 'ADD_TRANSACTION';
const TRANSACTIONBILL = 'TRANSACTIONBILL';
const NEW_TRANSACTIONBILL = 'NEW_TRANSACTIONBILL';
const ADD_TRANSACTIONBILL = 'ADD_TRANSACTIONBILL';
const UPDATE_BILL = 'UPDATE_BILL';
const RESET_TYPE = 'RESET_TYPE';
const DATA_UPDATE = 'DATA_UPDATE';

export const transactionReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER:
      return {
        type: GET_USER,
        user: 'data',
      };
    case TRANSACTION:
      // const check = state.data.filter(value => value.id == action.payload.id);
      state.data.map((value, index) => {
        if (value.id === action.payload.id) {
          state.data[index] = action.payload;
        }
      });
      return {
        type: TRANSACTION,
        data: [...state.data],
      };
    case NEW_TRANSACTION:
      return {
        type: NEW_TRANSACTION,
        data: action.payload,
      };
    case ADD_TRANSACTION:
      return {
        type: ADD_TRANSACTION,
        data: [...state.data, action.payload],
      };
    default:
      return state;
  }
};

export const transactionBillReducer = (state = initialStateBill, action) => {
  switch (action.type) {
    case TRANSACTIONBILL:
      // const check = state.data.filter(value => value.id == action.payload.id);
      // console.log('state.listData',state.listData)
      state.listData.map((value, index) => {
        if (value.id === action.payload.id) {
          state.listData[index] = action.payload;
        }
      });
      return {
        ...state,
        type: TRANSACTIONBILL,
        listData: action.payload,
      };
    // case NEW_TRANSACTIONBILL:
    //   return {
    //     type: NEW_TRANSACTIONBILL,
    //     data: action.payload,
    //   };
    case ADD_TRANSACTIONBILL:
      return {
        ...state,
        type: ADD_TRANSACTIONBILL,
        data: action.payload,
      };
    case UPDATE_BILL:
      return {
        ...state,
        dataUpdate: action.payload,
        type: UPDATE_BILL,
      };
    case RESET_TYPE:
      return {
        ...state,
        dataUpdate: null,
        type: TRANSACTIONBILL,
      };
    default:
      return state;
  }
};

export const transactionOldReducer = (state = initialStateOld, action) => {
  switch (action.type) {
    case 'GET_TRANSACTION_OLD':
      return {
        type: 'GET_TRANSACTION_OLD',
        listTransactionOld: action.payload,
      };
    default:
      return state;
  }
};
