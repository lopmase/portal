/* eslint-disable no-shadow */
/* eslint-disable no-bitwise */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-sequences */
/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
/* eslint-disable camelcase */
/* eslint-disable no-param-reassign */
/* eslint-disable no-return-assign */
import _ from 'lodash';
import moment from 'moment';
import { getAllStatisticByCondition, getAllTransaction, getHouse } from '../../apis/businessApi';

const million = 1000000;
export const mappingStatisticAccumulation = (
  accumulation,
  transaction,
  target,
  currentHouse,
  houseRemain,
  leftDays,
) => {
  if (accumulation && transaction && currentHouse && houseRemain && target) {
    const startTime = performance.now();
    const groupValues = _.groupBy(accumulation, 'user_id');
    console.log('target', target);
    console.log('transaction', transaction);

    const accumulationGrouped = Object.values(groupValues).map(
      value =>
        (value = Object.values(value).reduce(
          (
            val,
            {
              saleQuantity,
              saleCustomerSeenQuantity,
              purchaseQuantity,
              purchaseCustomerSeenQuantity,
              connectionQuantity,
              connectionCustomerSeenQuantity,
              user,
            },
          ) => {
            return {
              saleQuantity: saleQuantity + val.saleQuantity,
              saleCustomerSeenQuantity: saleCustomerSeenQuantity + val.saleCustomerSeenQuantity,
              purchaseQuantity: purchaseQuantity + val.purchaseQuantity,
              purchaseCustomerSeenQuantity:
                purchaseCustomerSeenQuantity + val.purchaseCustomerSeenQuantity,
              connectionQuantity: connectionQuantity + val.connectionQuantity,
              connectionCustomerSeenQuantity:
                connectionCustomerSeenQuantity + val.connectionCustomerSeenQuantity,
              user: user.name,
              user_id: user.id,
            };
          },
          {
            saleQuantity: 0,
            saleCustomerSeenQuantity: 0,
            purchaseQuantity: 0,
            purchaseCustomerSeenQuantity: 0,
            connectionQuantity: 0,
            connectionCustomerSeenQuantity: 0,
            user: {},
            user_id: 0,
          },
        )),
    );

    const transactionGroupValues = _.groupBy(transaction, 'user_id');
    const transactionGrouped = [];
    Object.values(transactionGroupValues).map(value => {
      transactionGrouped.push(
        value.reduce(
          (val, { brokerageFee, transactionType, user_id }) => {
            if (transactionType === 1) {
              return {
                connectionBrokerageFee: val.connectionBrokerageFee || 0,
                saleBrokerageFee: val.saleBrokerageFee || 0,
                saleTransactionQuantity: val.saleTransactionQuantity || 0,
                connectionTransactionQuantity: val.connectionTransactionQuantity || 0,
                purchaseBrokerageFee: val.purchaseBrokerageFee + brokerageFee,
                purchaseTransactionQuantity: val.purchaseTransactionQuantity + 1,
                user_id,
              };
            }
            if (transactionType === 2) {
              return {
                purchaseBrokerageFee: val.purchaseBrokerageFee || 0,
                connectionBrokerageFee: val.connectionBrokerageFee || 0,
                purchaseTransactionQuantity: val.purchaseTransactionQuantity || 0,
                connectionTransactionQuantity: val.connectionTransactionQuantity || 0,
                saleBrokerageFee: val.saleBrokerageFee + brokerageFee,
                saleTransactionQuantity: val.saleTransactionQuantity + 1,
                user_id,
              };
            }
            if (transactionType === 3) {
              return {
                purchaseBrokerageFee: val.purchaseBrokerageFee || 0,
                saleBrokerageFee: val.saleBrokerageFee || 0,
                purchaseTransactionQuantity: val.purchaseTransactionQuantity || 0,
                saleTransactionQuantity: val.saleTransactionQuantity || 0,
                connectionBrokerageFee: val.connectionBrokerageFee + brokerageFee,
                connectionTransactionQuantity: val.connectionTransactionQuantity + 1,
                user_id,
              };
            }
            if (transactionType === 4) {
              return val;
            }
          },
          {
            purchaseBrokerageFee: 0,
            connectionBrokerageFee: 0,
            purchaseTransactionQuantity: 0,
            saleBrokerageFee: 0,
            saleTransactionQuantity: 0,
            connectionTransactionQuantity: 0,
          },
        ),
      );
    });
    console.log('transactionGrouped', transactionGrouped);

    accumulationGrouped.length > 0 &&
      transactionGrouped.length > 0 &&
      accumulationGrouped.map(
        value => (
          (value.house_remain = houseRemain.find(tr => tr && tr.user_id === value.user_id)
            ? houseRemain.find(tr => tr && tr.user_id === value.user_id).house_remain // cho nay ne
            : 0),
          (value.house_current = currentHouse.find(tr => tr && Number(tr.id) === value.user_id)),
          (value.house_quantity = value.house_current ? value.house_current.house : 0),
          (value.new_house_quantity = value.house_quantity - value.house_remain),
          (value.transaction = transactionGrouped.find(tr => tr && tr.user_id === value.user_id)),
          (value.connection_first_value = target.length > 0 ? target[0].connection : 0),
          (value.potentialSeenOfSale =
            value.saleCustomerSeenQuantity > 0 && value.saleQuantity > 0
              ? value.saleCustomerSeenQuantity / value.saleQuantity
              : 0),
          (value.current_connection_quantity = value.transaction
            ? value.transaction.connectionTransactionQuantity
            : 0),
          (value.new_connection_quantity =
            value.current_connection_quantity !== 0
              ? value.current_connection_quantity + value.connection_first_value
              : 0),
          (value.potentialSeenOfConnection =
            value.connectionCustomerSeenQuantity > 0 &&
            (value.connection_first_value || value.current_connection_quantity)
              ? value.connectionCustomerSeenQuantity /
                (value.connection_first_value / (leftDays / 90) + value.current_connection_quantity)
              : 0),
          (value.potentialBuyingOfSale =
            value.transaction &&
            value.transaction.saleTransactionQuantity > 0 &&
            value.saleCustomerSeenQuantity > 0
              ? value.transaction.saleTransactionQuantity / value.house_quantity
              : 0),
          (value.potentialBuyingOfPurchase =
            value.transaction &&
            value.transaction.purchaseTransactionQuantity &&
            value.purchaseCustomerSeenQuantity > 0 &&
            value.transaction.purchaseTransactionQuantity > 0
              ? value.transaction.purchaseTransactionQuantity / value.purchaseCustomerSeenQuantity
              : 0),
          (value.potentialBuyingOfConnection =
            value.transaction &&
            value.transaction.connectionTransactionQuantity > 0 &&
            value.connectionCustomerSeenQuantity > 0
              ? value.transaction.connectionTransactionQuantity /
                value.connectionCustomerSeenQuantity
              : 0),
          (value.averageSaleBrokerageFee =
            value.transaction &&
            value.transaction.saleTransactionQuantity > 0 &&
            value.transaction.saleBrokerageFee
              ? value.transaction.saleBrokerageFee / value.transaction.saleTransactionQuantity
              : 0),
          (value.averagePurchaseBrokerageFee =
            value.transaction &&
            value.transaction.purchaseTransactionQuantity > 0 &&
            value.transaction.purchaseBrokerageFee
              ? value.transaction.purchaseBrokerageFee /
                value.transaction.purchaseTransactionQuantity
              : 0),
          (value.averageConnectionBrokerageFee =
            value.transaction &&
            value.transaction.connectionTransactionQuantity > 0 &&
            value.transaction.connectionBrokerageFee
              ? value.transaction.connectionBrokerageFee /
                value.transaction.connectionTransactionQuantity
              : 0),
          (value.connectionQuantity = value.target
            ? `${value.connectionQuantity}/${target[0].connectionQuantity}`
            : value.connectionQuantity),
          (value.purchaseQuantity = value.target
            ? `${value.purchaseQuantity}/${target[0].purchaseQuantity}`
            : value.purchaseQuantity),
          (value.saleCustomerSeenQuantity = value.target
            ? `${value.saleCustomerSeenQuantity}/${target[0].saleCustomerSeenQuantity}`
            : value.saleCustomerSeenQuantity),
          (value.saleQuantity = value.target
            ? `${value.saleQuantity}/${
                target.find(tr => tr && tr.user_id === value.user_id).saleQuantity
              }`
            : value.saleQuantity),
          // (value.target = target.find(tr => tr && tr.user_id === value.user_id)),
          (value.purchase_first_value = target.find(tr => tr.user_id === value.user_id)
            ? target.find(tr => tr.user_id === value.user_id).purchase
            : 0),
          (value.current_purchase_quantity =
            value.transaction && value.transaction.purchaseTransactionQuantity
              ? value.transaction.purchaseTransactionQuantity
              : 0),
          (value.new_purchase_quantity =
            value.current_purchase_quantity + value.purchase_first_value),
          (value.potentialSeenOfPurchase =
            // value.purchase_first_value > 0
            //   ? value.purchaseCustomerSeenQuantity /
            //     (value.purchase_first_value / (leftDays / 90) + value.new_purchase_quantity)
            //   : 0)
            value.purchaseCustomerSeenQuantity > 0 && value.new_purchase_quantity > 0
              ? value.purchaseCustomerSeenQuantity /
                (value.purchase_first_value / (leftDays / 90) + value.current_purchase_quantity)
              : 0)
          // cho nay ne
        ),
        console.log('accumulationGrouped', accumulationGrouped),
      );

    const endTime = performance.now();
    console.log(`Excution Time:  ${endTime - startTime} milliseconds`);
    return accumulationGrouped;
  }
};

export const convertData = (data, me) => {
  if (data) {
    const startTime = performance.now();
    data.sort((a, b) => {
      return new Date(b.date) - new Date(a.date);
    });
    data.map(
      value => (
        // eslint-disable-next-line no-sequences
        value.user_id === me.id && (value.name = me.name),
        value.user_id !== me.id && (value.name = value.user.name),
        (value.brokerageFee = value.brokerageFee.toLocaleString('vi-VN', {
          style: 'currency',
          currency: 'VND',
          // eslint-disable-next-line no-sequences
        })),
        (value.price = value.price.toLocaleString('vi-VN', {
          style: 'currency',
          currency: 'VND',
        })),
        (value.brokerageFeeReceived = value.brokerageFeeReceived.toLocaleString('vi-VN', {
          style: 'currency',
          currency: 'VND',
        })),
        Number(value.brokerageFeeStatus) === 1 && (value.brokerageFeeStatus = 'Chưa thu tiền'),
        Number(value.brokerageFeeStatus) === 2 && (value.brokerageFeeStatus = 'Nhận 1 phần'),
        Number(value.brokerageFeeStatus) === 3 && (value.brokerageFeeStatus = 'Thu tiền ngay'),
        (value.brokerageFeeReceivable = value.brokerageFeeReceivable.toLocaleString('vi-VN', {
          style: 'currency',
          currency: 'VND',
        })),
        (value.brokerageReceiveDay = value.brokerageReceiveDay.split(' ')[0]),
        (value.notarizedDay = value.notarizedDay.split(' ')[0]),
        (value.date = value.date.split(' ')[0]),
        Number(value.transactionType) === 1 && (value.transactionType = 'Khách hàng'),
        Number(value.transactionType) === 2 && (value.transactionType = 'Chủ'),
        Number(value.transactionType) === 3 && (value.transactionType = 'Kết nối'),
        Number(value.transactionType) === 4 && (value.transactionType = 'Chính khách - Chính chủ'),
        Number(value.status) === 1 && (value.status = 'Giữ chổ'),
        Number(value.status) === 2 && (value.status = 'Đặt cọc'),
        Number(value.status) === 3 && (value.status = 'Công chứng hợp đồng mua bán'),
        Number(value.status) === 4 && (value.status = 'Đăng bộ sang tên')
      ),
    );
    const endTime = performance.now();
    console.log(`Excution Time: ${endTime - startTime} milliseconds`);
  }
  return data;
};
// todo
export const convertDataV2 = (data, me) => {
  if (data) {
    const startTime = performance.now();
    data.sort((a, b) => {
      return new Date(b.date) - new Date(a.date);
    });
    data.map(
      value =>
        // eslint-disable-next-line no-sequences
        // value.user_id === me.id && (value.name = me.name),
        // value.user_id !== me.id && (value.name = value.user.name),
        // (value.brokerageFee = value.brokerageFee.toLocaleString('vi-VN', {
        //   style: 'currency',
        //   currency: 'VND',
        //   // eslint-disable-next-line no-sequences
        // })),
        // (value.price = value.price.toLocaleString('vi-VN', {
        //   style: 'currency',
        //   currency: 'VND',
        // })),
        // (value.brokerageFeeReceived = value.brokerageFeeReceived.toLocaleString('vi-VN', {
        //   style: 'currency',
        //   currency: 'VND',
        // })),
        // Number(value.brokerageFeeStatus) === 1 && (value.brokerageFeeStatus = 'Chưa thu tiền'),
        // Number(value.brokerageFeeStatus) === 2 && (value.brokerageFeeStatus = 'Nhận 1 phần'),
        // Number(value.brokerageFeeStatus) === 3 && (value.brokerageFeeStatus = 'Thu tiền ngay'),
        // (value.brokerageFeeReceivable = value.brokerageFeeReceivable.toLocaleString('vi-VN', {
        //   style: 'currency',
        //   currency: 'VND',
        // })),
        // (value.brokerageReceiveDate = value.brokerageReceiveDate.split(' ')[0]),
        // (value.notarizedDate = value.notarizedDate.split(' ')[0]),
        // (value.date = value.date.split(' ')[0]),
        // (value.reservationDate = value.reservationDate
        //   ? value.reservationDate.split(' ')[0]
        //   : value.reservationDay.split(' ')[0]),

        (value.address = value.address
          ? value.address
          : `${value.house.house_number} ${value.house.house_address} ${value.house.district}`),
        // (value.brokerageFeeReceivable = value.brokerageFee - value.brokerageFeeReceived)
      // value.detail &&
      //   value.detail.transactionType &&
      //   Number(value.detail.transactionType) === 1 &&
      //   (value.detail.transactionType = 'Khách hàng'),
      // value.detail &&
      //   value.detail.transactionType &&
      //   Number(value.detail.transactionType) === 2 &&
      //   (value.detail.transactionType = 'Chủ'),
      // value.detail &&
      //   value.detail.transactionType &&
      //   Number(value.detail.transactionType) === 3 &&
      //   (value.detail.transactionType = 'Kết nối'),
      // value.detail &&
      //   value.detail.transactionType &&
      //   Number(value.detail.transactionType) === 4 &&
      //   (value.detail.transactionType = 'Chính khách - Chính chủ'),
      // value.detail &&
      //   value.detail.status &&
      //   Number(value.detail.status) === 1 &&
      //   (value.detail.status = 'Giữ chổ'),
      // value.detail &&
      //   value.detail.status &&
      //   Number(value.detail.status) === 2 &&
      //   (value.detail.status = 'Đặt cọc'),
      // value.detail &&
      //   value.detail.status &&
      //   Number(value.detail.status) === 3 &&
      //   (value.detail.status = 'Công chứng hợp đồng mua bán'),
      // value.detail &&
      //   value.detail.status &&
      //   Number(value.detail.status) === 4 &&
      //   (value.detail.status = 'Đăng bộ sang tên')
    );
    const endTime = performance.now();
    console.log(`Excution Time: ${endTime - startTime} milliseconds`);
  }
  return data;
};

export const getDaysWithCondition = (startAt, endAt) => {
  const days = [];
  let currentDate = moment(startAt).format('YYYY-MM-DD');
  while (currentDate <= moment(endAt).format('YYYY-MM-DD')) {
    days.unshift(currentDate);
    currentDate = moment(currentDate)
      .add(1, 'days')
      .format('YYYY-MM-DD');
  }
  const generateDay = days.map(day => ({
    title: day,
    dataIndex: day,
    key: day,
    width: 150,
    align: 'center',
  }));
  generateDay.unshift({
    title: 'Nhân viên',
    dataIndex: 'name',
    key: 'name',
    width: 70,
    align: 'center',
    fixed: 'left',
  });
  return {
    dayOfMonth: days,
    columns: generateDay,
  };
};

export const fetchDataStatistic = async (condition, preCondition) => {
  const startTime = performance.now();
  let [data, house, transaction, preTransaction, preData] = await Promise.all([
    getAllStatisticByCondition(condition),
    getHouse(condition),
    getAllTransaction(condition),
    getAllTransaction(preCondition),
    getAllStatisticByCondition(preCondition),
  ]);
  house = house.data;
  data = data.data;
  preData = preData.data;
  transaction = transaction.data;
  preTransaction = preTransaction.data;
  const saleTransaction = transaction.filter(value => Number(value.transactionType) === 2); // transactionType = 2 =>giao dịch chủ =  bán
  const purchaseTransaction = transaction.filter(value => Number(value.transactionType) === 1); // transactionType = 1 =>giao dịch khách hàng = mua
  const connectTransaction = transaction.filter(value => Number(value.transactionType) === 3); // transactionType == 3 giao dịch kết nối
  const multipleTransaction = transaction.filter(value => Number(value.transactionType) === 4); // transactionType == 4 giao dịch khách chủ
  const brokerageFeeBySale = saleTransaction.reduce((total, value) => {
    return total + value.brokerageFee;
  }, 0);
  const brokerageFeeByPurchase = purchaseTransaction.reduce((total, value) => {
    return total + value.brokerageFee;
  }, 0);
  const brokerageFeeByConnection = connectTransaction.reduce((total, value) => {
    return total + value.brokerageFee;
  }, 0);
  const brokerageFeeByMultiple = multipleTransaction.reduce((total, value) => {
    return total + value.brokerageFee;
  }, 0);
  const preSale = preTransaction.filter(value => Number(value.transactionType) === 2); // giao dịch khách hàng kỳ trước
  const prePurchase = preTransaction.filter(value => Number(value.transactionType) === 1); // giao dịch chủ kỳ trước
  const preConnect = preTransaction.filter(value => Number(value.transactionType) === 3); // giao dịch kết nối kỳ trước
  const preMultiple = preTransaction.filter(value => Number(value.transactionType) === 4); // giao dịch khách - chủ kỳ trước
  const preBrokerageFeeBySale = preSale.reduce((total, value) => {
    return total + value.brokerageFee;
  }, 0);
  const preBrokerageFeeByPurchase = prePurchase.reduce((total, value) => {
    return total + value.brokerageFee;
  }, 0);
  const preBrokerageFeeByConnect = preConnect.reduce((total, value) => {
    return total + value.brokerageFee;
  }, 0);
  const preBrokerageFeeByMultiple = preMultiple.reduce((total, value) => {
    return total + value.brokerageFee;
  }, 0);
  const halfBrokerageFeeByMultiple = brokerageFeeByMultiple / 2;
  const halfPreBrokerageFeeByMultiple = preBrokerageFeeByMultiple / 2;
  // lấy nhu cầu mua, nhu cầu bán, nhu cầu kết nối
  const { saleDemand, purchaseDemand, connectDemand } = data.reduce(
    (
      { saleDemand, purchaseDemand, connectDemand },
      { saleQuantity, purchaseQuantity, connectionQuantity },
    ) => {
      return {
        saleDemand: saleDemand + parseInt(saleQuantity, 10),
        purchaseDemand: purchaseDemand + parseInt(purchaseQuantity, 10),
        connectDemand: connectDemand + parseInt(connectionQuantity, 10),
      };
    },
    {
      saleDemand: 0,
      purchaseDemand: 0,
      connectDemand: 0,
    },
  );
  // lấy nhu cầu mua, nhu cầu bán, nhu cầu kết nối của kỳ trước
  const { preSaleDemand, prePurchaseDemand, preConnectDemand } = preData.reduce(
    (
      { preSaleDemand, prePurchaseDemand, preConnectDemand },
      { saleQuantity, purchaseQuantity, connectionQuantity },
    ) => {
      return {
        preSaleDemand: preSaleDemand + parseInt(saleQuantity, 10),
        prePurchaseDemand: prePurchaseDemand + parseInt(purchaseQuantity, 10),
        preConnectDemand: preConnectDemand + parseInt(connectionQuantity, 10),
      };
    },
    {
      preSaleDemand: 0,
      prePurchaseDemand: 0,
      preConnectDemand: 0,
    },
  );
  // tính tổng chi tiết thành công tích lũy của kỳ này
  const {
    totalMarketingQuantity,
    totalSaleHouseSeenQuantity,
    totalSaleCustomerSeenQuantity,
    totalNewSaleNegotiationQuantity,
    totalPurchaseCustomerSeenQuantity,
    totalPurchaseToTakeCustomerSeenHouse,
    totalPurchaseProductSeenQuantity,
    totalPurchaseCustomerSeen2ndQuantity,
    totalNewPurchaseNegotiationQuantity,
    totalConnectionCustomerSeenQuantity,
    totalSuitableConnectionProductQuantity,
    totalConnectionProductSeenQuantity,
    totalNewConnectionNegotiationQuantity,
  } = data.reduce(
    (
      {
        totalMarketingQuantity,
        totalSaleHouseSeenQuantity,
        totalSaleCustomerSeenQuantity,
        totalNewSaleNegotiationQuantity,
        totalPurchaseCustomerSeenQuantity,
        totalPurchaseToTakeCustomerSeenHouse,
        totalPurchaseProductSeenQuantity,
        totalPurchaseCustomerSeen2ndQuantity,
        totalNewPurchaseNegotiationQuantity,
        totalConnectionCustomerSeenQuantity,
        totalSuitableConnectionProductQuantity,
        totalConnectionProductSeenQuantity,
        totalNewConnectionNegotiationQuantity,
      },
      {
        marketingQuantity,
        saleHouseSeenQuantity,
        saleCustomerSeenQuantity,
        newSaleNegotiationQuantity,
        purchaseCustomerSeenQuantity,
        purchaseToTakeCustomerSeenHouse,
        purchaseProductSeenQuantity,
        purchaseCustomerSeen2ndQuantity,
        newPurchaseNegotiationQuantity,
        connectionCustomerSeenQuantity,
        suitableConnectionProductQuantity,
        connectionProductSeenQuantity,
        newConnectionNegotiationQuantity,
      },
    ) => {
      return {
        totalMarketingQuantity: totalMarketingQuantity + parseInt(marketingQuantity, 10),
        totalSaleHouseSeenQuantity:
          totalSaleHouseSeenQuantity + parseInt(saleHouseSeenQuantity, 10),
        totalSaleCustomerSeenQuantity:
          totalSaleCustomerSeenQuantity + parseInt(saleCustomerSeenQuantity, 10),
        totalNewSaleNegotiationQuantity:
          totalNewSaleNegotiationQuantity + parseInt(newSaleNegotiationQuantity, 10),
        totalPurchaseCustomerSeenQuantity:
          totalPurchaseCustomerSeenQuantity + parseInt(purchaseCustomerSeenQuantity, 10),
        totalPurchaseToTakeCustomerSeenHouse:
          totalPurchaseToTakeCustomerSeenHouse + parseInt(purchaseToTakeCustomerSeenHouse, 10),
        totalPurchaseProductSeenQuantity:
          totalPurchaseProductSeenQuantity + parseInt(purchaseProductSeenQuantity, 10),
        totalPurchaseCustomerSeen2ndQuantity:
          totalPurchaseCustomerSeen2ndQuantity + parseInt(purchaseCustomerSeen2ndQuantity, 10),
        totalNewPurchaseNegotiationQuantity:
          totalNewPurchaseNegotiationQuantity + parseInt(newPurchaseNegotiationQuantity, 10),
        totalConnectionCustomerSeenQuantity:
          totalConnectionCustomerSeenQuantity + parseInt(connectionCustomerSeenQuantity, 10),
        totalSuitableConnectionProductQuantity:
          totalSuitableConnectionProductQuantity + parseInt(suitableConnectionProductQuantity, 10),
        totalConnectionProductSeenQuantity:
          totalConnectionProductSeenQuantity + parseInt(connectionProductSeenQuantity, 10),
        totalNewConnectionNegotiationQuantity:
          totalNewConnectionNegotiationQuantity + parseInt(newConnectionNegotiationQuantity, 10),
      };
    },
    {
      totalMarketingQuantity: 0,
      totalSaleHouseSeenQuantity: 0,
      totalSaleCustomerSeenQuantity: 0,
      totalNewSaleNegotiationQuantity: 0,
      totalPurchaseCustomerSeenQuantity: 0,
      totalPurchaseToTakeCustomerSeenHouse: 0,
      totalPurchaseProductSeenQuantity: 0,
      totalPurchaseCustomerSeen2ndQuantity: 0,
      totalNewPurchaseNegotiationQuantity: 0,
      totalConnectionCustomerSeenQuantity: 0,
      totalSuitableConnectionProductQuantity: 0,
      totalConnectionProductSeenQuantity: 0,
      totalNewConnectionNegotiationQuantity: 0,
    },
  );

  // tính tổng chi tiết thành công tích lũy của kỳ trước
  const {
    preTotalCustomerInteraction,
    preTotalCustomerSeen,
    preTotalMarketingQuantity,
    preTotalSaleHouseSeenQuantity,
    preTotalSaleCustomerSeenQuantity,
    preTotalNewSaleNegotiationQuantity,
    preTotalPurchaseCustomerSeenQuantity,
    preTotalPurchaseToTakeCustomerSeenHouse,
    preTotalPurchaseProductSeenQuantity,
    preTotalPurchaseCustomerSeen2ndQuantity,
    preTotalNewPurchaseNegotiationQuantity,
    preTotalConnectionCustomerSeenQuantity,
    preTotalSuitableConnectionProductQuantity,
    preTotalConnectionProductSeenQuantity,
    preTotalNewConnectionNegotiationQuantity,
  } = preData.reduce(
    (
      {
        preTotalCustomerInteraction,
        preTotalCustomerSeen,
        preTotalMarketingQuantity,
        preTotalSaleHouseSeenQuantity,
        preTotalSaleCustomerSeenQuantity,
        preTotalNewSaleNegotiationQuantity,
        preTotalPurchaseCustomerSeenQuantity,
        preTotalPurchaseToTakeCustomerSeenHouse,
        preTotalPurchaseProductSeenQuantity,
        preTotalPurchaseCustomerSeen2ndQuantity,
        preTotalNewPurchaseNegotiationQuantity,
        preTotalConnectionCustomerSeenQuantity,
        preTotalSuitableConnectionProductQuantity,
        preTotalConnectionProductSeenQuantity,
        preTotalNewConnectionNegotiationQuantity,
      },
      {
        customerInteraction,
        customerSeen,
        marketingQuantity,
        saleHouseSeenQuantity,
        saleCustomerSeenQuantity,
        newSaleNegotiationQuantity,
        purchaseCustomerSeenQuantity,
        purchaseToTakeCustomerSeenHouse,
        purchaseProductSeenQuantity,
        purchaseCustomerSeen2ndQuantity,
        newPurchaseNegotiationQuantity,
        connectionCustomerSeenQuantity,
        suitableConnectionProductQuantity,
        connectionProductSeenQuantity,
        newConnectionNegotiationQuantity,
      },
    ) => {
      return {
        preTotalCustomerInteraction:
          preTotalCustomerInteraction + parseInt(customerInteraction, 10),
        preTotalCustomerSeen: preTotalCustomerSeen + parseInt(customerSeen, 10),
        preTotalMarketingQuantity: preTotalMarketingQuantity + parseInt(marketingQuantity, 10),
        preTotalSaleHouseSeenQuantity:
          preTotalSaleHouseSeenQuantity + parseInt(saleHouseSeenQuantity, 10),
        preTotalSaleCustomerSeenQuantity:
          preTotalSaleCustomerSeenQuantity + parseInt(saleCustomerSeenQuantity, 10),
        preTotalNewSaleNegotiationQuantity:
          preTotalNewSaleNegotiationQuantity + parseInt(newSaleNegotiationQuantity, 10),
        preTotalPurchaseCustomerSeenQuantity:
          preTotalPurchaseCustomerSeenQuantity + parseInt(purchaseCustomerSeenQuantity, 10),
        preTotalPurchaseToTakeCustomerSeenHouse:
          preTotalPurchaseToTakeCustomerSeenHouse + parseInt(purchaseToTakeCustomerSeenHouse, 10),
        preTotalPurchaseProductSeenQuantity:
          preTotalPurchaseProductSeenQuantity + parseInt(purchaseProductSeenQuantity, 10),
        preTotalPurchaseCustomerSeen2ndQuantity:
          preTotalPurchaseCustomerSeen2ndQuantity + parseInt(purchaseCustomerSeen2ndQuantity, 10),
        preTotalNewPurchaseNegotiationQuantity:
          preTotalNewPurchaseNegotiationQuantity + parseInt(newPurchaseNegotiationQuantity, 10),
        preTotalConnectionCustomerSeenQuantity:
          preTotalConnectionCustomerSeenQuantity + parseInt(connectionCustomerSeenQuantity, 10),
        preTotalSuitableConnectionProductQuantity:
          preTotalSuitableConnectionProductQuantity +
          parseInt(suitableConnectionProductQuantity, 10),
        preTotalConnectionProductSeenQuantity:
          preTotalConnectionProductSeenQuantity + parseInt(connectionProductSeenQuantity, 10),
        preTotalNewConnectionNegotiationQuantity:
          preTotalNewConnectionNegotiationQuantity + parseInt(newConnectionNegotiationQuantity, 10),
      };
    },
    {
      preTotalCustomerInteraction: 0,
      preTotalCustomerSeen: 0,
      preTotalMarketingQuantity: 0,
      preTotalSaleHouseSeenQuantity: 0,
      preTotalSaleCustomerSeenQuantity: 0,
      preTotalNewSaleNegotiationQuantity: 0,
      preTotalPurchaseCustomerSeenQuantity: 0,
      preTotalPurchaseToTakeCustomerSeenHouse: 0,
      preTotalPurchaseProductSeenQuantity: 0,
      preTotalPurchaseCustomerSeen2ndQuantity: 0,
      preTotalNewPurchaseNegotiationQuantity: 0,
      preTotalConnectionCustomerSeenQuantity: 0,
      preTotalSuitableConnectionProductQuantity: 0,
      preTotalConnectionProductSeenQuantity: 0,
      preTotalNewConnectionNegotiationQuantity: 0,
    },
  );
  // tính tổng hoa hồng thực lãnh (doanh thu) và hoa hồng phải thu
  const { totalBrokerageFee, totalBrokerageFeeReceivable } = transaction.reduce(
    (
      { totalBrokerageFee, totalBrokerageFeeReceivable },
      { brokerageFee, brokerageFeeReceivable },
    ) => {
      return {
        totalBrokerageFee: totalBrokerageFee + parseInt(brokerageFee, 10),
        totalBrokerageFeeReceivable:
          totalBrokerageFeeReceivable + parseInt(brokerageFeeReceivable, 10),
      };
    },
    {
      totalBrokerageFee: 0,
      totalBrokerageFeeReceivable: 0,
    },
  );

  const days = [];
  transaction.map(value => {
    days.push({
      date: value.date.split(' ')[0],
      total: value.brokerageFee / million,
    });
  });
  // thống kê dữ liệu bên bảng chi tiết thành công tích lũy (đã tính ở những chổ reduce)
  const statisticData = {
    totalSaleHouseSeenQuantity,
    totalSaleCustomerSeenQuantity,
    totalNewSaleNegotiationQuantity,
    totalPurchaseCustomerSeenQuantity,
    totalPurchaseToTakeCustomerSeenHouse,
    totalPurchaseProductSeenQuantity,
    totalPurchaseCustomerSeen2ndQuantity,
    totalNewPurchaseNegotiationQuantity,
    totalConnectionCustomerSeenQuantity,
    totalSuitableConnectionProductQuantity,
    totalConnectionProductSeenQuantity,
    totalNewConnectionNegotiationQuantity,
    houseQuantity: house,
    totalMarketingQuantity,
    preSaleDemand,
    prePurchaseDemand,
    preConnectDemand,
    preTotalCustomerInteraction,
    preTotalCustomerSeen,
    preTotalMarketingQuantity,
    preTotalSaleHouseSeenQuantity,
    preTotalSaleCustomerSeenQuantity,
    preTotalNewSaleNegotiationQuantity,
    preTotalPurchaseCustomerSeenQuantity,
    preTotalPurchaseToTakeCustomerSeenHouse,
    preTotalPurchaseProductSeenQuantity,
    preTotalPurchaseCustomerSeen2ndQuantity,
    preTotalNewPurchaseNegotiationQuantity,
    preTotalConnectionCustomerSeenQuantity,
    preTotalSuitableConnectionProductQuantity,
    preTotalConnectionProductSeenQuantity,
    preTotalNewConnectionNegotiationQuantity,
    totalDemand: saleDemand + purchaseDemand + connectDemand,
    totalBrokerageFeeReceivable: (totalBrokerageFeeReceivable / million).toLocaleString(), // hoa hồng phải thu
    saleTransactionQuantity: saleTransaction.length + multipleTransaction.length, // số lượng giao dịch khách hàng
    preSaleTransactionQuantity: preSale.length + preMultiple.length, // số lượng giao dịch khách hàng kỳ trước
    purchaseTransactionQuantity: purchaseTransaction.length + multipleTransaction.length, // số lượng giao dịch chủ
    prePurchaseTransactionQuantity: prePurchase.length + preMultiple.length, // số lượng giao dịch chủ kỳ trước
    connectTransactionQuantity: connectTransaction.length, // số lượng giao dịch kết nối
    preConnectTransactionQuantity: preConnect.length, // số lượng giao dịch kết nối kỳ trước
    totalBrokerageFeeBySale: (
      (brokerageFeeBySale + halfBrokerageFeeByMultiple) / million || 0
    ).toLocaleString(), // tổng hoa hồng giao dịch khách hàng
    preTotalBrokerageFeeBySale: (
      (preBrokerageFeeBySale + halfPreBrokerageFeeByMultiple) / million || 0
    ).toLocaleString(), // tổng hoa hồng giao dịch khách hàng kỳ trước
    totalBrokerageFeeByPurchase: (
      (brokerageFeeByPurchase + halfBrokerageFeeByMultiple) / million || 0
    ).toLocaleString(), // tổng hoa hồng giao dịch chủ
    preTotalBrokerageFeeByPurchase: (
      (preBrokerageFeeByPurchase + halfPreBrokerageFeeByMultiple) / million || 0
    ).toLocaleString(), // tổng hoa hồng giao dịch chủ kỳ trước
    totalBrokerageFeeByConnect: (brokerageFeeByConnection / million || 0).toLocaleString(), // tổng hoa hồng giao dịch kết nối
    preTotalBrokerageFeeByConnect: (preBrokerageFeeByConnect / million || 0).toLocaleString(), // tổng hoa hồng giao dịch kết nối kỳ trước
    currentHouseQuantity: house,
    averageBrokerageFeeBySale: (
      (brokerageFeeBySale + halfBrokerageFeeByMultiple) /
        million /
        (saleTransaction.length + multipleTransaction.length) || 0
    ).toLocaleString(), // trung bình hoa hồng giao dịch khách hàng
    preAverageBrokerageFeeBySale: (
      (preBrokerageFeeBySale + halfPreBrokerageFeeByMultiple) /
        million /
        (preSale.length + preMultiple.length) || 0
    ).toLocaleString(), // trung bình hoa hồng giao dịch khách hàng kỳ trước
    averageBrokerageFeeByPurchase: (
      (brokerageFeeByPurchase + halfBrokerageFeeByMultiple) /
        (purchaseTransaction.length + multipleTransaction.length) /
        million || 0
    ).toLocaleString(), // trung bình hoa hồng giao dịch chủ
    preAverageBrokerageFeeByPurchase: (
      (preBrokerageFeeByPurchase + halfPreBrokerageFeeByMultiple) /
        (prePurchase.length + preMultiple.length) /
        million || 0
    ).toLocaleString(), // trung bình hoa hồng giao dịch chủ kỳ trước
    averageBrokerageFeeByConnect: (
      brokerageFeeByConnection / connectTransaction.length / million || 0
    ).toLocaleString(), // trung bình hoa hồng giao dịch kết nối
    preAverageBrokerageFeeByConnect: (
      preBrokerageFeeByConnect / preConnect.length / million || 0
    ).toLocaleString(), // trung bình hoa hồng giao dịch kết nối kỳ trước
    brokerageFeeRevenue: (totalBrokerageFee / million || 0).toLocaleString(), // hoa hồng doanh thu
    dataChart: days,
  };
  const fiveWaySale = {};
  const fiveWayPurchase = {};
  const fiveWayConnection = {};
  const fiveWaySynthetic = {};
  fiveWaySale.transactionQuantity = saleTransaction.length;
  fiveWaySale.potentialCustomerQuantity = data.reduce((total, value) => {
    return total + value.saleHouseSeenQuantity;
  }, 0);
  fiveWaySale.demand = saleDemand;
  fiveWaySale.potentialCustomerPercentNewCustomerInteraction = ~~(
    (fiveWaySale.potentialCustomerQuantity / fiveWaySale.demand) *
    100
  ).toFixed(0);
  fiveWaySale.transactionQuantityPercentPotentialCustomer = ~~(
    (fiveWaySale.transactionQuantity / fiveWaySale.potentialCustomerQuantity) *
    100
  ).toFixed(0);
  fiveWaySale.prePotentialCustomerPercentNewCustomerInteraction = ~~(
    (statisticData.preTotalSaleHouseSeenQuantity / statisticData.preSaleDemand) *
    100
  ).toFixed(0);
  fiveWaySale.preTransactionQuantityPercentPotentialCustomer = ~~(
    (statisticData.preSaleTransactionQuantity / statisticData.preTotalSaleCustomerSeenQuantity) *
    100
  ).toFixed(0);
  fiveWaySale.totalBrokerageFee = statisticData.totalBrokerageFeeBySale;
  fiveWaySale.averageBrokerageFee = statisticData.averageBrokerageFeeBySale;
  // purchase
  fiveWayPurchase.transactionQuantity = purchaseTransaction.length;
  fiveWayPurchase.potentialCustomerQuantity = data.reduce((total, value) => {
    return total + value.purchaseCustomerSeenQuantity;
  }, 0);
  fiveWayPurchase.demand = purchaseDemand;
  fiveWayPurchase.potentialCustomerPercentNewCustomerInteraction = ~~(
    (fiveWayPurchase.potentialCustomerQuantity / fiveWayPurchase.demand) *
    100
  ).toFixed(0);
  fiveWayPurchase.transactionQuantityPercentPotentialCustomer = ~~(
    (fiveWayPurchase.transactionQuantity / fiveWayPurchase.potentialCustomerQuantity) *
    100
  ).toFixed(0);
  fiveWayPurchase.prePotentialCustomerPercentNewCustomerInteraction = ~~(
    (statisticData.preTotalPurchaseCustomerSeenQuantity / statisticData.prePurchaseDemand) *
    100
  ).toFixed(0);
  fiveWayPurchase.preTransactionQuantityPercentPotentialCustomer = ~~(
    (statisticData.prePurchaseTransactionQuantity /
      statisticData.preTotalPurchaseCustomerSeenQuantity) *
    100
  ).toFixed(0);
  fiveWayPurchase.totalBrokerageFee = statisticData.totalBrokerageFeeByPurchase;
  fiveWayPurchase.averageBrokerageFee = statisticData.averageBrokerageFeeByPurchase;
  // connect

  fiveWayConnection.transactionQuantity = connectTransaction.length;
  fiveWayConnection.potentialCustomerQuantity = data.reduce((total, value) => {
    return total + value.connectionCustomerSeenQuantity;
  }, 0);
  fiveWayConnection.demand = connectDemand;
  fiveWayConnection.potentialCustomerPercentNewCustomerInteraction = ~~(
    (fiveWayConnection.potentialCustomerQuantity / fiveWayConnection.demand) *
    100
  ).toFixed(0);
  fiveWayConnection.transactionQuantityPercentPotentialCustomer = ~~(
    (fiveWayConnection.transactionQuantity / fiveWayConnection.potentialCustomerQuantity) *
    100
  ).toFixed(0);

  fiveWayConnection.prePotentialCustomerPercentNewCustomerInteraction = ~~(
    (statisticData.preTotalConnectionCustomerSeenQuantity / statisticData.preConnectDemand) *
    100
  ).toFixed(0);
  fiveWayConnection.preTransactionQuantityPercentPotentialCustomer = ~~(
    (statisticData.preConnectTransactionQuantity /
      statisticData.preTotalConnectionCustomerSeenQuantity) *
    100
  ).toFixed(0);
  fiveWayConnection.totalBrokerageFee = statisticData.totalBrokerageFeeByConnect;
  fiveWayConnection.averageBrokerageFee = statisticData.averageBrokerageFeeByConnect;
  // tổng hợp
  fiveWaySynthetic.transactionQuantity = transaction.length;
  fiveWaySynthetic.potentialCustomerQuantity = data.reduce((total, value) => {
    return (
      total +
      value.saleHouseSeenQuantity +
      value.saleCustomerSeenQuantity +
      value.purchaseCustomerSeenQuantity
    );
  }, 0);
  fiveWaySynthetic.demand = saleDemand + purchaseDemand + connectDemand; // số lượng nhu cầu = số lượng tương tác mới
  fiveWaySynthetic.potentialCustomerPercentNewCustomerInteraction = ~~(
    (fiveWaySynthetic.potentialCustomerQuantity / fiveWaySynthetic.demand) *
    100
  ).toFixed(0);

  fiveWaySynthetic.transactionQuantityPercentPotentialCustomer = ~~(
    (fiveWaySynthetic.transactionQuantity / fiveWaySynthetic.potentialCustomerQuantity) *
    100
  ).toFixed(0);
  fiveWaySynthetic.totalBrokerageFee = (totalBrokerageFee / million).toLocaleString();
  fiveWaySynthetic.averageBrokerageFee = ~~(
    totalBrokerageFee /
    million /
    (statisticData.saleTransactionQuantity +
      statisticData.purchaseTransactionQuantity +
      statisticData.connectTransactionQuantity)
  )
    .toFixed(0)
    .toLocaleString();
  statisticData.fiveWaySynthetic = fiveWaySynthetic;
  statisticData.fiveWayConnection = fiveWayConnection;
  statisticData.fiveWaySale = fiveWaySale;
  statisticData.fiveWayPurchase = fiveWayPurchase;
  const endTime = performance.now();

  console.log(`Excution Time: ${endTime - startTime} milliseconds`);
  return {
    fiveWays: fiveWaySynthetic,
    statistic: statisticData,
  };
};
