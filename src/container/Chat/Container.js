import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import { ROUTES } from '../../constants/ROUTES';
import { PrivateRoute } from '../../components';
import NotFound from '../Common/NotFound';
import ChatShell from './shell/ChatShell';

class HouseContainer extends Component {
  state = {};

  render() {
    return (
      <Switch>
        <PrivateRoute exact path={ROUTES.CHAT} component={ChatShell} />
        <Route component={NotFound} />
      </Switch>
    );
  }
}

export default HouseContainer;
