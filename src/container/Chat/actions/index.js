export const conversationChanged = conversation => {
  return {
    type: 'SELECTED_CONVERSATION_CHANGED',
    conversation,
  };
};

export const conversationsRequested = conversations => ({
  type: 'CONVERSATIONS_REQUESTED',
  conversations,
});

export const conversationDeleted = () => ({
  type: 'DELETE_CONVERSATION',
});

export const newMessageAdded = textMessage => ({
  type: 'NEW_MESSAGE_ADDED',
  textMessage,
});

export const messagesRequested = conversation => ({
  type: 'MESSAGES_REQUESTED',
  payload: {
    conversation,
  },
});

export const messagesLoaded = (conversationId, messages, hasMoreMessages, lastMessageId) => ({
  type: 'MESSAGES_LOADED',
  payload: {
    conversationId,
    messages,
    hasMoreMessages,
    lastMessageId,
  },
});
export const raiseHand = (conversationId, staff) => ({
  type: 'RAISE_HAND',
  payload: {
    conversationId,
    staff,
  },
});

export const stopSupport = conversationId => ({
  type: 'STOP_SUPPORT',
  payload: {
    conversationId,
  },
});
export const block = conversationId => ({
  type: 'BLOCK_CONVERSATION',
  payload: {
    conversationId,
  },
});
export const unBlock = conversationId => ({
  type: 'UNBLOCK_CONVERSATION',
  payload: {
    conversationId,
  },
});
export const messageNoti = quantity => ({
  type: 'NEW_MESSAGE_QUANTITY',
  payload: {
    quantity,
  },
});
