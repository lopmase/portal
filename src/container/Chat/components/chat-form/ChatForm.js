/* eslint-disable default-case */
/* eslint-disable react/prop-types */
import { UploadOutlined } from '@ant-design/icons';
import { Button, Upload } from 'antd';
import { doc, setDoc, Timestamp, updateDoc } from 'firebase/firestore';
import { getDownloadURL, getStorage, ref, uploadBytesResumable } from 'firebase/storage';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { db } from '../../../Firebase/firebaseConfig';
import './ChatForm.scss';

const storage = getStorage();
const fiveMinute = 2 * 60 * 1000;
const ChatForm = ({ selectedConversation, me, statusConversations }) => {
  const [textMessage, setTextMessage] = useState('');
  const [condition, setCondition] = useState();
  const [status, setSattus] = useState();
  const [image, setImage] = useState();
  const adjustTextMessage = mess => {
    return mess.trim();
  };
  const isMessageEmpty = mess => {
    return adjustTextMessage(mess).length === 0;
  };
  useEffect(() => {
    selectedConversation &&
      setSattus(statusConversations.filter(st => st.id === selectedConversation.phoneNumber));
  }, [selectedConversation, statusConversations]);
  useEffect(() => {
    const timer = setInterval(() => {
      if (
        (selectedConversation &&
          status &&
          status[0] &&
          status[0].status === 1 &&
          status[0].theLastMessage &&
          status[0].theLastMessage.seconds * 1000 + fiveMinute >= Number(new Date())) ||
        (status &&
          status[0] &&
          status[0].status === 1 &&
          status[0].startTimeSupport.seconds * 1000 + fiveMinute >= Number(new Date()))
      ) {
        setCondition(true);
      } else {
        setCondition(false);
      }
    }, 1000);
    return () => clearInterval(timer);
  }, [selectedConversation, status]);
  let formContents = null;
  let handleFormSubmit = null;

  if (selectedConversation) {
    formContents = (
      <>
        <div style={{ overflowY: 'scroll', overflowX: 'scroll', maxHeight: '100%' }}>
          <Upload
            id="uploader"
            style={{ maxWidth: '30%' }}
            multiple
            onChange={e => {
              setImage(e.fileList);
            }}
            disabled={
              !!(status && status.length > 0 && status[0].supporter !== me.name) || !condition
            }
          >
            <Button icon={<UploadOutlined />}>Click to Upload</Button>
          </Upload>
        </div>

        <input
          type="text"
          placeholder={
            status && status.length > 0 && status[0].supporter === me.name && condition
              ? 'type a message'
              : 'Vui lòng nhấn nút Hỗ trợ để trả lời tin nhắn '
          }
          disabled={
            !!(status && status.length > 0 && status[0].supporter !== me.name) || !condition
          }
          value={textMessage}
          onChange={e => {
            setTextMessage(e.target.value);
          }}
        />
        <Button style={{ marginLeft: '1%' }} htmlType="submit">
          Send
        </Button>
      </>
    );

    handleFormSubmit = async e => {
      e.preventDefault();
      if (
        !isMessageEmpty(textMessage) &&
        (!image || image.length === 0) &&
        selectedConversation.phoneNumber &&
        condition &&
        status &&
        status[0] &&
        status[0].uuid &&
        status[0].supporter === me.name
      ) {
        await setDoc(doc(db, 'messages', `${Date.now() + selectedConversation.phoneNumber}`), {
          text: textMessage,
          uuid: status[0].uuid,
          phoneNumber: selectedConversation.phoneNumber,
          name: me.name,
          owner: 'staff',
          status: 'Chưa xem',
          createdAt: Timestamp.now().toDate(),
        });
        updateDoc(doc(db, 'status', `${selectedConversation.phoneNumber}`), {
          theLastMessage: Timestamp.now().toDate(),
        });
        setTextMessage('');
      }
      if (
        // !isMessageEmpty(textMessage) &&
        image &&
        selectedConversation.phoneNumber &&
        condition &&
        status &&
        status[0].supporter === me.name
      ) {
        const metadata = {
          contentType: 'image/jpeg',
        };

        // Upload file and metadata to the object 'images/mountains.jpg'
        image.map(async value => {
          const storageRef = ref(storage, `images/${value.name}`);
          const uploadTask = uploadBytesResumable(storageRef, value.originFileObj, metadata);
          uploadTask.on(
            'state_changed',
            snapshot => {
              const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              switch (snapshot.state) {
                case 'paused':
                  console.log('Upload is paused');
                  break;
                case 'running':
                  console.log('Upload is running');
                  break;
              }
            },
            error => {
              switch (error.code) {
                case 'storage/unauthorized':
                  break;
                case 'storage/canceled':
                  break;

                // ...

                case 'storage/unknown':
                  break;
              }
            },
            () => {
              // Upload completed successfully, now we can get the download URL
              getDownloadURL(uploadTask.snapshot.ref).then(downloadURL => {
                setDoc(doc(db, 'messages', `${Date.now() + selectedConversation.phoneNumber}`), {
                  imageURL: downloadURL,
                  phoneNumber: selectedConversation.phoneNumber,
                  name: me.name,
                  owner: 'staff',
                  status: 'Chưa xem',
                  createdAt: Timestamp.now().toDate(),
                });
                updateDoc(doc(db, 'status', `${selectedConversation.phoneNumber}`), {
                  theLastMessage: Timestamp.now().toDate(),
                });
              });
            },
          );
        });
        setTextMessage('');
      }
    };
  }

  return (
    <form name="form" id="chat-form" onSubmit={handleFormSubmit}>
      {formContents}
    </form>
  );
};

const mapStateToProps = state => {
  return {
    getMessagesForConversation: state.chatRealtime.conversationState.selectedConversation,
    selectedConversation: state.chatRealtime.conversationState.selectedConversation,
    me: state.auth.user.data,
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatForm);
