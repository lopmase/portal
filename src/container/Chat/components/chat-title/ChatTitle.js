/* eslint-disable consistent-return */
/* eslint-disable react/prop-types */
import { Button } from 'antd';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { Spacer } from '../../../../components';
import { raiseHand, stopSupport, block, unBlock } from '../../actions';
import './ChatTitle.scss';

const fiveMinute = 2 * 60 * 1000;
const ChatTitle = ({
  selectedConversation,
  me,
  raiseHandSupport,
  raiseHandStopSupport,
  statusConversations,
  blockConversation,
  unblockConversation,
}) => {
  const [condition, setCondition] = useState();
  const [status, setSattus] = useState();
  const Support = () => {
    raiseHandSupport(selectedConversation.id, me.name);
  };
  const StopSupport = () => {
    raiseHandStopSupport(selectedConversation.id);
  };
  const Block = () => {
    blockConversation(selectedConversation.id);
  };
  const UnBlock = () => {
    unblockConversation(selectedConversation.id);
  };
  useEffect(() => {
    selectedConversation &&
      setSattus(statusConversations.filter(st => st.id === selectedConversation.phoneNumber));
  }, [selectedConversation, statusConversations]);
  useEffect(() => {
    try {
      const timer = setInterval(() => {
        if (
          (selectedConversation &&
            status &&
            status[0] &&
            status[0].status === 1 &&
            status[0].theLastMessage &&
            status[0].theLastMessage.seconds * 1000 + fiveMinute >= Number(new Date())) ||
          (status &&
            status[0] &&
            status[0].status === 1 &&
            status[0].startTimeSupport &&
            status[0].startTimeSupport.seconds * 1000 + fiveMinute >= Number(new Date()))
        ) {
          setCondition(true);
        } else {
          setCondition(false);
        }
      }, 1000);
      return () => clearInterval(timer);
    } catch {
      console.log('err');
    }
  }, [selectedConversation, status]);

  return (
    <div id="chat-title">
      {selectedConversation && (
        <span>
          Khách hàng:
          {` ${selectedConversation.sex === 'men' ? 'Anh' : 'Chị'} ${selectedConversation.name}`}
        </span>
      )}
      <Spacer />
      <Spacer />

      {selectedConversation && condition && status[0] && status[0].supporter != null && (
        <span>
          Nhân viên đang hỗ trợ:
          {` ${status[0].supporter || ''}`}
        </span>
      )}

      <div className="lst-button">
        {selectedConversation &&
          ((status && status.length > 0 && status[0].supporter !== me.name) || !condition) && (
            <Button onClick={() => Support()} className="raise-hand">
              Hỗ trợ
            </Button>
          )}
        {selectedConversation &&
          status &&
          status.length > 0 &&
          condition &&
          status[0].supporter === me.name && (
            <Button onClick={() => StopSupport()} className="raise-hand">
              Dừng hỗ trợ
            </Button>
          )}
        {selectedConversation &&
          status &&
          status.length > 0 &&
          status[0] &&
          status[0].status === 1 && (
            <Button onClick={() => Block()} className="raise-hand">
              Khóa hội thoại
            </Button>
          )}
        {selectedConversation && status && status.length > 0 && status[0].status === 0 && (
          <Button onClick={() => UnBlock()} className="raise-hand">
            Mở khóa hội thoại
          </Button>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    conversations: state.conversationState,
    state: state.chatRealtime,
    me: state.auth.user.data,
  };
};
export function mapDispatchToProps(dispatch) {
  return {
    raiseHandSupport: (conversationId, staff) => dispatch(raiseHand(conversationId, staff)),
    raiseHandStopSupport: conversationId => dispatch(stopSupport(conversationId)),
    blockConversation: conversationId => dispatch(block(conversationId)),
    unblockConversation: conversationId => dispatch(unBlock(conversationId)),
  };
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(ChatTitle);
