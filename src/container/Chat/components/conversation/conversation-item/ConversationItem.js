/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import moment from 'moment';
import { conversationChanged } from '../../../actions';
import './ConversationItem.scss';

const ConversationItem = ({ selectedConversation, conversation }) => {
  const [className, setClassName] = useState('conversation');
  const dispatch = useDispatch();
  useEffect(() => {
    if (selectedConversation && conversation.phoneNumber === selectedConversation.phoneNumber) {
      setClassName('conversation active');
    } else {
      setClassName('conversation');
    }
  }, [selectedConversation, conversation]);
  return (
    <div
      className={className}
      onClick={() => {
        dispatch(conversationChanged(conversation));
      }}
    >
      {/* <img src={userAvatar} alt="avatar" /> */}
      <div className="title-text">{`${conversation.name}`}</div>
      <div>{conversation.phoneNumber}</div>
      <div className="created-date">
        {`${conversation.theLastTime &&
          moment(conversation.theLastTime * 1000).format('DD MM YYYY HH:mm:ss')}`}
      </div>
      <div className="conversation-message">{conversation.latestMessageText}</div>
    </div>
  );
};

export default ConversationItem;
