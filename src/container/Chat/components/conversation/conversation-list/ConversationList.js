/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable array-callback-return */
/* eslint-disable no-shadow */
/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import ConversationItem from '../conversation-item/ConversationItem';
import './ConversationList.scss';

const ConversationList = ({
  selectedConversation,
  searchValue,
  statusConversations,
  conversations,
}) => {
  const [subConversations, setSubConversations] = useState([]);
  const [element, setElement] = useState();
  let conversationItems;
  useEffect(() => {
    const result =
      searchValue &&
      conversations.filter(
        element =>
          element.name.toUpperCase().match(searchValue.toUpperCase()) ||
          element.phoneNumber.match(searchValue),
      );
    result ? setSubConversations(result) : setSubConversations([]);
  }, [searchValue, conversations]);
  useEffect(() => {
    if (subConversations.length === 0 || !searchValue) {
      conversations.sort((a, b) => {
        return b.theLastTime - a.theLastTime;
      });
      conversationItems =
        conversations.length > 0 &&
        conversations.map(conversation => {
          return (
            <ConversationItem
              selectedConversation={selectedConversation}
              key={conversation.phoneNumber}
              statusConversations={statusConversations}
              conversation={conversation}
            />
          );
        });
    } else {
      conversationItems =
        subConversations.length > 0 &&
        subConversations.map(conversation => {
          return (
            <ConversationItem
              selectedConversation={selectedConversation}
              key={conversation.phoneNumber}
              conversation={conversation}
            />
          );
        });
    }
    setElement(conversationItems);
  }, [conversations, subConversations, selectedConversation]);

  return <div id="conversation-list">{element}</div>;
};

export default ConversationList;
