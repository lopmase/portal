/* eslint-disable react/button-has-type */
import React from 'react';
import './NewConversation.scss';

const NewConversation = () => {
  return (
    <div id="new-message-container">
      <div style={{ textAlign: 'right', width: '100%' }} />
    </div>
  );
};

export default NewConversation;
