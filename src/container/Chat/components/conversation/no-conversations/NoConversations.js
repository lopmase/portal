import React from 'react';

import './NoConversations.scss';

const NoConversations = () => {
  return (
    <div id="no-coversation-layout">
      <div id="no-conversation-content">
        <h2>Please choose a Conversation</h2>
      </div>
    </div>
  );
};

export default NoConversations;
