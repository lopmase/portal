/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable react/prop-types */
import React from 'react';
import moment from 'moment';
import classNames from 'classnames';
import './Message.scss';

const Message = ({ isMyMessage, message }) => {
  const messageClass = classNames('message-row', {
    'you-message': isMyMessage === 'staff',
    'other-message': isMyMessage === 'client',
  });
  return (
    <div className={messageClass}>
      <div className="message-content">
        {message.text && <div className="message-text">{message.text}</div>}
        {message.imageURL && (
          <div className="message-img">
            <img style={{ maxWidth: '170px', maxHeight: '150px' }} src={message.imageURL} />
          </div>
        )}
        <div className="message-time">
          {`${moment(message.createdAt.seconds * 1000).format('DD MM YYYY hh:mm:ss')}`}
        </div>
        {isMyMessage === 'staff' && <div>{message.status}</div>}
      </div>
    </div>
  );
};

export default Message;
