/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import Message from '../components/message/Message';
import './MessageList.scss';

const MessageList = ({ selectedConversation, message }) => {
  const [messageLst, setMessageLst] = useState([]);
  useEffect(() => {
    if (selectedConversation && message) {
      const messageOfConversation = message.filter(
        mess => mess.phoneNumber === selectedConversation.phoneNumber,
      );
      messageOfConversation.sort((a, b) => {
        return b.createdAt.seconds - a.createdAt.seconds;
      });
      setMessageLst(messageOfConversation);
    }
    if (!selectedConversation) {
      setMessageLst(null);
    }
  }, [selectedConversation, message]);

  return (
    <div id="chat-message-list">
      {messageLst.map(mess => (
        <Message key={mess.createdAt.seconds} isMyMessage={mess.owner} message={mess} />
      ))}
    </div>
  );
};

export default MessageList;
