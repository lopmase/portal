/* eslint-disable no-plusplus */
/* eslint-disable prefer-destructuring */
import { doc, Timestamp, updateDoc } from 'firebase/firestore';
import { db } from '../../Firebase/firebaseConfig';

const initialState = {
  conversations: [],
  selectedConversation: null,
  newMessage: 0,
};

// initialState.selectedConversation = initialState.conversations[1];

const conversationsReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'CONVERSATIONS_LOADED': {
      const newState = { ...state };
      newState.conversations = action.payload.conversations ? action.payload.conversations : [];
      newState.selectedConversation = action.payload.selectedConversation;

      return newState;
    }
    case 'SELECTED_CONVERSATION_CHANGED': {
      const newState = { ...state };
      newState.selectedConversation = action.conversation;
      return newState;
    }
    case 'DELETE_CONVERSATION': {
      if (state.selectedConversation) {
        const newState = { ...state };

        let selectedConversationIndex = newState.conversations.findIndex(
          c => c.id === newState.selectedConversation.id,
        );
        newState.conversations.splice(selectedConversationIndex, 1);

        if (newState.conversations.length > 0) {
          if (selectedConversationIndex > 0) {
            --selectedConversationIndex;
          }

          newState.selectedConversation = newState.conversations[selectedConversationIndex];
        } else {
          newState.selectedConversation = null;
        }

        return newState;
      }

      return state;
    }
    case 'NEW_MESSAGE_ADDED': {
      if (state.selectedConversation) {
        const newState = { ...state };
        newState.selectedConversation = { ...newState.selectedConversation };

        newState.selectedConversation.messages.unshift({
          imageUrl: null,
          imageAlt: null,
          messageText: action.textMessage,
          createdAt: 'Apr 16',
          isMyMessage: true,
        });

        return newState;
      }

      return state;
    }
    case 'NEW_MESSAGE_QUANTITY': {
      return {
        newMessage: action.payload.quantity,
      };
    }

    case 'CONVERSATIONS_REQUESTED': {
      return {
        conversations: action.conversations,
      };
    }
    case 'RAISE_HAND': {
      updateDoc(doc(db, 'status', `${action.payload.conversationId}`), {
        supporter: `${action.payload.staff}`,
        startTimeSupport: Timestamp.now().toDate(),
      });
      return state;
    }
    case 'STOP_SUPPORT': {
      updateDoc(doc(db, 'status', `${action.payload.conversationId}`), {
        supporter: null,
      });
      return state;
    }
    case 'BLOCK_CONVERSATION': {
      updateDoc(doc(db, 'status', `${action.payload.conversationId}`), {
        status: 0,
      });
      return state;
    }
    case 'UNBLOCK_CONVERSATION': {
      updateDoc(doc(db, 'status', `${action.payload.conversationId}`), {
        status: 1,
      });
      return state;
    }
    default:
      return state;
  }
};

export default conversationsReducer;
