/* eslint-disable array-callback-return */
/* eslint-disable no-param-reassign */
/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
import { Input } from 'antd';
import { updateDoc, doc } from 'firebase/firestore';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import {
  conversationChanged,
  conversationDeleted,
  conversationsRequested,
  newMessageAdded,
  messageNoti,
} from '../actions/index';
import ChatForm from '../components/chat-form/ChatForm';
import ChatTitle from '../components/chat-title/ChatTitle';
import ConversationList from '../components/conversation/conversation-list/ConversationList';
import NewConversation from '../components/conversation/new-conversation/NewConversation';
import NoConversations from '../components/conversation/no-conversations/NoConversations';
import MessageList from '../message/MessageList';
import './ChatShell.scss';
import { db } from '../../Firebase/firebaseConfig';

const { Search } = Input;
const ChatShell = ({
  selectedConversation,
  conversationChanged,
  onMessageSubmitted,
  onDeleteConversation,
  phoneNumber,
  status,
  messages,
  me,
}) => {
  const [conversations, setConversations] = useState([]);
  const [statusConversations, setStatusConversation] = useState([]);
  const [searchValue, setSearchValue] = useState();
  useEffect(() => {
    phoneNumber && setConversations(phoneNumber);
  }, [phoneNumber]);
  useEffect(() => {
    status && setStatusConversation(status);
  }, [status]);

  useEffect(() => {
    if (selectedConversation && selectedConversation.id !== null) {
      const converStatus = status.find(st => st.phoneNumber === selectedConversation.phoneNumber);
      converStatus &&
        converStatus.read === 0 &&
        updateDoc(doc(db, 'status', `${selectedConversation.phoneNumber}`), {
          read: 1,
        });
    }
  }, [selectedConversation, status]);
  useEffect(() => {
    conversations.map(convers => {
      const queryMessage = messages.filter(mess => mess.phoneNumber === convers.phoneNumber);
      convers.latestMessageText =
        queryMessage.length !== 0 ? queryMessage[queryMessage.length - 1].text : '';
      convers.theLastTime =
        queryMessage.length !== 0 ? queryMessage[queryMessage.length - 1].createdAt.seconds : 1;
      convers.message = queryMessage;
    });
  }, [conversations, messages]);

  const onSearch = value => {
    setSearchValue(value);
  };

  return (
    <div id="chat-container">
      <div className="search-container">
        <Search className="searchBox" placeholder="input search text" onSearch={onSearch} />
      </div>
      <ConversationList
        onConversationItemSelected={conversationChanged}
        conversations={conversations}
        selectedConversation={selectedConversation}
        searchValue={searchValue}
        statusConversations={statusConversations}
      />
      <NewConversation />

      <ChatTitle
        me={me}
        selectedConversation={selectedConversation}
        onDeleteConversation={onDeleteConversation}
        statusConversations={statusConversations}
      />

      {selectedConversation && (
        <MessageList message={messages} selectedConversation={selectedConversation} />
      )}
      {!selectedConversation && <NoConversations />}
      <ChatForm
        statusConversations={statusConversations}
        selectedConversation={selectedConversation}
        onMessageSubmitted={onMessageSubmitted}
      />
    </div>
  );
};

const mapStateToProps = state => {
  return {
    conversations: state.conversationState,
    selectedConversation: state.chatRealtime.conversationState.selectedConversation,
    phoneNumber: state.chat.data.phoneNumber,
    status: state.chat.data.status,
    messages: state.chat.data.message,
    state: state.chatRealtime,
    me: state.auth.user.data,
  };
};

const mapDispatchToProps = dispatch => ({
  conversationChanged: conversation => dispatch(conversationChanged(conversation)),
  onMessageSubmitted: messageText => {
    dispatch(newMessageAdded(messageText));
  },
  onDeleteConversation: () => {
    dispatch(conversationDeleted());
  },
  loadConversations: mess => {
    dispatch(conversationsRequested(mess));
  },
  sendMessageNoti: quantity => {
    dispatch(messageNoti(quantity));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ChatShell);
