import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShare } from '@fortawesome/free-solid-svg-icons';
import { Button, notification } from 'antd';
import { WEB_URL } from '../../../constants/APP_CONST';

const CopyToClipboard = ({ slug }) => {
  const openSuccessNoti = () => {
    notification.open({
      message: 'Kết quả sao chép URL',
      description: 'Đã sao chép URL thương mại của BĐS thành công',
    });
  };
  const onActionClick = () => {
    const el = document.createElement('textarea'); // Create a <textarea> element
    const landsideURL = `${WEB_URL}/bat-dong-san/`;
    el.value = landsideURL.concat(slug); // Set its value to the string that you want copied
    el.setAttribute('readonly', ''); // Make it readonly to be tamper-proof
    el.style.position = 'absolute';
    el.style.left = '-9999px'; // Move outside the screen to make it invisible
    document.body.appendChild(el); // Append the <textarea> element to the HTML document
    const selected =
      document.getSelection().rangeCount > 0 // Check if there is any content selected previously
        ? document.getSelection().getRangeAt(0) // Store selection if found
        : false; // Mark as false to know no selection existed before
    el.select(); // Select the <textarea> content
    document.execCommand('copy'); // Copy - only works as a result of a user action (e.g. click events)
    document.body.removeChild(el); // Remove the <textarea> element
    if (selected) {
      // If a selection existed before copying
      document.getSelection().removeAllRanges(); // Unselect everything on the HTML document
      document.getSelection().addRange(selected); // Restore the original selection
      openSuccessNoti();
    }
  };

  return (
    <div style={{ visibility: 'hidden', height: 0 }}>
      <div data-href={`${WEB_URL}/`} data-layout="button_count" data-size="large">
        <Button onClick={onActionClick} id={`house-copy-url-${slug}`}>
          <FontAwesomeIcon style={{ marginRight: 10 }} icon={faShare} />
          Copy URL
        </Button>
      </div>
    </div>
  );
};

CopyToClipboard.propTypes = {
  slug: PropTypes.string.isRequired,
};

export default CopyToClipboard;
