/* eslint-disable react/no-unused-prop-types */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import _ from 'lodash';
import { connect } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import { bindActionCreators } from 'redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import './DataList.scss';
import { Input } from '../../../components';

const DataList = ({
  dataSource,
  renderItems,
  height,
  search,
  onSearch,
  emptyMessage,
  size,
  customFilter,
  loadMore,
  onLoadMore,
  hasMore,
}) => {
  const { fetching, list, message } = dataSource;
  const [searchValue, updateSearch] = useState('');
  console.log('dataSource',dataSource)
  const onSearchChanged = value => {
    updateSearch(value);
    if (onSearch) {
      onSearch(value);
    }
  };

  const filter = item => {
    if (!onSearch && searchValue) {
      return (
        _.values(item).filter(value => value && value.indexOf && value.indexOf(searchValue) >= 0)
          .length > 0
      );
    }
    return true;
  };

  const renderList = () =>
    list
      .filter((item, index) => (!size || index < size) && customFilter(item) && filter(item))
      .map(item => renderItems(item));

  return (
    <div
      className={classNames('data-list', {
        loading: fetching,
      })}
    >
      {search && (
        <div className="search">
          <Input
            placeholder="Tìm"
            value={searchValue}
            onChange={(_name, value) => onSearchChanged(value)}
          />
        </div>
      )}
      {fetching && (
        <div className="loading">
          <FontAwesomeIcon icon={faSpinner} size="3x" spin color="white" />
        </div>
      )}
      {!fetching && message && (
        <div className="error">
          <div>{`Error: ${message}`}</div>
        </div>
      )}
      {!fetching && !message && list.length === 0 && (
        <div className="empty">
          <div>{emptyMessage}</div>
        </div>
      )}
      <div className="list" style={{ height: height || 'auto' }}>
        {loadMore ? (
          <InfiniteScroll
            initialLoad={false}
            pageStart={0}
            loadMore={onLoadMore}
            hasMore={hasMore}
            useWindow={false}
          >
            {renderList()}
          </InfiniteScroll>
        ) : (
          renderList()
        )}
      </div>
    </div>
  );
};

DataList.propTypes = {
  // reducerId: PropTypes.string.isRequired,
  dataSource: PropTypes.object.isRequired,
  renderItems: PropTypes.func.isRequired,
  selector: PropTypes.func.isRequired,
  onSearch: PropTypes.func,
  customFilter: PropTypes.func,
  size: PropTypes.number,
  height: PropTypes.any,
  emptyMessage: PropTypes.string,
  search: PropTypes.bool,
  loadMore: PropTypes.bool,
  hasMore: PropTypes.bool,
  onLoadMore: PropTypes.func,
};

DataList.defaultProps = {
  height: null,
  onSearch: null,
  search: false,
  size: null,
  customFilter: () => true,
  emptyMessage: 'No Data',
  loadMore: false,
  hasMore: false,
  onLoadMore: () => {},
};

export function mapStateToProps(state, ownProps) {
  return {
    dataSource: ownProps.selector(state),
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(DataList);
