import React from 'react';
import PropTypes from 'prop-types';
import { faFilter, faBorderAll, faDownload } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Menu, Dropdown } from 'antd';
import { Button } from '../../../components';

const Excel = ({ hideFilterButton, onExportFiltered, onExportAll }) => {
  const onActionClick = item => {
    if (item.key === '1') {
      onExportFiltered();
    } else {
      onExportAll();
    }
  };
  const menu = (
    <Menu onClick={onActionClick}>
      {!hideFilterButton && (
        <Menu.Item key="1">
          <FontAwesomeIcon icon={faFilter} style={{ marginRight: 10 }} />
          Theo bộ lọc
        </Menu.Item>
      )}
      <Menu.Item key="2">
        <FontAwesomeIcon style={{ marginRight: 10 }} icon={faBorderAll} />
        Tất cả
      </Menu.Item>
    </Menu>
  );

  return (
    <Dropdown trigger="click" overlay={menu}>
      <Button
        containerStyle={{ marginLeft: 10, paddingRight: 14, paddingLeft: 14 }}
        icon={faDownload}
        iconColor="gray"
        iconOnly
        backgroundColor="white"
      />
    </Dropdown>
  );
};

Excel.propTypes = {
  onExportFiltered: PropTypes.func,
  onExportAll: PropTypes.func.isRequired,
  hideFilterButton: PropTypes.bool,
};

Excel.defaultProps = {
  hideFilterButton: false,
  onExportFiltered: () => {},
};

export default Excel;
