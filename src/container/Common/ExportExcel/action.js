import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { showMessage } from '../GlobalErrorModal/action';

export const exportExcel = (type, filter) => async (dispatch, getState, thunkDependencies) => {
  try {
    const data = await thunkDependencies.user.exportExcel(type, filter);
    if (!data) {
      return false;
    }
    const blob = new Blob([data], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });
    const objectUrl = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = objectUrl;
    a.download = 'exports.xlsx';
    document.body.appendChild(a);
    a.click();

    return data;
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
    return false;
  }
};
