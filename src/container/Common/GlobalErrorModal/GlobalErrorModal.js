/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

import { Modal, Flexbox, Button } from '../../../components';

import { REDUCERS } from '../../../constants/COMMON';
import { generateModalActions } from '../../../helpers/modalAction';

const { closeModal } = generateModalActions(REDUCERS.GLOBAL_ERROR_MODAL);

class GlobalErrorModal extends Component {
  componentDidMount() {}

  render() {
    const { modal, message, close } = this.props;
    const { visible } = modal;
    return (
      <Modal
        containerStyle={{ width: 300 }}
        visible={visible}
        onClose={close}
        renderModal={() => (
          <div style={{ padding: 30, paddingBottom: 10, paddingTop: 10 }}>
            <h2 style={{ fontWeight: 900, marginBottom: 10 }}>Lỗi</h2>
            <div>{message}</div>
            <Flexbox containerStyle={{ marginTop: 20 }} row spaceBetween>
              <div />
              <Button onClick={close} text="OK" clear />
            </Flexbox>
          </div>
        )}
      />
    );
  }
}

export function mapStateToProps(state) {
  return {
    modal: state.modal[REDUCERS.GLOBAL_ERROR_MODAL].modal,
    message: state.modal[REDUCERS.GLOBAL_ERROR_MODAL].message,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      close: closeModal,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(GlobalErrorModal);
