import { REDUCERS } from '../../../constants/COMMON';
import { MODAL_ACTION_TYPES } from '../../../constants/MODAL_ACTION_TYPES';

const openModal = message => ({
  type: MODAL_ACTION_TYPES.OPEN,
  reducerId: REDUCERS.GLOBAL_ERROR_MODAL,
  message,
});

export const showMessage = message => async dispatch => {
  dispatch(openModal(message));
};
