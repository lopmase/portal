import { combineReducers } from 'redux';
import createModalReducer from '../../../helpers/createModalReducer';
import { REDUCERS } from '../../../constants/COMMON';
import { MODAL_ACTION_TYPES } from '../../../constants/MODAL_ACTION_TYPES';

const message = (state = '', action) => {
  if (action.reducerId === REDUCERS.GLOBAL_ERROR_MODAL && action.type === MODAL_ACTION_TYPES.OPEN) {
    return action.message;
  }
  return state;
};

export default combineReducers({
  message,
  modal: createModalReducer(REDUCERS.GLOBAL_ERROR_MODAL),
});
