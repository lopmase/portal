import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { convertStringToHTML } from '../../../helpers/common';
import './HtmlDescription.scss';

const HtmlDescription = ({ description, alwaysShowFull, convert }) => {
  const [showFullDescription, setShowFullDescription] = useState(alwaysShowFull);
  const htmlContent = convert ? convertStringToHTML(description) : description;

  return (
    <>
      {htmlContent ? (
        <p className="html-description" style={{ maxHeight: showFullDescription ? 500 : 40 }}>
          <div
            dangerouslySetInnerHTML={{
              __html: htmlContent,
            }}
          />
        </p>
      ) : (
        <p className="html-description" style={{ maxHeight: showFullDescription ? 500 : 40 }}>
          {description}
        </p>
      )}
      {description && !alwaysShowFull && (
        <div
          className="html-description-showFullButton"
          onClick={() => setShowFullDescription(!showFullDescription)}
        >
          {showFullDescription ? 'Thu gọn' : 'Xem thêm'}
        </div>
      )}
    </>
  );
};

HtmlDescription.propTypes = {
  description: PropTypes.string.isRequired,
  alwaysShowFull: PropTypes.bool,
  convert: PropTypes.bool,
};

HtmlDescription.defaultProps = {
  alwaysShowFull: false,
  convert: true,
};

export default HtmlDescription;
