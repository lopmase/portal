import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter, Link } from 'react-router-dom';
import { compose } from 'recompose';

import styles from '../../styles/_Colors.scss';
import { Button } from '../../components';

import { ROUTES } from '../../constants/ROUTES';

class NotFound extends Component {
  componentDidMount() {}

  render() {
    return (
      <div style={{}}>
        <div
          style={{
            textAlign: 'center',
            height: 'calc(100vh - 70px)',
            display: 'flex',
            justifyContent: 'center',
            flexDirection: 'column',
          }}
        >
          <div>
            <h1
              style={{
                marginBottom: 20,
                textTransform: 'uppercase',
                color: styles.grayBlueColor,
                opacity: 0.6,
              }}
            >
              Page Not Found
            </h1>
            <Link to={ROUTES.DASHBOARD}>
              <Button text="Back Home" />
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export function mapStateToProps() {
  return {};
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(NotFound);
