import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook } from '@fortawesome/free-brands-svg-icons';
import { WEB_URL } from '../../../constants/APP_CONST';

const FacebookShare = ({ slug }) => {
  return (
    <div style={{ visibility: 'hidden', height: 0 }}>
      <div data-href={`${WEB_URL}/`} data-layout="button_count" data-size="large">
        <a
          id={`house-fb-share-${slug}`}
          href={`https://www.facebook.com/sharer/sharer.php?u=${WEB_URL}/bat-dong-san/${slug}&amp;src=sdkpreparse`}
          target="_blank"
          rel="noopener noreferrer"
        >
          <FontAwesomeIcon style={{ marginRight: 10 }} icon={faFacebook} />
          {/* <FontAwesomeIcon style={{ marginRight: 10 }} icon={faShare} /> */}
          Chia sẻ FB
        </a>
      </div>
    </div>
  );
};

FacebookShare.propTypes = {
  slug: PropTypes.string.isRequired,
};

export default FacebookShare;
