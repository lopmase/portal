import React from 'react';
import PropTypes from 'prop-types';
import { faShare, faCopy } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook } from '@fortawesome/free-brands-svg-icons';
import { Menu, Dropdown } from 'antd';
import './ShareButton.scss';
import ZaloShare from './ZaloShare';
import FacebookShare from './FacebookShare';
import CopyToClipboard from '../CopyToClipboard/CopyToClipboard';

const ShareButton = ({ house, marginLeft, fontSize, color }) => {
  const onActionClick = item => {
    let shareButton;
    if (item.key === '1') {
      const shareDiv = document.getElementById(`share-zalo-btn-${house.id}`);
      shareButton = shareDiv && shareDiv.firstChild ? shareDiv.firstChild.firstChild : null;
    } else if (item.key === '2') {
      shareButton = document.getElementById(`house-fb-share-${house.slug}`);
    } else {
      shareButton = document.getElementById(`house-copy-url-${house.slug}`);
    }
    shareButton && shareButton.click();
  };
  const menu = (
    <Menu onClick={onActionClick}>
      <Menu.Item key="1" style={{ fontSize }}>
        <ZaloShare id={house.id} slug={house.slug} style={{ fontSize }} />
      </Menu.Item>
      <Menu.Item key="2" style={{ fontSize }}>
        <FontAwesomeIcon style={{ marginRight: 10 }} icon={faFacebook} />
        FaceBook
      </Menu.Item>
      <Menu.Item key="3" style={{ fontSize }}>
        <FontAwesomeIcon style={{ marginRight: 10 }} icon={faCopy} />
        Copy URL
      </Menu.Item>
    </Menu>
  );

  return (
    <span key="feed-share">
      <Dropdown trigger="click" overlay={menu}>
        <span className="share-button" style={{ fontSize, color }}>
          <FontAwesomeIcon style={{ marginRight: 5, marginLeft }} icon={faShare} />
          Chia sẻ
        </span>
      </Dropdown>
      <div style={{ display: 'inline-block' }}>
        {/* <ZaloShare id={house.id} slug={house.slug} style={{ fontSize }} /> */}
        <FacebookShare slug={house.slug} style={{ fontSize }} />
        <CopyToClipboard slug={house.slug} style={{ fontSize }} />
      </div>
    </span>
  );
};

ShareButton.propTypes = {
  marginLeft: PropTypes.any,
  fontSize: PropTypes.any,
  color: PropTypes.any,
  house: PropTypes.any.isRequired,
};

ShareButton.defaultProps = {
  marginLeft: 0,
  fontSize: 16,
  color: 808080,
};

export default ShareButton;
