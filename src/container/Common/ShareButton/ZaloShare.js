import React from 'react';
import PropTypes from 'prop-types';
import { WEB_URL } from '../../../constants/APP_CONST';

const ZaloShare = ({ id, slug }) => {
  React.useEffect(() => {
    setTimeout(() => {
      window.ZaloSocialSDK.reload();
    }, 1000);
  }, [slug]);

  return (
    <div
      style={{
        height: 'auto',
        width: 'auto',
        // border: '1px solid red',
        position: 'relative',
        // left: 50,
        // visibility: 'hidden',
      }}
      id={`share-zalo-btn-${id}`}
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{
        __html: `<div
              class="zalo-share-button"
              data-href="${WEB_URL}/bat-dong-san/${slug}"
              data-oaid="579745863508352884"
              data-layout="1"
              data-color="blue"
              data-customize=false
              style={width: 100px}
              ></div>`,
      }}
    />
  );
};

ZaloShare.propTypes = {
  id: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
};

export default ZaloShare;
