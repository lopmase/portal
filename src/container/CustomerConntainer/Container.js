import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { PrivateRoute } from '../../components';
import { ROUTES } from '../../constants/ROUTES';
import Customers from './Customers';
import NotFound from '../Common/NotFound';

class CustomerContainer extends Component {
  state = {};

  render() {
    return (
      <Switch>
        {/* <PrivateRoute exact path={ROUTES.NEW_HOUSE} component={NewHouse} /> */}
        <PrivateRoute
          exact
          path={ROUTES.EDIT_HOUSE}
          // component={props => <NewHouse {...props} editMode />}
        />
        <PrivateRoute path={ROUTES.CUSTOMERS} component={Customers} />
        <PrivateRoute exact path={ROUTES.HOUSE} component={Customers} />
        <Route component={NotFound} />
      </Switch>
    );
  }
}

export default CustomerContainer;
