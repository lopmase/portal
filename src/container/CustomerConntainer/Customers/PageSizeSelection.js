import React from 'react';

import { Dropdown } from '../../../components';

const styleProps = {
  clear: true,
  containerStyle: {
    marginRight: 10,
  },
};

const pageSizeOptions = [
  { value: 10, text: 'Hiển thị 10' },
  { value: 20, text: 'Hiển thị 20' },
  { value: 50, text: 'Hiển thị 50' },
  { value: 100, text: 'Hiển thị 100' },
];
// eslint-disable-next-line react/prop-types
const PageSizeDropdown = ({ selected, onChanged }) => (
  <div className="page-size">
    <Dropdown {...styleProps} items={pageSizeOptions} selected={selected} onChanged={onChanged} />
  </div>
);
export default PageSizeDropdown;
