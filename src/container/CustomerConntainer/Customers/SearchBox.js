import React from 'react';
import PropTypes from 'prop-types';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Input } from '../../../components';

const SearchBox = ({ data, doSearch, containerStyle }) => {
  const { search } = data;
  return (
    <div className="search-component" style={{ ...containerStyle }}>
      <FontAwesomeIcon icon={faSearch} />
      <Input
        value={search}
        placeholder="Tìm khách hàng"
        onChange={(name, value) => doSearch(value)}
      />
    </div>
  );
};

SearchBox.propTypes = {
  data: PropTypes.object.isRequired,
  containerStyle: PropTypes.object,
  doSearch: PropTypes.func.isRequired,
};

SearchBox.defaultProps = {
  containerStyle: {},
};

export default SearchBox;
