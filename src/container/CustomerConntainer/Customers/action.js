/* eslint-disable array-callback-return */
/* eslint-disable no-param-reassign */
import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { generateModalActions } from '../../../helpers/modalAction';
import { generateDatalistActions } from '../../../helpers/dataListAction';
import { waitForFinalEvent } from '../../../helpers/common';
import Selector from '../Selector';
import { exportExcel } from '../../Common/ExportExcel/action';

const {
  fetchDone,
  fetching,
  fetchError,
  nextPage,
  updateSort,
  search: searchAction,
  updatePage,
  previousPage,
} = generateDatalistActions(REDUCERS.CUSTOMERS);

const { openModal, closeModal } = generateModalActions(REDUCERS.GLOBAL_LOADING);

export const fetch = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { page, pageSize, sortColumn, search, sortOrder } = Selector.customers(getState());
    dispatch(fetching());
    const response = await thunkDependencies.customer.list({
      page,
      size: pageSize,
      order: sortOrder,
      sort: sortColumn,
      search,
    });

    const result = apiSerializer(response);
    let indexArr;
    result.data.map((value, index) => {
      const duplicateValue = [];
      const dataFilter = result.data.filter(
        (element, elementIndex) =>
          (element.phone_number === value.phone_number ||
            element.phone_number === value.phone_number2 ||
            element.phone_number === value.phone_number3 ||
            element.phone_number === value.phone_number4 ||
            element.phone_number2 === value.phone_number ||
            element.phone_number3 === value.phone_number ||
            element.phone_number4 === value.phone_number) &&
          index <= elementIndex,
      );

      dataFilter && duplicateValue.push(...dataFilter);
      duplicateValue.length > 0 && (value.user = dataFilter.map(e => e.user.name));
      indexArr = result.data.reduce((a, e, i) => {
        if (
          (e.phone_number === value.phone_number ||
            e.phone_number === value.phone_number2 ||
            e.phone_number === value.phone_number3 ||
            e.phone_number === value.phone_number4 ||
            e.phone_number2 === value.phone_number ||
            e.phone_number3 === value.phone_number ||
            e.phone_number4 === value.phone_number) &&
          i > index
        )
          a.push(i);
        return a;
      }, []);

      if (value.phone_number2) {
        value.phone_number = [value.phone_number, value.phone_number2];
      }
      if (value.phone_number3) {
        value.phone_number = [...value.phone_number, value.phone_number3];
      }
      if (value.phone_number4) {
        value.phone_number = [...value.phone_number, value.phone_number4];
      }
      duplicateValue.map((d, i) => {
        if (i > 0 && Array.isArray(value.phone_number)) {
          value.phone_number = [
            ...new Set([
              ...value.phone_number,
              d.phone_number,
              d.phone_number2,
              d.phone_number3,
              d.phone_number4,
            ]),
          ];
        }
        if (i > 0 && !Array.isArray(value.phone_number)) {
          value.phone_number = [
            ...new Set([
              value.phone_number,
              d.phone_number,
              d.phone_number2,
              d.phone_number3,
              d.phone_number4,
            ]),
          ];
        }
      });
      if (indexArr.length > 0) {
        indexArr.map((v, ind) => {
          ind === 0 && result.data.splice(v, 1);
          ind !== 0 && result.data.splice(v - ind, 1);
        });
      }
    });
    if (page === 1) {
      dispatch(fetchDone(result.data, result.total, Math.ceil(result.total / pageSize)));
    } else {
      dispatch(fetchDone(result.data, result.total, Math.ceil(result.total / pageSize)));
    }
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};

export const goPreviousPage = () => async (dispatch, getState) => {
  dispatch(previousPage());
  dispatch(fetch());
};
export const goNextPage = () => async (dispatch, getState) => {
  dispatch(nextPage());
  dispatch(fetch());
};

export const deleteCustomer = id => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(openModal());
    await thunkDependencies.customers.remove(id);
    dispatch(closeModal());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};

export const loadMore = () => async (dispatch, getState) => {
  const { total, list, fetching: isFetchinng } = Selector.customers(getState());
  if (isFetchinng) {
    return;
  }
  if (list.length >= total) {
    return;
  }
  dispatch(nextPage());
  dispatch(fetch());
};

export const sortColumn = (columnName, order) => async dispatch => {
  dispatch(updateSort(columnName, order));
  dispatch(updatePage(1));
  dispatch(fetch());
};

export const doSearch = value => async dispatch => {
  dispatch(searchAction(value));
  dispatch(updatePage(1));
  waitForFinalEvent(() => dispatch(fetch()), 500, 'search customers');
};

export const exportAllData = () => async dispatch => {
  try {
    await dispatch(exportExcel('customer'));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};
