/* eslint-disable camelcase */
/* eslint-disable react/jsx-indent */
import React from 'react';
import { formatImageUrl } from '../../../helpers/common';
// import UserFilter from '../CustomersFilter/UserFilter';

export const columns = [
  {
    title: '#',
    dataIndex: 'id',
    sorter: true,
    width: 50,
    // operation: 'like',
    // filterDropdown: props => <UserFilter {...props} />,
  },
  {
    title: 'Ảnh',
    dataIndex: 'avatar',
    width: 70,
    render: value => (
      <span>
        {value && (
          <div className="avatar-cell">
            <img src={formatImageUrl(value.thumbnail)} alt="customer" />
          </div>
        )}
      </span>
    ),
  },
  {
    title: 'Tên',
    dataIndex: 'name',
    sorter: true,
    width: 150,
    // operation: 'like',
    // filterDropdown: props => <UserFilter {...props} />,
    render: (value, { phone_number, phone_number2, phone_number3, phone_number4 }) => (
      <span>
        <div className="bold-primary-text long-text">{`${value}`}</div>
        {phone_number && Array.isArray(phone_number) ? (
          phone_number.map(p => <div className="bold-text long-text">{`${p || ''}`}</div>)
        ) : (
          <div className="bold-text long-text">
            {`${phone_number !== undefined ? phone_number : ''}`}
          </div>
        )}
        {/* {phone_number2 && (
          <div className="bold-text long-text">
            {`${phone_number2 !== undefined ? phone_number2 : ''}`}
          </div>
        )}
        {phone_number3 && (
          <div className="bold-text long-text">
            {`${phone_number3 !== undefined ? phone_number3 : ''}`}
          </div>
        )}
        {phone_number4 && (
          <div className="bold-text long-text">
            {`${phone_number4 !== undefined ? phone_number4 : ''}`}
          </div>
        )} */}
      </span>
    ),
  },
  {
    title: 'Ngày Sinh',
    dataIndex: 'date_of_birth',
    width: 120,
    sorter: true,
  },
  {
    title: 'Fax',
    dataIndex: 'fax',
    sorter: true,
    width: 100,
    // operation: 'like',
    // filterDropdown: props => <UserFilter {...props} />,
  },
  {
    title: 'Email',
    dataIndex: 'email',
    sorter: true,
    width: 150,
    // operation: 'like',
    // filterDropdown: props => <UserFilter {...props} />,
  },
  {
    title: 'Địa chỉ',
    dataIndex: 'address',
    sorter: true,
    width: 150,
    // operation: 'like',
    // filterDropdown: props => <UserFilter {...props} />,
  },
  {
    title: 'CMND',
    dataIndex: 'social_id',
    sorter: true,
    width: 120,
    // operation: 'like',
    // filterDropdown: props => <UserFilter {...props} />,
  },
  {
    title: 'Ghi chú',
    dataIndex: 'description',
    width: 100,
    sorter: true,
    // operation: 'like',
    // filterDropdown: props => <UserFilter {...props} />,
  },
  {
    title: 'Tính cách',
    dataIndex: 'character',
    width: 100,
    sorter: false,
    // operation: 'like',
    // filterDropdown: props => <UserFilter {...props} />,
  },
  {
    title: 'Nhân viên',
    dataIndex: 'user_id',
    sorter: true,
    width: 120,
    // operation: 'like',
    filterKey: 'user_name',
    // filterDropdown: props => <UserFilter {...props} />,
    render: (value, customer) =>
      customer.user
        ? customer.user.map(val => (
            <tr>
              <td>{val}</td>
            </tr>
          ))
        : '',
  },
];
