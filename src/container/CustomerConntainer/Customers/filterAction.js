import _ from 'lodash';
import { CUSTOMER_FILTER_TYPES, OPERATION } from './filterTypes';
import { fetch } from './action';
import { columns } from './columns';
import { generateDatalistActions } from '../../../helpers/dataListAction';
import { REDUCERS } from '../../../constants/COMMON';

const { updatePage } = generateDatalistActions(REDUCERS.CUSTOMERS);

const updateAll = filters => ({
  type: CUSTOMER_FILTER_TYPES.UPDATE_ALL,
  filters,
});

export const updateFilters = filters => async dispatch => {
  const filterList = _.toPairs(filters)
    .filter(p => p[1].length)
    .map(pair => {
      const col = columns.find(c => c.dataIndex === pair[0]) || {};
      return {
        column: col.filterKey || pair[0],
        operation: col.operation || OPERATION.IN,
        values: pair[1],
      };
    });
  dispatch(updateAll(filterList));
  dispatch(updatePage(1));
  dispatch(fetch());
};
