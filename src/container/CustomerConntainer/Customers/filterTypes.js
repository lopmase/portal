export const CUSTOMER_FILTER_TYPES = {
  UPDATE_ALL: 'CUSTOMER_FILTER_TYPES/UPDATE_ALL',
};
export const OPERATION = {
  EQUAL: '=',
  BETWEEN: 'between',
  GREATER_THAN: '>',
  IN: 'in',
};
