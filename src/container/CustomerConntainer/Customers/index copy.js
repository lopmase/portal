/* eslint-disable react/prop-types */
import React from 'react';
import { compose, lifecycle } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { faCog, faPen } from '@fortawesome/free-solid-svg-icons';
import { Flexbox, Table, Button } from '../../../components';
import './Customers.scss';
import { REDUCERS } from '../../../constants/COMMON';
import * as actions from './action';

import { columns } from './columns';
import FilterButton from './CustomerListFilterButton';
import PageSizeSelection from './PageSizeSelection';
import LeftButton from '../NewCustomer/LeftButton';
import Pagination from './Pagination';
import { confirm } from '../../ShellContainner/ConfirmDialog/action';
import { fetchDetail } from '../NewCustomer/action';

const editButtonProps = {
  iconColor: '#0ea60e',
  icon: faPen,
  iconOnly: true,
  clear: true,
};

const Dashboard = ({
  dataSource: { list, fetching, total, total_pages: totalPages, pageSize, page: currentPage },
  goNextPage,
  goPrevPage,
  changePageSize,
  openNewCustomerModal,
  fetchCustomerDetail,
}) => {
  const onEdit = item => {
    const { id } = item;
    fetchCustomerDetail(id);
    openNewCustomerModal(true);
  };

  const renderCell = ({ item, column }) => {
    if (column.key === 'detail') {
      return (
        <td>
          <Button {...editButtonProps} onClick={() => onEdit(item)} />
        </td>
      );
    }
    const cell = item[column.key];
    let value = '';
    if (column.accessor) {
      value = column.accessor(item, {});
    } else {
      value = cell ? cell.toString() : '';
    }
    return <td className={column.key === 'id' ? 'bold-text' : ''}>{value}</td>;
  };
  return (
    <div className="customers-page" style={{}}>
      <div className="toolbar">
        <Flexbox
          row
          spaceBetween
          containerStyle={{ marginBottom: 20, marginTop: 20, alignItems: 'center' }}
        >
          <Flexbox>
            <div className="total">{`${total} Khách hàng`}</div>
            <PageSizeSelection
              selected={pageSize}
              onChanged={({ item }) => changePageSize(item.value)}
            />
          </Flexbox>
          <div>
            <Pagination
              currentPage={currentPage}
              pageNumbers={totalPages}
              onNext={goNextPage}
              onPrev={goPrevPage}
            />
          </div>
          <Flexbox>
            <LeftButton openModal={openNewCustomerModal} />
            <FilterButton />
            <Button
              containerStyle={{ marginLeft: 10, paddingRight: 12, paddingLeft: 12 }}
              icon={faCog}
              iconColor="gray"
              iconOnly
              backgroundColor="white"
            />
          </Flexbox>
        </Flexbox>
      </div>
      <div style={{}}>
        <Table loading={fetching} data={list} columns={columns} renderCell={renderCell} />
      </div>
      <div>
        <Flexbox row containerStyle={{ marginTop: 20, alignItems: 'center' }}>
          <Pagination
            currentPage={currentPage}
            pageNumbers={totalPages}
            onNext={goNextPage}
            onPrev={goPrevPage}
          />
        </Flexbox>
      </div>
    </div>
  );
};

export const mapStateToProps = state => ({
  dataSource: state.customer[REDUCERS.CUSTOMERS],
});

const mapDispatchToProps = {
  ...actions,
  openConfirmDialog: confirm,
  fetchCustomerDetail: fetchDetail,
};

export const useLifeCycle = lifecycle({
  componentDidMount() {
    this.props.fetch();
  },
});

const connetToRedux = connect(mapStateToProps, mapDispatchToProps);
export default compose(connetToRedux, useLifeCycle, withRouter)(Dashboard);
