/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
/* eslint-disable prefer-destructuring */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { faSearch, faPlus } from '@fortawesome/free-solid-svg-icons';
import Pagination from './Pagination';
import { Flexbox, Button, Label } from '../../../components';
import './Customers.scss';
import * as actions from './action';
import Selector from '../Selector';
import { columns } from './columns';
import SearchBox from './SearchBox';
import AntTable from '../../../components/AntTable/AntTable';
import { openNewCustomerModal } from '../NewCustomer/action';
import Excel from '../../Common/ExportExcel/Excel';

class Customers extends Component {
  static propTypes = {};

  static defaultProps = {};

  state = {
    deviceMode: false,
  };

  componentDidMount() {
    const { fetch } = this.props;
    this.setState({
      deviceMode: window.innerWidth < 500,
      tableHeight: window.innerHeight - (window.innerWidth < 500 ? 190 : 150),
    });

    const list = document.getElementsByClassName('ant-table-body');
    if (list && list[0]) {
      this.tableBody = list[0];
      // if (this.tableBody.addEventListener) {
      //   this.tableBody.addEventListener('scroll', this.onTableScroll);
      // }
    }
    fetch();
  }

  // componentWillUnmount() {
  //   if (this.tableBody) {
  //     this.tableBody.removeEventListener('scroll', this.onTableScroll);
  //   }
  // }

  onTableScroll = event => {
    const { currentTarget } = event;
    const { tableHeight } = this.state;
    const { loadMore, dataSource } = this.props;
    const { fetching } = dataSource;
    if (fetching) {
      return;
    }
    if (currentTarget.scrollHeight - currentTarget.scrollTop <= tableHeight + 50) {
      loadMore();
    }
  };

  getColumnFilter = column => {
    const { options } = this.props;
    if (options && column.getFilter) {
      try {
        const filters = column.getFilter(options);
        if (Array.isArray(filters) && filters[0] && filters[0].value) {
          return { filters };
        }
      } catch (error) {
        return {};
      }
    }
    return {};
  };

  onRowSelect = ({ index, rowData }) => {
    const { openNewCustomerDialog } = this.props;
    if (index >= 0) {
      openNewCustomerDialog(true, rowData.id);
    }
  };

  onTableChanged = options => {
    const { sortColumn, updateAllFilters } = this.props;
    if (options.sortChanged) {
      sortColumn(options.sortField, options.sortOrder);
    } else {
      updateAllFilters && updateAllFilters(options.filters);
    }
  };

  render() {
    const {
      dataSource,
      doSearch,
      openNewCustomerDialog,
      exportAllData,
      goPreviousPage,
      goNextPage,
    } = this.props;
    const { list, fetching, total, page, total_pages } = dataSource;
    const { deviceMode, tableHeight } = this.state;
    const loadedRows = `${list.length}/${total}`;
    return (
      <div className="customers-page" style={{ padding: deviceMode ? 10 : 20 }}>
        <div className="toolbar">
          <Flexbox row spaceBetween containerStyle={{ marginBottom: 20, alignItems: 'center' }}>
            <Flexbox row flexStart alignItems="center">
              <h1 style={{ marginRight: 10, marginBottom: 0 }}>Khách hàng</h1>
              <Label text={loadedRows} />
            </Flexbox>
            {!deviceMode && (
              <SearchBox
                containerStyle={{ maxWidth: 400, width: 300 }}
                doSearch={doSearch}
                data={dataSource}
              />
            )}
            <div>
              <Excel hideFilterButton onExportAll={exportAllData} />
              <Button
                onClick={() => openNewCustomerDialog()}
                containerStyle={{ paddingRight: 14, paddingLeft: 14, marginLeft: 5 }}
                icon={faPlus}
                iconColor="white"
                iconOnly
              />
              {deviceMode && (
                <Button
                  containerStyle={{ marginLeft: 10, paddingRight: 14, paddingLeft: 14 }}
                  icon={faSearch}
                  iconColor="gray"
                  iconOnly
                  backgroundColor="white"
                />
              )}
            </div>
          </Flexbox>
        </div>
        <AntTable
          data={list}
          columns={columns.map(col => ({
            ...(deviceMode
              ? { ...col, fixed: undefined }
              : {
                  ...col,
                  width:
                    col.dataIndex === 'id' || col.dataIndex === 'avatar' ? col.width : undefined,
                }),
            ...this.getColumnFilter(col),
          }))}
          rowClick={this.onRowSelect}
          // autoSize={!deviceMode}
          width={900}
          height={tableHeight}
          onChanged={this.onTableChanged}
          loading={fetching}
        />
        <Pagination
          currentPage={page}
          pageNumbers={total_pages}
          onNext={goNextPage}
          onPrev={goPreviousPage}
        />
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    dataSource: Selector.customers(state),
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      openNewCustomerDialog: openNewCustomerModal,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Customers);
