import React from 'react';
import Button from '../../../components/Button/Button';

// eslint-disable-next-line react/prop-types
const LeftCreateButton = ({ openModal }) => <Button text="+ Tạo mới" onClick={() => openModal()} />;

export default LeftCreateButton;
