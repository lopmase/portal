import _ from 'lodash';
import { toast } from 'react-toastify';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateModalActions } from '../../../helpers/modalAction';
import { generateFormActions } from '../../../helpers/formAction';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { uploadImages } from '../../HouseContainer/UploadJobs/action';
import { fetch } from '../Customers/action';
import { generateDataActions } from '../../../helpers/dataAction';
import { isSuperAdmin } from '../../../helpers/roleUtil';

const { fetchDone, fetching, fetchError } = generateDataActions(REDUCERS.CUSTOMER_DETAIL_DATA);
const { closeModal } = generateModalActions(REDUCERS.NEW_CUSTOMER_MODAL);
const { openModal: openNewCustomerAction } = generateModalActions(REDUCERS.NEW_CUSTOMER_MODAL);
const {
  handleFieldChanged,
  reset,
  setEditMode,
  submitDone,
  submitError,
  submitting,
  validateAll,
} = generateFormActions(REDUCERS.NEW_CUSTOMER);

export const handleChange = (name, value) => async dispatch => {
  dispatch(handleFieldChanged(name, value));
};

const userFields = [
  'phone_number',
  'name',
  'email',
  'fax',
  'social_id',
  'date_of_birth',
  'address',
  'avatar',
  'id',
  'description',
  'user_id',
  'character',
  'phone_number2',
  'phone_number3',
  'phone_number4',
];

export const fetchDetail = id => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const me = getState().auth.user.data;
    const response = await thunkDependencies.customer.detailCustomer(id);
    const result = apiSerializer(response);
    dispatch(fetchDone(result));
    userFields.forEach(field => {
      if (field === 'id') {
        dispatch(handleChange('customer_id', result[field]));
      } else if (field === 'phone_number' && !isSuperAdmin(me) && result.user_id !== me.id) {
        dispatch(handleChange(field, '0000000000'));
      } else {
        dispatch(handleChange(field, result[field]));
      }
    });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};

export const create = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { data } = getState().customer[REDUCERS.NEW_CUSTOMER].data;
    let imageId = null;
    dispatch(submitting());
    if (data.image) {
      await dispatch(uploadImages([data.image]));
      const uploadJobs = getState().house[REDUCERS.UPLOAD_JOBS].jobs;
      imageId = uploadJobs.results[0] ? uploadJobs.results[0].id : null;
    }
    const form = { ...data, image: imageId || undefined };
    const response = await thunkDependencies.customer.new(form);
    const result = apiSerializer(response);
    dispatch(submitDone(result));
    dispatch(fetch());
    dispatch(closeModal());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
    dispatch(submitError(message));
  }
};

export const update = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { data } = getState().customer[REDUCERS.NEW_CUSTOMER].data;
    const loadedData = getState().customer[REDUCERS.CUSTOMER_DETAIL_DATA].data;
    const changedData = {};
    _.forOwn(data, (value, key) => {
      if (value !== loadedData[key]) {
        changedData[key] = value;
      }
    });
    if (_.keys(changedData).length >= 1) {
      changedData.customer_id = data.customer_id;
      let imageId = null;
      dispatch(submitting());
      if (changedData.image) {
        await dispatch(uploadImages([changedData.image]));
        const uploadJobs = getState().house[REDUCERS.UPLOAD_JOBS].jobs;
        imageId = uploadJobs.results[0] ? uploadJobs.results[0].id : null;
      }
      const form = { ...changedData, image: imageId || undefined };
      const response = await thunkDependencies.customer.updateCustomer(form);
      const result = apiSerializer(response);
      dispatch(submitDone(result));
      dispatch(fetch());
      dispatch(closeModal());
      toast.success('Cập nhật khách hàng thành công!');
    } else {
      toast.success('Không có thay đổi để cập nhật');
    }
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
    dispatch(submitError(message));
  }
};

export const submit = editMode => async (dispatch, getState) => {
  dispatch(validateAll());
  const { error } = getState().customer[REDUCERS.NEW_CUSTOMER].data;
  const isValid = _.values(error).filter(i => i.length).length === 0;
  if (isValid) {
    if (editMode) {
      dispatch(update());
    } else {
      dispatch(create());
    }
  }
};

export const openNewCustomerModal = (editMode, customerId) => async (dispatch, getState) => {
  const user = getState().auth.user.data;
  dispatch(reset());
  if (user) {
    dispatch(handleFieldChanged('user_id', user.id));
  }
  if (editMode) {
    dispatch(fetchDetail(customerId));
  }
  dispatch(setEditMode(editMode || false));
  dispatch(reset());
  dispatch(openNewCustomerAction());
};
