/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { Modal, Flexbox } from '../../../components';
import './NewCustomer.scss';
import { REDUCERS } from '../../../constants/COMMON';
import * as actions from './action';
import { generateModalActions } from '../../../helpers/modalAction';

import BaseBody from './ModalBody';
import Footer from './ModalFooter';
import { fetch } from '../../UserConntainer/Users/action';
import { generateDatalistActions } from '../../../helpers/dataListAction';

const { closeModal, toggle } = generateModalActions(REDUCERS.NEW_CUSTOMER_MODAL);
const { updatePageSize } = generateDatalistActions(REDUCERS.USERS);

class NewCustomer extends Component {
  static propTypes = {};

  static defaultProps = {};

  state = {
    deviceMode: false,
  };

  componentDidMount() {
    this.setState({
      deviceMode: window.innerWidth < 500,
    });
  }

  componentDidUpdate(prevProps) {
    const { visible } = this.props;
    if (visible && !prevProps.visible) {
      const { updateUserPageSize, fetchUsers } = this.props;
      updateUserPageSize(100);
      fetchUsers();
    }
  }

  render() {
    const { form, modal, closeNewCustomerModal, handleChange, users, submit } = this.props;
    const { visible } = modal;
    const { deviceMode } = this.state;
    const { submitting, error, editMode, touched } = form;
    const isValid = _.values(error).filter(i => i.length).length === 0;
    const isTouched = _.values(touched).filter(i => i === true).length > 0;

    return (
      <>
        <Modal
          visible={visible}
          onClose={submitting ? () => {} : closeNewCustomerModal}
          loading={submitting}
          containerStyle={deviceMode ? {} : { width: 'auto', maxWidth: 900 }}
          renderModal={() => (
            <div className="new-customer">
              {!editMode && <h2>Tạo khách hàng mới</h2>}
              {editMode && <h2>Cập nhật khách hàng</h2>}

              <BaseBody users={users} form={form} handleChange={handleChange} />

              <Flexbox row spaceBetween>
                <div />
                <Flexbox row>
                  <Footer
                    isValid={isValid && isTouched}
                    closeModal={closeNewCustomerModal}
                    onSubmit={() => submit(editMode)}
                    editMode={editMode}
                  />
                </Flexbox>
              </Flexbox>
            </div>
          )}
        />
      </>
    );
  }
}

export function mapStateToProps(state) {
  return {
    form: state.customer[REDUCERS.NEW_CUSTOMER].data,
    modal: state.customer[REDUCERS.NEW_CUSTOMER].modal,
    visible: state.customer[REDUCERS.NEW_CUSTOMER].modal.visible,
    users: state.user[REDUCERS.USERS].list,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      closeNewCustomerModal: closeModal,
      toggleModal: toggle,
      fetchUsers: fetch,
      updateUserPageSize: updatePageSize,
    },
    dispatch,
  );
}
export default compose(connect(mapStateToProps, mapDispatchToProps))(NewCustomer);
