import { combineReducers } from 'redux';
import createFormReducer from '../../../helpers/createFormReducer';
import createModalReducer from '../../../helpers/createModalReducer';
import { REDUCERS } from '../../../constants/COMMON';

const initialValue = {
  name: '',
  phone_number: '',
  email: '',
  phone_number2: '',
  phone_number3: '',
  phone_number4: '',
};

export default combineReducers({
  modal: createModalReducer(REDUCERS.NEW_CUSTOMER_MODAL),
  data: createFormReducer(REDUCERS.NEW_CUSTOMER, initialValue, {
    name: [['required', 'Tên bắt buộc']],
    phone_number: [
      ['required', 'Số điện thoại bắt buộc'],
      [value => value.match(/^[0-9]*$/g), 'Số điện thoại chỉ điền số'],
    ],
  }),
});
