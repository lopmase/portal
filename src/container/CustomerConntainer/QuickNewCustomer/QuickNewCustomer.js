/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Modal, FormInput, Button, Flexbox, Avatar } from '../../../components';
import './QuickNewCustomer.scss';
import { REDUCERS } from '../../../constants/COMMON';
import * as actions from './action';
import { generateModalActions } from '../../../helpers/modalAction';

const { closeModal } = generateModalActions(REDUCERS.QUICK_NEW_CUSTOMER_MODAL);

class QuickNewCustomer extends Component {
  static propTypes = {};

  static defaultProps = {};

  state = {};

  componentDidMount() {}

  renderCell = ({ item, column }) => {
    const cell = item[column.key];
    let value = '';
    if (column.accessor) {
      value = column.accessor(item, {});
    } else {
      value = cell ? cell.toString() : '';
    }
    return <td className={column.key === 'id' ? 'bold-text' : ''}>{value}</td>;
  };

  handleCreate = async () => {
    const { create, onAfterCreated, userId } = this.props;

    const rs = await create(userId);
    if (rs) {
      onAfterCreated(rs);
    }
  };

  render() {
    const { form, modal, close, handleChange } = this.props;
    const { visible } = modal;
    const { data, submitting } = form;
    const { image } = data;
    return (
      <Modal
        visible={visible}
        onClose={close}
        loading={submitting}
        containerStyle={{ maxWidth: 400 }}
        renderModal={() => (
          <div className="new-customer">
            <h2>Tạo khách hàng mới</h2>
            <Avatar
              containerStyle={{ marginTop: 20 }}
              onImageAdded={newImage => handleChange('image', newImage)}
              image={image}
              onImageRemoved={() => handleChange('image', null)}
            />
            <FormInput form={form} handleChange={handleChange} label="Tên" name="name" />
            <FormInput form={form} handleChange={handleChange} label="SĐT" name="phone_number" />
            <Button containerStyle={{ marginTop: 10 }} clear text="+ Thêm thông tin" />
            <Flexbox row spaceBetween>
              <div />
              <Flexbox row>
                <Button clear text="Huỷ" onClick={close} />
                <Button text="Lưu" onClick={this.handleCreate} />
              </Flexbox>
            </Flexbox>
          </div>
        )}
      />
    );
  }
}

export function mapStateToProps(state) {
  return {
    form: state.customer[REDUCERS.NEW_CUSTOMER].data,
    modal: state.customer[REDUCERS.QUICK_NEW_CUSTOMER_MODAL],
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      close: closeModal,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(QuickNewCustomer);
