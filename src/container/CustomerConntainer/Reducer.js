import { combineReducers } from 'redux';
import createDatalistReducer from '../../helpers/createDataListReducer';
import { REDUCERS } from '../../constants/COMMON';
import reducer from './NewCustomer/reducer';
import createModalReducer from '../../helpers/createModalReducer';
import createDataReducer from '../../helpers/createDataReducer';

export default combineReducers({
  [REDUCERS.CUSTOMERS]: createDatalistReducer(REDUCERS.CUSTOMERS, { pageSize: 20 }),
  [REDUCERS.QUICK_NEW_CUSTOMER_MODAL]: createModalReducer(REDUCERS.QUICK_NEW_CUSTOMER_MODAL),
  [REDUCERS.NEW_CUSTOMER]: reducer,
  [REDUCERS.CUSTOMER_DETAIL_DATA]: createDataReducer(REDUCERS.CUSTOMER_DETAIL_DATA),
});
