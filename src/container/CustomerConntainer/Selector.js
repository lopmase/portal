import { REDUCERS } from '../../constants/COMMON';

export default {
  customers: state => state.customer[REDUCERS.CUSTOMERS],
  //   filters: state => state.customer[REDUCERS.HOUSE_FILTER],
  //   houseGrouping: state => state.house[REDUCERS.HOUSES_GROUPING],
  //   options: state => state.metadata[REDUCERS.OPTIONS],
  //   newHouse: state => state.house[REDUCERS.NEW_HOUSE],
};
