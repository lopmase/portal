/* eslint-disable react/prop-types */
import React from 'react';
import moment from 'moment';
import { Row, Col } from 'antd';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { FormInput, FormDropdown, FormLabel } from '../../../components';
import { isAdmin } from '../../../helpers/roleUtil';
import { useUserProfile } from '../../../hooks/userProfileHook';

const addressFieldProps = {
  label: 'Địa chỉ',
  name: 'address',
};
const emailFieldProps = {
  label: 'Email',
  name: 'email',
  inputProps: {
    width: 200,
  },
};
const idFieldProps = {
  label: 'CMND',
  name: 'social_id',
  inputProps: {
    width: 200,
  },
};
const remarkFieldProps = {
  label: 'Ghi chú',
  name: 'description',
  multiline: true,
};
const staffFieldProps = {
  label: 'Nhân Viên',
  name: 'user_id',
  containerStyle: {
    minWidth: 150,
  },
};
const characterFieldProps = {
  label: 'Tính cách',
  name: 'character',
  containerStyle: {
    minWidth: 150,
  },
};
const styles = {
  container: {
    marginBottom: 15,
  },
};

let datePicker = null;

const ExtraFields = ({ form, handleChange, users }) => {
  const { data } = form;
  const user = useUserProfile();

  React.useEffect(() => {
    if (datePicker && !data.date_of_birth) {
      const { input } = datePicker;

      if (input) {
        input.value = '';
      }
    }
  }, [data.date_of_birth, datePicker]);

  return (
    <div style={styles.container}>
      <Row gutter={[10, 10]}>
        <Col xs={24} md={12}>
          <FormInput form={form} handleChange={handleChange} {...emailFieldProps} />
        </Col>
        <Col xs={24} md={12}>
          <div className="input-container" style={{ flex: 1 }}>
            <FormLabel text="Ngày sinh" />
            <DayPickerInput
              inputProps={{
                style: { width: 'calc(100%)' },
              }}
              ref={ref => {
                datePicker = ref;
              }}
              value={data.date_of_birth}
              dayPickerProps={{ disabledDays: { after: new Date() } }}
              onDayChange={day =>
                moment(day, 'YYYY-MM-DD', true).isValid()
                  ? handleChange('date_of_birth', moment(day).format('YYYY-MM-DD'))
                  : handleChange('date_of_birth', null)
              }
            />
          </div>
        </Col>
      </Row>
      <Row gutter={[10, 10]}>
        <Col xs={24} md={24}>
          <FormInput form={form} handleChange={handleChange} {...addressFieldProps} />
        </Col>
      </Row>
      <Row gutter={[10, 10]}>
        <Col xs={24} md={12}>
          <FormInput form={form} handleChange={handleChange} {...idFieldProps} />
        </Col>
        <Col xs={24} md={12}>
          {isAdmin(user) && (
            <FormDropdown
              form={form}
              handleChange={handleChange}
              contentHeight={250}
              items={users.map(u => ({ value: u.id, text: u.name }))}
              {...staffFieldProps}
            />
          )}
        </Col>
      </Row>
      <Row gutter={[10, 10]}>
        <Col xs={24} md={24}>
          <FormInput form={form} handleChange={handleChange} {...remarkFieldProps} />
        </Col>
      </Row>
      <Row gutter={[10, 10]}>
        <Col xs={24} md={24}>
          <FormInput form={form} handleChange={handleChange} {...characterFieldProps} />
        </Col>
      </Row>
    </div>
  );
};

export default ExtraFields;
