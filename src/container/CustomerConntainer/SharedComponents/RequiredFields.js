/* eslint-disable react/prop-types */
import React from 'react';
import { Row, Col } from 'antd';
import { FormInput, Avatar } from '../../../components';

const nameFieldProps = {
  label: 'Tên',
  name: 'name',
};
const phoneNumberFieldProps = {
  label: 'SĐT',
  name: 'phone_number',
};
const phoneNumber2FieldProps = {
  label: 'SĐT 2',
  name: 'phone_number2',
};
const phoneNumber3FieldProps = {
  label: 'SĐT 3',
  name: 'phone_number3',
};
const phoneNumber4FieldProps = {
  label: 'SĐT 4',
  name: 'phone_number4',
};

const faxNumberFieldProps = {
  label: 'Số fax',
  name: 'fax',
  inputProps: {
    width: 200,
  },
};

const RequiredFields = ({ form, handleChange }) => {
  const { image } = form.data;

  return (
    <Row gutter={[10, 10]}>
      <Col xs={24} md={8}>
        <Avatar
          containerStyle={{ width: 170 }}
          onImageAdded={newImage => handleChange('image', newImage)}
          image={image}
          onImageRemoved={() => handleChange('image', null)}
        />
      </Col>
      <Col xs={24} md={16}>
        <FormInput form={form} handleChange={handleChange} {...nameFieldProps} />
        <Row gutter={[10, 10]}>
          <Col xs={24} md={8}>
            <FormInput form={form} handleChange={handleChange} {...phoneNumberFieldProps} />
          </Col>
          <Col xs={24} md={8}>
            <FormInput form={form} handleChange={handleChange} {...phoneNumber2FieldProps} />
          </Col>
          <Col xs={24} md={8}>
            <FormInput form={form} handleChange={handleChange} {...phoneNumber3FieldProps} />
          </Col>
        </Row>
        <Row gutter={[10, 10]}>
          <Col xs={24} md={12}>
            <FormInput form={form} handleChange={handleChange} {...phoneNumber4FieldProps} />
          </Col>
          <Col xs={24} md={12}>
            <FormInput form={form} handleChange={handleChange} {...faxNumberFieldProps} />
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default RequiredFields;
