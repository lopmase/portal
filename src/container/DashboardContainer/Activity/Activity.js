import React from 'react';
import PropTypes from 'prop-types';
import './Activity.scss';

import avatar from '../../../assets/images/avatar.jpg';
import { Flexbox } from '../../../components';

const Activity = ({ containerStyle, user, type }) => {
  return (
    <div className="activity" style={{ ...containerStyle }}>
      <Flexbox row flexStart alignItems="flex-start">
        <img src={avatar} alt="avatar" />
        <div className="des">
          <div>
            <span className="user">{user.name || 'Phuong Lam'}</span>
            {' vừa tạo một '}
            <span className="item">{type === 1 ? 'Bất Động Sản' : 'Nhu Cầu'}</span>
            {' mới'}
          </div>
        </div>
      </Flexbox>
    </div>
  );
};

Activity.propTypes = {
  containerStyle: PropTypes.object,
  // onClick: PropTypes.func,
  user: PropTypes.object,
  type: PropTypes.number,
};

Activity.defaultProps = {
  containerStyle: {},
  // onClick: () => {},
  user: {},
  type: 1,
};

export default Activity;
