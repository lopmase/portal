/* eslint-disable react/prop-types */
import React from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';

import './AllCommunity.scss';
import Community from '../Community/Community';

const AllCommunity = () => {
  return (
    <div className="community-page">
      <h1>Hoạt động mới</h1>
      <Community enableLoadMore />
    </div>
  );
};

export default compose(withRouter)(AllCommunity);
