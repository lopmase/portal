import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Comment, Avatar, Card } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faEllipsisH, faHistory } from '@fortawesome/free-solid-svg-icons';
import moment from 'moment';
import { formatImageUrl } from '../../../helpers/common';
import * as actions from './action';
import { isAdmin } from '../../../helpers/roleUtil';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { toggleCommentStatus } from '../../../apis/userApi';
import { ActionMenu } from '../../../components';
import './Comment.scss';
import { apiSerializer } from '../../../helpers/apiSerializer';
import avatarImage from '../../../assets/images/avatar.jpg';

const CommentComponent = ({ comment, user, onChange }) => {
  const [loading, setLoading] = useState(false);
  const avatar = comment.user.avatar ? formatImageUrl(comment.user.avatar.thumbnail) : avatarImage;
  const actionItems = [
    {
      text: comment.status === 0 ? 'Ẩn' : 'Hiện lại',
      value: 'del',
      icon: comment.status === 0 ? faTrashAlt : faHistory,
    },
  ];

  const hide = async () => {
    try {
      setLoading(true);
      const rs = await toggleCommentStatus(comment.id);
      apiSerializer(rs);
      setLoading(false);
      onChange();
    } catch (error) {
      setLoading(false);
      showMessage(error);
    }
  };

  const commentContent = (
    <div>
      <div>{comment.content}</div>
      {comment.image && (
        <div className="image-box">
          <img src={formatImageUrl(comment.image.main)} alt="comment" />
        </div>
      )}
    </div>
  );

  return (
    <Card
      className={`comment-box ${comment.status === 1 ? 'hidden' : ''}`}
      bordered={false}
      bodyStyle={{ padding: 0 }}
      loading={loading}
    >
      <Comment
        key={comment.id}
        author={comment.user.name}
        avatar={<Avatar src={avatar} alt="Ảnh đại diện" />}
        // datetime={moment(comment.created_at * 1000).fromNow()}
        datetime={moment(comment.created_at * 1000).format('HH:mm:ss DD/MM/YYYY')}
        content={commentContent}
      />

      {(isAdmin(user) || comment.user_id === user.id) && (
        <div className="action-box">
          <ActionMenu
            render={() => <FontAwesomeIcon size={12} icon={faEllipsisH} />}
            onSelect={() => hide()}
            items={actionItems}
          />
        </div>
      )}
      {comment.status === 1 && <div className="hidden-label">Đã ẩn</div>}
    </Card>
  );
};

export function mapStateToProps(state) {
  return {
    user: state.auth.user.data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
    },
    dispatch,
  );
}

CommentComponent.propTypes = {
  comment: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
};

CommentComponent.defaultProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(CommentComponent);
