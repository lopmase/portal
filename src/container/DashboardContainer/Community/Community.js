/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'connected-react-router';

import * as actions from './action';
import Feed from './Feed';
import './Community.scss';
import { REDUCERS } from '../../../constants/COMMON';
import { ROUTES } from '../../../constants/ROUTES';
import DataList from '../../Common/DataList/DataList';

const Community = props => {
  const { doPush, getAllComments, loadMore, page, total_page: totalPage, enableLoadMore } = props;
  const tableHeight = window.innerHeight - (window.innerWidth < 500 ? 150 : 110);

  useEffect(() => {
    getAllComments();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="community">
      <DataList
        selector={state => state.dashboard[REDUCERS.COMMENTS]}
        loadMore={enableLoadMore}
        height={enableLoadMore ? tableHeight : 'auto'}
        hasMore={page < totalPage}
        onLoadMore={loadMore}
        renderItems={house => (
          <Feed
            key={house.id}
            onHouseClick={() => doPush(`${ROUTES.HOUSE_ROOT}/${house.id}`)}
            house={house || {}}
          />
        )}
      />
    </div>
  );
};

export function mapStateToProps(state) {
  return {
    user: state.auth.user.data,
    page: state.dashboard.COMMENTS.page,
    total_page: state.dashboard.COMMENTS.total_pages,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      doPush: push,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Community);
