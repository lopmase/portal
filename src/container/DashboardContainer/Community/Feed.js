/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { useState, useEffect } from 'react';
import moment from 'moment';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Tooltip, Icon, Avatar, Comment, Card, notification, Spin } from 'antd';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { push } from 'connected-react-router';

import './Feed.scss';
import { InfoButton, FeedImages, CommentEditor } from './components';
import * as actions from './action';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { getCommentByHouseId } from '../../../apis/houseApi';
import CommentComponent from './Comment';
import avatarImage from '../../../assets/images/avatar.jpg';
import { formatImageUrl } from '../../../helpers/common';
import HtmlDescription from '../../Common/HtmlDescription/HtmlDescription';
import ShareButton from '../../Common/ShareButton/ShareButton';

const Feed = props => {
  const { house, onHouseClick, updatePage, likeHouse } = props;
  const {
    comments_count: commentCount,
    likes_count: likeCount,
    id,
    description,
    title,
    house_number: houseNumber,
    house_address: houseAddress,
    thumbnails: images,
    mains: mainImages,
  } = house;

  const avatar =
    house.user && house.user.avatar ? formatImageUrl(house.user.avatar.thumbnail) : avatarImage;
  const name = house.user ? house.user.name : '';
  const time = house.updated_at * 1000;
  const [showComments, setShowComments] = useState(false);
  const [comments, setComments] = useState([]);
  const [loading, setLoading] = useState(false);

  const comment = () => setShowComments(!showComments);

  const fetchComments = async () => {
    try {
      setLoading(true);
      const result = await getCommentByHouseId(id);
      const rs = apiSerializer(result);
      setComments(rs);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      const message = error.message || UNKNOWN_ERROR;
      notification.error({ message });
    }
  };

  useEffect(() => {
    if (showComments) {
      fetchComments();
    }
    return () => {};
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [showComments, commentCount]);

  const commentActions = [
    <span key="like-count">
      <Tooltip className="like-button" title={`${likeCount} Likes`} onClick={() => likeHouse(id)}>
        <Icon type="like" />
        <span style={{ paddingLeft: 5 }}>Thích</span>
      </Tooltip>
      <span style={{ paddingLeft: 8, cursor: 'auto', fontSize: 16, fontWeight: 'bold' }}>
        {likeCount}
      </span>
    </span>,
    <span key="comment-count">
      <Tooltip className="action-button" title={`${commentCount} Bình luận`} onClick={comment}>
        <Icon type="message" />
        <span style={{ paddingLeft: 5 }}>Bình luận</span>
      </Tooltip>
      <span style={{ paddingLeft: 8, cursor: 'auto', fontSize: 16, fontWeight: 'bold' }}>
        {commentCount}
      </span>
    </span>,
    <ShareButton house={house} marginLeft={20} fontsize={16} />,
  ];

  const avatarBox = <Avatar src={avatar} alt="Han Solo" />;

  const timer = (
    <Tooltip title={moment(time).format('YYYY-MM-DD HH:mm:ss')}>
      <span>{moment(time).fromNow()}</span>
    </Tooltip>
  );

  const top = (
    <div>
      <div>{name}</div>
      <InfoButton house={house} />
    </div>
  );

  const content = (
    <div>
      <p className="title" onClick={onHouseClick}>
        {title}
      </p>
      <p className="address">{`${houseNumber} ${houseAddress}`}</p>
      <HtmlDescription description={description} />
      <FeedImages thumbnails={images} mains={mainImages} />
    </div>
  );

  const Editor = (
    <CommentEditor
      id={id}
      onCommentDone={() => {
        updatePage(1);
        fetchComments();
      }}
    />
  );

  return (
    <Card
      bordered={false}
      className="comment"
      bodyStyle={{ padding: 10, paddingTop: 0, paddingBottom: 0 }}
      style={{ marginBottom: 10, borderRadius: 10 }}
    >
      <Comment
        actions={commentActions}
        author={top}
        avatar={avatarBox}
        content={content}
        datetime={timer}
      >
        {showComments && (
          <div>
            {Editor}
            {loading && <Spin className="centerSpin" />}
            {comments.map(c => (
              <CommentComponent onChange={fetchComments} key={c.id} comment={c} />
            ))}
          </div>
        )}
      </Comment>
    </Card>
  );
};

Feed.propTypes = {
  house: PropTypes.func.isRequired,
  onHouseClick: PropTypes.func.isRequired,
  updatePage: PropTypes.func.isRequired,
  likeHouse: PropTypes.func.isRequired,
};

Feed.defaultProps = {};

export function mapStateToProps() {
  return {};
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      doPush: push,
    },
    dispatch,
  );
}

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Feed);
