/* eslint-disable no-param-reassign */
import _ from 'lodash';
import { formatImageUrl } from '../../../helpers/common';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateModalActions } from '../../../helpers/modalAction';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { generateDatalistActions } from '../../../helpers/dataListAction';
import { uploadImages } from '../../HouseContainer/UploadJobs/action';

const { openModal, closeModal } = generateModalActions(REDUCERS.GLOBAL_LOADING);
export const { fetchDone, fetching, fetchError, nextPage, updatePage } = generateDatalistActions(
  REDUCERS.COMMENTS,
);

export const getAllComments = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { page, list, pageSize } = getState().dashboard[REDUCERS.COMMENTS];
    dispatch(fetching(true));
    const rs = await thunkDependencies.house.comments({ page, size: pageSize });
    const result = apiSerializer(rs);
    const data = result.data.map(house => {
      const { image } = house;
      const { internal, public: publicImages } = image || {};
      const thumbnails = _.values(internal)
        .map(p => formatImageUrl(p.thumbnail || p.main))
        .concat(_.values(publicImages).map(p => formatImageUrl(p.thumbnail || p.main)));
      const images = _.values(internal)
        .map(p => formatImageUrl(p.main))
        .concat(_.values(publicImages).map(p => formatImageUrl(p.main)));
      house.thumbnails = thumbnails;
      house.mains = images;
      return house;
    });
    if (page === 1) {
      dispatch(fetchDone(data, result.total, Math.ceil(result.total / pageSize)));
    } else {
      dispatch(fetchDone(list.concat(data), result.total, Math.ceil(result.total / pageSize)));
    }
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};

export const loadMore = () => async (dispatch, getState) => {
  const { total, list, fetching: isFetchinng } = getState().dashboard[REDUCERS.COMMENTS];
  if (isFetchinng) {
    return;
  }
  if (list.length >= total) {
    return;
  }
  dispatch(nextPage());
  dispatch(getAllComments());
};

export const addComment = (id, content, image) => async (dispatch, getState, thunkDependencies) => {
  try {
    let imageIds = [];
    dispatch(openModal());
    if (image) {
      await dispatch(uploadImages([{ file: image }]));
      const internalJobs = getState().house[REDUCERS.UPLOAD_JOBS].jobs;
      imageIds = _.values(internalJobs.results)
        .filter(r => r.id)
        .map(r => r.id);
    }
    const [imageId] = imageIds;
    const result = await thunkDependencies.user.comment(id, content, imageId);
    apiSerializer(result);
    dispatch(closeModal());
  } catch (error) {
    dispatch(closeModal());
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const likeHouse = id => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(openModal());
    const result = await thunkDependencies.user.likeHouse(id);
    apiSerializer(result);
    dispatch(getAllComments());
    dispatch(closeModal());
  } catch (error) {
    dispatch(closeModal());
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};
