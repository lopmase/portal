/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable jsx-a11y/label-has-for */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Input, Icon } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { bindActionCreators } from 'redux';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { addComment } from '../action';
import './CommentEditor.scss';

const CommentEditor = ({ id, onCommentDone, handleAddComment, knowledge, addKnowledgeComment }) => {
  const [newComment, setNewComment] = useState('');
  const [image, setImage] = useState(null);
  const [localImage, setLocalImage] = useState(null);
  const [loading, setLoading] = useState(false);
  console.log('knowledge', knowledge);
  const add = async () => {
    if (knowledge) {
      await addKnowledgeComment(id, newComment);
    } else {
      await handleAddComment(id, newComment, image);
    }
    onCommentDone();
  };
  const readImage = file => {
    if (!file) return;

    setLoading(true);
    if (file.size > 5000000) {
      return;
    }

    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      setLoading(false);

      setLocalImage(reader.result);
    };
    reader.onerror = () => {
      setLoading(false);
    };
  };

  const onImageChange = event => {
    const file = event.target.files.item(0);
    setImage(file);
    readImage(file);
  };

  return (
    <div key={`comment-add-${id}`} style={{ marginBottom: 0 }} className="comment-editor">
      <Input.Search
        placeholder="Nhập nội dung bình luận"
        enterButton="Gửi"
        size="large"
        suffix={
          <label htmlFor={`uploader${id}`}>
            <Icon type="file-image" onClick={() => {}} />
          </label>
        }
        value={newComment}
        onChange={event => setNewComment(event.target.value)}
        onSearch={() => {
          add();
          setNewComment('');
          setImage(null);
          setLocalImage(null);
        }}
      />
      <div className="upload-area">
        {localImage && (
          <div className="photo">
            <img alt="p" src={localImage} />
            <div
              className="delete"
              onClick={event => {
                event.preventDefault();
                setImage(null);
                setLocalImage(null);
              }}
            >
              <FontAwesomeIcon icon={faTrashAlt} color="white" />
            </div>
          </div>
        )}
        <input
          type="file"
          accept="image/*"
          disabled={loading}
          id={`uploader${id}`}
          onChange={event => onImageChange(event)}
        />
      </div>
    </div>
  );
};

CommentEditor.propTypes = {
  id: PropTypes.string,
  handleAddComment: PropTypes.func.isRequired,
  onCommentDone: PropTypes.func.isRequired,
};

CommentEditor.defaultProps = {
  id: 'new',
};

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      handleAddComment: addComment,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(null, mapDispatchToProps))(CommentEditor);
