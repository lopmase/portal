import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'antd';
import ImageLightbox from '../../../../components/LightBox/Lightbox';

const FeedImages = ({ thumbnails, mains }) => {
  const [lightboxVisible, setLightboxVisible] = useState(false);
  const featureImages = thumbnails.slice(0, 4);

  return (
    <>
      <Row gutter={[10, 10]}>
        {featureImages.map((image, index) => (
          <Col
            xs={12}
            className={index === 3 ? 'feature-images' : ''}
            onClick={() => setLightboxVisible(true)}
          >
            <img className="image" src={image} alt="Ảnh 1" />
            {index === 3 && (
              <div className="see-more-image">
                <div className="see-more-text">{`Xem thêm ${thumbnails.length - 3} hình`}</div>
              </div>
            )}
          </Col>
        ))}
      </Row>
      <ImageLightbox
        isOpen={lightboxVisible}
        onClose={() => setLightboxVisible(false)}
        images={mains}
      />
    </>
  );
};

FeedImages.propTypes = {
  thumbnails: PropTypes.array.isRequired,
  mains: PropTypes.array.isRequired,
};

FeedImages.defaultProps = {};

export default FeedImages;
