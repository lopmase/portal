import React from 'react';
import PropTypes from 'prop-types';
import { Popover } from 'antd';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { REDUCERS } from '../../../../constants/COMMON';
import { FIELDS } from '../../../../constants/HOUSE_FIELDS';
import { Flexbox } from '../../../../components';

const notDetail = [
  'number_wc',
  'number_bedroom',
  'area',
  'floors',
  'wide_street',
  'house_number',
  'house_address',
  'district',
  'public',
  'web',
  'into_money',
  'id',
  'customer_id',
  'user_id',
  'province',
  'title',
  'description',
];

const FeedInfoButton = ({ options, house }) => {
  const content = (
    <div className="detail">
      {FIELDS.filter(f => !notDetail.includes(f.property)).map(field => (
        <Flexbox row spaceBetween key={field.property}>
          <div className="label">{field.label}</div>
          <span>{field.format ? field.format(house, options) : house[field.property]}</span>
        </Flexbox>
      ))}
    </div>
  );

  return (
    <Popover content={content} title="Chi tiết" trigger="click">
      <FontAwesomeIcon className="top-info-button" icon={faInfoCircle} color="gray" />
    </Popover>
  );
};

FeedInfoButton.propTypes = {
  options: PropTypes.any.isRequired,
  house: PropTypes.any.isRequired,
};

FeedInfoButton.defaultProps = {};

export function mapStateToProps(state) {
  return {
    options: state.metadata[REDUCERS.OPTIONS].data,
  };
}

export default connect(mapStateToProps, null)(FeedInfoButton);
