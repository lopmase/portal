import React, { Component } from 'react';
import { Switch } from 'react-router-dom';
import { PrivateRoute } from '../../components';
import { ROUTES } from '../../constants/ROUTES';
import Dashboard from './Dashboard/Dashboard';
import Notification from './Notification/index';
import AllCommunity from './AllCommunity/AllCommunity';

class DashboardContainer extends Component {
  state = {};

  render() {
    return (
      <div className="dashboard-container">
        <Switch>
          <PrivateRoute exact path={ROUTES.DASHBOARD} component={Dashboard} />
          <PrivateRoute path={ROUTES.COMMUNITY} component={AllCommunity} />
          <PrivateRoute path={ROUTES.NOTIFICATIONS} component={Notification} />
        </Switch>
      </div>
    );
  }
}

export default DashboardContainer;
