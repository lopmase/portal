/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { compose } from 'recompose';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col, Table } from 'antd';
import classNames from 'classnames';
import moment from 'moment';
import { push } from 'connected-react-router';

import { Card, Flexbox, Button, Reponsive, Label } from '../../../components';
import * as actions from './action';
import './Dashboard.scss';
import { REDUCERS, NOTIFICATION_TYPES } from '../../../constants/COMMON';
import { ROUTES } from '../../../constants/ROUTES';
import Notification from '../../ShellContainner/Header/Notification';
import avatar from '../../../assets/images/avatar.jpg';
import { generateModalActions } from '../../../helpers/modalAction';
import { formatImageUrl } from '../../../helpers/common';
import DataList from '../../Common/DataList/DataList';
import Image from '../../../components/Image/Image';
import Community from '../Community/Community';
import { columns } from '../../HouseContainer/Houses/columns';
import OverallSummary from './OverallSummary';
import HouseChartByTime from './HouseChartByTime';

const { openModal } = generateModalActions(REDUCERS.USER_PROFILE_MODAL);

const columnsList = columns
  .filter(col => ['id', 'house_number', 'total_view'].indexOf(col.dataIndex) >= 0)
  .map(col => ({ ...col, filterDropdown: null, sorter: null, fixed: null }));

class Dashboard extends Component {
  static propTypes = {};

  static defaultProps = {};

  state = {};

  componentDidMount() {
    const { fetch, fetchTopView } = this.props;
    fetch();
    fetchTopView();
  }

  renderCell = ({ item, column }) => {
    const cell = item[column.key];
    let value = '';
    if (column.accessor) {
      value = column.accessor(item, {});
    } else {
      value = cell ? cell.toString() : '';
    }
    return <td className={column.key === 'id' ? 'bold-text' : ''}>{value}</td>;
  };

  render() {
    const { dataSource, overall, user, openProfile, doPush } = this.props;
    const { list, fetching } = dataSource;
    return (
      <div className="dashboard-page">
        <Flexbox row spaceBetween alignItems="center" containerStyle={{ marginBottom: 20 }}>
          <h1>Trang chủ</h1>
          <Flexbox row>
            {user && user.create_product_permission === 1 && (
              <Link to={ROUTES.NEW_HOUSE}>
                <Button clear text="+ Tạo BĐS" />
              </Link>
            )}
            <Notification />
            <Reponsive.Mobile>
              <div className="user-avt" onClick={openProfile}>
                <img
                  src={user && user.avatar ? formatImageUrl(user.avatar.thumbnail) : avatar}
                  alt=""
                />
              </div>
            </Reponsive.Mobile>
          </Flexbox>
        </Flexbox>

        <Row gutter={[20, 20]}>
          <Col md={15}>
            <Flexbox row alignItems="center" spaceBetween>
              <h2 style={{ marginBottom: 0 }}>Hoạt động mới cập nhật</h2>
              <Link to={ROUTES.COMMUNITY}>
                <Button clear text="Xem tất cả" />
              </Link>
            </Flexbox>
            <Community />
          </Col>
          <Col md={9}>
            <h2>Tổng Quan</h2>
            <OverallSummary overall={overall} />
            <h2>Bất động sản theo thời gian</h2>
            <Card shadow>
              <HouseChartByTime
                data={overall ? overall.chart : []}
                keys={['total']}
                timeKey="date"
              />
            </Card>

            <Reponsive.Default>
              <Flexbox row alignItems="center" spaceBetween>
                <h2 style={{ marginBottom: 0 }}>Thông báo</h2>
                <Link to={ROUTES.NOTIFICATIONS}>
                  <Button clear text="Xem tất cả" />
                </Link>
              </Flexbox>
              <DataList
                selector={state => state.notification}
                size={6}
                renderItems={noti => (
                  <Card
                    hoverable
                    shadow
                    onClick={() => doPush(`${ROUTES.HOUSE_ROOT}/${noti.house.id}`)}
                    containerStyle={{ marginBottom: 10 }}
                  >
                    <div className={classNames('notiItem', { read: noti.status })}>
                      <Flexbox row spaceBetween alignItems="flex-start">
                        <Flexbox row>
                          <div>
                            <Image
                              alt="useravt"
                              src={
                                noti.user && noti.user.avatar
                                  ? formatImageUrl(noti.user.avatar.thumbnail)
                                  : avatar
                              }
                              fallbackSrc={avatar}
                              className="avatar"
                            />
                          </div>
                          <p>
                            <span className="user">{noti.user ? noti.user.name : ''}</span>
                            {NOTIFICATION_TYPES[noti.type]}
                          </p>
                        </Flexbox>
                        <div className="time">{moment(noti.created_at * 1000).fromNow()}</div>
                      </Flexbox>
                      {noti.status === 0 && (
                        <div className="dot">
                          <Label dot background="#ff6c00" />
                        </div>
                      )}
                    </div>
                  </Card>
                )}
              />
            </Reponsive.Default>
            <Row gutter={[20, 20]}>
              <Col md={24}>
                <h2>BĐS Nổi bật</h2>
                <Table
                  columns={columnsList}
                  style={{
                    background: 'white',
                    boxShadow: '0px 0px 10px 2px rgba(165, 165, 165, 0.3)',
                    borderRadius: 10,
                  }}
                  bodyStyle={{ margin: 0 }}
                  className="tg-table-custom"
                  useFixedHeader
                  size="small"
                  rowKey={record => record.id}
                  dataSource={list}
                  pagination={false}
                  loading={fetching}
                  onRow={r => ({
                    // eslint-disable-next-line no-return-assign
                    onClick: () => (window.location = `${ROUTES.HOUSE_ROOT}/${r.id}`),
                  })}
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    dataSource: state.dashboard[REDUCERS.TOP_VIEWED_HOUSES],
    overall: state.dashboard[REDUCERS.DASHBOARD].data,
    user: state.auth.user.data,
    state,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      openProfile: openModal,
      doPush: push,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Dashboard);
