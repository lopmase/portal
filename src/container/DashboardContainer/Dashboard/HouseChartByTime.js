/* eslint-disable prefer-destructuring */
/* eslint-disable react/destructuring-assignment */
import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';
import PropTypes from 'prop-types';
import { Radio } from 'antd';
import { LineChart } from '../../../components';

const GROUP_NAME = {
  DAY: { key: 'day', text: 'Ngày' },
  WEEK: { key: 'week', text: 'Tuần' },
  MONTH: { key: 'month', text: 'Tháng' },
};

const DEFAULT_SELECTED_GROUP = GROUP_NAME.MONTH.key;

class HouseChartByTime extends Component {
  constructor(props) {
    super(props);

    this.switchGroupDataEvent = this.switchGroupDataEvent.bind(this);
    const initState = {};
    _.forOwn(GROUP_NAME, object => {
      initState[object.key] = {
        id: object.key,
        data: null,
      };
    });
    this.state = initState;
  }
  componentDidMount() {
    if (!_.isEmpty(this.props)) {
      this.initData();
    }
  }

  componentDidUpdate(prevProps) {
    if (!_.isEqual(this.props, prevProps)) {
      this.initData();
    }
  }

  // eslint-disable-next-line class-methods-use-this
  getCategoryName(groupName, date) {
    const parsedDate = moment(date);
    const yearVal = parsedDate.year();
    if (groupName === GROUP_NAME.MONTH.key) {
      return `${GROUP_NAME.MONTH.text} ${parsedDate.month()}-${yearVal}`;
    }
    return `${GROUP_NAME.WEEK.text} ${parsedDate.week()}-${yearVal}`;
  }

  isGroupDataLoaded(groupName) {
    return (
      this.state[groupName] && this.state[groupName].data && this.state[groupName].data.length > 0
    );
  }

  isSelectedGroup(groupName) {
    return this.state[groupName].id === this.state.selectedGroup;
  }

  groupBy(groupName) {
    const isDataLoaded = this.isGroupDataLoaded(groupName);
    if (this.isSelectedGroup(groupName) && isDataLoaded) {
      return;
    }
    if (!isDataLoaded) {
      const { data } = this.props;
      // create new object as a template
      const trackingList = [];
      const groupData = [];
      _.forEach(data, groupByDay => {
        const categoryName = this.getCategoryName(groupName, groupByDay.date);
        const indexOfCategory = trackingList.indexOf(categoryName);
        // check if the object exists or not
        if (indexOfCategory < 0) {
          // if the object doesn't exist, it need to create new one
          const DateObject = { date: categoryName, total: groupByDay.total };
          trackingList.push(categoryName);
          groupData.push(DateObject);
        } else {
          // the object exists already, it will count in new total
          const prevCountTotal = groupData[indexOfCategory].total;
          const newCountTotal = groupByDay.total;
          groupData[indexOfCategory].total = prevCountTotal + newCountTotal;
        }
      });

      this.updateSelectedGroupToState(groupName, groupData);
    } else {
      this.updateSelectedGroupToState(groupName);
    }
  }

  groupDataByMonth() {
    this.groupBy(GROUP_NAME.MONTH.key);
  }

  groupDataByWeek() {
    this.groupBy(GROUP_NAME.WEEK.key);
  }

  groupDataByDay() {
    const groupName = GROUP_NAME.DAY.key;
    const isDataLoaded = this.isGroupDataLoaded(groupName);
    if (this.isSelectedGroup(groupName) && isDataLoaded) {
      return;
    }
    if (!isDataLoaded) {
      const { data } = this.props;

      this.updateSelectedGroupToState(groupName, data);
    } else {
      this.updateSelectedGroupToState(groupName);
    }
  }

  updateSelectedGroupToState(groupName, data) {
    if (data) {
      this.setState(prevState => {
        const group = {};
        group[groupName] = { ...prevState[groupName], data };
        return { ...group, selectedGroup: prevState[groupName].id };
      });
    } else {
      this.setState(prevState => {
        return {
          selectedGroup: prevState[groupName].id,
        };
      });
    }
  }

  switchGroupData(groupName) {
    switch (groupName) {
      case GROUP_NAME.DAY.key:
        this.groupDataByDay();
        break;
      case GROUP_NAME.WEEK.key:
        this.groupDataByWeek();
        break;
      case GROUP_NAME.MONTH.key:
        this.groupDataByMonth();
        break;
      default:
        this.groupDataByMonth();
    }
  }

  switchGroupDataEvent(event) {
    this.switchGroupData(event.target.value);
  }

  initData() {
    const { defaultSelectedGroup } = this.props;
    this.switchGroupData(defaultSelectedGroup);
  }

  render() {
    const { timeKey, keys, defaultSelectedGroup } = this.props;
    const data =
      this.state.selectedGroup && this.state[this.state.selectedGroup]
        ? this.state[this.state.selectedGroup].data
        : [];

    if (data) {
      return (
        <div>
          <div align="right">
            <Radio.Group
              defaultValue={defaultSelectedGroup}
              size="small"
              buttonStyle="solid"
              onChange={this.switchGroupDataEvent}
            >
              <Radio.Button value={GROUP_NAME.DAY.key}>{GROUP_NAME.DAY.text}</Radio.Button>
              <Radio.Button value={GROUP_NAME.WEEK.key}>{GROUP_NAME.WEEK.text}</Radio.Button>
              <Radio.Button value={GROUP_NAME.MONTH.key}>{GROUP_NAME.MONTH.text}</Radio.Button>
            </Radio.Group>
          </div>
          <LineChart data={data} keys={keys} timeKey={timeKey} />
        </div>
      );
    }
    return 'Loading!';
  }
}

HouseChartByTime.propTypes = {
  timeKey: PropTypes.string,
  keys: PropTypes.array,
  data: PropTypes.arrayOf({}).isRequired,
  defaultSelectedGroup: PropTypes.string,
};

HouseChartByTime.defaultProps = {
  timeKey: 'date',
  keys: ['Total'],
  defaultSelectedGroup: DEFAULT_SELECTED_GROUP,
};

export default HouseChartByTime;
