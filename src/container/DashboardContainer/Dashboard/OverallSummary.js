import React from 'react';
import { Row, Col } from 'antd';
import PropTypes from 'prop-types';
import { Card } from '../../../components';

const cardStyle = {
  flex: 1,
  padding: 20,
  minWidth: 120,
};

const OverallSummary = ({ overall }) => {
  return (
    <Row gutter={[20, 20]} style={{ marginBottom: 20 }}>
      <Col xs={12}>
        <Card shadow containerStyle={cardStyle}>
          <div className="title">BĐS</div>
          <div className="number">{overall ? overall.houses : 0}</div>
          <div className="new">200 đã chia sẻ</div>
        </Card>
      </Col>
      <Col xs={12}>
        <Card shadow containerStyle={cardStyle}>
          <div className="title">Nhu cầu</div>
          <div className="number">{overall ? overall.requests : 0}</div>
          <div className="new">4 đã chia sẻ</div>
        </Card>
      </Col>
      <Col xs={12}>
        <Card shadow containerStyle={cardStyle}>
          <div className="title">Người dùng</div>
          <div className="number">{overall ? overall.user : 0}</div>
          <div className="new">20 nhân viên</div>
        </Card>
      </Col>
      <Col xs={12}>
        <Card shadow containerStyle={cardStyle}>
          <div className="title">Khách hàng</div>
          <div className="number">{overall ? overall.customer : 0}</div>
          <div className="new">3 khách hàng mới</div>
        </Card>
      </Col>
    </Row>
  );
};

OverallSummary.propTypes = {
  overall: PropTypes.object.isRequired,
};

OverallSummary.defaultProps = {};

export default OverallSummary;
