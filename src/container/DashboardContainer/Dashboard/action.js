import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateDataActions } from '../../../helpers/dataAction';
import { generateDatalistActions } from '../../../helpers/dataListAction';

const { fetchDone, fetching, fetchError } = generateDataActions(REDUCERS.DASHBOARD);
const {
  fetchDone: fetchListDone,
  fetching: fetchingList,
  fetchError: fetchListError,
} = generateDatalistActions(REDUCERS.TOP_VIEWED_HOUSES);

export const fetch = () => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const result = await thunkDependencies.dashboard.overall();
    dispatch(fetchDone(result));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};

export const fetchTopView = () => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetchingList());
    const result = await thunkDependencies.dashboard.topViewedHouses(10);
    dispatch(fetchListDone(result));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchListError(message));
  }
};
