import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateDatalistActions } from '../../../helpers/dataListAction';

const { fetchDone, fetching, fetchError, nextPage } = generateDatalistActions(
  REDUCERS.NOTIFICATION,
);

export const fetch = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { notification } = getState();
    const { page, list } = notification;
    const newPageSize = 20;
    dispatch(fetching());
    const response = await thunkDependencies.user.notifications(newPageSize, page);
    const result = apiSerializer(response);
    if (page === 1) {
      dispatch(fetchDone(result.data, result.total, Math.ceil(result.total / newPageSize)));
    } else {
      dispatch(
        fetchDone(list.concat(result.data), result.total, Math.ceil(result.total / newPageSize)),
      );
    }
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};

export const loadMore = () => async (dispatch, getState) => {
  const { notification } = getState();
  const { total, list, fetching: isFetchinng } = notification;
  if (isFetchinng) {
    return;
  }
  if (list.length >= total) {
    return;
  }
  dispatch(nextPage());
  dispatch(fetch());
};
