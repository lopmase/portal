import React from 'react';
import moment from 'moment';
import { formatImageUrl } from '../../../helpers/common';
import { NOTIFICATION_TYPES } from '../../../constants/COMMON';
import Image from '../../../components/Image/Image';
import avatar from '../../../assets/images/avt2.png';
// import classnames from 'classnames';
import { Flexbox } from '../../../components';
// import UserFilter from '../CustomersFilter/UserFilter';

export const columns = [
  {
    title: '#',
    dataIndex: 'id',
    sorter: true,
    width: 50,
  },
  {
    title: 'Ảnh',
    dataIndex: 'avatar',
    width: 70,
    render: (value, notification) => (
      <Flexbox row flexStart containerStyle={{ alignItems: 'flex-start' }}>
        <Image
          alt="useravt"
          src={
            notification.user && notification.user.avatar
              ? formatImageUrl(notification.user.avatar.thumbnail)
              : avatar
          }
          fallbackSrc={avatar}
          className="avatar"
        />
        <div className="bold-text user-item" style={{ marginLeft: 5 }}>
          <div className="">{notification.user.name}</div>
        </div>
      </Flexbox>
    ),
  },
  {
    title: 'Loại',
    dataIndex: 'type',
    sorter: true,
    width: 100,
    filterKey: 'type',
    render: (value, notification) => NOTIFICATION_TYPES[notification.type],
  },
  {
    title: 'Bất động sản',
    dataIndex: 'house_id',
    sorter: true,
    width: 200,
    // operation: 'like',
    filterKey: 'title',
    // filterDropdown: props => <UserFilter {...props} />,
    render: (value, notification) => (notification.house ? notification.house.title : ''),
  },
  {
    title: 'Ngày',
    dataIndex: 'created_at',
    width: 120,
    sorter: true,
    render: (value, notification) => moment(notification.created_at * 1000).fromNow(),
  },
];
