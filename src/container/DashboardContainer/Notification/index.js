/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable prefer-destructuring */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import { push } from 'connected-react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { List } from 'antd';
import InfiniteScroll from 'react-infinite-scroller';
import { Flexbox, Label } from '../../../components';
import { formatImageUrl } from '../../../helpers/common';
import { NOTIFICATION_TYPES } from '../../../constants/COMMON';
import { ROUTES } from '../../../constants/ROUTES';
import Image from '../../../components/Image/Image';
import avatar from '../../../assets/images/avt2.png';
import './Notification.scss';
import * as actions from './action';

class Notification extends Component {
  static propTypes = {};

  static defaultProps = {};

  state = {
    deviceMode: false,
  };

  componentDidMount() {
    const { fetch } = this.props;
    this.setState({
      deviceMode: window.innerWidth < 500,
      tableHeight: window.innerHeight - (window.innerWidth < 500 ? 190 : 150),
    });
    fetch();
  }

  navToHouse(houseId) {
    const { doPush } = this.props;
    doPush(`${ROUTES.HOUSE_ROOT}/${houseId}`);
  }

  render() {
    const { dataSource, loadMore } = this.props;
    const { list, fetching, total } = dataSource;
    const { deviceMode, tableHeight } = this.state;
    const loadedRows = `${list.length}/${total}`;
    return (
      <div className="notification-page" style={{ padding: deviceMode ? 10 : 20 }}>
        <div className="toolbar">
          <Flexbox row spaceBetween containerStyle={{ marginBottom: 20, alignItems: 'center' }}>
            <Flexbox row flexStart alignItems="center">
              <h1 style={{ marginRight: 10, marginBottom: 0 }}>Thông báo</h1>
              <Label text={loadedRows} />
            </Flexbox>
          </Flexbox>
        </div>
        <div className="infinite-container" style={{ height: tableHeight }}>
          <InfiniteScroll
            initialLoad={false}
            pageStart={0}
            loadMore={loadMore}
            hasMore={!fetching}
            useWindow={false}
          >
            <List
              style={{
                padding: deviceMode ? 10 : 20,
                background: 'white',
              }}
              dataSource={list}
              renderItem={item => (
                <List.Item>
                  <List.Item.Meta
                    avatar={
                      <div
                        onClick={() => this.navToHouse(item.house.id)}
                        style={{ cursor: 'pointer' }}
                      >
                        <Image
                          alt="useravt"
                          src={
                            item.user && item.user.avatar
                              ? formatImageUrl(item.user.avatar.thumbnail)
                              : avatar
                          }
                          fallbackSrc={avatar}
                          className="avatar"
                        />
                      </div>
                    }
                    title={
                      <div
                        onClick={() => this.navToHouse(item.house.id)}
                        style={{ cursor: 'pointer' }}
                      >
                        <p>
                          <span className="user">{item.user ? item.user.name : ''}</span>
                          <span className="type">{NOTIFICATION_TYPES[item.type]}</span>
                        </p>
                      </div>
                    }
                    description={
                      <div
                        onClick={() => this.navToHouse(item.house.id)}
                        style={{ cursor: 'pointer' }}
                      >
                        <p>
                          <span className="title">{item.house.title}</span>
                        </p>
                      </div>
                    }
                  />
                  <div onClick={() => this.navToHouse(item.house.id)} style={{ cursor: 'pointer' }}>
                    <div className="time">{moment(item.created_at * 1000).fromNow()}</div>
                  </div>
                </List.Item>
              )}
            />
          </InfiniteScroll>
        </div>
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    dataSource: state.notification,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      doPush: push,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Notification);
