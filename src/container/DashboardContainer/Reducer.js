import { combineReducers } from 'redux';
import { REDUCERS } from '../../constants/COMMON';
import createDataReducer from '../../helpers/createDataReducer';
import createDataListReducer from '../../helpers/createDataListReducer';

export default combineReducers({
  [REDUCERS.DASHBOARD]: createDataReducer(REDUCERS.DASHBOARD),
  [REDUCERS.TOP_VIEWED_HOUSES]: createDataListReducer(REDUCERS.TOP_VIEWED_HOUSES),
  [REDUCERS.COMMENTS]: createDataListReducer(REDUCERS.COMMENTS),
});
