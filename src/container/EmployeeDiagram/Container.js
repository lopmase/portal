/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
import { Card, Tag, Tree } from 'antd';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { getUserListByManagerId } from '../../apis/userApi';
import Layout from '../ShellContainner/Layout/Layout';

const EmployeeDiagram = props => {
  const [user, setUser] = useState([]);
  const [superAdmin, setSuperAdmin] = useState([]);
  const [saleAdmin, setSaleAdmin] = useState([]);
  const [admin, setAdmin] = useState([]);
  const [seniorManager, setSeniorManager] = useState([]);
  const [manager, setManager] = useState([]);
  const [teamLeader, setTeamLeader] = useState([]);
  const [staff, setStaff] = useState([]);
  const [treeMap, setTreeMap] = useState();
  const [selectInfo, setSelectInfo] = useState([]);
  const { me } = props;
  const onSelect = selectedKeys => {
    const info = user.filter(val => val.id === Number(selectedKeys));
    setSelectInfo(info);
  };
  const fetchUser = async () => {
    const users = await getUserListByManagerId();
    setUser(Object.values(users.data));
  };
  const mappingData = users => {
    const superAdminLst = users.filter(user => user.job_position === 1);
    const saleAdminLst = users.filter(user => user.job_position === 3);
    const adminLst = users.filter(user => user.job_position === 2);
    const seniorManagerLst = users.filter(user => user.job_position === 4);
    const managerLst = users.filter(user => user.job_position === 5);
    const teamLeaderLst = users.filter(user => user.job_position === 6);
    const staffLst = users.filter(user => user.job_position === 7);
    setSuperAdmin(superAdminLst);
    setSaleAdmin(saleAdminLst);
    setAdmin(adminLst);
    setSeniorManager(seniorManagerLst);
    setManager(managerLst);
    setTeamLeader(teamLeaderLst);
    setStaff(staffLst);
  };
  useEffect(() => {
    fetchUser();
  }, []);
  useEffect(() => {
    user.length > 0 && mappingData(user);
  }, [user]);
  useEffect(() => {
    const tree =
      (superAdmin.length === 0 &&
        admin.length > 0 && [
          ...admin.map(ad => ({
            title: [ad.name, ' ', <Tag color="magenta">Admin</Tag>],
            key: ad.id,
            children: saleAdmin
              .filter(sa => sa.manager === ad.id)
              .map(sa => ({
                title: [sa.name, ' ', <Tag color="orange"> Sale Admin</Tag>],
                key: sa.id,
                children: seniorManager
                  .filter(sm => sm.manager === sa.id)
                  .map(sm => ({
                    title: [sm.name, ' ', <Tag color="lime"> Senior Manager</Tag>],
                    key: sm.id,
                    children: manager
                      .filter(m => m.manager === sm.id)
                      .map(m => ({
                        title: [m.name, ' ', <Tag color="blue">Manager</Tag>],
                        key: m.id,
                        children: teamLeader
                          .filter(tl => tl.manager === m.id)
                          .map(tl => ({
                            title: [tl.name, ' ', <Tag color="geekblue">Team Leader</Tag>],
                            key: tl.id,
                            children: staff
                              .filter(ts => ts.manager === tl.id)
                              .map(ts => ({
                                title: ts.name,
                                key: ts.id,
                              })),
                          })),
                      })),
                  })),
              })),
          })),
        ]) ||
      (superAdmin.length === 0 &&
        admin.length === 0 &&
        saleAdmin.length > 0 && [
          ...saleAdmin.map(sa => ({
            title: [sa.name, ' ', <Tag color="orange"> Sale Admin</Tag>],
            key: sa.id,
            children: seniorManager
              .filter(sm => sm.manager === sa.id)
              .map(sm => ({
                title: [sm.name, ' ', <Tag color="lime"> Senior Manager</Tag>],
                key: sm.id,
                children: manager
                  .filter(m => m.manager === sm.id)
                  .map(m => ({
                    title: [m.name, ' ', <Tag color="blue">Manager</Tag>],
                    key: m.id,
                    children: teamLeader
                      .filter(tl => tl.manager === m.id)
                      .map(tl => ({
                        title: [tl.name, ' ', <Tag color="geekblue">Team Leader</Tag>],
                        key: tl.id,
                        children: staff
                          .filter(ts => ts.manager === tl.id)
                          .map(ts => ({
                            title: ts.name,
                            key: ts.id,
                          })),
                      })),
                  })),
              })),
          })),
        ]) ||
      (superAdmin.length === 0 &&
        admin.length === 0 &&
        saleAdmin.length === 0 && [
          // sale admin
          ...seniorManager.map(sm => ({
            title: [sm.name, ' ', <Tag color="lime"> Senior Manager</Tag>],
            key: sm.id,
            children: manager
              .filter(m => m.manager === sm.id)
              .map(m => ({
                title: [m.name, ' ', <Tag color="blue">Manager</Tag>],
                key: m.id,
                children: teamLeader
                  .filter(tl => tl.manager === m.id)
                  .map(tl => ({
                    title: [tl.name, ' ', <Tag color="geekblue">Team Leader</Tag>],
                    key: tl.id,
                    children: staff
                      .filter(ts => ts.manager === tl.id)
                      .map(ts => ({
                        title: ts.name,
                        key: ts.id,
                      })),
                  })),
              })),
          })),
        ]) ||
      (superAdmin.length === 0 &&
        admin.length === 0 &&
        saleAdmin.length === 0 &&
        seniorManager.length === 0 && [
          ...manager.map(m => ({
            title: [m.name, ' ', <Tag color="blue">Manager</Tag>],
            key: m.id,
            children: teamLeader
              .filter(tl => tl.manager === m.id)
              .map(tl => ({
                title: [tl.name, ' ', <Tag color="geekblue">Team Leader</Tag>],
                key: tl.id,
                children: staff
                  .filter(ts => ts.manager === tl.id)
                  .map(ts => ({
                    title: ts.name,
                    key: ts.id,
                  })),
              })),
          })),
        ]) ||
      (superAdmin.length === 0 &&
        admin.length === 0 &&
        saleAdmin.length === 0 &&
        seniorManager.length === 0 &&
        manager.length === 0 && [
          ...teamLeader.map(tl => ({
            title: [tl.name, ' ', <Tag color="geekblue">Team Leader</Tag>],
            key: tl.id,
            children: staff
              .filter(ts => ts.manager === tl.id)
              .map(ts => ({
                title: [ts.name, ' ', <Tag color="black">Team Leader</Tag>],
                key: ts.id,
              })),
          })),
        ]) ||
      (superAdmin.length === 0 &&
        admin.length === 0 &&
        saleAdmin.length === 0 &&
        seniorManager.length === 0 &&
        manager.length === 0 &&
        teamLeader.length === 0 && [
          ...staff.map(ts => ({
            title: [ts.name, ' ', <Tag color="black">Team Leader</Tag>],
            key: ts.id,
          })),
        ]);
    setTreeMap(tree);
  }, [superAdmin, saleAdmin, admin, seniorManager, manager, teamLeader, staff, me !== null]);

  return (
    <>
      <Layout
        renderHeader={() => (
          <>
            <h1>Sơ đồ nhân sự</h1>
          </>
        )}
      />
      <Card>
        <div style={{ display: 'flex' }}>
          <div style={{ width: '45%', marginLeft: '5%' }}>
            <Tree showLine defaultExpandAll onSelect={onSelect} treeData={treeMap} />
          </div>
          <div style={{ width: '50%' }}>
            {selectInfo.length > 0 && (
              <div style={{ display: 'flex' }}>
                <img
                  width="300"
                  height="400"
                  src={selectInfo[0].avatar ? selectInfo[0].avatar.main : ''}
                  alt="avatar"
                />
                <div style={{ marginLeft: '1%' }}>
                  <p>Name: {selectInfo[0].name}</p>
                  <p>Phone: {selectInfo[0].phone_number}</p>
                </div>
              </div>
            )}
          </div>
        </div>
      </Card>
    </>
  );
};
export function mapStateToProps(state) {
  return {
    me: state.auth.user.data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(EmployeeDiagram);
