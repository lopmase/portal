/* eslint-disable array-callback-return */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { collection, onSnapshot } from 'firebase/firestore';
import { connect, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { db } from './firebaseConfig';
import { isAdmin } from '../../helpers/roleUtil';

const GET_MESSAGE = 'GET_MESSAGE';
const GET_STATUS = 'GET_STATUS';
const GET_PHONENUMBER = 'GET_PHONENUMBER';

const AppFireBase = ({ user }) => {
  const colStatus = collection(db, 'status');
  const colMessage = collection(db, 'messages');
  const colPhoneNumber = collection(db, 'phoneNumber');
  const [statusArray, setStatusArray] = useState([]);
  const [messageArray, setMessageArray] = useState([]);
  const [phoneNumberArray, setPhoneNumberArray] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    if ((user && isAdmin(user)) || (user && user.chat_permission === 1)) {
      onSnapshot(colStatus, snapshot => {
        const statusArr = [];
        snapshot.docs.map(doc => {
          statusArr.push({
            id: doc.id,
            ...doc.data(),
          });
        });
        setStatusArray(statusArr);
      });
      onSnapshot(colMessage, snapshot => {
        const messageArr = [];
        snapshot.docs.map(doc => {
          messageArr.push({
            id: doc.id,
            ...doc.data(),
          });
        });
        setMessageArray(messageArr);
      });
      onSnapshot(colPhoneNumber, snapshot => {
        const phoneNumberArr = [];
        snapshot.docs.map(doc => {
          phoneNumberArr.push({
            id: doc.id,
            ...doc.data(),
          });
        });
        setPhoneNumberArray(phoneNumberArr);
      });
    }
  }, [user]);
  useEffect(() => {
    dispatch({ type: GET_STATUS, payload: statusArray });
  }, [statusArray]);
  useEffect(() => {
    dispatch({ type: GET_MESSAGE, payload: messageArray });
  }, [messageArray]);
  useEffect(() => {
    dispatch({ type: GET_PHONENUMBER, payload: phoneNumberArray });
  }, [phoneNumberArray]);
  return <></>;
};

export function mapStateToProps(state) {
  return {
    user: state.auth.user.data,
    state,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(AppFireBase);
