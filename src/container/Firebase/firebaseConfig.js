import { getFirestore } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';
import { initializeApp } from 'firebase/app';

// const firebaseConfig = {
//   apiKey: 'AIzaSyCbtIjmlfLjBbWWTlAS61IChCIKnd9z3mc',
//   authDomain: 'thinhgialandrealtine.firebaseapp.com',
//   projectId: 'thinhgialandrealtine',
//   storageBucket: 'thinhgialandrealtine.appspot.com',
//   messagingSenderId: '109860909208',
//   appId: '1:109860909208:web:4585d96fe7b67163aa60ed',
//   measurementId: 'G-T9LDZ7S9JB',
// };

const firebaseConfig = {
  apiKey: 'AIzaSyAczeN--O1v8ldHFsSBXDM6MWmQyRF75UU',
  authDomain: 'thinhgia-5cf43.firebaseapp.com',
  projectId: 'thinhgia-5cf43',
  storageBucket: 'thinhgia-5cf43.appspot.com',
  messagingSenderId: '403218950952',
  appId: '1:403218950952:web:71f1c5818f4939ac6b3e1d',
  measurementId: 'G-VCWPVCNY54',
};
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

const auth = getAuth();

export { db, auth };
