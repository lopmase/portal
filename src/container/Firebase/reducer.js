const GET_MESSAGE = 'GET_MESSAGE';
const GET_STATUS = 'GET_STATUS';
const GET_PHONENUMBER = 'GET_PHONENUMBER';
const initialState = {
  type: 'Chat',
  data: {
    status: [],
    message: [],
    phoneNumber: [],
  },
};
export const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_MESSAGE:
      return {
        data: {
          ...state.data,
          message: action.payload,
        },
      };
    case GET_STATUS:
      return {
        data: { ...state.data, status: action.payload },
      };
    case GET_PHONENUMBER:
      return {
        data: { ...state.data, phoneNumber: action.payload },
      };
    default:
      return state;
  }
};
