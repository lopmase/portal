import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import { ROUTES } from '../../constants/ROUTES';
import NewHouse from './NewHouse/NewHouse';
import { PrivateRoute } from '../../components';
import Houses from './Houses';
import NotFound from '../Common/NotFound';
import Detail from './Detail/Detail';

class HouseContainer extends Component {
  state = {};

  render() {
    return (
      <Switch>
        <PrivateRoute exact path={ROUTES.NEW_HOUSE} component={NewHouse} />
        <PrivateRoute
          exact
          path={ROUTES.EDIT_HOUSE}
          component={props => <NewHouse {...props} editMode />}
        />
        <PrivateRoute exact path={ROUTES.HOUSES} component={Houses} />
        <PrivateRoute exact path={ROUTES.HOUSE} component={Detail} />
        <Route component={NotFound} />
      </Switch>
    );
  }
}

export default HouseContainer;
