import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Spin } from 'antd';

import Selector from '../../Selector';
import { Card } from '../../../../components';
import { getCommentByHouseId } from '../../../../apis/houseApi';
import { apiSerializer } from '../../../../helpers/apiSerializer';
import { showMessage } from '../../../Common/GlobalErrorModal/action';
import CommentEditor from '../../../DashboardContainer/Community/components/CommentEditor';
import CommentComponent from '../../../DashboardContainer/Community/Comment';

const Comments = props => {
  const { dataSource } = props;

  const [loading, setLoading] = useState(false);
  const [comments, setComments] = useState([]);
  const { data } = dataSource;
  // eslint-disable-next-line react/destructuring-assignment
  // eslint-disable-next-line react/prop-types
  const { id } = props && props.id ? props : data;
  const fetch = async () => {
    try {
      setLoading(true);
      const rs = await getCommentByHouseId(id);
      const result = apiSerializer(rs);
      if (result) {
        setComments(result);
      }
      setLoading(false);
    } catch (error) {
      setLoading(false);
      showMessage(error);
    }
  };

  useEffect(() => {
    if (id) {
      fetch();
      setTimeout(() => {
        window.ZaloSocialSDK.reload();
      }, 200);
    }
    return () => {};
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  return (
    <Card shadow containerStyle={{ marginTop: 10 }}>
      <h3 style={{ fontWeight: 'bold' }}>
        {'Bình luận ('}
        {comments.length}
        {')'}
      </h3>
      <div style={{}}>
        {loading && <Spin size="large" />}
        {comments.length > 0 &&
          comments.map(comment => (
            <CommentComponent onChange={fetch} key={comment.id} comment={comment} />
          ))}
        <CommentEditor
          id={id}
          onCommentDone={() => {
            fetch();
          }}
        />
      </div>
    </Card>
  );
};

Comments.propTypes = {
  dataSource: PropTypes.object.isRequired,
};

Comments.defaultProps = {};

export function mapStateToProps(state) {
  return {
    dataSource: Selector.detail(state),
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Comments);
