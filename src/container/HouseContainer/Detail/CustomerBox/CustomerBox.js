import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Selector from '../../Selector';
import { Card } from '../../../../components';

const CustomerBox = ({ dataSource }) => {
  const { data } = dataSource;

  const { customer } = data || {};
  const { firstName } = customer || {};
  return (
    <Card shadow>
      <h3 style={{ fontWeight: 'bold' }}>Khách hàng</h3>
      <div style={{}} onClick={() => {}}>
        {firstName}
      </div>
    </Card>
  );
};

CustomerBox.propTypes = {
  dataSource: PropTypes.object.isRequired,
};

CustomerBox.defaultProps = {};

export function mapStateToProps(state) {
  return {
    dataSource: Selector.detail(state),
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(CustomerBox);
