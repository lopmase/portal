/* eslint-disable camelcase */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { Row, Col, Modal } from 'antd';
import _ from 'lodash';
import { push, goBack } from 'connected-react-router';
import { faTimes, faPenAlt } from '@fortawesome/free-solid-svg-icons';

import * as actions from './action';
import './Detail.scss';
import { REDUCERS } from '../../../constants/COMMON';
import { FIELDS } from '../../../constants/HOUSE_FIELDS';
import { ROUTES } from '../../../constants/ROUTES';
import Selector from '../Selector';
import * as optionsAction from '../Metadata/optionsAction';
import { Flexbox, Card, Button } from '../../../components';
import { formatMoney, formatImageUrl } from '../../../helpers/common';
import ImagesBox from './Images/ImagesBox';
import CustomerBox from './CustomerBox/CustomerBox';
import ImageLightbox from '../../../components/LightBox/Lightbox';
import Comments from './Comments/Comments';
import History from './History/History';
import HtmlDescription from '../../Common/HtmlDescription/HtmlDescription';
import ShareButton from '../../Common/ShareButton/ShareButton';
class Detail extends Component {
  static propTypes = {};

  static defaultProps = {};

  state = { lightboxVisible: false, imageIndex: 0 };

  componentDidMount() {
    const { fetch, match, getOptions } = this.props;

    getOptions();
    fetch(match.params.id);
  }

  render() {
    const { lightboxVisible, imageIndex } = this.state;
    const { dataSource, options, doPush, back } = this.props;
    const { data } = dataSource;

    const notDetail = ['internal_image', 'public_image', 'image', 'house_direction'];
    const {
      title,
      description,
      house_number,
      house_address,
      into_money,
      image,
      purpose,
      created_at,
      user_id,
      internalDescription,
      id,
    } = data || {};
    const { internal, public: publicImages } = image || {};
    const images = _.values(internal)
      .map(p => formatImageUrl(p.main))
      .concat(_.values(publicImages).map(p => formatImageUrl(p.main)));
    return (
      <div className="detail-page">
        <div className="header">
          <Flexbox row spaceBetween alignItems="center">
            <Flexbox row alignItems="center">
              <h2 style={{ marginBottom: '0' }}>
                <span className="title">{`Nhân viên ${user_id}: `}</span>
                {moment(created_at * 1000).format('DD/MM/YYYY HH:mm:ss')}
              </h2>
              <Button
                onClick={() => doPush(`${ROUTES.HOUSE_ROOT}/edit/${id}`)}
                icon={faPenAlt}
                iconColor="#ff6c00"
                text="Sửa"
                clear
              />
              {data && <ShareButton house={data} marginleft={0} color="#ff6c00" fontSize={16} />}
            </Flexbox>
            <div>
              <Button
                iconOnly
                icon={faTimes}
                onClick={() => back()}
                containerStyle={{ paddingLeft: 15, paddingRight: 15 }}
              />
            </div>
          </Flexbox>
          <Row>
            <h2>{`${house_number} ${house_address}`}</h2>
            <Flexbox row spaceBetween alignItems="center">
              <h2>{`${title}`}</h2>
              <div>
                <div style={{ fontSize: 12 }}>{purpose === 0 ? 'BÁN' : 'THUÊ'}</div>
                <div className="price">{formatMoney(into_money)}</div>
              </div>
            </Flexbox>
          </Row>
        </div>
        {data !== null && (
          <div className="content">
            <Card containerStyle={{ marginBottom: 10 }} shadow>
              <Row gutter={[10, 10]}>
                {FIELDS.filter(key => !notDetail.includes(key)).map(field => (
                  <Col md={6} sm={8} xs={12}>
                    <div>
                      <div style={{ fontWeight: 'bold', fontSize: 13 }}>{field.label}</div>
                      <div className="small-text">
                        {field.format ? field.format(data, options) : data[field.property]}
                      </div>
                    </div>
                  </Col>
                ))}
              </Row>
            </Card>
          </div>
        )}
        <div className="content">
          {description && (
            <Card containerStyle={{ marginBottom: 10 }} shadow>
              <h3 style={{ fontWeight: 'bold' }}>Mô tả</h3>
              <HtmlDescription alwaysShowFull description={description} />
            </Card>
          )}
          {internalDescription && (
            <Card containerStyle={{ marginBottom: 10 }} shadow>
              <h3 style={{ fontWeight: 'bold' }}>Mô tả nội bộ</h3>
              <HtmlDescription alwaysShowFull description={internalDescription} />
            </Card>
          )}
          {data && (
            <ImagesBox
              onClick={index => this.setState({ lightboxVisible: true, imageIndex: index })}
            />
          )}
          {data && <CustomerBox />}
          {data && <Comments />}
          {data && <History />}
        </div>
        <ImageLightbox
          selectedIndex={imageIndex}
          isOpen={lightboxVisible}
          onClose={() => this.setState({ lightboxVisible: false })}
          images={images}
        />
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    dataSource: Selector.detail(state),
    options: state.metadata[REDUCERS.OPTIONS].data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      ...optionsAction,
      doPush: push,
      back: goBack,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Detail);
