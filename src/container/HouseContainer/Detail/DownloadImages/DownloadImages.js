import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Button, Dropdown, Menu } from 'antd';
import * as actions from '../action';

const DownloadImages = ({ house, downloadAllImage }) => {
  const [loading, setLoading] = useState(false);

  const onSelect = async ({ key }) => {
    setLoading(true);
    await downloadAllImage(house, key);
    setLoading(false);
  };

  const menu = (
    <Menu onClick={onSelect}>
      <Menu.Item key="all">Tất cả</Menu.Item>
      <Menu.Item key="public">Ảnh công khai</Menu.Item>
      <Menu.Item key="internal">Ảnh nội bộ</Menu.Item>
    </Menu>
  );

  return (
    <Dropdown overlay={menu}>
      <Button icon="download" loading={loading}>
        Tải xuống
      </Button>
    </Dropdown>
  );
};

DownloadImages.propTypes = {
  house: PropTypes.object.isRequired,
  downloadAllImage: PropTypes.func.isRequired,
};

DownloadImages.defaultProps = {};

export function mapStateToProps() {
  return {};
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
    },
    dispatch,
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(DownloadImages);
