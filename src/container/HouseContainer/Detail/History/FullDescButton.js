import React from 'react';
import PropTypes from 'prop-types';
import { Popover } from 'antd';
import HtmlDescription from '../../../Common/HtmlDescription/HtmlDescription';

const FullDescButton = ({ description }) => {
  const content = (
    <div style={{ width: 900 }}>
      <HtmlDescription alwaysShowFull description={description} />
    </div>
  );

  return (
    <Popover content={content} title="Nội dung mô tả" trigger="click">
      <div style={{ cursor: 'pointer', color: 'blue' }}>Xem tất cả</div>
    </Popover>
  );
};

FullDescButton.propTypes = {
  description: PropTypes.any.isRequired,
};

export default FullDescButton;
