import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Selector from '../../Selector';
import { Card } from '../../../../components';
import { history } from '../../../../apis/houseApi';
import { getTransactionByHouseIdV2 } from '../../../../apis/businessApi';
import { columns } from './columns';
import AntTable from '../../../../components/AntTable/AntTable';
import { apiSerializer } from '../../../../helpers/apiSerializer';
import { showMessage } from '../../../Common/GlobalErrorModal/action';
import { addComment } from '../../../DashboardContainer/Community/action';

const History = ({ dataSource, options }) => {
  const [loading, setLoading] = useState(false);
  const [items, setItem] = useState([]);
  const [transaction, setTransaction] = useState();

  const { data } = dataSource;
  const deviceMode = false;
  const { id } = data || {};
  console.log('dataSource', dataSource);
  const fetch = async (sort, order) => {
    try {
      setLoading(true);
      // const rs = await history(id, sort, order);
      const [house, transactionByHouseId] = await Promise.all([
        history(id, sort, order),
        getTransactionByHouseIdV2(id),
      ]);
      const result = apiSerializer(house);
      const transactionById = apiSerializer(transactionByHouseId);

      if (result.length) {
        setItem(
          result.map(item => {
            return {
              ...item,
              ...transactionById,
            };
          }),
        );
      } else {
        setItem([{ ...transactionById }]);
      }

      setLoading(false);
    } catch (error) {
      setLoading(false);
      showMessage(error);
    }
  };

  const getColumnFilter = column => {
    if (options && column.getFilter) {
      try {
        const filters = column.getFilter(options);
        if (Array.isArray(filters) && filters[0] && filters[0].value) {
          return { filters };
        }
      } catch (error) {
        return {};
      }
    }
    return {};
  };

  const onTableChanged = optionsChange => {
    if (optionsChange.sortChanged) {
      const order = optionsChange.sortOrder === 'descend' ? 'DESC' : 'ASC';
      fetch(optionsChange.sortField, order);
    } else {
      // updateAllFilters(optionsChange.filters);
    }
  };

  useEffect(() => {
    const getHistory = async () => {
      await fetch();
    };
    getHistory();
  }, [id]);
  console.log('item', items);
  return (
    <Card shadow containerStyle={{ marginTop: 10 }}>
      <h3 style={{ fontWeight: 'bold' }}>
        {'Số lần thay đổi ('}
        {items.length}
        {')'}
      </h3>
      <div>
        <AntTable
          data={items}
          columns={columns.map(col => ({
            ...(deviceMode
              ? { ...col, fixed: undefined }
              : {
                  ...col,
                  width:
                    col.dataIndex === 'id' || col.dataIndex === 'avatar' ? col.width : undefined,
                }),
            ...getColumnFilter(col),
          }))}
          autoSize={!deviceMode}
          width={900}
          height={400}
          onChanged={onTableChanged}
          loading={loading}
        />
      </div>
    </Card>
  );
};

History.propTypes = {
  dataSource: PropTypes.object.isRequired,
  options: PropTypes.any.isRequired,
};

History.defaultProps = {};

export function mapStateToProps(state) {
  return {
    dataSource: Selector.detail(state),
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      add: addComment,
    },
    dispatch,
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(History);
