/* eslint-disable react/no-danger */
import React from 'react';
import { Avatar } from 'antd';
import { FIELDS } from '../../../../constants/HOUSE_FIELDS';
import { formatImageUrl, convertStringToHTML, formatMoney } from '../../../../helpers/common';
import MetadataContainer from '../../Houses/MetadataContainer';
import FullDescButton from './FullDescButton';

const fields = [
  ...FIELDS,
  {
    property: 'public_approval',
    label: 'Duyệt cộng đồng',
    format: house => (Number(house.public_approval) ? 'Đã duyệt' : 'Chưa duyệt'),
  },
  {
    property: 'status',
    label: 'Tình trạng',
    format: house => (Number(house.status) ? 'Ngưng bán' : 'Mở bán'),
  },
];

export const createHouseFieldMap = () => {
  const newMapField = {
    internal_image: 'Ảnh công khai',
    public_image: 'Ảnh nội bộ',
    description: 'Mô tả',
    internalDescription: 'Mô tả nội bộ',
  };
  fields.forEach(item => {
    newMapField[item.property] = item.label;
  });
  return newMapField;
};

const renderDataCell = (value, item) => {
  if (Array.isArray(value)) {
    if (value.length === 0) {
      return <span>No data</span>;
    }
    if (value[0].main) {
      return (
        <span>
          {value.map(image => (
            <Avatar
              src={formatImageUrl(image.thumbnail)}
              shape="square"
              style={{ marginRight: 5 }}
            />
          ))}
        </span>
      );
    }
    return <span>{value.toString ? value.toString() : 'Binary Data'}</span>;
  }
  if (item.column === 'description') {
    if (!value) {
      return <span>Không có dữ liệu</span>;
    }
    if (value.length < 100) {
      return <span>{value}</span>;
    }
    const htmlContent = convertStringToHTML(value.toString());
    return (
      <span>
        {htmlContent ? (
          <p
            style={{ maxHeight: 50, overflow: 'hidden' }}
            dangerouslySetInnerHTML={{ __html: htmlContent }}
          />
        ) : (
          <p>{`${value.toString().substr(0, 50)}...`}</p>
        )}
        <FullDescButton description={value} />
      </span>
    );
  }
  if (item.column === 'internalDescription') {
    if (!value) {
      return <span>Không có dữ liệu</span>;
    }
    if (value.length < 100) {
      return <span>{value}</span>;
    }
    const htmlContent = convertStringToHTML(value.toString());
    return (
      <span>
        {htmlContent ? (
          <p
            style={{ maxHeight: 50, overflow: 'hidden' }}
            dangerouslySetInnerHTML={{ __html: htmlContent }}
          />
        ) : (
          <p>{`${value.toString().substr(0, 50)}...`}</p>
        )}
        <FullDescButton description={value} />
      </span>
    );
  }
  if (item.column === 'user_id') {
    return <span>{value}</span>;
  }
  return (
    <MetadataContainer
      render={options => {
        const column = fields.find(field => field.property === item.column);
        return (
          <span>
            {column && column.format ? column.format({ [item.column]: value }, options) : value}
          </span>
        );
      }}
    />
  );
};

const mapField = createHouseFieldMap();
export const columns = [
  {
    title: 'Người sửa',
    dataIndex: 'user',
    width: 150,
    render: value => value && (
      <span>
        <div className="bold-primary-text long-text">{`${value.name}`}</div>
      </span>
    ),
  },
  {
    title: 'Cột',
    dataIndex: 'column',
    width: 120,
    sorter: true,
    render: value => mapField[value] || value,
  },
  {
    title: 'Giá trị cũ',
    dataIndex: 'old_value',
    width: 100,
    render: renderDataCell,
  },
  {
    title: 'Giá trị mới',
    dataIndex: 'new_value',
    width: 100,
    render: renderDataCell,
  },
  {
    title: 'Thời điểm',
    dataIndex: 'created_at',
    sorter: true,
    width: 150,
    render: value => value && new Date(value * 1000).toLocaleString(),
  },
  {
    title: 'Giá chốt',
    dataIndex: 'price',
    width: 150,
    align: 'center',
    render: value => formatMoney(Number(value)),
  },
];
