import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Selector from '../../Selector';
import { Card } from '../../../../components';
import { formatImageUrl } from '../../../../helpers/common';
import DownloadImages from '../DownloadImages/DownloadImages';

const ImagesBox = ({ dataSource, onClick }) => {
  const { data } = dataSource;
  const { image } = data || {};
  const { internal, public: publicImages } = image || {};
  const thumbnails = _.values(internal)
    .map(p => formatImageUrl(p.thumbnail || p.main))
    .concat(_.values(publicImages).map(p => formatImageUrl(p.thumbnail || p.main)));
  const images = _.values(internal)
    .map(p => formatImageUrl(p.main))
    .concat(_.values(publicImages).map(p => formatImageUrl(p.main)));
  return (
    <div>
      <Card shadow containerStyle={{ marginBottom: 10 }}>
        <div
          style={{
            flexDirection: 'row',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <h3 style={{ fontWeight: 'bold' }}>{`Hình ảnh (${images.length})`}</h3>
          <DownloadImages house={data} />
        </div>
        <div style={{ flexDirection: 'row', display: 'flex', flexWrap: 'wrap' }}>
          {thumbnails.map((im, index) => (
            <div onClick={() => onClick(index)}>
              <img
                style={{
                  width: 80,
                  height: 80,
                  borderRadius: 5,
                  marginRight: 10,
                  marginBottom: 10,
                }}
                src={im}
                alt="detail"
              />
            </div>
          ))}
        </div>
      </Card>
    </div>
  );
};

ImagesBox.propTypes = {
  dataSource: PropTypes.object.isRequired,
  onClick: PropTypes.func,
};

ImagesBox.defaultProps = {
  onClick: () => {},
};

export function mapStateToProps(state) {
  return {
    dataSource: Selector.detail(state),
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(ImagesBox);
