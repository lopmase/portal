/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
import React, { useState, useEffect, useCallBack } from 'react';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import PropTypes from 'prop-types';
import { useDispatch, connect } from 'react-redux';
import { Form, Input, Modal } from 'antd';
import moment from 'moment';
import { REDUCERS } from '../../../../constants/COMMON';
import { generateModalActions } from '../../../../helpers/modalAction';
import { showMessage } from '../../../Common/GlobalErrorModal/GlobalErrorModal';
import { Button, Flexbox, Dropdown, Uploader } from '../../../../components';
import * as action from '../../HouseDetail/action';
import { Editor } from 'react-draft-wysiwyg';
import { convertToRaw, EditorState, convertFromRaw } from 'draft-js';
const { TextArea } = Input;
// eslint-disable-next-line react/prop-types
const NewModal = ({ backgroundColor, label, name, title, status, id, createReason }) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [isModalVisible, setIsModalVisible] = useState(status === 1);
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  // eslint-disable-next-line no-shadow
  useEffect(() => {
    setIsModalVisible(status === 1);
  }, [status]);
  const handleCancel = () => {
    form.resetFields();
    setIsModalVisible(false);
  };

  const onFinish = values => {
    createReason(values);
    form.resetFields();
    setIsModalVisible(false);
  };
  return (
    <div style={{ margin: 10 }}>
      <Modal
        title={title}
        visible={isModalVisible}
        onCancel={handleCancel}
        onOk={form.submit}
        okButtonProps={{ style: { backgroundColor: '#ff8731' } }}
      >
        <div>
          <Form
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            layout="vertical"
            form={form}
          >
            <Flexbox row spaceBetween>
              <Form.Item
                style={{ width: '100%' }}
                label={label.title}
                // eslint-disable-next-line react/prop-types
                name={name.reason}
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập lý do',
                  },
                ]}
              >
                <TextArea rows={4} required />
              </Form.Item>
            </Flexbox>
          </Form>
        </div>
      </Modal>
    </div>
  );
};
NewModal.defaultProps = {
  title: 'Lý do ngưng bán',
  field: {},
  backgroundColor: '#ff8731',
  name: {
    reason: 'reason_stop_selling',
  },
  label: {
    title: 'Nhập lý do',
  },
  message: {
    error: 'Someting was wrong, please try agian!',
  },
};
export function mapStateToProps(state) {
  return {
    // transactionBill: state.transactionBill.data,
    // typeCreate: state.Knowledge.typeKnowledge.typeCreate,
    state,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...action,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(NewModal);
