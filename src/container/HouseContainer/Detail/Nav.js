/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { goBack } from 'connected-react-router';
import { bindActionCreators } from 'redux';
import './Nav.scss';
import { Button, Flexbox } from '../../../components';

class NavBar extends Component {
  static propTypes = {};

  static defaultProps = {};

  state = {};

  render() {
    const { back } = this.props;
    return (
      <div className="navbar">
        <Flexbox row spaceBetween>
          <Button text="Upcoming" onClick={back} />
        </Flexbox>
      </div>
    );
  }
}

export function mapStateToProps() {
  return {};
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      back: goBack,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(NavBar);
