/* eslint-disable no-plusplus */
import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateDataActions } from '../../../helpers/dataAction';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { downloadAll } from '../../../helpers/common';

const ENDPOINT = process.env.REACT_APP_API_URL;
const { fetchDone, fetching, fetchError } = generateDataActions(REDUCERS.HOUSE_DETAIL_DATA);

export const fetch = houseId => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const response = await thunkDependencies.house.detail(houseId);
    const result = apiSerializer(response);
    dispatch(fetchDone(result));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};

export const downloadAllImage = (house, mode) => async (dispatch, getState, thunkDependencies) => {
  try {
    const response = await thunkDependencies.house.downloadAllImageByHouseId(house.id);
    if (!response) {
      return false;
    }

    const urls = [
      ['all', 'internal'].includes(mode) && `${ENDPOINT}${response.data.internal}`,
      ['all', 'public'].includes(mode) && `${ENDPOINT}${response.data.public}`,
    ];

    const address = `${house.house_number}_${house.house_address}`.replace(/\s/gi, '_');
    const fileNames = [
      ['all', 'internal'].includes(mode) && `${address}_noi_bo.zip`,
      ['all', 'public'].includes(mode) && `${address}_cong_khai.zip`,
    ];
    await downloadAll(urls.filter(Boolean), fileNames.filter(Boolean));

    return response;
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
    return false;
  }
};
