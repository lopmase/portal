import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

import { push } from 'connected-react-router';
import { faPenAlt, faShareAlt, faEllipsisV, faSyncAlt } from '@fortawesome/free-solid-svg-icons';
import { Flexbox, Button, ActionMenu } from '../../../components';
import { DELETE_HOUSE_CONFIRM } from '../../../constants/MESSAGE';
import { confirm } from '../../ShellContainner/ConfirmDialog/action';
import * as actions from './action';
import { ROUTES } from '../../../constants/ROUTES';
import { useUserProfile } from '../../../hooks/userProfileHook';
import { isAdmin } from '../../../helpers/roleUtil';

const getMenu = remove => {
  let items = [];
  if (remove) {
    items = [
      {
        text: 'Chuyển trạng thái',
        value: 'switch',
        icon: faSyncAlt,
      },
    ];
    // items.push({
    //   text: 'Xoá',
    //   value: 'delete',
    //   icon: faTrashAlt,
    // });
  }
  return items;
};

const ActionBox = ({
  house,
  confirmDelete,
  removeHouse,
  pushRoute,
  sharePublic,
  unsharePublic,
  shareWeb,
  unshareWeb,
  updateHouseStatus,
  openModalFillReason,
}) => {
  const user = useUserProfile();

  const onRemoveHouse = () => {
    confirmDelete(DELETE_HOUSE_CONFIRM, result => {
      if (result) {
        removeHouse(house.id);
      }
    });
  };

  const onEditHouse = () => {
    pushRoute(`${ROUTES.HOUSE_ROOT}/edit/${house.id}`);
  };

  const onSelectMenu = item => {
    if (item.value === 'public') {
      sharePublic(house.id);
    }
    if (item.value === 'inpublic') {
      unsharePublic(house.id);
    }
    if (item.value === 'web') {
      shareWeb(house.id);
    }
    if (item.value === 'unweb') {
      unshareWeb(house.id);
    }
    if (item.value === 'delete') {
      onRemoveHouse();
    }
    if (item.value === 'switch') {
      updateHouseStatus(house);
      // openModalFillReason(house.status);
    }
  };

  const getShareMenu = () => {
    const items = [];
    if (!house || (house.user_id !== user.id && !isAdmin(user))) {
      return [];
    }
    if (house.public_approval) {
      items.push({
        text: 'Thu hồi cộng đồng',
        value: 'inpublic',
      });
    } else {
      items.push({
        text: 'Chia sẻ cộng đồng',
        value: 'public',
      });
    }
    if (house.web) {
      if (isAdmin(user)) {
        items.push({
          text: 'Thu hồi Web',
          value: 'unweb',
        });
      }
    } else {
      items.push({
        text: 'Upload Web',
        value: 'web',
      });
    }
    return items;
  };

  return (
    <div className="action-box">
      <Flexbox row spaceBetween containerStyle={{}}>
        <Button onClick={onEditHouse} icon={faPenAlt} iconColor="#ff6c00" text="Sửa" clear />
        <ActionMenu
          items={getShareMenu()}
          onSelect={onSelectMenu}
          render={() => <Button icon={faShareAlt} iconColor="#ff6c00" text="Tải lên" clear />}
        />
        <ActionMenu
          render={() => (
            // <Button
            //   icon={faShareAlt}
            //   iconColor="#ff6c00"
            //   text="Đăng tin"
            //   onClick={() => pushRoute(ROUTES.POST_MANAGER)}
            //   clear
            // />
            <Link
              className="linkHouseToPost"
              // style={{ color: '#ff6c00', fontSize: 15, fontWeight: 700 }}
              to={{
                pathname: ROUTES.POST_MANAGER,
                state: {
                  house_id: house.id,
                  house_address: house.house_address,
                  house_number: house.house_number,
                  province: house.province,
                  district: house.district,
                },
              }}
            >
              Đăng tin
            </Link>
          )}
        />
        <ActionMenu
          items={getMenu(
            (house && house.user_id === user.id) ||
              (user && isAdmin(user)) ||
              (user && user.update_product_permission === 1),
          )}
          onSelect={onSelectMenu}
          render={() => <Button icon={faEllipsisV} iconColor="#ff6c00" text="Thêm" clear />}
        />
      </Flexbox>
    </div>
  );
};

ActionBox.propTypes = {
  house: PropTypes.object,
  removeHouse: PropTypes.func.isRequired,
  confirmDelete: PropTypes.func.isRequired,
  pushRoute: PropTypes.func.isRequired,
  sharePublic: PropTypes.func.isRequired,
  unsharePublic: PropTypes.func.isRequired,
  shareWeb: PropTypes.func.isRequired,
  unshareWeb: PropTypes.func.isRequired,
  updateHouseStatus: PropTypes.func.isRequired,
};

ActionBox.defaultProps = {
  house: null,
};
export function mapStateToProps(state) {
  return {
    user: state.auth.user.data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      confirmDelete: confirm,
      pushRoute: push,
    },
    dispatch,
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(ActionBox);
