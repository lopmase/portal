/* eslint-disable react/no-danger */
/* eslint-disable react/no-array-index-key */
/* eslint-disable camelcase */
import classNames from 'classnames';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import avatar from '../../../assets/images/avt2.png';
import { Flexbox } from '../../../components';
import ImageLightbox from '../../../components/LightBox/Lightbox';
import { REDUCERS } from '../../../constants/COMMON';
import { FIELDS } from '../../../constants/HOUSE_FIELDS';
import { convertStringToHTML, formatImageUrl } from '../../../helpers/common';
// eslint-disable-next-line import/no-useless-path-segments
import Comments from '../../HouseContainer/Detail/Comments/Comments';
import DownloadImages from '../Detail/DownloadImages/DownloadImages';
import * as actions from './action';
import ActionBox from './ActionBox';
import './HouseDetail.scss';
import Info from './Info';
import ModalReason from '../Detail/ModalReason/ModalReason';

const notDetail = [
  'number_wc',
  'number_bedroom',
  'area',
  'floors',
  'wide_street',
  'house_number',
  'house_address',
  'district',
  'public',
  'web',
  'into_money',
  'id',
  'customer_id',
  'user_id',
  'province',
  'title',
  'description',
  'descriptions',
];
const HouseDetail = props => {
  const [lightboxVisible, setLightboxVisible] = useState(false);
  const [imageIndex, setImageIndex] = useState(0);
  const { house, visible, options, status } = props;
  const {
    image,
    customer,
    description,
    internalDescription,
    reject_web_condition,
    reject_public_condition,
    reason_stop_selling,
  } = house;
  const { internal, public: publicImages } = image || {};
  const [htmlContent, setHtml] = useState(null);
  const [interalDescriptionhtmlContent, setInteralDescriptionhtmlContent] = useState(null);
  const { id } = house;
  const thumbnails = _.values(internal)
    .map(p => formatImageUrl(p.thumbnail || p.main))
    .concat(_.values(publicImages).map(p => formatImageUrl(p.thumbnail || p.main)));
  const images = _.values(internal)
    .map(p => formatImageUrl(p.main))
    .concat(_.values(publicImages).map(p => formatImageUrl(p.main)));
  useEffect(() => {
    setHtml(convertStringToHTML(description));
  }, [description]);
  console.log('house', house);
  console.log('status', status);

  useEffect(() => {
    setInteralDescriptionhtmlContent(convertStringToHTML(internalDescription));
  }, [internalDescription]);
  return (
    <div className={classNames('house-detail', { visible })}>
      <div className="header">
        <Info house={house} />
        <ActionBox house={house} />
        <div className="customer">
          <img
            alt="detail-cus"
            src={customer && customer.avatar ? customer.avatar.image : avatar}
          />
          <div>
            <div className="text">{customer ? customer.name : ''}</div>
            <div className="phone">{customer ? customer.phone_number : ''}</div>
          </div>
        </div>
      </div>

      <div className="image-row">
        <Flexbox row spaceBetween containerStyle={{ marginRight: 10, marginBottom: 10 }}>
          <h4>{`${images.length} ảnh`}</h4>
          <DownloadImages house={house} />
        </Flexbox>
        <div className="row flex">
          {thumbnails.map((im, index) => (
            <div
              onClick={() => {
                setLightboxVisible(true);
                setImageIndex(index);
              }}
            >
              <img src={im} alt="detail" />
            </div>
          ))}
        </div>
      </div>
      {(reject_public_condition || reject_web_condition || reason_stop_selling) && (
        <div className="reject-condition">
          {reject_public_condition && (
            <div className="reject-public">
              <h4 className="warning">Lý do từ chối duyệt Cộng đồng</h4>
              <p>{reject_public_condition}</p>
            </div>
          )}
          {reject_web_condition && (
            <div className="reject-web">
              <h4 className="warning">Lý do từ chối duyệt Web</h4>
              <p>{reject_web_condition}</p>
            </div>
          )}
          {reason_stop_selling && (
            <div className="reject-web">
              <h4 className="warning">Lý do ngưng bán</h4>
              <p>{reason_stop_selling}</p>
            </div>
          )}
        </div>
      )}
      {htmlContent ? (
        <div className="description">
          <h4>Mô tả</h4>
          <div
            dangerouslySetInnerHTML={{
              __html: htmlContent,
            }}
          />
        </div>
      ) : (
        <div className="description">
          <h4>Mô tả</h4>
          <p>{description}</p>
        </div>
      )}
      {interalDescriptionhtmlContent ? (
        <div className="description">
          <h4>Mô tả nội bộ</h4>
          <div
            dangerouslySetInnerHTML={{
              __html: interalDescriptionhtmlContent,
            }}
          />
        </div>
      ) : (
        <div className="description">
          <h4>Mô tả nội bộ</h4>
          <p>{internalDescription}</p>
        </div>
      )}
      <div className="detail">
        {FIELDS.filter(
          f =>
            f.label !== 'Description' && f.label !== 'Key Word' && !notDetail.includes(f.property),
        ).map(field => (
          <Flexbox row spaceBetween key={field.property}>
            <div className="label">{field.label}</div>
            <span>{field.format ? field.format(house, options) : house[field.property]}</span>
          </Flexbox>
        ))}
      </div>
      <ImageLightbox
        selectedIndex={imageIndex}
        isOpen={lightboxVisible}
        onClose={() => setLightboxVisible(false)}
        images={images}
      />
      {id && <Comments id={id} />}
      <ModalReason status={status} id={id} />
    </div>
  );
};

HouseDetail.propTypes = {
  house: PropTypes.any,
  visible: PropTypes.bool.isRequired,
  options: PropTypes.any.isRequired,
};

HouseDetail.defaultProps = {
  house: {},
};

export function mapStateToProps(state) {
  return {
    visible: state.house[REDUCERS.HOUSE_DETAIL].visible,
    options: state.metadata[REDUCERS.OPTIONS].data,
    status: state.house[REDUCERS.HOUSE_DETAIL].openModalFillReason,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(HouseDetail);
