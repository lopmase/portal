/* eslint-disable react/no-danger */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable camelcase */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faMapMarkerAlt,
  faUserAlt,
  faBed,
  faLayerGroup,
  faRoad,
  faSquare,
  faEye,
} from '@fortawesome/free-solid-svg-icons';
import styles from '../../../styles/_Colors.scss';
import { Flexbox, Label } from '../../../components';
import { formatMoney } from '../../../helpers/common';
import { REDUCERS } from '../../../constants/COMMON';
import { ROUTES } from '../../../constants/ROUTES';
import ShareButton from '../../Common/ShareButton/ShareButton';

const Info = ({ house, dataSource: { fetching } }) => {
  const {
    purpose,
    into_money,
    house_number,
    house_address,
    district,
    project,
    floors,
    wide_street,
    province,
    number_bedroom,
    number_wc,
    public_approval: isPublic,
    web,
    area,
    id,
    status,
  } = house;
  useEffect(() => {
    setTimeout(() => {
      window.ZaloSocialSDK.reload();
    }, 200);
    return () => {};
  }, [fetching]);

  return (
    <div className="info">
      {status == 2 && (
        <div>
          <h3 style={{ color: '#86DE70' }}>Đang giao dịch</h3>
        </div>
      )}
      <Flexbox row spaceBetween>
        <Flexbox row flexStart>
          <h3>{`VNĐ ${formatMoney(into_money, purpose)}`}</h3>
          {isPublic === 1 && (
            <Label containerStyle={{ marginLeft: 10 }} background="limegreen" text="Cộng đồng" />
          )}
          {web === 1 && (
            <Label containerStyle={{ marginLeft: 10 }} background="dodgerblue" text="Web" />
          )}
        </Flexbox>
        <Link to={`${ROUTES.HOUSE_ROOT}/${id}`}>
          <FontAwesomeIcon style={{ fontSize: 25 }} color={styles.primaryColor} icon={faEye} />
        </Link>
      </Flexbox>
      <div style={{ marginLeft: 3 }}>
        <FontAwesomeIcon color="gray" icon={faMapMarkerAlt} />
        <span style={{ color: 'gray', marginLeft: 10, fontSize: 13, fontWeight: 800 }}>
          {project && project.name
            ? `${house_number} ${house_address}, ${project.name}, ${district}, ${province}, `
            : `${house_number} ${house_address}, ${district}, ${province}`}
        </span>
      </div>
      <div>
        <FontAwesomeIcon color="gray" icon={faUserAlt} />
        <span style={{ color: 'gray', marginLeft: 10, fontSize: 13, fontWeight: 800 }}>
          {house.user ? house.user.name : ''}
        </span>
      </div>
      <ShareButton house={house} marginleft={0} color="#ff6c00" fontSize={16} />
      <Flexbox row spaceBetween containerStyle={{ paddingTop: 20 }}>
        <div className="icon-item">
          <FontAwesomeIcon icon={faBed} color="grey" />
          <span>{number_bedroom || 0}</span>
        </div>
        <div className="icon-item">
          <FontAwesomeIcon icon={faBed} color="grey" />
          <span>{number_wc || 0}</span>
        </div>
        <div className="icon-item">
          <FontAwesomeIcon icon={faLayerGroup} color="grey" />
          <span>{floors || 0}</span>
        </div>
        <div className="icon-item">
          <FontAwesomeIcon icon={faRoad} color="grey" />
          <span>{`${wide_street}m`}</span>
        </div>
        <div className="icon-item">
          <FontAwesomeIcon icon={faSquare} color="grey" />
          <span>{`${area || 0}m2`}</span>
        </div>
      </Flexbox>
    </div>
  );
};

Info.propTypes = {
  house: PropTypes.object,
  dataSource: PropTypes.object.isRequired,
};

Info.defaultProps = {
  house: {},
};

export function mapStateToProps(state) {
  return {
    dataSource: state.house[REDUCERS.HOUSES],
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Info);
