import { toast } from 'react-toastify';
import { HOUSE_DETAIL_TYPES } from './types';
import { generateModalActions } from '../../../helpers/modalAction';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { REDUCERS } from '../../../constants/COMMON';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { confirm } from '../../ShellContainner/ConfirmDialog/action';
import { fetch } from '../Houses/action';
import { generateDatalistActions } from '../../../helpers/dataListAction';

const { openModal, closeModal } = generateModalActions(REDUCERS.GLOBAL_LOADING);
const { updatePage } = generateDatalistActions(REDUCERS.HOUSES);

export const open = () => ({
  type: HOUSE_DETAIL_TYPES.OPEN,
});

export const close = () => ({
  type: HOUSE_DETAIL_TYPES.CLOSE,
});

// todo
export const setHouseTypeFocus = keyType => ({
  type: HOUSE_DETAIL_TYPES.SET_HOUSE_TYPE_FOCUS,
  keyType,
  fetchType:
    (keyType === '1' && 'publicList') ||
    (keyType === '2' && 'approved') ||
    (keyType === '3' && 'waitingApproval') ||
    (keyType === '4' && 'notApproved') ||
    (keyType === '5' && 'personalList') ||
    (keyType === '6' && 'suspend'),
});

export const removeHouse = id => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(openModal());
    await thunkDependencies.house.remove(id);
    dispatch(closeModal());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};

export const sharePublic = id => async (dispatch, getState, thunkDependencies) => {
  try {
    const wantToContinue = await dispatch(
      confirm(
        `Bạn có muốn chia sẻ ra cộng đồng không?`,
        null,
        'Tất cả mọi người sẽ nhìn thấy BĐS này nhưng không thể thấy thông tin khách hàng.',
      ),
    );
    if (!wantToContinue) {
      return;
    }

    dispatch(openModal());
    await thunkDependencies.house.update({ house_id: id, public: 1 });
    dispatch(closeModal());
    dispatch(close());
    dispatch(updatePage(1));
    dispatch(fetch());
    toast.success('Chia sẻ thành công!');
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};

export const unsharePublic = id => async (dispatch, getState, thunkDependencies) => {
  try {
    const wantToContinue = await dispatch(
      confirm(
        `Bạn có muốn thu hồi BĐS khỏi cộng đồng không?`,
        null,
        'Mọi người sẽ không nhìn thấy BĐS này nữa.',
      ),
    );
    if (!wantToContinue) {
      return;
    }

    dispatch(openModal());
    await thunkDependencies.house.update({ house_id: id, public: 0, public_approval: 0 });
    dispatch(closeModal());
    dispatch(close());
    dispatch(updatePage(1));
    dispatch(fetch());
    toast.success('Thu hồi thành công!');
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};

export const shareWeb = id => async (dispatch, getState, thunkDependencies) => {
  try {
    const wantToContinue = await dispatch(
      confirm(
        `Bạn có muốn chia sẻ ra web không?`,
        null,
        'Admin sẽ duyệt BĐS của bạn trước khi đưa lên Web thương mại.',
      ),
    );
    if (!wantToContinue) {
      return;
    }

    dispatch(openModal());
    await thunkDependencies.house.uploadWeb({ house_id: id });
    dispatch(closeModal());
    dispatch(close());
    dispatch(updatePage(1));
    dispatch(fetch());
    toast.success('Chia sẻ thành công!');
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};

export const unshareWeb = id => async (dispatch, getState, thunkDependencies) => {
  try {
    const wantToContinue = await dispatch(
      confirm(
        `Bạn có muốn thu hồi BĐS khỏi web không?`,
        null,
        'BĐS sẽ không còn hiển thị trên Web thương mại nữa.',
      ),
    );
    if (!wantToContinue) {
      return;
    }

    dispatch(openModal());
    await thunkDependencies.house.update({ house_id: id, web: 0 });
    dispatch(closeModal());
    dispatch(close());
    dispatch(updatePage(1));
    dispatch(fetch());
    toast.success('Gỡ BĐS thành công!');
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};

export const openModalFillReason = (status, data) => async (
  dispatch,
  getState,
  thunkDependencies,
) => {
  try {
    // const newStatus = status === 0 ? 1 : 0;
    dispatch({ type: HOUSE_DETAIL_TYPES.SET_OPEN_MODAL, status, data });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};

export const updateHouseStatus = house => async (dispatch, getState, thunkDependencies) => {
  try {
    const newStatus = house.status === 0 ? 1 : 0;
    const resetCondition = true;
    const message =
      newStatus === 0
        ? 'Bạn có muốn chuyển BĐS thành mở bán'
        : 'Bạn có muốn chuyển BĐS thành ngưng bán';
    const confirmed = await dispatch(confirm(message));
    if (!confirmed) {
      return;
    }
    dispatch(openModal());
    const param = { house_id: house.id, status: newStatus, resetCondition };
    if (newStatus === 1) {
      param.web = 0;
    }
    if (newStatus === 0) {
      const resetReason = true;
      await thunkDependencies.house.update({ ...param, resetReason });
      dispatch(closeModal());
      dispatch(fetch());
      toast.success('Thay đổi thành công!');
    } else {
      dispatch(openModalFillReason(newStatus, param));
    }
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};

export const createReason = reason => async (dispatch, getState, thunkDependencies) => {
  try {
    const data = getState().house[REDUCERS.HOUSE_DETAIL].dataUpdateStopSelling;
    await thunkDependencies.house.update({ ...data, ...reason });
    dispatch(closeModal());
    dispatch(fetch());
    toast.success('Thay đổi thành công!');
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};
