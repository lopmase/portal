import { HOUSE_DETAIL_TYPES } from './types';

// 1 => public
// 2 => approved
// 6 loại type tương ứngg với 6 trạng thái bds bds=> cộng đồng, đã duyệt...
const initial = {
  visible: false,
  keyTypeHouse: '1',
  fetchType: 'publicList',
  openModalFillReason: null,
  dataUpdateStopSelling: {},
};

const detailReducer = (state = initial, action) => {
  switch (action.type) {
    case HOUSE_DETAIL_TYPES.OPEN:
      return {
        ...state,
        visible: true,
      };
    case HOUSE_DETAIL_TYPES.CLOSE:
      return {
        ...state,
        visible: false,
        openModalFillReason: null,
      };
    case HOUSE_DETAIL_TYPES.SET_HOUSE_TYPE_FOCUS:
      return {
        ...state,
        keyTypeHouse: action.keyType,
        fetchType: action.fetchType,
      };
    case HOUSE_DETAIL_TYPES.SET_OPEN_MODAL:
      return {
        ...state,
        openModalFillReason: action.status,
        dataUpdateStopSelling: action.data,
      };
    default:
      return state;
  }
};

export default detailReducer;
