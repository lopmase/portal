import React, { useState } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { faTrashAlt, faCaretDown, faCaretUp } from '@fortawesome/free-solid-svg-icons';
import { Card, Flexbox, Input, Dropdown, Button, Checkbox } from '../../../components';
import { OPERATION } from './types';
import { FIELDS } from '../../../constants/HOUSE_FIELDS';
import Summary from './Summary';
import { escapeUnicode } from '../../../helpers/formatter';
import * as actions from './action';

const LoadingItems = () => <div className="value-item">Loading...</div>;

const FilterItem = ({
  filter,
  onColumnSelect,
  onValueChange,
  onRemove,
  selectAll,
  index,
  unselectAll,
}) => {
  const [isExpanded, setExpanded] = useState(true);
  const [searchValue, updateSearch] = useState('');
  const field = FIELDS.filter(f => f.property === filter.column)[0];
  const filterSettings = field && field.filter ? field.filter : {};

  const handleValueChange = (value, removed) => {
    const { values } = filter;
    if (removed) {
      values.splice(values.indexOf(value), 1);
    } else {
      values.push(value);
    }
    onValueChange(values);
  };

  const onSearchChanged = value => {
    updateSearch(value);
  };

  const localFilter = item => {
    if (searchValue) {
      const uppercaseValue = item && item.toUpperCase ? item.toUpperCase() : '';
      const escapedValue = escapeUnicode(uppercaseValue).toUpperCase();
      return (
        escapedValue.indexOf(searchValue.toUpperCase()) >= 0 ||
        uppercaseValue.indexOf(searchValue.toUpperCase()) >= 0
      );
    }
    return true;
  };

  const handleMinMaxChange = (name, value) => {
    const { values } = filter;
    if (name === 'from') {
      values[0] = value;
    } else {
      values[1] = value;
    }
    onValueChange(values);
  };

  const renderValues = () => {
    if (filterSettings.fetchValue || filterSettings.fetchOption) {
      if (!filter.data) return <LoadingItems />;
      return filter.data.filter(localFilter).map(value => (
        <div key={value} className="value-item">
          <Checkbox
            onChange={check => handleValueChange(value, !check)}
            label={value}
            checked={filter.values.includes(value)}
          />
        </div>
      ));
    }

    if (filterSettings.operation === OPERATION.BETWEEN) {
      return (
        <div className="value-item minmax">
          <Flexbox row spaceBetween>
            <Flexbox row>
              <div>Từ</div>
              <Input value={filter.values[0]} name="from" onChange={handleMinMaxChange} />
              <div>{filterSettings.suffix}</div>
            </Flexbox>
            <Flexbox row>
              <div>Đến</div>
              <Input value={filter.values[1]} name="to" onChange={handleMinMaxChange} />
              <div>{filterSettings.suffix}</div>
            </Flexbox>
          </Flexbox>
        </div>
      );
    }

    return <div />;
  };

  const handleColumnSelect = ({ item }) => {
    const selectedField = FIELDS.filter(f => f.property === item.value)[0];
    onColumnSelect(selectedField);
  };

  return (
    <div className={classNames('filter-item', { expand: isExpanded })}>
      <Card shadow>
        <Flexbox row spaceBetween>
          <div>
            <Dropdown
              selected={filter.column}
              onChanged={handleColumnSelect}
              placeholder="Chọn cột"
              items={FIELDS.map(col => ({ text: col.label, value: col.property }))}
            />
          </div>
          <div className="btns">
            <Button
              icon={faTrashAlt}
              onClick={onRemove}
              containerStyle={{ paddingRight: 8, paddingLeft: 8 }}
              backgroundColor="white"
              iconColor="gray"
              iconOnly
            />
            <Button
              icon={isExpanded ? faCaretUp : faCaretDown}
              onClick={() => setExpanded(!isExpanded)}
              backgroundColor="white"
              iconColor="gray"
              iconOnly
            />
          </div>
        </Flexbox>
        <div className={classNames('filter-item-body', { expand: isExpanded })}>
          <div
            className={classNames('filter-content', {
              visible: isExpanded,
              'filter-values': true,
            })}
          >
            {filter.column && filterSettings.operation !== OPERATION.BETWEEN && (
              <div className="search">
                <Input
                  placeholder="Tìm"
                  value={searchValue}
                  onChange={(_name, value) => onSearchChanged(value)}
                />
              </div>
            )}
            {renderValues()}
          </div>
          {filter.column && isExpanded && filterSettings.operation !== OPERATION.BETWEEN && (
            <div className="selector" style={{ background: 'white' }}>
              <Button text="Chọn tất cả" clear onClick={() => selectAll(index)} />
              <Button text="Bỏ chọn tất cả" onClick={() => unselectAll(index)} clear />
            </div>
          )}
          <div
            className={classNames('filter-content', {
              visible: !isExpanded,
              compact: true,
            })}
          >
            <Summary filter={filter} />
          </div>
        </div>
      </Card>
    </div>
  );
};

FilterItem.propTypes = {
  filter: PropTypes.object.isRequired,
  onColumnSelect: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  onValueChange: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  selectAll: PropTypes.func.isRequired,
  unselectAll: PropTypes.func.isRequired,
};

FilterItem.defaultProps = {};

export function mapStateToProps() {
  return {};
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(FilterItem);
