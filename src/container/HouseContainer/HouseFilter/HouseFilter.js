/* eslint-disable react/no-array-index-key */
/* eslint-disable camelcase */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes, faFilter } from '@fortawesome/free-solid-svg-icons';
import { Flexbox, Button } from '../../../components';
import * as actions from './action';

import { REDUCERS } from '../../../constants/COMMON';
import './HouseFilter.scss';
import FilterItem from './FilterItem';

const HouseFilter = props => {
  const {
    filters,
    addNew,
    options,
    selectColumn,
    removeAllFilter,
    removeFilter,
    updateColumnValues,
    toggle,
    visible,
  } = props;
  return (
    <div className={classNames('filter-bar', { visible })}>
      <Flexbox row spaceBetween>
        <Flexbox row>
          <FontAwesomeIcon style={{ marginRight: 10 }} color="#3f536e" icon={faFilter} />
          <h3>Lọc</h3>
        </Flexbox>
        <Button
          iconOnly
          icon={faTimes}
          onClick={toggle}
          iconColor="black"
          backgroundColor="white"
          containerStyle={{ paddingRight: 14, paddingLeft: 14 }}
        />
      </Flexbox>
      <div className="body">
        {filters.map((filter, index) => (
          <FilterItem
            options={options}
            key={`column-${index}`}
            index={index}
            filter={filter}
            onRemove={() => removeFilter(index)}
            onValueChange={values => updateColumnValues(index, values)}
            onColumnSelect={selectedField => selectColumn(index, selectedField)}
          />
        ))}
        <Button text="+ Thêm tiêu chí" containerStyle={{}} clear onClick={addNew} />
      </div>
      <Flexbox
        row
        spaceBetween
        containerStyle={{ position: 'absolute', width: '100%', bottom: 20, right: 20 }}
      >
        <div />
        <div className="clearBtn">
          <Button
            text="Xoá tất cả"
            onClick={removeAllFilter}
            containerStyle={{ paddingLeft: 0 }}
            clear
          />
        </div>
        {/* <Button text="Lưu" /> */}
      </Flexbox>
    </div>
  );
};

HouseFilter.propTypes = {
  // onClick: PropTypes.func,
  filters: PropTypes.array.isRequired,
  options: PropTypes.any.isRequired,
  addNew: PropTypes.func.isRequired,
  toggle: PropTypes.func.isRequired,
  removeAllFilter: PropTypes.func.isRequired,
  removeFilter: PropTypes.func.isRequired,
  selectColumn: PropTypes.func.isRequired,
  visible: PropTypes.bool.isRequired,
  updateColumnValues: PropTypes.func.isRequired,
};

HouseFilter.defaultProps = {
  // onClick: () => {},
};

export function mapStateToProps(state) {
  return {
    visible: state.house[REDUCERS.HOUSE_FILTER].visible,
    filters: state.house[REDUCERS.HOUSE_FILTER].filters,
    options: state.metadata[REDUCERS.OPTIONS].data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(HouseFilter);
