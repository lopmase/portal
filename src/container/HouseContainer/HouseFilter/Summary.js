import React from 'react';

import PropTypes from 'prop-types';
import { OPERATION } from './types';
import { Label, Flexbox } from '../../../components';

const Summary = ({ filter }) => {
  if (filter.operation === OPERATION.BETWEEN) {
    return (
      <div>
        {'Từ '}
        <b style={{ color: '#ff6c00', fontSize: 14 }}>{filter.values[0] + filter.suffix}</b>
        {' đến '}
        <b style={{ color: '#ff6c00', fontSize: 14 }}>{filter.values[1] + filter.suffix}</b>
      </div>
    );
  }
  if (filter.values) {
    return (
      <Flexbox row flexStart containerStyle={{}}>
        {filter.values.slice(0, 3).map(value => (
          <div style={{ marginRight: 5, marginBottom: 5 }} key={value}>
            <Label background="#ff6c00" text={value} />
          </div>
        ))}
        {filter.values.length > 3 && (
          <div style={{ marginBottom: 5 }}>
            <Label background="#ff9241" text={`${filter.values.length - 3} more`} />
          </div>
        )}
      </Flexbox>
    );
  }

  return <div />;
};

Summary.propTypes = {
  filter: PropTypes.object.isRequired,
};

Summary.defaultProps = {};

export default Summary;
