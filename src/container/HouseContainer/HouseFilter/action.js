import { fetch } from '../Houses/action';
import { HOUSE_FILTER_TYPES } from './types';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { REDUCERS } from '../../../constants/COMMON';

const addFilterItem = () => ({
  type: HOUSE_FILTER_TYPES.NEW_FILTER,
});

export const toggle = () => ({
  type: HOUSE_FILTER_TYPES.TOGGLE,
});

export const selectAllAction = index => ({
  type: HOUSE_FILTER_TYPES.SELECT_ALL_VALUES,
  index,
});

export const unselectAllAction = index => ({
  type: HOUSE_FILTER_TYPES.UNSELECT_ALL_VALUES,
  index,
});

export const removeAll = () => ({
  type: HOUSE_FILTER_TYPES.REMOVE_ALL,
});

export const removeItem = index => ({
  type: HOUSE_FILTER_TYPES.REMOVE_FILTER,
  index,
});

const selectColumnAction = (index, column) => ({
  type: HOUSE_FILTER_TYPES.SELECT_COLUMN,
  index,
  column,
});

const updateColumnValueAction = (index, values) => ({
  type: HOUSE_FILTER_TYPES.UPDATE_COLUMN_VALUES,
  index,
  values,
});

export const fetchColumnValueDone = (index, values) => ({
  type: HOUSE_FILTER_TYPES.FETCH_COLUMN_DONE,
  index,
  values,
});

export const addNew = () => async dispatch => {
  dispatch(addFilterItem());
};

export const fetchDistinctValues = (index, column) => async (
  dispatch,
  getState,
  thunkDependencies,
) => {
  try {
    const response = await thunkDependencies.house.distinctValues(column);
    const result = apiSerializer(response);
    dispatch(fetchColumnValueDone(index, result));
  } catch (error) {
    dispatch(fetchColumnValueDone(index, []));
  }
};

export const selectColumn = (index, column) => async (dispatch, getState) => {
  dispatch(selectColumnAction(index, { ...column.filter, column: column.property }));
  if (column.filter && column.filter.fetchValue) {
    dispatch(fetchDistinctValues(index, column.property));
  }
  if (column.filter && column.filter.fetchOption) {
    const options = getState().metadata[REDUCERS.OPTIONS].data;
    dispatch(fetchColumnValueDone(index, column.filter.fetchOption(options)));
  }
};

export const updateColumnValues = (index, values) => async dispatch => {
  dispatch(updateColumnValueAction(index, values));
  dispatch(fetch());
};

export const removeAllFilter = () => async dispatch => {
  dispatch(removeAll());
  dispatch(fetch());
};

export const removeFilter = index => async dispatch => {
  dispatch(removeItem(index));
  dispatch(fetch());
};

export const selectAll = index => async dispatch => {
  dispatch(selectAllAction(index));
  dispatch(fetch());
};
export const unselectAll = index => async dispatch => {
  dispatch(unselectAllAction(index));
  dispatch(fetch());
};
