import { HOUSE_FILTER_TYPES, OPERATION } from './types';

const initial = {
  visible: false,
  filters: [],
};
let filters = null;

const filterReducer = (state = initial, action) => {
  switch (action.type) {
    case HOUSE_FILTER_TYPES.TOGGLE:
      return {
        ...state,
        visible: !state.visible,
      };
    case HOUSE_FILTER_TYPES.REMOVE_ALL:
      return {
        ...state,
        filters: [],
      };
    case HOUSE_FILTER_TYPES.REMOVE_FILTER:
      filters = state.filters.slice();
      filters.splice(action.index, 1);
      return {
        ...state,
        filters,
      };
    case HOUSE_FILTER_TYPES.NEW_FILTER:
      filters = state.filters.slice();
      filters.push({ column: '', operation: OPERATION.IN, values: [] });
      return {
        ...state,
        filters,
      };
    case HOUSE_FILTER_TYPES.SELECT_COLUMN:
      filters = state.filters.slice();
      filters[action.index] = { ...filters[action.index], ...action.column };
      return {
        ...state,
        filters,
      };
    case HOUSE_FILTER_TYPES.UPDATE_COLUMN_VALUES:
      filters = state.filters.slice();
      filters[action.index].values = action.values.slice();
      return {
        ...state,
        filters,
      };
    case HOUSE_FILTER_TYPES.FETCH_COLUMN_DONE:
      filters = state.filters.slice();
      filters[action.index].data = action.values;
      filters[action.index].values = action.values.slice();
      return {
        ...state,
        filters,
      };
    case HOUSE_FILTER_TYPES.SELECT_ALL_VALUES:
      filters = state.filters.slice();
      filters[action.index].values = filters[action.index].data.slice();
      return {
        ...state,
        filters,
      };
    case HOUSE_FILTER_TYPES.UNSELECT_ALL_VALUES:
      filters = state.filters.slice();
      filters[action.index].values = [];
      return {
        ...state,
        filters,
      };
    case HOUSE_FILTER_TYPES.UPDATE_ALL:
      return {
        ...state,
        filters: action.filters,
      };
    default:
      return state;
  }
};

export default filterReducer;
