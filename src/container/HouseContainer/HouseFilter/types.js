export const HOUSE_FILTER_TYPES = {
  TOGGLE: 'HOUSE_FILTER_TYPES/TOGGLE',
  SELECT_COLUMN: 'HOUSE_FILTER_TYPES/SELECT_COLUMN',
  NEW_FILTER: 'HOUSE_FILTER_TYPES/NEW',
  FETCH_COLUMN_DONE: 'HOUSE_FILTER_TYPES/FETCH_COLUMN_DONE',
  UPDATE_COLUMN_VALUES: 'HOUSE_FILTER_TYPES/UPDATE_COLUMN_VALUES',
  REMOVE_ALL: 'HOUSE_FILTER_TYPES/REMOVE_ALL',
  REMOVE_FILTER: 'HOUSE_FILTER_TYPES/REMOVE_FILTER',
  SELECT_ALL_VALUES: 'HOUSE_FILTER_TYPES/SELECT_ALL_VALUES',
  UNSELECT_ALL_VALUES: 'HOUSE_FILTER_TYPES/UNSELECT_ALL_VALUES',
  UPDATE_ALL: 'HOUSE_FILTER_TYPES/UPDATE_ALL',
};
export const OPERATION = {
  EQUAL: '=',
  BETWEEN: 'between',
  GREATER_THAN: '>',
  IN: 'in',
};
