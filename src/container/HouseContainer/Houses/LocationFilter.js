/* eslint-disable react/prop-types */
import React from 'react';
import { Button, Select, Checkbox } from 'antd';
import MetadataContainer from './MetadataContainer';

const CheckboxGroup = Checkbox.Group;

const LocationFilter = ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => {
  return (
    <div style={{ padding: 20 }}>
      <MetadataContainer
        render={options => {
          const { province, district } = options;
          return (
            <div>
              Huyện/TP
              <div>
                <Select
                  value={selectedKeys[0]}
                  placeholder="Chọn Huyện/TP"
                  onChange={value => setSelectedKeys(value ? [value] : [])}
                  style={{ width: 188, marginBottom: 8 }}
                >
                  {province.map(p => (
                    <Select.Option value={p}>{p}</Select.Option>
                  ))}
                </Select>
              </div>
              <div>
                Phường/Xã
                <div>
                  {selectedKeys[0] ? (
                    <CheckboxGroup
                      style={{ maxWidth: 350, marginBottom: 10 }}
                      placeholder="Chọn Phường"
                      options={selectedKeys[0] && district[selectedKeys[0]]}
                      value={selectedKeys[1]}
                      onChange={value => setSelectedKeys(value ? [selectedKeys[0], value] : [])}
                    />
                  ) : (
                    'Chọn Huyện/TP trước'
                  )}
                </div>
              </div>
            </div>
          );
        }}
      />
      <Button
        type="primary"
        onClick={() => {
          confirm();
        }}
        size="small"
        style={{ width: 90, marginRight: 8 }}
      >
        Search
      </Button>
      <Button
        onClick={() => {
          clearFilters();
          confirm();
        }}
        size="small"
        style={{ width: 90 }}
      >
        Reset
      </Button>
    </div>
  );
};

export default LocationFilter;
