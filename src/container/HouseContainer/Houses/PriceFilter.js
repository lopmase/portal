/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { Input, Button, Select } from 'antd';

const convertValue = type => (type === 1 ? 1000000000 : 1000000);

const PriceFilter = ({ setSelectedKeys, confirm, clearFilters }) => {
  const [unit, setUnit] = useState(1);
  const [range, setRange] = useState([]);

  const unitSelector = (
    <Select value={unit} onChange={value => setUnit(value)} style={{ width: 80 }}>
      <Select.Option value={1}>Tỷ</Select.Option>
      <Select.Option value={2}>Triệu</Select.Option>
    </Select>
  );
  let inputRef;
  return (
    <div style={{ padding: 8 }}>
      <div>Khoảng giá</div>
      <Input
        addonAfter={unitSelector}
        placeholder="Từ"
        value={range[0]}
        onChange={e => setRange(e.target.value ? [e.target.value, range[1]] : [])}
        onPressEnter={() => inputRef && inputRef.select()}
        style={{ width: 188, marginBottom: 8, display: 'block' }}
      />
      <Input
        ref={node => {
          inputRef = node;
        }}
        placeholder="Đến"
        value={range[1]}
        addonAfter={unitSelector}
        onChange={e => setRange(e.target.value ? [range[0], e.target.value] : [])}
        onPressEnter={() => {
          setSelectedKeys(range.map(val => val * convertValue(unit)));
          confirm();
        }}
        style={{ width: 188, marginBottom: 8, display: 'block' }}
      />
      <Button
        type="primary"
        onClick={() => {
          setSelectedKeys(range.map(val => val * convertValue(unit)));
          confirm();
        }}
        size="small"
        style={{ width: 90, marginRight: 8 }}
      >
        Search
      </Button>
      <Button
        onClick={() => {
          setRange([]);
          clearFilters();
          confirm();
        }}
        size="small"
        style={{ width: 90 }}
      >
        Reset
      </Button>
    </div>
  );
};

export default PriceFilter;
