/* eslint-disable prefer-destructuring */
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS, defaultTag } from '../../../constants/COMMON';
import { generateDatalistActions } from '../../../helpers/dataListAction';
import { waitForFinalEvent } from '../../../helpers/common';
import Selector from '../Selector';
import { exportExcel } from '../../Common/ExportExcel/action';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { deleteTag } from '../../../apis/houseApi';

const {
  fetching,
  fetchError,
  updateSort,
  search: searchAction,
  fetchPersonalDone,
  fetchNotApprovedDone,
  fetchApprovedDone,
  fetchWaitingApprovalDone,
  publicFetchDone,
  fetchSuspendListDone,
  allDone,
} = generateDatalistActions(REDUCERS.HOUSES);
const nextPage = () => async (dispatch, getState) => {
  dispatch(fetching());
  const {
    publicList,
    waitingApproval,
    approved,
    notApproved,
    personal,
    suspend,
    pageSize,
  } = Selector.houses(getState());
  const { fetchType } = Selector.houseGrouping(getState());
  if (fetchType === 'publicList') {
    dispatch(
      publicFetchDone(
        publicList.list,
        publicList.total,
        Math.ceil(publicList.total / pageSize),
        publicList.page + 1,
      ),
    );
  }
  if (fetchType === 'approved') {
    dispatch(
      fetchApprovedDone(
        approved.list,
        approved.total,
        Math.ceil(approved.total / pageSize),
        approved.page + 1,
      ),
    );
  }
  if (fetchType === 'waitingApproval') {
    dispatch(
      fetchWaitingApprovalDone(
        waitingApproval.list,
        waitingApproval.total,
        Math.ceil(waitingApproval.total / pageSize),
        waitingApproval.page + 1,
      ),
    );
  }
  if (fetchType === 'notApproved') {
    dispatch(
      fetchNotApprovedDone(
        notApproved.list,
        notApproved.total,
        Math.ceil(notApproved.total / pageSize),
        notApproved.page + 1,
      ),
    );
  }
  if (fetchType === 'personalList') {
    dispatch(
      fetchPersonalDone(
        personal.list,
        personal.total,
        Math.ceil(personal.total / pageSize),
        personal.page + 1,
      ),
    );
  }
  if (fetchType === 'suspend') {
    dispatch(
      fetchSuspendListDone(
        suspend.list,
        suspend.total,
        Math.ceil(suspend.total / pageSize),
        suspend.page + 1,
      ),
    );
  }
  dispatch(allDone());
};

const createFilterInput = state => {
  const { filters } = Selector.filters(state);
  const options = state.metadata[REDUCERS.OPTIONS].data;

  const filterInput = filters
    .slice()
    .filter(f => f.column)
    .map(f => {
      const { column, operation, values } = f;
      if (f.toValue) {
        const newValues = values.map(v => f.toValue(v, options));
        return { column, operation, values: newValues };
      }
      return { column, operation, values };
    });
  return filterInput;
};

export const getTag = () => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const tag = await thunkDependencies.house.getTag(getState());
    if (tag.data.length === 0) {
      tag.data = defaultTag;
    } else if (tag.data.length > 0 && tag.data.length < defaultTag.length) {
      const ids = new Set(tag.data.map(d => d.color));
      const merged = [...tag.data, ...defaultTag.filter(d => !ids.has(d.color))];
      tag.data = merged;
    }
    dispatch({ type: 'GET_TAG', tag: tag.data });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};

export const fetch = tagSearch => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const { fetchType } = Selector.houseGrouping(getState());
    let page;
    const {
      pageSize,
      sortColumn,
      sortOrder,
      search,
      publicList,
      waitingApproval,
      approved,
      notApproved,
      personal,
      suspend,
    } = Selector.houses(getState());

    if (fetchType === 'publicList') {
      page = publicList.page;
    } else if (fetchType === 'approved') {
      page = approved.page;
    } else if (fetchType === 'waitingApproval') {
      page = waitingApproval.page;
    } else if (fetchType === 'notApproved') {
      page = notApproved.page;
    } else if (fetchType === 'personalList') {
      page = personal.page;
    } else if (fetchType === 'suspend') {
      page = suspend.page;
    }
    const { status, permission } = Selector.houseGrouping(getState());
    const filterInput = createFilterInput(getState());

    const result = await thunkDependencies.house.listV2({
      page,
      size: pageSize,
      filters: filterInput,
      column: sortColumn,
      direction: sortOrder,
      search,
      status,
      mode: permission === 0 ? 'public' : 'private',
      tagSearch,
      fetchType,
    });
    if (page === 1) {
      result.data.waiting &&
        dispatch(
          fetchWaitingApprovalDone(
            result.data.waiting.data,
            result.data.waiting.total,
            Math.ceil(result.data.waiting.total / pageSize),
            page,
          ),
        );
      result.data.approved &&
        dispatch(
          fetchApprovedDone(
            result.data.approved.data,
            result.data.approved.total,
            Math.ceil(result.data.approved.total / pageSize),
            page,
          ),
        );
      result.data.notApproved &&
        dispatch(
          fetchNotApprovedDone(
            result.data.notApproved.data,
            result.data.notApproved.total,
            Math.ceil(result.data.notApproved.total / pageSize),
            page,
          ),
        );
      result.data.personal &&
        dispatch(
          fetchPersonalDone(
            result.data.personal.data,
            result.data.personal.total,
            Math.ceil(result.data.personal.total / pageSize),
            page,
          ),
        );
      result.data.public &&
        dispatch(
          publicFetchDone(
            result.data.public.data,
            result.data.public.total,
            Math.ceil(result.data.public.total / pageSize),
            page,
          ),
        );
      if (result.data.suspend) {
        dispatch(
          fetchSuspendListDone(
            result.data.suspend.data,
            result.data.suspend.total,
            Math.ceil(result.data.suspend.total / pageSize),
            page,
          ),
        );
      }

      dispatch(allDone(true));
    } else {
      result.data.public &&
        dispatch(
          publicFetchDone(
            publicList.list.concat(result.data.public.data),
            result.data.public.total,
            Math.ceil(result.data.public.total / pageSize),
            page,
          ),
        );
      result.data.personal &&
        dispatch(
          fetchPersonalDone(
            personal.list.concat(result.data.personal.data),
            result.data.personal.total,
            Math.ceil(result.data.personal.total / pageSize),
            page,
          ),
        );
      result.data.notApproved &&
        dispatch(
          fetchNotApprovedDone(
            notApproved.list.concat(result.data.notApproved.data),
            result.data.notApproved.total,
            Math.ceil(result.data.notApproved.total / pageSize),
            page,
          ),
        );
      result.data.approved &&
        dispatch(
          fetchApprovedDone(
            approved.list.concat(result.data.approved.data),
            result.data.approved.total,
            Math.ceil(result.data.approved.total / pageSize),
            page,
          ),
        );
      result.data.waitingApproval &&
        dispatch(
          fetchWaitingApprovalDone(
            waitingApproval.list.concat(result.data.waitingApproval.data),
            result.data.waitingApproval.total,
            Math.ceil(result.data.waitingApproval.total / pageSize),
            page,
          ),
        );
      result.data.suspend &&
        dispatch(
          fetchSuspendListDone(
            suspend.list.concat(result.data.suspend.data),
            result.data.suspend.total,
            Math.ceil(result.data.suspend.total / pageSize),
            page,
          ),
        );
      dispatch(allDone(true));
    }
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};

export const loadMore = () => async (dispatch, getState) => {
  const { total, publicList, fetching: isFetchinng } = Selector.houses(getState());
  if (isFetchinng) {
    return;
  }
  if (publicList.length >= total) {
    return;
  }
  await dispatch(nextPage());
  dispatch(fetch());
};

export const updatePage = page => async (dispatch, getState) => {
  const { fetchType } = Selector.houseGrouping(getState());
  const {
    publicList,
    waitingApproval,
    approved,
    notApproved,
    personal,
    suspend,
    pageSize,
  } = Selector.houses(getState());
  console.log('fetch', fetchType);
  if (fetchType === 'publicList') {
    dispatch(
      publicFetchDone(
        publicList.list,
        publicList.total,
        Math.ceil(publicList.total / pageSize),
        page,
      ),
    );
  }
  if (fetchType === 'approved') {
    dispatch(
      fetchApprovedDone(approved.list, approved.total, Math.ceil(approved.total / pageSize), page),
    );
  }
  if (fetchType === 'waitingApproval') {
    dispatch(
      fetchWaitingApprovalDone(
        waitingApproval.list,
        waitingApproval.total,
        Math.ceil(waitingApproval.total / pageSize),
        page,
      ),
    );
  }
  if (fetchType === 'notApproved') {
    dispatch(
      fetchNotApprovedDone(
        notApproved.list,
        notApproved.total,
        Math.ceil(notApproved.total / pageSize),
        page,
      ),
    );
  }
  if (fetchType === 'personalList') {
    dispatch(
      fetchPersonalDone(personal.list, personal.total, Math.ceil(personal.total / pageSize), page),
    );
  }
  if (fetchType === 'suspend') {
    dispatch(
      fetchSuspendListDone(suspend.list, suspend.total, Math.ceil(suspend.total / pageSize), page),
    );
  }
};
export const sortColumn = (columnName, order) => async dispatch => {
  dispatch(updateSort(columnName, order));
  dispatch(updatePage(1));
  dispatch(fetch());
};

export const doSearch = value => async dispatch => {
  dispatch(searchAction(value));
  dispatch(updatePage(1));
  waitForFinalEvent(() => dispatch(fetch()), 500, 'search houses');
};

export const exportExcelByFilter = () => async (dispatch, getState) => {
  try {
    const { sortColumn: column, sortOrder, search } = Selector.houses(getState());
    const { status, permission } = Selector.houseGrouping(getState());
    const filterInput = createFilterInput(getState());
    await dispatch(
      exportExcel('house', {
        filters: filterInput,
        column,
        direction: sortOrder,
        search,
        status,
        mode: permission === 0 ? 'public' : 'private',
      }),
    );
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const exportAllData = () => async dispatch => {
  try {
    await dispatch(exportExcel('house'));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

// delete tag
export const deleteHouseTag = (selections, reset) => async dispatch => {
  try {
    const noTagsList = selections.filter(value => value.house_tag.length === 0);
    if (noTagsList.length > 0) {
      dispatch(showMessage('Tồn tại bất động sản không có nhãn'));
    } else {
      await deleteTag(selections);
      dispatch(reset());
      dispatch(fetch());
    }
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

//to do

export const fetchAfterUpdate = tagSearch => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const { fetchType } = getState().house.HOUSE_DETAIL;
    let page;
    const {
      pageSize,
      // eslint-disable-next-line no-shadow
      sortColumn,
      sortOrder,
      search,
      publicList,
      waitingApproval,
      approved,
      notApproved,
      personal,
      suspend,
    } = Selector.houses(getState());

    if (fetchType === 'publicList') {
      page = publicList.page;
    } else if (fetchType === 'approved') {
      page = approved.page;
    } else if (fetchType === 'waitingApproval') {
      page = waitingApproval.page;
    } else if (fetchType === 'notApproved') {
      page = notApproved.page;
    } else if (fetchType === 'personalList') {
      page = personal.page;
    } else if (fetchType === 'suspend') {
      page = suspend.page;
    }
    const { status, permission } = Selector.houseGrouping(getState());
    const filterInput = createFilterInput(getState());

    const result = await thunkDependencies.house.listV2({
      page,
      size: pageSize,
      filters: filterInput,
      column: sortColumn,
      direction: sortOrder,
      search,
      status,
      mode: permission === 0 ? 'public' : 'private',
      tagSearch,
      fetchType,
    });
    if (page === 1) {
      result.data.waiting &&
        dispatch(
          fetchWaitingApprovalDone(
            result.data.waiting.data,
            result.data.waiting.total,
            Math.ceil(result.data.waiting.total / pageSize),
            page,
          ),
        );
      result.data.approved &&
        dispatch(
          fetchApprovedDone(
            result.data.approved.data,
            result.data.approved.total,
            Math.ceil(result.data.approved.total / pageSize),
            page,
          ),
        );
      result.data.notApproved &&
        dispatch(
          fetchNotApprovedDone(
            result.data.notApproved.data,
            result.data.notApproved.total,
            Math.ceil(result.data.notApproved.total / pageSize),
            page,
          ),
        );
      result.data.personal &&
        dispatch(
          fetchPersonalDone(
            result.data.personal.data,
            result.data.personal.total,
            Math.ceil(result.data.personal.total / pageSize),
            page,
          ),
        );
      result.data.public &&
        dispatch(
          publicFetchDone(
            result.data.public.data,
            result.data.public.total,
            Math.ceil(result.data.public.total / pageSize),
            page,
          ),
        );
      if (result.data.suspend) {
        dispatch(
          fetchSuspendListDone(
            result.data.suspend.data,
            result.data.suspend.total,
            Math.ceil(result.data.suspend.total / pageSize),
            page,
          ),
        );
      }

      dispatch(allDone(true));
    } else {
      result.data.public &&
        dispatch(
          publicFetchDone(
            publicList.list.concat(result.data.public.data),
            result.data.public.total,
            Math.ceil(result.data.public.total / pageSize),
            page,
          ),
        );
      result.data.personal &&
        dispatch(
          fetchPersonalDone(
            personal.list.concat(result.data.personal.data),
            result.data.personal.total,
            Math.ceil(result.data.personal.total / pageSize),
            page,
          ),
        );
      result.data.notApproved &&
        dispatch(
          fetchNotApprovedDone(
            notApproved.list.concat(result.data.notApproved.data),
            result.data.notApproved.total,
            Math.ceil(result.data.notApproved.total / pageSize),
            page,
          ),
        );
      result.data.approved &&
        dispatch(
          fetchApprovedDone(
            approved.list.concat(result.data.approved.data),
            result.data.approved.total,
            Math.ceil(result.data.approved.total / pageSize),
            page,
          ),
        );
      result.data.waitingApproval &&
        dispatch(
          fetchWaitingApprovalDone(
            waitingApproval.list.concat(result.data.waitingApproval.data),
            result.data.waitingApproval.total,
            Math.ceil(result.data.waitingApproval.total / pageSize),
            page,
          ),
        );
      result.data.suspend &&
        dispatch(
          fetchSuspendListDone(
            suspend.list.concat(result.data.suspend.data),
            result.data.suspend.total,
            Math.ceil(result.data.suspend.total / pageSize),
            page,
          ),
        );
      dispatch(allDone(true));
    }
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};
