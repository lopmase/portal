/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { Input, Button } from 'antd';

const AreaFilter = ({ setSelectedKeys, confirm, clearFilters, name }) => {
  const [range, setRange] = useState([]);
  let inputRef;
  return (
    <div style={{ padding: 8 }}>
      <div>{name}</div>
      <Input
        placeholder="Từ"
        value={range[0]}
        onChange={e => setRange(e.target.value ? [e.target.value, range[1]] : [])}
        onPressEnter={() => inputRef && inputRef.select()}
        style={{ width: 188, marginBottom: 8, display: 'block' }}
      />
      <Input
        ref={node => {
          inputRef = node;
        }}
        placeholder="Đến"
        value={range[1]}
        onChange={e => setRange(e.target.value ? [range[0], e.target.value] : [])}
        onPressEnter={() => {
          setSelectedKeys(range);
          confirm();
        }}
        style={{ width: 188, marginBottom: 8, display: 'block' }}
      />
      <Button
        type="primary"
        onClick={() => {
          setSelectedKeys(range);
          confirm();
        }}
        size="small"
        style={{ width: 90, marginRight: 8 }}
      >
        Search
      </Button>
      <Button
        onClick={() => {
          setRange([]);
          clearFilters();
          confirm();
        }}
        size="small"
        style={{ width: 90 }}
      >
        Reset
      </Button>
    </div>
  );
};

export default AreaFilter;
