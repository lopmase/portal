/* eslint-disable no-underscore-dangle */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import moment from 'moment';
import { Tag } from 'antd';
import _ from 'lodash';
import MetadataContainer from './MetadataContainer';
import { formatMoney } from '../../../helpers/common';
import { DIRECTIONS } from '../../../constants/DIRECTIONS';
import PriceFilter from './PriceFilter';
import AreaFilter from './areaFilter';
import LocationFilter from './LocationFilter';
import UserFilter from '../HousesFilter/UserFilter';
import 'antd/dist/antd.css';
import RowSelectionBox from './components/RowSelectionBox';

export const columns = [
  {
    title: '',
    dataIndex: 'selected',
    visible: true,
    width: 40,
    fixed: 'left',
    render: (value, item) => <RowSelectionBox item={item} />,
  },
  {
    title: 'Nhãn',
    dataIndex: 'house_tag',
    visible: true,
    width: 70,
    fixed: 'left',
    render: (value, item) =>
      value &&
      value[0] &&
      value[0].tag &&
      value[0].tag.color && <Tag color={value[0].tag.color}>{value[0].tag.name || 'TagName'}</Tag>,
    getFilter: option => option.tag && option.tag.map(t => ({ value: t.id, text: t.name })),
  },
  {
    title: '#',
    dataIndex: 'id',
    visible: true,
    sorter: true,
    width: 70,
    fixed: 'left',
    operation: 'like',
    filterDropdown: props => <UserFilter {...props} />,
    render: (value, item) => (
      <div style={{ padding: '5px' }}>
        <h4 style={{ textAlign: 'center' }}>{value}</h4>
      </div>
    ),
  },

  {
    title: 'Giá',
    dataIndex: 'into_money',
    visible: true,
    filterKey: 'into_money',
    fixed: 'left',
    width: 70,
    sorter: true,
    operation: 'between',
    filterDropdown: props => <PriceFilter {...props} />,
    render: (value, house) => (
      <span>
        <div className="price">{formatMoney(house.into_money)}</div>
      </span>
    ),
  },
  {
    title: 'Địa chỉ',
    dataIndex: 'house_number',
    visible: true,
    filterKey: 'address',
    width: 200,
    sorter: true,
    render: (value, house) => (
      <span>
        <div className="bold-primary-text long-text">
          {`${house.house_number} ${
            house.project && house.project.name
              ? `${house.house_address}, ${house.project.name}`
              : house.house_address
          }`}
        </div>
        <div>
          {house.public_approval === 1 && (
            <Tag style={{ fontSize: 11 }} color="#2db7f5">
              Cộng đồng
            </Tag>
          )}
          {house.web === 1 && (
            <Tag style={{ fontSize: 11 }} color="#87d068">
              Web
            </Tag>
          )}
          {house.public_approval === 0 && house.public === 1 && (
            <Tag style={{ fontSize: 11 }} color="#fc9803">
              Chờ duyệt cộng đồng
            </Tag>
          )}
        </div>
      </span>
    ),
  },
  {
    title: 'Diện tích',
    dataIndex: 'area',
    visible: true,
    sorter: true,
    filterDropdown: props => <AreaFilter {...props} name="Diện tích" />,
    render: (area, house) => (
      <>
        <div className="bold-primary-text">{`${house.area || 0} m2`}</div>
        <div className="small-text">{`${house.width || 0}m x ${house.length || 0}m`}</div>
      </>
    ),
    width: 140,
  },
  {
    title: 'Ngang',
    dataIndex: 'width',
    visible: true,
    sorter: true,
    filterDropdown: props => <AreaFilter {...props} name="Chiều ngang" />,
    render: (area, house) => (
      <>
        <div className="bold-primary-text">{`${house.width || 0} m`}</div>
      </>
    ),
    width: 100,
  },
  {
    title: 'Dài',
    dataIndex: 'length',
    visible: true,
    sorter: true,
    filterDropdown: props => <AreaFilter {...props} name="Chiều dài" />,
    render: (length, house) => (
      <>
        <div className="bold-primary-text">{`${house.length || 0} m`}</div>
      </>
    ),
    width: 90,
  },
  {
    title: 'Dự án',
    dataIndex: 'project_id',
    visible: true,
    ellipsis: true,
    sorter: true,
    filterKey: 'project_name',
    filterSearch: true,
    // onFilter: (value, record) => record.project && record.project.name.includes(value),
    getFilter: option => option.project && option.project.map(t => ({ value: t.id, text: t.name })),
    render: (value, house) => (house.project ? house.project.name : 'Không có'),
    width: 120,
  },

  {
    title: 'Block/Khu',
    dataIndex: 'block_section',
    ellipsis: true,
    sorter: true,
    render: (value, house) => (house.block_section ? house.block_section : 'Không có'),
    width: 80,
  },
  {
    title: 'Tầng/Lô',
    dataIndex: 'floor_lot',
    ellipsis: true,
    sorter: true,
    render: (value, house) => (house.floor_lot ? house.floor_lot : 'Không có'),
    width: 80,
  },
  {
    title: 'Khu vực',
    dataIndex: 'district',
    visible: true,
    filterKey: 'district_province',
    filterDropdown: props => <LocationFilter {...props} />,
    render: (value, house) => (
      <span>
        <div style={{ fontWeight: 'bold', fontSize: 13 }}>{house.province}</div>
        <div className="small-text">
          {house.district.split(' ').length === 2 ? house.district : house.district.slice(6)}
        </div>
      </span>
    ),
    width: 120,
  },
  {
    title: 'Loại BĐS',
    dataIndex: 'property_type',
    visible: true,
    getFilter: options => _.toPairs(options.type).map(pair => ({ value: pair[0], text: pair[1] })),
    render: (value, house) => (
      <MetadataContainer
        render={options => (
          <div style={{ fontSize: 13 }}>
            <div style={{ fontWeight: 'bold' }}>{options.type[house.property_type]}</div>
          </div>
        )}
      />
    ),
    width: 140,
  },
  {
    title: 'Loại nhà',
    dataIndex: 'house_type',
    visible: true,
    getFilter: options => _.toPairs(options.class).map(pair => ({ value: pair[0], text: pair[1] })),
    render: (value, house) => (
      <MetadataContainer
        render={options => (
          <div style={{ fontSize: 13 }}>
            <div>{options.class[house.house_type]}</div>
          </div>
        )}
      />
    ),
    width: 80,
  },

  {
    title: 'Diện tích sàn',
    dataIndex: 'floor_area',
    visible: true,
    sorter: true,
    render: (value, house) => `${house.floor_area || 0}m2`,
    width: 100,
  },
  {
    title: 'Pháp Lý',
    dataIndex: 'ownership',
    visible: true,
    getFilter: options =>
      _.toPairs(options.ownership).map(pair => ({ value: pair[0], text: pair[1] })),
    render: (value, house) => (
      <MetadataContainer
        render={options => (
          <div>
            {options && options.ownership ? options.ownership[house.ownership] : 'Chưa xác định'}
          </div>
        )}
      />
    ),
    width: 120,
  },
  {
    title: 'Số lầu',
    dataIndex: 'floors',
    visible: true,
    width: 80,
    sorter: true,
  },
  {
    title: 'Hướng',
    dataIndex: 'direction',
    visible: true,
    filters: DIRECTIONS.map(d => ({ text: d, value: d })),
    render: (value, house) => (
      <span>
        {house.house_direction
          ? house.house_direction.map(d => {
              const color = 'green';
              return (
                <Tag color={color} key={d.direction}>
                  {d.direction}
                </Tag>
              );
            })
          : 'Không xác định'}
      </span>
    ),
    width: 100,
  },
  {
    title: 'Hướng ban công',
    dataIndex: 'balcony_direction',
    visible: true,
    filters: DIRECTIONS.map(d => ({ text: d, value: d })),
    render: (value, house) => (
      <span>
        {house.house_balcony_direction
          ? house.house_balcony_direction.map(d => {
              const color = 'green';
              return (
                <Tag color={color} key={d.direction}>
                  {d.balcony}
                </Tag>
              );
            })
          : 'Không xác định'}
      </span>
    ),
    width: 150,
  },
  {
    title: 'Nở hậu',
    dataIndex: 'end_open',
    visible: true,
    width: 90,
    sorter: true,
    render: (value, house) => (house.end_open ? 'Có' : 'Không'),
  },
  {
    title: 'Lượt xem',
    dataIndex: 'total_view',
    visible: true,
    sorter: true,
    width: 100,
  },
  {
    title: 'Nhân viên',
    dataIndex: 'user_id',
    visible: true,
    sorter: true,
    operation: 'like',
    filterKey: 'user_name',
    filterDropdown: props => <UserFilter {...props} />,
    render: (value, house) => (house.user ? house.user.name : ''),
    width: 180,
  },
  {
    title: 'Khách hàng',
    dataIndex: 'customer_id',
    visible: true,
    operation: 'like',
    filterKey: 'customer_name',
    filterDropdown: props => <UserFilter {...props} />,
    sorter: true,
    render: (value, house) => (
      <span>
        <div style={{ fontWeight: 'bold', fontSize: 13 }}>{house.customerName}</div>
        <div className="small-text">{house.customer ? house.customer.phone_number : ''}</div>
      </span>
    ),
    width: 150,
  },
  // { title: 'Phường', dataIndex: 'district', hide: true, sorter: true },
  // { title: 'Huyện', dataIndex: 'province', hide: true, sorter: true },
  {
    title: 'Phí môi giới',
    dataIndex: 'brokerage_rate',
    sorter: true,
    width: 120,
    visible: true,
  },
  {
    title: 'Số phòng ngủ',
    dataIndex: 'number_bedroom',
    visible: true,
    sorter: true,
    width: 120,
  },
  {
    title: 'Loại đường',
    dataIndex: 'street_type',
    visible: true,
    sorter: true,
    getFilter: options =>
      _.toPairs(options.street_type).map(pair => ({ value: pair[0], text: pair[1] })),
    render: (value, house) => (
      <MetadataContainer
        render={options => (
          <div>
            {options && options.street_type
              ? options.street_type[house.street_type]
              : 'Chưa xác định'}
          </div>
        )}
      />
    ),
    width: 125,
  },
  {
    title: 'Đường rộng (m)',
    dataIndex: 'wide_street',
    visible: true,
    sorter: true,
    render: (value, house) => `${house.wide_street}m`,
    width: 125,
  },

  {
    title: 'Đăng ký ngày',
    dataIndex: 'created_at',
    visible: true,
    sorter: true,
    render: (value, house) => moment(house.created_at * 1000).format('DD/MM/YYYY HH:mm:ss'),
    width: 150,
  },
  {
    title: 'Lượt chia sẻ',
    dataIndex: 'postQuantity',
    visible: true,
    sorter: true,
    width: 120,
    render: (value, house) => house.postQuantity,
  },
  {
    title: 'Lượt mời chào',
    dataIndex: 'recommend_quantity',
    visible: true,
    width: 120,
    sorter: true,
    render: (value, house) => house.recommend_quantity,
  },
  {
    title: 'Lượt đi xem',
    dataIndex: 'seen_quantity',
    visible: true,
    width: 120,
    render: (value, house) => house.seen_quantity,
  },
  {
    title: 'Loại',
    dataIndex: 'purpose',
    visible: true,
    filterKey: 'purpose',
    fixed: 'left',
    width: 70,
    filters: [
      { text: 'Thuê', value: 1 },
      { text: 'Bán', value: 0 },
    ],
    render: (value, house) => (
      <span>
        <div className="purpose">{house.purpose === 1 ? 'THUÊ' : 'BÁN'}</div>
      </span>
    ),
  },
  // {
  //   title: 'Ngày cập nhật',
  //   dataIndex: 'updated_at',
  //   visible: true,
  //   sorter: true,
  //   render: (value, project) => moment(project.updated_at * 1000).format('DD/MM/YYYY HH:mm:ss'),
  // },
  // {
  //   title: 'Tuỳ chọn',
  //   dataIndex: 'actions',
  //   fixed: 'right',
  //   render: (value, project) => (
  //     <span>
  //       <a>Sửa</a>
  //       <Divider type="vertical" />
  //       <a>Xoá</a>
  //       <Divider type="vertical" />
  //       <a>Chia sẻ</a>
  //     </span>
  //   ),
  // },
];
