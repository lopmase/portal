import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Dropdown } from '../../../../components';
import { HOUSE_PERMISSION } from '../../../../constants/COMMON';
import Selector from '../../Selector';
import * as groupingAction from '../groupingAction';

const styleProps = {
  containerStyle: {
    minWidth: 90,
    borderRadius: 18,
    paddingTop: 8,
    paddingBottom: 8,
  },
};

const FilterByPermission = ({ permission, changePermission }) => (
  <div style={{ width: 150, display: 'inline-block' }}>
    <Dropdown
      {...styleProps}
      items={HOUSE_PERMISSION}
      selected={permission}
      onChanged={({ item }) => changePermission(item.value)}
    />
  </div>
);

FilterByPermission.propTypes = {
  permission: PropTypes.any.isRequired,
  changePermission: PropTypes.func.isRequired,
};

FilterByPermission.defaultProps = {};

export function mapStateToProps(state) {
  return {
    permission: Selector.houseGrouping(state).permission,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...groupingAction,
    },
    dispatch,
  );
}
export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(FilterByPermission);
