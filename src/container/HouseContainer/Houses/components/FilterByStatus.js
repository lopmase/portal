import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { Dropdown } from '../../../../components';
import { isAdmin } from '../../../../helpers/roleUtil';
import { HOUSE_STATUS } from '../../../../constants/COMMON';

const styleProps = {
  containerStyle: {
    minWidth: 90,
    borderRadius: 18,
    paddingTop: 8,
    paddingBottom: 8,
  },
};

const FilterByStatus = ({ selected, onChanged, permission, me }) => {
  const [houseStatus, setHouseStatus] = useState(HOUSE_STATUS);
  useEffect(() => {
    if (permission === 1 && !isAdmin(me)) {
      setHouseStatus([
        { key: 0, text: 'Mở bán', value: 0 },
        { key: 1, text: 'Ngưng bán', value: 1 },
      ]);
    }
  }, [permission, me]);
  return (
    <div style={{ width: 150, display: 'inline-block' }}>
      <Dropdown
        {...styleProps}
        items={houseStatus}
        selected={selected}
        onChanged={({ item }) => onChanged(item.value)}
      />
    </div>
  );
};

FilterByStatus.propTypes = {
  selected: PropTypes.any.isRequired,
  onChanged: PropTypes.func.isRequired,
  permission: PropTypes.func.isRequired,
  me: PropTypes.func.isRequired,
};

FilterByStatus.defaultProps = {};

export default FilterByStatus;
