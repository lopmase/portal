import React from 'react';
import PropTypes from 'prop-types';
import { Checkbox } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Selector from '../../Selector';
import { updateSelection } from '../groupingAction';

const RowSelectionBox = ({ selection, item, update }) => (
  <span style={{ display: 'flex', justifyContent: 'center' }}>
    <Checkbox
      checked={selection.map(h => h.id).includes(item.id)}
      onClick={event => {
        event.stopPropagation();
        update(item);
      }}
    />
  </span>
);

RowSelectionBox.propTypes = {
  selection: PropTypes.array.isRequired,
  item: PropTypes.any.isRequired,
  update: PropTypes.func.isRequired,
};

RowSelectionBox.defaultProps = {};

export function mapStateToProps(state) {
  return {
    selection: Selector.houseGrouping(state).selection,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      update: updateSelection,
    },
    dispatch,
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(RowSelectionBox);
