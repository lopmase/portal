/* eslint-disable react/prop-types */
import React from 'react';
import { Form, Input, Modal } from 'antd';
import { updateTag } from '../../../../apis/houseApi';

const TagModal = ({ isUpdateTag, handleToUpdateTag, tagValue, getTag, fetch }) => {
  const onFinish = async value => {
    try {
      const res = await updateTag({
        tagId: tagValue.id,
        tagName: value.tagName,
        color: tagValue.color,
      });
      if (res) {
        handleToUpdateTag();
        getTag();
        fetch();
      }
    } catch (err) {
      console.log('err', err);
    }
  };
  const [form] = Form.useForm();

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };
  return (
    <>
      <Modal
        visible={isUpdateTag}
        onCancel={() => handleToUpdateTag()}
        onOk={form.submit}
        style={window.innerWidth > 900 ? { minWidth: '800px' } : null}
      >
        <Form
          name="basic"
          labelCol={{ span: 16 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          autoComplete="off"
          layout="vertical"
          form={form}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Tên Nhãn"
            name="tagName"
            rules={[
              {
                required: true,
                message: 'Vui lòng nhập tên nhãn dán',
              },
            ]}
          >
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default TagModal;
