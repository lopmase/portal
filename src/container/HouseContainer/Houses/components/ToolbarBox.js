/* eslint-disable react/prop-types */
import React from 'react';
import { Popover } from 'antd';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { faPlus, faSearch, faCog, faTable } from '@fortawesome/free-solid-svg-icons';
import { bindActionCreators } from 'redux';

import Selector from '../../Selector';
import { resetSelection, toggleSelection, changeStatus } from '../groupingAction';
import * as actions from '../action';
import { ROUTES } from '../../../../constants/ROUTES';
import { Button } from '../../../../components';
import Excel from '../../../Common/ExportExcel/Excel';
import FilterByPermission from './FilterByPermission';
import FilterByStatus from './FilterByStatus';
import styles from '../../../../styles/_Colors.scss';

import './Toolbar.scss';

const ToolbarBox = ({
  selection,
  selectionFilter,
  deviceMode,
  exportExcelByFilter,
  exportAllData,
  changeHouseStatus,
  status,
  onShowSetting,
  reset,
  toggle,
  deleteHouseTag,
  permission,
  me,
}) => {
  const text = selectionFilter ? 'Hiển thị tất cả' : `Hiển thị ${selection.length} BĐS`;

  const content = (
    <div>
      <div style={{ marginBottom: 10 }}>
        <FilterByPermission />
      </div>
      <div>
        <FilterByStatus
          me={me.data}
          permission={permission}
          selected={status}
          onChanged={changeHouseStatus}
        />
      </div>
      <div style={{ marginTop: 10, width: 160 }}>
        <Button
          containerStyle={{ width: 160 }}
          iconColor="gray"
          icon={faTable}
          text="Tuỳ chỉnh bảng"
          backgroundColor="white"
          textColor="gray"
          small
          onClick={onShowSetting}
        />
      </div>
    </div>
  );
  return (
    <div className="house-toolbar">
      {selection.length > 0 ? (
        <div>
          <Button
            text="Gỡ nhãn"
            containerStyle={{ marginRight: 10 }}
            backgroundColor="white"
            textColor={styles.primaryColor}
            onClick={() => deleteHouseTag(selection, reset)}
          />
          <Button
            text="Bỏ chọn"
            containerStyle={{ marginRight: 10 }}
            backgroundColor="white"
            textColor={styles.primaryColor}
            onClick={reset}
          />
          <Button text={text} onClick={toggle} />
        </div>
      ) : (
        <div>
          {me.data && me.data.create_product_permission === 1 && (
            <Link to={ROUTES.NEW_HOUSE}>
              <Button
                containerStyle={{ paddingRight: 14, paddingLeft: 14 }}
                icon={faPlus}
                iconColor="white"
                iconOnly
              />
            </Link>
          )}
          {deviceMode && (
            <Button
              containerStyle={{ marginLeft: 10, paddingRight: 14, paddingLeft: 14 }}
              icon={faSearch}
              iconColor="gray"
              iconOnly
              backgroundColor="white"
            />
          )}
          <Excel onExportFiltered={exportExcelByFilter} onExportAll={exportAllData} />
          <Popover content={content} placement="bottomLeft" title="Tuỳ chỉnh" trigger="click">
            <Button
              containerStyle={{ marginLeft: 10, paddingRight: 13, paddingLeft: 13 }}
              icon={faCog}
              iconColor="gray"
              iconOnly
              backgroundColor="white"
            />
          </Popover>
        </div>
      )}
    </div>
  );
};

export function mapStateToProps(state) {
  return {
    selection: Selector.houseGrouping(state).selection,
    permission: Selector.houseGrouping(state).permission,
    selectionFilter: Selector.houseGrouping(state).selectionFilter,
    status: Selector.houseGrouping(state).status,
    me: Selector.me(state),
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      reset: resetSelection,
      toggle: toggleSelection,
      changeHouseStatus: changeStatus,
    },
    dispatch,
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(ToolbarBox);
