import { HOUSE_GROUP_TYPES } from './reducer';
import { fetch } from './action';
import { generateDatalistActions } from '../../../helpers/dataListAction';
import { REDUCERS } from '../../../constants/COMMON';

const { updatePage } = generateDatalistActions(REDUCERS.HOUSES);

export const updateStatus = status => ({
  type: HOUSE_GROUP_TYPES.SET_STATUS,
  status,
});

export const updatePermission = permission => ({
  type: HOUSE_GROUP_TYPES.SET_PERMISSION,
  permission,
});

export const updateSelection = item => ({
  type: HOUSE_GROUP_TYPES.UPDATE_SELECTION,
  item,
});

export const resetSelection = () => ({
  type: HOUSE_GROUP_TYPES.RESET_SELECTION,
});

export const toggleSelection = () => ({
  type: HOUSE_GROUP_TYPES.TOGGLE_SELECTION,
});

export const updateType = fetchType => ({
  type: HOUSE_GROUP_TYPES.SET_TYPE,
  fetchType,
});
// todo
export const updateKeyType = keyType => ({
  type: HOUSE_GROUP_TYPES.SET_KEY_TYPE,
  keyType,
});
export const changeStatus = status => async dispatch => {
  dispatch(updateStatus(status));
  dispatch(updatePage(1));
  dispatch(fetch());
};

export const changePermission = permission => async dispatch => {
  dispatch(updatePermission(permission));
  dispatch(updatePage(1));
  dispatch(fetch());
};

export const updateTabsType = type => async dispatch => {
  if (Number(type) === 1) {
    dispatch(updateType('publicList'));
    // dispatch(updateKeyType(type));
  } else if (Number(type) === 2) {
    dispatch(updateType('approved'));
    // dispatch(updateKeyType(type));
  } else if (Number(type) === 3) {
    dispatch(updateType('waitingApproval'));
    // dispatch(updateKeyType(type));
  } else if (Number(type) === 4) {
    dispatch(updateType('notApproved'));
    // dispatch(updateKeyType(type));
  } else if (Number(type) === 5) {
    dispatch(updateType('personalList'));
    dispatch(updateKeyType(type));
  } else if (Number(type) === 6) {
    dispatch(updateType('suspend'));
    // dispatch(updateKeyType(type));
  }
};
