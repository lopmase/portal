/* eslint-disable react/no-did-update-set-state */
/* eslint-disable react/no-unused-state */
/* eslint-disable prefer-destructuring */
/* eslint-disable react/prop-types */
import React, { PureComponent } from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'connected-react-router';
import { Input, Button as AntButton, Drawer, Tag, Divider, Tabs } from 'antd';
import { Flexbox, Label } from '../../../components';
import { REDUCERS } from '../../../constants/COMMON';
import * as actions from './action';
import { columns } from './columns';
import './Houses.scss';
import * as optionsAction from '../Metadata/optionsAction';
import * as filterAction from '../HouseFilter/action';
import * as groupingAction from './groupingAction';
import HouseDetail from '../HouseDetail/HouseDetail';
import { open, close, setHouseTypeFocus } from '../HouseDetail/action';
import SearchBox from './SearchBox';
import Selector from '../Selector';
import ExpensiveTable from '../../../components/ExpensiveTable/ExpensiveTable';
import { updateFilters } from './filterAction';
import { updateHouseTag } from '../../../apis/houseApi';
import TagModal from './components/TagModal';
import ToolbarBox from './components/ToolbarBox';
import { resetSelection } from './groupingAction';
import { showMessage } from '../../Common/GlobalErrorModal/action';

const { TabPane } = Tabs;
class Houses extends PureComponent {
  static propTypes = {};

  static defaultProps = {};

  state = {
    selectedItem: null,
    selectedRowId: null,
    isUpdateTag: false,
    deviceMode: window.innerWidth < 500,
    tagValue: null,
    searchTag: [],
    filterList: [],
    total: 0,
  };

  componentDidMount() {
    const { getTag, showError } = this.props;
    try {
      this.setState({
        tableHeight: window.innerHeight - (window.innerWidth < 500 ? 190 : 150),
      });

      const list = document.getElementsByClassName('ant-table-body');

      if (list && list[0]) {
        this.tableBody = list[0];
        if (this.tableBody.addEventListener) {
          this.tableBody.addEventListener('scroll', this.onTableScroll);
        }
      }
      this.getData();
      getTag();
    } catch (err) {
      showError('Something wentn wrong. Please try again!');
    }
  }

  componentDidUpdate(prevProps) {
    const { dataSource, fetchType, selection, selectionFilter } = this.props;
    const { publicList, notApproved, personal, approved, waitingApproval, suspend } = dataSource;

    if (selectionFilter) {
      if (fetchType === 'publicList') {
        this.setState({ filterList: selection, total: selection.length });
      } else if (fetchType === 'approved') {
        this.setState({ filterList: selection, total: selection.length });
      } else if (fetchType === 'waitingApproval') {
        this.setState({ filterList: selection, total: selection.length });
      } else if (fetchType === 'notApproved') {
        this.setState({ filterList: selection, total: selection.length });
      } else if (fetchType === 'personalList') {
        this.setState({ filterList: selection, total: selection.length });
      } else if (fetchType === 'suspend') {
        // prevProps.dataSource.suspend.list !== suspend.list
        this.setState({ filterList: selection, total: selection.length });
      }
    } else if (
      fetchType === 'publicList'
      //&&
      // prevProps.dataSource.publicList.list !== publicList.list
    ) {
      this.setState({ filterList: publicList.list, total: publicList.total });
    } else if (
      // (fetchType === 'approved' && prevProps.dataSource.approved.list !== approved.list)
      fetchType === 'approved'
    ) {
      this.setState({ filterList: approved.list, total: approved.total });
    } else if (
      fetchType === 'waitingApproval'
      // &&
      // prevProps.dataSource.waitingApproval.list.length === waitingApproval.list.length &&
      // prevProps.dataSource.waitingApproval.list.every(
      //   (value, index) => value === waitingApproval.list[index],
      // )
    ) {
      this.setState({ filterList: waitingApproval.list, total: waitingApproval.total });
    } else if (
      fetchType === 'notApproved'
      //&& prevProps.dataSource.notApproved.list !== notApproved.list
    ) {
      this.setState({ filterList: notApproved.list, total: notApproved.total });
    } else if (
      fetchType === 'personalList'
      // && prevProps.dataSource.personal.list !== personal.list
    ) {
      this.setState({ filterList: personal.list, total: personal.total });
    } else if (
      fetchType === 'suspend'
      //  &&
      // prevProps.dataSource.suspend.list.length === suspend.list.length &&
      // prevProps.dataSource.suspend.list.every((value, index) => value === suspend.list[index])
    ) {
      // prevProps.dataSource.suspend.list !== suspend.list
      this.setState({ filterList: suspend.list, total: suspend.total });
    }
  }

  componentWillUnmount() {
    const { updateTabsType } = this.props;
    if (this.tableBody) {
      this.tableBody.removeEventListener('scroll', this.onTableScroll);
    }
    updateTabsType(1);
  }

  updateTagForHouse = async values => {
    const { showError, reset } = this.props;
    if (!values.tag_name) {
      showError('Vui lòng đổi tên tag trước khi gắn nhãn');
    } else {
      const res = await updateHouseTag(values);
      res && this.getData();
    }
    reset();
  };

  getData = async (searchTag = null) => {
    const { fetch, getOptions, closeHouseDetail } = this.props;
    await getOptions();
    fetch(searchTag);

    closeHouseDetail();
  };

  onTableScroll = event => {
    const { currentTarget } = event;
    const { tableHeight } = this.state;
    const { loadMore, dataSource, selectionFilter } = this.props;
    const { fetching } = dataSource;
    if (fetching || selectionFilter) {
      return;
    }
    if (currentTarget.scrollHeight - currentTarget.scrollTop <= tableHeight + 50) {
      loadMore();
    }
  };

  getColumnFilter = column => {
    const { options, tag } = this.props;
    if (options && column.getFilter) {
      try {
        const filters = tag
          ? column.getFilter({ ...options, tag: tag.filter(val => val.id > 0) })
          : column.getFilter(options);

        if (Array.isArray(filters) && filters[0] && filters[0].value) {
          return { filters };
        }
      } catch (error) {
        return {};
      }
    }
    return {};
  };

  onRowSelect = ({ index, rowData }) => {
    const { openHouseDetail, closeHouseDetail } = this.props;
    this.setState({ selectedItem: rowData, selectedRowId: rowData.id });
    if (index >= 0) {
      openHouseDetail();
    } else {
      closeHouseDetail();
    }
  };

  setRowClassName = record => {
    const { selectedRowId } = this.state;
    return record.id === selectedRowId ? 'selectedRow' : '';
  };

  onTableChanged = options => {
    const { sortColumn, updateAllFilters } = this.props;
    if (options.sortChanged) {
      sortColumn(options.sortField, options.sortOrder);
    } else {
      updateAllFilters(options.filters);
    }
  };

  handleToUpdateTag = () => {
    this.setState({ isUpdateTag: false, tagId: null });
  };

  getColumnSearchProps = dataIndex =>
    dataIndex === 'house_number'
      ? {
          filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
              <Input
                ref={node => {
                  this.searchInput = node;
                }}
                placeholder="Tìm theo địa chỉ"
                value={selectedKeys[0]}
                onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                onPressEnter={() => confirm()}
                style={{ width: 188, marginBottom: 8, display: 'block' }}
              />
              <AntButton
                type="primary"
                onClick={() => confirm()}
                size="small"
                style={{ width: 90, marginRight: 8 }}
              >
                Search
              </AntButton>
              <AntButton
                onClick={() => {
                  clearFilters();
                  confirm();
                }}
                size="small"
                style={{ width: 90 }}
              >
                Reset
              </AntButton>
            </div>
          ),
          onFilterDropdownVisibleChange: visible => {
            if (visible && this.searchInput) {
              setTimeout(() => this.searchInput.select());
            }
          },
        }
      : {};

  showTableSetting = () => {
    const { tableSettingShowing } = this.state;
    this.setState({ tableSettingShowing: !tableSettingShowing });
  };

  render() {
    const {
      dataSource,
      doSearch,
      closeHouseDetail,
      detailVisible,
      options,
      selection,
      tag,
      fetch,
      getTag,
      updateTabsType,
      fetchType,
      selectionFilter,
      keyType,
      setHouseType,
    } = this.props;
    const { fetching, publicList } = dataSource;
    const {
      selectedItem,
      deviceMode,
      tableHeight,
      tableSettingShowing,
      isUpdateTag,
      tagValue,
      filterList,
      total,
    } = this.state;
    // const filterList = selectionFilter ? selection : publicList.list;
    const formattedColumns = !options
      ? []
      : [
          ...columns.map(col => ({
            ...(deviceMode ? { ...col, fixed: undefined } : col),
            ...this.getColumnFilter(col),
            ...this.getColumnSearchProps(col.dataIndex),
          })),
        ];
    return (
      <div className="houses-page" style={{ padding: deviceMode ? 10 : 20 }}>
        <div className="toolbar">
          <Flexbox row spaceBetween containerStyle={{ marginBottom: 20, alignItems: 'center' }}>
            <Flexbox row flexStart alignItems="center">
              <h1 style={{ marginRight: 10, marginBottom: 0 }}>Bất động sản</h1>
              <Label text={total} />
            </Flexbox>
            {!deviceMode && (
              <SearchBox
                containerStyle={{ maxWidth: 500, width: 300 }}
                doSearch={doSearch}
                data={dataSource}
              />
            )}
            <ToolbarBox deviceMode={deviceMode} onShowSetting={this.showTableSetting} />
          </Flexbox>

          <Divider orientation="left">Nhãn Bất Động Sản</Divider>
          {tag && tag.length > 0 && (
            <div style={{ padding: '5px' }}>
              {tag.map(value => (
                <Tag
                  onClick={() =>
                    selection.length > 0
                      ? this.updateTagForHouse({
                          tag_name: value.name,
                          tag_id: value.id || value.tag_id,
                          houses: selection,
                        })
                      : this.setState({ isUpdateTag: !isUpdateTag, tagValue: value })
                  }
                  key={value.tag_id}
                  color={value.color}
                >
                  {value.name || 'TagName'}
                </Tag>
              ))}
            </div>
          )}
        </div>
        {isUpdateTag && (
          <TagModal
            isUpdateTag={isUpdateTag}
            handleToUpdateTag={this.handleToUpdateTag}
            tagValue={tagValue}
            fetch={fetch}
            getTag={getTag}
          />
        )}
        <Tabs
          defaultActiveKey={keyType}
          onChange={async key => {
            const list = document.getElementsByClassName('ant-table-body');
            this.tableBody = list[0];

            this.tableBody.scrollTo(0, 0);

            await updateTabsType(key);
            await setHouseType(key);
            fetch();
          }}
        >
          <TabPane disabled={fetching} tab="Cộng đồng" key="1" />
          <TabPane disabled={fetching} tab="Đã duyệt" key="2" />
          <TabPane disabled={fetching} tab="Chờ duyệt" key="3" />
          <TabPane disabled={fetching} tab="Không được duyệt" key="4" />
          <TabPane disabled={fetching} tab="Sản phẩm cá nhân" key="5" />
          <TabPane disabled={fetching} tab="Cá nhân ngưng bán" key="6" />
        </Tabs>
        <ExpensiveTable
          id="houses"
          data={fetchType === 'publicList' && !selectionFilter ? publicList.list : filterList}
          toggleSetting={tableSettingShowing}
          columns={[...formattedColumns]}
          rowClick={this.onRowSelect}
          height={tableHeight}
          onChanged={this.onTableChanged}
          loading={fetching}
          rowClassName={this.setRowClassName}
        />
        <Drawer
          title="Chi tiết"
          placement="right"
          width={deviceMode ? window.innerWidth : '600px'}
          mask={false}
          maskClosable={false}
          bodyStyle={{ padding: 0 }}
          onClose={closeHouseDetail}
          visible={detailVisible}
          style={{ position: 'absolute', zIndex: 50 }}
        >
          <HouseDetail house={selectedItem || {}} />
        </Drawer>
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    dataSource: Selector.houses(state),
    filters: Selector.filters(state).filters,
    selectionFilter: Selector.houseGrouping(state).selectionFilter,
    selection: Selector.houseGrouping(state).selection,
    // fetchType: Selector.houseGrouping(state).fetchType,
    keyType: state.house[REDUCERS.HOUSE_DETAIL].keyTypeHouse,
    fetchType: state.house[REDUCERS.HOUSE_DETAIL].fetchType,
    options: state.metadata[REDUCERS.OPTIONS].data,
    detailVisible: state.house[REDUCERS.HOUSE_DETAIL].visible,
    tag: state.house[REDUCERS.HOUSES_GROUPING].tag,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      ...optionsAction,
      ...groupingAction,
      toggleFilter: filterAction.toggle,
      openHouseDetail: open,
      closeHouseDetail: close,
      pushRoute: push,
      updateAllFilters: updateFilters,
      reset: resetSelection,
      showError: showMessage,
      setHouseType: setHouseTypeFocus,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Houses);
