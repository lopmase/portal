import { HOUSE_STATUS, HOUSE_PERMISSION } from '../../../constants/COMMON';

export const HOUSE_GROUP_TYPES = {
  SET_STATUS: 'HOUSE_GROUP_TYPES/SET_STATUS',
  SET_PERMISSION: 'HOUSE_GROUP_TYPES/SET_PERMISSION',
  UPDATE_SELECTION: 'HOUSE_GROUP_TYPES/UPDATE_SELECTION',
  TOGGLE_SELECTION: 'HOUSE_GROUP_TYPES/TOGGLE_SELECTION',
  RESET_SELECTION: 'HOUSE_GROUP_TYPES/RESET_SELECTION',
  GET_TAG: 'GET_TAG',
  UPDATE_HOUSE_FILTER: 'UPDATE_HOUSE_FILTER',
  SET_TYPE: 'HOUSE_GROUP_TYPES/SET_TYPE',
  SET_KEY_TYPE: 'HOUSE_GROUP_TYPES/SET_KEY_TYPE',
};

const initial = {
  status: HOUSE_STATUS[0].value,
  permission: HOUSE_PERMISSION[0].value,
  selection: [],
  selectionFilter: false,
  fetchType: 'publicList',
  keyType: '1',
};

const groupHouseReducer = (state = initial, action) => {
  switch (action.type) {
    case HOUSE_GROUP_TYPES.SET_TYPE:
      return {
        ...state,
        fetchType: action.fetchType,
      };
    case HOUSE_GROUP_TYPES.SET_STATUS:
      return {
        ...state,
        status: action.status,
      };
    case HOUSE_GROUP_TYPES.SET_PERMISSION:
      return {
        ...state,
        permission: action.permission,
      };
    case HOUSE_GROUP_TYPES.UPDATE_SELECTION:
      const found = state.selection.map(item => item.id).includes(action.item.id);
      const newSelection = found
        ? state.selection.filter(s => s.id !== action.item.id)
        : [...state.selection, action.item];
      return {
        ...state,
        selection: newSelection,
      };

    case HOUSE_GROUP_TYPES.TOGGLE_SELECTION:
      return {
        ...state,
        selectionFilter: !state.selectionFilter,
      };
    case HOUSE_GROUP_TYPES.RESET_SELECTION:
      return {
        ...state,
        selection: [],
        selectionFilter: false,
      };
    case HOUSE_GROUP_TYPES.GET_TAG:
      return {
        ...state,
        tag: action.tag,
      };
    case HOUSE_GROUP_TYPES.SET_KEY_TYPE:
      return {
        ...state,
        keyType: action.keyType,
      };
    default:
      return state;
  }
};

export default groupHouseReducer;
