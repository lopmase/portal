/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { Button, Select, Checkbox } from 'antd';
import { searchByName } from '../../../apis/projectApi';
import { Input } from '../../../components';
import MetadataContainer from '../Houses/MetadataContainer';

const CheckboxGroup = Checkbox.Group;

const ProjectFilter = ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => {
  // const onSearch = async value => {
  //   try {
  //     const rs = await searchByName(value);
  //     if (rs && rs.data) {
  //       setOptions(rs.data.map(item => item.name));
  //     }
  //   } catch (error) {
  //     console.error(error);
  //   }
  // };

  return (
    <div style={{ padding: 8 }}>
      <MetadataContainer
        render={options => {
          const { project } = options;
          return (
            <div>
              Dự án
              <div>
                <Select
                  value={selectedKeys[0]}
                  placeholder="Chọn Huyện/TP"
                  onChange={value => setSelectedKeys(value ? [value] : [])}
                  style={{ width: 188, marginBottom: 8 }}
                >
                  {project.map(p => (
                    <Select.Option value={p.id}>{p.name}</Select.Option>
                  ))}
                </Select>
              </div>
              {/* <div>
                Phường/Xã
                <div>
                  {selectedKeys[0] ? (
                    <CheckboxGroup
                      style={{ maxWidth: 350, marginBottom: 10 }}
                      placeholder="Chọn Phường"
                      options={selectedKeys[0] && district[selectedKeys[0]]}
                      value={selectedKeys[1]}
                      onChange={value => setSelectedKeys(value ? [selectedKeys[0], value] : [])}
                    />
                  ) : (
                    'Chọn Huyện/TP trước'
                  )}
                </div>
              </div> */}
            </div>
          );
        }}
      />
      <Button
        type="primary"
        onClick={() => {
          confirm();
        }}
        size="small"
        style={{ width: 90, marginRight: 8 }}
      >
        Search
      </Button>
      <Button
        onClick={() => {
          clearFilters();
          confirm();
        }}
        size="small"
        style={{ width: 90 }}
      >
        Reset
      </Button>
    </div>
  );
};

export default ProjectFilter;
