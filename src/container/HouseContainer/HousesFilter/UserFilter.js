/* eslint-disable react/prop-types */
import React from 'react';
import { Input, Button } from 'antd';

const UserFilter = ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => {
  let inputRef;
  return (
    <div style={{ padding: 8 }}>
      <Input
        placeholder="Tìm theo tên"
        value={selectedKeys[0]}
        onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
        onPressEnter={() => inputRef && inputRef.select()}
        style={{ width: 188, marginBottom: 8, display: 'block' }}
      />
      <Button
        type="primary"
        onClick={() => {
          confirm();
        }}
        size="small"
        style={{ width: 90, marginRight: 8 }}
      >
        Search
      </Button>
      <Button
        onClick={() => {
          clearFilters();
          confirm();
        }}
        size="small"
        style={{ width: 90 }}
      >
        Reset
      </Button>
    </div>
  );
};

export default UserFilter;
