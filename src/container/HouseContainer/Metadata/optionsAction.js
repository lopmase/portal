import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateDataActions } from '../../../helpers/dataAction';

const { fetchDone, fetching, fetchError } = generateDataActions(REDUCERS.OPTIONS);

export const getOptions = () => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const response = await thunkDependencies.metadata.options();
    const result = apiSerializer(response);
    dispatch(fetchDone(result));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};
