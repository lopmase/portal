import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateDatalistActions } from '../../../helpers/dataListAction';

const { fetchDone, fetching, fetchError } = generateDatalistActions(REDUCERS.TIMEZONE);

export const listTimezone = () => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const response = await thunkDependencies.metadata.timezoneList();
    const result = apiSerializer(response);
    dispatch(fetchDone(result));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};
