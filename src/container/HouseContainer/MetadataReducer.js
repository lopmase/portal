import { combineReducers } from 'redux';
import createDatalistReducer from '../../helpers/createDataListReducer';
import createDataReducer from '../../helpers/createDataReducer';
import { REDUCERS } from '../../constants/COMMON';

export default combineReducers({
  [REDUCERS.STREETS]: createDatalistReducer(REDUCERS.STREETS),
  [REDUCERS.OPTIONS]: createDataReducer(REDUCERS.OPTIONS),
});
