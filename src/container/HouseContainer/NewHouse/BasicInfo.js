/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Editor } from 'react-draft-wysiwyg';
import { convertToRaw, EditorState, convertFromRaw, ContentState } from 'draft-js';

import { Card, FormInput, FormField, FormLabel } from '../../../components';

const BasicInfo = ({ editMode, form, handleChange, internal, changeFromAloNhaDat }) => {
  const [editorState, setEditorState] = useState(EditorState.createEmpty());
  const [internalDesctiptionEditor, setInternalDesctiptionEditor] = useState(
    EditorState.createEmpty(),
  );
  const [init, setInit] = useState(false);
  const [interalInitial, setInteralInitial] = useState(false);
  const { description, internalDescription } = form.data;
  const descriptionOnChange = editor => {
    const rawContent = convertToRaw(editor.getCurrentContent());
    setEditorState(editor);
    handleChange('description', JSON.stringify(rawContent));
  };
  const internalDescriptionOnChange = editor => {
    const rawContent = convertToRaw(editor.getCurrentContent());
    setInternalDesctiptionEditor(editor);
    handleChange('internalDescription', JSON.stringify(rawContent));
  };

  useEffect(() => {
    if (editMode && description !== undefined && !init) {
      setInit(true);
      let editor;
      try {
        if (!description) {
          editor = EditorState.createEmpty();
          return;
        }
        const jsonContent = JSON.parse(description);
        editor = EditorState.createWithContent(convertFromRaw(jsonContent));
      } catch (error) {
        editor = EditorState.createWithContent(ContentState.createFromText(description));
      } finally {
        setEditorState(editor);
      }
    }
    if (editMode && description !== undefined && changeFromAloNhaDat) {
      let editor;
      try {
        if (!description) {
          editor = EditorState.createEmpty();
          return;
        }
        const jsonContent = JSON.parse(description);
        editor = EditorState.createWithContent(convertFromRaw(jsonContent));
      } catch (error) {
        editor = EditorState.createWithContent(ContentState.createFromText(description));
      } finally {
        setEditorState(editor);
      }
    }
  }, [description]);
  useEffect(() => {
    if (editMode && internalDescription !== undefined && !interalInitial) {
      setInteralInitial(true);
      let editor;
      try {
        if (!internalDescription) {
          editor = EditorState.createEmpty();
          return;
        }
        const jsonContent = JSON.parse(internalDescription);
        editor = EditorState.createWithContent(convertFromRaw(jsonContent));
      } catch (error) {
        editor = EditorState.createWithContent(ContentState.createFromText(internalDescription));
      } finally {
        // setInteralInitial(false);
        setInternalDesctiptionEditor(editor);
      }
    }
  }, [internalDescription]);

  return (
    <Card shadow containerStyle={{ flex: 1, minWidth: 300, height: '100%' }}>
      <FormInput form={form} name="title" label="Tiêu đề" handleChange={handleChange} />
      <FormField
        name="description"
        form={form}
        handleChange={handleChange}
        render={() => (
          <>
            <FormLabel text="Mô tả" />
            <Editor
              editorState={editorState}
              wrapperStyle={{
                borderWidth: 1,
                borderRadius: 5,
                borderColor: 'rgba(34,36,38,.15)',
                borderStyle: 'solid',
                backgroundColor: 'white',
                minHeight: '100%',
              }}
              editorStyle={{ padding: 10 }}
              onEditorStateChange={descriptionOnChange}
              handlePastedText={() => false}
            />
          </>
        )}
      />
      {internal && (
        <FormField
          name="internalDescription"
          form={form}
          handleChange={handleChange}
          render={() => (
            <>
              <FormLabel text="Mô tả(Nội bộ)" />
              <Editor
                editorState={internalDesctiptionEditor}
                wrapperStyle={{
                  borderWidth: 1,
                  borderRadius: 5,
                  borderColor: 'rgba(34,36,38,.15)',
                  borderStyle: 'solid',
                  backgroundColor: 'white',
                  minHeight: '100%',
                }}
                editorStyle={{ padding: 10 }}
                onEditorStateChange={internalDescriptionOnChange}
                handlePastedText={() => false}
              />
            </>
          )}
        />
      )}
    </Card>
  );
};

BasicInfo.propTypes = {
  form: PropTypes.object.isRequired,
  editMode: PropTypes.object.isRequired,
  internal: PropTypes.bool,
  handleChange: PropTypes.func.isRequired,
};

BasicInfo.defaultProps = {
  internal: true,
};

export default BasicInfo;
