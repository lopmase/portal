import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Card, FormDropdown, Button, Spacer, Flexbox } from '../../../components';
import { REDUCERS } from '../../../constants/COMMON';
import { isAdmin } from '../../../helpers/roleUtil';
import { generateDatalistActions } from '../../../helpers/dataListAction';
import { fetch } from '../../CustomerConntainer/Customers/action';
import { generateModalActions } from '../../../helpers/modalAction';
import QuickNewCustomer from '../../CustomerConntainer/QuickNewCustomer/QuickNewCustomer';

const { search } = generateDatalistActions(REDUCERS.CUSTOMERS);
const { openModal } = generateModalActions(REDUCERS.QUICK_NEW_CUSTOMER_MODAL);

const CustomerInfo = ({
  form,
  handleChange,
  loadingUser,
  user,
  users,
  doSearch,
  loadCustomer,
  customers,
  openNewCustomerModal,
}) => {
  const searchCustomer = searchValue => {
    doSearch(searchValue);
    loadCustomer();
  };

  useEffect(() => {
    loadCustomer();
  }, [loadCustomer]);

  return (
    <Card shadow containerStyle={{ flex: 2 }}>
      {!loadingUser && isAdmin(user) && (
        <div style={{ flex: 1 }}>
          <FormDropdown
            name="user_id"
            label="Nhân viên"
            form={form}
            search
            handleChange={handleChange}
            items={users.map(u => ({ text: u.name, value: u.id, meta: u.phone_number }))}
          />
        </div>
      )}
      <Spacer />
      <Flexbox row containerStyle={{ alignItems: 'flex-end' }}>
        <div style={{ flex: 1 }}>
          <FormDropdown
            name="customer_id"
            search
            onSearch={searchCustomer}
            label="Khách hàng"
            placeholder="Tìm theo tên hoặc SĐT"
            require
            form={form}
            handleChange={handleChange}
            items={customers.map(c => ({ text: c.name, value: c.id, meta: c.phone_number }))}
          />
        </div>
        <Spacer />
        <div style={{ textAlign: 'center' }}>
          <Button
            containerStyle={{ marginTop: 10 }}
            text="+ Tạo"
            clear
            onClick={openNewCustomerModal}
          />
        </div>
      </Flexbox>
      <QuickNewCustomer
        onAfterCreated={newCustomer => {
          if (newCustomer) {
            doSearch(newCustomer.name);
            loadCustomer();
            handleChange('customer_id', newCustomer.id);
          }
        }}
      />
    </Card>
  );
};

CustomerInfo.propTypes = {
  form: PropTypes.object.isRequired,
  customers: PropTypes.array.isRequired,
  users: PropTypes.array.isRequired,
  user: PropTypes.any.isRequired,
  loadingUser: PropTypes.bool.isRequired,
  doSearch: PropTypes.func.isRequired,
  loadCustomer: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
  openNewCustomerModal: PropTypes.func.isRequired,
};

CustomerInfo.defaultProps = {};

export function mapStateToProps(state) {
  return {
    users: state.user[REDUCERS.USERS].list,
    user: state.auth.user.data,
    loadingUser: state.auth.user.loading,
    customers: state.customer[REDUCERS.CUSTOMERS].list,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      doSearch: search,
      loadCustomer: fetch,
      openNewCustomerModal: openModal,
    },
    dispatch,
  );
}

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(CustomerInfo);
