import React from 'react';
import PropTypes from 'prop-types';

import { Card, Flexbox, Uploader, Spacer } from '../../../components';

const ImageInfo = ({ form, handleChange }) => {
  const { internal_image: internal, public_image: publicImages } = form.data;
  return (
    <div className="images">
      <h3>Hình ảnh</h3>
      <Flexbox row alignItems="flex-start">
        <Card shadow containerStyle={{ flex: 1, minWidth: 300 }}>
          <h3>Nội bộ</h3>
          <Uploader
            onImageAdded={images => handleChange('internal_image', internal.concat(images))}
            onImageRemoved={index =>
              handleChange(
                'internal_image',
                internal.filter((_, i) => i !== index),
              )
            }
            onImageRotated={(index, rotation) =>
              handleChange(
                'internal_image',
                internal.map((p, ind) => {
                  if (ind === index) {
                    return { ...p, rotation };
                  }
                  return p;
                }),
              )
            }
            rotateVisible
            containerStyle={{ marginTop: 10 }}
            images={internal}
            id="internal"
          />
        </Card>
        <Spacer />
        <Card shadow containerStyle={{ flex: 1, minWidth: 300 }}>
          <h3>Công khai</h3>
          <Uploader
            onImageAdded={images => handleChange('public_image', publicImages.concat(images))}
            onImageRemoved={index =>
              handleChange(
                'public_image',
                publicImages.filter((_, i) => i !== index),
              )
            }
            onImageRotated={(index, rotation) =>
              handleChange(
                'public_image',
                publicImages.map((p, ind) => {
                  if (ind === index) {
                    return { ...p, rotation };
                  }
                  return p;
                }),
              )
            }
            rotateVisible
            containerStyle={{ marginTop: 10 }}
            images={publicImages}
            id="public"
          />
        </Card>
      </Flexbox>
    </div>
  );
};

ImageInfo.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
};

ImageInfo.defaultProps = {};

export default ImageInfo;
