import React from 'react';
import PropTypes from 'prop-types';

import { Card, Uploader } from '../../../components';

const InternalImageInfo = ({ form, handleChange }) => {
  const { internal_image: internal } = form.data;
  console.log('internal', internal);
  return (
    <div className="images">
      <Card shadow containerStyle={{ flex: 1, minWidth: 300 }}>
        <h3>Hình ảnh nội bộ</h3>
        <Uploader
          onImageAdded={images => handleChange('internal_image', internal.concat(images))}
          onImageRemoved={index =>
            handleChange(
              'internal_image',
              internal.filter((_, i) => i !== index),
            )
          }
          onImageRotated={(index, rotation) =>
            handleChange(
              'internal_image',
              internal.map((p, ind) => {
                if (ind === index) {
                  return { ...p, rotation };
                }
                return p;
              }),
            )
          }
          rotateVisible
          containerStyle={{ marginTop: 10 }}
          images={internal}
          id="internal"
        />
      </Card>
    </div>
  );
};

InternalImageInfo.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
};

InternalImageInfo.defaultProps = {};

export default InternalImageInfo;
