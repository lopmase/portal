import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { fetch } from '../../ProjectContainer/Projects/action';
import {
  Card,
  Flexbox,
  FormDropdown,
  FormInput,
  Spacer,
  FormAntDropdown,
} from '../../../components';
import { list } from '../../../apis/projectApi';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';

const LocationInfo = ({ form, handleChange, streets, showError, data }) => {
  const [projects, setProjects] = useState([]);
  const { province, district } = data || {};
  const fetchProjects = async () => {
    try {
      const response = await list({ size: 200 });
      const result = apiSerializer(response);
      setProjects(result.data);
    } catch (error) {
      const message = error.message || UNKNOWN_ERROR;
      showError(message);
    }
  };

  useEffect(() => {
    fetchProjects();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <Card shadow containerStyle={{ flex: 2, height: '100%' }}>
      <Flexbox row flexStart containerStyle={{}}>
        <div style={{ flex: 1 }}>
          <FormDropdown
            name="city"
            label="Tỉnh"
            form={form}
            // handleChange={handleChange}
            // TODO: NEED API
            items={[{ text: 'Bà Rịa - Vũng Tàu', value: 1 }]}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormDropdown
            name="province"
            label="Huyện/TP"
            form={form}
            handleChange={handleChange}
            items={province ? province.map(s => ({ text: s, value: s })) : []}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormDropdown
            name="district"
            label="Phường/Xã"
            form={form}
            handleChange={handleChange}
            items={
              form.data.province && district
                ? district[form.data.province].map(s => ({
                    text: s,
                    value: s,
                  }))
                : []
            }
          />
        </div>
      </Flexbox>
      <Flexbox row flexStart containerStyle={{}}>
        <div style={{ flex: 1 }}>
          <FormAntDropdown
            name="project_id"
            label="Dự án"
            form={form}
            search
            allowClear
            handleChange={handleChange}
            filterOption={(input, option) => {
              if (!option.props) {
                return true;
              }
              if (!option.props.children) {
                return true;
              }
              return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
            }}
            items={projects.map(p => ({ text: p.name, value: p.id }))}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormDropdown
            name="house_address"
            label="Tên đường"
            form={form}
            search
            handleChange={handleChange}
            items={streets.map(s => ({ text: s.street_name, value: s.street_name }))}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormInput name="house_number" label="Số nhà" form={form} handleChange={handleChange} />
        </div>
      </Flexbox>

      {form.data.project_id && (
        <Flexbox row flexStart containerStyle={{}}>
          <div style={{ flex: 1 }}>
            <FormInput
              name="block_section"
              label="Block/Khu"
              form={form}
              handleChange={handleChange}
            />
          </div>
          <Spacer />
          <div style={{ flex: 1 }}>
            <FormInput name="floor_lot" label="Tầng/Lô" form={form} handleChange={handleChange} />
          </div>
        </Flexbox>
      )}
      {/* TODO: NEED API CHANGED TO ADD THOSE FIELDS
      <Flexbox row flexStart containerStyle={{}}>
        <div style={{ flex: 1 }}>
          <FormInput
            name="project_block"
            label="Block/Khu"
            form={form}
            handleChange={handleChange}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormInput name="project_floor" label="Tầng/Lô" form={form} handleChange={handleChange} />
        </div>
      </Flexbox> */}
    </Card>
  );
};

LocationInfo.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  streets: PropTypes.array.isRequired,
  showError: PropTypes.func.isRequired,
  data: PropTypes.any.isRequired,
};

LocationInfo.defaultProps = {};

export function mapStateToProps(state) {
  return {
    streets: state.metadata[REDUCERS.STREETS].list,
    data: state.metadata[REDUCERS.OPTIONS].data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchProjects: fetch, showError: showMessage }, dispatch);
}

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(LocationInfo);
