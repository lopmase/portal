/* eslint-disable react/self-closing-comp */
/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable no-shadow */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { goBack } from 'connected-react-router';
import _ from 'lodash';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { Flexbox, Spacer, Button, FormRadioGroup } from '../../../components';
import { REDUCERS } from '../../../constants/COMMON';
import * as actions from './action';
import * as streetActions from '../Metadata/streetAction';
import * as optionsAction from '../Metadata/optionsAction';
import './NewHouse.scss';
import BasicInfo from './BasicInfo';
import CustomerInfo from './CustomerInfo';
import LocationInfo from './LocationInfo';
import PurposeInfo from './PurposeInfo';
import TypeInfo from './TypeInfo';
import SpecInfo from './SpecInfo';
import Description from '../../../components/Description/Description';
import InternalImageInfo from './InternalImageInfo';
import PublicImageInfo from './PublicImageInfo';
import OtherInfo from './OtherInfo';
import { fetch as fetchUsers } from '../../UserConntainer/Users/action';
import { generateDatalistActions } from '../../../helpers/dataListAction';
import { generateFormActions } from '../../../helpers/formAction';

const { updatePageSize } = generateDatalistActions(REDUCERS.USERS);
const { reset } = generateFormActions(REDUCERS.NEW_HOUSE);

class NewAuction extends Component {
  state = {};

  componentDidMount() {
    const {
      match,
      loadStreets,
      editMode,
      getOptions,
      fetchHouse,
      fetchUser,
      updatePageSize,
      resetForm,
    } = this.props;

    const { id } = match.params;
    editMode && fetchHouse(id);
    console.log('editMode', editMode)
    !editMode && resetForm();
    loadStreets();
    getOptions();
    updatePageSize(100);
    fetchUser();
  }

  componentWillUnmount() {
    const { resetForm } = this.props;
    resetForm();
  }
 
  render() {
    const { form, handleChange, editMode, submit, back, location } = this.props;
    const { goBackApprove } = location.state || {};
    const { match } = this.props;
    const { error } = form;
    const { title } = form.data;
    const isValid = _.values(error).filter(i => i.length).length === 0;
    return (
      <div className="newhouse-page">
        {editMode && <h2>Sửa bất động sản</h2>}
        {!editMode && <h2>Tạo bất động sản</h2>}
        {/* <Flexbox row spaceBetween containerStyle={{ marginBottom: 10 }}> */}
        <div className="header-newHouse">
          <Spacer />
          <PurposeInfo form={form} handleChange={handleChange} />
          <Button
            ortherClass
            iconOnly
            icon={faTimes}
            onClick={() => back()}
            containerStyle={{ paddingLeft: 15, paddingRight: 15 }}
          />
        </div>
        {/* </Flexbox> */}
        <div style={{}}>
          <Spacer />
          <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
            <div style={{ flex: 2 }}>
              <CustomerInfo form={form} handleChange={handleChange} />
            </div>
            <Spacer />
            <div style={{ flex: 5 }}>
              <LocationInfo form={form} handleChange={handleChange} />
            </div>
          </Flexbox>
          <Spacer />
          <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
            <div style={{ flex: 3 }}>
              <TypeInfo form={form} handleChange={handleChange} />
            </div>
            <Spacer />
            <div style={{ flex: 2 }}>
              <OtherInfo form={form} handleChange={handleChange} />
            </div>
          </Flexbox>
          <Spacer />
          <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
            <div style={{ flex: 5 }}>
              <SpecInfo form={form} handleChange={handleChange} />
              <Spacer />
              <Description editMode={editMode} form={form} handleChange={handleChange} />
              {/* <KeyWord form={form}></KeyWord> */}
            </div>
            <Spacer />
            <div style={{ flex: 3 }}>
              <BasicInfo editMode={editMode} form={form} handleChange={handleChange} />
            </div>
          </Flexbox>

          <Spacer />
          <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
            <div style={{ flex: 1 }}>
              {title && <InternalImageInfo form={form} handleChange={handleChange} />}
            </div>
            <Spacer />
            <div style={{ flex: 1 }}>
              {title && <PublicImageInfo form={form} handleChange={handleChange} />}
            </div>
          </Flexbox>
          <Spacer />
          {!editMode && (
            <div style={{ maxWidth: 450 }}>
              <FormRadioGroup
                name="public"
                label="Bạn có muốn chia sẻ sản phẩm ra cộng đồng sau khi tạo không?"
                {...{ form, handleChange }}
                options={[
                  { value: 1, text: 'Có' },
                  { text: 'Không', value: 0 },
                ]}
              />
            </div>
          )}
          <Spacer height={0} />
          <div className="footer">
            <Flexbox row spaceBetween>
              <Button clear text="Lưu nháp" />
              {/* <Button disabled={!isValid} onClick={() => submit(false, null, null)} text="Lưu" /> */}
              <Button
                disabled={!isValid}
                onClick={() => submit(editMode, match.params.id, goBackApprove)}
                text="Lưu"
              />
            </Flexbox>
          </div>
        </div>
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    form: state.house[REDUCERS.NEW_HOUSE],
    me: state.auth.user.data,
  };
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      ...streetActions,
      ...optionsAction,
      fetchUser: fetchUsers,
      updatePageSize,
      back: goBack,
      resetForm: reset,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(NewAuction);
