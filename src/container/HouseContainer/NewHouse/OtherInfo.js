/* eslint-disable prettier/prettier */
/* eslint-disable no-empty */
import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Card, Flexbox, FormInput, Spacer, FormDropdown } from '../../../components';
import { REDUCERS } from '../../../constants/COMMON';
import { fetch } from '../../ProjectContainer/Projects/action';
import { isAdmin } from '../../../helpers/roleUtil';
import { showMessage } from '../../Common/GlobalErrorModal/action';

const OtherInfo = ({ form, handleChange, loadingUser, user }) => {
  const { purpose } = form.data;

  return (
    <Card shadow>
      <Flexbox row flexStart containerStyle={{}}>
        <div style={{ flex: 1 }}>
          <FormInput
            inputProps={{ numberOnly: true }}
            name="into_money"
            label="Giá"
            form={form}
            handleChange={handleChange}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormDropdown
            name="money_unit"
            label="Đơn vị"
            form={form}
            search
            handleChange={handleChange}
            items={
              purpose === 0
                ? [
                    { text: 'Triệu', value: 'mil' },
                    { text: 'Tỷ', value: 'bil' },
                  ]
                : [
                    { text: 'Triệu/tháng', value: 'mpm' },
                    { text: 'Tỷ/tháng', value: 'bpm' },
                  ]
            }
          />
        </div>
      </Flexbox>
      <Flexbox row flexStart containerStyle={{}}>
        <div style={{ flex: 1 }}>
          <FormInput
            inputProps={{ numberOnly: true, suffix: ' VNĐ' }}
            name="brokerage_fee"
            label="Số tiền môi giới"
            form={form}
            handleChange={handleChange}
          />
        </div>
        <Spacer />
        {!loadingUser && isAdmin(user) && (
          <div style={{ flex: 1 }}>
            <FormInput
              name="brokerage_rate"
              label="Tỉ lệ môi giới"
              inputProps={{ numberOnly: true, suffix: '%' }}
              form={form}
              handleChange={handleChange}
            />
          </div>
        )}
      </Flexbox>
    </Card>
  );
};

OtherInfo.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  user: PropTypes.any.isRequired,
  loadingUser: PropTypes.bool.isRequired,
};

OtherInfo.defaultProps = {};

export function mapStateToProps(state) {
  return {
    users: state.user[REDUCERS.USERS].list,
    user: state.auth.user.data,
    loadingUser: state.auth.user.loading,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      fetchProjects: fetch,
      showError: showMessage,
    },
    dispatch,
  );
}

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(OtherInfo);
