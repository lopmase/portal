/* eslint-disable no-param-reassign */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { Card, Uploader } from '../../../components';
import { escapeUnicode } from '../../../helpers/formatter';

const PublicImageInfo = ({ form, handleChange }) => {
  const [imageName, setImageName] = useState('');
  const { public_image: publicImages, title } = form.data;
  const dispatch = useDispatch();
  useEffect(() => {
    if (form.data.public_image.length !== 0 && form.data.title == null) {
      dispatch(showMessage('Vui lòng nhập Tiêu đề'));
    } else {
      form.data.newName = imageName;
    }
  }, [form.data.public_image]);
  useEffect(() => {
    let newTitle = escapeUnicode(title);
    newTitle = newTitle.replaceAll(' ', '_');
    newTitle += `_${new Date().getUTCMilliseconds()}`;
    setImageName(newTitle);
  }, [title]);

  return (
    <div className="images">
      <Card shadow containerStyle={{ flex: 1, minWidth: 300 }}>
        <h3>Hình ảnh công khai</h3>
        <Uploader
          title={title}
          onImageAdded={images => handleChange('public_image', publicImages.concat(images))}
          onImageRemoved={index =>
            handleChange(
              'public_image',
              publicImages.filter((_, i) => i !== index),
            )
          }
          onImageRotated={(index, rotation) =>
            handleChange(
              'public_image',
              publicImages.map((p, ind) => {
                if (ind === index) {
                  return { ...p, rotation };
                }
                return p;
              }),
            )
          }
          rotateVisible
          containerStyle={{ marginTop: 10 }}
          images={publicImages}
          id="public"
        />
      </Card>
    </div>
  );
};

PublicImageInfo.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
};

PublicImageInfo.defaultProps = {};

export default PublicImageInfo;
