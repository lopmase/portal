import React from 'react';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';
import { Card, Flexbox, FormField, Spacer } from '../../../components';

const PurposeInfo = ({ form, handleChange }) => {
  return (
    <FormField
      name="purpose"
      label="Nhu cầu"
      form={form}
      render={({ value, name }) => (
        <div>
          <Flexbox row containerStyle={{ justifyContent: 'flex-start' }}>
            <div
              onClick={() => handleChange(name, 0)}
              className={`purpose-item${value === 0 ? ' active' : ''}`}
            >
              <div className="icon">
                <FontAwesomeIcon icon={faCheckCircle} color="#ff6c00" />
              </div>
              <Card shadow>
                <div>Bán</div>
              </Card>
            </div>
            <Spacer />
            <div
              onClick={() => handleChange(name, 1)}
              className={`purpose-item${value === 1 ? ' active' : ''}`}
            >
              <div className="icon">
                <FontAwesomeIcon icon={faCheckCircle} color="#ff6c00" />
              </div>
              <Card shadow>
                <div>Cho thuê</div>
              </Card>
            </div>
          </Flexbox>
        </div>
      )}
    />
  );
};

PurposeInfo.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
};

PurposeInfo.defaultProps = {};

export default PurposeInfo;
