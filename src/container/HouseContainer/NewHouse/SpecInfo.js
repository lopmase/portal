import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Select } from 'antd';
import {
  Card,
  Flexbox,
  Spacer,
  FormInput,
  FormLabel,
  FormField,
  FormDropdown,
  FormRadioGroup,
} from '../../../components';
import { REDUCERS } from '../../../constants/COMMON';
import * as TYPES from '../../../constants/NUMERIC_REQUEST';

const SpecInfo = ({ form, handleChange }) => {
  return (
    <Card shadow containerStyle={{ flex: 3 }}>
      <Flexbox row flexStart containerStyle={{}}>
        <div style={{ flex: 1 }}>
          <FormInput
            name="area"
            inputProps={{ numberOnly: true, suffix: 'm2' }}
            label="Diện tích đất/Tim đường"
            form={form}
            handleChange={handleChange}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormInput
            name="width"
            inputProps={{ numberOnly: true, suffix: 'm' }}
            label="Ngang"
            form={form}
            handleChange={handleChange}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormInput
            name="length"
            inputProps={{ numberOnly: true, suffix: 'm' }}
            label="Dài"
            form={form}
            handleChange={handleChange}
          />
        </div>
      </Flexbox>
      <Flexbox row flexStart containerStyle={{}}>
        <div style={{ flex: 1 }}>
          <FormInput
            name="floor_area"
            label="Diện tích sàn/Thông thuỷ"
            inputProps={{ numberOnly: true, suffix: 'm2' }}
            form={form}
            handleChange={handleChange}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormDropdown
            name="end_open"
            label="Nở hậu"
            form={form}
            handleChange={handleChange}
            items={[
              { text: 'Có', value: 1 },
              { text: 'Không', value: 0 },
            ]}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormInput
            require
            name="floors"
            inputProps={{ numberOnly: true }}
            label="Số lầu"
            form={form}
            handleChange={handleChange}
          />
        </div>
      </Flexbox>
      <Flexbox row flexStart containerStyle={{}}>
        <div style={{ flex: 2 }}>
          <FormField
            form={form}
            name="balcony_direction"
            render={({ value }) => (
              <div>
                <FormLabel require text="Hướng" />
                <Select
                  mode="multiple"
                  style={{ width: '100%', minWidth: 200 }}
                  placeholder="Chọn hướng ban công"
                  value={value}
                  clearIcon
                  onChange={v => handleChange('balcony_direction', v)}
                >
                  {TYPES.DIRECTION_TYPES.map(direction => (
                    <Select.Option value={direction.value}>{direction.text}</Select.Option>
                  ))}
                </Select>
              </div>
            )}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormInput
            name="number_bedroom"
            inputProps={{ numberOnly: true }}
            label="Số phòng ngủ"
            form={form}
            handleChange={handleChange}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormInput
            name="number_wc"
            inputProps={{ numberOnly: true }}
            label="Số WC"
            form={form}
            handleChange={handleChange}
          />
        </div>
      </Flexbox>

      <Flexbox row flexStart containerStyle={{}}>
        <div style={{ flex: 1 }}>
          <FormRadioGroup
            name="dining_room"
            label="Có nhà ăn không?"
            {...{ form, handleChange }}
            options={[
              { value: 0, text: 'Không' },
              { text: 'Có', value: 1 },
            ]}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormRadioGroup
            name="terrace"
            label="Có nhà sân thượng không?"
            {...{ form, handleChange }}
            options={[
              { value: 0, text: 'Không' },
              { text: 'Có', value: 1 },
            ]}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormRadioGroup
            name="kitchen"
            label="Có nhà bếp không?"
            {...{ form, handleChange }}
            options={[
              { value: 0, text: 'Không' },
              { text: 'Có', value: 1 },
            ]}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormRadioGroup
            name="car_parking"
            label="Có chổ để ôtô không?"
            {...{ form, handleChange }}
            options={[
              { value: 0, text: 'Không' },
              { text: 'Có', value: 1 },
            ]}
          />
        </div>
      </Flexbox>
    </Card>
  );
};

SpecInfo.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  // data: PropTypes.any.isRequired,
};

SpecInfo.defaultProps = {};

export function mapStateToProps(state) {
  return {
    data: state.metadata[REDUCERS.OPTIONS].data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(SpecInfo);
