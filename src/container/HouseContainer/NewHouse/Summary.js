import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Card, Flexbox, Spacer, FormLabel } from '../../../components';
import { REDUCERS } from '../../../constants/COMMON';
import { FIELDS } from '../../../constants/HOUSE_FIELDS';

const Summary = ({ form, data }) => {
  const { public: images, internal } = form.data;
  return (
    <Card shadow containerStyle={{ flex: 2 }}>
      <div className="images">
        <Flexbox row>
          <div style={{ flex: 1 }}>
            <h3>Hình nội bộ</h3>
            <Flexbox row flexStart>
              {internal.map(image => (
                <img
                  style={{ width: 70, height: 70, objectFit: 'cover' }}
                  alt=""
                  src={image.data || image.url}
                />
              ))}
            </Flexbox>
          </div>
          <Spacer />
          <div style={{ flex: 1 }}>
            <h3>Hình công khai</h3>
            <Flexbox row flexStart>
              {images.map(image => (
                <img
                  style={{ width: 70, height: 70, objectFit: 'cover' }}
                  alt=""
                  src={image.data || image.url}
                />
              ))}
            </Flexbox>
          </div>
        </Flexbox>
      </div>
      <Flexbox row flexStart containerStyle={{}}>
        {FIELDS.map(field => (
          <>
            <div style={{ flex: 1, minWidth: 120 }}>
              <FormLabel text={field.label} />
              <div>{field.format ? field.format(form.data, data) : form.data[field.property]}</div>
            </div>
            <Spacer />
          </>
        ))}

        {/* <div style={{ flex: 1 }}>
          <div style={{ flex: 1 }}>
            <FormLabel text="Tỉ lệ môi giới" />
            <div>{rate}</div>
          </div>
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormInput
            name="floors"
            inputProps={{ numberOnly: true }}
            label="Số lầu"
            form={form}
            handleChange={handleChange}
          />
        </div> */}
      </Flexbox>
    </Card>
  );
};

Summary.propTypes = {
  form: PropTypes.object.isRequired,
  data: PropTypes.any.isRequired,
};

Summary.defaultProps = {};

export function mapStateToProps(state) {
  return {
    data: state.metadata[REDUCERS.OPTIONS].data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Summary);
