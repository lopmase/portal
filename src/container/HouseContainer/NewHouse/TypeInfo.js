import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { Card, Flexbox, FormDropdown, Spacer, FormInput } from '../../../components';
import { REDUCERS } from '../../../constants/COMMON';

const TypeInfo = ({ form, handleChange, data }) => {
  const { class: houseType, type, ownership, direction, street_type: streetTypes } = data || {};
  return (
    <Card shadow containerStyle={{ flex: 2 }}>
      <Flexbox row flexStart containerStyle={{}}>
        <div style={{ flex: 1 }}>
          <FormDropdown
            name="property_type"
            label="Loại BĐS"
            form={form}
            search
            handleChange={handleChange}
            items={
              type ? _.entries(type).map(pair => ({ text: pair[1], value: Number(pair[0]) })) : []
            }
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormDropdown
            name="house_type"
            label="Loại Nhà"
            form={form}
            search
            require
            handleChange={handleChange}
            items={
              houseType
                ? _.entries(houseType).map(pair => ({ text: pair[1], value: Number(pair[0]) }))
                : []
            }
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormDropdown
            name="ownership"
            label="Pháp Lý"
            form={form}
            search
            handleChange={handleChange}
            items={
              ownership
                ? _.entries(ownership).map(pair => ({ text: pair[1], value: Number(pair[0]) }))
                : []
            }
          />
        </div>
      </Flexbox>
      <Flexbox row flexStart containerStyle={{}}>
        <div style={{ flex: 1 }}>
          <FormDropdown
            name="street_type"
            label="Loại đường"
            form={form}
            search
            handleChange={handleChange}
            items={
              streetTypes
                ? _.entries(streetTypes).map(pair => ({ text: pair[1], value: Number(pair[0]) }))
                : []
            }
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormInput
            name="wide_street"
            label="Đường rộng"
            inputProps={{ numberOnly: true, suffix: 'm' }}
            form={form}
            handleChange={handleChange}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormDropdown
            name="direction"
            label="Hướng"
            require
            form={form}
            search
            handleChange={handleChange}
            items={direction ? direction.map(s => ({ text: s, value: s })) : undefined}
          />
        </div>
      </Flexbox>
    </Card>
  );
};

TypeInfo.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  data: PropTypes.any.isRequired,
};

TypeInfo.defaultProps = {};

export function mapStateToProps(state) {
  return {
    streets: state.metadata[REDUCERS.STREETS].list,
    data: state.metadata[REDUCERS.OPTIONS].data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(TypeInfo);
