/* eslint-disable no-param-reassign */
import _ from 'lodash';
import { push, goBack } from 'connected-react-router';
import { toast } from 'react-toastify';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateFormActions } from '../../../helpers/formAction';
import { fetch } from '../Detail/action';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { uploadImages, rotateImages } from '../UploadJobs/action';
import { generateModalActions } from '../../../helpers/modalAction';
import { ROUTES } from '../../../constants/ROUTES';
import { fetchAfterUpdate } from '../Houses/action';
import Selector from '../Selector';
import {
  formatImageUrl,
  convertNumberIntoUnit,
  convertUnitIntoNumber,
  convertStringToHTML,
} from '../../../helpers/common';

export const {
  handleFieldChanged,
  submitting,
  submitDone,
  submitError,
  validateAll,
  reset,
  waiting,
  waitingDone,
} = generateFormActions(REDUCERS.NEW_HOUSE);
const { openModal, closeModal } = generateModalActions(REDUCERS.GLOBAL_LOADING);

export const handleChange = (name, value) => async dispatch => {
  dispatch(handleFieldChanged(name, value));
};

const houseFields = [
  'id',
  'area',
  'brokerage_fee',
  'brokerage_rate',
  'customer_id',
  'descriptions',
  'key_word',
  'description',
  'internalDescription',
  'key_word',
  'district',
  'end_open',
  'floor_area',
  'floors',
  'house_address',
  'house_direction',
  'house_balcony_direction',
  'house_number',
  'house_type',
  'image',
  'into_money',
  'length',
  'number_bedroom',
  'number_wc',
  'ownership',
  'project_id',
  'block_section',
  'floor_lot',
  'property_type',
  'province',
  'purpose',
  'status',
  'street_type',
  'title',
  'user_id',
  'wide_street',
  'width',
  'internal_image',
  'public_image',
  'public',
  'type_news',
  'commission',
  'type_news_day',
  'dining_room',
  'kitchen',
  'terrace',
  'car_parking',
  'post',
  'failed_quantity',
  'type_news_value',
];

export const fetchHouse = id => async (dispatch, getState) => {
  dispatch(openModal());
  dispatch(waiting());
  await dispatch(fetch(id));
  const house = getState().house[REDUCERS.HOUSE_DETAIL_DATA].data;
  dispatch(closeModal());
  houseFields.forEach(field => {
    if (field === 'house_direction') {
      dispatch(handleChange('direction', house[field][0] ? house[field][0].direction : null));
    } else if (field === 'house_balcony_direction') {
      dispatch(
        handleChange(
          'balcony_direction',
          house[field].map(item => item.balcony),
        ),
      );
    } else if (field === 'public_image' || field === 'internal_image') {
      const images = _.values(house.image[field === 'public_image' ? 'public' : 'internal']).map(
        img => ({
          ...img,
          url: formatImageUrl(img.main),
        }),
      );
      dispatch(handleChange(field, images));
    } else if (field === 'into_money') {
      const { purpose } = house;
      const [moneyOnly, moneyUnit] = convertNumberIntoUnit(house.into_money, purpose);
      dispatch(handleChange('into_money', moneyOnly));
      dispatch(handleChange('money_unit', moneyUnit));
    } else {
      dispatch(handleChange(field, house[field]));
    }
  });
  dispatch(waitingDone());
};

export const create = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { data } = getState().house[REDUCERS.NEW_HOUSE];
    let internalImageIds = [];
    let publicImageIds = [];
    dispatch(openModal());
    dispatch(submitting());

    if (data.internal_image && data.internal_image.length) {
      await dispatch(uploadImages(data.internal_image, data.newName));
      const internalJobs = getState().house[REDUCERS.UPLOAD_JOBS].jobs;
      internalImageIds = _.values(internalJobs.results)
        .filter(r => r.id)
        .map(r => r.id);
    }
    if (data.public_image && data.public_image.length) {
      await dispatch(uploadImages(data.public_image, data.newName));
      const publicJobs = getState().house[REDUCERS.UPLOAD_JOBS].jobs;
      publicImageIds = _.values(publicJobs.results)
        .filter(r => r.id)
        .map(r => r.id);
    }

    const form = {
      ...data,
      into_money: convertUnitIntoNumber(data.into_money, data.money_unit),
      internal_image: internalImageIds,
      public_image: publicImageIds,
      direction: data.direction ? [data.direction] : undefined,
    };
    const response = await thunkDependencies.house.new(form);
    const result = apiSerializer(response);
    dispatch(submitDone(result));
    dispatch(closeModal());
    dispatch(reset());
    dispatch(push(ROUTES.HOUSES));
    toast.success('Tạo mới BĐS thành công');
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
    dispatch(submitError(message));
  }
};

export const update = (houseId, goBackApprove) => async (dispatch, getState, thunkDependencies) => {
  try {
    const { data } = getState().house[REDUCERS.NEW_HOUSE];
    let internalImageIds = data.internal_image.filter(im => im.id).map(im => im.id);
    let publicImageIds = data.public_image.filter(im => im.id).map(im => im.id);
    const newInternalImages = data.internal_image.filter(im => !im.id);
    const newPublicImages = data.public_image.filter(im => !im.id);
    const rotateImageList = data.internal_image
      .filter(im => im.id && im.rotation)
      .concat(data.public_image.filter(im => im.id && im.rotation));

    if (newInternalImages.length) {
      await dispatch(uploadImages(newInternalImages, data.newName));
      const internalJobs = getState().house[REDUCERS.UPLOAD_JOBS].jobs;
      internalImageIds = internalImageIds.concat(
        _.values(internalJobs.results)
          .filter(r => r.id)
          .map(r => r.id),
      );
    }

    if (newPublicImages.length) {
      await dispatch(uploadImages(newPublicImages, data.newName));
      const publicJobs = getState().house[REDUCERS.UPLOAD_JOBS].jobs;
      publicImageIds = publicImageIds.concat(
        _.values(publicJobs.results)
          .filter(r => r.id)
          .map(r => r.id),
      );
    }

    if (rotateImageList.length) {
      await dispatch(rotateImages(rotateImageList));
    }

    const house = getState().house[REDUCERS.HOUSE_DETAIL_DATA].data;
    const changedData = {};
    _.forIn(data, (value, key) => {
      if (key === 'into_money') {
        value = convertUnitIntoNumber(data.into_money, data.money_unit);
      }
      if (value !== house[key]) {
        changedData[key] = value;
      }
    });
    changedData.resetCondition = true;
    changedData.house_id = houseId;
    changedData.internal_image = internalImageIds;
    changedData.public_image = publicImageIds;
    changedData.direction = data.direction ? [data.direction] : undefined;
    changedData.descriptions = data.descriptions ? data.descriptions : undefined;
    changedData.key_word = data.key_word ? data.key_word : undefined;
    changedData.initialDescription = data.description
      ? convertStringToHTML(data.description)
      : undefined;
    dispatch(submitting());
    console.log('changedData',changedData)
    const response = await thunkDependencies.house.update(changedData);
    const result = apiSerializer(response);
    dispatch(submitDone(result));
    if (goBackApprove) {
      dispatch(goBack());
    } else {
      dispatch(push(`${ROUTES.HOUSE_ROOT}/${houseId}`));
    }
    dispatch(reset());
    dispatch(fetchAfterUpdate());
    toast.success('Cập nhật BĐS thành công');
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
    dispatch(submitError(message));
  }
};

export const submit = (editMode, houseId, goBackApprove) => async (dispatch, getState) => {
  dispatch(validateAll());
  const { error } = Selector.newHouse(getState());
  const isValid = _.values(error).filter(i => i.length).length === 0;
  console.log('editMode', editMode)
  if (isValid) {
    if (editMode) {
      console.log('update')
      dispatch(update(houseId, goBackApprove));
    } else {
      console.log('create')

      dispatch(create());
    }
  }
};
