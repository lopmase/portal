import createFormReducer from '../../../helpers/createFormReducer';
import { REDUCERS } from '../../../constants/COMMON';

const initialValue = {
  id: 0,
  purpose: 0,
  internal_image: [],
  public_image: [],
  brokerage_rate: 1,
  public: true,
  house_number: '',
  house_address: '',
  province: null,
  wide_street: 0,
  district: null,
  customer_id: null,
  city: 1,
  balcony_direction: [],
  type_news: 0,
  commission: 0,
  type_news_day: 0,
  dining_room: 0,
  kitchen: 0,
  terrace: 0,
  car_parking: 0,
  post: 0,
  failed_quantity: 0,
  type_news_value: 0,
  addressPost: ['Alonhadat'],
};

const rules = {
  house_number: [['required', 'Số nhà bắt buộc']],
  house_address: [['required', 'Đường bắt buộc']],
  province: [['required', 'Huyện bắt buộc']],
  district: [['required', 'Phường bắt buộc']],
  street_type: [['required', 'Loại đường bắt buộc']],
  house_type: [['required', 'Loại nhà bắt buộc']],
  property_type: [['required', 'Loại bđs bắt buộc']],
  customer_id: [['required', 'Khách hàng bắt buộc']],
  direction: [['required', 'Hướng nhà bắt buộc']],
  floors: [['required', 'Số lầu bắt buộc']],
};

export default createFormReducer(REDUCERS.NEW_HOUSE, initialValue, rules);
