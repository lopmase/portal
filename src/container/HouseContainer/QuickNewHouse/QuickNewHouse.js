/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import { Modal, FormInput, Button, Flexbox, FormDropdown } from '../../../components';
import './QuickNewHouse.scss';
import { REDUCERS } from '../../../constants/COMMON';
import * as actions from './action';
import * as streetActions from '../Metadata/streetAction';
import * as optionsAction from '../Metadata/optionsAction';
import { generateModalActions } from '../../../helpers/modalAction';
import { fetch as fetchUsers } from '../../UserConntainer/Users/action';
import LocationInfo from '../NewHouse/LocationInfo';
import PurposeInfo from '../NewHouse/PurposeInfo';
import OtherInfo from '../NewHouse/OtherInfo';
import CustomerInfo from '../NewHouse/CustomerInfo';

const { closeModal } = generateModalActions(REDUCERS.QUICK_NEW_HOUSE_MODAL);

const QuickNewHouse = ({
  form,
  handleChange,
  quickCreateSubmit,
  modal,
  userId,
  loadStreets,
  getOptions,
  onAfterCreated,
  close,
  fetchUserList,
  data,
}) => {
  const { visible } = modal;
  const { submitting } = form;
  const { class: houseType, type, ownership, direction, street_type: streetTypes } = data || {};
  const handleCreate = async () => {
    console.log('handleCreate');
    const rs = await quickCreateSubmit(userId);
    console.log('res', rs);
    if (rs) {
      console.log('result');
      onAfterCreated(rs);
    }
  };

  useEffect(() => {
    fetchUserList();
    loadStreets();
    getOptions();
  }, []);
  return (
    <>
      <Modal
        visible={visible}
        onClose={close}
        loading={submitting}
        containerStyle={{ maxWidth: 400 }}
        renderModal={() => (
          <div className="new-house">
            <h2>Tạo bất động sản mới</h2>
            <PurposeInfo form={form} handleChange={handleChange} />
            <CustomerInfo form={form} handleChange={handleChange} />
            <LocationInfo form={form} handleChange={handleChange} />
            <OtherInfo form={form} handleChange={handleChange} />
            <FormInput
              require
              name="floors"
              inputProps={{ numberOnly: true }}
              label="Số lầu"
              form={form}
              handleChange={handleChange}
            />
            <FormDropdown
              name="direction"
              label="Hướng"
              require
              form={form}
              search
              handleChange={handleChange}
              items={direction ? direction.map(s => ({ text: s, value: s })) : undefined}
            />
            <FormDropdown
              name="street_type"
              label="Loại đường"
              form={form}
              search
              handleChange={handleChange}
              items={
                streetTypes
                  ? _.entries(streetTypes).map(pair => ({ text: pair[1], value: Number(pair[0]) }))
                  : []
              }
            />
            <FormDropdown
              name="property_type"
              label="Loại BĐS"
              form={form}
              search
              handleChange={handleChange}
              items={
                type ? _.entries(type).map(pair => ({ text: pair[1], value: Number(pair[0]) })) : []
              }
            />

            <FormDropdown
              name="house_type"
              label="Loại Nhà"
              form={form}
              search
              require
              handleChange={handleChange}
              items={
                houseType
                  ? _.entries(houseType).map(pair => ({ text: pair[1], value: Number(pair[0]) }))
                  : []
              }
            />
            <Flexbox row spaceBetween>
              <div />
              <Flexbox row>
                <Button clear text="Huỷ" onClick={close} />
                <Button text="Lưu" onClick={() => handleCreate()} />
              </Flexbox>
            </Flexbox>
          </div>
        )}
      />
    </>
  );
};
export function mapStateToProps(state) {
  return {
    form: state.house[REDUCERS.NEW_HOUSE],
    modal: state.house[REDUCERS.QUICK_NEW_HOUSE_MODAL],
    data: state.metadata[REDUCERS.OPTIONS].data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...streetActions,
      ...optionsAction,
      ...actions,
      close: closeModal,
      fetchUserList: fetchUsers,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(QuickNewHouse);
