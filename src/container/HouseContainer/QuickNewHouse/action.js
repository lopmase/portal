/* eslint-disable consistent-return */
import _ from 'lodash';
import { toast } from 'react-toastify';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateFormActions } from '../../../helpers/formAction';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { generateModalActions } from '../../../helpers/modalAction';

import Selector from '../Selector';
import { convertUnitIntoNumber } from '../../../helpers/common';

export const {
  handleFieldChanged,
  submitting,
  submitDone,
  submitError,
  validateAll,
  reset,
  waiting,
  waitingDone,
} = generateFormActions(REDUCERS.NEW_HOUSE);
const { closeModal } = generateModalActions(REDUCERS.QUICK_NEW_HOUSE_MODAL);

export const handleChange = (name, value) => async dispatch => {
  dispatch(handleFieldChanged(name, value));
};

export const quickCreateSubmit = userId => async (dispatch, getState, thunkDependencies) => {
  try {
    if (userId) {
      dispatch(handleChange('user_id', userId));
    }
    dispatch(validateAll());
    const { error } = Selector.newHouse(getState());
    const isValid = _.values(error).filter(i => i.length).length === 0;
    if (isValid) {
      const { data } = getState().house[REDUCERS.NEW_HOUSE];
      if (userId) {
        data.user_id = userId;
      }
      // dispatch(openModal());
      // dispatch(submitting());

      const form = {
        ...data,
        into_money: convertUnitIntoNumber(data.into_money, data.money_unit),
        direction: data.direction ? [data.direction] : undefined,
      };
      const response = await thunkDependencies.house.new(form);
      const result = apiSerializer(response);
      dispatch(submitDone(result));
      dispatch(closeModal());
      dispatch(reset());
      toast.success('Tạo mới BĐS thành công');
      return result;
    }
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
    dispatch(submitError(message));
    return false;
  }
};
