import { combineReducers } from 'redux';
import createDatalistReducer from '../../helpers/createDataListReducer';
import { REDUCERS } from '../../constants/COMMON';
import createDataReducer from '../../helpers/createDataReducer';
import newHouseReducer from './NewHouse/reducer';
import uploadJobReducer from './UploadJobs/reducer';
import filterReducer from './HouseFilter/reducer';
import detailReducer from './HouseDetail/reducer';
import createModalReducer from '../../helpers/createModalReducer';
import groupHouseReducer from './Houses/reducer';

export default combineReducers({
  [REDUCERS.HOUSES]: createDatalistReducer(REDUCERS.HOUSES, {
    pageSize: 20,
  }),
  [REDUCERS.GET_TAG]: createDataReducer(REDUCERS.GET_TAG),
  [REDUCERS.NEW_HOUSE]: newHouseReducer,
  [REDUCERS.UPCOMING]: createDatalistReducer(REDUCERS.UPCOMING),
  [REDUCERS.COMPLETED]: createDatalistReducer(REDUCERS.COMPLETED),
  [REDUCERS.HOUSE_DETAIL_DATA]: createDataReducer(REDUCERS.HOUSE_DETAIL_DATA),
  [REDUCERS.LOTS]: createDatalistReducer(REDUCERS.LOTS),
  [REDUCERS.UPLOAD_JOBS]: uploadJobReducer,
  [REDUCERS.HOUSE_FILTER]: filterReducer,
  [REDUCERS.HOUSES_GROUPING]: groupHouseReducer,
  [REDUCERS.HOUSE_DETAIL]: detailReducer,
  [REDUCERS.SHARE_HOUSE_ZALO_MODAL]: createModalReducer(REDUCERS.SHARE_HOUSE_ZALO_MODAL),
  [REDUCERS.QUICK_NEW_HOUSE_MODAL]: createModalReducer(REDUCERS.QUICK_NEW_HOUSE_MODAL),
});
