import { REDUCERS } from '../../constants/COMMON';

export default {
  houses: state => state.house[REDUCERS.HOUSES],
  detail: state => state.house[REDUCERS.HOUSE_DETAIL_DATA],
  filters: state => state.house[REDUCERS.HOUSE_FILTER],
  houseGrouping: state => state.house[REDUCERS.HOUSES_GROUPING],
  options: state => state.metadata[REDUCERS.OPTIONS],
  newHouse: state => state.house[REDUCERS.NEW_HOUSE],
  tag: state => state.house[REDUCERS.GET_TAG],
  me: state => state.auth.user,
};
