import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './action';

import { REDUCERS } from '../../../constants/COMMON';
import './ShareHouseZalo.scss';
import { Button, Flexbox, Modal, Checkbox, Input } from '../../../components';
import ZaloConnect from '../../SocialContainer/Zalo/ZaloConnect/ZaloConnect';
import { generateModalActions } from '../../../helpers/modalAction';

const { closeModal } = generateModalActions(REDUCERS.SHARE_HOUSE_ZALO_MODAL);

const ShareHouseZalo = ({ house, visible, close, postZalo }) => {
  const [step, setStep] = useState(1);
  const [message, setMessage] = useState('');
  const [sendPublicImage, setPublic] = useState(true);
  const [sendInternalImage, setInternal] = useState(false);
  // const [selectedFriends, setFriends] = useState([]);

  useEffect(() => {
    setStep(1);
    // setFriends([]);
    setPublic(true);
    setInternal(false);
    return () => {};
  }, [visible]);

  const send = () => {
    postZalo(house, message);
  };

  const renderStep = () => {
    switch (step) {
      case 1:
        return (
          <ZaloConnect>
            <div>Nội dung bài viết</div>
            <Input
              multiline
              value={message}
              onChange={(name, value) => setMessage(value)}
              placeholder="Điền nội dung"
            />
            <Flexbox row spaceBetween containerStyle={{ marginTop: 10 }}>
              <div />
              <Button disabled={!message} text="Đăng ngay" onClick={send} />
            </Flexbox>
          </ZaloConnect>
        );
      case 3:
        return (
          <div>
            <div style={{ fontSize: 13, fontWeight: 'bold', marginBottom: 5 }}>
              Tuỳ chỉnh ảnh kèm theo
            </div>
            <Checkbox
              checked={sendPublicImage}
              onChange={checked => setPublic(checked)}
              containerStyle={{ marginBottom: 10 }}
              label="Kèm ảnh công khai"
            />
            <Checkbox
              checked={sendInternalImage}
              onChange={checked => setInternal(checked)}
              label="Kèm ảnh nội bộ"
            />
            <Flexbox row spaceBetween>
              <div />
              <Button text="Gửi ngay" onClick={send} />
            </Flexbox>
          </div>
        );

      default:
        return <div />;
    }
  };

  return (
    <Modal
      closeBtn
      containerStyle={{ maxWidth: 400 }}
      visible={visible}
      onClose={close}
      renderModal={() => (
        <div className="share-zalo" style={{ padding: 20 }}>
          <h3>Đăng BĐS lên Zalo</h3>
          <div>{renderStep()}</div>
        </div>
      )}
    />
  );
};

ShareHouseZalo.propTypes = {
  house: PropTypes.any,
  visible: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  postZalo: PropTypes.func.isRequired,
};

ShareHouseZalo.defaultProps = {
  house: {},
};

export function mapStateToProps(state) {
  return {
    visible: state.house[REDUCERS.SHARE_HOUSE_ZALO_MODAL].visible,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      close: closeModal,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(ShareHouseZalo);
