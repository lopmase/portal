import { generateModalActions } from '../../../helpers/modalAction';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { REDUCERS } from '../../../constants/COMMON';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';

const { openModal, closeModal } = generateModalActions(REDUCERS.GLOBAL_LOADING);

export const postZalo = (house, message = '') => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(openModal());
    await thunkDependencies.zalo.postZalo({ house_id: house.id, message });
    dispatch(closeModal());
  } catch (error) {
    const errorMessage = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(errorMessage));
  }
};
