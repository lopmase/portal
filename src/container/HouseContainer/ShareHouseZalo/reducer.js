/* eslint-disable prefer-destructuring */
import { HOUSE_DETAIL_TYPES } from './types';

const initial = {
  visible: false,
};

const detailReducer = (state = initial, action) => {
  switch (action.type) {
    case HOUSE_DETAIL_TYPES.OPEN:
      return {
        ...state,
        visible: true,
      };
    case HOUSE_DETAIL_TYPES.CLOSE:
      return {
        ...state,
        visible: false,
      };
    default:
      return state;
  }
};

export default detailReducer;
