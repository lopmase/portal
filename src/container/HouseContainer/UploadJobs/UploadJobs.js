/* eslint-disable react/no-array-index-key */
/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Modal } from '../../../components';
import { REDUCERS } from '../../../constants/COMMON';
import * as actions from './action';
import './Modal.scss';
import { STATUS } from './reducer';

class UploadJobs extends Component {
  state = {};

  render() {
    const { modal, closeModal, list, jobs } = this.props;
    const { visible } = modal;
    const { images } = jobs;
    return (
      <Modal
        visible={visible}
        onClose={closeModal}
        renderModal={() => (
          <div className="upload-jobs">
            <h2>Uploading images</h2>
            {list.map((image, index) => (
              <div className="photo" key={`image-${index}`}>
                <img alt="" src={image.data} />
                {images[index] === STATUS.LOADING && <div className="upload">Uploading...</div>}
                {images[index] === STATUS.DONE && <div className="done">Done!</div>}
                {images[index] === STATUS.ERROR && <div className="error">Error!</div>}
                {!images[index] && <div>Waiting</div>}
              </div>
            ))}
          </div>
        )}
      />
    );
  }
}

export function mapStateToProps(state) {
  return {
    modal: state.auction[REDUCERS.UPLOAD_JOBS].modal,
    jobs: state.auction[REDUCERS.UPLOAD_JOBS].jobs,
    list: state.auction[REDUCERS.NEW_LOT].data.images,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(UploadJobs);
