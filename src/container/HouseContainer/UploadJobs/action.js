/* eslint-disable no-param-reassign */
import { REDUCERS } from '../../../constants/COMMON';
import { generateModalActions } from '../../../helpers/modalAction';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { TYPES } from './TYPES';

export const { openModal, closeModal } = generateModalActions(REDUCERS.UPLOAD_JOBS);

const startUpload = index => ({
  type: TYPES.UPLOADING,
  index,
});
const uploadDone = (index, result) => ({
  type: TYPES.DONE,
  index,
  result,
});
const uploadError = (index, message) => ({
  type: TYPES.ERROR,
  index,
  message,
});

export const uploadImages = (images, newName) => async (dispatch, getState, thunkDependencies) => {
  const imageJobs = images.map(
    (image, index) =>
      new Promise(resolve => {
        dispatch(startUpload(index));
        thunkDependencies.house
          .upload(image.file, image.rotation, newName)
          .then(response => {
            try {
              const result = apiSerializer(response);
              dispatch(uploadDone(index, result));
              resolve({ data: result });
            } catch (error) {
              const message = error.message || UNKNOWN_ERROR;
              throw new Error(message);
            }
          })
          .catch(error => {
            dispatch(uploadError(index, error.message || UNKNOWN_ERROR));
            resolve({ error: error.message || UNKNOWN_ERROR });
          });
      }),
  );
  const results = await Promise.all(imageJobs);
  const ids = results.filter(i => i.data).map(i => i.data.id);
  return ids;
};

export const rotateImages = images => async (dispatch, getState, thunkDependencies) => {
  const imageJobs = images.map(
    (image, index) =>
      new Promise(resolve => {
        dispatch(startUpload(index));
        thunkDependencies.house
          .rotateImage(image.id, image.rotation)
          .then(response => {
            try {
              const result = apiSerializer(response);
              resolve({ data: result });
            } catch (error) {
              const message = error.message || UNKNOWN_ERROR;
              throw new Error(message);
            }
          })
          .catch(error => {
            resolve({ error: error.message || UNKNOWN_ERROR });
          });
      }),
  );
  const results = await Promise.all(imageJobs);
  return results;
};
