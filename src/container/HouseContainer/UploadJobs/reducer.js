import { combineReducers } from 'redux';
import { TYPES } from './TYPES';
import createModalReducer from '../../../helpers/createModalReducer';
import { REDUCERS } from '../../../constants/COMMON';

export const STATUS = {
  DONE: 2,
  LOADING: 1,
  ERROR: 3,
};

const initial = {
  images: {},
  error: {},
  results: {},
};
const uploadReducer = (state = initial, action) => {
  switch (action.type) {
    case TYPES.UPLOADING:
      return {
        ...state,
        images: { ...state.images, [action.index]: STATUS.LOADING },
        error: {},
        results: {},
      };
    case TYPES.DONE:
      return {
        ...state,
        images: { ...state.images, [action.index]: STATUS.DONE },
        results: { ...state.results, [action.index]: action.result },
      };
    case TYPES.ERROR:
      return {
        ...state,
        images: { ...state.images, [action.index]: STATUS.ERROR },
        error: { ...state.error, [action.index]: action.message },
      };
    default:
      return state;
  }
};

export default combineReducers({
  jobs: uploadReducer,
  modal: createModalReducer(REDUCERS.UPLOAD_JOBS),
});
