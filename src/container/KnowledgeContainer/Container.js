import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import { ROUTES } from '../../constants/ROUTES';
import { PrivateRoute } from '../../components';
import NotFound from '../Common/NotFound';
import Knowledge from './Knowledge/index';
// import AlonhadatPostList from './Post/AlonhadatPostList';

class KnowledgeContainer extends Component {
  state = {};

  render() {
    return (
      <Switch>
        <PrivateRoute exact path={ROUTES.KNOWLEDGE_ROOT} component={Knowledge} />
        <Route component={NotFound} />
      </Switch>
    );
  }
}

export default KnowledgeContainer;
