/* eslint-disable react/prop-types */
import React from 'react';
import { Card } from 'antd';
import { Draggable } from 'react-beautiful-dnd';
import { useDispatch } from 'react-redux';
import { setDataDetail } from './action';
import avatar from '../../../../assets/images/avt2.png';

const { Meta } = Card;

const CardContent = ({ setIsContentVisible, item, index, me }) => {
  const dispatch = useDispatch();
  console.log('test', me.role !== 1 || me.role !== 2);
  const showContent = () => {
    dispatch(setDataDetail(item));
    setIsContentVisible(true);
  };
  return (
    <Draggable draggableId={item.key} index={index} isDragDisabled={me && (me.role !== 1 && me.role !== 2)}>
      {(provided, snapshot) => {
        return (
          <div
            style={{ width: '100%', marginTop: 200 }}
            onClick={showContent}
            ref={provided.innerRef}
            snapshot={snapshot}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            key={item.key}
          >
            <Card
              hoverable
              style={{
                width: '100%',
              }}
              className="body-item-scrollable"
              cover={
                <img
                  height={100}
                  alt="example"
                  src={Object.keys(item.coverImageUrls).length !== 0 && item.coverImageUrls[0].main}
                />
              }
            >
              <Meta title={item.title} />
              <div className="userImagePost">
                <img src={item.user && item.user.avatar ? item.user.avatar.thumbnail : avatar }/>
              </div>
            </Card>
          </div>
        );
      }}
    </Draggable>
  );
};

export default CardContent;
