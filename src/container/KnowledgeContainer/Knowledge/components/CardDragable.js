import React, { useEffect, useRef, useState } from 'react';
import { Droppable, Draggable } from 'react-beautiful-dnd';
import { showMessage } from '../../../Common/GlobalErrorModal/action';
import { Collapse } from 'antd';
import { useDispatch } from 'react-redux';
import CardContent from './CardContent';
import _ from 'lodash';
import { Col, Row, Button } from 'antd';
import { v4 as uuidv4 } from 'uuid';
// eslint-disable-next-line react/prop-types
const { Panel } = Collapse;
const CardDragable = ({
  showModal,
  setIsContentVisible,
  elements,
  prefix,
  columnId,
  me,
  index,
  lastPage,
  page,
  getWithPage,
  setListData,
}) => {
  const columnRef = useRef(null);
  const [currentPage, setCurrentPage] = useState(Number(page));
  const [isDraggable, setIsDraggable] = useState(false);
  const dispatch = useDispatch();
  // useEffect(()=> {
  //   if(me) {
  //     setIsDraggable(me.role !== 1 && me.role !== 2)
  //   }

  // },[me])
  console.log('currentPage' + `${prefix}`, currentPage);
  console.log('page' + `${prefix}`, page);
  console.log('lastPage' + `${prefix}`, lastPage);

  const getDataWithPage = () => {
    // setCurrentPage(prev => prev + 1);

    const id = columnRef.current.getAttribute('id');
    const newPage = currentPage ? currentPage + 1 : page + 1;
    if (newPage > lastPage) {
      console.log('asdsad');
      dispatch(showMessage('Không còn bài viết nào'));
      return;
    }
    setCurrentPage(newPage);
    getWithPage(id, newPage).then(res => {
      setListData(prev => {
        return {
          ...prev,
          [prefix]: prev[prefix].concat(res.data),
        };
      });
      // console.log('elements', elements.concat(res.data));
    });
  };
  // console.log('newData', newData);
  // useEffect(() => {
  //   columnRef.current.addEventListener('scroll', event => {
  //     const maxScroll = event.target.scrollHeight - event.target.clientHeight;
  //     const currentScroll = event.target.scrollTop;
  //     console.log('lastPage', lastPage);
  //     console.log('currentScroll', currentScroll);

  //     if (Math.floor(currentScroll) >= maxScroll - 50 && currentPage <= lastPage) {
  //       const newPage = currentPage + 1;
  //       const id = event.target.getAttribute('id');
  //       getWithPage(id, newPage, prefix).then(res => {

  //       });
  //       setCurrentPage(newPage);
  //     }
  //   });
  //   return () => {
  //     columnRef.current.removeEventListener('scroll', {});
  //   };
  // });
  return (
    <Draggable draggableId={`${prefix}`} index={index} isDragDisabled={true}>
      {provided => (
        <div
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          ref={provided.innerRef}
          key={uuidv4()}
          style={{ height: 'fit-content' }}
        >
          <Collapse defaultActiveKey={['1']} className="gutterGroup">
            <Panel header={prefix} key="1">
              <Droppable droppableId={`${prefix}`}>
                {provided => (
                  <div
                    className="gutterCol"
                    id={columnId}
                    style={{ width: '100%' }}
                    ref={columnRef}
                  >
                    <div
                      {...provided.droppableProps}
                      ref={provided.innerRef}
                      className="knowLedgeContainer"
                    >
                      {/* <h2 style={{ color: 'white' }}>{prefix}</h2> */}
                      {elements.length &&
                        elements.map((item, index) => (
                          <>
                            <CardContent
                              me={me}
                              key={item.id}
                              item={item}
                              index={index}
                              setIsContentVisible={setIsContentVisible}
                            />
                          </>
                        ))}
                      <div className="knowLedge_item-button">
                        <button
                          className="buttonAdd btn-hover color-blue"
                          onClick={e => showModal(e, columnId)}
                          name="post"
                        >
                          Thêm thẻ
                        </button>
                        <button
                          className="buttonAdd btn-hover color-pink btn_move"
                          onClick={() => getDataWithPage()}
                          // name="post"
                          // disabled={currentPage == lastPage || page == lastPage}
                          style={
                            currentPage == lastPage || page == lastPage
                              ? { pointerEvents: 'none', opacity: 0.5 }
                              : {}
                          }
                        >
                          Load more
                        </button>
                      </div>
                    </div>
                  </div>
                )}
              </Droppable>
            </Panel>
          </Collapse>
        </div>
      )}
    </Draggable>
  );
};

export default CardDragable;
