/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
import React, { useState, useEffect, useCallBack } from 'react';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';
import PropTypes from 'prop-types';
import { useDispatch, connect } from 'react-redux';
import { Form, Input, Modal } from 'antd';
import moment from 'moment';
import { REDUCERS } from '../../../../constants/COMMON';
import { generateModalActions } from '../../../../helpers/modalAction';
import { showMessage } from '../../../Common/GlobalErrorModal/GlobalErrorModal';
import { Button, Flexbox, Dropdown, Uploader } from '../../../../components';
import * as action from './action';
import _ from 'lodash';
import { Editor } from 'react-draft-wysiwyg';
import { convertStringToHTML, formatImageUrl } from '../../../../helpers/common';
import { convertToRaw, EditorState, convertFromRaw } from 'draft-js';
const { TextArea } = Input;
// eslint-disable-next-line react/prop-types
const NewModal = ({
  backgroundColor,
  label,
  name,
  title,
  type,
  isModalVisible,
  setIsModalVisible,
  create,
  typeCreate,
  createColumn,
  fetchKnowledge,
  fetchType,
  editMode,
  data,
  updateKnowledge,
  reset,
}) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [image, setImage] = useState([]);
  const [coverImage, setCoverImage] = useState([]);

  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const [editorState, setEditorState] = useState(EditorState.createEmpty());
  const [content, setContent] = useState();

  // eslint-disable-next-line no-shadow
  const onEditorStateChange = editorState => {
    // const { handleChange } = this.props;
    const rawContent = convertToRaw(editorState.getCurrentContent());
    setEditorState(editorState);
    setContent(JSON.stringify(rawContent));
    // handleChange('content', JSON.stringify(rawContent));
  };

  console.log('editMode', editMode);
  console.log('content', content);

  useEffect(() => {
    if (editMode && type === 'post') {
      const images = _.values(data.imageUrls).map(p => ({
        ...p,
        url: formatImageUrl(p.main),
      }));

      const coverImage = _.values(data.coverImageUrls).map(p => ({
        ...p,
        url: formatImageUrl(p.main),
      }));

      setImage(images);
      setCoverImage(coverImage);
      const jsonContent = JSON.parse(data.content);
      const editor = EditorState.createWithContent(convertFromRaw(jsonContent));
      setEditorState(editor);
      setContent(data.content);
      form.setFieldsValue({
        title: data.title,
        content: editor,
      });
      // setIsModalVisible(true);
    }
  }, [editMode]);

  const handleCancel = () => {
    form.resetFields();
    setIsModalVisible(false);
    reset();
    // dispatch({ type: 'RESET_TYPE' });
  };

  const onFinish = values => {
    if (type === 'post' && !editMode) {
      values.images = image;
      values.cover_image = coverImage;
      values.type_id = typeCreate;
      values.content = content;
      create(values);
    } else if (type === 'post' && editMode) {
      values.images = image;
      values.cover_image = coverImage;
      values.content = content;
      values.type_id = data.type_id;
      values.id = data.id;
      updateKnowledge(values);
      reset();
    } else {
      createColumn(values);
    }
    setIsModalVisible(false);
  };
  return (
    <div style={{ margin: 10 }}>
      <Modal
        title={
          type && type === 'column' ? 'Thêm cột' : editMode ? 'Chỉnh sửa bài viết' : 'Thêm bài viết'
        }
        visible={isModalVisible}
        onCancel={handleCancel}
        onOk={form.submit}
        okButtonProps={{ style: { backgroundColor: '#ff8731' } }}
      >
        <div>
          <Form
            name="basic"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            layout="vertical"
            form={form}
          >
            <Flexbox row spaceBetween>
              {type && type === 'column' ? (
                <Form.Item
                  style={{ width: '100%' }}
                  label={type === 'column' ? label.column : label.title}
                  // eslint-disable-next-line react/prop-types
                  name={type === 'column' ? 'type_name' : 'title'}
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng nhập tên cột',
                    },
                  ]}
                >
                  <Input required />
                </Form.Item>
              ) : (
                <Form.Item
                  style={{ width: '100%' }}
                  label={label.title}
                  // eslint-disable-next-line react/prop-types
                  name="title"
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng nhập tiêu đề',
                    },
                  ]}
                >
                  <TextArea rows={4} required />
                </Form.Item>
              )}
            </Flexbox>

            {type && type === 'post' && (
              <>
                <Flexbox row spaceBetween>
                  <Form.Item style={{ width: '45%' }} label="Chọn ảnh bìa" name="cover_image">
                    <Uploader
                      title={title}
                      single
                      onImageAdded={images => setCoverImage(prev => prev.concat(images))}
                      onImageRemoved={index =>
                        setCoverImage(prev => prev.filter((_, i) => i !== index))
                      }
                      // onImageRotated={(index, rotation) =>
                      //   handleChange(
                      //     'public_image',
                      //     publicImages.map((p, ind) => {
                      //       if (ind === index) {
                      //         return { ...p, rotation };
                      //       }
                      //       return p;
                      //     }),
                      //   )
                      // }
                      rotateVisible
                      containerStyle={{ marginTop: 10 }}
                      images={coverImage}
                      id="coverImage"
                    />
                  </Form.Item>
                  <Form.Item style={{ width: '45%' }} label="Chọn hình ảnh" name="images">
                    <Uploader
                      title={title}
                      onImageAdded={images => setImage(prev => prev.concat(images))}
                      onImageRemoved={index => setImage(prev => prev.filter((_, i) => i !== index))}
                      // onImageRotated={(index, rotation) =>
                      //   handleChange(
                      //     'public_image',
                      //     publicImages.map((p, ind) => {
                      //       if (ind === index) {
                      //         return { ...p, rotation };
                      //       }
                      //       return p;
                      //     }),
                      //   )
                      // }
                      rotateVisible
                      containerStyle={{ marginTop: 10 }}
                      images={image}
                      id="post"
                    />
                  </Form.Item>
                </Flexbox>
                <Flexbox row spaceBetween>
                  <Form.Item
                    style={{ width: '100%' }}
                    label="Nội dung bài đăng"
                    // eslint-disable-next-line react/prop-types
                    name="content"
                    rules={[
                      {
                        required: true,
                        message: 'Vui lòng nhập nội dung',
                      },
                    ]}
                  >
                    <Editor
                      editorState={editorState}
                      wrapperStyle={{
                        borderWidth: 1,
                        borderRadius: 5,
                        borderColor: 'rgba(34,36,38,.15)',
                        borderStyle: 'solid',
                        backgroundColor: 'white',
                      }}
                      handlePastedText={() => false}
                      editorStyle={{ padding: 10 }}
                      onEditorStateChange={onEditorStateChange}
                    />
                  </Form.Item>
                </Flexbox>
              </>
            )}
          </Form>
        </div>
      </Modal>
    </div>
  );
};
NewModal.defaultProps = {
  title: ' ',
  field: {},
  backgroundColor: '#ff8731',
  name: {},
  label: {
    title: 'Tiêu đề',
    column: 'Cột',
  },
  message: {
    error: 'Someting was wrong, please try agian!',
  },
};
export function mapStateToProps(state) {
  return {
    // transactionBill: state.transactionBill.data,
    // dataUpdate: state.transactionBill.dataUpdate,
    // typeCreate: state.Knowledge.typeKnowledge.typeCreate,
    editMode: state.Knowledge.knowledgeList.editMode,
    data: state.Knowledge.knowledgeList.data,
    state,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...action,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(NewModal);
