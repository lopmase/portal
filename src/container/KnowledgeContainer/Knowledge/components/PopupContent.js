/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
import React, { useState, useEffect, useCallBack } from 'react';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { useDispatch, connect } from 'react-redux';
import { Modal } from 'antd';
import _ from 'lodash';
import ImageLightbox from '../../../../components/LightBox/Lightbox';
import { convertStringToHTML, formatImageUrl } from '../../../../helpers/common';
import Comments from './Comments';
import * as action from './action';
// eslint-disable-next-line react/prop-types
const PopupContent = ({
  backgroundColor,
  label,
  name,
  title,
  isContentVisible,
  setIsContentVisible,
  data,
  addKnowledgeComment,
  setIsModalVisible,
  setEditMode,
  me,
}) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [lightboxVisible, setLightboxVisible] = useState(false);
  const [imageIndex, setImageIndex] = useState(0);
  // eslint-disable-next-line react/prop-types
  const { imageUrls, content, coverImageUrls } = data;
  const [htmlContent, setHtml] = useState(null);
  const thumbnails = _.values(imageUrls).map(p => formatImageUrl(p.thumbnail || p.main));
  const images = _.values(imageUrls).map(p => formatImageUrl(p.main));
  const coverImage = _.values(coverImageUrls).map(p => formatImageUrl(p.main));

  useEffect(() => {
    setHtml(convertStringToHTML(content));
  }, [content]);

  const handleCancel = () => {
    // form.resetFields();
    setIsContentVisible(false);
    // dispatch({ type: 'RESET_TYPE' });
  };

  const edit = () => {
    // form.resetFields();
    setEditMode('post', 'posted');
    setIsContentVisible(false);
    setIsModalVisible(true);
    // dispatch({ type: 'RESET_TYPE' });
  };

  const onFinish = values => {};
  return (
    <div style={{ margin: 10 }}>
      <Modal
        title="Chi tiết bài viết"
        visible={isContentVisible}
        onCancel={handleCancel}
        onOk={handleCancel}
        okButtonProps={{ style: { backgroundColor: '#ff8731' } }}
      >
        <>
          <div style={{ width: '100%' }}>
            <img src={coverImage[0]} style={{ width: '100%' }} />
          </div>
          <h2>{data.title}</h2>

          {htmlContent && (
            <div className="description">
              <h4>Mô tả</h4>

              <div
                className="description_internal"
                dangerouslySetInnerHTML={{
                  __html: htmlContent,
                }}
              />
            </div>
          )}
          <div className="image-row">
            <div className="row flex" style={{ border: '1px solid #e3e3e3', borderRadius: 10 }}>
              {thumbnails.map((im, index) => (
                <div
                  onClick={() => {
                    setLightboxVisible(true);
                    setImageIndex(index);
                  }}
                >
                  <img src={im} alt="detail" />
                </div>
              ))}
            </div>
          </div>
          {(me.role === 1 || me.role === 2 || me.id === data.user_id) && (
            <button style={{ display: 'flex', marginLeft: 'auto' }} onClick={edit}>
              Cập nhật bài viết
            </button>
          )}

          <Comments addKnowledgeComment={addKnowledgeComment} knowledge data={data} />
          <ImageLightbox
            selectedIndex={imageIndex}
            isOpen={lightboxVisible}
            onClose={() => setLightboxVisible(false)}
            images={images}
          />
        </>
      </Modal>
    </div>
  );
};
PopupContent.defaultProps = {
  title: ' ',
  field: {},
  backgroundColor: '#ff8731',
  message: {
    notarizedDate: 'Vui lòng nhập Ngày công chứng',
    brokerageReceiveDate: 'Vui lòng nhập Ngày nhận hoa hồng',
    success: 'Tạo mới thành công',
    error: 'Someting was wrong, please try agian!',
  },
};
export function mapStateToProps(state) {
  return {
    // transactionBill: state.transactionBill.data,
    data: state.Knowledge.knowledgeList.data,
    state,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...action,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(PopupContent);
