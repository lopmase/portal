/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
import React, { useState, useEffect, useCallBack } from 'react';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { useDispatch, connect } from 'react-redux';
import { Modal, Collapse } from 'antd';
import _ from 'lodash';
import ImageLightbox from '../../../../components/LightBox/Lightbox';
import { convertStringToHTML, formatImageUrl } from '../../../../helpers/common';
import Comments from './Comments';
import * as action from './action';
// eslint-disable-next-line react/prop-types

const { Panel } = Collapse;

const PopupListNotApprove = ({
  backgroundColor,
  label,
  name,
  title,
  data,
  setIsModalVisible,
  setEditMode,
  setNotApproveVisible,
  notApproveVisible,
  total,
  setDataDetail,
  setEditFromNotApprove,
  deleteKnowledge
}) => {
  const handleCancel = () => {
    // form.resetFields();
    setNotApproveVisible(false);
    // dispatch({ type: 'RESET_TYPE' });
  };

  const edit = data => {
    // form.resetFields();
    setEditMode('post', 'posted');
    setDataDetail(data);
    setEditFromNotApprove();
    setIsModalVisible(true);
    // dispatch({ type: 'RESET_TYPE' });
  };

  const deletePost = id => {
    deleteKnowledge(id)
  }

  const onFinish = values => {
    setNotApproveVisible(false);
  };
  return (
    <div style={{ margin: 10 }}>
      <Modal
        title={title}
        visible={notApproveVisible}
        onCancel={handleCancel}
        onOk={handleCancel}
        okButtonProps={{ style: { backgroundColor: '#ff8731' } }}
      >
        <>
          <Collapse defaultActiveKey={['1']} 
           bordered={false}
           accordion={true}
           expandIconPosition={"right"}>
            <Panel header="Danh sách" key="1"
            style={{ maxHeight: "70vh", overflow: "auto" }}>
              
              {total &&
                data.map(item => (
                  <div className="postItem">
                    <h6>{item.title}</h6>
                    <p>
                      <span>Lý do từ chối:</span> {item.reason_reject}
                    </p>
                    <div className="postItem_button">
                      <button className="btn-hover color-blue" onClick={() => edit(item)}>
                        Cập nhật bài viết
                      </button>
                      <button className="btn-hover color-pink" onClick={() => deletePost(item.id)}>
                        Xoá bài viết
                      </button>
                    </div>
                  </div>
                ))}
            </Panel>
          </Collapse>
        </>
      </Modal>
    </div>
  );
};
PopupListNotApprove.defaultProps = {
  title: 'Danh sách bài viết không được duyệt',
  field: {},
  backgroundColor: '#ff8731',
};
export function mapStateToProps(state) {
  return {
    // transactionBill: state.transactionBill.data,
    data: state.Knowledge.knowledgeList.listNotApprove.data,
    total: state.Knowledge.knowledgeList.listNotApprove.total,

    state,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...action,
    },
    dispatch,
  );
}
export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(PopupListNotApprove);
