/* eslint-disable no-param-reassign */
import _ from 'lodash';
import { toast } from 'react-toastify';
import { showMessage } from '../../../Common/GlobalErrorModal/action';
import { uploadImages, rotateImages } from '../../../HouseContainer/UploadJobs/UploadJobs';
import { v4 as uuidv4 } from 'uuid';
import { UNKNOWN_ERROR } from '../../../../constants/MESSAGE';
import { REDUCERS } from '../../../../constants/COMMON';
import { fetch, fetchKnowledgeById } from '../../../AdminContainer/ApprovePost/action';
// import { ROUTES } from '../../../constants/ROUTES';
// import {
//   formatImageUrl,
//   convertNumberIntoUnit,
//   convertUnitIntoNumber,
//   convertStringToHTML,
// } from '../../../helpers/common';

export const fetchType = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const res = await thunkDependencies.knowledge.getType();
    dispatch({
      type: REDUCERS.TYPE,
      payload: res.data,
    });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const fetchKnowledge = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const res = await thunkDependencies.knowledge.getKnowledge();
    dispatch({
      type: REDUCERS.KNOWLEDGE,
      payload: res.data,
    });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const fetchKnowledgeNotApprove = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const res = await thunkDependencies.knowledge.getListNotApprove();
    dispatch({
      type: REDUCERS.KNOWLEDGE_NOT_APPROVE,
      payload: res.data,
    });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const createColumn = values => async (dispatch, getState, thunkDependencies) => {
  try {
    const response = await thunkDependencies.knowledge.addType(values);
    toast.success('Tạo cột thành công');
    dispatch(fetchKnowledge());
    // dispatch(fetchType());
    dispatch(fetch());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};
export const setTypeCreate = id => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch({
      type: REDUCERS.TYPE_CREATE,
      payload: id,
    });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const reset = () => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch({
      type: REDUCERS.RESET_DATA_KNOWLEDGE,
    });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const create = values => async (dispatch, getState, thunkDependencies) => {
  try {
    // eslint-disable-next-line camelcase
    const { images, cover_image } = values;
    // let internalImageIds = [];
    // let publicImageIds = [];
    console.log('values create', values);
    const imageJobs = cover_image.concat(images).map(
      (image, index) =>
        new Promise(resolve => {
          thunkDependencies.knowledge
            .upload(image.file, image.rotation, uuidv4())
            .then(response => {
              try {
                resolve({ data: response.data });
              } catch (error) {}
            })
            .catch(error => {});
        }),
    );

    const results = await Promise.all(imageJobs);
    const ids = results.filter(i => i.data).map(i => i.data.id);
    values.images = ids.slice(1);
    values.cover_image = ids.slice(0, 1);
    const response = await thunkDependencies.knowledge.addKnowledge(values);
    dispatch(fetchKnowledge());
    toast.success('Tạo mới bài viết thành công');
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};
export const setDataDetail = data => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch({
      type: REDUCERS.KNOWLEDGE_DETAIL,
      payload: data,
    });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const addKnowledgeComment = (id, content) => async (
  dispatch,
  getState,
  thunkDependencies,
) => {
  try {
    const result = await thunkDependencies.knowledge.comment(id, content);
    // apiSerializer(result);
    // dispatch(closeModal());
  } catch (error) {
    // dispatch(closeModal());
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const setEditMode = (type, status) => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch({
      type: REDUCERS.EDIT_KNOWLEDGE,
      payload: { type, status },
    });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const setType = type => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch({
      type: REDUCERS.TYPE_VISIBLE,
      payload: type,
    });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const updateKnowledge = values => async (dispatch, getState, thunkDependencies) => {
  const status = getState().Knowledge.knowledgeList.status;
  const editFromNotApprove = getState().Knowledge.knowledgeList.isEditFromNotApprove;
  console.log('editFromNotApprove', editFromNotApprove);
  try {
    // eslint-disable-next-line camelcase
    const { images, cover_image } = values;
    let imagesIds = images.filter(im => im.id).map(im => im.id);
    let coverImageIds = cover_image.filter(im => im.id).map(im => im.id);
    const newImages = images.filter(im => !im.id);
    const newCoverImages = cover_image.filter(im => !im.id);

    if (newImages.length) {
      const newImagesJob = newImages.map(
        (image, index) =>
          new Promise(resolve => {
            //   dispatch(startUpload(index));
            thunkDependencies.knowledge
              .upload(image.file, image.rotation, uuidv4())
              .then(response => {
                try {
                  resolve({ data: response.data });
                } catch (error) {
                  // const message = error.message || UNKNOWN_ERROR;
                  // throw new Error(message);
                }
              })
              .catch(error => {
                //   dispatch(uploadError(index, error.message || UNKNOWN_ERROR));
                //   resolve({ error: error.message || UNKNOWN_ERROR });
              });
          }),
      );
      const results = await Promise.all(newImagesJob);
      imagesIds = imagesIds.concat(results.filter(i => i.data).map(i => i.data.id));
    }

    if (newCoverImages.length) {
      const coverImagesJob = newCoverImages.map(
        (image, index) =>
          new Promise(resolve => {
            //   dispatch(startUpload(index));
            thunkDependencies.knowledge
              .upload(image.file, image.rotation, uuidv4())
              .then(response => {
                try {
                  resolve({ data: response.data });
                } catch (error) {
                  // const message = error.message || UNKNOWN_ERROR;
                  // throw new Error(message);
                }
              })
              .catch(error => {
                //   dispatch(uploadError(index, error.message || UNKNOWN_ERROR));
                //   resolve({ error: error.message || UNKNOWN_ERROR });
              });
          }),
      );
      const results = await Promise.all(coverImagesJob);
      coverImageIds = coverImageIds.concat(results.filter(i => i.data).map(i => i.data.id));
    }

    // const results = await Promise.all(imageJobs);
    // const ids = results.filter(i => i.data).map(i => i.data.id);
    values.images = imagesIds;
    values.cover_image = coverImageIds;
    if (editFromNotApprove) {
      values.reset_reason = true;
      await thunkDependencies.knowledge.updateKnowledge(values);
      dispatch(fetchKnowledgeNotApprove());
    } else {
      const response = await thunkDependencies.knowledge.updateKnowledge(values);
      if (status === 'posted') {
        dispatch(fetchKnowledge());
      } else {
        const id = getState().admin.fetchTypePost.fetchType;
        dispatch(fetchKnowledgeById(id));
      }
    }

    toast.success('Cập nhật bài viết thành công');
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const setEditFromNotApprove = () => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch({
      type: REDUCERS.KNOWLEDGE_EDIT_FROM_NOT_APPROVE,
    });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};
export const getWithPage = (id, page, column) => async (dispatch, getState, thunkDependencies) => {
  try {
    const res = await thunkDependencies.knowledge.getKnowledgeWithPage(id, page);
    return res;
    // dispatch({
    //   type: REDUCERS.FETCH_WITH_PAGE,
    //   column,
    //   data: res.data.list,
    // });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const deleteKnowledge = id => async (dispatch, getState, thunkDependencies) => {
  try {
    const res = await thunkDependencies.knowledge.deleteKnowledge(id);
    toast.success('Đã xoá bài viết');
    // dispatch({
    //   type: REDUCERS.FETCH_WITH_PAGE,
    //   column,
    //   data: res.data.list,
    // });
    dispatch(fetchKnowledgeNotApprove());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const knowledgeUpdate = data => async (dispatch, getState, thunkDependencies) => {
  try {
    // const res = await thunkDependencies.knowledge.deleteKnowledge(id);
    dispatch({
      type: REDUCERS.LIST_READY_UPDATE,
      payload: data,
    });
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const knowledgeUpdateColumn = data => async (dispatch, getState, thunkDependencies) => {
  try {
    const res = await thunkDependencies.knowledge.updateListPostChangeColumn(data);
    dispatch({
      type: REDUCERS.RESET_LIST_UPDATE,
      payload: data,
    });
    dispatch(fetchKnowledge());
    toast.success('Cập nhật thành công');
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};
