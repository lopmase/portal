import { combineReducers } from 'redux';
// import { TYPES } from './TYPES';
// import createModalReducer from '../../../helpers/createModalReducer';
import { REDUCERS } from '../../../../constants/COMMON';

const initial = {
  typeList: [],
  typeCreate: null,
};

const initialState = {
  knowledgeList: {},
  data: {},
  editMode: false,
  type: '',
  status: '',
  listNotApprove: [],
  isEditFromNotApprove: false,
};

const init = {
  list: [],
};

export const typeKnowledgeReducer = (state = initial, action) => {
  switch (action.type) {
    case REDUCERS.TYPE:
      return {
        ...state,
        typeList: action.payload,
      };
    case REDUCERS.TYPE_CREATE:
      return {
        ...state,
        typeCreate: action.payload,
      };

    default:
      return state;
  }
};

export const knowledgeReducer = (state = initialState, action) => {
  switch (action.type) {
    case REDUCERS.KNOWLEDGE:
      return {
        ...state,
        knowledgeList: action.payload,
      };
    case REDUCERS.KNOWLEDGE_DETAIL:
      return {
        ...state,
        data: action.payload,
      };
    case REDUCERS.EDIT_KNOWLEDGE:
      return {
        ...state,
        type: action.payload.type,
        editMode: true,
        status: action.payload.status,
      };
    case REDUCERS.TYPE_VISIBLE:
      return {
        ...state,
        type: action.payload,
      };
    case REDUCERS.RESET_DATA_KNOWLEDGE:
      return {
        ...state,
        type: '',
        editMode: false,
        data: {},
        status: '',
        // isEditFromNotApprove: false,
      };

    case REDUCERS.KNOWLEDGE_NOT_APPROVE:
      return {
        ...state,
        listNotApprove: action.payload,
      };
    case REDUCERS.KNOWLEDGE_EDIT_FROM_NOT_APPROVE:
      return {
        ...state,
        isEditFromNotApprove: true,
      };
    // case REDUCERS.FETCH_WITH_PAGE:
    //   return {
    //     ...state,
    //     // knowledgeList[action.column].list : knowledgeList[action.column].list.push(action.data)
    //   };
    default:
      return state;
  }
};

export const knowledgeReadyUpdateReducer = (state = init, action) => {
  switch (action.type) {
    case REDUCERS.LIST_READY_UPDATE:
      return {
        ...state,
        list: [...state.list, action.payload],
      };
    case REDUCERS.RESET_LIST_UPDATE:
      return {
        ...state,
        list: [],
      };
    default:
      return state;
  }
};
