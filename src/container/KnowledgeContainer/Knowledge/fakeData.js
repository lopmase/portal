import { v4 as uuidv4 } from 'uuid';

export const column1 = [
  { id: uuidv4(), prefix: 'column1', key: 'column1', title: '1' },
  { id: uuidv4(), prefix: 'column1', key: 'column1', title: '2' },
  { id: uuidv4(), prefix: 'column1', key: 'column1', title: '3' },
  { id: uuidv4(), prefix: 'column1', key: 'column1', title: '4' },
  { id: uuidv4(), prefix: 'column1', key: 'column1', title: '5' },
];

export const column2 = [
  { id: uuidv4(), prefix: 'column2', key: 'column2', title: '6' },
  { id: uuidv4(), prefix: 'column2', key: 'column2', title: '7' },
  { id: uuidv4(), prefix: 'column2', key: 'column2', title: '8' },
  { id: uuidv4(), prefix: 'column2', key: 'column2', title: '9' },
  { id: uuidv4(), prefix: 'column2', key: 'column2', title: '10' },
];

export const column3 = [
  { id: uuidv4(), prefix: 'column3', key: 'column3', title: '11' },
  { id: uuidv4(), prefix: 'column3', key: 'column3', title: '12' },
  { id: uuidv4(), prefix: 'column3', key: 'column3', title: '13' },
  { id: uuidv4(), prefix: 'column3', key: 'column3', title: '14' },
  { id: uuidv4(), prefix: 'column3', key: 'column3', title: '15' },
];

export const column4 = [
  { id: uuidv4(), prefix: 'column4', key: 'column4', title: '11' },
  { id: uuidv4(), prefix: 'column4', key: 'column4', title: '12' },
  { id: uuidv4(), prefix: 'column4', key: 'column4', title: '13' },
  { id: uuidv4(), prefix: 'column4', key: 'column4', title: '14' },
  { id: uuidv4(), prefix: 'column4', key: 'column4', title: '15' },
];

export default {
  column1,
  column2,
  column3,
  column4,
};
