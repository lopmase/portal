import React, { useEffect, useState } from 'react';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Row } from 'antd';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import './Knowledge.scss';
import Modal from './components/Modal';
import PopupContent from './components/PopupContent';
import PopupListNotApprove from './components/PopupListNotApprove';

import list from './fakeData';
import CardDragable from './components/CardDragable';
import * as actions from './components/action';

const contentStyle = {
  maxWidth: '600px',
  width: '90%',
};

const Knowledge = ({
  fetchType,
  fetchKnowledge,
  typeKnowledge,
  setTypeCreate,
  typeCreate,
  knowledgeList,
  me,
  setType,
  type,
  fetchKnowledgeNotApprove,
  getWithPage,
  knowledgeUpdate,
  listUpdate,
  knowledgeUpdateColumn,
}) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [isModalVisible, setIsModalVisible] = useState(false);
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [isContentVisible, setIsContentVisible] = useState(false);
  const [notApproveVisible, setNotApproveVisible] = useState(false);

  // const [type, setType] = useState('');

  const [listKey, setListKey] = useState([]);
  const [listData, setListData] = useState({});
  const [listColumnMapId, setListColumnMapId] = useState({});
  useEffect(() => {
    fetchType();
    fetchKnowledge();
    fetchKnowledgeNotApprove();
  }, []);
  console.log('listData', listData);
  useEffect(() => {
    const arrayData = Object.keys(knowledgeList).map(k => knowledgeList[k].list);
    const keyList = Object.keys(knowledgeList).map(k => k);
    const data = keyList.reduce((a, v, index) => ({ ...a, [v]: arrayData[index] }), {});
    setListData(data);
  }, [knowledgeList]);

  useEffect(() => {
    // eslint-disable-next-line react/prop-types
    setListKey(typeKnowledge.length && typeKnowledge.map(item => item.type_name));
    setListColumnMapId(
      typeKnowledge.length &&
        typeKnowledge.reduce((result, item, index, array) => {
          result[item.type_name] = item.id;
          return result;
        }, {}),
    );
  }, [typeKnowledge]);

  const removeFromList = (listD, index) => {
    const result = Array.from(listD);
    const [removed] = result.splice(index, 1);
    return [removed, result];
  };

  const addToList = (listD, index, element) => {
    const result = Array.from(listD);
    result.splice(index, 0, element);
    return result;
  };

  const showModal = (e, id) => {
    setTypeCreate(id);
    setType(e.target.name);
    setIsModalVisible(true);
  };
  const updatePost = () => {
    knowledgeUpdateColumn(listUpdate);
  };
  const showListNotApprove = () => {
    setNotApproveVisible(true);
  };

  const onDragEnd = result => {
    console.log('result', result);

    if (!result.destination) {
      return;
    }
    if (
      result.destination.droppableId === result.source.droppableId &&
      result.destination.index === result.source.index
    ) {
      return;
    }
    if (result.type === 'column') {
      const newColumnOrder = Array.from(listKey);
      newColumnOrder.splice(result.source.index, 1);
      newColumnOrder.splice(result.destination.index, 0, result.draggableId);
      setListKey(newColumnOrder);
      return;
    }
    const listCopy = { ...listData };
    const prevDropId = result.source.droppableId;
    const currentDropId = result.destination.droppableId;
    const sourceList = listCopy[prevDropId];
    const [removedElement, newSourceList] = removeFromList(sourceList, result.source.index);
    listCopy[prevDropId] = newSourceList;
    const destinationList = listCopy[currentDropId];
    listCopy[currentDropId] = addToList(destinationList, result.destination.index, removedElement);
    const currentId = typeKnowledge.find(item => currentDropId === item.type_name).id;
    const itemId = listData[prevDropId].find(item => item.key == result.draggableId).id;
    knowledgeUpdate({ currentId, itemId });
    setListData(listCopy);
  };
  return (
    <div className="knowLedge">
      <div className="knowLedge_button">
        <button className="btn-hover color-red" onClick={showListNotApprove}>
          Danh sách bài viết không được duyệt
        </button>
        {listUpdate.length && (me && (me.role === 2 || me.role === 1)) ? (
          <button className="btn-grad" onClick={updatePost}>
            Lưu thay đổi
          </button>
        ) : (
          ' '
        )}
        {/* <button className="btn-grad" onClick={updatePost}>
            Lưu thay đổi
          </button> */}
      </div>
      <div className="gutterList">
        <DragDropContext onDragEnd={onDragEnd}>
          {listKey.length > 0 && (
            <Droppable droppableId="all-columns" direction="horizontal" type="column">
              {provided => (
                <div
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                  style={{
                    display: 'inline-flex',
                    paddingRight: 200,
                    height: 'fit-content',
                    // overflowY: 'auto',
                  }}
                >
                  {listKey.map((item, index) => (
                    <CardDragable
                      me={me}
                      elements={Object.keys(listData).length !== 0 && listData[item]}
                      key={item}
                      prefix={item}
                      columnId={listColumnMapId[item]}
                      showModal={showModal}
                      setIsContentVisible={setIsContentVisible}
                      index={index}
                      lastPage={
                        Object.keys(knowledgeList).length !== 0 && knowledgeList[item].lastPage
                      }
                      page={Object.keys(knowledgeList).length !== 0 && knowledgeList[item].page}
                      getWithPage={getWithPage}
                      setListData={setListData}
                    />
                  ))}
                </div>
              )}
            </Droppable>
          )}
        </DragDropContext>
      </div>
      {/* </Row> */}
      {isModalVisible && (
        <Modal
          typeCreate={typeCreate}
          type={type}
          setIsModalVisible={setIsModalVisible}
          isModalVisible={isModalVisible}
          setType={setType}
        />
      )}
      {isContentVisible && (
        <PopupContent
          me={me}
          setIsContentVisible={setIsContentVisible}
          isContentVisible={isContentVisible}
          setIsModalVisible={setIsModalVisible}
        />
      )}
      <PopupListNotApprove
        notApproveVisible={notApproveVisible}
        setNotApproveVisible={setNotApproveVisible}
        setIsModalVisible={setIsModalVisible}
      />
    </div>
  );
};
export function mapStateToProps(state) {
  return {
    typeKnowledge: state.Knowledge.typeKnowledge.typeList,
    typeCreate: state.Knowledge.typeKnowledge.typeCreate,
    knowledgeList: state.Knowledge.knowledgeList.knowledgeList,
    type: state.Knowledge.knowledgeList.type,
    me: state.auth.user.data,
    listUpdate: state.Knowledge.listReadyUpdate.list,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Knowledge);
