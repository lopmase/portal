import { combineReducers } from 'redux';
import {
  typeKnowledgeReducer,
  knowledgeReducer,
  knowledgeReadyUpdateReducer,
} from './Knowledge/components/reducer';

export default combineReducers({
  typeKnowledge: typeKnowledgeReducer,
  knowledgeList: knowledgeReducer,
  listReadyUpdate: knowledgeReadyUpdateReducer,
});
