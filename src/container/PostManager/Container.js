import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import { ROUTES } from '../../constants/ROUTES';
import { PrivateRoute } from '../../components';
import NotFound from '../Common/NotFound';
import PostManager from './Post/index';
// import AlonhadatPostList from './Post/AlonhadatPostList';

class HouseContainer extends Component {
  state = {};

  render() {
    return (
      <Switch>
        <PrivateRoute exact path={ROUTES.POST_MANAGER} component={PostManager} />
        {/* <PrivateRoute exact path={ROUTES.ALONHADAT_LIST} component={AlonhadatPostList} /> */}
        <Route component={NotFound} />
      </Switch>
    );
  }
}

export default HouseContainer;
