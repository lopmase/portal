/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { Table } from 'antd';
import moment from 'moment';
import FormPost from './FormPost';
import { postActionType } from '../../../constants/COMMON';
import { Button } from '../../../components';

const AddressModal = ({ data, form, onFinish, setUpdateAddress }) => {
  const [isUpdate, setIsUpdate] = useState(false);
  const column = [
    {
      title: 'Người tạo',
      dataIndex: 'user',
      key: 'user',
      render: value => <h4>{value.name}</h4>,
    },
    {
      title: 'Nguồn tin',
      dataIndex: 'channel',
      key: 'channel',
      render: value => <h4>{value}</h4>,
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'created_at',
      key: 'created_at',
      render: value => <h4>{moment(value * 1000).format('DD/MM/YYYY HH:mm:ss')}</h4>,
    },
    {
      title: 'Chỉnh sửa',
      key: 'update',
      render: (text, record) => (
        <Button
          onClick={() => {
            setUpdateAddress(record);
            setIsUpdate(true);
          }}
          text="Chỉnh sửa"
        />
      ),
    },
  ];
  return (
    <>
      <div className="column-list">
        <div>
          <h1 style={{ marginRight: 10, marginBottom: 0 }}>Danh sách nguồn tin</h1>
        </div>
        {isUpdate === false && (
          <Table
            dataSource={data}
            scroll={{ x: 700, y: 750 }}
            columns={column}
            pagination={false}
          />
        )}
        {isUpdate && (
          <FormPost type={postActionType.address_setting} form={form} onFinish={onFinish} />
        )}
      </div>
    </>
  );
};

export default AddressModal;
