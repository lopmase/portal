/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { Checkbox } from 'antd';
import { Flexbox, Spacer } from '../../../components';
import PurposeInfo from '../../HouseContainer/NewHouse/PurposeInfo';
import LocationInfo from '../../HouseContainer/NewHouse/LocationInfo';
import TypeInfo from './component/TypeInfo';
import OtherInfo from './component/OtherInfo';
import SpecInfo from './component/SpecInfo';
import BasicInfo from '../../HouseContainer/NewHouse/BasicInfo';
import PublicImageInfo from '../../HouseContainer/NewHouse/PublicImageInfo';
import PostInfo from './PostInfo';
import { REDUCERS, postActionType } from '../../../constants/COMMON';
import action from './action';

const { Group } = Checkbox;
const options = [
  {
    label: 'Alonhadat.vn',
    value: 'Alonhadat',
  },
  {
    label: 'Batdongsan.com.vn',
    value: 'Batdongsan',
  },
];
const AlonhadatForm = ({
  form,
  fetchHouse,
  fetchPostDetail,
  handleChange,
  houseId,
  type,
  addressToUpdate,
  prevType,
}) => {
  useEffect(() => {
    if (
      type !== postActionType.update_post &&
      type !== postActionType.update_posted &&
      type !== postActionType.confirm &&
      prevType !== postActionType.confirm
    ) {
      fetchHouse(houseId);
    } else if (prevType !== postActionType.confirm) {
      fetchPostDetail(houseId, addressToUpdate);
    }
  }, [houseId, type, addressToUpdate]);

  return (
    <div className="newhouse-page">
      <>
        <Group
          disabled={type === postActionType.update_post}
          options={options.map(o => o.value)}
          defaultValue={options[0].value}
          onChange={value => handleChange('addressPost', value)}
        />
        <Spacer />
        <div className="header-newHouse">
          <PurposeInfo editMode form={form} handleChange={handleChange} />
        </div>
        <div>
          <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
            <div style={{ flex: 5 }}>
              <LocationInfo form={form} handleChange={handleChange} />
            </div>
          </Flexbox>
          <Spacer />
          <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
            <div style={{ flex: 3 }}>
              <TypeInfo form={form} handleChange={handleChange} />
            </div>
            <Spacer />
            <div style={{ flex: 2 }}>
              <OtherInfo form={form} handleChange={handleChange} />
            </div>
          </Flexbox>
          <Spacer />
          <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
            <div style={{ flex: 5 }}>
              <SpecInfo form={form} handleChange={handleChange} />
            </div>
            <Spacer />
            <div style={{ flex: 3 }}>
              <BasicInfo
                editMode
                internal={false}
                form={form}
                handleChange={handleChange}
                changeFromAloNhaDat
              />
            </div>
          </Flexbox>

          <Spacer />
          <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
            <div style={{ flex: 1 }}>
              <PublicImageInfo form={form} handleChange={handleChange} />
            </div>
          </Flexbox>
          <Spacer />
          <PostInfo form={form} handleChange={handleChange} />
        </div>
      </>
    </div>
  );
};

export function mapStateToProps(state) {
  return {
    form: state.alonhadat[REDUCERS.NEW_POST],
    me: state.auth.user.data,
    type: state.alonhadat[REDUCERS.POST_STATE].type,
    prevType: state.alonhadat[REDUCERS.POST_STATE].prevType,
    state,
  };
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...action,
    },
    dispatch,
  );
}
export default compose(connect(mapStateToProps, mapDispatchToProps))(AlonhadatForm);
