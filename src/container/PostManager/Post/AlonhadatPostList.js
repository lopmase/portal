/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { push } from 'connected-react-router';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Collapse, Table } from 'antd';
import { Button } from '../../../components';
import { postActionType, REDUCERS } from '../../../constants/COMMON';
import { alonhadatColumns } from './alonhadatColumns';
import { approvalAlonhadat, deleteAlonhadat, deleteBatdongsan } from '../../../apis/houseApi';
import { confirm } from '../../ShellContainner/ConfirmDialog/action';
import * as streetActions from '../../HouseContainer/Metadata/streetAction';
import * as optionsAction from '../../HouseContainer/Metadata/optionsAction';
import action from './action';

const { Panel } = Collapse;
function AlonhadatPostList({
  data,
  setType,
  setId,
  getPostList,
  loadStreets,
  getOptions,
  setAddressToUpdate,
  confirmDelete,
  closeModal,
  deletePosted,
}) {
  const [waitingList, setWaitingList] = useState([]);
  const [postedList, setPostedList] = useState([]);
  const [failedList, setFailedList] = useState([]);
  const [waitingApprovalList, setWaitingApprovalList] = useState([]);
  const approval = async house => {
    const res = await approvalAlonhadat(house.id);
    res && getPostList();
  };

  const deletePost = async house => {
    confirmDelete('Bạn có chắc muốn xóa tin này?', result => {
      if (result) {
        deletePosted(house.house_id);
        // house.post_address === 'alonhadat.com.vn' && deleteAlonhadat(house.id);
        // house.post_address === 'batdongsan.com.vn' && deleteBatdongsan(house.id);
      }
    });
  };

  const waitingApprovalActionColumns = [
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => <Button small onClick={() => approval(record)} text="Duyệt" />,
    },
    {
      title: 'Action',
      key: 'edit',
      render: (text, record) => (
        <Button
          small
          onClick={() => {
            setType(postActionType.update_post);
            setAddressToUpdate(record.post_address);
            setId(record.house_id);
          }}
          text="Sửa"
        />
      ),
    },
  ];

  const postedActionColumns = [
    {
      title: 'Action',
      key: 'deletePosted',
      render: (text, record) => (
        <Button
          small
          onClick={() => {
            closeModal();
            setType(postActionType.delete_posted);
            setAddressToUpdate(record.post_address);
            deletePost(record);
          }}
          text="Xóa"
        />
      ),
    },
    {
      title: 'Action',
      key: 'editPosted',
      render: (text, record) => (
        <Button
          small
          onClick={() => {
            setType(postActionType.update_posted);
            setAddressToUpdate(record.post_address);
            setId(record.house_id);
          }}
          text="Sửa"
        />
      ),
    },
  ];
  useEffect(() => {
    if (data.waiting) {
      setWaitingList(data.waiting);
    }
    if (data.posted) {
      setPostedList(data.posted);
    }
    if (data.failed) {
      setFailedList(data.failed);
    }
    if (data.waitingApproval) {
      setWaitingApprovalList(data.waitingApproval);
    }
  }, [data]);
  useEffect(() => {
    loadStreets();
    getOptions();
  }, []);
  return (
    <>
      <Collapse bordered={false} defaultActiveKey={['0']} className="site-collapse-custom-collapse">
        <Panel header={<b>Chờ duyệt</b>} key="0" className="site-collapse-custom-panel">
          <Table
            columns={[...alonhadatColumns, ...waitingApprovalActionColumns]}
            dataSource={waitingApprovalList}
            pagination={false}
            rowKey="uid"
            scroll={{ x: 400 }}
          />
        </Panel>
        <Panel header={<b>Chờ đăng</b>} key="1" className="site-collapse-custom-panel">
          <Table
            columns={alonhadatColumns}
            dataSource={waitingList}
            pagination={false}
            rowKey="uid"
            scroll={{ x: 400 }}
          />
        </Panel>
        <Panel header={<b>Đã đăng</b>} key="2" className="site-collapse-custom-panel">
          <Table
            columns={[...alonhadatColumns, ...postedActionColumns]}
            dataSource={postedList}
            pagination={false}
            rowKey="uid"
            scroll={{ x: 400 }}
          />
        </Panel>
        <Panel header={<b>Lỗi Tin</b>} key="3" className="site-collapse-custom-panel">
          <Table
            columns={alonhadatColumns}
            dataSource={failedList}
            pagination={false}
            rowKey="uid"
            scroll={{ x: 400 }}
          />
        </Panel>
      </Collapse>
    </>
  );
}

export function mapStateToProps(state) {
  return {
    data: state.alonhadat[REDUCERS.POST_STATE].data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...streetActions,
      ...optionsAction,
      ...action,
      confirmDelete: confirm,
      doPush: push,
    },
    dispatch,
  );
}

AlonhadatPostList.propTypes = {
  data: PropTypes.array,
};
AlonhadatPostList.defaultProps = {
  data: [],
};
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(AlonhadatPostList);
