/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import { Tag, Row, Col } from 'antd';
import _ from 'lodash';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import { connect } from 'react-redux';

import { Card, Button, Spacer } from '../../../components';
import HtmlDescription from '../../Common/HtmlDescription/HtmlDescription';
import { formatMoney, formatImageUrl } from '../../../helpers/common';
import ImageLightbox from '../../../components/LightBox/Lightbox';
import { FIELDS } from '../../../constants/DETAIL_POST_FIELDS';
import { postActionType } from '../../../constants/COMMON';
import 'antd/dist/antd.css';

const notDetail = ['internal_image', 'public_image', 'image', 'house_direction'];

const DetailPost = ({
  record,
  options,
  deletePost,
  setType,
  setId,
  openModal,
  addressToUpdate,
  type,
  cell,
}) => {
  const [lightboxVisible, setLightboxVisible] = useState(false);
  const [imageIndex, setImageIndex] = useState(0);
  const [image, setImage] = useState([]);
  const [thumbnails, setThumbnails] = useState([]);
  const [channel, setChannel] = useState('Alonhadat.vn');
  useEffect(() => {
    console.log('record', record);
    if (record && record.image) {
      const thumb = _.values(record.image.public).map(p => formatImageUrl(p.thumbnail || p.main));
      setThumbnails(thumb);
      const img = _.values(record.image.public).map(p => formatImageUrl(p.main));
      setImage(img);
    }
  }, [record]);
  useEffect(() => {
    cell && setChannel(cell.channel);
  }, [cell]);
  return (
    <>
      <div className="detailPost">
        {record.type === 'posted' && (
          <Row gutter={[10, 10]}>
            <Button
              small
              onClick={() => {
                openModal();
                setType(postActionType.update_posted);
                setId(record.house_id);
              }}
              text="Sửa"
            />
            <Spacer />
            <Button small onClick={() => deletePost(record)} text="Xóa" />
          </Row>
        )}
        <Spacer />
        {/* đăng bằng tool */}
        {record && record[channel] && (
          <Card containerStyle={{ marginBottom: 10 }} shadow>
            <Row gutter={[10, 10]}>
              {FIELDS.filter(key => !notDetail.includes(key)).map(field => (
                <Col md={6} sm={8} xs={12}>
                  <div>
                    <div style={{ fontWeight: 'bold', fontSize: 13 }}>{field.label}</div>
                    <div className="small-text">
                      {field.format && record[channel][addressToUpdate]
                        ? field.format(record[channel][addressToUpdate], options)
                        : record[channel][field.property]}
                    </div>
                  </div>
                </Col>
              ))}
            </Row>
          </Card>
        )}
        {/* confỉm và đăng tay  */}
        {record && !record[channel] && (
          <Card containerStyle={{ marginBottom: 10 }} shadow>
            <Row gutter={[10, 10]}>
              {FIELDS.filter(key => !notDetail.includes(key)).map(field => (
                <Col md={6} sm={8} xs={12}>
                  <div>
                    <div style={{ fontWeight: 'bold', fontSize: 13 }}>{field.label}</div>
                    <div className="small-text">
                      {field.format && record[addressToUpdate]
                        ? field.format(record[addressToUpdate], options)
                        : record[field.property]}
                    </div>
                  </div>
                </Col>
              ))}
            </Row>
          </Card>
        )}
        <HtmlDescription
          convert={type === 16}
          description={record.initialDescription || record.description}
        />
        <div className="row flex">
          {thumbnails.map((im, index) => (
            <div
              onClick={() => {
                setLightboxVisible(true);
                setImageIndex(index);
              }}
            >
              <img src={im} alt="detail" />
            </div>
          ))}
        </div>
        <ImageLightbox
          selectedIndex={imageIndex}
          isOpen={lightboxVisible}
          onClose={() => setLightboxVisible(false)}
          images={image}
        />
      </div>
    </>
  );
};
export default DetailPost;
