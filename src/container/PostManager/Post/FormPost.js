/* eslint-disable react/no-array-index-key */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-one-expression-per-line */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Form, Select, Input, Spin } from 'antd';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { postActionType, REDUCERS } from '../../../constants/COMMON';
import action from './action';
import AlonhadatForm from './AlonhadatForm';

const FormPost = withRouter(
  ({
    houseToPost,
    setPrevIdHouse,
    setId,
    prevId,
    house,
    type,
    form,
    onFinish,
    initialValues,
    postAddress,
    houseDirect,
    postType,
    waiting,
    addressToUpdate,
    me,
    location,
    setHouseDirect,
  }) => {
    const { Option } = Select;

    useEffect(() => {
      const iniArray = {};
      if (initialValues) {
        iniArray.house_id = initialValues.house_id;
        iniArray.house_address = initialValues.house_address;
        Object.keys(initialValues).map(value => {
          if (value.split('_')[1] === 'link') {
            iniArray[value.split('_')[0]] = initialValues[value];
          }
          return value;
        });
        form.setFieldsValue({
          ...iniArray,
        });
      }
      if (houseDirect) {
        form.setFieldsValue({ house_id: houseDirect.house_id });
      }
      console.log('prevId', prevId)
      if (location && location.state) {
        if (prevId !== location.state.house_id) {
          setPrevIdHouse(location.state.house_id);
          setId(location.state.house_id);
        }
        // form.setFieldsValue({ house_id: location.state.house_id });
      }
    }, [initialValues, houseDirect, form, location]);

    const onChange = value => {
      setId(value.house_id);
    };

    return (
      <Spin tip="Loading..." spinning={waiting} delay={500}>
        <Form
          name="basic"
          labelCol={{ span: 16 }}
          wrapperCol={{ span: 16 }}
          onFinish={onFinish}
          autoComplete="off"
          layout="vertical"
          form={form}
          onValuesChange={onChange}
        >
          {((type === postActionType.create && postType) ||
            (type === postActionType.update && postType)) && (
            <Form.Item
              label={
                postType === 1 && !me.alonhadat_name
                  ? 'Bạn cần cung cấp tài khoản Alonhadat.com.vn'
                  : 'Bất động sản'
              }
              name="house_id"
              rules={[{ required: true, message: 'Bất động sản không thể trống' }]}
            >
              <Select
                placeholder="Chọn bất động sản"
                allowClear
                showSearch
                disabled={
                  type === postActionType.update ||
                  houseDirect ||
                  (postType === 1 && !me.alonhadat_name)
                }
                defaultValue={houseDirect ? houseDirect.house_address : undefined}
                filterOption={(input, option) => {
                  return (
                    (option.children &&
                      option.children.toUpperCase().indexOf(input.toUpperCase()) >= 0) ||
                    (option.children &&
                      option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0)
                  );
                }}
              >
                {house &&
                  house.map((val, index) => (
                    <Option key={index} value={val.id}>
                      {`${val.house_address} - (${val.into_money} - dài: ${val.length} - rộng: ${val.width})`}
                    </Option>
                  ))}
              </Select>
            </Form.Item>
          )}
          {type === postActionType.create_post_address && (
            <Form.Item label="Nguồn tin" name="channel">
              <Input />
            </Form.Item>
          )}
          {type === 7 && (
            <Form.Item label="Nguồn tin mới" name="channel">
              <Input />
            </Form.Item>
          )}
          {(postType === 2 || type === postActionType.update) &&
            postAddress &&
            postAddress.map((e, index) => (
              <Form.Item key={index} label={e.channel} name={e.channel}>
                <Input />
              </Form.Item>
            ))}
          {(postType === 1 ||
            type === postActionType.update_post ||
            type === postActionType.confirm ||
            type === postActionType.update_posted) &&
            houseToPost && (
              <AlonhadatForm houseId={houseToPost} addressToUpdate={addressToUpdate} />
            )}
        </Form>
      </Spin>
    );
  },
);

FormPost.propTypes = {
  waiting: PropTypes.bool,
};
FormPost.defaultProps = {
  waiting: false,
};

export function mapStateToProps(state) {
  return {
    houseToPost: state.alonhadat[REDUCERS.POST_STATE].idHouseToPost,
    prevId: state.alonhadat[REDUCERS.POST_STATE].prevIdHouseToPost,
    addressToUpdate: state.alonhadat[REDUCERS.POST_STATE].addressToUpdate,
    type: state.alonhadat[REDUCERS.POST_STATE].type,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...action,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(FormPost);
