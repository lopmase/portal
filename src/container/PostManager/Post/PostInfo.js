import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Card, Flexbox, Spacer, FormDropdown, FormRadioGroup } from '../../../components';
import { REDUCERS } from '../../../constants/COMMON';

const typeNewsValue = [
  {
    value: 0,
    text: 'VIP Đặc Biệt / 80.000đ / ngày',
  },
  {
    value: 1,
    text: 'VIP 1 / 40.000đ / ngày',
  },
  {
    value: 2,
    text: 'VIP 2 / 20.000đ / ngày',
  },
  {
    value: 3,
    text: 'VIP 3 / 14.000đ / ngày',
  },
  {
    value: 4,
    text: 'VIP 4 / 12.000đ / ngày',
  },
  {
    value: 5,
    text: 'VIP 5 / 10.000đ / ngày',
  },
];
const batdongsanTypeNews = [
  { value: 0, text: 'VIP Đặc biệt' },
  { value: 1, text: 'VIP 1' },
  { value: 2, text: 'VIP 2' },
  { value: 3, text: 'VIP 3' },
  { value: 4, text: 'VIP Ưu đãi' },
  { value: 5, text: 'Tin thường' },
];
const VIPType = [
  {
    value: 0,
    text: 'VIP Đặc Biệt / 2.000.000đ / tháng',
  },
  {
    value: 1,
    text: 'VIP 1 / 1.000.000đ / tháng',
  },
  {
    value: 2,
    text: 'VIP 2 / 500.000đ / tháng',
  },
  {
    value: 3,
    text: 'VIP 3 / 350.000đ / tháng',
  },
  {
    value: 4,
    text: 'VIP 4 / 300.000đ / tháng',
  },
  {
    value: 5,
    text: 'VIP 5 / 250.000đ / tháng',
  },
];
const batdongsanTypeNewsVlaue = () => {
  const days = new Array(25).fill(null).map((v, index) => ({ text: index + 7, value: index + 7 }));

  days.push(
    ...[
      { text: 90, value: 90 },
      { text: 180, value: 180 },
    ],
  );
  return days;
};

const PostInfo = ({ form, handleChange }) => {
  return (
    <Card shadow containerStyle={{ flex: 4 }}>
      {form && form.data.addressPost.includes('Alonhadat') && (
        <>
          <h3 className="bt-line">Loại tin Alonhadat.com.vn</h3>
          <Flexbox row flexStart containerStyle={{ alignItems: 'flex-start' }}>
            <div style={{ flex: 2 }}>
              <FormRadioGroup
                name="commission"
                label="Có phải trả phí hoa hồng cho người giới thiệu không?"
                {...{ form, handleChange }}
                options={[
                  { value: 0, text: 'Không' },
                  { text: 'Có', value: 1 },
                ]}
              />
            </div>
            <Spacer />
            <div style={{ flex: 1 }}>
              <FormRadioGroup
                name="type_news"
                label="Loại tin?"
                {...{ form, handleChange }}
                options={[
                  { value: 0, text: 'Tin thường' },
                  { text: 'Vip ngày', value: 1 },
                  { value: 2, text: 'Vip tháng' },
                ]}
              />
              {form.data.type_news === 1 && (
                <div style={{ flex: 2 }}>
                  <Flexbox row flexStart>
                    <FormDropdown
                      name="type_news_value"
                      label="Loại phí tin"
                      form={form}
                      search
                      handleChange={handleChange}
                      items={typeNewsValue.map(s => ({ text: s.text, value: s.value }))}
                    />
                    <Spacer />
                    <FormDropdown
                      name="type_news_day"
                      label="Số ngày"
                      form={form}
                      search
                      handleChange={handleChange}
                      items={new Array(19)
                        .fill(null)
                        .map((v, index) => ({ text: index + 1, value: index + 1 }))}
                    />
                  </Flexbox>
                </div>
              )}
              {form.data.type_news === 2 && (
                <div style={{ flex: 1 }}>
                  <Flexbox row flexStart>
                    <FormDropdown
                      name="type_news_value"
                      label="Loại VIP"
                      form={form}
                      search
                      handleChange={handleChange}
                      items={VIPType.map(s => ({ text: s.text, value: s.value }))}
                    />
                  </Flexbox>
                </div>
              )}
            </div>
            <Spacer />
          </Flexbox>
        </>
      )}
      {form && form.data.addressPost.includes('Batdongsan') && (
        <>
          <h3 className="bt-line">Loại tin Batdongsan.com.vn</h3>
          <Flexbox row flexStart containerStyle={{ alignItems: 'flex-start' }}>
            <FormDropdown
              name="batdongsan_type_news"
              label="Loại tin đăng"
              form={form}
              search
              handleChange={handleChange}
              items={batdongsanTypeNews.map(s => ({ text: s.text, value: s.value }))}
            />
            <Spacer />
            <FormDropdown
              name="batdongsan_type_news_days"
              label="Số ngày đăng"
              form={form}
              search
              handleChange={handleChange}
              items={batdongsanTypeNewsVlaue()}
            />
          </Flexbox>
        </>
      )}
    </Card>
  );
};

PostInfo.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  // data: PropTypes.any.isRequired,
};

PostInfo.defaultProps = {};

export function mapStateToProps(state) {
  return {
    data: state.metadata[REDUCERS.OPTIONS].data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(PostInfo);
