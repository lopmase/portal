/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { Switch, Row, Col } from 'antd';
import { connect, useDispatch } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { toast } from 'react-toastify';
import { updatePostAddressStatus } from '../../../apis/userApi';
import { showMessage } from '../../Common/GlobalErrorModal/action';

const SelectOption = ({ data, getData, condition, getAddress }) => {
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const onChange = async (item, checked) => {
    try {
      setLoading(true);
      const res = await updatePostAddressStatus({
        channel: item.channel,
        status: checked === false ? 0 : 1,
      });
      if (res) {
        toast.success('Update Successfull');
        getAddress();
        const result = await getData(condition);
        result && setLoading(false);
      }
    } catch (err) {
      dispatch(showMessage('Something went Wrong!'));
    }
  };
  return (
    <>
      <div className="column-list">
        <div>
          <h1 style={{ marginRight: 10, marginBottom: 0 }}>Danh sách cột</h1>
        </div>
        <div className="column-list-content">
          <div>
            {data.map(item => (
              <div className="list-group-item moveable" key={item.channel}>
                <Row key={item.channel}>
                  <Col flex={2}>
                    <h4 className="title">{item.channel}</h4>
                  </Col>
                  <Col flex={3}>
                    <Switch
                      loading={loading}
                      className="switch"
                      defaultChecked={item.status === 1}
                      onChange={checked => onChange(item, checked)}
                    />
                  </Col>
                </Row>
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  );
};
export function mapStateToProps(state) {
  return {
    user: state.transaction.user,
    transaction: state.transaction.data,
    me: state.auth.user.data,
    condition: state.condition.data.condition,
    type: state.condition.type,
    state,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(SelectOption);
