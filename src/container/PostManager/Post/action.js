/* eslint-disable no-param-reassign */
import _ from 'lodash';
import { toast } from 'react-toastify';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import {
  alonhadatReducerType,
  REDUCERS,
  postActionType,
  postReducerType,
} from '../../../constants/COMMON';
import { generateFormActions } from '../../../helpers/formAction';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { uploadImages, rotateImages } from '../../HouseContainer/UploadJobs/action';
import { generateDataActions } from '../../../helpers/dataAction';
import {
  formatImageUrl,
  convertNumberIntoUnit,
  convertUnitIntoNumber,
  convertStringToHTML,
  formatMoney,
} from '../../../helpers/common';

export const {
  handleFieldChanged,
  submitting,
  submitDone,
  submitError,
  validateAll,
  reset,
  waiting,
  waitingDone,
} = generateFormActions(REDUCERS.NEW_POST);

const handleChange = (name, value) => async dispatch => {
  dispatch(handleFieldChanged(name, value));
};

const houseFields = [
  'id',
  'area',
  'brokerage_fee',
  'brokerage_rate',
  'customer_id',
  'descriptions',
  'key_word',
  'description',
  'internalDescription',
  'key_word',
  'district',
  'end_open',
  'floor_area',
  'floors',
  'house_address',
  'house_direction',
  'house_balcony_direction',
  'house_number',
  'house_type',
  'image',
  'into_money',
  'length',
  'number_bedroom',
  'number_wc',
  'ownership',
  'project_id',
  'block_section',
  'floor_lot',
  'property_type',
  'province',
  'purpose',
  'status',
  'street_type',
  'title',
  'user_id',
  'wide_street',
  'width',
  'internal_image',
  'public_image',
  'public',
  'type_news',
  'commission',
  'type_news_day',
  'dining_room',
  'kitchen',
  'terrace',
  'car_parking',
  'post',
  'failed_quantity',
  'type_news_value',
  'post_address',
];
const { fetchDone, fetching, fetchError } = generateDataActions(REDUCERS.POST_DETAIL_DATA);

export const setDefaultTypeForCreatePost = () => async dispatch => {
  dispatch({
    type: alonhadatReducerType.SET_DEFAULT_TYPE,
  });
};

export const setId = id => async dispatch => {
  dispatch({
    type: alonhadatReducerType.SET_ID,
    payload: id,
  });
};

export const getPostData = conditions => async (dispatch, getState, thunkDependencies) => {
  const { addressToUpdate } = getState().alonhadat[REDUCERS.POST_STATE];
  const postArray = [];
  dispatch({
    type: postReducerType.WAITING,
  });
  const postData = await thunkDependencies.user.getPostManager(conditions);

  const data = postData.data.data ? postData.data.data : postData.data;
  dispatch({
    type: postReducerType.LAST_PAGE,
    payload: postData.data.last_page,
  });
  dispatch({
    type: postReducerType.CURRENT_PAGE,
    payload: postData.data.current_page,
  });
  if (data) {
    if (postData.data && postData.data.data && postData.data.data.length > 0) {
      dispatch({
        type: postReducerType.SET_POST_QUANTITY,
        payload: postData.data.data[0].postQuantity,
      });
    }

    data.map(value => {
      value.post.forEach(p => {
        if (value) {
          value.type = 'posted';
          if (p.channel === 'alonhadat.vn') {
            if (value['alonhadat.vn']) {
              // đăng bằng tool
              value.title = value['alonhadat.vn'].title;
              value.initialDescription = value['alonhadat.vn'].initialDescription;
              value.house_number = value['alonhadat.vn'].house_number;
              value.into_money = formatMoney(value['alonhadat.vn'].into_money);
              if (value.post.length > 0) {
                value.post.forEach(val => {
                  value[`${val.channel}_link`] = val.link.substring(0, 10);
                });
              }
              value.tool = true;
              value.house_address = `${value['alonhadat.vn'].house_number} ${value['alonhadat.vn'].house_address}, ${value['alonhadat.vn'].district}...`;
            } else {
              value[`${value.channel}_link`] = value.link;
              value.house_address = `${value.house.house_number} ${value.house.house_address}, ${value.house.district}...`;
              value.manual = true;
              value.into_money = formatMoney(value.house.into_money);
            }
          } else if (p.channel === 'batdongsan.com.vn') {
            if (value['batdongsan.com.vn']) {
              value.title = value['batdongsan.com.vn'].title;
              value.initialDescription = value['batdongsan.com.vn'].initialDescription;
              value.house_number = value['batdongsan.com.vn'].house_number;
              value.into_money = formatMoney(value['batdongsan.com.vn'].into_money);
              if (value.post.length > 0) {
                value.post.forEach(val => {
                  value[`${val.channel}_link`] = val.link.substring(0, 10);
                });
              }
              value.house_address = `${value['batdongsan.com.vn'].house_number} ${value['batdongsan.com.vn'].house_address}, ${value['batdongsan.com.vn'].district}...`;
              value.tool = true;
            } else {
              value[`${value.channel}_link`] = value.link;
              value.house_address = `${value.house.house_number} ${value.house.house_address}, ${value.house.district}...`;
              value.manual = true;
              value.into_money = formatMoney(value.house.into_money);
            }
          } else if (value.post.length > 0) {
            value.post.forEach(val => {
              value[`${val.channel}_link`] = val.link;
              value.house_address = `${value.house.house_number} ${value.house.house_address}, ${value.house.district}...`;
              value.manual = true;
              value.into_money = formatMoney(value.house.into_money);
            });
          }
          value.status = value.house.status === 0 ? 'Mở bán' : ' Ngưng bán';
        }
      });
      return value;
    });
  }
  const postGroupBy = _.groupBy(data, x => x && x.house_id);
  Object.values(postGroupBy).forEach(value => {
    const postElement = value.reduce(
      (total, element) => element !== null && { ...total, ...element },
    );
    postElement && postArray.push(postElement);
  });

  if (conditions.page) {
    const prePost = getState().alonhadat.POST.posted;
    dispatch({
      type: postReducerType.POSTED_SET_DATA,
      payload: [...prePost, ...postArray],
    });
  } else {
    dispatch({
      type: postReducerType.POSTED_SET_DATA,
      payload: postArray,
    });
  }
  dispatch({
    type: postReducerType.DONE,
  });
};

export const getWaitingData = conditions => async (dispatch, getState, thunkDependencies) => {
  const waitingArray = [];
  dispatch({
    type: postReducerType.WAITING,
  });
  const waitingData = await thunkDependencies.user.getPostManagerWaitingList(conditions);
  const data = waitingData.data.data ? waitingData.data.data : waitingData.data;
  if (data) {
    if (waitingData.data) {
      dispatch({
        type: postReducerType.SET_WAITING_QUANTITY,
        payload: waitingData.data.length,
      });
    }
    data.map(value => {
      if (value) {
        value.type = 'waiting';
        value.into_money = formatMoney(value.into_money);
        value.status = value.house && value.house.status === 0 ? 'Mở bán' : ' Ngưng bán';
        value[`${value.post_address}_link`] = 'Chờ đăng';
        value.house_address = `${value.house_number} ${value.house_address}, ${value.district}...`;
      }
      return value;
    });
  }
  const postGroupBy = _.groupBy(data, x => x && x.house_id);
  Object.values(postGroupBy).forEach(value => {
    const postElement = value.reduce(
      (total, element) => element !== null && { ...total, ...element },
    );
    postElement && waitingArray.push(postElement);
  });

  dispatch({
    type: postReducerType.WAIT_FOR_POSTING_SET_DATA,
    payload: waitingArray,
  });

  dispatch({
    type: postReducerType.DONE,
  });
};

export const getErrorData = conditions => async (dispatch, getState, thunkDependencies) => {
  const errorArray = [];
  dispatch({
    type: postReducerType.WAITING,
  });
  const errorData = await thunkDependencies.user.getPostManagerErrorList(conditions);
  const data = errorData.data.data ? errorData.data.data : errorData.data;
  if (data) {
    if (errorData.data) {
      dispatch({
        type: postReducerType.SET_ERROR_QUANTITY,
        payload: errorData.data.length,
      });
    }
    data.map(value => {
      if (value) {
        value.into_money = formatMoney(value.into_money);
        value.status = value.status === 0 ? 'Mở bán' : ' Ngưng bán';
        value[`${value.post_address}_link`] = 'Lỗi tin';
        value.house_address = `${value.house_number} ${value.house_address}, ${value.district}...`;
      }
      return value;
    });
  }
  const postGroupBy = _.groupBy(data, x => x && x.house_id);
  Object.values(postGroupBy).forEach(value => {
    const postElement = value.reduce(
      (total, element) => element !== null && { ...total, ...element },
    );
    postElement && errorArray.push(postElement);
  });

  dispatch({
    type: postReducerType.ERROR_POST_SET_DATA,
    payload: errorArray,
  });

  dispatch({
    type: postReducerType.DONE,
  });
};
export const closeModal = () => async dispatch => {
  dispatch({ type: alonhadatReducerType.CLOSE_MODAL });
};
export const openModal = () => async dispatch => {
  dispatch({ type: alonhadatReducerType.OPEN_MODAL });
};
export const fetch = houseId => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const response = await thunkDependencies.house.detail(houseId);
    const result = apiSerializer(response);
    dispatch(fetchDone(result));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};

export const deletePosted = houseId => async (dispatch, getState, thunkDependencies) => {
  const response = await thunkDependencies.user.deleteAlonhadatPosted({
    houseId,
    type: 1,
  });
};
export const getPostList = () => async (dispatch, getState, thunkDependencies) => {
  dispatch({ type: alonhadatReducerType.LOADING, payload: true });
  const [alonhadat, batdongsan] = await Promise.all([
    thunkDependencies.house.getAlonhadat(),
    thunkDependencies.house.getBatdongsan(),
  ]);
  const mergeData = {
    waiting: [...alonhadat.data.waiting, ...batdongsan.data.waiting],
    posted: [...alonhadat.data.posted, ...batdongsan.data.posted],
    failed: [...alonhadat.data.failed, ...batdongsan.data.failed],
    waitingApproval: [...alonhadat.data.waitingApproval, ...batdongsan.data.waitingApproval],
  };
  dispatch({ type: alonhadatReducerType.UPDATE, payload: mergeData });
  dispatch({ type: alonhadatReducerType.LOADING_DONE });
};

export const addOrUpdateAutoPost = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { data } = getState().alonhadat[REDUCERS.NEW_POST];
    const { type } = getState().alonhadat[REDUCERS.POST_STATE];
    let publicImageIds = data.public_image.filter(im => im.id).map(im => im.id);
    const newPublicImages = data.public_image.filter(im => !im.id);
    const rotateImageList = data.public_image.filter(im => im.id && im.rotation);
    if (newPublicImages.length) {
      await dispatch(uploadImages(newPublicImages, data.newName));
      const publicJobs = getState().house[REDUCERS.UPLOAD_JOBS].jobs;
      publicImageIds = publicImageIds.concat(
        _.values(publicJobs.results)
          .filter(r => r.id)
          .map(r => r.id),
      );
    }

    if (rotateImageList.length) {
      await dispatch(rotateImages(rotateImageList));
    }

    const house = getState().alonhadat[REDUCERS.POST_DETAIL_DATA].data;
    const changedData = {};
    _.forIn(data, (value, key) => {
      if (key === 'into_money') {
        changedData.into_money = convertUnitIntoNumber(data.into_money, data.money_unit || 'bil');
      }
      if (value !== house[key] && key !== 'into_money') {
        changedData[key] = value;
      }
    });

    changedData.public_image = publicImageIds;
    changedData.direction = data.direction ? [data.direction] : undefined;
    changedData.descriptions = data.descriptions ? data.descriptions : undefined;
    changedData.key_word = data.key_word ? data.key_word : undefined;
    changedData.initialDescription = data.description
      ? convertStringToHTML(data.description)
      : undefined;
    if (type === postActionType.update_post) {
      changedData.id = data.id;
      changedData.house_id = house.house_id;
    }
    if (type === postActionType.update_posted) {
      changedData.id = data.id;
      changedData.house_id = house.house_id;
      changedData.update_status = 1;
      changedData.post = 1;
      changedData.failed_quantity = 0;
    }
    dispatch(submitting());
    const form = {
      ...data,
      into_money: convertUnitIntoNumber(data.into_money, data.money_unit),
      public_image: publicImageIds,
      direction: data.direction ? [data.direction] : undefined,
    };
    if (type === postActionType.create || type === postActionType.confirm) {
      form.addressPost.forEach(addr => {
        if (addr === 'Alonhadat') {
          thunkDependencies.house.addAlonhadat({ ...data, ...changedData });
        }
        if (addr === 'Batdongsan') {
          changedData.type_news = changedData.batdongsan_type_news;
          changedData.type_news_day = changedData.batdongsan_type_news_days;
          thunkDependencies.house.addBatdongsan({ ...data, ...changedData });
        }
      });
    }
    if (type === postActionType.update_post || type === postActionType.update_posted) {
      form.post_address === 'alonhadat.vn' && thunkDependencies.house.updateAlonhadat(changedData);
      form.post_address === 'batdongsan.com.vn' &&
        thunkDependencies.house.updateBatdongsan(changedData);
    }
    dispatch(closeModal());
    dispatch(submitDone());
    dispatch(setDefaultTypeForCreatePost());
    dispatch(setId(null));
    dispatch(reset());
    dispatch(
      getWaitingData({
        startAt: null,
        endAt: null,
        staff: null,
        page: 1,
      }),
    );
    toast.success('Cập nhật thành công');
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
    dispatch(submitError(message));
  }
};

export const fetchHouse = id => async (dispatch, getState) => {
  dispatch(waiting());
  await dispatch(fetch(id));
  const house = getState().alonhadat[REDUCERS.POST_DETAIL_DATA].data;
  houseFields.forEach(field => {
    if (field === 'house_direction') {
      dispatch(handleChange('direction', house[field][0] ? house[field][0].direction : null));
    } else if (field === 'house_balcony_direction') {
      dispatch(
        handleChange(
          'balcony_direction',
          house[field].map(item => item.balcony),
        ),
      );
    } else if (field === 'public_image') {
      const images = _.values(house.image.public).map(img => ({
        ...img,
        url: formatImageUrl(img.main),
      }));
      dispatch(handleChange(field, images));
    } else if (field === 'into_money') {
      const { purpose } = house;
      const [moneyOnly, moneyUnit] = convertNumberIntoUnit(house.into_money, purpose);
      dispatch(handleChange('into_money', moneyOnly));
      dispatch(handleChange('money_unit', moneyUnit));
    } else {
      dispatch(handleChange(field, house[field]));
    }
  });
  dispatch(waitingDone());
};
export const fetchAlonhadat = houseId => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const response = await thunkDependencies.house.getDetailAlonhadat(houseId);
    const result = apiSerializer(response);
    dispatch(fetchDone(result));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};
export const fetchBatdongsan = houseId => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const response = await thunkDependencies.house.getDetailBatdongsan(houseId);
    const result = apiSerializer(response);
    dispatch(fetchDone(result));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};
export const fetchPostDetail = (id, addressToUpdate) => async (dispatch, getState) => {
  dispatch(waiting());
  if (addressToUpdate === 'alonhadat.vn') {
    await dispatch(fetchAlonhadat(id));
  } else if (addressToUpdate === 'batdongsan.com.vn') {
    await dispatch(fetchBatdongsan(id));
  }
  const house = getState().alonhadat[REDUCERS.POST_DETAIL_DATA].data;
  houseFields.forEach(field => {
    if (field === 'house_direction') {
      dispatch(
        handleChange(
          'direction',
          house[field] && house[field][0] ? house[field][0].direction : null,
        ),
      );
    } else if (field === 'house_balcony_direction' && house[field]) {
      dispatch(
        handleChange(
          'balcony_direction',
          house[field].map(item => item.balcony),
        ),
      );
    } else if (field === 'public_image' && house.image) {
      const images = _.values(house.image[field === 'public_image' ? 'public' : 'internal']).map(
        img => ({
          ...img,
          url: formatImageUrl(img.main),
        }),
      );
      dispatch(handleChange(field, images));
    } else if (field === 'into_money') {
      const { purpose } = house;
      const [moneyOnly, moneyUnit] = convertNumberIntoUnit(house.into_money, purpose);
      dispatch(handleChange('into_money', moneyOnly));
      dispatch(handleChange('money_unit', moneyUnit));
    } else {
      dispatch(handleChange(field, house[field]));
    }
  });
  dispatch(waitingDone());
};

export const setAddressToUpdate = address => async dispatch => {
  dispatch({
    type: alonhadatReducerType.UPDATE_ADDRESS_UPDATE,
    payload: address,
  });
};

export const setType = type => async (dispatch, getState) => {
  const prevType = getState().alonhadat[REDUCERS.POST_STATE].type;

  dispatch({
    type: alonhadatReducerType.SET_PREV_TYPE,
    payload: prevType,
  });

  dispatch({
    type: alonhadatReducerType.SET_TYPE,
    payload: type,
  });
};

export const setPostType = type => async dispatch => {
  console.log('postType', type);
  dispatch({
    type: alonhadatReducerType.SET_POST_TYPE,
    payload: type,
  });
};

export const setPost = data => async dispatch => {
  dispatch({
    type: postReducerType.POSTED_SET_DATA,
    payload: data,
  });
};
export const setLoading = (bool = false) => async dispatch => {
  dispatch({
    type: bool ? postReducerType.WAITING : postReducerType.DONE,
  });
};

// to do
export const setPrevIdHouse = id => async dispatch => {
  dispatch({
    type: alonhadatReducerType.SET_PREV_ID,
    payload: id,
  });
};
export default {
  setType,
  setId,
  closeModal,
  openModal,
  fetchPostDetail,
  getPostList,
  setPostType,
  addOrUpdateAutoPost,
  fetchHouse,
  handleChange,
  setAddressToUpdate,
  deletePosted,
  getPostData,
  setPost,
  setLoading,
  getWaitingData,
  getErrorData,
  setDefaultTypeForCreatePost,
  setPrevIdHouse,
};
