/* eslint-disable no-underscore-dangle */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { Tag, Row, Col } from 'antd';

import { Card, Button, Spacer } from '../../../components';
import HtmlDescription from '../../Common/HtmlDescription/HtmlDescription';
import { formatMoney } from '../../../helpers/common';
import { FIELDS } from '../../../constants/DETAIL_POST_FIELDS';
import { postActionType } from '../../../constants/COMMON';
import 'antd/dist/antd.css';

const notDetail = ['internal_image', 'public_image', 'image', 'house_direction'];

export const alonhadatColumns = [
  {
    title: '#',
    dataIndex: 'house_id',
    visible: true,
    width: 20,
    render: (value, item) => (
      <div style={{ padding: '5px' }}>
        <h4 style={{ textAlign: 'center' }}>{value}</h4>
      </div>
    ),
  },

  {
    title: 'Giá',
    dataIndex: 'price',
    visible: true,
    width: 20,
    operation: 'between',
    render: (value, house) => (
      <span>
        <div className="price">{formatMoney(house.price)}</div>
      </span>
    ),
  },
  {
    title: 'Địa chỉ',
    dataIndex: 'houseAddress',
    visible: true,
    width: 20,
    render: (value, house) => (
      <span>
        <div className="bold-primary-text long-text">
          {`${house.houseAddress} ${house.project ? `${house.project}` : house.street}`}
        </div>
      </span>
    ),
  },
  {
    title: 'Ngang',
    dataIndex: 'width',
    visible: true,
    render: (area, house) => (
      <>
        <div className="bold-primary-text">{`${house.width || 0} m`}</div>
      </>
    ),
    width: 15,
  },
  {
    title: 'Dài',
    dataIndex: 'length',
    visible: true,
    render: (length, house) => (
      <>
        <div className="bold-primary-text">{`${house.length || 0} m`}</div>
      </>
    ),
    width: 10,
  },
  {
    title: 'Dự án',
    dataIndex: 'project',
    visible: true,
    render: (value, house) => house.project || 'Không có',
    width: 20,
  },
  {
    title: 'Khu vực',
    dataIndex: 'district',
    visible: true,
    render: house => (
      <span>
        <div style={{ fontWeight: 'bold', fontSize: 13 }}>{house.district}</div>
        <div className="small-text">{house.ward}</div>
      </span>
    ),
    width: 20,
  },
  {
    title: 'Loại BĐS',
    dataIndex: 'houseType',
    visible: true,
    render: value => (
      <span>
        <div style={{ fontWeight: 'bold', fontSize: 13 }}>{value}</div>
      </span>
    ),
    width: 20,
  },
  {
    title: 'Diện tích',
    dataIndex: 'area',
    visible: true,
    render: value => `${value || 0}m2`,
    width: 15,
  },
  {
    title: 'Pháp Lý',
    dataIndex: 'juridical ',
    visible: true,
    render: value => (
      <span>
        <div style={{ fontWeight: 'bold', fontSize: 13 }}>
          {value === 1 ? 'Sổ đỏ/ Sổ hồng' : 'Giấy tờ hợp lệ'}
        </div>
      </span>
    ),
    width: 10,
  },
  {
    title: 'Số lầu',
    dataIndex: 'floors',
    visible: true,
    width: 15,
  },
  {
    title: 'Hướng',
    dataIndex: 'direction',
    visible: true,
    render: (value, house) => (
      <span>
        {house.house_direction
          ? house.house_direction.map(d => {
              const color = 'green';
              return (
                <Tag color={color} key={d.direction}>
                  {d.direction}
                </Tag>
              );
            })
          : 'Không xác định'}
      </span>
    ),
    width: 30,
  },
  {
    title: 'Số phòng ngủ',
    dataIndex: 'room',
    visible: true,
    width: 10,
  },
  {
    title: 'Đường rộng (m)',
    dataIndex: 'roadInFrontOfHouse ',
    visible: true,
    render: (value, house) => `${house.roadInFrontOfHouse}m`,
    width: 10,
  },
  {
    title: 'Nhà bếp',
    dataIndex: 'kitchen',
    visible: true,
    width: 10,
    render: (value, house) => (
      <span>
        <div className="purpose">{house.kitchen === 1 ? 'Có' : 'Không'}</div>
      </span>
    ),
  },
  {
    title: 'Phòng ăn',
    dataIndex: 'diningRoom',
    visible: true,
    width: 10,
    render: (value, house) => (
      <span>
        <div className="purpose">{house.diningRoom === 1 ? 'Có' : 'Không'}</div>
      </span>
    ),
  },
  {
    title: 'Sân thượng',
    dataIndex: ' terrace ',
    visible: true,
    width: 10,
    render: (value, house) => (
      <span>
        <div className="purpose">{house.terrace === 1 ? 'Có' : 'Không'}</div>
      </span>
    ),
  },
  {
    title: 'Chổ để xe hơi',
    dataIndex: 'carParking',
    visible: true,
    width: 10,
    render: (value, house) => (
      <span>
        <div className="purpose">{house.carParking === 1 ? 'Có' : 'Không'}</div>
      </span>
    ),
  },
  {
    title: 'Nhân viên',
    dataIndex: 'user',
    visible: true,
    width: 10,
    render: value => (
      <span>
        <div className="purpose">{value ? value.name : 'Không'}</div>
      </span>
    ),
  },
  {
    title: 'Loại',
    dataIndex: 'newType',
    visible: true,
    width: 10,
    render: house => (
      <span>
        <div className="purpose">{house.purpose === 1 ? 'THUÊ' : 'BÁN'}</div>
      </span>
    ),
  },
  {
    title: 'Nguồn tin',
    dataIndex: 'post_address',
    visible: true,
    width: 10,
    render: (value, house) => (
      <span>
        <div className="purpose">{value}</div>
      </span>
    ),
  },
];

export const postDetail = ({
  record,
  options,
  deletePost,
  setType,
  setId,
  openModal,
  addressToUpdate,
}) => {
  return (
    <div>
      <div className="detailPost">
        {record.type === 'posted' && (
          <Row gutter={[10, 10]}>
            <Button
              small
              onClick={() => {
                openModal();
                setType(postActionType.update_posted);
                setId(record.house_id);
              }}
              text="Sửa"
            />
            <Spacer />
            <Button small onClick={() => deletePost(record)} text="Xóa" />
          </Row>
        )}
        <Spacer />
        <Card containerStyle={{ marginBottom: 10 }} shadow>
          <Row gutter={[10, 10]}>
            {FIELDS.filter(key => !notDetail.includes(key)).map(field => (
              <Col md={6} sm={8} xs={12}>
                <div>
                  <div style={{ fontWeight: 'bold', fontSize: 13 }}>{field.label}</div>
                  <div className="small-text">
                    {field.format && record[addressToUpdate]
                      ? field.format(record[addressToUpdate], options)
                      : record[field.property]}
                  </div>
                </div>
              </Col>
            ))}
          </Row>
        </Card>
        <HtmlDescription convert={false} description={record.initialDescription} />
      </div>
    </div>
  );
};
