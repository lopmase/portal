/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { Popover } from 'antd';
import { faPlus, faSearch, faCog, faTable } from '@fortawesome/free-solid-svg-icons';

import { Button } from '../../../../components';
import { postActionType } from '../../../../constants/COMMON';
import { isAdmin } from '../../../../helpers/roleUtil';
import './actionBox.scss';

export default function ActionBox({
  resetFields,
  openModal,
  setHouseDirect,
  setType,
  setUpdateHouse,
  setUpdateOption,
  getPostList,
  me,
}) {
  const [visible, setVisible] = useState(false);
  const content = (
    <div className="listBtn">
      {me && isAdmin(me) && (
        <Button
          text="Tạo nguồn tin"
          onClick={() => {
            setVisible(false);
            resetFields();
            openModal();
            setType(Number(postActionType.create_post_address));
            setUpdateHouse(null);
          }}
        />
      )}
      <Button
        text="Tùy chỉnh"
        onClick={() => {
          setVisible(false);
          openModal();
          setType(Number(postActionType.set_option));
          setUpdateOption(true);
        }}
      />
      <Button
        text="Danh sách nguồn tin"
        onClick={() => {
          setVisible(false);
          openModal();
          setType(Number(postActionType.address_setting));
        }}
      />
      <Button
        text="Danh sách Alonhadat"
        onClick={() => {
          getPostList();
          setVisible(false);
          openModal();
          setType(Number(postActionType.post_list));
        }}
      />
    </div>
  );
  return (
    <div className="actionBox">
      <Popover
        visible={visible}
        content={content}
        placement="bottomLeft"
        title="Tuỳ chỉnh"
        trigger="click"
      >
        <Button
          containerStyle={{ marginLeft: 10, paddingRight: 13, paddingLeft: 13 }}
          icon={faCog}
          iconColor="gray"
          iconOnly
          backgroundColor="white"
          onClick={() => setVisible(!visible)}
        />
      </Popover>
    </div>
  );
}
