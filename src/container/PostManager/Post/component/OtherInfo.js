/* eslint-disable no-useless-escape */
/* eslint-disable prettier/prettier */
/* eslint-disable no-empty */
import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Card, Flexbox, FormInput, Spacer, FormDropdown, Input } from '../../../../components';
import { showMessage } from '../../../Common/GlobalErrorModal/action';

const OtherInfo = ({ form, handleChange }) => {
  const { purpose } = form.data;
  const onChange = (name, value) => {
    const valueChange = value.replace(/[^0-9\.]+/g, '');
    handleChange(name, valueChange);
  };

  return (
    <Card shadow>
      <Flexbox row flexStart containerStyle={{}}>
        <div style={{ flex: 1 }}>
          <FormInput name="into_money" label="Giá" form={form} handleChange={onChange} />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormDropdown
            name="money_unit"
            label="Đơn vị"
            form={form}
            search
            handleChange={handleChange}
            items={
              purpose === 0
                ? [
                    { text: 'Triệu', value: 'mil' },
                    { text: 'Tỷ', value: 'bil' },
                  ]
                : [
                    { text: 'Triệu/tháng', value: 'mpm' },
                    { text: 'Tỷ/tháng', value: 'bpm' },
                  ]
            }
          />
        </div>
      </Flexbox>
    </Card>
  );
};

OtherInfo.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
};

OtherInfo.defaultProps = {};

export function mapStateToProps(state) {
  return {};
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      showError: showMessage,
    },
    dispatch,
  );
}

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(OtherInfo);
