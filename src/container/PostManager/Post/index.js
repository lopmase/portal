/* eslint-disable consistent-return */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/prop-types */
/* eslint-disable no-param-reassign */
import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';
import { Modal, Form, Space, DatePicker, Input, Table, Tabs } from 'antd';
import { bindActionCreators } from 'redux';
import { connect, useDispatch } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { compose } from 'recompose';
import moment from 'moment';
import { Button, Card } from '../../../components';
import ActionBox from './component/ActionBox';
import SelectOption from './SelectOption';
import AddressModal from './AddressModal';
import Layout from '../../ShellContainner/Layout/Layout';
import { mappingUser, formatMoney } from '../../../helpers/common';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import AntTable from '../../../components/AntTable/AntTable';
import FormPost from './FormPost';
import { ROUTES } from '../../../constants/ROUTES';
import {
  postActionType,
  postManagerHistoryColumns,
  REDUCERS,
  alonhadatReducerType,
} from '../../../constants/COMMON';
import { confirm } from '../../ShellContainner/ConfirmDialog/action';
import action from './action';
import { CONFIRM_DELETE_POST } from '../../../constants/MESSAGE';
import UserArchive from '../../../components/UserArchive/UserArchive';
import AlonhadatPostList from './AlonhadatPostList';
import * as streetActions from '../../HouseContainer/Metadata/streetAction';
import * as optionsAction from '../../HouseContainer/Metadata/optionsAction';
import DetailPost from './DetailPost';

import {
  getHouse,
  createPostManager,
  updatePostManager,
  createPostAddress,
  getPostAddressStatusActive,
  getPostAddressStatus,
  getPostAddress,
  deletePostManager,
  updatePostAddress,
  getUserListByManagerId,
  getPostHistory,
} from '../../../apis/userApi';
import './style.scss';

const Post = ({
  me,
  setPostType,
  postType,
  type,
  setType,
  confirmDialog,
  houseToPost,
  setId,
  waiting,
  addOrUpdateAutoPost,
  getPostList,
  isOpenModal,
  closeModal,
  openModal,
  loadStreets,
  getOptions,
  deletePosted,
  getPostData,
  resetFields,
  post,
  setPost,
  setLoading,
  loading,
  postQuantity,
  getWaitingData,
  getErrorData,
  lastPage,
  waitingList,
  errorList,
  options,
  setAddressToUpdate,
  addressToUpdate,
  prevType,
  ...houseState
}) => {
  const [house, setHouse] = useState();
  const [updateHouse, setUpdateHouse] = useState();
  const [updateAddress, setUpdateAddress] = useState(null);
  const [updateOption, setUpdateOption] = useState(false);
  const [subColumns, setSubColumns] = useState([]);
  const [addressStatus, setAddressStatus] = useState([]);
  const [historyValue, setHistory] = useState([]);
  const [columns, setColumns] = useState([]);
  const [addressLst, setAddressLst] = useState([]);
  const [houseDirect, setHouseDirect] = useState();
  const [searchValue, setSearchValue] = useState();
  const [postDetail, setPostDetailData] = useState();
  const [tabsIndex, setTabsIndex] = useState(1);
  const [condition, setCondition] = useState({
    startAt: null,
    endAt: null,
    staff: null,
    search: null,
  });
  const [cell, setCell] = useState();
  const [users, setUsers] = useState();
  const { TabPane } = Tabs;
  const pageRef = useRef(1);
  const firstRef = useRef(false);
  const [form] = Form.useForm();
  const { RangePicker } = DatePicker;
  const dispatch = useDispatch();

  const deviceMode = true;
  const mainCard = {
    flex: 1,
    padding: 20,
    minWidth: '100%',
  };
  const getData = async conditions => {
    try {
      if (subColumns && subColumns.length > 0) {
        getPostData(conditions);
        getWaitingData(conditions);
        getErrorData(conditions);
      }
    } catch (err) {
      dispatch(showMessage(err.message));
      return false;
    }
  };
  const update = value => {
    form.resetFields();
    setUpdateHouse(value);
    openModal();
    setType(postActionType.update);
  };
  const deletePost = value => {
    setType(Number(postActionType.delete));
    confirmDialog(CONFIRM_DELETE_POST, async result => {
      if (result) {
        const deletedPost = await deletePostManager({ house_id: value.house_id });
        if (deletedPost) {
          toast.success('Xóa thành công');
          setPost([]);
          getData({ ...condition, page: 1 });
          pageRef.current = 1;
        }
      }
    });
  };
  const getAddress = async () => {
    try {
      setLoading(true);
      const [houseData, addressActive, postAddressStatus, address, userList] = await Promise.all([
        getHouse(),
        getPostAddressStatusActive(),
        getPostAddressStatus(),
        getPostAddress(),
        getUserListByManagerId(),
      ]);
      setAddressStatus(postAddressStatus.data);
      setAddressLst(address.data);
      const { admin, saleAdmin, seniorManager, manager, teamLeader, staffs } = mappingUser(
        userList,
      );
      setUsers({ admin, saleAdmin, seniorManager, manager, teamLeader, staffs });
      houseData.data.map(value => {
        value.into_money = formatMoney(Number(value.into_money));
        value.length = value.length ? `${value.length} m` : '0 m';
        value.width = value.width ? `${value.width} m` : '0 m';
        value.house_address = `${value.house_number} ${value.house_address}, ${value.district}, ${value.province}`;
        return value;
      });
      addressActive.data.length > 0 &&
        addressActive.data.map(value => {
          value.title = value.channel;
          value.dataIndex = `${value.channel}_link`;
          value.key = `${value.channel}_link`;
          value.width = 140;
          value.render = (text, record) => (
            <b
              onClick={() => {
                console.log('record', record);
                setAddressToUpdate(value.channel);
                setPostDetailData(record);
                !record.manual && openModal();
                setType(postActionType.detail_post);
                setCell(value);
              }}
            >
              {text}
            </b>
          );

          value.sorter = (a, b) => a.created_at - b.created_at;
          return value;
        });
      setHouse(houseData.data);
      setSubColumns(addressActive.data);
      return true;
    } catch (err) {
      dispatch(showMessage(err.message));
      return false;
    }
  };

  const history = async value => {
    setType(Number(postActionType.history));
    openModal();
    const actionHistory = await getPostHistory({
      created_at: value.created_at,
      house_id: value.house.id,
    });
    setHistory(actionHistory.data);
  };

  useEffect(() => {
    loadStreets();
    getOptions();
    getAddress({ ...condition, page: pageRef.current });
  }, []);
  useEffect(() => {
    const waitingContent = document.querySelector('.waiting .ant-table-body');

    const postedContent = document.querySelector('.posted .ant-table-body');

    const errorContent = document.querySelector('.error .ant-table-body');

    if (waitingContent) {
      waitingContent.addEventListener('scroll', event => {
        const maxScroll = event.target.scrollHeight - event.target.clientHeight;
        const currentScroll = event.target.scrollTop;
        if (Math.floor(currentScroll) >= maxScroll - 50 && pageRef.current < lastPage && !loading) {
          pageRef.current += 1;
          getData({ ...condition, page: pageRef.current });
        }
      });
    }
    if (postedContent) {
      postedContent.addEventListener('scroll', event => {
        const maxScroll = event.target.scrollHeight - event.target.clientHeight;
        const currentScroll = event.target.scrollTop;
        if (Math.floor(currentScroll) >= maxScroll - 50 && pageRef.current < lastPage && !loading) {
          pageRef.current += 1;
          getData({ ...condition, page: pageRef.current });
        }
      });
    }
    if (errorContent) {
      errorContent.addEventListener('scroll', event => {
        const maxScroll = event.target.scrollHeight - event.target.clientHeight;
        const currentScroll = event.target.scrollTop;
        if (Math.floor(currentScroll) >= maxScroll - 50 && pageRef.current < lastPage && !loading) {
          pageRef.current += 1;
          getData({ ...condition, page: pageRef.current });
        }
      });
    }
  });
  useEffect(() => {
    if (houseState.location.state && subColumns) {
      setHouseDirect({
        house_id: houseState.location.state.house_id,
        house_address: `${houseState.location.state.house_number} ${houseState.location.state.house_address}, ${houseState.location.state.district}, ${houseState.location.state.province}`,
      });
      openModal();
      setType(postActionType.create);
    }
  }, [houseState.location.state]);
  useEffect(() => {
    try {
      if (subColumns.length > 0 && firstRef.current === false) {
        getData({ ...condition, page: pageRef.current });
        firstRef.current = true;
      }
      if (subColumns.length > 0) {
        const column = [
          {
            title: 'STT',
            dataIndex: 'ordinal_number',
            width: 80,
            key: 'ordinal_number',
            sorter: (a, b) => a.ordinal_number - b.ordinal_number,
          },
          {
            title: 'ID',
            dataIndex: 'house_id',
            width: 80,
            key: 'house_id',
            sorter: (a, b) => a.house_id - b.house_id,
            filters: post.map(val => {
              return { value: val.house_id, text: val.house_id };
            }),
            filterMode: 'menu',
            filterSearch: true,
            onFilter: (values, record) => record.house_id === values,
          },
          {
            title: 'Địa chỉ',
            dataIndex: 'house_address',
            width: 120,
            key: 'house_address',
            render: value => <h4>{value}</h4>,
          },
          ...subColumns,
          {
            title: 'Giá',
            dataIndex: 'into_money',
            width: 120,
            key: 'into_money',
            render: (text, record) => {
              if (
                record &&
                record.action &&
                record.action.length > 0 &&
                record.action[0].column &&
                record.action[0].column === 'into_money' &&
                Number(record.action[0].old_value) !== Number(record.action[0].new_value)
              ) {
                return (
                  <span
                    style={{
                      backgroundColor: '#ff6c00',
                      fontSize: '18px',
                      color: 'white',
                      fontWeight: '13px',
                      borderRadius: '8px',
                      padding: '0 10px',
                    }}
                  >
                    {text}
                  </span>
                );
              }
              return <span style={{ fontSize: '18px' }}>{text}</span>;
            },
          },
          {
            title: 'Trạng thái',
            dataIndex: 'status',
            width: 80,
            key: 'status',
          },
          {
            title: 'Xem',
            key: 'detail',
            width: 120,
            render: (text, record) => (
              <Link to={`${ROUTES.HOUSE_ROOT}/${record.house_id}`}>
                <Button small text="Xem" />
              </Link>
            ),
          },
          {
            title: 'Chỉnh sửa',
            key: 'update',
            width: 120,
            render: (text, record) => (
              <Button
                small
                onClick={() => {
                  update(record);
                }}
                text="SỬA"
              />
            ),
          },
          {
            title: 'Lịch sử',
            key: 'history',
            width: 120,
            render: (text, record) => (
              <Button small onClick={() => history(record)} text="Lịch sử" />
            ),
          },
          {
            title: 'Xóa',
            key: 'history',
            width: 120,
            render: (text, record) => (
              <Button small onClick={() => deletePost(record)} text="Xóa" />
            ),
          },
        ];
        setColumns(column);
      }
    } catch (err) {
      dispatch(showMessage('Something went wrong'));
    }
  }, [post, subColumns]);

  const onFinish = async values => {
    let res;
    try {
      if (type === postActionType.create && postType !== 1) {
        res = await createPostManager(values);
      } else if (prevType === 1) {
        addOrUpdateAutoPost();
      } else if (
        type === postActionType.update_post ||
        type === postActionType.update_posted ||
        (type === postActionType.create && postType === 1 && prevType !== postActionType.create)
      ) {
        setPostDetailData({ ...houseToPost.data, type: 'confirm' });
        setType(postActionType.confirm);
      } else if (type === postActionType.update) {
        res = await updatePostManager({ ...values, house_id: updateHouse.house_id });
      } else if (type === postActionType.create_post_address) {
        res = await createPostAddress(values);
        getAddress();
      } else if (type === postActionType.address_setting) {
        res = await updatePostAddress({
          channel: updateAddress.channel,
          new_channel: values.channel,
        });
        getAddress();
      }
      if (res && postType !== 1) {
        pageRef.current = 1;
        toast.success('SuccessFull');
        getData(condition);
      }
      if (
        !res &&
        postType !== 1 &&
        type !== postActionType.update_post &&
        type !== postActionType.update_posted
      ) {
        toast.error('Failed');
      }
      res && closeModal();
    } catch (err) {
      dispatch(showMessage(err.message));
    }
  };
  const rangePickerOnChange = value => {
    if (value) {
      const from = value[0];
      const to = value[1];
      const timeConditions = {
        startAt: moment(from)
          .set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
          .utcOffset(0, true)
          .format(),
        endAt: moment(to)
          .set({ hour: 23, minute: 59, second: 59, millisecond: 59 })
          .utcOffset(0, true)
          .format(),
      };
      setCondition({ ...condition, ...timeConditions });
    } else {
      setCondition({ startAt: null, endAt: null });
    }
  };
  const Search = searchCondition => {
    setPost([]);
    searchCondition && getData(searchCondition);
    pageRef.current = 1;
  };

  useEffect(() => {
    setCondition({ ...condition, search: searchValue });
    setPost([]);
    const timeOut = setTimeout(() => {
      searchValue && getData({ ...condition, search: searchValue, page: 1 });
      pageRef.current = 1;
    }, [1000]);
    return () => clearTimeout(timeOut);
  }, [searchValue]);
  return (
    <div className="container">
      <Layout
        renderHeader={() => (
          <>
            <div className="postHeader">
              <h1>Quản lý tin đăng</h1>
              <ActionBox
                resetFields={form.resetFields}
                openModal={openModal}
                setHouseDirect={setHouseDirect}
                setType={setType}
                setUpdateHouse={setUpdateHouse}
                me={me}
                setUpdateOption={setUpdateOption}
                getPostList={getPostList}
              />
            </div>
            <div>
              <Button
                text="Tạo bài đăng"
                onClick={() => {
                  form.resetFields();
                  openModal();
                  setHouseDirect(null);
                  setType(Number(postActionType.create));
                  setUpdateHouse(null);
                }}
              />
            </div>
          </>
        )}
      />

      <Card shadow containerStyle={mainCard}>
        <RangePicker
          // format="YYYY-MM-DD HH:mm:ss"
          onChange={rangePickerOnChange}
          className="rangePicker"
        />
        {users && (
          <UserArchive
            className="selectUserPost"
            users={users}
            onChange={staff => setCondition({ ...condition, staff })}
          />
        )}
        <Input className="inputSearch" onChange={e => setSearchValue(e.target.value)} />
        <Button
          containerStyle={{ marginLeft: '5px' }}
          text="Search"
          onClick={() => Search({ ...condition, page: 1 })}
        />
        <h4>
          {`Tổng cộng: 
          ${postQuantity}`}
        </h4>
      </Card>
      <Modal
        visible={isOpenModal}
        onCancel={() => {
          type !== postActionType.confirm && setPostType(null);
          type !== postActionType.confirm && setType(null);
          type !== postActionType.confirm && setId(null);
          type !== postActionType.confirm && closeModal();
          setUpdateAddress(null);
          type === postActionType.confirm && setType(1);
          houseDirect && setHouseDirect(null);
        }}
        onOk={form.submit}
        okButtonProps={
          type === postActionType.history ||
          type === postActionType.set_option ||
          type === postActionType.post_list ||
          (type === postActionType.create && !postType) ||
          (type === postActionType.address_setting && updateAddress === null)
            ? { style: { display: 'none' } }
            : { style: { backgroundColor: '#ff8731' } }
        }
        style={window.innerWidth > 900 ? { minWidth: '800px' } : null}
      >
        {!postType && type === postActionType.create && (
          <Space>
            <Button
              text="Nguồn tin mới"
              onClick={() => {
                setPostType(1);
              }}
            />
            <Button
              text="Nguồn tin có sẵn"
              onClick={() => {
                setPostType(2);
              }}
            />
          </Space>
        )}
        {(type === postActionType.update_posted ||
          type === postActionType.update_post ||
          type === postActionType.update ||
          type === postActionType.create_post_address ||
          type === postActionType.create) && (
          <FormPost
            postType={postType}
            house={house}
            form={form}
            houseDirect={houseDirect}
            postAddress={subColumns}
            onFinish={onFinish}
            waiting={waiting}
            me={me}
            initialValues={updateHouse}
            setHouseDirect={setHouseDirect}
          />
        )}
        {type && type === postActionType.history && historyValue && (
          <AntTable
            data={historyValue}
            columns={postManagerHistoryColumns.map(col => ({
              ...(deviceMode
                ? { ...col, fixed: undefined }
                : {
                    ...col,
                    width:
                      col.dataIndex === 'id' || col.dataIndex === 'avatar' ? col.width : undefined,
                  }),
            }))}
            autoSize={!deviceMode}
            width={900}
            height={400}
          />
        )}
        {updateOption && type === postActionType.set_option && (
          <SelectOption
            data={addressStatus}
            getAddress={getAddress}
            getData={getData}
            condition={condition}
          />
        )}
        {type === postActionType.address_setting && (
          <AddressModal
            data={addressLst}
            form={form}
            setUpdateAddress={setUpdateAddress}
            onFinish={onFinish}
          />
        )}
        {type === postActionType.post_list && <AlonhadatPostList />}
        {(type === postActionType.detail_post || type === postActionType.confirm) && (
          <DetailPost
            record={postDetail}
            options={options}
            deletePost={deletePost}
            setType={setType}
            setId={setId}
            openModal={openModal}
            addressToUpdate={addressToUpdate}
            type={type}
            cell={cell}
          />
        )}
      </Modal>

      <div className="mainContainer">
        <Tabs defaultActiveKey="1" onTabClick={value => setTabsIndex(value)}>
          <TabPane tab="Chờ đăng" key="1">
            <Table
              className="waiting"
              dataSource={waitingList}
              columns={columns.filter(
                item => item.key !== 'history' && item.key !== 'detail' && item.key !== 'update',
              )}
              loading={loading}
              size="middle"
              scroll={{ x: 1500, y: 750 }}
              pagination={false}
              locale={{
                triggerDesc: 'Sắp xếp theo bài đăng mới nhất',
                triggerAsc: 'Sắp xếp theo bài đăng cũ nhất',
                cancelSort: 'Hủy sắp xếp',
              }}
            />
          </TabPane>
          <TabPane tab="Đã đăng" key="2">
            <Table
              className="posted"
              dataSource={post}
              columns={columns}
              loading={loading}
              size="middle"
              scroll={{ x: 1500, y: 750 }}
              pagination={false}
              locale={{
                triggerDesc: 'Sắp xếp theo bài đăng mới nhất',
                triggerAsc: 'Sắp xếp theo bài đăng cũ nhất',
                cancelSort: 'Hủy sắp xếp',
              }}
            />
          </TabPane>
          <TabPane tab="Lỗi tin" key="3">
            <Table
              className="error"
              dataSource={errorList}
              columns={columns.filter(
                item => item.key !== 'history' && item.key !== 'detail' && item.key !== 'update',
              )}
              loading={loading}
              size="middle"
              scroll={{ x: 1500, y: 750 }}
              pagination={false}
              locale={{
                triggerDesc: 'Sắp xếp theo bài đăng mới nhất',
                triggerAsc: 'Sắp xếp theo bài đăng cũ nhất',
                cancelSort: 'Hủy sắp xếp',
              }}
            />
          </TabPane>
        </Tabs>
      </div>
    </div>
  );
};

Post.propTypes = {
  me: PropTypes.object,
  houseToPost: PropTypes.object,
  waiting: PropTypes.bool,
};
Post.defaultProps = {
  me: {},
  houseToPost: {},
  waiting: false,
};
export function mapStateToProps(state) {
  return {
    me: state.auth.user.data,
    options: state.metadata[REDUCERS.OPTIONS].data,
    houseToPost: state.alonhadat[REDUCERS.NEW_POST],
    waiting: state.alonhadat[REDUCERS.NEW_POST].waiting,
    isOpenModal: state.alonhadat[REDUCERS.POST_STATE].isOpenModal,
    type: state.alonhadat[REDUCERS.POST_STATE].type,
    prevType: state.alonhadat[REDUCERS.POST_STATE].prevType,
    postType: state.alonhadat[REDUCERS.POST_STATE].postType,
    post: state.alonhadat.POST.posted,
    waitingList: state.alonhadat.POST.waiting,
    errorList: state.alonhadat.POST.error,
    loading: state.alonhadat.POST.loading,
    postQuantity: state.alonhadat.POST.postQuantity,
    lastPage: state.alonhadat.POST.lastPage,
    addressToUpdate: state.alonhadat[REDUCERS.POST_STATE].addressToUpdate,
    postDetail: state.alonhadat[REDUCERS.POST_DETAIL_DATA].data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...action,
      ...streetActions,
      ...optionsAction,
      confirmDialog: confirm,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Post);
