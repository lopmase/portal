import { combineReducers } from 'redux';
import createFormReducer from '../../../helpers/createFormReducer';
import createDataReducer from '../../../helpers/createDataReducer';
import { alonhadatReducerType, REDUCERS, postReducerType } from '../../../constants/COMMON';

export const STATUS = {
  DONE: 2,
  LOADING: 1,
  ERROR: 3,
};

const initialValue = {
  id: 0,
  purpose: 0,
  internal_image: [],
  public_image: [],
  into_money: 0,
  brokerage_rate: 1,
  public: true,
  house_number: '',
  house_address: '',
  province: null,
  wide_street: 0,
  district: null,
  customer_id: null,
  city: 1,
  balcony_direction: [],
  type_news: 0,
  batdongsan_type_news: 5,
  commission: 0,
  type_news_day: 0,
  dining_room: 0,
  kitchen: 0,
  terrace: 0,
  car_parking: 0,
  post: 0,
  failed_quantity: 0,
  type_news_value: 0,
  batdongsan_type_news_days: 10,
  addressPost: ['Alonhadat'],
  money_unit: 'bil',
};
const rules = {
  house_number: [['required', 'Số nhà bắt buộc']],
  house_address: [['required', 'Đường bắt buộc']],
  province: [['required', 'Huyện bắt buộc']],
  district: [['required', 'Phường bắt buộc']],
  street_type: [['required', 'Loại đường bắt buộc']],
  // house_type: [['required', 'Loại nhà bắt buộc']],
  property_type: [['required', 'Loại bđs bắt buộc']],
  customer_id: [['required', 'Khách hàng bắt buộc']],
  direction: [['required', 'Hướng nhà bắt buộc']],
  floors: [['required', 'Số lầu bắt buộc']],
  into_money: [['required', 'Giá bắt buộc']],
};
const initialState = {
  loading: false,
  isOpenModal: false,
  idHouseToPost: null,
  addressToUpdate: null,
  fetchDone: false,
  fetching: false,
  fetchError: '',
  total: 0,
  type: null,
  postType: 0,
  data: [],
  results: {},
  prevType: 0,
  prevIdHouseToPost: null,
};

const postInitialValue = {
  loading: false,
  posted: [],
  waiting: [],
  error: [],
  lastPage: 0,
  postQuantity: 0,
  waitingQuantity: 0,
  errorQuantity: 0,
  currentPage: 0,
};

export const PostListReducer = (state = postInitialValue, action) => {
  switch (action.type) {
    case postReducerType.WAITING:
      return {
        ...state,
        loading: true,
      };

    case postReducerType.WAIT_FOR_POSTING_SET_DATA:
      return {
        ...state,
        waiting: action.payload,
      };
    case postReducerType.POSTED_SET_DATA:
      return {
        ...state,
        posted: action.payload,
      };
    case postReducerType.LAST_PAGE:
      return {
        ...state,
        lastPage: action.payload,
      };
    case postReducerType.ERROR_POST_SET_DATA:
      return {
        ...state,
        error: action.payload,
      };
    case postReducerType.SET_POST_QUANTITY:
      return {
        ...state,
        postQuantity: action.payload,
      };
    case postReducerType.SET_WAITING_QUANTITY:
      return {
        ...state,
        waitingQuantity: 0,
      };
    case postReducerType.SET_ERROR_QUANTITY:
      return {
        ...state,
        errorQuantity: action.payload,
      };
    case postReducerType.DONE:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
};

export const PostReducer = (state = initialState, action) => {
  switch (action.type) {
    case alonhadatReducerType.SET_POST_TYPE:
      return {
        ...state,
        postType: action.payload,
      };
    case alonhadatReducerType.SET_TYPE:
      return {
        ...state,
        type: action.payload,
      };
    case alonhadatReducerType.SET_PREV_TYPE:
      return {
        ...state,
        prevType: action.payload,
      };
    case alonhadatReducerType.SET_ID:
      return {
        ...state,
        idHouseToPost: action.payload,
      };
    case alonhadatReducerType.OPEN_MODAL:
      return {
        ...state,
        isOpenModal: true,
      };
    case alonhadatReducerType.CLOSE_MODAL:
      return {
        ...state,
        isOpenModal: false,
      };
    case alonhadatReducerType.GET:
      return {
        state,
      };
    case alonhadatReducerType.UPDATE:
      return {
        ...state,
        data: action.payload,
      };
    case alonhadatReducerType.LOADING:
      return {
        ...state,
        loading: true,
      };
    case alonhadatReducerType.LOADING_DONE:
      return {
        ...state,
        loading: false,
      };
    case alonhadatReducerType.UPDATE_ADDRESS_UPDATE:
      return {
        ...state,
        addressToUpdate: action.payload,
      };

    case alonhadatReducerType.SET_DEFAULT_TYPE:
      return {
        ...state,
        type: null,
        postType: 0,
      };
    case alonhadatReducerType.SET_PREV_ID:
      return {
        ...state,
        prevIdHouseToPost: action.payload,
      };
    default:
      return state;
  }
};

export default combineReducers({
  [REDUCERS.POST_STATE]: PostReducer,
  [REDUCERS.NEW_POST]: createFormReducer(REDUCERS.NEW_POST, initialValue, rules),
  [REDUCERS.POST_DETAIL_DATA]: createDataReducer(REDUCERS.POST_DETAIL_DATA),
  POST: PostListReducer,
});
