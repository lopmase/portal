import React, { Component } from 'react';
import { PrivateRoute } from '../../components';
import { ROUTES } from '../../constants/ROUTES';
import Projects from './Projects';
import NewProject from './NewProject';
import ProjectQA from './Project_QA';

class ProjectContainer extends Component {
  state = {};

  render() {
    return (
      <div className="customer-container">
        <PrivateRoute path={ROUTES.PROJECTS} component={Projects} />
        <PrivateRoute path={ROUTES.NEW_PROJECT} component={NewProject} />
        <PrivateRoute exact path={ROUTES.PROJECT_QA} component={ProjectQA} />
        <PrivateRoute
          path={ROUTES.EDIT_PROJECT}
          component={props => <NewProject {...props} editMode />}
        />
      </div>
    );
  }
}

export default ProjectContainer;
