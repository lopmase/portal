import _ from 'lodash';
import { push } from 'connected-react-router';
import { toast } from 'react-toastify';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateFormActions } from '../../../helpers/formAction';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { uploadImages } from '../../HouseContainer/UploadJobs/action';
import { ROUTES } from '../../../constants/ROUTES';
import { generateModalActions } from '../../../helpers/modalAction';
import Selector from '../Selector';
import { generateDataActions } from '../../../helpers/dataAction';
import { formatImageUrl } from '../../../helpers/common';

const { openModal, closeModal } = generateModalActions(REDUCERS.GLOBAL_LOADING);

const {
  handleFieldChanged,
  submitDone,
  submitError,
  submitting,
  validateAll,
  reset,
} = generateFormActions(REDUCERS.NEW_PROJECT);

export const handleChange = (name, value) => async dispatch => {
  dispatch(handleFieldChanged(name, value));
};

export const uploadProjectImages = images => async (dispatch, getState) => {
  try {
    const { images: currentImages } = getState().project[REDUCERS.NEW_PROJECT].data;
    dispatch(openModal());
    await dispatch(uploadImages(images));
    const uploadJobs = getState().house[REDUCERS.UPLOAD_JOBS].jobs;
    const uploaded = _.values(uploadJobs.results).map(j => ({
      url: formatImageUrl(j.main),
      id: j.id,
    }));
    dispatch(handleChange('images', currentImages.concat(uploaded)));
    dispatch(closeModal());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
    dispatch(closeModal());
  }
};

export const create = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { data } = getState().project[REDUCERS.NEW_PROJECT];
    dispatch(submitting());
    const form = { ...data, images: data.images.filter(i => i.id).map(i => i.id) || undefined };
    const response = await thunkDependencies.project.newProject(form);
    const result = apiSerializer(response);
    dispatch(submitDone(result));
    dispatch(reset());
    toast.success('Tạo dự án thành công!');
    dispatch(push(ROUTES.PROJECTS));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
    dispatch(submitError(message));
  }
};

export const update = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { data } = getState().project[REDUCERS.NEW_PROJECT];
    dispatch(submitting());
    const form = {
      ...data,
      project_id: data.id,
      id: undefined,
      images: data.images.filter(i => i.id).map(i => i.id) || undefined,
    };
    const response = await thunkDependencies.project.update(form);
    const result = apiSerializer(response);
    dispatch(submitDone(result));
    dispatch(reset());
    toast.success('Cập nhật dự án thành công!');
    dispatch(push(ROUTES.PROJECTS));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
    dispatch(submitError(message));
  }
};

export const submit = editMode => async (dispatch, getState) => {
  dispatch(validateAll());
  const { error } = getState().project[REDUCERS.NEW_PROJECT];
  const isValid = _.values(error).filter(i => i.length).length === 0;

  if (isValid) {
    if (editMode) {
      dispatch(update());
    } else {
      dispatch(create());
    }
  }
};

const projectFields = ['id', 'name', 'content', 'images'];

const { fetchDone, fetching, fetchError } = generateDataActions(REDUCERS.PROJECT_DETAIL);

export const fetch = id => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const response = await thunkDependencies.project.detail(id);
    const result = apiSerializer(response);
    dispatch(fetchDone(result));
    return result;
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
    return false;
  }
};

export const fetchProject = id => async (dispatch, getState) => {
  dispatch(openModal());
  const rs = await dispatch(fetch(id));
  const project = Selector.project(getState()).data;
  dispatch(closeModal());

  projectFields.forEach(field => {
    // if (field === 'project_direction') {
    //   dispatch(handleChange('direction', project[field][0] ? project[field][0].direction : null));
    if (field === 'images') {
      const images = _.values(project.imageUrls).map(img => ({
        ...img,
        url: formatImageUrl(img.main),
      }));
      dispatch(handleChange(field, images));
    } else {
      dispatch(handleChange(field, project[field]));
    }
  });
  return rs;
};
