/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { goBack } from 'connected-react-router';
import { Editor } from 'react-draft-wysiwyg';
import { convertToRaw, EditorState, convertFromRaw } from 'draft-js';
import moment from 'moment';
import { faSave, faTimes } from '@fortawesome/free-solid-svg-icons';
import { withRouter } from 'react-router-dom';

import 'draft-js/dist/Draft.css';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import _ from 'lodash';
import './NewProject.scss';
import styles from '../../../styles/_Colors.scss';
import { REDUCERS } from '../../../constants/COMMON';
import * as actions from './action';
import { generateModalActions } from '../../../helpers/modalAction';
import {
  Flexbox,
  FormInput,
  FormLabel,
  Card,
  Button,
  FormField,
  Uploader,
  Spacer,
} from '../../../components';
import Description from '../../../components/Description/Description';
import coverImage from '../../../assets/images/left.jpg';

const { toggle } = generateModalActions(REDUCERS.NEW_USER_MODAL);

class NewBlog extends Component {
  static propTypes = {};

  static defaultProps = {};

  state = {
    editorState: EditorState.createEmpty(),
  };

  componentDidMount() {
    const { match } = this.props;
    const { id } = match.params;
    this.loadProjectData(id);
  }

  loadProjectData = async id => {
    const { editMode, fetchProject } = this.props;

    if (editMode) {
      const rs = await fetchProject(id);
      if (rs) {
        try {
          const { form } = this.props;
          form.data.descriptions = rs.descriptions !== null ? rs.descriptions : ' ';
          form.data.key_word = rs.key_word !== null ? rs.key_word : ' ';
          const jsonContent = JSON.parse(form.data.content);
          const editorState = EditorState.createWithContent(convertFromRaw(jsonContent));
          this.setState({ editorState });
        } catch (error) {
          // eslint-disable-next-line no-console
          console.log(error);
        }
      }
    }
  };

  onImageAdded = newImages => {
    const { uploadProjectImages } = this.props;
    uploadProjectImages(newImages);
  };

  onImageRemoved = index => {
    const { handleChange, form } = this.props;
    const { images } = form.data;
    images.splice(index, 1);
    handleChange('images', images);
  };

  onChange = editorState => {
    if (!editorState) {
      return;
    }
    const { handleChange } = this.props;
    const rawContent = convertToRaw(editorState.getCurrentContent());
    this.setState({ editorState });
    handleChange('content', JSON.stringify(rawContent));
  };

  render() {
    const { form, handleChange, submit, user, editMode, back } = this.props;
    const { submitting, error, data } = form;
    const { editorState } = this.state;
    const isValid = _.values(error).filter(i => i.length).length === 0;
    const textContent = editorState.getCurrentContent().getPlainText('\n');
    return (
      <div className="new-project-page">
        <Flexbox row alignItems="flex-start">
          <div style={{ flex: 3, marginLeft: 10, marginRight: 10 }}>
            <div style={{ display: 'flex' }}>
              <h2>{editMode ? 'Cập nhật dự án' : 'Tạo dự án'}</h2>
              <Button
                ortherClass
                iconOnly
                spaceBetween
                icon={faTimes}
                onClick={() => back()}
                containerStyle={{ paddingLeft: 15, paddingRight: 15 }}
              />
            </div>
            <Card shadow containerStyle={{ padding: 20, marginTop: 10 }}>
              <h3>Chọn để upload ảnh</h3>
              <Uploader
                onImageAdded={newImages => this.onImageAdded(newImages)}
                onImageRemoved={index => this.onImageRemoved(index)}
                containerStyle={{ marginTop: 10 }}
                copyUrl
                images={data.images}
                id="internal"
              />
              <FormInput form={form} handleChange={handleChange} name="name" label="Tiêu đề" />
              <FormField
                name="content"
                form={form}
                handleChange={handleChange}
                render={() => (
                  <>
                    <FormLabel text="Nội dụng" />
                    <Editor
                      editorState={editorState}
                      wrapperStyle={{
                        borderWidth: 1,
                        borderRadius: 5,
                        borderColor: 'rgba(34,36,38,.15)',
                        borderStyle: 'solid',
                        backgroundColor: 'white',
                      }}
                      editorStyle={{ padding: 10 }}
                      onEditorStateChange={this.onChange}
                      handlePastedText={() => false}
                    />
                  </>
                )}
              />
              <Spacer />
              <Description editMode={editMode} form={form} handleChange={handleChange} />
            </Card>
          </div>
          <div style={{ flex: 1, maxWidth: 400, minWidth: 200, marginLeft: 10, marginRight: 10 }}>
            <h3 style={{ margin: 0, fontSize: 16, color: styles.grayColor }}>Xem trước</h3>
            <Card shadow noPadding containerStyle={{ overflow: 'hidden', marginTop: 15 }}>
              <img
                style={{ width: '100%', height: 150, objectFit: 'cover' }}
                alt="cover blog"
                src={data.cover ? data.cover.data || data.cover.url : coverImage}
              />
              <div style={{ padding: 10 }}>
                <div style={{ fontSize: 11, color: styles.primaryColor }}>
                  {`Tạo bởi ${user ? user.name : ''} lúc ${moment().format('DD-MM-YYYY')}`}
                </div>
                <h4 style={{ marginBottom: 0, marginTop: 10 }}>{data.name || 'Tiêu đề'}</h4>
                <p
                  style={{
                    whiteSpace: 'pre-line',
                    fontSize: 12,
                    fontWeight: 'bold',
                    color: styles.grayColor,
                    margin: 0,
                    maxHeight: 145,
                  }}
                >
                  {textContent || 'Nội dung'}
                </p>
              </div>
            </Card>
            <div style={{ marginTop: 30 }}>
              <Button
                text={editMode ? 'Cập nhật' : 'Tạo'}
                onClick={() => submit(editMode)}
                icon={faSave}
                loading={submitting}
                disabled={!isValid}
                containerStyle={{ width: '100%' }}
              />
            </div>
          </div>
        </Flexbox>
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    form: state.project[REDUCERS.NEW_PROJECT],
    user: state.auth.user.data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      toggleModal: toggle,
      back: goBack,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(NewBlog);
