import createFormReducer from '../../../helpers/createFormReducer';
import { REDUCERS } from '../../../constants/COMMON';

export default createFormReducer(
  REDUCERS.NEW_PROJECT,
  { images: [] },
  {
    images: [['required', 'Ảnh bắt buộc']],
    name: [['required', 'Tiêu đề bắt buộc']],
    content: [['required', 'Nội dung bắt buộc']],
  },
);
