/* eslint-disable react/no-danger */
/* eslint-disable react/no-array-index-key */
/* eslint-disable camelcase */
import classNames from 'classnames';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { Flexbox } from '../../../components';
import ImageLightbox from '../../../components/LightBox/Lightbox';
import { REDUCERS } from '../../../constants/COMMON';
import { ROUTES } from '../../../constants/ROUTES';
import { convertStringToHTML, formatImageUrl } from '../../../helpers/common';
// eslint-disable-next-line import/no-useless-path-segments
import DownloadImages from '../../HouseContainer/Detail/DownloadImages/DownloadImages';
import Comments from '../../HouseContainer/Detail/Comments/Comments';
import './Info.scss';

const ProjectInfo = props => {
  const [lightboxVisible, setLightboxVisible] = useState(false);
  const [imageIndex, setImageIndex] = useState(0);
  // eslint-disable-next-line react/prop-types
  const { project, visible, options } = props;
  // eslint-disable-next-line react/prop-types
  const { imageUrls, content } = project;
  //   const { imageUrls } = image || {};
  const [htmlContent, setHtml] = useState(null);
  // eslint-disable-next-line no-unused-vars
  const { id } = project;
  const thumbnails = _.values(imageUrls).map(p => formatImageUrl(p.thumbnail || p.main));
  const images = _.values(imageUrls).map(p => formatImageUrl(p.main));
  useEffect(() => {
    setHtml(convertStringToHTML(content));
  }, [content]);

  //   useEffect(() => {
  //     setInteralDescriptionhtmlContent(convertStringToHTML(internalDescription));
  //   }, [internalDescription]);
  return (
    <>
      <div className={classNames('project-detail', { visible })}>
        <div className="image-row">
          <Flexbox row spaceBetween containerStyle={{ marginRight: 10, marginBottom: 10 }}>
            <h4>{`${images.length} ảnh`}</h4>
            <DownloadImages house={project} />
          </Flexbox>
          <div className="row flex">
            {thumbnails.map((im, index) => (
              <div
                onClick={() => {
                  setLightboxVisible(true);
                  setImageIndex(index);
                }}
              >
                <img src={im} alt="detail" />
              </div>
            ))}
          </div>
        </div>

        {htmlContent ? (
          <div className="description">
            <h4>Mô tả</h4>

            <div
              className="description_internal"
              dangerouslySetInnerHTML={{
                __html: htmlContent,
              }}
            />
          </div>
        ) : (
          <div className="description">
            <h4>Mô tả</h4>
            <p>{project.name}</p>
          </div>
        )}
        {/* <div className="detail">
          {FIELDS.filter(
            f =>
              f.label !== 'Description' && f.label !== 'Key Word' && !notDetail.includes(f.property),
          ).map(field => (
            <Flexbox row spaceBetween key={field.property}>
              <div className="label">{field.label}</div>
              <span>{field.format ? field.format(house, options) : house[field.property]}</span>
            </Flexbox>
          ))}
        </div> */}
        <ImageLightbox
          selectedIndex={imageIndex}
          isOpen={lightboxVisible}
          onClose={() => setLightboxVisible(false)}
          images={images}
        />
        {id && <Comments id={id} />}
      </div>
    </>
  );
};

ProjectInfo.propTypes = {
  house: PropTypes.any,
  visible: PropTypes.bool.isRequired,
  options: PropTypes.any.isRequired,
};

ProjectInfo.defaultProps = {
  house: {},
};

export function mapStateToProps(state) {
  return {
    visible: state.project[REDUCERS.PROJECT_INFO].visible,
    options: state.metadata[REDUCERS.OPTIONS].data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      //   ...actions,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(ProjectInfo);
