import React, { useEffect, useState } from 'react';
import { DatePicker, Form, Input, InputNumber, Modal, Select, Spin } from 'antd';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import { Flexbox } from '../../../components';
import * as actions from './action';
// eslint-disable-next-line react/prop-types
const ModalQuestion = ({ isModalVisible, setIsModalVisible, houseId, createQuestion }) => {
  const [form] = Form.useForm();
  const handleCancel = () => {
    form.resetFields();
    setIsModalVisible(false);
  };
  const onFinish = values => {
    values.house_id = houseId;
    console.log('values', values)
    createQuestion(values);
    form.resetFields();
    setIsModalVisible(false);
  };
  return (
    <Modal
      title="Tạo chủ đề về dự án"
      visible={isModalVisible}
      onCancel={handleCancel}
      onOk={form.submit}
      okButtonProps={{ style: { backgroundColor: '#ff8731' } }}
    >
      <div>
        <Form
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          autoComplete="off"
          layout="vertical"
          form={form}
        >
          <Flexbox row spaceBetween>
            <Form.Item
              label="Chủ đề về dự án"
              name="questionName"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập chủ đề',
                },
              ]}
            >
              <Input style={{ width: 150 }} />
            </Form.Item>
          </Flexbox>
        </Form>
      </div>
    </Modal>
  );
};

export function mapStateToProps(state) {
  return {
    // visible: state.project[REDUCERS.PROJECT_INFO].visible,
    // options: state.metadata[REDUCERS.OPTIONS].data,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(ModalQuestion);
