import { v4 as uuidv4 } from 'uuid';
import { generateDatalistActions } from '../../../helpers/dataListAction';
import { addQuestion, getQuestionByHouseId } from '../../../apis/houseApi';

const GET_USER = 'GET_USER';
const GET_LIST_RECEIPT = 'GET_LIST_RECEIPT';

// const { fetchDone, fetching } = generateDatalistActions(GET_USER);

// export const getUser = async dispatch => {
//   dispatch(fetching());
//   const user = await getUserListByManagerId();
//   dispatch(fetchDone(user, null, null));
// };

export const getQuestionByProjectId = houseId => async dispatch => {
  // dispatch(fetching());
  const questions = await getQuestionByHouseId(Number(houseId));
  dispatch({
    type: 'GET_QUESTION',
    payload: questions.data,
  });
};

export const createQuestion = data => async dispatch => {
  const question = await addQuestion(data);
  dispatch({
    type: 'ADD_QUESTION',
    payload: question.data,
  });
};
