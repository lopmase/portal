import React, { useEffect, useState } from 'react';
import { Avatar, List, Skeleton } from 'antd';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';
import * as actions from './action';
import { REDUCERS } from '../../../constants/COMMON';
import { Button } from '../../../components';
import ModalQuestion from './Modal';

const count = 3;
const fakeDataUrl = `https://randomuser.me/api/?results=${count}&inc=name,gender,email,nat,picture&noinfo`;

// eslint-disable-next-line react/prop-types
const ProjectQA = ({ openNewQuestionModal, houseId, questionList, getQuestionByProjectId }) => {
  const [initLoading, setInitLoading] = useState(true);
  const [loading, setLoading] = useState(false);
  const [open, setOpenModal] = useState(false);
  const [data, setData] = useState([]);
  const [list, setList] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  useEffect(() => {
    getQuestionByProjectId(houseId)
  }, []);

  const loadMore =
    !initLoading && !loading ? (
      <div
        style={{
          textAlign: 'center',
          marginTop: 12,
          height: 32,
          lineHeight: '32px',
        }}
      >
        {/* <Button onClick={onLoadMore}>loading more</Button> */}
      </div>
    ) : null;

  return (
    <>
      <Button text="+ Tạo mới" onClick={showModal} />
      <List
        className="demo-loadmore-list"
        // loading={initLoading}
        itemLayout="horizontal"
        loadMore={loadMore}
        dataSource={questionList}
        renderItem={item => (
          <List.Item
            // eslint-disable-next-line jsx-a11y/anchor-is-valid
            actions={[<a key="list-loadmore-edit">edit</a>, <a key="list-loadmore-more">more</a>]}
          >
            <Skeleton avatar title={false} loading={item.loading} active>
              <List.Item.Meta
                
                // title={<a href="https://ant.design">{item.name?.last}</a>}
                description={item.questionName}
              />
              <div>content</div>
            </Skeleton>
          </List.Item>
        )}
      />
      <ModalQuestion
        houseId={houseId && houseId}
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
      />
    </>
  );
};
export function mapStateToProps(state) {
  return {
    houseId: state.router.location.pathname.split('/')[2],
    questionList: state.project.QUESTION.list,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(ProjectQA);
