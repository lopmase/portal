import { REDUCERS } from '../../../constants/COMMON';

const ADD_QUESTION = 'ADD_QUESTION';
const UPDATE_QUESTION = 'UPDATE_QUESTION';
const GET_QUESTION = 'GET_QUESTION';

const initialValues = {
  list: [],
  type: '',
};

export const questionReducer = (state = initialValues, action) => {
  switch (action.type) {
    case GET_QUESTION:
      // const check = state.data.filter(value => value.id == action.payload.id);
      // console.log('state.listData',state.listData)
      state.list.map((value, index) => {
        if (value.id === action.payload.id) {
          // eslint-disable-next-line no-param-reassign
          state.list[index] = action.payload;
        }
      });
      return {
        ...state,
        type: GET_QUESTION,
        list: action.payload,
      };
    // case NEW_TRANSACTIONBILL:
    //   return {
    //     type: NEW_TRANSACTIONBILL,
    //     data: action.payload,
    //   };
    case ADD_QUESTION:
      return {
        type: ADD_QUESTION,
        list: [...state.list, action.payload],
      };
    case UPDATE_QUESTION:
      return {
        ...state,
        dataUpdate: action.payload,
        type: UPDATE_QUESTION,
      };
    default:
      return state;
  }
};
