/* eslint-disable no-param-reassign */
import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { connect } from 'react-redux';
import { Modal, Select } from 'antd';
import _ from 'lodash';
import { push } from 'connected-react-router';
import { faEye } from '@fortawesome/free-solid-svg-icons';
import { Table, Button } from '../../../components';
import { ROUTES } from '../../../constants/ROUTES';
import { createHouseRecommendStatus } from '../../../apis/customerApi';

const { Option } = Select;
const columns = [
  {
    header: '#',
    key: 'id',
  },
  {
    header: 'KHÁCH HÀNG',
    key: 'customer',
    accessor: project => (project.customer ? project.customer.name : ''),
  },
  {
    header: 'THÔNG TIN',
    key: 'info',
  },
  {
    header: 'NGÀY TẠO',
    key: 'created_at',
    accessor: project => moment(project.created_at * 1000).format('DD/MM/YYYY HH:mm:ss'),
  },
  {
    header: 'ACTION',
    key: 'action',
  },
];

const status = [
  { key: 1, value: 'Đã chào' },
  { key: 2, value: 'Đã đi xem' },
  { key: 3, value: 'Thương lượng' },
  { key: 4, value: 'Giao dịch hoàn thành' },
  { key: 0, value: 'Không ưng' },
];
const HouseListModal = ({ visible, list, onClose, doPush, request, customerId }) => {
  const changeStatus = async value => {
    createHouseRecommendStatus({
      house_id: value.item,
      customer_id: customerId,
      status: value.value,
    }).then(reject => {
      const index = list.findIndex(element => element.id === reject.data.house_id);
      list[index].houseRecommendStatus = reject.data.status;
    });
  };
  const renderCell = data => {
    const { item, column } = data;
    if (column.key === 'action') {
      return (
        <td>
          <Button
            icon={faEye}
            text="Xem"
            onClick={() => doPush(`${ROUTES.HOUSE_ROOT}/${item.id}`)}
          />
        </td>
      );
    }
    if (column.key === 'houseRecommendStatus' && request) {
      return (
        <td>
          <Select
            defaultValue="Không xác định"
            value={item.houseRecommendStatus}
            onChange={value => changeStatus({ item: item.id, value })}
            style={{ width: 180 }}
          >
            {status.map(value => (
              <Option key={value.key} value={value.key}>
                {value.value}
              </Option>
            ))}
          </Select>
        </td>
      );
    }
    if (column.key === 'info') {
      return (
        <td>
          <div>{`${item.house_number} ${item.house_address}`}</div>
          <div style={{ fontWeight: 'bold' }}>{`${item.district} / ${item.province}`}</div>
        </td>
      );
    }
    if (column.key === 'images') {
      return (
        <td>
          {_.values(item.imageUrls)
            .slice(0, 4)
            .map(image => (
              <div className="image">
                <img src={URL + image.thumbnail} alt="sss" />
              </div>
            ))}
        </td>
      );
    }

    const cell = item[column.key];
    let value = '';
    if (column.accessor) {
      value = column.accessor(item, {});
    } else {
      value = cell ? cell.toString() : '';
    }
    return <td className={column.key === 'id' ? 'bold-text' : ''}>{value}</td>;
  };

  return (
    <Modal
      visible={visible}
      maskClosable
      bodyStyle={{ padding: 0 }}
      title="BĐS"
      onCancel={onClose}
      width={800}
      onOk={onClose}
      cancelButtonProps={{ hidden: true }}
    >
      <div style={{ maxHeight: window.innerWidth < 500 ? 450 : 600, overflow: 'scroll' }}>
        <Table
          selectable
          data={list}
          columns={
            request ? [...columns, { header: 'Status', key: 'houseRecommendStatus' }] : columns
          }
          renderCell={renderCell}
        />
      </div>
    </Modal>
  );
};

HouseListModal.propTypes = {
  visible: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  list: PropTypes.any.isRequired,
  doPush: PropTypes.func.isRequired,
  request: PropTypes.bool,
  customerId: PropTypes.number,
};

HouseListModal.defaultProps = {
  request: false,
  customerId: null,
};

const mapDispatchToProps = {
  doPush: push,
};

export default connect(null, mapDispatchToProps)(HouseListModal);
