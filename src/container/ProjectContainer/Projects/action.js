import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateDatalistActions } from '../../../helpers/dataListAction';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { generateModalActions } from '../../../helpers/modalAction';
import { generateFormActions } from '../../../helpers/formAction';
import { PROJECT_DETAIL_TYPES } from './types';
const {
  fetchDone,
  fetching,
  fetchError,
  nextPage,
  previousPage,
  updatePageSize,
} = generateDatalistActions(REDUCERS.PROJECTS);

const { openModal, closeModal } = generateModalActions(REDUCERS.GLOBAL_LOADING);
const { openModal: openNewProjectAction } = generateModalActions(REDUCERS.NEW_PROẸCT_MODAL);
const { openModal: openEditProjectModal } = generateModalActions(REDUCERS.EDIT_USER_MODAL);
const { handleFieldChanged, reset } = generateFormActions(REDUCERS.NEW_USER);

export const fetch = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { page, pageSize, search } = getState().project[REDUCERS.PROJECTS];
    dispatch(fetching());
    const response = await thunkDependencies.project.list({ page, size: pageSize, search });
    const result = apiSerializer(response);
    dispatch(fetchDone(result.data, result.total, Math.ceil(result.total / pageSize)));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};

export const goNextPage = () => async dispatch => {
  dispatch(nextPage());
  dispatch(fetch());
};

export const goPrevPage = () => async dispatch => {
  dispatch(previousPage());
  dispatch(fetch());
};

export const changePageSize = size => async dispatch => {
  dispatch(updatePageSize(size));
  dispatch(fetch());
};

export const deleteProject = id => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(openModal());
    await thunkDependencies.project.remove(id);
    dispatch(closeModal());
    dispatch(fetch());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};

export const openNewProjectModal = () => async (dispatch, getState) => {
  const user = getState().auth.user.data;

  if (user) {
    dispatch(handleFieldChanged('user_id', user.id));
  }

  dispatch(reset());
  dispatch(openNewProjectAction());
};
export const openEditUserModal = userId => async dispatch => {
  dispatch(openEditProjectModal(userId));
};

export const open = () => ({
  type: PROJECT_DETAIL_TYPES.OPEN,
});

export const close = () => ({
  type: PROJECT_DETAIL_TYPES.CLOSE,
});
