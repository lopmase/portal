import moment from 'moment';

export const columns = [
  {
    header: '#',
    key: 'id',
  },
  {
    header: 'TÊN',
    key: 'name',
  },
  {
    header: 'ẢNH',
    key: 'images',
  },
  {
    header: 'BĐS',
    key: 'houses',
  },
  {
    header: 'NGÀY TẠO',
    key: 'created_at',
    accessor: project => moment(project.created_at * 1000).format('DD/MM/YYYY HH:mm:ss'),
  },
  {
    header: 'NGÀY CẬP NHẬT',
    key: 'updated_at',
    accessor: project => moment(project.updated_at * 1000).format('DD/MM/YYYY HH:mm:ss'),
  },
  {
    header: 'ACTION',
    key: 'action',
  },
];
