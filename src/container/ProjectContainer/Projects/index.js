/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { compose, lifecycle } from 'recompose';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';

import _ from 'lodash';
import { faTrashAlt, faPen } from '@fortawesome/free-solid-svg-icons';
import { Drawer } from 'antd';
import { generateModalActions } from '../../../helpers/modalAction';
import { Flexbox, Table, Button } from '../../../components';
import { confirm } from '../../ShellContainner/ConfirmDialog/action';
import Pagination from '../../../utils/Pagination';
import { DELETE_PROJECT_CONFIRM } from '../../../constants/MESSAGE';
import './Projects.scss';
import { REDUCERS } from '../../../constants/COMMON';
import * as actions from './action';
import { columns } from './columns';
import { ROUTES } from '../../../constants/ROUTES';
import HouseListModal from './HouseListModal';
import Image from '../../../components/Image/Image';
import avatar from '../../../assets/images/building.jpg';
import { UserSelector } from '../../UserConntainer/Selector';
import { isAdmin, isMarketer } from '../../../helpers/roleUtil';
import { formatImageUrl } from '../../../helpers/common';
import { open, close } from './action';
import ProjectInfo from '../ProjectDetail/Info';

const editButtonProps = {
  iconColor: '#0ea60e',
  icon: faPen,
  iconOnly: true,
  clear: true,
};

const deleteButtonProps = {
  iconColor: '#e70f0f',
  icon: faTrashAlt,
  iconOnly: true,
  clear: true,
};

const { openModal: openEdit } = generateModalActions(REDUCERS.EDIT_PROJECT_MODAL);
const { openModal: openNew } = generateModalActions(REDUCERS.NEW_PROJECT_MODAL);

const Projects = ({
  dataSource: { list, fetching, total, total_pages: totalPages, page: currentPage },
  goNextPage,
  goPrevPage,
  user,
  openConfirmDialog,
  deleteProject,
  pushRoute,
  openProjectDetail,
  closeProjectDetail,
  detailVisible,
  ...props
}) => {
  const [showListHouse, setShowListHouse] = React.useState(false);
  const [selected, selectProject] = React.useState(null);
  const [selectedItem, setSelectedItem] = useState({
    selectedItem: null,
    selectedRowId: null,
  });
  const [deviceMode, setDeviceMode] = useState(window.innerWidth < 500);

  const me = props.users;
  const onDelete = id => {
    openConfirmDialog(DELETE_PROJECT_CONFIRM, result => {
      if (result) {
        deleteProject(id);
      }
    });
  };

  const onEdit = project => {
    pushRoute(`${ROUTES.PROJECT_ROOT}/edit/${project.id}`);
  };

  const renderCell = ({ item, column }) => {
    if (column.key === 'action') {
      return (
        <td>
          {(isAdmin(me) || isMarketer(me)) && (
            <>
              <Button {...editButtonProps} onClick={() => onEdit(item)} />
              <Button {...deleteButtonProps} onClick={() => onDelete(item.id)} />
            </>
          )}
        </td>
      );
    }
    if (column.key === 'houses') {
      return (
        <td>
          <Button
            text={`${item.houses.length} BĐS`}
            clear
            onClick={() => {
              selectProject(item);
              setShowListHouse(true);
            }}
          />
        </td>
      );
    }
    if (column.key === 'images') {
      return (
        <td>
          {_.values(item.imageUrls)
            .slice(0, 4)
            .map(image => (
              <div className="image">
                <Image fallbackSrc={avatar} src={formatImageUrl(image.thumbnail)} alt="sss" />
              </div>
            ))}
        </td>
      );
    }

    const cell = item[column.key];
    let value = '';
    if (column.accessor) {
      value = column.accessor(item, {});
    } else {
      value = cell ? cell.toString() : '';
    }
    return <td className={column.key === 'id' ? 'bold-text' : ''}>{value}</td>;
  };

  const onRowSelect = ({ index, rowData }) => {
    console.log('rowData', rowData)
    setSelectedItem({ selectedItem: rowData, selectedRowId: rowData.id });
    if (index >= 0) {
      openProjectDetail();
    } else {
      closeProjectDetail();
    }
  };

  return (
    <div className="projects-page" style={{}}>
      <div className="toolbar">
        <Flexbox
          row
          spaceBetween
          containerStyle={{ marginBottom: 20, marginTop: 20, alignItems: 'center' }}
        >
          <Flexbox>
            <div className="total">{`${total} Dự án`}</div>
          </Flexbox>
          <div>
            <Pagination
              currentPage={currentPage}
              pageNumbers={totalPages}
              onNext={goNextPage}
              onPrev={goPrevPage}
            />
          </div>
          <Flexbox>
            {(isAdmin(me) || isMarketer(me)) && (
              <Link to={ROUTES.NEW_PROJECT}>
                <Button text="Tạo mới" />
              </Link>
            )}
          </Flexbox>
        </Flexbox>
      </div>
      <Table
        loading={fetching}
        data={list}
        columns={columns}
        renderCell={renderCell}
        // eslint-disable-next-line no-undef
        rowClick={onRowSelect}
      />
      <div>
        <Flexbox row containerStyle={{ marginTop: 20, alignItems: 'center' }}>
          <Pagination
            currentPage={currentPage}
            pageNumbers={totalPages}
            onNext={goNextPage}
            onPrev={goPrevPage}
          />
        </Flexbox>
      </div>
      <HouseListModal
        onClose={() => setShowListHouse(false)}
        visible={showListHouse}
        list={selected ? selected.houses : []}
      />
      <Drawer
        title="Chi tiết"
        placement="right"
        width={deviceMode ? window.innerWidth : '600px'}
        mask={false}
        maskClosable={false}
        bodyStyle={{ padding: 0 }}
        onClose={closeProjectDetail}
        visible={detailVisible}
        style={{ position: 'absolute', zIndex: 50 }}
      >
        <ProjectInfo project={selectedItem.selectedItem || {}} />
      </Drawer>
    </div>
  );
};
const useLifeCycle = lifecycle({
  componentDidMount() {
    this.props.fetch();
  },
});
const mapStateToProps = state => ({
  users: UserSelector.me(state).data,
  dataSource: state.project[REDUCERS.PROJECTS],
  detailVisible: state.project[REDUCERS.PROJECT_INFO].visible,
});

const mapDispatchToProps = {
  ...actions,
  openEditProjectModal: openEdit,
  openConfirmDialog: confirm,
  openNewProjectModal: openNew,
  pushRoute: push,
  openProjectDetail: open,
  closeProjectDetail: close,
};

const connetToRedux = connect(mapStateToProps, mapDispatchToProps);
export default compose(connetToRedux, useLifeCycle, withRouter)(Projects);
