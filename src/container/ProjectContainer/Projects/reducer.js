import { PROJECT_DETAIL_TYPES } from './types';

const initialState = {
  visible: false,
};
const detailReducer = (state = initialState, action) => {
  switch (action.type) {
    case PROJECT_DETAIL_TYPES.OPEN:
      return {
        ...state,
        visible: true,
      };
    case PROJECT_DETAIL_TYPES.CLOSE:
      return {
        ...state,
        visible: false,
      };
    default:
      return state;
  }
};

export default detailReducer;