import { combineReducers } from 'redux';
import createDatalistReducer from '../../helpers/createDataListReducer';
import createDataReducer from '../../helpers/createDataReducer';
import { REDUCERS } from '../../constants/COMMON';
import reducer from './NewProject/reducer';
import { questionReducer } from './Project_QA/reducer';

import infoReducer from './Projects/reducer';

export default combineReducers({
  [REDUCERS.PROJECTS]: createDatalistReducer(REDUCERS.PROJECTS, { pageSize: 20 }),
  [REDUCERS.PROJECT_DETAIL]: createDataReducer(REDUCERS.PROJECT_DETAIL),
  [REDUCERS.NEW_PROJECT]: reducer,
  [REDUCERS.PROJECT_INFO]: infoReducer,
  [REDUCERS.QUESTION]: questionReducer,
});
