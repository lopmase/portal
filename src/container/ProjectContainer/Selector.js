import { REDUCERS } from '../../constants/COMMON';

export default {
  project: state => state.project[REDUCERS.PROJECT_DETAIL],
};
