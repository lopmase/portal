import React, { Component } from 'react';
import { PrivateRoute } from '../../components';
import { ROUTES } from '../../constants/ROUTES';
import Requests from './Requests';
import NewRequest from './NewRequest';

class RequestContainer extends Component {
  state = {};

  render() {
    return (
      <div className="request-container">
        <PrivateRoute path={ROUTES.REQUESTS} component={Requests} />
        <PrivateRoute path={ROUTES.NEW_REQUEST} component={NewRequest} />
        <PrivateRoute
          path={ROUTES.EDIT_REQUEST}
          component={props => <NewRequest {...props} editMode />}
        />
      </div>
    );
  }
}

export default RequestContainer;
