/* eslint-disable array-callback-return */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Flexbox, Spacer } from '../../../components';
import FormSelect from '../../../components/Form/FormSelect';

const districtCategoryFieldProps = {
  label: 'Huyện',
  name: 'province',
  containerStyle: {
    minWidth: 150,
  },
};

const PlaceSeletor = ({ handleChange, form, options }) => {
  const { province, district } = options || {};
  const [districtsSelected, setDistrictsSelected] = useState();

  useEffect(() => {
    if (form.data.province && Array.isArray(form.data.province) && district) {
      const result = [];
      form.data.province.length > 0 &&
        form.data.province.map(value => {
          district[value].map(s => {
            result.push({ value: s, text: s });
            return result;
          });
        });
      setDistrictsSelected(result);
    } else {
      const result = [];
      district &&
        form.data.province &&
        district[form.data.province].map(s =>
          result.push({
            text: s,
            value: s,
          }),
        );
      setDistrictsSelected(result);
    }
  }, [form.data.province, district]);
  return (
    <Card shadow containerStyle={{ flex: 2 }}>
      <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
        <div style={{ flex: 1 }}>
          <FormSelect
            form={form}
            require
            multiple
            handleChange={(name, value) => {
              if (value !== form.data.province) {
                handleChange('districts', []);
              }
              handleChange(name, value);
            }}
            {...districtCategoryFieldProps}
            items={province ? province.map(s => ({ text: s, value: s })) : []}
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormSelect
            form={form}
            handleChange={handleChange}
            require
            multiple
            label="Phường"
            name="districts"
            // containerStyle={{
            //   minWidth: 150,
            // }}
            items={districtsSelected || []}
          />
        </div>
      </Flexbox>
    </Card>
  );
};

PlaceSeletor.propTypes = {
  form: PropTypes.any.isRequired,
  options: PropTypes.any.isRequired,
  handleChange: PropTypes.func.isRequired,
};

PlaceSeletor.defaultProps = {};

export default PlaceSeletor;
