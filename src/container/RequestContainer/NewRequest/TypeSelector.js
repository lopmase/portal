import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import { Card, Flexbox, Spacer } from '../../../components';
import FormSelect from '../../../components/Form/FormSelect';

const productCategoryFieldProps = {
  label: 'Loại bất động sản',
  name: 'property_type',
  containerStyle: {
    minWidth: 150,
  },
};

const houseCategoryFieldProps = {
  label: 'Loại nhà',
  name: 'house_type',
  containerStyle: {
    minWidth: 150,
  },
};
const TypeSeletor = ({ handleChange, form, options }) => {
  const { class: houseType, type } = options || {};

  return (
    <Card shadow containerStyle={{ flex: 2 }}>
      <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
        <div style={{ flex: 1 }}>
          <FormSelect
            form={form}
            handleChange={handleChange}
            require
            multiple
            {...productCategoryFieldProps}
            items={
              type ? _.entries(type).map(pair => ({ text: pair[1], value: Number(pair[0]) })) : []
            }
          />
        </div>
        <Spacer />
        <div style={{ flex: 1 }}>
          <FormSelect
            form={form}
            require
            multiple
            handleChange={handleChange}
            {...houseCategoryFieldProps}
            items={
              houseType
                ? _.entries(houseType).map(pair => ({ text: pair[1], value: Number(pair[0]) }))
                : []
            }
          />
        </div>
      </Flexbox>
    </Card>
  );
};

TypeSeletor.propTypes = {
  form: PropTypes.any.isRequired,
  options: PropTypes.any.isRequired,
  handleChange: PropTypes.func.isRequired,
};

TypeSeletor.defaultProps = {};

export default TypeSeletor;
