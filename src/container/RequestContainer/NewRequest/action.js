/* eslint-disable array-callback-return */
import _ from 'lodash';
import { push } from 'connected-react-router';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS, REQUESTCONSTANT } from '../../../constants/COMMON';
import { generateFormActions } from '../../../helpers/formAction';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { generateModalActions } from '../../../helpers/modalAction';
import { ROUTES } from '../../../constants/ROUTES';
import { generateDataActions } from '../../../helpers/dataAction';
import { convertNumberIntoUnit, convertUnitIntoNumber } from '../../../helpers/common';
import Selector from '../Selector';

const {
  handleFieldChanged,
  submitting,
  reset,
  submitDone,
  submitError,
  validateAll,
} = generateFormActions(REDUCERS.NEW_REQUEST);
const { fetchDone } = generateDataActions(REDUCERS.REQUEST_DETAIL);
const { openModal, closeModal } = generateModalActions(REDUCERS.GLOBAL_LOADING);

export const handleChange = (name, value) => async dispatch => {
  dispatch(handleFieldChanged(name, value));
};

const fields = [
  'area',
  'customer_id',
  'description',
  'districts',
  'floors',
  'house_type',
  'id',
  'max_price',
  'min_price',
  'number_bedroom',
  'property_type',
  'province',
  'purpose',
  'request_direction',
  'require_column',
];

export const fetchRequest = id => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(openModal());
    const response = await thunkDependencies.request.detail(id);
    const request = apiSerializer(response);
    dispatch(fetchDone(request));
    fields.forEach(field => {
      if (field === 'request_direction') {
        dispatch(
          handleChange('direction', request[field] ? request[field].map(d => d.direction) : []),
        );
      } else if (field === 'require_column') {
        const result = [];
        request[field] &&
          request[field].length > 0 &&
          Array.isArray(request[field]) &&
          request[field].map(value => {
            REQUESTCONSTANT.map(s => {
              value === s.name && result.push({ name: s.name, text: s.text, value: s.name });
              return result;
            });
          });
        request[field] = result;
        dispatch(
          handleChange('require_column', request[field] ? request[field].map(d => d.value) : []),
        );
      } else if (field === 'max_price' || field === 'min_price') {
        const [moneyOnly, moneyUnit] = convertNumberIntoUnit(request[field], 0);
        dispatch(handleChange(field, moneyOnly));

        dispatch(handleChange('money_unit', moneyUnit));
      } else {
        dispatch(handleChange(field, request[field]));
      }
    });
    dispatch(closeModal());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};

export const create = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { data } = getState().request[REDUCERS.NEW_REQUEST];
    dispatch(openModal());
    dispatch(submitting());

    const form = { ...data };
    if (form.max_price) {
      form.max_price = convertUnitIntoNumber(form.max_price, form.money_unit);
    }
    if (form.min_price) {
      form.min_price = convertUnitIntoNumber(form.min_price, form.money_unit);
    }
    const response = await thunkDependencies.request.new(form);
    const result = apiSerializer(response);
    dispatch(submitDone(result));
    dispatch(closeModal());
    dispatch(reset());
    dispatch(push(ROUTES.REQUESTS));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
    dispatch(submitError(message));
  }
};

export const update = requestId => async (dispatch, getState, thunkDependencies) => {
  try {
    const { data } = getState().request[REDUCERS.NEW_REQUEST];
    const request = Selector.detail(getState()).data;
    const changedData = {};
    _.forIn(data, (value, key) => {
      if (value !== request[key]) {
        changedData[key] = data[key];
      }
    });
    changedData.request_id = requestId;
    if (changedData.max_price) {
      changedData.max_price = convertUnitIntoNumber(changedData.max_price, changedData.money_unit);
    }
    if (changedData.min_price) {
      changedData.min_price = convertUnitIntoNumber(changedData.min_price, changedData.money_unit);
    }
    dispatch(submitting());
    const response = await thunkDependencies.request.update(changedData);
    const result = apiSerializer(response);
    dispatch(submitDone(result));
    dispatch(reset());
    dispatch(push(ROUTES.REQUESTS));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
    dispatch(submitError(message));
  }
};

export const submit = (id, editMode) => async (dispatch, getState) => {
  dispatch(validateAll());
  const { error } = getState().request[REDUCERS.NEW_REQUEST];

  const isValid = Object.values(error).every(msg => msg.length === 0);

  if (isValid) {
    if (editMode) {
      dispatch(update(id));
    } else {
      dispatch(create());
    }
  }
};
