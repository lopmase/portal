/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { goBack } from 'connected-react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { Select } from 'antd';
import { faSave, faTimes } from '@fortawesome/free-solid-svg-icons';
import {
  Flexbox,
  Spacer,
  Button,
  FormRadioGroup,
  FormInput,
  Card,
  FormField,
  FormLabel,
  FormDropdown,
} from '../../../components';
import { REDUCERS, REQUESTCONSTANT } from '../../../constants/COMMON';
import * as actions from './action';
import * as optionsAction from '../../HouseContainer/Metadata/optionsAction';
import * as TYPES from '../../../constants/NUMERIC_REQUEST';
import './NewRequest.scss';
import PlaceSeletor from './PlaceSelector';
import Selector from '../../HouseContainer/Selector';
import TypeSeletor from './TypeSelector';
import CustomerInfo from '../../HouseContainer/NewHouse/CustomerInfo';

const NewRequest = ({
  match,
  form,
  handleChange,
  editMode,
  submit,
  options,
  getOptions,
  fetchRequest,
  back,
}) => {
  const { error } = form;
  const isValid = _.values(error).filter(i => i.length).length === 0;

  useEffect(() => {
    editMode && fetchRequest(match.params.id);
    getOptions();
  }, [editMode, fetchRequest, getOptions, match.params.id]);
  return (
    <div className="newrequest-page">
      <Flexbox row alignItems="flex-start">
        <div style={{ flex: 3, marginLeft: 10, marginRight: 10 }}>
          <h2>{editMode ? 'Sửa nhu cầu' : 'Tạo nhu cầu'}</h2>
        </div>
        <Button
          ortherClass
          iconOnly
          spaceBetween
          icon={faTimes}
          onClick={() => back()}
          containerStyle={{ paddingLeft: 15, paddingRight: 15 }}
        />
      </Flexbox>
      <div style={{}}>
        <div style={{ maxWidth: 500 }}>
          <FormRadioGroup
            name="purpose"
            label="Nhu cầu của khách hàng?"
            {...{ form, handleChange }}
            options={[
              { text: 'Mua', value: '0' },
              { value: '1', text: 'Thuê' },
            ]}
          />
          <FormField
            form={form}
            name="require_column"
            render={({ value }) => (
              <div>
                <FormLabel text="Dữ liệu bắt buộc" />
                <Select
                  value={value}
                  mode="multiple"
                  style={{ width: '100%' }}
                  placeholder="Trường bắt buộc"
                  name="require_column"
                  clearIcon
                  onChange={v => handleChange('require_column', v)}
                >
                  {REQUESTCONSTANT &&
                    REQUESTCONSTANT.map(row => (
                      <Select.Option value={row.name}>{row.text}</Select.Option>
                    ))}
                </Select>
              </div>
            )}
          />
        </div>
        <Spacer />
        <PlaceSeletor form={form} handleChange={handleChange} options={options} />
        <Spacer />
        <Spacer />
        <TypeSeletor form={form} handleChange={handleChange} options={options} />
        <Spacer />
        <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
          <Card shadow containerStyle={{ flex: 1 }}>
            <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
              <div style={{ flex: 2 }}>
                <FormInput
                  name="min_price"
                  label="Giá tối thiểu"
                  inputProps={{ numberOnly: true }}
                  form={form}
                  require
                  handleChange={handleChange}
                />
                <Spacer />
                <FormInput
                  name="max_price"
                  label="Giá tối đa"
                  inputProps={{ numberOnly: true }}
                  form={form}
                  require
                  handleChange={handleChange}
                />
              </div>
              <Spacer />
              <div style={{ flex: 1 }}>
                <FormDropdown
                  name="money_unit"
                  label="Đơn vị"
                  form={form}
                  require
                  search
                  handleChange={handleChange}
                  items={[
                    { text: 'Triệu', value: 'mil' },
                    { text: 'Tỷ', value: 'bil' },
                  ]}
                />
              </div>
            </Flexbox>
          </Card>
          <Spacer />
          <CustomerInfo form={form} handleChange={handleChange} />
        </Flexbox>
        <Spacer />
        <Card shadow containerStyle={{ flex: 2 }}>
          <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
            <div style={{ flex: 1 }}>
              <FormField
                form={form}
                name="direction"
                render={({ value }) => (
                  <div>
                    <FormLabel require text="Hướng" />
                    <Select
                      mode="multiple"
                      style={{ width: '100%' }}
                      placeholder="Chọn hướng"
                      value={value}
                      clearIcon
                      onChange={v => handleChange('direction', v)}
                    >
                      {TYPES.DIRECTION_TYPES &&
                        TYPES.DIRECTION_TYPES.map(direction => (
                          <Select.Option value={direction.value}>{direction.text}</Select.Option>
                        ))}
                    </Select>
                  </div>
                )}
              />
            </div>
          </Flexbox>
        </Card>
        <Spacer />
        <Card shadow containerStyle={{ flex: 2 }}>
          <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
            <div style={{ flex: 1 }}>
              <FormInput
                name="number_bedroom"
                label="Số phòng ngủ tối thiểu"
                inputProps={{ numberOnly: true }}
                form={form}
                handleChange={handleChange}
              />
            </div>
            <Spacer />
            <div style={{ flex: 1 }}>
              <FormInput
                name="floors"
                label="Số lầu tối thiểu"
                inputProps={{ numberOnly: true }}
                form={form}
                handleChange={handleChange}
              />
            </div>
            <Spacer />
            <div style={{ flex: 1 }}>
              <FormInput
                name="area"
                label="Diện tích tối thiểu"
                inputProps={{ numberOnly: true, suffix: 'm2' }}
                form={form}
                handleChange={handleChange}
              />
            </div>
          </Flexbox>
          <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
            <div style={{ flex: 1 }}>
              <FormInput
                name="min_width"
                label="Chiều ngang tối thiểu"
                inputProps={{ numberOnly: true, suffix: 'm2' }}
                form={form}
                handleChange={handleChange}
              />
            </div>
            <Spacer />
            <div style={{ flex: 1 }}>
              <FormInput
                name="road_area"
                label="Đường rộng tối thiểu"
                inputProps={{ numberOnly: true, suffix: 'm2' }}
                form={form}
                handleChange={handleChange}
              />
            </div>
            <Spacer />
          </Flexbox>
        </Card>
        <Spacer />
        <Card shadow containerStyle={{ flex: 2 }}>
          <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
            <div style={{ flex: 1 }}>
              <FormInput name="description" label="Mô tả" form={form} handleChange={handleChange} />
            </div>
          </Flexbox>
        </Card>
        <Spacer height={30} />
        <div className="footer">
          <Flexbox row spaceBetween>
            <Button clear text="Lưu nháp" />
            <Button
              disabled={!isValid}
              onClick={() => submit(match.params.id, editMode)}
              text="Lưu"
            />
          </Flexbox>
        </div>
      </div>
    </div>
  );
};

export function mapStateToProps(state) {
  return {
    form: state.request[REDUCERS.NEW_REQUEST],
    options: Selector.options(state).data,
  };
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      ...optionsAction,
      back: goBack,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(NewRequest);
