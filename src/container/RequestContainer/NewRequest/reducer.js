import createFormReducer from '../../../helpers/createFormReducer';
import { REDUCERS } from '../../../constants/COMMON';

const initialValue = {
  purpose: '0',
};

export default createFormReducer(REDUCERS.NEW_REQUEST, initialValue, {
  customer_id: [['required', 'Khách hàng bắt buộc']],
  min_price: [['required', 'Giá bắt buộc']],
  max_price: [['required', 'Giá bắt buộc']],
  purpose: [['required', 'Vui lòng chọn nhu cầu']],
  property_type: [['required', 'Vui lòng chọn loại BĐS']],
  house_type: [['required', 'Vui lòng chọn loại nhà']],
  province: [['required', 'Vui lòng chọn huyện']],
  districts: [['required', 'Vui lòng chọn phường']],
});
