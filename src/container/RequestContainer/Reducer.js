import { combineReducers } from 'redux';
import createDatalistReducer from '../../helpers/createDataListReducer';
import createDataReducer from '../../helpers/createDataReducer';
import { REDUCERS } from '../../constants/COMMON';
import newRequestReducer from './NewRequest/reducer';
import groupingReducer from './Requests/reducer';

export default combineReducers({
  [REDUCERS.REQUESTS]: createDatalistReducer(REDUCERS.REQUESTS, { pageSize: 10 }),
  [REDUCERS.REQUEST_DETAIL]: createDataReducer(REDUCERS.REQUEST_DETAIL),
  [REDUCERS.REQUEST_GROUPING]: groupingReducer,
  [REDUCERS.NEW_REQUEST]: newRequestReducer,
});
