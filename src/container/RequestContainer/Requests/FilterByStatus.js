import React from 'react';
import PropTypes from 'prop-types';

import { Dropdown } from '../../../components';
import { REQUEST_STATUS } from '../../../constants/COMMON';

const styleProps = {
  containerStyle: {
    minWidth: 90,
    borderRadius: 18,
    paddingTop: 8,
    paddingBottom: 8,
  },
};

const FilterByStatus = ({ selected, onChanged }) => (
  <div style={{ marginRight: 10, display: 'inline-block' }}>
    <Dropdown
      {...styleProps}
      items={REQUEST_STATUS}
      selected={selected}
      onChanged={({ item }) => onChanged(item.value)}
    />
  </div>
);

FilterByStatus.propTypes = {
  selected: PropTypes.any.isRequired,
  onChanged: PropTypes.func.isRequired,
};

FilterByStatus.defaultProps = {};

export default FilterByStatus;
