/* eslint-disable consistent-return */
/* eslint-disable array-callback-return */
import { toast } from 'react-toastify';
import { notification } from 'antd';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { generateModalActions } from '../../../helpers/modalAction';
import { generateDatalistActions } from '../../../helpers/dataListAction';
import Selector from '../Selector';
import { confirm } from '../../ShellContainner/ConfirmDialog/action';
import { houseByRequest, getSubRecommendHouse } from '../../../apis/houseApi';
import { exportExcel } from '../../Common/ExportExcel/action';

const {
  fetchDone,
  fetching,
  fetchError,
  nextPage,
  previousPage,
  updatePageSize,
  fetchSubListDone,
} = generateDatalistActions(REDUCERS.REQUESTS);

const { openModal, closeModal } = generateModalActions(REDUCERS.GLOBAL_LOADING);

// export const fetchMatchings = ids => async (dispatch, getState) => {
//   try {
//     const [response, subRecommend] = await Promise.all([
//       houseByRequest(ids),
//       getSubRecommendHouse({ request_id: ids }),
//     ]);
//     const result = apiSerializer(response);
//     const { list } = Selector.requests(getState());
//     dispatch(
//       fetchDone(
//         list.map(h => {
//           const req = result.find(r => r.id === h.id);
//           const subRecommendResult = subRecommend.data
//             .filter(v => v.request_id === h.id)
//             .map(house => {
//               return house.house;
//             });
//           if (req && req.houses && !req.house_require) {
//             return { ...h, matchings: req ? [...subRecommendResult, ...req.houses] : [] };
//           }
//           if (req && req.houses && req.house_require) {
//             return {
//               ...h,
//               matchings: req ? [...subRecommendResult, ...req.houses, ...req.house_require] : [],
//             };
//           }
//         }),
//       ),
//     );
//   } catch (error) {
//     const message = error.message || UNKNOWN_ERROR;

//     notification.error({ message, duration: 2 });
//   }
// };

export const fetchMatchings = ids => async (dispatch, getState) => {
  try {
    const response = await houseByRequest(ids);
    const result = apiSerializer(response);
    const { list } = Selector.requests(getState());
    dispatch(
      fetchDone(
        list.map(h => {
          const req = result.find(r => r.id === h.id);
          if (req && req.houses) {
            return { ...h, matchings: req ? req.houses : [] };
          }
          // if (req && req.houses && req.house_require) {
          //   return {
          //     ...h,
          //     matchings: req ? [...req.houses, ...req.house_require] : [],
          //   };
          // }
        }),
      ),
    );
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;

    notification.error({ message, duration: 2 });
  }
};
export const fetchMatchingsSubRecommendHouse = ids => async (dispatch, getState) => {
  try {
    const { list } = Selector.requests(getState());
    const response = await getSubRecommendHouse({ request_id: ids });
    const result = apiSerializer(response);
    const subMatchings = list.map(h => {
      const req = result.find(r => r.id === h.id);
      if (req && req.house_require) {
        return { ...h, house_require: req ? req.house_require : [] };
      }
    });
    dispatch(fetchSubListDone(subMatchings.filter(n => n)));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;

    notification.error({ message, duration: 2 });
  }
};

export const fetch = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { page, pageSize } = getState().request[REDUCERS.REQUESTS];
    const { status } = Selector.requestGrouping(getState());

    dispatch(fetching());
    const response = await thunkDependencies.request.list({ page, size: pageSize, status });
    const result = apiSerializer(response);
    dispatch(fetchDone(result));
    dispatch(fetchMatchings(result.map(h => h.id)));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;

    dispatch(fetchError(message));
  }
};

export const goNextPage = () => async dispatch => {
  dispatch(nextPage());
  dispatch(fetch());
};

export const goPrevPage = () => async dispatch => {
  dispatch(previousPage());
  dispatch(fetch());
};

export const changePageSize = size => async dispatch => {
  dispatch(updatePageSize(size));
  dispatch(fetch());
};

export const deletRequest = id => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(openModal());
    await thunkDependencies.request.remove(id);
    dispatch(closeModal());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};

export const updateRequestStatus = request => async (dispatch, getState, thunkDependencies) => {
  try {
    const newStatus = request.status === 0 ? 1 : 0;
    const message =
      newStatus === 0
        ? 'Bạn có muốn chuyển nhu cầu thành chưa giải quyết'
        : 'Bạn có muốn chuyển nhu cầu thành đã giải quyết';
    const confirmed = await dispatch(confirm(message));
    if (!confirmed) {
      return;
    }
    dispatch(openModal());
    await thunkDependencies.request.update({ request_id: request.id, status: newStatus });
    dispatch(closeModal());
    dispatch(fetch());
    toast.success('Thay đổi thành công!');
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};

export const exportExcelByFilter = () => async (dispatch, getState) => {
  try {
    const { status } = Selector.requestGrouping(getState());
    await dispatch(
      exportExcel('customer_request', {
        status,
      }),
    );
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};

export const exportAllData = () => async dispatch => {
  try {
    await dispatch(exportExcel('customer_request'));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};
