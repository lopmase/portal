/* eslint-disable consistent-return */
/* eslint-disable react/no-array-index-key */
import moment from 'moment';
import React from 'react';
import { formatMoney } from '../../../helpers/common';

export const columns = [
  {
    header: '#',
    key: 'id',
  },
  {
    header: 'Khách hàng',
    key: 'customer',
  },
  {
    header: 'Khu vực',
    key: 'place',
  },
  {
    header: 'Khoảng Giá',
    key: 'price',
    accessor: item => `${formatMoney(item.min_price)} - ${formatMoney(item.max_price)}`,
  },
  {
    header: 'Số lầu',
    key: 'floors',
  },
  {
    header: 'Chiều ngang tối thiểu',
    key: 'min_width',
  },
  {
    header: 'Đường rộng tối thiểu',
    key: 'road_area',
  },
  {
    header: 'Số PN',
    key: 'number_bedroom',
  },
  {
    header: 'Hướng',
    key: 'direction',
    accessor: customer =>
      customer.request_direction
        ? customer.request_direction.map(d => d.direction).join(', ')
        : 'Chưa xác định',
  },
  {
    header: 'Nhân Viên',
    key: 'user_id',
    accessor: customer => (customer.user ? customer.user.name : ''),
  },
  {
    header: 'Mô tả',
    key: 'description',
  },
  {
    header: 'Loại BĐS',
    key: 'property_type',
    accessor: (item, options) => {
      if (options && options.type && !Array.isArray(item.property_type)) {
        return options.type[item.property_type];
      }
      if (options && options.type && Array.isArray(item.property_type)) {
        const element = item.property_type.map(
          (value, index) => {
            return <div key={index}>{options.type[value]}</div>;
          },
          [''],
        );
        return element;
      }
      if (!item) {
        return 'Chưa xác định';
      }
    },
  },
  {
    header: 'Loại nhà',
    key: 'house_type',
    accessor: (item, options) => {
      if (options && options.class && !Array.isArray(item.house_type)) {
        return options.class[item.property_type];
      }
      if (options && options.class && Array.isArray(item.house_type)) {
        const element = item.house_type.map(
          (value, index) => {
            return <div key={index}>{options.class[value]}</div>;
          },
          [''],
        );
        return element;
      }
      if (!item) {
        return 'Chưa xác định';
      }
    },
  },
  {
    header: 'Diện tích',
    key: 'area',
  },
  {
    header: 'BĐS phù hợp',
    key: 'matched_product',
  },
  {
    header: '',
    key: 'detail',
  },
];

export const tableColumn = [
  {
    header: '#',
    key: 'id',
  },
  // {
  //   header: 'KHÁCH HÀNG',
  //   key: 'customer',
  //   accessor: project => (project.customer ? project.customer.name : ''),
  // },
  {
    header: 'THÔNG TIN',
    key: 'info',
  },
  {
    header: 'NGÀY TẠO',
    key: 'created_at',
    accessor: project => moment(project.created_at * 1000).format('DD/MM/YYYY HH:mm:ss'),
  },
  {
    header: 'ACTION',
    key: 'action',
  },
  // {
  //   header: 'Lịch sử',
  //   key: 'history',
  // },
];

export const tableStatus = [
  { key: 1, value: 'Đã chào' },
  { key: 2, value: 'Đã đi xem' },
  { key: 3, value: 'Thương lượng' },
  { key: 4, value: 'Giao dịch hoàn thành' },
  { key: 0, value: 'Không ưng' },
];

export const historyColumns = [
  {
    title: 'Nội dung',
    dataIndex: 'target',
    width: 100,
    render: value => (
      <span>
        <div className="bold-primary-text long-text">{`${value}`}</div>
      </span>
    ),
  },
  {
    title: 'Nhân viên',
    dataIndex: 'user_name',
    width: 100,
    render: value => (
      <span>
        <div className="bold-primary-text long-text">{`${value}`}</div>
      </span>
    ),
  },
  {
    title: 'Địa chỉ',
    dataIndex: 'house_address',
    width: 100,
    render: value => (
      <span>
        <div className="bold-primary-text long-text">{`${value}`}</div>
      </span>
    ),
  },
  {
    title: 'Thời điểm',
    dataIndex: 'created_at',
    sorter: true,
    width: 150,
    render: value => new Date(value * 1000).toLocaleString(),
  },
];

export const houseColumns = [
  {
    header: 'Mã tin',
    dataIndex: 'id',
    key: 'id',
    width: 100,
  },
  {
    header: 'Địa chỉ',
    dataIndex: 'house_address',
    key: 'house_address',
    width: 100,
  },
  {
    header: 'Dự án',
    dataIndex: 'project',
    key: 'project',
    width: 100,
  },
  {
    header: 'Giá',
    dataIndex: 'into_money',
    key: 'into_money',
    width: 100,
  },
  {
    header: 'Thêm',
    dataIndex: 'add',
    key: 'add',
    width: 100,
  },
];
