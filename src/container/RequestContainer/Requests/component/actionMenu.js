/* eslint-disable react/prop-types */
import React from 'react';
import { Menu, Icon } from 'antd';
import { ROUTES } from '../../../../constants/ROUTES';

const ActionMenu = ({
  item,
  pushRoute,
  updateRequestStatus,
  getHistory,
  shareRequest,
  user,
  ...rest
}) => {
  return (
    <>
      <Menu {...rest}>
        <Menu.Item onClick={() => pushRoute(`${ROUTES.REQUEST_ROOT}/edit/${item.id}`)}>
          <Icon type="edit" />
          Sửa
        </Menu.Item>
        <Menu.Item onClick={() => updateRequestStatus(item)}>
          <Icon type="swap" />
          Chuyển trạng thái
        </Menu.Item>
        <Menu.Item onClick={() => getHistory(item.id)}>
          <Icon type="swap" />
          Lịch sử
        </Menu.Item>
        {item.user_id === user.id && (
          <Menu.Item onClick={() => shareRequest(item)}>
            <Icon type="swap" />
            Chia sẻ
          </Menu.Item>
        )}
      </Menu>
    </>
  );
};

export default ActionMenu;
