/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable consistent-return */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { Drawer, Tabs, Input, Form, Button } from 'antd';
import { Label, Table, Flexbox, Reponsive } from '../../../../components';
import { houseColumns } from '../columns';
import { REDUCERS } from '../../../../constants/COMMON';
import * as actions from '../action';
import Selector from '../../Selector';
import { list } from '../../../../apis/houseApi';
import { formatMoney } from '../../../../helpers/common';

const { TabPane } = Tabs;
const RequestModal = props => {
  const {
    setShowListHouse,
    showListHouse,
    fetching,
    selected,
    renderCellTable,
    setSelectRequest,
    suitableLst,
    tableColumn,
    setTabs,
    subSuitableLst,
    recommendedLst,
    subRecommendedLst,
    seenLst,
    subSeenLst,
    negotiate,
    subNegotiate,
    transactionFinishLst,
    dislikeLst,
    setHouseSearch,
    houseSearch,
    requireLst,
    fetchMatchingsSubRecommendHouse,
    options,
  } = props;
  const [requestInfo, serRequestInfo] = useState([]);
  const onFinish = async value => {
    const housesList = await list({
      filters: [{ column: 'address', operation: 'in', values: [value.address] }],
    });
    setHouseSearch(housesList.data);
  };
  const columns = [
    {
      header: 'Mã BĐS',
      key: 'id',
    },
    {
      header: 'Khoảng Giá',
      key: 'price',
      accessor: item => `${formatMoney(item.min_price)} - ${formatMoney(item.max_price)}`,
    },
    {
      header: 'Chiều ngang',
      key: 'min_width',
    },
    {
      header: 'Đường rộng',
      key: 'road_area',
    },
    {
      header: 'Số lầu',
      key: 'floors',
    },
    {
      header: 'Số PN',
      key: 'number_bedroom',
    },
    {
      header: 'Hướng',
      key: 'direction',
      accessor: customer =>
        customer.request_direction
          ? customer.request_direction.map(d => d.direction).join(', ')
          : 'Chưa xác định',
    },
    {
      header: 'Loại BĐS',
      key: 'property_type',
      accessor: item => {
        if (options && options.type && !Array.isArray(item.property_type)) {
          return options.type[item.property_type];
        }
        if (options && options.type && Array.isArray(item.property_type)) {
          const element = item.property_type.map(
            value => {
              return <div key={item.id}>{options.type[value]}</div>;
            },
            [''],
          );
          return element;
        }
        if (!item) {
          return 'Chưa xác định';
        }
      },
    },
    {
      header: 'Loại nhà',
      key: 'house_type',
      accessor: item => {
        console.log('item', item);
        if (options && options.class && !Array.isArray(item.house_type)) {
          return options.class[item.property_type];
        }
        if (options && options.class && Array.isArray(item.house_type)) {
          const element = item.house_type.map(
            value => {
              return <div key={item.id}>{options.class[value]}</div>;
            },
            [''],
          );
          return element;
        }
        if (!item) {
          return 'Chưa xác định';
        }
      },
    },
    {
      header: 'Diện tích',
      key: 'area',
    },
  ];
  const postColumns = [
    {
      header: 'Đã chào',
      key: 'recommendedLst',
    },
    {
      header: 'Đã đi xem',
      key: 'seenLst',
    },
    {
      header: 'Thương lượng',
      key: 'negotiate',
    },
    {
      header: 'Giao dịch thành công',
      key: 'transactionFinishLst',
    },
    {
      header: 'Không ưng',
      key: 'dislikeLst',
    },
  ];
  useEffect(() => {
    const info = {
      suitableLst: suitableLst.length,
      recommendedLst: recommendedLst.length,
      seenLst: seenLst.length,
      transactionFinishLst: transactionFinishLst.length,
      dislikeLst: dislikeLst.length,
      negotiate: negotiate.length,
    };
    serRequestInfo(info);
    console.log('selected', selected);
  }, [props]);
  return (
    <Drawer
      title="Nhu cầu"
      placement="bottom"
      closable
      onClose={() => setShowListHouse(false)}
      visible={showListHouse}
      key="bottom"
      height={window.innerWidth < 500 ? 600 : 700}
    >
      <Tabs defaultActiveKey="1" centered onChange={value => setTabs(Number(value))}>
        <TabPane tab="Thông tin nhu cầu">
          <Reponsive.Default>
            <Table
              loading={fetching}
              data={[{ ...selected, ...requestInfo }]}
              columns={columns}
              renderCell={renderCellTable}
              rowDoubleClick={item => {
                setSelectRequest(item.rowData);
                setShowListHouse(true);
              }}
            />
          </Reponsive.Default>
          <Reponsive.Mobile>
            <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
              {selected && (
                <>
                  <div style={{ flex: 1 }}>
                    {columns.map(
                      col =>
                        selected[col.key] &&
                        selected[col.key] > 0 && (
                          <h4>
                            {col.header}:
                            {col.accessor
                              ? console.log('aa') && col.accessor(selected)
                              : selected[col.key]}
                          </h4>
                        ),
                    )}
                  </div>
                  <div style={{ flex: 1 }}>
                    {postColumns.map(col => (
                      <h4>
                        {col.header}: {requestInfo[col.key]}
                      </h4>
                    ))}
                  </div>
                </>
              )}
            </Flexbox>
          </Reponsive.Mobile>

          <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            onFinish={onFinish}
            layout="inline"
            autoComplete="off"
          >
            <Form.Item
              label="Địa chỉ"
              name="address"
              rules={[{ required: true, message: 'Vui lòng nhập địa chỉ để tìm kiếm' }]}
            >
              <Input />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
          <Table data={houseSearch} columns={houseColumns} renderCell={renderCellTable} />
        </TabPane>

        <TabPane tab="Phù hợp" key="1">
          <div style={{ maxHeight: window.innerWidth < 500 ? 450 : 600, overflow: 'scroll' }}>
            <Table
              selectable
              data={suitableLst}
              columns={[
                ...tableColumn,
                { header: 'Status', key: 'houseRecommendStatus' },
                { header: '', key: 'priority' },
              ]}
              renderCell={renderCellTable}
            />
            <Label containerStyle={{ marginTop: '10px' }} text="Quan tâm sau" />
            <Table
              selectable
              data={subSuitableLst.length > 0 ? subSuitableLst : []}
              columns={[...tableColumn, { header: 'Status', key: 'houseRecommendStatus' }]}
              renderCell={renderCellTable}
            />
          </div>
        </TabPane>

        <TabPane tab="Đã chào" key="2">
          <Table
            selectable
            data={recommendedLst.length > 0 ? recommendedLst : []}
            columns={[
              ...tableColumn,
              { header: 'Status', key: 'houseRecommendStatus' },
              { header: '', key: 'priority' },
            ]}
            renderCell={renderCellTable}
          />
          <Label containerStyle={{ marginTop: '10px' }} text="Quan tâm sau" />
          <Table
            selectable
            data={subRecommendedLst.length > 0 ? subRecommendedLst : []}
            columns={[...tableColumn, { header: 'Status', key: 'houseRecommendStatus' }]}
            renderCell={renderCellTable}
          />
        </TabPane>
        <TabPane tab="Đã đi xem" key="3">
          <Table
            selectable
            data={seenLst.length > 0 ? seenLst : []}
            columns={[
              ...tableColumn,
              { header: 'Status', key: 'houseRecommendStatus' },
              { header: '', key: 'priority' },
            ]}
            renderCell={renderCellTable}
          />
          <Label containerStyle={{ marginTop: '10px' }} text="Quan tâm sau" />
          <Table
            selectable
            data={subSeenLst.length > 0 ? subSeenLst : []}
            columns={[...tableColumn, { header: 'Status', key: 'houseRecommendStatus' }]}
            renderCell={renderCellTable}
          />
        </TabPane>
        <TabPane tab="Thương lượng" key="4">
          <Table
            selectable
            data={negotiate.length > 0 ? negotiate : []}
            columns={[
              ...tableColumn,
              { header: 'Status', key: 'houseRecommendStatus' },
              { header: '', key: 'priority' },
            ]}
            renderCell={renderCellTable}
          />
          <Label containerStyle={{ marginTop: '10px' }} text="Quan tâm sau" />
          <Table
            selectable
            data={subNegotiate.length > 0 ? subNegotiate : []}
            columns={[...tableColumn, { header: 'Status', key: 'houseRecommendStatus' }]}
            renderCell={renderCellTable}
          />
        </TabPane>
        <TabPane tab="Giao dịch hoàn thành" key="5">
          <Table
            selectable
            data={transactionFinishLst.length > 0 ? transactionFinishLst : []}
            columns={[
              ...tableColumn,
              { header: 'Status', key: 'houseRecommendStatus' },
              { header: '', key: 'priority', textAlign: 'center' },
            ]}
            renderCell={renderCellTable}
          />
        </TabPane>
        <TabPane tab="Không ưng" key="6">
          <Table
            selectable
            data={dislikeLst.length > 0 ? dislikeLst : []}
            columns={[...tableColumn, { header: 'Status', key: 'houseRecommendStatus' }]}
            renderCell={renderCellTable}
          />
        </TabPane>

        <TabPane tab="Các trường bắt buộc" key="7">
          {requireLst.length > 0 ? (
            <Table
              selectable
              data={requireLst && requireLst.length > 0 ? requireLst : []}
              columns={[...tableColumn, { header: 'Status', key: 'houseRecommendStatus' }]}
              renderCell={renderCellTable}
            />
          ) : (
            <Button onClick={() => fetchMatchingsSubRecommendHouse(selected.id)}>Xem</Button>
          )}
        </TabPane>
      </Tabs>
    </Drawer>
  );
};

export const mapStateToProps = state => ({
  dataSource: Selector.requests(state),
  status: Selector.requestGrouping(state).status,
  options: state.metadata[REDUCERS.OPTIONS].data,
  historyRequest: state.request.REQUEST_GROUPING.historyRequestModal,
  state,
});

const mapDispatchToProps = {
  ...actions,
  pushRoute: push,
};

export default connect(mapStateToProps, mapDispatchToProps)(RequestModal);
