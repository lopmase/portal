import { REQUEST_GROUP_TYPES } from './reducer';
import { fetch } from './action';

export const updateStatus = status => ({
  type: REQUEST_GROUP_TYPES.SET_STATUS,
  status,
});

export const changeStatus = status => async dispatch => {
  dispatch(updateStatus(status));
  dispatch(fetch());
};
