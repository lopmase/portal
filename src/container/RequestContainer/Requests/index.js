/* eslint-disable prefer-destructuring */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable camelcase */
/* eslint-disable no-return-assign */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-param-reassign */
/* eslint-disable array-callback-return */
/* eslint-disable no-shadow */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import { push } from 'connected-react-router';
import classnames from 'classnames';
import { Dropdown, Icon, Select, Modal, Form, Checkbox } from 'antd';
import { faEye } from '@fortawesome/free-solid-svg-icons';
import _ from 'lodash';
import { toast } from 'react-toastify';
import { Flexbox, Table, Button, Label } from '../../../components';
import AntTable from '../../../components/AntTable/AntTable';
import { REDUCERS } from '../../../constants/COMMON';
import {
  createHouseRecommendStatus,
  updatePriority,
  getHouseRecommendHistory,
} from '../../../apis/customerApi';
import * as actions from './action';
import { getAll } from '../../../apis/userApi';
import { formatMoney, mappingHouseRecommend } from '../../../helpers/common';
import * as groupingAction from './groupingAction';
import * as optionsAction from '../../HouseContainer/Metadata/optionsAction';
import { columns, tableColumn, tableStatus, historyColumns } from './columns';
import PageSizeSelection from './PageSizeSelection';
import Pagination from './Pagination';
import { confirm } from '../../ShellContainner/ConfirmDialog/action';
import { update } from '../../../apis/requestApi';
import { ROUTES } from '../../../constants/ROUTES';
import Layout from '../../ShellContainner/Layout/Layout';
import FilterByStatus from './FilterByStatus';
import Selector from '../Selector';
import { addSubRecommendHouse } from '../../../apis/houseApi';
import './Requests.scss';
import { useUserProfile } from '../../../hooks/userProfileHook';
import { isAdmin } from '../../../helpers/roleUtil';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import RequestModal from './component/requestModal';
import ActionMenu from './component/actionMenu';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import Excel from '../../Common/ExportExcel/Excel';

const Requests = ({
  dataSource: {
    list,
    fetching,
    total,
    total_pages: totalPages,
    pageSize,
    page: currentPage,
    subList,
  },
  goNextPage,
  goPrevPage,
  changePageSize,
  fetch,
  pushRoute,
  getOptions,
  options,
  changeStatus,
  updateRequestStatus,
  status,
  exportAllData,
  exportExcelByFilter,
  showMessage,
  historyRequest,
  state,
}) => {
  const user = useUserProfile();
  const [users, setUsers] = useState([]);
  const [showListHouse, setShowListHouse] = useState(false);
  const [selected, setSelectRequest] = useState(null);
  const [history, setHistory] = useState();
  const [isShareRequest, setIsShareRequest] = useState(false);
  const [shareInfo, setShareInfo] = useState({
    manager: 0,
  });
  const [visible, setVisible] = useState(false);
  const [suitableLst, setSuitableLst] = useState([]); // phù hơp
  const [subSuitableLst, setSubSuitableLst] = useState([]); // phù hơp
  const [recommendedLst, setRecommendedLst] = useState([]); // đã chào
  const [subRecommendedLst, setSubRecommendedLst] = useState([]);
  const [seenLst, setSeenLst] = useState([]); // đã đi xem
  const [subSeenLst, setSubSeenLst] = useState([]);
  const [negotiate, setNegotiate] = useState([]); // thuong luong
  const [subNegotiate, setSubNegotiate] = useState([]);
  const [transactionFinishLst, setTransactionFinishLst] = useState([]); // hoan thanh
  const [dislikeLst, setDislikeLst] = useState([]); // không ưng
  const [requireLst, setRequireLst] = useState([]); // danh sách theo cấc trường bắt buộc
  const [tabs, setTabs] = useState(1);
  const [houseSearch, setHouseSearch] = useState([]);
  const [houseList, setHouseList] = useState([]);
  const { Option } = Select;
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const getUser = async () => {
    const user = await getAll();
    setUsers(user.data);
    return user;
  };
  useEffect(() => {
    console.log('tab', tabs);
  }, [tabs]);
  useEffect(() => {
    getOptions();
    fetch();
  }, [fetch, getOptions]);
  useEffect(() => {
    getUser();
    historyRequest && setSelectRequest(historyRequest);
    historyRequest && setShowListHouse(true);
  }, []);
  useEffect(() => {
    try {
      // setRecommendedLst([]);
      // setSeenLst([]);
      // setNegotiate([]);
      // setTransactionFinishLst([]);
      // setDislikeLst([]);
      // setSuitableLst([]);
      // setSubRecommendedLst([]);
      // setSubSeenLst([]);
      // setSubNegotiate([]);
      // setSubSuitableLst([]);
      // selected &&
      //   selected.matchings &&
      //   selected.matchings.map(value => {
      //     if (value.houseRecommendStatus === 'Đã chào') {
      //       value.priority === 1 && setRecommendedLst(recommendedLst => [...recommendedLst, value]);
      //       value.priority === 0 &&
      //         setSubRecommendedLst(recommendedLst => [...recommendedLst, value]);
      //     } else if (value.houseRecommendStatus === 'Đã đi xem') {
      //       value.priority === 1 && setSeenLst(seenLst => [...seenLst, value]);
      //       value.priority === 0 && setSubSeenLst(seenLst => [...seenLst, value]);
      //     } else if (value.houseRecommendStatus === 'Thương lượng') {
      //       value.priority === 1 && setNegotiate(negotiate => [...negotiate, value]);
      //       value.priority === 0 && setSubNegotiate(negotiate => [...negotiate, value]);
      //     } else if (value.houseRecommendStatus === 'Giao dịch hoàn thành') {
      //       value.priority === 1 &&
      //         setTransactionFinishLst(transactionFinishLst => [...transactionFinishLst, value]);
      //     } else if (value.houseRecommendStatus === 'Không ưng') {
      //       value.priority === 1 && setDislikeLst(dislikeLst => [...dislikeLst, value]);
      //     } else if (value.houseRecommendStatus === 'Phù hợp sau') {
      //       value.priority === 0 && setSubSuitableLst(subSuitableLst => [...subSuitableLst, value]);
      //     } else {
      //       setSuitableLst(suitableLst => [...suitableLst, value]);
      //     }
      //   });

      mappingHouseRecommend({
        setRecommendedLst,
        setSeenLst,
        setNegotiate,
        setTransactionFinishLst,
        setDislikeLst,
        setSuitableLst,
        setSubRecommendedLst,
        setSubSeenLst,
        setSubNegotiate,
        setSubSuitableLst,
        setRequireLst,
        selected,
      });
    } catch (err) {
      console.log('err', err);
      showMessage('Something went wrong. Please try agian');
    }
  }, [selected, showMessage]);
  useEffect(() => {
    try {
      if (subList && subList.length > 0 && subList[0].house_require) {
        mappingHouseRecommend({
          setRecommendedLst,
          setSeenLst,
          setNegotiate,
          setTransactionFinishLst,
          setDislikeLst,
          setSuitableLst,
          setSubRecommendedLst,
          setSubSeenLst,
          setSubNegotiate,
          setSubSuitableLst,
          setRequireLst,
          houseRequire: subList[0].house_require,
        });
      }
    } catch (err) {
      console.log('err', err);
      showMessage('Something went wrong. Please try agian');
    }
  }, [subList, showMessage]);

  useEffect(() => {
    if (selected && houseList) {
      const selectedChange = houseList.find(match => match.id === selected.id);
      setSelectRequest({
        ...selected,
        matchings: selectedChange.matching,
      });
    }
    setHouseList(list);
  }, [list]);

  const getHistory = requestId => {
    try {
      getHouseRecommendHistory({ request_id: requestId }).then(resolve => {
        resolve.data.map(value => {
          value.target === 1 && (value.target = 'Đã chào');
          value.target === 2 && (value.target = 'Đã đi xem');
          value.target === 3 && (value.target = 'Thương lượng');
          value.target === 4 && (value.target = 'Giao dịch hoàn thành');
          value.target === 0 && (value.target = 'Không ưng');
        });
        setHistory(resolve.data);
        setVisible(true);
      });
    } catch (err) {
      console.log('err', err);
      showMessage('Something went wrong. Please try agian');
    }
  };
  const shareRequest = item => {
    try {
      const shareSelectedItem = list ? list.filter(e => e.id === item.id)[0] : null;
      shareSelectedItem &&
        setShareInfo({
          staff:
            shareSelectedItem.staff &&
            users.filter(user => shareSelectedItem.staff.includes(user.id)).map(e => e.name),

          manager: shareSelectedItem.manager,
          staffRole: shareSelectedItem.staff_role,
          managerRole: shareSelectedItem.manager_role,
          requestId: item.id,
        });
      setIsShareRequest(true);
    } catch (err) {
      showMessage(err.message || UNKNOWN_ERROR);
    }
  };
  const onFinish = values => {
    try {
      const info = {
        staff: values.staff,
        manager: values.manager,
        manager_role: shareInfo.managerRole,
        staff_role: shareInfo.staffRole,
        request_id: shareInfo.requestId,
      };
      const rs = update(info);
      rs && setIsShareRequest(false);
    } catch (err) {
      showMessage(err.message || UNKNOWN_ERROR);
    }
  };

  const onFinishFailed = () => {
    showMessage(UNKNOWN_ERROR);
  };
  const changeRecommendStatus = async value => {
    try {
      selected &&
        createHouseRecommendStatus({
          house_id: value.item.id,
          customer_id: selected.customer_id,
          status: value.value,
          request_id: selected.id,
          previousTarget: tabs - 1,
        }).then(resolve => {
          if (tabs === 1) {
            const index = suitableLst.findIndex(element => element.id === value.item.id);
            suitableLst.splice(index, 1);
            index > -1 && setSuitableLst(suitableLst => [...suitableLst]);
          } else if (tabs === 2) {
            const indexOfRecommendedLst = recommendedLst.findIndex(
              element => element.id === value.item.id,
            ); // find element in main table
            const indexOfSubRecommendedLst = subRecommendedLst.findIndex(
              element => element.id === value.item.id,
            ); // find element in subtable
            // if exist main table remove element in main table and set value again
            indexOfRecommendedLst > -1 && recommendedLst.splice(indexOfRecommendedLst, 1);
            indexOfRecommendedLst > -1 && setRecommendedLst(recommendedLst => [...recommendedLst]);
            // if exist in subtable remove element in subtable and set value again
            indexOfSubRecommendedLst > -1 && subRecommendedLst.splice(indexOfSubRecommendedLst, 1);
            indexOfSubRecommendedLst > -1 &&
              setSubRecommendedLst(subRecommendedLst => [...subRecommendedLst]);
          } else if (tabs === 3) {
            const indexOfSeenLst = seenLst.findIndex(element => element.id === value.item.id);
            const indexOfSubSeenLst = subSeenLst.findIndex(element => element.id === value.item.id);
            indexOfSeenLst > -1 && seenLst.splice(indexOfSeenLst, 1);
            indexOfSeenLst > -1 && setSeenLst(seenLst => [...seenLst]);
            indexOfSubSeenLst > -1 && subSeenLst.splice(indexOfSubSeenLst, 1);
            indexOfSubSeenLst > -1 && setSubSeenLst(subSeenLst => [...subSeenLst]);
          } else if (tabs === 4) {
            const indexOfNegotiate = negotiate.findIndex(element => element.id === value.item.id);
            const indexOfSubNegotiate = subNegotiate.findIndex(
              element => element.id === value.item.id,
            );
            indexOfNegotiate > -1 && negotiate.splice(indexOfNegotiate, 1);
            indexOfNegotiate > -1 && setNegotiate(negotiate => [...negotiate]);
            indexOfSubNegotiate > -1 && subNegotiate.splice(indexOfSubNegotiate, 1);
            indexOfSubNegotiate > -1 && setSubNegotiate(subNegotiate => [...subNegotiate]);
          } else if (tabs === 5) {
            const index = transactionFinishLst.findIndex(element => element.id === value.item.id);
            transactionFinishLst.splice(index, 1);
            index > -1 &&
              setTransactionFinishLst(transactionFinishLst => [...transactionFinishLst]);
          } else {
            const index = dislikeLst.findIndex(element => element.id === value.item.id);
            dislikeLst.splice(index, 1);
            index > -1 && setDislikeLst(dislikeLst => [...dislikeLst]);
          }
          const index = selected.matchings.findIndex(
            element => element.id === resolve.data.house_id,
          );
          if (index > -1) {
            resolve.data.status === 1 &&
              (selected.matchings[index].houseRecommendStatus = 'Đã chào') &&
              setRecommendedLst(recommendedLst.concat(selected.matchings[index]));
            resolve.data.status === 2 &&
              (selected.matchings[index].houseRecommendStatus = 'Đã đi xem') &&
              setSeenLst(seenLst.concat(selected.matchings[index]));
            resolve.data.status === 3 &&
              (selected.matchings[index].houseRecommendStatus = 'Thương lượng') &&
              setNegotiate(negotiate.concat(selected.matchings[index]));
            resolve.data.status === 4 &&
              (selected.matchings[index].houseRecommendStatus = 'Giao dịch hoàn thành') &&
              setTransactionFinishLst(transactionFinishLst.concat(selected.matchings[index]));
            resolve.data.status === 0 &&
              (selected.matchings[index].houseRecommendStatus = 'Không ưng') &&
              setDislikeLst(dislikeLst.concat(selected.matchings[index]));
          }
        });
    } catch (err) {
      showMessage(err.message || UNKNOWN_ERROR);
    }
  };
  const changePriority = (houseId, customerId) => {
    try {
      updatePriority({ houseId, customerId, previousTarget: tabs - 1 }).then(resolve => {
        if (resolve.data.status === 1) {
          const index = recommendedLst.findIndex(element => element.id === houseId);
          index > -1 && setSubRecommendedLst(subRecommendedLst.concat(recommendedLst[index]));
          index > -1 && recommendedLst.splice(index, 1);
          index > -1 && setRecommendedLst(recommendedLst => [...recommendedLst]);
        } else if (resolve.data.status === 2) {
          const index = seenLst.findIndex(element => element.id === houseId);
          index > -1 && setSubSeenLst(subSeenLst.concat(seenLst[index]));
          index > -1 && seenLst.splice(index, 1);
          index > -1 && setSeenLst(seenLst => [...seenLst]);
        } else if (resolve.data.status === 3) {
          const index = negotiate.findIndex(element => element.id === houseId);
          index > -1 && setSubNegotiate(subNegotiate.concat(negotiate[index]));
          index > -1 && negotiate.splice(index, 1);
          index > -1 && setNegotiate(negotiate => [...negotiate]);
        } else if (resolve.data.status === 0) {
          const index = suitableLst.findIndex(element => element.id === houseId);
          index > -1 && setSubSuitableLst(subSuitableLst.concat(suitableLst[index]));
          index > -1 && suitableLst.splice(index, 1);
          index > -1 && setSuitableLst(suitableLst => [...suitableLst]);
        }
      });
    } catch (err) {
      showMessage(err.message || UNKNOWN_ERROR);
    }
  };
  const addSuitableList = async ({ house_id, customer_id, request_id }) => {
    addSubRecommendHouse({
      house_id,
      customer_id,
      request_id,
    })
      .then(() => {
        fetch();
        toast.success('Khôi phục bảng thành công');
        const house = houseSearch.filter(house => house.id !== house_id);

        setHouseSearch(house);
      })
      .catch(error => {
        showMessage(error.message || UNKNOWN_ERROR);
      });
  };
  const renderCellTable = data => {
    const { item, column } = data;
    if (column.key === 'house_address') {
      return (
        <td>
          <div>{`${item.house_number} ${item.house_address}, ${item.district}, ${item.province}`}</div>
        </td>
      );
    }
    if (column.key === 'add') {
      return (
        <td>
          <Button
            text="Thêm sản phẩm phù hợp"
            onClick={() => {
              addSuitableList({
                house_id: item.id,
                customer_id: item.customer_id,
                request_id: selected.id,
              });
            }}
          />
        </td>
      );
    }
    if (column.key === 'into_money') {
      return (
        <td>
          <div>{formatMoney(item.into_money)}</div>
        </td>
      );
    }
    if (column.key === 'project') {
      return (
        <td>
          <div>{(item.project && item.project.name) || ''}</div>
        </td>
      );
    }
    if (column.key === 'action') {
      return (
        <td>
          <Button
            icon={faEye}
            text="Xem"
            onClick={() => {
              pushRoute(`${ROUTES.HOUSE_ROOT}/${item.id}`);
            }}
          />
        </td>
      );
    }
    if (column.key === 'houseRecommendStatus') {
      return (
        <td>
          <Select
            defaultValue="Không xác định"
            value={item.houseRecommendStatus ? item.houseRecommendStatus : 'Không xác định'}
            onChange={value => changeRecommendStatus({ item, value })}
            style={{ width: 180 }}
          >
            {tableStatus.map(value => (
              <Option key={value.key} value={value.key}>
                {value.value}
              </Option>
            ))}
          </Select>
        </td>
      );
    }
    if (column.key === 'info') {
      return (
        <td>
          <div>{`${item.house_number} ${item.house_address}`}</div>
          <div style={{ fontWeight: 'bold' }}>{`${item.district} / ${item.province}`}</div>
        </td>
      );
    }
    if (column.key === 'priority') {
      return (
        <td>
          <Button text="Xem sau" onClick={() => changePriority(item.id, selected.customer_id)} />
        </td>
      );
    }
    if (column.key === 'images') {
      return (
        <td>
          {_.values(item.imageUrls)
            .slice(0, 4)
            .map(image => (
              <div className="image">
                <img src={URL + image.thumbnail} alt="sss" />
              </div>
            ))}
        </td>
      );
    }

    const cell = item[column.key];
    let value = '';
    if (column.accessor) {
      value = column.accessor(item, {});
    } else {
      value = cell ? cell.toString() : '';
    }
    return <td className={column.key === 'id' ? 'bold-text' : ''}>{value}</td>;
  };

  const renderCell = ({ item, column }) => {
    if (column.key === 'detail') {
      return (
        <td>
          <div>
            {user &&
              (item.user_id === user.id ||
                (item.staff && item.staff.includes(user.id) && item.staff_role === 1) ||
                (item.manager &&
                  Array.isArray(item.manager) &&
                  item.manager.includes(user.id) &&
                  item.manager_role === 1) ||
                (item.manager && item.manager === user.id && item.manager_role === 1) ||
                isAdmin(user)) && (
                <Dropdown
                  overlay={props => (
                    <ActionMenu
                      item={item}
                      pushRoute={pushRoute}
                      updateRequestStatus={updateRequestStatus}
                      getHistory={getHistory}
                      shareRequest={shareRequest}
                      user={user}
                      {...props}
                    />
                  )}
                >
                  <div className="action-btn">
                    Chọn
                    <Icon style={{ marginLeft: 5 }} type="down" />
                  </div>
                </Dropdown>
              )}
          </div>
        </td>
      );
    }
    if (column.key === 'place') {
      const districtsDropDown =
        item.districts && item.districts.length > 3
          ? item.districts.map((value, index) => (value = { label: value, key: index }))
          : [];
      return (
        <td>
          <div>{item.districts && item.districts.length <= 3 ? item.districts.join(', ') : ''}</div>
          {item.districts && item.districts.length > 3 && (
            <Select
              defaultValue={[item.districts[0], item.districts[1]]}
              style={{ width: 150 }}
              bordered={false}
            >
              {districtsDropDown.map(value => (
                <Option key={value.key} value={value.label}>
                  {value.label.split(' ').length === 2
                    ? value.label
                    : value.label
                        .split(' ')
                        .slice(1)
                        .join(' ')}
                </Option>
              ))}
            </Select>
          )}
          {item.province && Array.isArray(item.province) ? (
            <div style={{ fontWeight: 'bold' }}>{item.province.join(', ')}</div>
          ) : (
            <div style={{ fontWeight: 'bold' }}>{item.province}</div>
          )}
        </td>
      );
    }
    if (column.key === 'customer') {
      return (
        <td>
          <div className="customer">{item.customer ? item.customer.name : ''}</div>
          <div style={{ fontWeight: 'bold' }}>{item.purpose === '0' ? 'Mua' : 'Thuê'}</div>
        </td>
      );
    }

    if (column.key === 'matched_product') {
      const matchingsLength =
        item.matchings && Array.isArray(item.matchings) ? item.matchings.length : 0;
      const subMatchingLength =
        item.house_require && Array.isArray(item.house_require) ? item.house_require.length : 0;
      return (
        <td>
          <Button
            text={`${matchingsLength + subMatchingLength} BĐS`}
            clear
            onClick={() => {
              setSelectRequest(item);
              setShowListHouse(true);
              dispatch({
                type: 'REQUEST_GROUP_TYPES/SET_HISTORY_REQUEST_MODAL',
                payload: item,
              });
            }}
          />
        </td>
      );
    }
    const cell = item[column.key];
    let value = '';
    if (column.accessor) {
      value = column.accessor(item, options || {});
    } else {
      value = cell ? cell.toString() : '';
    }
    return (
      <td
        className={classnames({
          'bold-text': column.key === 'id',
          [column.class]: column.class,
        })}
      >
        {value}
      </td>
    );
  };

  return (
    <div className="requests-page" style={{}}>
      <Layout
        renderHeader={() => (
          <>
            <Flexbox row spaceBetween>
              <Flexbox row flexStart alignItems="center" containerStyle={{ marginBottom: 10 }}>
                <h1 style={{ marginRight: 10 }}>Nhu cầu</h1>
                <Label text={total} />
              </Flexbox>
              <Link to={ROUTES.NEW_REQUEST}>
                <Button clear text="+ Tạo mới" />
              </Link>
            </Flexbox>
          </>
        )}
        bodyStyle={{ padding: 20 }}
      >
        <div className="toolbar">
          <Flexbox row spaceBetween containerStyle={{ marginBottom: 20, alignItems: 'center' }}>
            <Flexbox>
              <PageSizeSelection
                selected={pageSize}
                onChanged={({ item }) => changePageSize(item.value)}
              />
            </Flexbox>
            <div>
              <Pagination
                currentPage={currentPage}
                pageNumbers={totalPages}
                onNext={goNextPage}
                onPrev={goPrevPage}
              />
            </div>
            <Flexbox>
              <FilterByStatus selected={status} onChanged={changeStatus} />
              <Excel onExportFiltered={exportExcelByFilter} onExportAll={exportAllData} />
            </Flexbox>
          </Flexbox>
        </div>
        <div style={{}}>
          <Table
            loading={fetching}
            data={houseList}
            columns={columns}
            renderCell={renderCell}
            rowDoubleClick={item => {
              setSelectRequest(item.rowData);
              setShowListHouse(true);
            }}
          />
        </div>
        <div>
          <Flexbox row containerStyle={{ marginTop: 20, alignItems: 'center' }}>
            <Pagination
              currentPage={currentPage}
              pageNumbers={totalPages}
              onNext={goNextPage}
              onPrev={goPrevPage}
            />
          </Flexbox>
        </div>

        <RequestModal
          options={options}
          setHouseSearch={setHouseSearch}
          houseSearch={houseSearch}
          transactionFinishLst={transactionFinishLst}
          setShowListHouse={setShowListHouse}
          showListHouse={showListHouse}
          selected={selected}
          houseRequire={
            selected && selected.house_require && selected.house_require.length > 0
              ? selected.house_require
              : []
          }
          requireLst={requireLst}
          columns={columns}
          setSelectRequest={setSelectRequest}
          suitableLst={suitableLst}
          tableColumn={tableColumn}
          renderCellTable={renderCellTable}
          setTabs={setTabs}
          subSuitableLst={subSuitableLst}
          recommendedLst={recommendedLst}
          subRecommendedLst={subRecommendedLst}
          seenLst={seenLst}
          subSeenLst={subSeenLst}
          negotiate={negotiate}
          subNegotiate={subNegotiate}
          dislikeLst={dislikeLst}
        />

        {history && (
          <Modal
            title="Lịch sử"
            centered
            visible={visible}
            onOk={() => setVisible(false)}
            onCancel={() => setVisible(false)}
            width={1000}
          >
            <AntTable
              data={history}
              columns={[
                ...historyColumns,
                {
                  title: 'Xem',
                  dataIndex: 'house_id',
                  width: 150,
                  render: value => (
                    <Button
                      icon={faEye}
                      text="Chi tiết"
                      onClick={() => pushRoute(`${ROUTES.HOUSE_ROOT}/${value}`)}
                    />
                  ),
                },
              ]}
              scroll
              width={900}
              height={400}
            />
          </Modal>
        )}
        {isShareRequest && (
          <Modal
            title="Chia sẻ"
            centered
            visible={isShareRequest}
            onOk={form.submit}
            onCancel={() => setIsShareRequest(false)}
            width={1000}
          >
            <Form
              name="basic"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
              form={form}
            >
              <Form.Item label="Nhân viên" name="staff">
                <Select
                  defaultValue={shareInfo.staff}
                  mode="multiple"
                  className="selectMonth"
                  filterOption={(input, option) => {
                    return (
                      (option.children &&
                        option.children.toUpperCase().indexOf(input.toUpperCase()) >= 0) ||
                      (option.children &&
                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0)
                    );
                  }}
                >
                  {users.length > 0 &&
                    users.map(value => (
                      <Option key={value.id} value={value.id}>
                        {value.name}
                      </Option>
                    ))}
                </Select>
              </Form.Item>

              <Form.Item label="Quyền chỉnh sửa" name="staffRole">
                <Label text="Có" />
                <Checkbox
                  onChange={() => {
                    setShareInfo({ ...shareInfo, staffRole: 1 });
                  }}
                  // defaultChecked={checkBox.fixed}
                  checked={shareInfo.staffRole === 1}
                />
                <Label text="Không" />
                <Checkbox
                  onChange={() => {
                    setShareInfo({ ...shareInfo, staffRole: 0 });
                  }}
                  // defaultChecked={checkBox.fixed}
                  checked={shareInfo.staffRole === 0}
                />
              </Form.Item>

              <Form.Item label="Manager" name="manager">
                <Select
                  defaultValue={
                    shareInfo.manager &&
                    Array.isArray(shareInfo.manager) &&
                    shareInfo.manager.length > 1
                      ? 'Các cấp trên'
                      : Array.isArray(shareInfo.manager) && shareInfo.manager.length === 1
                      ? 'Cấp trên trực tiếp'
                      : 'Không'
                  }
                >
                  <Option key={0} value={0}>
                    Không
                  </Option>
                  <Option key={1} value={1}>
                    Cấp trên trực tiếp
                  </Option>
                  <Option key={2} value={2}>
                    Các cấp trên
                  </Option>
                </Select>
              </Form.Item>
              <Form.Item label="Quyền chỉnh sửa" name="managerfRole">
                <Label text="Có" />
                <Checkbox
                  onChange={() => {
                    setShareInfo({ ...shareInfo, managerRole: 1 });
                  }}
                  checked={shareInfo.managerRole === 1}
                />
                <Label text="Không" />
                <Checkbox
                  onChange={() => {
                    setShareInfo({ ...shareInfo, managerRole: 0 });
                  }}
                  checked={shareInfo.managerRole === 0}
                />
              </Form.Item>
            </Form>
          </Modal>
        )}
      </Layout>
    </div>
  );
};

export const mapStateToProps = state => ({
  dataSource: Selector.requests(state),
  status: Selector.requestGrouping(state).status,
  options: state.metadata[REDUCERS.OPTIONS].data,
  historyRequest: state.request.REQUEST_GROUPING.historyRequestModal,
  state,
});

const mapDispatchToProps = {
  ...actions,
  ...optionsAction,
  ...groupingAction,
  showMessage,
  pushRoute: push,
  openConfirmDialog: confirm,
};

export default connect(mapStateToProps, mapDispatchToProps)(Requests);
