import { REQUEST_STATUS } from '../../../constants/COMMON';

export const REQUEST_GROUP_TYPES = {
  SET_STATUS: 'REQUEST_GROUP_TYPES/SET_STATUS',
  SET_PERMISSION: 'REQUEST_GROUP_TYPES/SET_PERMISSION',
  SET_HISTORY_REQUEST_MODAL: 'REQUEST_GROUP_TYPES/SET_HISTORY_REQUEST_MODAL',
};

const initial = {
  status: REQUEST_STATUS[0].value,
  historyRequestModal: null,
};

const groupRequestReducer = (state = initial, action) => {
  switch (action.type) {
    case REQUEST_GROUP_TYPES.SET_STATUS:
      return {
        ...state,
        status: action.status,
      };
    case REQUEST_GROUP_TYPES.SET_HISTORY_REQUEST_MODAL:
      return {
        ...state,
        historyRequestModal: action.payload,
      };
    default:
      return state;
  }
};

export default groupRequestReducer;
