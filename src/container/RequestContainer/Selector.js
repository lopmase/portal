import { REDUCERS } from '../../constants/COMMON';

export default {
  requests: state => state.request[REDUCERS.REQUESTS],
  detail: state => state.request[REDUCERS.REQUEST_DETAIL],
  requestGrouping: state => state.request[REDUCERS.REQUEST_GROUPING],
};
