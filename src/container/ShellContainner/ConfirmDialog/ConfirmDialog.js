import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import * as actions from './action';

import { REDUCERS } from '../../../constants/COMMON';
import { Modal, Flexbox, Button } from '../../../components';

const ConfirmDialog = props => {
  const { visible, closeModal, message, doConfirm, description } = props;
  return (
    <Modal
      onClose={closeModal}
      visible={visible}
      containerStyle={{ width: 400 }}
      renderModal={() => (
        <div style={{ padding: 20 }}>
          <h3>{message}</h3>
          {description && <p style={{ fontSize: 13, color: 'gray' }}>{description}</p>}
          <Flexbox row spaceBetween>
            <div />
            <div>
              <Button onClick={closeModal} clear text="Huỷ" />
              <Button onClick={doConfirm} text="Chắc" />
            </div>
          </Flexbox>
        </div>
      )}
    />
  );
};

ConfirmDialog.propTypes = {
  visible: PropTypes.bool.isRequired,
  message: PropTypes.string.isRequired,
  closeModal: PropTypes.func.isRequired,
  doConfirm: PropTypes.func.isRequired,
  description: PropTypes.string,
};

ConfirmDialog.defaultProps = {
  description: null,
};

export function mapStateToProps(state) {
  return {
    visible: state.modal[REDUCERS.CONFIRM_DIALOG].visible,
    message: state.modal[REDUCERS.CONFIRM_DIALOG].message,
    description: state.modal[REDUCERS.CONFIRM_DIALOG].description,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...actions }, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ConfirmDialog));
