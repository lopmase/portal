import { REDUCERS } from '../../../constants/COMMON';
import { CONFIRM_DIALOG_TYPES } from './TYPES';

const openModal = (message, description) => ({
  type: CONFIRM_DIALOG_TYPES.OPEN,
  reducerId: REDUCERS.CONFIRM_DIALOG,
  message,
  description,
});

export const closeModal = () => ({
  type: CONFIRM_DIALOG_TYPES.CLOSE,
  reducerId: REDUCERS.CONFIRM_DIALOG,
});

const createCallback = () => {
  let listener = null;
  let callback = null;
  return {
    on: newListener => {
      listener = newListener;
    },
    setCallback: func => {
      callback = func;
    },
    run: result => {
      callback && callback(result);
      listener(result);
    },
  };
};

const callbackInstance = createCallback();

export const confirm = (message, fn, description) => async dispatch => {
  dispatch(openModal(message, description));
  callbackInstance.setCallback(fn);

  return new Promise(resolve => {
    callbackInstance.on(result => {
      resolve(result);
    });
  });
};

export const doConfirm = reply => async dispatch => {
  dispatch(closeModal());
  callbackInstance.run(reply);
};
