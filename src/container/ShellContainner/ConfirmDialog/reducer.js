import { CONFIRM_DIALOG_TYPES } from './TYPES';

const initial = {
  visible: false,
  message: '',
  description: '',
};
const confirmReducer = (state = initial, action) => {
  switch (action.type) {
    case CONFIRM_DIALOG_TYPES.OPEN:
      return {
        visible: true,
        message: action.message,
        description: action.description,
      };
    case CONFIRM_DIALOG_TYPES.CLOSE:
      return {
        visible: false,
        message: '',
      };
    default:
      return state;
  }
};

export default confirmReducer;
