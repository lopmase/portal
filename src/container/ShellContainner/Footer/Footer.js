/* eslint-disable react/prop-types */

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { withRouter, Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Flexbox } from '../../../components';
import { isAdmin } from '../../../helpers/roleUtil';

import * as actions from '../NotificationAction';
import './Footer.scss';
import { menuItems } from '../../../components/Drawer/Drawer';

const Footer = props => {
  const { location, user } = props;
  return (
    <footer className="footer-container">
      <Flexbox row spaceBetween>
        {menuItems
          .filter(
            item =>
              !item.requireAdmin ||
              (user && isAdmin(user)) ||
              (user && user.chat_permission === item.staffPermission),
          )
          .map(menu => (
            <Link
              key={menu.route}
              to={menu.route}
              style={{ flex: 1, textAlign: 'center', paddingTop: 10 }}
            >
              <div
                key={menu.name}
                className={classNames('item', { active: location.pathname === menu.route })}
              >
                {menu.fontawesome ? menu.icon : <FontAwesomeIcon icon={menu.icon} color="white" />}
              </div>
            </Link>
          ))}
      </Flexbox>
    </footer>
  );
};

export function mapStateToProps(state) {
  return {
    user: state.auth.user.data,
    loading: state.auth.user.fetching,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...actions }, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Footer));
