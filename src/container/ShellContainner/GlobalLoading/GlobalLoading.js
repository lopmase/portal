import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

import './Style.scss';
import { REDUCERS } from '../../../constants/COMMON';

const GlobalLoading = props => {
  const { visible } = props;
  return (
    <div className="loading-container">
      {visible && (
        <div className="global-loading">
          <FontAwesomeIcon icon={faSpinner} size="3x" spin color="white" />
        </div>
      )}
    </div>
  );
};

GlobalLoading.propTypes = {
  visible: PropTypes.bool.isRequired,
};

GlobalLoading.defaultProps = {};

export function mapStateToProps(state) {
  return {
    visible: state.modal[REDUCERS.GLOBAL_LOADING].visible,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GlobalLoading));
