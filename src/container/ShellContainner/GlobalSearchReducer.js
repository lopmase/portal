export const GLOBAL_ACTION_TYPES = {
  UPDATE_SEARCH: 'GLOBAL_ACTION_TYPES/UPDATE_SEARCH',
  CLEAR_SEARCH: 'GLOBAL_ACTION_TYPES/CLEAR_SEARCH',
};

const initial = {
  searchValue: '',
  model: 'HOUSE',
};
const globalSearchReducer = (state = initial, action) => {
  switch (action.type) {
    case GLOBAL_ACTION_TYPES.UPDATE_SEARCH:
      return {
        searchValue: action.searchValue,
        model: action.modal || 'HOUSE',
      };
    case GLOBAL_ACTION_TYPES.CLEAR_SEARCH:
      return {
        searchValue: '',
        model: action.modal || 'HOUSE',
      };
    default:
      return state;
  }
};

export default globalSearchReducer;
