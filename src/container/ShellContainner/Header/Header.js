import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Flexbox, Button } from '../../../components';

import './Header.scss';
import { ROUTES } from '../../../constants/ROUTES';
import avatar from '../../../assets/images/avatar.jpg';
import { generateModalActions } from '../../../helpers/modalAction';
import { REDUCERS } from '../../../constants/COMMON';
import { searchGlobal } from '../SearchAction';
import Notification from './Notification';
import { formatImageUrl } from '../../../helpers/common';

const { openModal } = generateModalActions(REDUCERS.USER_PROFILE_MODAL);

class Header extends Component {
  static propTypes = {
    renderLeft: PropTypes.func,
    openProfile: PropTypes.func.isRequired,
    // search: PropTypes.func.isRequired,
    // searchValue: PropTypes.string.isRequired,
    user: PropTypes.object.isRequired,
  };

  static defaultProps = {
    renderLeft: null,
  };

  componentDidMount() {}

  render() {
    const { user, openProfile, renderLeft } = this.props;
    return (
      <div className="header-container">
        <Flexbox row spaceBetween>
          {renderLeft ? renderLeft() : <h2>Trang Chủ</h2>}
          {/* <div className="search-box">
            <FontAwesomeIcon icon={faSearch} />
            <Input
              placeholder="Tìm kiếm"
              value={searchValue}
              onChange={(name, value) => search(value)}
            />
          </div> */}
          <Flexbox row>
            <Link to={ROUTES.NEW_HOUSE}>
              <Button clear text="+ Tạo BĐS" />
            </Link>
            <div className="divider" />
            <Notification />
            <div className="avatar" onClick={openProfile}>
              <img
                src={user && user.avatar ? formatImageUrl(user.avatar.thumbnail) : avatar}
                alt="avatar"
              />
            </div>
          </Flexbox>
        </Flexbox>
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    user: state.auth.user.data,
    loading: state.auth.user.fetching,
    searchValue: state.search.searchValue,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({ openProfile: openModal, search: searchGlobal }, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
