import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBell, faSearch, faPlus } from '@fortawesome/free-solid-svg-icons';
import { Flexbox } from '../../../components';

import * as actions from '../NotificationAction';
import './Header.scss';
import { ROUTES } from '../../../constants/ROUTES';
import avatar from '../../../assets/images/avatar.jpg';
import { generateModalActions } from '../../../helpers/modalAction';
import { REDUCERS } from '../../../constants/COMMON';
import { formatImageUrl } from '../../../helpers/common';

const { toggle } = generateModalActions(REDUCERS.NOTI_POPUP);
const { openModal } = generateModalActions(REDUCERS.USER_PROFILE_MODAL);

class MobileHeader extends Component {
  static propTypes = {
    allNotifications: PropTypes.func.isRequired,
    toggleModal: PropTypes.func.isRequired,
    readNotification: PropTypes.func.isRequired,
    openProfile: PropTypes.func.isRequired,
    visible: PropTypes.bool.isRequired,
    user: PropTypes.object.isRequired,
    notifications: PropTypes.array.isRequired,
  };

  static defaultProps = {};

  componentDidMount() {
    const { allNotifications } = this.props;
    allNotifications();
  }

  openNoti = () => {
    const { toggleModal, readNotification, allNotifications, visible } = this.props;

    if (visible) {
      readNotification();
      allNotifications();
    }
    toggleModal();
  };

  render() {
    const { notifications, user, openProfile } = this.props;
    return (
      <div className="header-container mobile">
        <Flexbox row spaceBetween>
          <FontAwesomeIcon icon={faSearch} color="gray" style={{ fontSize: 22 }} />
          <div className="middle-button">
            <Link to={ROUTES.NEW_HOUSE}>
              <FontAwesomeIcon icon={faPlus} color="white" style={{ fontSize: 30 }} />
            </Link>
          </div>
          <Flexbox row>
            <div className="noti" onClick={this.openNoti}>
              <FontAwesomeIcon
                icon={faBell}
                color="gray"
                style={{ fontSize: 22, marginRight: 10 }}
              />
              <div className="badge">{notifications.filter(n => !n.status).length}</div>
            </div>
            <div className="avatar" onClick={openProfile}>
              <img
                src={user && user.avatar ? formatImageUrl(user.avatar.thumbnail) : avatar}
                alt="avatar"
              />
            </div>
          </Flexbox>
        </Flexbox>
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    user: state.auth.user.data,
    loading: state.auth.user.fetching,
    notifications: state.notification.list,
    fetchingNoti: state.notification.fetching,
    visible: state.modal[REDUCERS.NOTI_POPUP].visible,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...actions, toggleModal: toggle, openProfile: openModal }, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MobileHeader));
