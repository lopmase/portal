/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBell } from '@fortawesome/free-solid-svg-icons';
import { REDUCERS } from '../../../constants/COMMON';
import { generateModalActions } from '../../../helpers/modalAction';
import * as actions from '../NotificationAction';
import './Notification.scss';

const { toggle } = generateModalActions(REDUCERS.NOTI_POPUP);

const Notification = ({
  toggleModal,
  readNotification,
  allNotifications,
  visible,
  notifications,
}) => {
  useEffect(() => {
    allNotifications();
  }, [allNotifications]);

  const openNoti = () => {
    if (visible) {
      readNotification();
      allNotifications();
    }
    toggleModal();
  };
  return (
    <div className="noti" onClick={openNoti}>
      <FontAwesomeIcon icon={faBell} color="gray" style={{ fontSize: 22, marginRight: 10 }} />
      <div className="badge">{notifications.filter(n => !n.status).length}</div>
    </div>
  );
};

export function mapStateToProps(state) {
  return {
    notifications: state.notification.list,
    visible: state.modal[REDUCERS.NOTI_POPUP].visible,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...actions, toggleModal: toggle }, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Notification));
