import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import './Layout.scss';

const Layout = props => {
  const { renderHeader, children, bodyStyle } = props;
  return (
    <div className="layout-container">
      <div className="big-header">{renderHeader()}</div>
      <div className="layout-body" style={{ ...bodyStyle }}>
        {children}
      </div>
    </div>
  );
};

Layout.propTypes = {
  bodyStyle: PropTypes.object,
  renderHeader: PropTypes.func.isRequired,
  children: PropTypes.any.isRequired,
};

Layout.defaultProps = {
  bodyStyle: {},
};

export function mapStateToProps() {
  return {};
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout));
