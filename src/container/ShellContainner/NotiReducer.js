import { REDUCERS } from '../../constants/COMMON';
import createDatalistReducer from '../../helpers/createDataListReducer';

export default createDatalistReducer(REDUCERS.NOTIFICATION);
