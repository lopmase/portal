import _ from 'lodash';
import { toast } from 'react-toastify';
import { apiSerializer } from '../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../constants/MESSAGE';
import { REDUCERS } from '../../constants/COMMON';
import { generateDatalistActions } from '../../helpers/dataListAction';

const { fetchDone, fetchError, fetching } = generateDatalistActions(REDUCERS.NOTIFICATION);

export const allNotifications = size => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const response = await thunkDependencies.user.notifications(size);
    const result = apiSerializer(response);
    dispatch(fetchDone(result.data, result.total, Math.ceil(result.total / size)));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};

export const readNotification = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const notis = getState().notification.list;
    const ids = notis.filter(noti => !noti.status).map(noti => noti.id);
    if (ids.length) {
      _.each(ids, async id => {
        const response = await thunkDependencies.user.readNotification(id);
        apiSerializer(response);
      });
    }
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    toast.error(message);
  }
};
