/* eslint-disable no-nested-ternary */
/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import './NotificationPopup.scss';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import classNames from 'classnames';
import moment from 'moment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faComment, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { REDUCERS, NOTIFICATION_TYPES } from '../../../constants/COMMON';
import { Flexbox, Label } from '../../../components';
import avatar from '../../../assets/images/avt2.png';
import { generateModalActions } from '../../../helpers/modalAction';
import Image from '../../../components/Image/Image';
import { formatImageUrl } from '../../../helpers/common';
import { ROUTES } from '../../../constants/ROUTES';

const { toggle } = generateModalActions(REDUCERS.NOTI_POPUP);

const NotificationPopup = ({ visible, notifications, fetching, toggleModal }) => {
  return (
    <div
      className={classNames('notification-popup', {
        visible,
      })}
    >
      <div className="header">
        Thông báo
        <div className="close" onClick={toggleModal}>
          <FontAwesomeIcon icon={faTimesCircle} />
        </div>
      </div>
      <div className="body">
        {fetching ? (
          <div className="loading">loading</div>
        ) : notifications.length === 0 ? (
          <div className="placeholder">
            <FontAwesomeIcon
              icon={faComment}
              color="lightgray"
              size="3x"
              style={{ marginBottom: 10 }}
            />
            <div>Chưa có thông báo</div>
          </div>
        ) : (
          notifications.map(noti => (
            <Link
              onClick={toggleModal}
              key={noti.id}
              to={`${ROUTES.HOUSE_ROOT}/${noti && noti.house ? noti.house.id : ''}`}
            >
              <div className={classNames('item', { read: noti.status })}>
                <Flexbox row spaceBetween alignItems="flex-start">
                  <Flexbox row>
                    <div>
                      <Image
                        alt="useravt"
                        src={
                          noti.user && noti.user.avatar
                            ? formatImageUrl(noti.user.avatar.thumbnail)
                            : avatar
                        }
                        fallbackSrc={avatar}
                        className="avatar"
                      />
                    </div>
                    <p>
                      <span className="user">{noti.user ? noti.user.name : ''}</span>
                      {NOTIFICATION_TYPES[noti.type]}
                    </p>
                  </Flexbox>
                  <div className="time">{moment(noti.created_at * 1000).fromNow()}</div>
                </Flexbox>
                {noti.status === 0 && (
                  <div className="dot">
                    <Label dot background="#ff6c00" />
                  </div>
                )}
              </div>
            </Link>
          ))
        )}
      </div>
    </div>
  );
};

NotificationPopup.propTypes = {
  notifications: PropTypes.array,
};

NotificationPopup.defaultProps = {
  notifications: [],
};

export function mapStateToProps(state) {
  return {
    visible: state.modal[REDUCERS.NOTI_POPUP].visible,
    notifications: state.notification.list,
    fetching: state.notification.fetching,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({ toggleModal: toggle }, dispatch);
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NotificationPopup));
