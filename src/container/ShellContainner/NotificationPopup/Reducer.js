import createModalReducer from '../../../helpers/createModalReducer';
import { REDUCERS } from '../../../constants/COMMON';

export default createModalReducer(REDUCERS.NOTI_POPUP);
