import { GLOBAL_ACTION_TYPES } from './GlobalSearchReducer';
import { ROUTES } from '../../constants/ROUTES';

const updateSearch = searchValue => ({
  type: GLOBAL_ACTION_TYPES.UPDATE_SEARCH,
  searchValue,
});

export const searchGlobal = value => async (dispatch, getState) => {
  const { location } = getState().router;
  dispatch(updateSearch(value));
  if (location.pathname === ROUTES.HOUSES) {
    // dispatch(searchHouses(value));
  }
};
