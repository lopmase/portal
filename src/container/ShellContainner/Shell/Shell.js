/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Flexbox from '../../../components/Flexbox/Flexbox';
import Drawer from '../../../components/Drawer/Drawer';
import './Shell.scss';
import NotificationPopup from '../NotificationPopup/NotificationPopup';
import { Reponsive } from '../../../components';
import GlobalLoading from '../GlobalLoading/GlobalLoading';
import Footer from '../Footer/Footer';

const Shell = ({ children }) => {
  const [compact, setCompact] = useState(false);
  return (
    <>
      <Reponsive.Default>
        <div className="shell">
          <Flexbox row containerStyle={{ alignItems: 'stretch' }}>
            <Drawer compact={compact} onModeChanged={() => setCompact(!compact)} />
            <div
              style={{
                width: compact ? 'calc(100vw - 70px)' : 'calc(100vw - 250px)',
                transition: 'width 300ms',
              }}
            >
              <Flexbox
                alignItems="flex-start"
                row
                containerStyle={{
                  flex: 1,
                  height: 'calc(100vh)',
                  overflowY: 'scroll',
                  overflowX: 'hidden',
                  position: 'relative',
                }}
              >
                <GlobalLoading />
                <div className="main-content">{children}</div>
              </Flexbox>
            </div>
          </Flexbox>
        </div>
      </Reponsive.Default>
      <Reponsive.Mobile>
        <div style={{ paddingBottom: 60, overflow: 'hidden', background: '#fdf7f2' }}>
          {children}
        </div>
        <Footer />
      </Reponsive.Mobile>
      <NotificationPopup />
    </>
  );
};

Shell.propTypes = {
  children: PropTypes.object.isRequired,
};

Shell.defaultProps = {};

export default Shell;
