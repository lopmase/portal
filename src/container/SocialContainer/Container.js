import React, { Component } from 'react';
import { PrivateRoute } from '../../components';
import { ROUTES } from '../../constants/ROUTES';
import ZaloReceiveToken from './Zalo/ZaloReceiveToken/ZaloReceiveToken';

class SocialContainer extends Component {
  state = {};

  render() {
    return (
      <div className="social-container">
        <PrivateRoute path={ROUTES.ZALO_TOKEN} component={ZaloReceiveToken} />
      </div>
    );
  }
}

export default SocialContainer;
