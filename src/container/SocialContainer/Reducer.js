import { combineReducers } from 'redux';
import reducer from './Zalo/reducer';

export default combineReducers({
  zalo: reducer,
});
