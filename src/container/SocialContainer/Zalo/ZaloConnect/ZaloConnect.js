import React from 'react';
import PropTypes from 'prop-types';
import { useUserProfile } from '../../../../hooks/userProfileHook';
import { Button, Flexbox } from '../../../../components';
import './ZaloConnect.scss';
import zaloIcon from '../../../../assets/images/zalo.jpg';
import { getAuthUrl } from '../../../../apis/zaloApi';
import { ROUTES } from '../../../../constants/ROUTES';

const ZaloConnect = ({ children }) => {
  const user = useUserProfile();

  const connectZalo = () => {
    window.open(getAuthUrl(ROUTES.HOUSES), '_self');
  };

  return (
    <div>
      {user.zalo_access_token ? (
        <div>{children}</div>
      ) : (
        <div className="zalo-connect">
          <Flexbox containerStyle={{ flexDirection: 'column', alignItems: 'center' }}>
            <img src={zaloIcon} width="80px" alt="zalo icon" />
            <div className="message">Tài khoản Zalo của bạn chưa được kết nối.</div>
            <Button text="Kết nối ngay" onClick={connectZalo} />
          </Flexbox>
        </div>
      )}
    </div>
  );
};

ZaloConnect.propTypes = {
  children: PropTypes.any.isRequired,
};

export default ZaloConnect;
