import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { zaloSelector } from '../zaloSelector';
import ZaloConnect from '../ZaloConnect/ZaloConnect';
import DataList from '../../../Common/DataList/DataList';
import * as actions from './action';
import { Flexbox, Checkbox } from '../../../../components';

const ZaloFriends = ({ loadZaloFriend, loadMore, onSelect, selection }) => {
  useEffect(() => {
    loadZaloFriend();
  }, [loadZaloFriend]);

  return (
    <ZaloConnect>
      <DataList
        selector={zaloSelector.friends}
        height={400}
        search
        onBottomScroll={loadMore}
        renderItems={item => (
          <Flexbox row flexStart containerStyle={{ marginBottom: 5 }}>
            <Checkbox
              checked={selection.indexOf(item.id) >= 0}
              onChange={checked => onSelect(item, !checked)}
            />
            <img
              style={{ width: 28, height: 28, borderRadius: 14, marginRight: 10 }}
              src={item.picture.data.url}
              alt="zalo friend avat"
            />
            <div style={{ fontSize: 13, fontWeight: 'bold' }}>{item.name}</div>
          </Flexbox>
        )}
      />
    </ZaloConnect>
  );
};

ZaloFriends.propTypes = {
  loadZaloFriend: PropTypes.func.isRequired,
  loadMore: PropTypes.func.isRequired,
  selection: PropTypes.array.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export function mapStateToProps(state) {
  return {
    friends: zaloSelector.friends(state),
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({ ...actions }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(ZaloFriends);
