import _ from 'lodash';
import { apiSerializer } from '../../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../../constants/MESSAGE';
import { REDUCERS } from '../../../../constants/COMMON';
import { generateDatalistActions } from '../../../../helpers/dataListAction';

const { fetchDone, fetching, fetchError } = generateDatalistActions(REDUCERS.ZALO_FRIENDS);

export const loadZaloFriend = () => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(fetching());
    const response = await thunkDependencies.zalo.getFriendList();
    const result = apiSerializer(response);
    const list = result.data ? _.sortBy(result.data, 'name') : [];
    dispatch(fetchDone(list));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(fetchError(message));
  }
};
