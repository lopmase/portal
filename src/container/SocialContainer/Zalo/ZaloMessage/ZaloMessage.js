import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ZaloConnect from '../ZaloConnect/ZaloConnect';
import { Input } from '../../../../components';

const ZaloMessage = ({ friends }) => {
  return (
    <ZaloConnect>
      <div>{`Gửi tin nhắn tới ${friends.length} người`}</div>
      <Input multiline placeholder="Điền nội dung thêm" />
    </ZaloConnect>
  );
};

ZaloMessage.propTypes = {
  friends: PropTypes.array,
};

ZaloMessage.defaultProps = {
  friends: [],
};

export function mapStateToProps() {
  return {};
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(ZaloMessage);
