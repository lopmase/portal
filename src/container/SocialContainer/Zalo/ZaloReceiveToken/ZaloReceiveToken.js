import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import { bindActionCreators } from 'redux';
import { Button, Flexbox, Modal } from '../../../../components';
import { parseQuery } from '../../../../helpers/common';
import * as actions from './action';
import { zaloSelector } from '../zaloSelector';
import { generateModalActions } from '../../../../helpers/modalAction';
import { REDUCERS } from '../../../../constants/COMMON';

const { closeModal } = generateModalActions(REDUCERS.ZALO_TOKEN);

const ZaloReceiveToken = ({ visible, updateToken, closeSuccessModal }) => {
  useEffect(() => {
    const { search } = window.location;
    const query = parseQuery(search);
    if (query && query.code) {
      updateToken(query.code);
    }
  }, [updateToken]);

  return (
    <div>
      <Modal
        containerStyle={{ maxWidth: 400 }}
        visible={visible}
        renderModal={() => (
          <div style={{ padding: 20 }}>
            <h3 style={{ marginTop: 0 }}>Rất tốt</h3>
            <p>
              Tài khoản đã được kết nối tới Zalo. Giờ bạn có thể chia sẻ nội dung tới bạn bè Zalo.
            </p>
            <Flexbox row spaceBetween>
              <div />
              <Button text="OK" onClick={closeSuccessModal} />
            </Flexbox>
          </div>
        )}
      />
    </div>
  );
};

ZaloReceiveToken.propTypes = {
  visible: PropTypes.bool.isRequired,
  updateToken: PropTypes.func.isRequired,
  closeSuccessModal: PropTypes.func.isRequired,
};

export function mapStateToProps(state) {
  return {
    visible: zaloSelector.modal(state).visible,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      closeSuccessModal: closeModal,
      pushRoute: push,
    },
    dispatch,
  );
}
export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(ZaloReceiveToken);
