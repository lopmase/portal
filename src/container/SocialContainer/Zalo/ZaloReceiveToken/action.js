import { push } from 'connected-react-router';
import { apiSerializer } from '../../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../../constants/MESSAGE';
import { REDUCERS } from '../../../../constants/COMMON';
import { generateModalActions } from '../../../../helpers/modalAction';
import { showMessage } from '../../../Common/GlobalErrorModal/action';
import { ROUTES } from '../../../../constants/ROUTES';

const { closeModal, openModal } = generateModalActions(REDUCERS.GLOBAL_LOADING);
const { openModal: openZaloTokenModal } = generateModalActions(REDUCERS.ZALO_TOKEN);

export const updateToken = code => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(openModal());
    const response = await thunkDependencies.zalo.updateZaloToken(code);
    apiSerializer(response);
    dispatch(closeModal());
    dispatch(push(ROUTES.HOUSES));
    dispatch(openZaloTokenModal());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};
