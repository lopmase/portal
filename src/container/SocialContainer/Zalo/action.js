import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateModalActions } from '../../../helpers/modalAction';
import { generateFormActions } from '../../../helpers/formAction';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { uploadImages } from '../../HouseContainer/UploadJobs/action';

const { closeModal } = generateModalActions(REDUCERS.NEW_CUSTOMER_MODAL);
const { handleFieldChanged, submitDone, submitError, submitting } = generateFormActions(
  REDUCERS.NEW_CUSTOMER,
);

export const handleChange = (name, value) => async dispatch => {
  dispatch(handleFieldChanged(name, value));
};

export const create = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { data } = getState().customer[REDUCERS.NEW_CUSTOMER].data;
    let imageId = null;
    dispatch(submitting());
    if (data.image) {
      await dispatch(uploadImages([data.image]));
      const uploadJobs = getState().house[REDUCERS.UPLOAD_JOBS].jobs;
      imageId = uploadJobs.results[0] ? uploadJobs.results[0].id : null;
    }
    const form = { ...data, image: imageId || undefined };
    const response = await thunkDependencies.customer.new(form);
    const result = apiSerializer(response);
    dispatch(submitDone(result));
    dispatch(closeModal());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
    dispatch(submitError(message));
  }
};
