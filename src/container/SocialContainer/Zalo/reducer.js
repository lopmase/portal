import { combineReducers } from 'redux';
import createDataListReducer from '../../../helpers/createDataListReducer';
import createDataReducer from '../../../helpers/createDataReducer';
import createModalReducer from '../../../helpers/createModalReducer';
import { REDUCERS } from '../../../constants/COMMON';

export default combineReducers({
  profile: createDataReducer(REDUCERS.ZALO_PROFILE),
  friends: createDataListReducer(REDUCERS.ZALO_FRIENDS),
  zaloTokenModal: createModalReducer(REDUCERS.ZALO_TOKEN),
});
