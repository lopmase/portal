export const zaloSelector = {
  profile: state => state.social.zalo.profile,
  friends: state => state.social.zalo.friends,
  modal: state => state.social.zalo.zaloTokenModal,
};
