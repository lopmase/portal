import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { PrivateRoute } from '../../components';
import { ROUTES } from '../../constants/ROUTES';
import Users from './Users';

class UserContainer extends Component {
  state = {};

  render() {
    return (
      <div className="user-container">
        <PrivateRoute path={ROUTES.USERS} component={Users} />
        <Redirect to={ROUTES.USERS} />
      </div>
    );
  }
}

export default UserContainer;
