/* eslint-disable no-empty */
import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Card, FormDropdown } from '../../../components';
import { REDUCERS } from '../../../constants/COMMON';
import { fetch } from '../Users/action';
import { showMessage } from '../../Common/GlobalErrorModal/action';

const OtherInfo = ({ form, handleChange, users, loadingUser, user }) => {
  return (
    <Card shadow>
      <div style={{ flex: 1 }}>
        <FormDropdown
          name="user_id"
          label="Nhân viên"
          form={form}
          search
          handleChange={handleChange}
          items={users.map(u => ({ text: u.name, value: u.id, meta: u.phone_number }))}
        />
      </div>
    </Card>
  );
};

OtherInfo.propTypes = {
  form: PropTypes.object.isRequired,
  handleChange: PropTypes.func.isRequired,
  users: PropTypes.array.isRequired,
  user: PropTypes.any.isRequired,
  loadingUser: PropTypes.bool.isRequired,
};

OtherInfo.defaultProps = {};

export function mapStateToProps(state) {
  return {
    users: state.user[REDUCERS.USERS].list,
    user: state.auth.user.data,
    loadingUser: state.auth.user.loading,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      showError: showMessage,
    },
    dispatch,
  );
}

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(OtherInfo);
