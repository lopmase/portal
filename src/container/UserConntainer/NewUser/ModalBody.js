/* eslint-disable react/prop-types */
import React from 'react';

import ExtraFields from '../SharedComponents/ExtraFields';
import RequiredFields from '../SharedComponents/RequiredFields';

const BaseBodyFormInput = ({ form, handleChange, users }) => {
  return (
    <>
      <RequiredFields form={form} handleChange={handleChange} />
      <ExtraFields form={form} users={users} handleChange={handleChange} />
    </>
  );
};

export default BaseBodyFormInput;
