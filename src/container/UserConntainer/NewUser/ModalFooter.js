import React from 'react';
import Button from '../../../components/Button/Button';
import styles from '../../../styles/_Colors.scss';

const styleProps = {
  text: 'Huỷ',
  backgroundColor: styles.transparent,
  textColor: styles.black,
  border: true,
  clear: true,
  containerStyle: {
    marginRight: 10,
  },
};

// eslint-disable-next-line react/prop-types
const Footer = ({ closeModal, onSubmit, isValid, editMode }) => (
  <div>
    <Button {...styleProps} onClick={() => closeModal()} />
    <Button
      text={!editMode ? 'Tạo mới' : 'Cập nhật'}
      disabled={!isValid}
      onClick={() => {
        onSubmit();
      }}
    />
  </div>
);

export default Footer;
