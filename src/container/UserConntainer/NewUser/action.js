import { toast } from 'react-toastify';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { generateModalActions } from '../../../helpers/modalAction';
import { generateFormActions } from '../../../helpers/formAction';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { uploadImages } from '../../HouseContainer/UploadJobs/action';
import { fetch } from '../Users/action';

const { closeModal } = generateModalActions(REDUCERS.NEW_USER_MODAL);
const { openModal: openGlobalLoading, closeModal: closeGlobalLoading } = generateModalActions(
  REDUCERS.GLOBAL_LOADING,
);
const {
  handleFieldChanged,
  submitDone,
  submitError,
  submitting,
  validateAll,
} = generateFormActions(REDUCERS.NEW_USER);

export const handleChange = (name, value) => async dispatch => {
  dispatch(handleFieldChanged(name, value));
};

const userFields = [
  'phone_number',
  'name',
  'email',
  'password',
  'date_of_birth',
  'profile_picture',
  'social_id',
  'provided_date',
  'address',
  'role',
  'id',
  'achievement',
  'introduction',
  'introduction_status',
  'position',
  'manager',
  'job_position',
  'maximum_price',
  'minimum_price',
  'project',
  'province',
  'create_product_permission',
  'update_product_permission',
  'chat_permission',
  'alonhadat_name',
  'alonhadat_password',
];

export const fetchUser = id => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(openGlobalLoading());
    const response = await thunkDependencies.user.detail(id);
    const result = apiSerializer(response);
    userFields.forEach(field => {
      if (field === 'id') {
        dispatch(handleChange('user_id', result[field]));
      } else {
        dispatch(handleChange(field, result[field]));
      }
    });
    // dispatch(handleChange('password', '********'));
    dispatch(closeGlobalLoading(result));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeGlobalLoading());
    dispatch(showMessage(message));
  }
};

export const create = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { data } = getState().user[REDUCERS.NEW_USER].data;
    let imageId = null;
    dispatch(submitting());
    if (data.image) {
      await dispatch(uploadImages([data.image]));
      const uploadJobs = getState().house[REDUCERS.UPLOAD_JOBS].jobs;
      imageId = uploadJobs.results[0] ? uploadJobs.results[0].id : null;
    }
    const form = { ...data, profile_picture: imageId || undefined };

    const response = await thunkDependencies.user.newUser(form);
    const result = apiSerializer(response);
    dispatch(submitDone(result));
    dispatch(fetch());
    dispatch(closeModal());
    toast.success('Tạo người dùng mới thành công!');
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
    dispatch(submitError(message));
  }
};

export const update = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { data } = getState().user[REDUCERS.NEW_USER].data;
    let imageId = null;
    dispatch(submitting());
    if (data.image) {
      await dispatch(uploadImages([data.image]));
      const uploadJobs = getState().house[REDUCERS.UPLOAD_JOBS].jobs;
      imageId = uploadJobs.results[0] ? uploadJobs.results[0].id : null;
    }
    const form = { ...data, profile_picture: imageId || undefined };

    delete form.password;
    const response = await thunkDependencies.user.updateUser(form);
    const result = apiSerializer(response);
    dispatch(submitDone(result));
    dispatch(fetch());
    dispatch(closeModal());
    toast.success('Cập nhật người dùng thành công!');
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
    dispatch(submitError(message));
  }
};

export const submit = editMode => async (dispatch, getState) => {
  dispatch(validateAll());
  const { error } = getState().user[REDUCERS.NEW_USER].data;

  const isValid = Object.values(error).every(msg => msg.length === 0);
  if (isValid) {
    if (editMode) {
      dispatch(update());
    } else {
      dispatch(create());
    }
  }
};
