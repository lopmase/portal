/* eslint-disable react/prop-types */
import React from 'react';
import { compose, lifecycle } from 'recompose';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { Modal, Flexbox } from '../../../components';
import './NewUser.scss';
import { REDUCERS } from '../../../constants/COMMON';
import * as actions from './action';
import { generateModalActions } from '../../../helpers/modalAction';

import BaseBody from './ModalBody';
import Footer from './ModalFooter';

const { closeModal, toggle } = generateModalActions(REDUCERS.NEW_USER_MODAL);

const NewUser = props => {
  const { form, modal, closeNewUserModal, handleChange, users, submit } = props;
  const { visible } = modal;
  const { submitting, error, editMode } = form;
  const isValid = _.values(error).filter(i => i.length).length === 0;
  return (
    <>
      <Modal
        visible={visible}
        onClose={submitting ? () => {} : closeNewUserModal}
        loading={submitting}
        classModal="pd-t"
        containerStyle={{
          width: 'auto',
          maxWidth: 700,
          minHeight: 900,
        }}
        renderModal={() => (
          <div className="new-user">
            {!editMode && <h2>Tạo người dùng mới</h2>}
            {editMode && <h2>Cập nhật người dùng</h2>}

            <BaseBody users={users} form={form} handleChange={handleChange} />

            <Flexbox row spaceBetween>
              <div />
              <Flexbox row>
                <Footer
                  isValid={isValid}
                  onSubmit={() => submit(editMode)}
                  closeModal={closeNewUserModal}
                  editMode={editMode}
                />
              </Flexbox>
            </Flexbox>
          </div>
        )}
      />
    </>
  );
};

export const useLifeCycle = lifecycle({
  componentDidMount() {},
});
export function mapStateToProps(state) {
  return {
    form: state.user[REDUCERS.NEW_USER].data,
    modal: state.user[REDUCERS.NEW_USER].modal,
    visible: state.user[REDUCERS.NEW_USER].modal.visible,
  };
}
export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      ...actions,
      closeNewUserModal: closeModal,
      toggleModal: toggle,
    },
    dispatch,
  );
}

const connetToRedux = connect(mapStateToProps, mapDispatchToProps);
export default compose(connetToRedux, useLifeCycle)(NewUser);
