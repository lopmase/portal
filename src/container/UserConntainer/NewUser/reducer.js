import { combineReducers } from 'redux';
import createFormReducer from '../../../helpers/createFormReducer';
import createModalReducer from '../../../helpers/createModalReducer';
import { REDUCERS } from '../../../constants/COMMON';

const initialValue = {
  name: '',
  phone_number: '',
  password: '12345678',
  address: '',
  social_id: '',
  date_of_birth: '',
  provided_date: '',
  project: null,
  province: null,
  minimum_price: null,
  maximum_price: null,
  create_product_permission: 1,
  customer_permission: 1,
  request_permission: 1,
  project_permission: 0,
  news_permission: 0,
  employment_diagram_permission: 0,
  chat_permission: 0,
  bussiness_permission: 0,
  update_product_permission: 0,
  role: 3,
  alonhadat_name: '',
  alonhadat_password: '',
  batdongsan_name: '',
  batdongsan_password: '',
};

export default combineReducers({
  modal: createModalReducer(REDUCERS.NEW_USER_MODAL),
  data: createFormReducer(REDUCERS.NEW_USER, initialValue, {
    name: [['required', 'Tên bắt buộc']],
    phone_number: [
      ['required', 'Số điện thoại bắt buộc'],
      [value => value.match(/^[0-9]*$/g), 'Số điện thoại chỉ điền số'],
    ],
    // password: [['required', 'Mật khẩu bắt buộc']],
    date_of_birth: [['required', 'Ngày sinh bắt buộc']],
    social_id: [['required', 'CMND bắt buộc']],
    provided_date: [['required', 'Ngày cấp bắt buộc']],
    address: [['required', 'Địa chỉ bắt buộc']],
    role: [['required', 'Phân quyền bắt buộc']],
    minimum_price: [[value => (value != null ? value >= 0 : value == null), 'Phải lớn hơn 0']],
    maximum_price: [[value => (value != null ? value >= 0 : value == null), 'Phải lớn hơn 0']],
  }),
});
