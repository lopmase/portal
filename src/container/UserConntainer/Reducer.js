import { combineReducers } from 'redux';
import createDatalistReducer from '../../helpers/createDataListReducer';
import { REDUCERS } from '../../constants/COMMON';
import reducer from './NewUser/reducer';
import { UPDATE_ROLE_FILTER } from './Users/types';

const extendedReducer = (state, action) => {
  if (action.type === UPDATE_ROLE_FILTER) {
    const newState = state;
    newState.role = action.roleId;
    return newState;
  }
  return false;
};

export default combineReducers({
  [REDUCERS.USERS]: createDatalistReducer(
    REDUCERS.USERS,
    { pageSize: 100, role: 0 },
    extendedReducer,
  ),
  [REDUCERS.NEW_USER]: reducer,
});
