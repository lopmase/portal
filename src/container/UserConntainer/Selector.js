import { REDUCERS } from '../../constants/COMMON';

export const UserSelector = {
  users: state => state.user[REDUCERS.USERS],
  friends: state => state.social.zalo.friends,
  me: state => state.auth.user,
};
