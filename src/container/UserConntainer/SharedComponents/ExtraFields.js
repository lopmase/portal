/* eslint-disable camelcase */
/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
import { Collapse } from 'antd';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import _ from 'lodash';
import { Editor } from 'react-draft-wysiwyg';
import { convertToRaw, EditorState, convertFromRaw, ContentState } from 'draft-js';
import { getDictionary } from '../../../apis/metadataApi';
import { list as projectList } from '../../../apis/projectApi';
import { list } from '../../../apis/userApi';

import {
  Flexbox,
  FormDropdown,
  FormField,
  FormInput,
  FormLabel,
  Spacer,
  FormRadioGroup,
} from '../../../components';
import FormSelect from '../../../components/Form/FormSelect';
import { JOB_POSITION_SELECTION, ROLE_NAME_SELECTIONS } from '../constants';

const { Panel } = Collapse;

const addressFieldProps = {
  label: 'Địa chỉ',
  name: 'address',
};
const positionFieldProps = {
  label: 'Chức vụ',
  name: 'position',
  items: [
    { key: 'sales_manager', value: 'Sales Manager', text: 'Sales Manager' },
    { key: 'managing_director', value: 'Managing Director', text: 'Managing Director' },
    { key: 'sales_director', value: 'Sales Director', text: 'Sales Director' },
    { key: 'sales_executive', value: 'Sales Executive', text: 'Sales Executive' },
  ],
  containerStyle: {},
};
const achievementFieldProps = {
  label: 'Thành tích cao nhất',
  name: 'achievement',
};

const introductionStatusFieldProps = {
  label: 'Hiển thị',
  name: 'introduction_status',
  items: [
    { key: 1, value: 1, text: 'Hiển thị' },
    { key: 0, value: 0, text: 'Ẩn' },
  ],
};

const idFieldProps = {
  label: 'CMND',
  name: 'social_id',
  // numberOnly: true,
  thousandSeparator: '',
};
const customerCategoryFieldProps = {
  label: 'Phân quyền',
  name: 'role',
  items: ROLE_NAME_SELECTIONS,
  containerStyle: {},
};
const jobPositionFieldProps = {
  label: 'Vị trí',
  name: 'job_position',
  items: JOB_POSITION_SELECTION,
  containerStyle: {},
};
const maximumPriceFieldProps = {
  label: 'Giá cao nhất',
  name: 'maximum_price',
  type: 'number',
};
const minimumPriceFieldProps = {
  label: 'Giá thấp nhất',
  name: 'minimum_price',
  type: 'number',
};
const managerFieldProps = {
  label: 'Manager',
  name: 'manager',
};
const projectFieldProps = {
  label: 'Dự án',
  name: 'project',
};
const provineFieldProps = {
  label: 'Khu vực',
  name: 'province',
};
const styles = {
  container: {
    marginBottom: 15,
  },
};

const ExtraFields = ({ form, handleChange, users }) => {
  const [init, setInit] = useState(false);
  const [userLst, setUserLst] = useState([]);
  const [user, setUser] = useState([]);
  const [project, setProject] = useState([]);
  const [province, setProvince] = useState([]);
  const [editorState, setEditorState] = useState(EditorState.createEmpty());
  const { data } = form;
  const onChange = editor => {
    const rawContent = convertToRaw(editor.getCurrentContent());
    setEditorState(editor);
    handleChange('introduction', JSON.stringify(rawContent));
  };
  const fetchData = async () => {
    const [lstUser, lstProject, lstProvince] = await Promise.all([
      list({ role: null, page: 1, size: 9000 }),
      projectList({ page: 1, size: 99999 }),
      getDictionary('province'),
    ]);
    lstProject.data.data.map(value => {
      value.value = value.id;
      value.text = value.name;
    });
    lstProvince.data.map(value => {
      value.text = value.value;
    });
    setProvince(lstProvince.data);
    setUserLst(lstUser.data);
    setProject(lstProject.data.data);
  };
  useEffect(() => {
    fetchData();
  }, []);
  useEffect(() => {
    const newUserLst = _.pickBy(
      userLst,
      ({ job_position, id }) =>
        data.job_position !== 1 && job_position === data.job_position - 1 && id !== data.user_id,
    );
    const userArrLength = userLst.length;
    data.job_position === 7 && (newUserLst[userArrLength] = { id: null, name: '---Không chọn---' });
    data.job_position === 1 && (data.manager = null);
    userLst.length > 0 && setUser(new Array(Object.values(newUserLst))[0]);
  }, [data.job_position]);
  useEffect(() => {
    if (data.introduction !== undefined && !init) {
      setInit(true);
      let editor;
      try {
        if (!data.introduction) {
          editor = EditorState.createEmpty();
          return;
        }
        const jsonContent = JSON.parse(data.introduction);
        editor = EditorState.createWithContent(convertFromRaw(jsonContent));
      } catch (error) {
        editor = EditorState.createWithContent(ContentState.createFromText(data.introduction));
      } finally {
        setEditorState(editor);
      }
    }
  }, [data.introduction]);
  return (
    <div style={styles.container}>
      <Flexbox row alignItems="flex-start">
        <FormField
          {...{ form, handleChange, name: 'date_of_birth' }}
          render={() => (
            <div className="input-container" style={{ flex: 1 }}>
              <FormLabel text="Ngày sinh" />
              <DayPickerInput
                inputProps={{ style: { width: '100%' } }}
                value={data.date_of_birth}
                dayPickerProps={{ disabledDays: { after: new Date() } }}
                onDayChange={day =>
                  moment(day, 'YYYY-MM-DD', true).isValid()
                    ? handleChange('date_of_birth', moment(day).format('YYYY-MM-DD'))
                    : null
                }
              />
            </div>
          )}
        />
        <Spacer />
        <FormInput
          style={{ width: '60%' }}
          form={form}
          handleChange={handleChange}
          {...idFieldProps}
        />
        <Spacer />
        <FormField
          {...{ form, handleChange, name: 'provided_date' }}
          render={() => (
            <div className="input-container" style={{ flex: 1 }}>
              <FormLabel text="Ngày cấp" />
              <DayPickerInput
                inputProps={{ style: { width: '100%' } }}
                value={data.provided_date}
                dayPickerProps={{ disabledDays: { after: new Date() } }}
                onDayChange={day =>
                  moment(day, 'YYYY-MM-DD', true).isValid()
                    ? handleChange('provided_date', moment(day).format('YYYY-MM-DD'))
                    : null
                }
              />
            </div>
          )}
        />
      </Flexbox>
      <Flexbox row>
        <FormInput form={form} handleChange={handleChange} {...addressFieldProps} />
        <Spacer />
        <FormDropdown form={form} handleChange={handleChange} {...customerCategoryFieldProps} />
        <Spacer />
        <FormDropdown form={form} handleChange={handleChange} {...jobPositionFieldProps} />
      </Flexbox>
      <Flexbox row>
        <FormDropdown
          form={form}
          search
          handleChange={handleChange}
          items={
            ({
              text: '--Không chọn--',
              value: 0,
              mata: null,
            },
            user.map(u => ({ text: u.name, value: u.id, meta: u.phone_number })))
          }
          {...managerFieldProps}
        />
        <Spacer />
        <FormDropdown form={form} handleChange={handleChange} {...positionFieldProps} />
        <Spacer />
        <FormDropdown form={form} handleChange={handleChange} {...introductionStatusFieldProps} />
      </Flexbox>
      <Spacer />
      <Collapse>
        <Panel header="Giới hạn quyền thao tác bất động sản">
          {data.role !== 1 && data.role !== 2 && (
            <FormRadioGroup
              name="create_product_permission"
              inputProps={{ min: 0 }}
              label="Có quyền tạo mới Bất động sản không?"
              {...{ form, handleChange }}
              options={[
                { value: 1, text: 'Có' },
                { text: 'Không', value: 0 },
              ]}
            />
          )}
          {data.role !== 1 && data.role !== 2 && (
            <FormRadioGroup
              name="update_product_permission"
              inputProps={{ min: 0 }}
              label="Có quyền chuyển trạng thái Bất động sản không?"
              {...{ form, handleChange }}
              options={[
                { value: 1, text: 'Có' },
                { text: 'Không', value: 0 },
              ]}
            />
          )}
          {project.length > 0 && (
            <FormSelect
              form={form}
              handleChange={handleChange}
              multiple
              items={project}
              require={false}
              {...projectFieldProps}
            />
          )}
          <Spacer />
          <label>Giới hạn giá Bất động sản</label>
          <Spacer />
          <Flexbox row>
            <FormInput
              numberOnly
              form={form}
              handleChange={handleChange}
              {...minimumPriceFieldProps}
            />
            <Spacer />
            <FormInput
              numberOnly
              form={form}
              handleChange={handleChange}
              {...maximumPriceFieldProps}
            />
          </Flexbox>
          <Spacer />
          {province.length > 0 && (
            <FormSelect
              form={form}
              handleChange={handleChange}
              multiple
              items={province}
              require={false}
              {...provineFieldProps}
            />
          )}
        </Panel>
      </Collapse>
      <Spacer />
      <Collapse>
        <Panel header="Giới hạn danh mục">
          {data.role !== 1 && data.role !== 2 && (
            <>
              <FormRadioGroup
                name="customer_permission"
                inputProps={{ min: 0 }}
                label="Khách hàng?"
                {...{ form, handleChange }}
                options={[
                  { value: 1, text: 'Có' },
                  { text: 'Không', value: 0 },
                ]}
              />
              <Spacer />
              <FormRadioGroup
                name="request_permission"
                inputProps={{ min: 0 }}
                label="Nhu cầu?"
                {...{ form, handleChange }}
                options={[
                  { value: 1, text: 'Có' },
                  { text: 'Không', value: 0 },
                ]}
              />
              <Spacer />
              <FormRadioGroup
                name="project_permission"
                inputProps={{ min: 0 }}
                label="Dự án?"
                {...{ form, handleChange }}
                options={[
                  { value: 1, text: 'Có' },
                  { text: 'Không', value: 0 },
                ]}
              />
              <Spacer />
              <FormRadioGroup
                name="news_permission"
                inputProps={{ min: 0 }}
                label="Tin tức?"
                {...{ form, handleChange }}
                options={[
                  { value: 1, text: 'Có' },
                  { text: 'Không', value: 0 },
                ]}
              />
              <Spacer />
              <FormRadioGroup
                name="employment_diagram_permission"
                inputProps={{ min: 0 }}
                label="Sơ đồ nhân sự?"
                {...{ form, handleChange }}
                options={[
                  { value: 1, text: 'Có' },
                  { text: 'Không', value: 0 },
                ]}
              />
              <Spacer />
              <FormRadioGroup
                name="chat_permission"
                inputProps={{ min: 0 }}
                label="Chat?"
                {...{ form, handleChange }}
                options={[
                  { value: 1, text: 'Có' },
                  { text: 'Không', value: 0 },
                ]}
              />
              <Spacer />
              <FormRadioGroup
                name="bussiness_permission"
                inputProps={{ min: 0 }}
                label="Tổng quan?"
                {...{ form, handleChange }}
                options={[
                  { value: 1, text: 'Có' },
                  { text: 'Không', value: 0 },
                ]}
              />
            </>
          )}
        </Panel>
      </Collapse>
      <Spacer />

      <Flexbox row>
        <FormInput form={form} handleChange={handleChange} {...achievementFieldProps} />
      </Flexbox>
      <FormField
        {...{ form, handleChange, name: 'introduction' }}
        render={() => (
          <>
            <Editor
              wrapperStyle={{
                borderWidth: 1,
                borderRadius: 5,
                borderColor: 'rgba(34,36,38,.15)',
                borderStyle: 'solid',
                backgroundColor: 'white',
                minHeight: '100%',
              }}
              editorState={editorState}
              editorStyle={{ padding: 10 }}
              handlePastedText={() => false}
              onEditorStateChange={onChange}
            />
          </>
        )}
      />
    </div>
  );
};

export default ExtraFields;
