/* eslint-disable react/prop-types */
import React from 'react';
import { FormInput, Spacer, Flexbox, Avatar } from '../../../components';

const nameFieldProps = {
  label: 'Tên',
  name: 'name',
};
const phoneNumberFieldProps = {
  label: 'SĐT',
  name: 'phone_number',
};

const passwordProps = {
  label: 'Mật khẩu',
  name: 'password',
  inputProps: {
    width: 200,
  },
};
const emailFieldProps = {
  label: 'Email',
  name: 'email',
  inputProps: {
    width: 200,
  },
};

const RequiredFields = ({ form, handleChange }) => {
  const { image } = form.data;
  const { editMode } = form;

  return (
    <Flexbox row containerStyle={{ flex: 1 }}>
      <Avatar
        containerStyle={{ marginTop: 10, width: 170, paddingTop: 20 }}
        onImageAdded={newImage => handleChange('image', newImage)}
        image={image}
        onImageRemoved={() => handleChange('image', null)}
      />
      <div style={{ flex: 1 }}>
        <Flexbox row alignItems="flex-start">
          <FormInput form={form} handleChange={handleChange} {...nameFieldProps} />
          <Spacer />
          <FormInput form={form} handleChange={handleChange} {...phoneNumberFieldProps} />
        </Flexbox>
        <Flexbox row alignItems="flex-start">
          <FormInput form={form} handleChange={handleChange} {...emailFieldProps} />
          <Spacer />
          <FormInput
            form={form}
            handleChange={handleChange}
            {...passwordProps}
            editable={!editMode}
          />
        </Flexbox>
      </div>
    </Flexbox>
  );
};

export default RequiredFields;
