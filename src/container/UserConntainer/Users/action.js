import { toast } from 'react-toastify';
import { apiSerializer } from '../../../helpers/apiSerializer';
import { UNKNOWN_ERROR } from '../../../constants/MESSAGE';
import { REDUCERS } from '../../../constants/COMMON';
import { showMessage } from '../../Common/GlobalErrorModal/action';
import { generateModalActions } from '../../../helpers/modalAction';
import { generateDatalistActions } from '../../../helpers/dataListAction';
import { generateFormActions } from '../../../helpers/formAction';
import { UPDATE_ROLE_FILTER } from './types';
import { exportExcel } from '../../Common/ExportExcel/action';

const {
  fetchDone,
  fetching,
  fetchError,
  nextPage,
  previousPage,
  updatePageSize,
} = generateDatalistActions(REDUCERS.USERS);

const { openModal, closeModal } = generateModalActions(REDUCERS.GLOBAL_LOADING);
const { openModal: openNewUserAction } = generateModalActions(REDUCERS.NEW_USER_MODAL);
const { reset, setEditMode } = generateFormActions(REDUCERS.NEW_USER);

export const filterByRole = roleId => ({
  type: UPDATE_ROLE_FILTER,
  reducerId: REDUCERS.USERS,
  roleId,
});

export const fetch = () => async (dispatch, getState, thunkDependencies) => {
  try {
    const { page, pageSize, role } = getState().user[REDUCERS.USERS];
    // const { role } = getState().user[REDUCERS.USERS_FILTER];
    dispatch(fetching());
    const response = await thunkDependencies.user.list({
      page,
      size: pageSize,
      role,
    });
    const result = apiSerializer(response);
    dispatch(fetchDone(result));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;

    dispatch(fetchError(message));
  }
};

export const goNextPage = () => async dispatch => {
  dispatch(nextPage());
  dispatch(fetch());
};

export const goPrevPage = () => async dispatch => {
  dispatch(previousPage());
  dispatch(fetch());
};

export const changePageSize = size => async dispatch => {
  dispatch(updatePageSize(size));
  dispatch(fetch());
};

export const deleteUser = id => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(openModal());
    await thunkDependencies.user.removeUser(id);
    dispatch(closeModal());
    toast.success('Xoá user thành công');
    dispatch(fetch());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};

export const resetPassword = id => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(openModal());
    await thunkDependencies.user.resetPassword(id);
    dispatch(closeModal());
    toast.success('Đặt lại mật khẩu thành công');
    dispatch(fetch());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};

export const updateUserStatus = (id, status) => async (dispatch, getState, thunkDependencies) => {
  try {
    dispatch(openModal());
    await thunkDependencies.user.updateUser({ user_id: id, status });
    dispatch(closeModal());
    toast.success('Cập nhật user thành công');
    dispatch(fetch());
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(closeModal());
    dispatch(showMessage(message));
  }
};

export const openNewUserModal = editMode => async dispatch => {
  dispatch(setEditMode(editMode || false));
  dispatch(reset());
  dispatch(openNewUserAction());
};

export const filterRole = role => async dispatch => {
  dispatch(filterByRole(role));
  dispatch(fetch());
};

export const exportAllData = () => async dispatch => {
  try {
    await dispatch(exportExcel('users'));
  } catch (error) {
    const message = error.message || UNKNOWN_ERROR;
    dispatch(showMessage(message));
  }
};
