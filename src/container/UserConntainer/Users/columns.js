export const columns = [
  {
    header: '#',
    key: 'id',
  },
  {
    header: 'NHÂN VIÊN',
    key: 'info',
  },
  {
    header: 'SĐT',
    key: 'phone_number',
  },
  {
    header: 'Email',
    key: 'email',
  },
  // {
  //   header: 'HÌNH ẢNH',
  //   key: 'avatar',
  // },
  {
    header: 'NGÀY SINH',
    key: 'date_of_birth',
  },
  {
    header: 'CMND',
    key: 'social_id',
  },
  {
    header: 'Ngày cấp CMND',
    key: 'provided_date',
  },
  //  {
  //    header: 'Manager',
  //    key: 'manager',
  //  },
  {
    header: 'Vị trí',
    key: 'job_position',
    accessor: user =>
      (user.job_position === 1 && 'Super Admin') ||
      (user.job_position === 2 && 'Admin') ||
      (user.job_position === 3 && 'Sale Admin') ||
      (user.job_position === 4 && 'Senior Manager') ||
      (user.job_position === 5 && 'Manager ') ||
      (user.job_position === 6 && 'Team Leader') ||
      (user.job_position === 7 && 'Staff'),
  },
  // {
  //   header: 'Thành Tích',
  //   key: 'achievement',
  // },
  // {
  //   header: 'Giới thiệu',
  //   key: 'introduction',
  // },
  {
    header: 'Trạng thái hiển thị',
    key: 'introduction_status',
    accessor: user => (user.introduction_status === 1 ? 'Hiển thị' : 'Ẩn'),
  },
  {
    header: 'Trạng thái',
    key: 'status',
    accessor: user => (user.status ? 'Đang hoạt động' : 'Đã khoá'),
  },
  // {
  //   header: 'VAI TRÒ',
  //   key: 'role',
  //   accessor: user => (user.role ? ROLE_NAMES[user.role] : ''),
  // },
  {
    header: 'CHỈNH SỬA',
    key: 'action',
  },
];
