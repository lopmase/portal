import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import { Dropdown, Flexbox } from '../../../../components';
import { ROLE_OPTIONS } from '../../constants';

const styleProps = {
  // clear: true,
  containerStyle: {
    minWidth: 120,
    borderRadius: 18,
    paddingTop: 8,
    paddingBottom: 8,
  },
};

const RoleNameDropDown = ({ selected, onChanged }) => {
  useEffect(() => {
    console.log('selected', selected);
  }, [selected]);
  return (
    <div className="page-size" style={{ marginRight: 10 }}>
      <Flexbox row>
        <div style={{ fontSize: 13, fontWeight: 'bold', marginRight: 10 }}>Phân Quyền:</div>
        <Dropdown
          {...styleProps}
          items={[...ROLE_OPTIONS, { value: 100, text: 'Đã khóa' }]}
          selected={selected}
          onChanged={({ item }) => onChanged(item.value)}
        />
      </Flexbox>
    </div>
  );
};

RoleNameDropDown.propTypes = {
  selected: PropTypes.any.isRequired,
  onChanged: PropTypes.func.isRequired,
};

RoleNameDropDown.defaultProps = {};

export default RoleNameDropDown;
