import React from 'react';
import PropTypes from 'prop-types';
import { isNull } from 'lodash';
import classnames from 'classnames';
import { Flexbox } from '../../../../components';
import { ROLE_NAMES } from '../../../../constants/COMMON';
import { isAdmin } from '../../../../helpers/roleUtil';
import Image from '../../../../components/Image/Image';
import avatar from '../../../../assets/images/avt2.png';
import { formatImageUrl } from '../../../../helpers/common';

const styles = {
  avatar: {
    width: 43,
    height: 43,
    objectFit: 'cover',
    borderRadius: '50%',
    backgroundColor: 'white',
  },
};

const UserInfo = ({ user }) => {
  const avaLink = !isNull(user.avatar) ? (
    <Image
      style={styles.avatar}
      fallbackSrc={avatar}
      src={formatImageUrl(user.avatar.thumbnail)}
      alt="ava"
    />
  ) : (
    <img style={styles.avatar} src={avatar} alt="ava" />
  );
  return (
    <Flexbox row flexStart containerStyle={{ alignItems: 'flex-start' }}>
      {avaLink}
      <div className="bold-text user-item" style={{ marginLeft: 5 }}>
        <div className={classnames('name', { admin: isAdmin(user) })}>{user.name}</div>
        <div className={classnames('role', { admin: isAdmin(user) })}>
          {user.role ? ROLE_NAMES[user.role] : ''}
        </div>
      </div>
    </Flexbox>
  );
};

UserInfo.propTypes = {
  user: PropTypes.object.isRequired,
};

UserInfo.defaultProps = {};

export default UserInfo;
