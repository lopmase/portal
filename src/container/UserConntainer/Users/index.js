/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { compose, lifecycle } from 'recompose';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  faTrashAlt,
  faPenAlt,
  faSyncAlt,
  faLock,
  faLockOpen,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Dropdown, Menu } from 'antd';
import { Flexbox, Table, Button, Spacer } from '../../../components';
import './Users.scss';
import { REDUCERS } from '../../../constants/COMMON';
import * as actions from './action';
import { columns } from './columns';
import RightButton from '../../../utils/CreateNewButton';
import Pagination from '../../../utils/Pagination';
import FilterByRole from './components/FilterByRole';
import { confirm } from '../../ShellContainner/ConfirmDialog/action';
import {
  DELETE_USER_CONFIRM,
  RESET_USER_CONFIRM,
  DISABLE_USER_CONFIRM,
  ENABLE_USER_CONFIRM,
} from '../../../constants/MESSAGE';
import NewUserModal from '../NewUser';
import UserInfo from './components/UserInfo';
import { fetchUser } from '../NewUser/action';
import Excel from '../../Common/ExportExcel/Excel';

const actionItems = [
  { text: 'Sửa', value: 'edit', icon: faPenAlt },
  { text: 'Xoá', value: 'del', icon: faTrashAlt },
  { text: 'Đặt lại mật khẩu', value: 'reset', icon: faSyncAlt },
];

const Dashboard = ({
  dataSource: { list, fetching, total, total_pages: totalPages, page: currentPage, role },
  goNextPage,
  goPrevPage,
  openConfirmDialog,
  openNewUserModal,
  deleteUser,
  resetPassword,
  updateUserStatus,
  fetchUserInfo,
  filterRole,
  exportAllData,
}) => {
  const onDelete = id => {
    openConfirmDialog(DELETE_USER_CONFIRM, result => {
      if (result) {
        deleteUser(id);
      }
    });
  };

  const onEdit = id => {
    fetchUserInfo(id);
    openNewUserModal(true);
  };

  const onReset = id => {
    openConfirmDialog(RESET_USER_CONFIRM, result => {
      if (result) {
        resetPassword(id);
      }
    });
  };

  const onDisable = user => {
    openConfirmDialog(user.status === 1 ? DISABLE_USER_CONFIRM : ENABLE_USER_CONFIRM, result => {
      if (result) {
        updateUserStatus(user.id, user.status === 1 ? 0 : 1);
      }
    });
  };

  const onActionClick = (key, rowData) => {
    if (key === 'del') {
      onDelete(rowData.id);
    } else if (key === 'edit') {
      onEdit(rowData.id);
    } else if (key === 'reset') {
      onReset(rowData.id);
    } else if (key === 'disable') {
      onDisable(rowData);
    }
  };

  const renderCell = ({ item, column }) => {
    if (column.key === 'info') {
      return (
        <td style={{ textAlign: 'left' }}>
          <UserInfo user={item} />
        </td>
      );
    }
    if (column.key === 'action') {
      const menu = (
        <Menu onClick={data => onActionClick(data.key, item)}>
          {[
            ...actionItems,
            {
              text: item.status === 1 ? 'Khoá' : 'Gỡ khoá',
              value: 'disable',
              icon: item.status === 1 ? faLock : faLockOpen,
            },
          ].map(menuItem => (
            <Menu.Item key={menuItem.value}>
              <FontAwesomeIcon icon={menuItem.icon} style={{ marginRight: 10 }} />
              {menuItem.text}
            </Menu.Item>
          ))}
        </Menu>
      );
      return (
        <td>
          <Dropdown overlay={menu} trigger="click">
            <Button text="Chọn" clear />
          </Dropdown>
        </td>
      );
    }

    const cell = item[column.key];
    let value = '';
    if (column.accessor) {
      value = column.accessor(item, {});
    } else {
      value = cell ? cell.toString() : '';
    }
    return <td className={column.key === 'id' ? 'bold-text' : ''}>{value}</td>;
  };
  return (
    <div className="users-page" style={{}}>
      <div className="toolbar">
        <Flexbox
          row
          spaceBetween
          containerStyle={{ marginBottom: 20, marginTop: 20, alignItems: 'center' }}
        >
          <Flexbox>
            <div className="total">{`${total} Người dùng`}</div>
          </Flexbox>
          <div>
            <Pagination
              currentPage={currentPage}
              pageNumbers={totalPages}
              onNext={goNextPage}
              onPrev={goPrevPage}
            />
          </div>
          <Flexbox>
            <FilterByRole selected={role} onChanged={filterRole} />
            <Excel hideFilterButton onExportAll={exportAllData} />
            <Spacer />
            <RightButton openModal={openNewUserModal} />
          </Flexbox>
        </Flexbox>
      </div>
      <Table loading={fetching} selectable data={list} columns={columns} renderCell={renderCell} />
      <div>
        <Flexbox row containerStyle={{ marginTop: 20, alignItems: 'center' }}>
          <Pagination
            currentPage={currentPage}
            pageNumbers={totalPages}
            onNext={goNextPage}
            onPrev={goPrevPage}
          />
        </Flexbox>
      </div>
      <NewUserModal form={null} />
    </div>
  );
};

export const mapStateToProps = state => ({
  dataSource: state.user[REDUCERS.USERS],
  state,
});

const mapDispatchToProps = {
  ...actions,
  openConfirmDialog: confirm,
  fetchUserInfo: fetchUser,
};

export const useLifeCycle = lifecycle({
  componentDidMount() {
    this.props.fetch();
  },
});

const connetToRedux = connect(mapStateToProps, mapDispatchToProps);
export default compose(connetToRedux, useLifeCycle, withRouter)(Dashboard);
