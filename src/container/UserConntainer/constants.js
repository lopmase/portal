import { ROLE_NAMES, JOB_POSITION } from '../../constants/COMMON';

export const ROLE_NAME_SELECTIONS = Object.keys(ROLE_NAMES).map(key => ({
  value: Number(key),
  text: ROLE_NAMES[key],
}));

export const JOB_POSITION_SELECTION = Object.keys(JOB_POSITION).map(key => ({
  value: Number(key),
  text: JOB_POSITION[key],
}));
export const JOB_POSITION_OPTION = [{ value: 0, text: 'Tất cả' }, ...JOB_POSITION_SELECTION];
export const ROLE_OPTIONS = [{ value: 0, text: 'Tất cả' }, ...ROLE_NAME_SELECTIONS];
