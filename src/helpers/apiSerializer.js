export const apiSerializer = data => {
  if (data.data) {
    return data.data;
  }
  if (data.message) {
    throw new Error(data.message);
  } else if (data.success) {
    return null;
  } else {
    throw new Error('Something wrong. Please try again!');
  }
};

export default {
  apiSerializer,
};
