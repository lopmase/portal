/* eslint-disable array-callback-return */
/* eslint-disable no-lonely-if */
/* eslint-disable prefer-template */
/* eslint-disable no-restricted-syntax */
/* eslint-disable prefer-const */
/* eslint-disable guard-for-in */
/* eslint-disable no-plusplus */

import draftToHtml from 'draftjs-to-html';

/* eslint-disable no-param-reassign */
// const ENDPOINT = process.env.REACT_APP_API_URL;
const ENDPOINT = 'http://localhost:8000';
const date = new Date();
// Wait for final event
export const waitForFinalEvent = (() => {
  const timers = {};
  return (callback, ms, uniqueId) => {
    let id = uniqueId;
    if (!uniqueId) {
      id = "Don't call this twice without a uniqueId";
    }
    if (timers[id]) {
      clearTimeout(timers[id]);
    }
    timers[id] = setTimeout(callback, ms);
  };
})();

export const capitalize = string => string.charAt(0).toUpperCase() + string.slice(1);

const BILLION = 1000000000;
const MILLION = 1000000;

export const formatMoney = (string, purpose) => {
  try {
    const number = Number(string);
    if (purpose) {
      if (number >= BILLION) {
        return `${Math.round((number / BILLION) * 1000) / 1000} tỷ/th`;
      }
      if (number >= MILLION) {
        return `${Math.round((number / MILLION) * 1000) / 1000} tr/th`;
      }
    } else {
      if (number >= BILLION) {
        return `${Math.round((number / BILLION) * 1000) / 1000} tỷ`;
      }
      if (number >= MILLION) {
        return `${Math.round((number / MILLION) * 1000) / 1000} tr`;
      }
    }
    return number;
  } catch (error) {
    return string;
  }
};

export const convertUnitIntoNumber = (number, unit) => {
  if (number && unit) {
    if (unit === 'mil' || unit === 'mpm') {
      return number * 1000 * 1000;
    }
    if (unit === 'bil' || unit === 'bpm') {
      return number * 1000 * 1000 * 1000;
    }
  }
  return number;
};
export const convertNumberIntoUnit = (number, purpose = 0) => {
  let moneyOnly = 0;
  let moneyUnit = '';
  if (purpose === 0) {
    if (number >= BILLION) {
      moneyOnly = Math.round((number / BILLION) * 1000) / 1000;
      moneyUnit = 'bil';
    } else {
      moneyOnly = Math.round((number / MILLION) * 1000) / 1000;
      moneyUnit = 'mil';
    }
  } else if (purpose === 1) {
    if (number >= BILLION) {
      moneyOnly = Math.round((number / BILLION) * 1000) / 1000;
      moneyUnit = 'bpm';
    } else {
      moneyOnly = Math.round((number / MILLION) * 1000) / 1000;
      moneyUnit = 'mpm';
    }
  }
  return [moneyOnly, moneyUnit];
};

export const parseQuery = search => {
  if (search.indexOf('?') === 0) {
    search = search.substr(1);
  }
  const paramObject = {};
  const params = search.split('&');
  params.forEach(query => {
    const [key, value] = query.split('=');
    paramObject[key] = value;
  });
  return paramObject;
};

export const formatImageUrl = url => {
  if (url.startsWith('https://thinhgia.s3')) {
    return url;
  }
  return ENDPOINT + url;
};

export const downloadAll = async (urls, fileNames) => {
  const jobs = await Promise.all(
    urls.map(url => {
      return fetch(url, {
        method: 'GET',
        headers: {},
      })
        .then(response => response.arrayBuffer())
        .catch(() => false);
    }),
  );

  jobs.forEach((result, index) => {
    if (!result) {
      return;
    }
    const url = window.URL.createObjectURL(new Blob([result]));

    const link = document.createElement('a');

    link.download = fileNames[index];
    link.href = url;
    link.target = '_blank';
    link.click();
  });
  return jobs;
};

export const convertStringToHTML = value => {
  if (!value) {
    return '';
  }
  try {
    const content = JSON.parse(value);
    return draftToHtml(content);
  } catch (error) {
    return '';
  }
};

export const flattenObjAdding = (obj, parent, adding, recursion, res = {}) => {
  for (const key in obj) {
    const propName = key;
    if (typeof obj[key] === 'object') {
      flattenObjAdding(obj[key], propName, adding, true, res);
    } else {
      if (recursion) {
        res[`${propName}_${adding}`] = obj[key];
      } else {
        res[propName] = obj[key];
      }
    }
  }
  return res;
};

export function flattenObj(obj, parent, res = {}) {
  for (const key in obj) {
    const propName = key;
    if (typeof obj[key] === 'object') {
      flattenObj(obj[key], propName, res);
    } else {
      res[propName] = obj[key];
    }
  }
  return res;
}

export const mappingUser = lstUser => {
  const admin = lstUser.data.filter(value => value.job_position === 2);
  const saleAdmin = lstUser.data.filter(value => value.job_position === 3);
  const seniorManager = lstUser.data.filter(value => value.job_position === 4);
  const manager = lstUser.data.filter(value => value.job_position === 5);
  const teamLeader = lstUser.data.filter(value => value.job_position === 6);
  const staffs = lstUser.data.filter(value => value.job_position === 7);
  return { admin, saleAdmin, seniorManager, manager, teamLeader, staffs };
};

export const removeDuplicateObject = arr => {
  const result = arr.reduce((unique, o) => {
    if (!unique.some(obj => obj.label === o.label && obj.value === o.value)) {
      unique.push(o);
    }
    return unique;
  }, []);
  return result;
};

export const mappingHouseRecommend = ({
  setRecommendedLst,
  setSeenLst,
  setNegotiate,
  setTransactionFinishLst,
  setDislikeLst,
  setSuitableLst,
  setSubRecommendedLst,
  setSubSeenLst,
  setSubNegotiate,
  setSubSuitableLst,
  setRequireLst,
  selected,
  houseRequire,
}) => {
  if (selected && selected.matchings) {
    setRecommendedLst([]);
    setSeenLst([]);
    setNegotiate([]);
    setTransactionFinishLst([]);
    setDislikeLst([]);
    setSubRecommendedLst([]);
    setSubSeenLst([]);
    setSubNegotiate([]);
    setSubSuitableLst([]);
    setSuitableLst([]);
    selected.matchings.map(value => {
      if (value.houseRecommendStatus === 'Đã chào') {
        value.priority === 1 && setRecommendedLst(recommendedLst => [...recommendedLst, value]);
        value.priority === 0 && setSubRecommendedLst(recommendedLst => [...recommendedLst, value]);
      } else if (value.houseRecommendStatus === 'Đã đi xem') {
        value.priority === 1 && setSeenLst(seenLst => [...seenLst, value]);
        value.priority === 0 && setSubSeenLst(seenLst => [...seenLst, value]);
      } else if (value.houseRecommendStatus === 'Thương lượng') {
        value.priority === 1 && setNegotiate(negotiate => [...negotiate, value]);
        value.priority === 0 && setSubNegotiate(negotiate => [...negotiate, value]);
      } else if (value.houseRecommendStatus === 'Giao dịch hoàn thành') {
        value.priority === 1 &&
          setTransactionFinishLst(transactionFinishLst => [...transactionFinishLst, value]);
      } else if (value.houseRecommendStatus === 'Không ưng') {
        value.priority === 1 && setDislikeLst(dislikeLst => [...dislikeLst, value]);
      } else if (value.houseRecommendStatus === 'Phù hợp sau') {
        value.priority === 0 && setSubSuitableLst(subSuitableLst => [...subSuitableLst, value]);
      } else {
        console.log('set');
        setSuitableLst(suitableLst => [...suitableLst, value]);
      }
    });
  }
  if (houseRequire && houseRequire.length > 0) {
    setRequireLst([]);
    houseRequire.map(value => {
      if (value.houseRecommendStatus === 'Đã chào') {
        value.priority === 1 && setRecommendedLst(recommendedLst => [...recommendedLst, value]);
        value.priority === 0 && setSubRecommendedLst(recommendedLst => [...recommendedLst, value]);
      } else if (value.houseRecommendStatus === 'Đã đi xem') {
        value.priority === 1 && setSeenLst(seenLst => [...seenLst, value]);
        value.priority === 0 && setSubSeenLst(seenLst => [...seenLst, value]);
      } else if (value.houseRecommendStatus === 'Thương lượng') {
        value.priority === 1 && setNegotiate(negotiate => [...negotiate, value]);
        value.priority === 0 && setSubNegotiate(negotiate => [...negotiate, value]);
      } else if (value.houseRecommendStatus === 'Giao dịch hoàn thành') {
        value.priority === 1 &&
          setTransactionFinishLst(transactionFinishLst => [...transactionFinishLst, value]);
      } else if (value.houseRecommendStatus === 'Không ưng') {
        value.priority === 1 && setDislikeLst(dislikeLst => [...dislikeLst, value]);
      } else if (value.houseRecommendStatus === 'Phù hợp sau') {
        value.priority === 0 && setSubSuitableLst(subSuitableLst => [...subSuitableLst, value]);
      } else {
        setRequireLst(requireLst => [...requireLst, value]);
      }
    });
  }
};

export const calcLeftDays = ({ quarter, currentQuarter }) => {
  let firstDay;
  const currentDate = new Date();
  if (currentQuarter <= quarter) {
    if (quarter === 1) {
      firstDay = new Date(date.getFullYear(), 0, 1);
      // lastDay = new Date(date.getFullYear(), 2, 28);
    } else if (quarter === 2) {
      firstDay = new Date(date.getFullYear(), 3, 1);
      // lastDay = new Date(date.getFullYear(), 5, 30);
    } else if (quarter === 3) {
      firstDay = new Date(date.getFullYear(), 6, 1);
      // lastDay = new Date(date.getFullYear(), 8, 30);
    } else {
      firstDay = new Date(date.getFullYear(), 9, 1);
      // lastDay = new Date(date.getFullYear(), 11, 31);
    }
    return Math.floor((Number(currentDate) - Number(firstDay)) / (24 * 60 * 60 * 1000));
  }
  return 1;
};
