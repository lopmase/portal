/* eslint-disable no-nested-ternary */
import { DATALIST_ACTION_TYPES } from '../constants/DATALIST_ACTION_TYPES';

const SORT_ORDER = {
  DESC: 'desc',
  ASC: 'asc',
};

const createDatalistReducer = (reducerId, values = {}, extendedReducer) => {
  const initialState = {
    fetching: false,
    list: [],
    publicList: {
      total: 0,
      total_page: 0,
      page: 1,
      list: [],
    },
    waitingApproval: {
      total: 0,
      total_page: 0,
      page: 1,
      list: [],
    },
    approved: {
      total: 0,
      total_page: 0,
      page: 1,
      list: [],
    },
    notApproved: {
      total: 1,
      total_page: 0,
      page: 1,
      list: [],
    },
    personal: {
      total: 0,
      total_page: 0,
      page: 1,
      list: [],
    },
    suspend: {
      total: 0,
      total_page: 0,
      page: 1,
      list: [],
    },
    total_pages: 0,
    page: 1,
    total: 0,
    message: '',
    search: null,
    pageSize: 10,
    sortOrder: SORT_ORDER.ASC,
    sortColumn: null,
    ...values,
  };
  return (state = initialState, action) => {
    if (action.reducerId !== reducerId) {
      return state;
    }
    if (extendedReducer) {
      const returnState = extendedReducer(state, action);
      if (returnState) {
        return { ...returnState };
      }
    }

    switch (action.type) {
      case DATALIST_ACTION_TYPES.FETCHING:
        return {
          ...state,
          fetching: true,
        };
      case DATALIST_ACTION_TYPES.UPDATE_PAGE_SIZE:
        return {
          ...state,
          pageSize: action.pageSize,
        };
      case DATALIST_ACTION_TYPES.UPDATE_SORT:
        return {
          ...state,
          sortOrder: action.order === 'ascend' ? SORT_ORDER.ASC : SORT_ORDER.DESC,
          sortColumn: action.order ? action.column : null,
        };
      case DATALIST_ACTION_TYPES.NEXT_PAGE:
        return {
          ...state,
          page: state.page < state.total_pages ? state.page + 1 : state.page,
        };
      case DATALIST_ACTION_TYPES.PREV_PAGE:
        return {
          ...state,
          page: state.page > 1 ? state.page - 1 : state.page,
        };
      case DATALIST_ACTION_TYPES.UPDATE_PAGE:
        return {
          ...state,
          page: action.page,
        };
      case DATALIST_ACTION_TYPES.SEARCH:
        return {
          ...state,
          search: action.search,
        };
      case DATALIST_ACTION_TYPES.FETCHING_DONE:
        return {
          ...state,
          fetching: false,
          list: action.data,
          total: action.total || action.data.length,
          total_pages: action.total_pages || 1,
        };
      case DATALIST_ACTION_TYPES.ALL_DONE:
        return {
          ...state,
          fetching: false,
        };
      case DATALIST_ACTION_TYPES.WAITING_APPROVAL_FETCH_DONE:
        return {
          ...state,
          waitingApproval: {
            list: action.data,
            total: action.total || action.data.length,
            total_pages: action.total_pages || 1,
            page: action.page,
          },
        };
      case DATALIST_ACTION_TYPES.PUBLIC_FETCH_DONE:
        return {
          ...state,
          publicList: {
            list: action.data,
            total: action.total || action.data.length,
            total_pages: action.total_pages || 1,
            page: action.page,
          },
        };
      case DATALIST_ACTION_TYPES.APPROVED_FETCH_DONE:
        return {
          ...state,
          approved: {
            list: action.data,
            total: action.total || action.data.length,
            total_pages: action.total_pages || 1,
            page: action.page,
          },
        };
      case DATALIST_ACTION_TYPES.NOT_APRROVED_FETCH_DONE:
        return {
          ...state,
          notApproved: {
            list: action.data,
            total: action.total || action.data.length,
            total_pages: action.total_pages || 1,
            page: action.page,
          },
        };
      case DATALIST_ACTION_TYPES.PERSONAL:
        return {
          ...state,
          personal: {
            list: action.data,
            total: action.total || action.data.length,
            total_pages: action.total_pages || 1,
            page: action.page,
          },
        };
      case DATALIST_ACTION_TYPES.SUSPEND_FETCH_DONE:
        return {
          ...state,
          suspend: {
            list: action.data,
            total: action.total || action.data.length,
            total_pages: action.total_pages || 1,
            page: action.page,
          },
        };
      case DATALIST_ACTION_TYPES.ERROR:
        return {
          ...state,
          fetching: false,
          message: action.error,
        };

      default:
        return state;
    }
  };
};

export default createDatalistReducer;
