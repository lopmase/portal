import { DATA_ACTION_TYPES } from '../constants/DATA_ACTION_TYPES';

const createDataReducer = (reducerId, extendedState = {}, extendedReducer) => {
  const initialState = {
    fetching: false,
    data: null,
    message: '',
    ...extendedState,
  };
  return (state = initialState, action) => {
    if (action.reducerId !== reducerId) {
      return state;
    }
    if (extendedReducer) {
      const returnState = extendedReducer(state, action);
      if (returnState) {
        return { ...returnState };
      }
    }
    switch (action.type) {
      case DATA_ACTION_TYPES.FETCHING:
        return {
          ...state,
          fetching: true,
        };
      case DATA_ACTION_TYPES.FETCHING_DONE:
        return {
          ...state,
          fetching: false,
          data: action.result,
        };
      case DATA_ACTION_TYPES.ERROR:
        return {
          ...state,
          fetching: false,
          message: action.error,
        };
      default:
        return state;
    }
  };
};

export default createDataReducer;
