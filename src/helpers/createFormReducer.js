import _ from 'lodash';
import { FORM_ACTION_TYPES } from '../constants/FORM_ACTION_TYPES';

const empty = value => value === '' || value === null || value === undefined;

const validateField = (field, value, form, fieldRules) => {
  let message = '';
  let valid = true;
  _.forEach(fieldRules, rule => {
    const ruleName = rule[0];
    const errorMessage = rule[1];
    if (typeof ruleName === 'string') {
      if (ruleName === 'required' && empty(value)) {
        valid = false;
      }
    } else if (typeof ruleName === 'function') {
      valid = ruleName(value, form);
    }
    if (!valid) {
      message = errorMessage;
      return false;
    }
    return true;
  });

  return message;
};

const validateFields = (form, formRule) => {
  const error = {};
  const touched = {};

  _.forIn(formRule, (rules, field) => {
    const value = form[field];
    error[field] = validateField(field, value, form, rules);
    if (error[field]) {
      touched[field] = true;
    }
  });

  return [error, touched];
};

const createFormReducer = (reducerId, formData, rules = {}) => {
  const initialState = {
    submitting: false,
    waiting: false,
    data: { ...formData },
    touched: {},
    error: {},
    message: '',
    editMode: false,
  };

  return (state = initialState, action) => {
    if (action.reducerId !== reducerId) {
      return state;
    }

    switch (action.type) {
      case FORM_ACTION_TYPES.VALIDATE_ALL_FIELDS:
        const [error, touched] = validateFields(state.data, rules);
        return {
          ...state,
          error: { ...error },
          touched: { ...touched },
        };

      case FORM_ACTION_TYPES.WAITING:
        return {
          ...state,
          waiting: true,
        };

      case FORM_ACTION_TYPES.WAITING_DONE:
        return {
          ...state,
          waiting: false,
        };

      case FORM_ACTION_TYPES.SUBMITTING:
        return {
          ...state,
          submitting: true,
        };
      case FORM_ACTION_TYPES.SUBMIT_DONE:
        return {
          ...state,
          submitting: false,
          result: action.result,
          touched: {},
          error: {},
        };
      case FORM_ACTION_TYPES.FIELD_CHANGED:
        return {
          ...state,
          data: { ...state.data, [action.name]: action.value },
          error: {
            ...state.error,
            [action.name]: validateField(action.name, action.value, state.data, rules[action.name]),
          },
          touched: { ...state.touched, [action.name]: true },
        };
      case FORM_ACTION_TYPES.ERROR:
        return {
          ...state,
          submitting: false,
          message: action.error,
        };
      case FORM_ACTION_TYPES.RESET:
        return {
          ...state,
          data: { ...formData },
          touched: {},
          error: {},
        };
      case FORM_ACTION_TYPES.EDIT_MODE:
        return {
          ...state,
          editMode: action.editMode || false,
        };
      default:
        return state;
    }
  };
};

export default createFormReducer;
