import { MODAL_ACTION_TYPES } from '../constants/MODAL_ACTION_TYPES';

const createModalReducer = reducerId => {
  const initialState = {
    visible: false,
  };
  return (state = initialState, action) => {
    if (action.reducerId !== reducerId) {
      return state;
    }
    switch (action.type) {
      case MODAL_ACTION_TYPES.OPEN:
        return {
          ...state,
          visible: true,
        };
      case MODAL_ACTION_TYPES.CLOSE:
        return {
          ...state,
          visible: false,
        };
      case MODAL_ACTION_TYPES.TOGGLE:
        return {
          ...state,
          visible: !state.visible,
        };
      default:
        return state;
    }
  };
};

export default createModalReducer;
