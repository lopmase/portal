import { DATA_ACTION_TYPES } from '../constants/DATA_ACTION_TYPES';

export const generateDataActions = reducerId => {
  const fetching = () => ({
    type: DATA_ACTION_TYPES.FETCHING,
    reducerId,
  });

  const fetchDone = result => ({
    type: DATA_ACTION_TYPES.FETCHING_DONE,
    reducerId,
    result,
  });

  const fetchError = error => ({
    type: DATA_ACTION_TYPES.ERROR,
    reducerId,
    error,
  });

  return {
    fetching,
    fetchDone,
    fetchError,
  };
};
