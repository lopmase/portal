/* eslint-disable camelcase */
import { DATALIST_ACTION_TYPES } from '../constants/DATALIST_ACTION_TYPES';

export const generateDatalistActions = reducerId => {
  const fetching = () => ({
    type: DATALIST_ACTION_TYPES.FETCHING,
    reducerId,
  });

  const fetchDone = (data, total, total_pages) => ({
    type: DATALIST_ACTION_TYPES.FETCHING_DONE,
    reducerId,
    data,
    total,
    total_pages,
  });
  const allDone = data => ({
    type: DATALIST_ACTION_TYPES.ALL_DONE,
    reducerId,
    data,
  });
  const publicFetchDone = (data, total, total_pages, page) => {
    return {
      type: DATALIST_ACTION_TYPES.PUBLIC_FETCH_DONE,
      reducerId,
      data,
      total,
      total_pages,
      page,
    };
  };
  const fetchPersonalDone = (data, total, total_pages, page) => {
    return {
      type: DATALIST_ACTION_TYPES.PERSONAL,
      reducerId,
      data,
      total,
      total_pages,
      page,
    };
  };
  const fetchNotApprovedDone = (data, total, total_pages, page) => {
    return {
      type: DATALIST_ACTION_TYPES.NOT_APRROVED_FETCH_DONE,
      reducerId,
      data,
      total,
      total_pages,
      page,
    };
  };
  const fetchApprovedDone = (data, total, total_pages, page) => {
    return {
      type: DATALIST_ACTION_TYPES.APPROVED_FETCH_DONE,
      reducerId,
      data,
      total,
      total_pages,
      page,
    };
  };
  const fetchWaitingApprovalDone = (data, total, total_pages, page) => {
    return {
      type: DATALIST_ACTION_TYPES.WAITING_APPROVAL_FETCH_DONE,
      reducerId,
      data,
      total,
      total_pages,
      page,
    };
  };
  const fetchSubListDone = (data, total, total_pages, page) => ({
    type: DATALIST_ACTION_TYPES.SUB_LIST_FETCH_DONE,
    reducerId,
    data,
    total,
    total_pages,
    page,
  });
  const fetchSuspendListDone = (data, total, total_pages, page) => ({
    type: DATALIST_ACTION_TYPES.SUSPEND_FETCH_DONE,
    reducerId,
    data,
    total,
    total_pages,
    page,
  });

  const search = searchValue => ({
    type: DATALIST_ACTION_TYPES.SEARCH,
    reducerId,
    search: searchValue,
  });
  const updatePageSize = pageSize => ({
    type: DATALIST_ACTION_TYPES.UPDATE_PAGE_SIZE,
    reducerId,
    pageSize,
  });

  const updatePage = page => ({
    type: DATALIST_ACTION_TYPES.UPDATE_PAGE,
    reducerId,
    page,
  });

  const updateSort = (column, order) => ({
    type: DATALIST_ACTION_TYPES.UPDATE_SORT,
    reducerId,
    column,
    order,
  });

  const nextPage = () => ({
    type: DATALIST_ACTION_TYPES.NEXT_PAGE,
    reducerId,
  });

  const previousPage = () => ({
    type: DATALIST_ACTION_TYPES.PREV_PAGE,
    reducerId,
  });

  const fetchError = error => ({
    type: DATALIST_ACTION_TYPES.ERROR,
    reducerId,
    error,
  });

  return {
    fetching,
    search,
    fetchDone,
    fetchError,
    nextPage,
    previousPage,
    updatePageSize,
    updatePage,
    updateSort,
    fetchSubListDone,
    fetchPersonalDone,
    fetchNotApprovedDone,
    fetchApprovedDone,
    fetchWaitingApprovalDone,
    publicFetchDone,
    allDone,
    fetchSuspendListDone,
  };
};
