export const handleError = (status, data) => {
  switch (status) {
    case 200:
    case 201:
      return data;
    case 400:
    case 401: {
      const err = new Error(data.message || 'Your session is timeout. Please login again.');
      err.unauthorized = true;
      throw err;
    }
    case 404:
      throw new Error((data.err && data.err.message) || data.message || 'Method Not Allowed');
    case 405:
      throw new Error(
        (data.err && data.err.message) ||
          data.message ||
          'Look like the server is down. Please contact us at thinhgia@gmail.com',
      );
    case 500:
      throw new Error(
        (data.err && data.err.message) ||
          data.message ||
          'Whoops! Sorry something went wrong on our end. Our engineers are working on it.',
      );
    default:
      throw new Error(
        'Whoops! Sorry something went wrong on our end. Our engineers are working on it.',
      );
  }
};
