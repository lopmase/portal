import { FORM_ACTION_TYPES } from '../constants/FORM_ACTION_TYPES';

export const generateFormActions = reducerId => {
  const handleFieldChanged = (name, value) => ({
    type: FORM_ACTION_TYPES.FIELD_CHANGED,
    reducerId,
    name,
    value,
  });

  const submitting = () => ({
    type: FORM_ACTION_TYPES.SUBMITTING,
    reducerId,
  });

  const validateAll = () => ({
    type: FORM_ACTION_TYPES.VALIDATE_ALL_FIELDS,
    reducerId,
  });

  const submitDone = result => ({
    type: FORM_ACTION_TYPES.SUBMIT_DONE,
    reducerId,
    result,
  });

  const submitError = error => ({
    type: FORM_ACTION_TYPES.ERROR,
    reducerId,
    error,
  });

  const reset = error => ({
    type: FORM_ACTION_TYPES.RESET,
    reducerId,
    error,
  });

  const setEditMode = editMode => ({
    type: FORM_ACTION_TYPES.EDIT_MODE,
    reducerId,
    editMode,
  });
  const waiting = () => ({
    type: FORM_ACTION_TYPES.WAITING,
    reducerId,
  });
  const waitingDone = () => ({
    type: FORM_ACTION_TYPES.WAITING_DONE,
    reducerId,
  });

  return {
    handleFieldChanged,
    submitting,
    submitDone,
    submitError,
    validateAll,
    reset,
    setEditMode,
    waiting,
    waitingDone,
  };
};
