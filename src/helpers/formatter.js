import _ from 'lodash';
import { FIELDS } from '../constants/HOUSE_FIELDS';

export const escapeUnicode = value => {
  const string = value && value.toString ? value.toString() : '';
  return string
    ? string
        .replace(/[áàâăãảạấầẫẩậắằẵẳặ]/gi, 'a')
        .replace(/[ýỳỷỹỵ]/gi, 'y')
        .replace(/[óòõỏọôốồỗổộơớờỡởợ]/gi, 'o')
        .replace(/[úùũủưụứừữửự]/gi, 'u')
        .replace(/[éèẽẻẹếêềểễệ]/gi, 'e')
        .replace(/[đĐ]/gi, 'd')
        .replace(/[íìĩỉị]/gi, 'i')
        .replace(/[!@#$%^&*()=+/?<>`;'",.-]/gi, '_')
        .replace(' ', '_')
        .replace(/(_)\1*/g, '$1')
    : '';
};

export const formatData = (houses, options) => {
  try {
    const result = {};
    const searchTexts = {};
    _.forEach(houses, house => {
      let row = '';
      _.forEach(FIELDS, field => {
        const key = field.property;
        const value = house[key];
        if (!result[key]) {
          result[key] = [];
        }
        if (Array.isArray(value)) {
          value.forEach(item => {
            const itemValue = item[field.itemKey];
            if (result[key].filter(f => f.value === itemValue).length === 0) {
              if (field.format) {
                result[key].push({
                  label: itemValue,
                  value: itemValue,
                });
              } else {
                result[key].push({ label: itemValue, value: itemValue });
              }
            }
          });
        } else if (result[key].filter(item => item.value === value).length === 0) {
          if (field.format) {
            result[key].push({
              label: field.format(house, options),
              value,
            });
          } else {
            result[key].push({ label: value, value });
          }
        }
        if (field.format) {
          row += field.format(house, options) + escapeUnicode(field.format(house, options));
        } else {
          row += value + escapeUnicode(value);
        }
      });
      searchTexts[house.id] = row.toUpperCase();
    });
    return [result, searchTexts];
  } catch (error) {
    return [{}, {}];
  }
};
