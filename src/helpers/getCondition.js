const date = new Date();
const currentMonth = date.getMonth();
export const getCondition = (state = 3, stateOption = currentMonth + 1, staff) => {
  const condition = {};
  const preCondition = {};
  if (state === 3) {
    const startAt = new Date(date.getFullYear(), stateOption - 1, 1);
    let endAt = new Date(date.getFullYear(), stateOption, 0, 23, 59, 59);
    condition.startAt = `${startAt.getFullYear()}-${startAt.getMonth() +
      1}-${startAt.getDate()} 00:00:00`;
    condition.endAt = `${endAt.getFullYear()}-${endAt.getMonth() + 1}-${endAt.getDate()} 23:59:59`;
    if (stateOption === 1) {
      preCondition.startAt = `${startAt.getFullYear() - 1}-12-01 00:00:00`;
      preCondition.endAt = `${endAt.getFullYear() - 1}-12-31 23:59:59`;
    } else if (stateOption === 2) {
      preCondition.startAt = `${startAt.getFullYear()}-${startAt.getMonth()}-${startAt.getDate()} 00:00:00`;
      preCondition.endAt = `${endAt.getFullYear()}-${endAt.getMonth()}-31 23:59:59`;
    } else {
      endAt = new Date(date.getFullYear(), stateOption - 1, 0, 23, 59, 59);
      preCondition.startAt = `${startAt.getFullYear()}-${startAt.getMonth()}-${startAt.getDate()} 00:00:00`;
      preCondition.endAt = `${endAt.getFullYear()}-${endAt.getMonth() +
        1}-${endAt.getDate()} 23:59:59`;
    }
    condition.quarter = Math.floor(startAt.getMonth() / 3) + 1;
    // const startDayOfQuarter = moment().startOf('quarter');
    // const endDayOfQuarter = moment().endOf('quarter');
    // console.log('daysLeft', moment(endDayOfQuarter).diff(startDayOfQuarter, 'days'));
  }
  if (state === 2) {
    if (stateOption === 1) {
      const startAt = new Date(date.getFullYear(), stateOption - 1, 1);
      const endAt = new Date(date.getFullYear(), 3, 0, 23, 59, 59);
      condition.startAt = `${startAt.getFullYear()}-${startAt.getMonth() +
        1}-${startAt.getDate()} 00:00:00`;
      condition.endAt = `${endAt.getFullYear()}-${endAt.getMonth() +
        1}-${endAt.getDate()} 23:59:59`;

      preCondition.startAt = `${startAt.getFullYear() - 1}-10-01 00:00:00`;
      preCondition.endAt = `${startAt.getFullYear() - 1}-12-31 23:59:59`;
    }
    if (stateOption === 2) {
      const startAt = new Date(date.getFullYear(), 3, 1);
      const endAt = new Date(date.getFullYear(), 6, 0, 23, 59, 59);
      condition.startAt = `${startAt.getFullYear()}-${startAt.getMonth() +
        1}-${startAt.getDate()} 00:00:00`;
      condition.endAt = `${endAt.getFullYear()}-${endAt.getMonth() +
        1}-${endAt.getDate()} 23:59:59`;

      preCondition.startAt = `${startAt.getFullYear()}-01-01 00:00:00`;
      preCondition.endAt = `${startAt.getFullYear()}-03-31 23:59:59`;
    }
    if (stateOption === 3) {
      const startAt = new Date(date.getFullYear(), 6, 1);
      const endAt = new Date(date.getFullYear(), 9, 0, 23, 59, 59);
      condition.startAt = `${startAt.getFullYear()}-${startAt.getMonth() +
        1}-${startAt.getDate()} 00:00:00`;
      condition.endAt = `${endAt.getFullYear()}-${endAt.getMonth() +
        1}-${endAt.getDate()} 23:59:59`;

      preCondition.startAt = `${startAt.getFullYear()}-04-01 00:00:00`;
      preCondition.endAt = `${startAt.getFullYear()}-06-30 23:59:59`;
    }
    if (stateOption === 4) {
      const startAt = new Date(date.getFullYear(), 9, 1);
      const endAt = new Date(date.getFullYear(), 12, 0, 23, 59, 59);
      condition.startAt = `${startAt.getFullYear()}-${startAt.getMonth() +
        1}-${startAt.getDate()} 00:00:00`;
      condition.endAt = `${endAt.getFullYear()}-${endAt.getMonth() +
        1}-${endAt.getDate()} 23:59:59`;

      preCondition.startAt = `${startAt.getFullYear()}-07-01 00:00:00`;
      preCondition.endAt = `${startAt.getFullYear()}-09-30 23:59:59`;
    }
    condition.quarter = stateOption;
  }
  if (state === 1) {
    const startAt = new Date(date.getFullYear(), 0, 1);
    const endAt = new Date(date.getFullYear(), 12, 0, 23, 59, 59);
    condition.startAt = `${startAt.getFullYear()}-${startAt.getMonth() +
      1}-${startAt.getDate()} 00:00:00`;
    condition.endAt = `${endAt.getFullYear()}-${endAt.getMonth() + 1}-${endAt.getDate()} 23:59:59`;

    preCondition.startAt = `${startAt.getFullYear() - 1}-01-01 00:00:00`;
    preCondition.endAt = `${startAt.getFullYear() - 1}-12-31 23:59:59`;
  }
  if (staff) {
    condition.staff = staff;
    preCondition.staff = staff;
  }

  return [condition, preCondition];
};
