import platform from 'platform';
import { getIpAddress, getGeoAddressFromCoordinate } from '../apis/userApi';

export const getUserCurrentLocation = async () => {
  return new Promise((resolve, reject) => {
    if (!navigator.geolocation) {
      resolve(null);
    }
    navigator.geolocation.getCurrentPosition(
      position => {
        resolve({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        });
      },
      err => resolve(null),
    );
  });
};

export const getPlatform = () => {
  return platform.description;
};

export const getIpAdressInfo = async () => {
  const ipData = await getIpAddress();
  return ipData ? ipData.ip : '';
};

export const getUserInfo = async () => {
  let location = await getUserCurrentLocation();
  const ip = await getIpAdressInfo();
  if (!location) {
    location = { notAuthorized: true };
  }
  return {
    location,
    device_type: getPlatform(),
    ip_address: ip,
  };
};

const GEOCODE_CACHING = 'TG_GEOCODE_CACHING';

const roundValueForCoordinate = value => {
  return Math.round((Number(value) + Number.EPSILON) * 100000) / 100000;
};

const parseCacheKey = (latitude, longitude) =>
  GEOCODE_CACHING + roundValueForCoordinate(latitude) + roundValueForCoordinate(longitude);

const getAddressByCoordinateFromCache = (latitude, longitude) => {
  const cache = localStorage.getItem(parseCacheKey(latitude, longitude));
  return cache;
};

export const getAddressFromCoordinate = async (latitude, longitude) => {
  const cacheData = getAddressByCoordinateFromCache(latitude, longitude);
  if (cacheData) {
    return cacheData;
  }
  const geocodeData = await getGeoAddressFromCoordinate(latitude, longitude);
  localStorage.setItem(parseCacheKey(latitude, longitude), geocodeData);
  return geocodeData;
};

export const isInfoChanged = (info, newInfo) => {
  const newLocation = newInfo.location;
  const { location, device_type: deviceType, ip_address: ip } = info;
  if (newLocation.latitude !== location.latitude) return true;
  if (newLocation.longitude !== location.longitude) return true;
  if (newInfo.device_type !== deviceType) return true;
  if (newInfo.ip_address !== ip) return true;

  return false;
};
