import { MODAL_ACTION_TYPES } from '../constants/MODAL_ACTION_TYPES';

export const generateModalActions = reducerId => {
  const openModal = () => ({
    type: MODAL_ACTION_TYPES.OPEN,
    reducerId,
  });
  const closeModal = () => ({
    type: MODAL_ACTION_TYPES.CLOSE,
    reducerId,
  });
  const toggle = () => ({
    type: MODAL_ACTION_TYPES.TOGGLE,
    reducerId,
  });

  return {
    openModal,
    closeModal,
    toggle,
  };
};
