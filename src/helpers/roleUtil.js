export const isAdmin = user => {
  return user && (Number(user.role) === 1 || Number(user.role) === 2);
};
export const isSuperAdmin = user => {
  return user && Number(user.role) === 1;
};
export const isMarketer = user => {
  return user && Number(user.role) === 5;
};
