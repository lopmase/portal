import { useSelector } from 'react-redux';

export const useUserProfile = () => {
  const user = useSelector(state => state.auth.user.data);
  return user || {};
};
