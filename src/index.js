/* eslint-disable no-param-reassign */
/* eslint-disable no-return-assign */
import React from 'react';
import ReactDOM from 'react-dom';
import { CronJob } from 'cron';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { statisticcHouse } from './apis/businessApi';
// import { getHouseTopPost } from './apis/dashoardApi';
// import { request } from './apis/request';
// import axios from 'axios';

const job = new CronJob(
  '0 * * * *',
  async function test() {
    // const houses = await getHouseTopPost();
    // console.log('res house', houses);
    statisticcHouse();
  },
  null,
  true,
  'Asia/Ho_Chi_Minh',
);

// const job = new CronJob(
//   '* */1 * * *',
//   async function test() {
//     const d = new Date();
//     const minutes = d.getMinutes();
//     console.log('min', minutes);
//     if (minutes === 59) {
//       const houses = await getHouseTopPost();
//       console.log('post house', houses);
//       const post = await axios.post(
//         'http://18.141.174.219:5000/AloNhaDat/register-realestate-alonhadat',
//         houses,
//       );
//       console.log('post', post);
//     }
//   },
//   null,
//   true,
//   'Asia/Ho_Chi_Minh',
// );

// Use this if the 4th param is default value(false)
job.start();
ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
