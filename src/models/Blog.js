import moment from 'moment';
import { convertFromRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import coverImage from '../assets/images/left.jpg';
import { formatImageUrl } from '../helpers/common';
import { BLOG_TYPE } from '../constants/COMMON';

export class Blog {
  constructor(json) {
    if (json) {
      this.content = json.content;
      this.cover = json.cover;
      this.created_at = json.created_at;
      this.created_by = json.created_by;
      this.deleted_at = json.deleted_at;
      this.id = json.id;
      this.status = json.status;
      this.tag = json.tag;
      this.title = json.title;
      this.updated_at = json.updated_at;
      this.updated_by = json.updated_by;
      this.user = json.user;
      this.type = json.type;
    }
  }

  get photo() {
    return this.cover && this.cover.main ? formatImageUrl(this.cover.main) : coverImage;
  }

  get thumbnail() {
    return this.cover && this.cover.thumbnail ? formatImageUrl(this.cover.url) : coverImage;
  }

  get formattedDate() {
    return moment(this.created_at).format('DD-MM-YYYY');
  }

  get formattedTime() {
    return moment(this.created_at).format('hh:mm a');
  }

  get author() {
    return this.user && this.user.name ? this.user.name : this.created_by;
  }

  get textContent() {
    let rawContent = {};
    try {
      rawContent = JSON.parse(this.content);
    } catch (error) {
      rawContent = {};
    }
    const contentState = convertFromRaw(rawContent);
    const textContent = contentState.getPlainText('\n');

    return textContent;
  }

  get htmlContent() {
    let rawContent = {};
    try {
      rawContent = JSON.parse(this.content);
    } catch (error) {
      rawContent = {};
    }
    const html = draftToHtml(rawContent);

    return html;
  }

  get blogType() {
    const typeObject = BLOG_TYPE.find(type => type.value === this.type);
    return typeObject ? typeObject.text : 'Chưa xác định';
  }
}
