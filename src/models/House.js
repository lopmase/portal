import moment from 'moment';
import coverImage from '../assets/images/left.jpg';
import { formatImageUrl } from '../helpers/common';

export class House {
  constructor(json) {
    if (json) {
      this.approve = json.approve;
      this.area = json.area;
      this.bought_customer = json.bought_customer;
      this.brokerage_fee = json.brokerage_fee;
      this.brokerage_rate = json.brokerage_rate;
      this.created_at = json.created_at;
      this.customer_id = json.customer_id;
      this.customer = json.customer;
      this.deleted_at = json.deleted_at;
      this.deposit_customer = json.deposit_customer;
      this.description = json.description;
      this.district = json.district;
      this.end_open = json.end_open;
      this.floor_area = json.floor_area;
      this.floors = json.floors;
      this.house_address = json.house_address;
      this.house_direction = json.house_direction;
      this.house_balcony_direction = json.house_balcony_direction;
      this.house_number = json.house_number;
      this.house_type = json.house_type;
      this.id = json.id;
      this.image = json.image;
      this.internal_image = json.internal_image;
      this.into_money = json.into_money;
      this.length = json.length;
      this.number_bedroom = json.number_bedroom;
      this.number_wc = json.number_wc;
      this.offered_customer = json.offered_customer;
      this.ownership = json.ownership;
      this.project = json.project;
      this.block_section = json.block_section;
      this.floor_lot = json.floor_lot;
      this.project_id = json.project_id;
      this.property_type = json.property_type;
      this.province = json.province;
      this.public = json.public;
      this.public_image = json.public_image;
      this.purpose = json.purpose;
      this.require_info_customer = json.require_info_customer;
      this.seen_customer = json.seen_customer;
      this.status = json.status;
      this.street_type = json.street_type;
      this.suitable_customer = json.suitable_customer;
      this.title = json.title;
      this.updated_at = json.updated_at;
      this.user = json.user;
      this.user_id = json.user_id;
      this.village = json.village;
      this.web = json.web;
      this.slug = json.slug;
      this.wide_street = json.wide_street;
      this.public_approval = json.public_approval;
      this.total_view = json.total_view;
      this.width = json.width;
      this.comments_count = json.comments_count;
      this.recommend_quantity = json.recommend_quantity;
      this.seen_quantity = json.seen_quantity;
      this.postQuantity = json.postQuantity;
      this.house_tag = json.house_tag;
      this.type_news = json.type_news;
      this.commission = json.commission;
      this.type_news_day = json.type_news_day;
      this.dining_room = json.dining_room;
      this.kitchen = json.kitchen;
      this.terrace = json.terrace;
      this.car_parking = json.car_parking;
      this.post = json.post;
      this.failed_quantity = json.failed_quantity;
      this.type_news_value = json.type_news_value;
      this.internalDescription = json.internalDescription;
      this.initialDescription = json.initialDescription;
      this.reject_web_condition = json.reject_web_condition;
      this.reject_public_condition = json.reject_public_condition;
    }
  }

  get photo() {
    return this.cover && this.cover.main ? formatImageUrl(this.cover.main) : coverImage;
  }

  get customerName() {
    return this.customer ? this.customer.name : 'Không có';
  }

  get thumbnail() {
    return this.cover && this.cover.thumbnail ? formatImageUrl(this.cover.url) : coverImage;
  }

  get formattedDate() {
    return moment(this.created_at).format('DD-MM-YYYY');
  }

  get formattedTime() {
    return moment(this.created_at).format('hh:mm a');
  }

  get owner() {
    return this.user || {};
  }
}
