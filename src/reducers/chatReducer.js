import * as types from '../actions/types';

const initial = {
  conversations: {
    list: [],
    total_page: 1,
    page: 1,
  },
  messages: {
    loading: false,
    refreshing: false,
    message: '',
  },
  initialized: false,
  loading: false,
  refreshing: false,
  selected: null,
  selectedMessages: [],
  message: '',
};

const chatReducer = (state = initial, action) => {
  switch (action.type) {
    case types.LOADING_CHATS:
      return {
        ...state,
        loading: action.loading,
      };
    case types.REFRESHING_CHATS:
      return {
        ...state,
        refreshing: action.loading,
      };
    case types.UPDATE_CHATS:
      return {
        ...state,
        conversations: {
          list: action.conversations.data,
          total_page: action.conversations.total_page,
          page: action.conversations.page,
        },
        refreshing: false,
        initialized: true,
        loading: false,
      };
    case types.UPDATE_CHATS_ERROR:
      return {
        ...state,
        refreshing: false,
        message: action.message,
        loading: false,
      };
    // MESSAGES
    case types.LOADING_MESSAGES:
      return {
        ...state,
        messages: { ...state.messages, loading: action.loading },
      };
    case types.REFRESHING_MESSAGES:
      return {
        ...state,
        messages: { ...state.messages, refreshing: action.loading },
      };
    case types.UPDATE_MESSAGES:
      return {
        ...state,
        messages: {
          refreshing: false,
          loading: false,
          message: '',
          page: action.messages.page,
          total_page: action.messages.total_page,
        },
        selectedMessages: action.messages.data,
      };
    case types.UPDATE_MESSAGES_ERROR:
      return {
        ...state,
        messages: {
          refreshing: false,
          loading: false,
          message: action.message,
        },
      };
    case types.SELECT_CHAT:
      return {
        ...state,
        selected: action.conversation,
      };
    default:
      return state;
  }
};

export default chatReducer;
