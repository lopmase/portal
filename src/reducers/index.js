import { combineReducers } from 'redux';
import auth from '../container/AuthContainer/AuthReducer';
import houseReducer from '../container/HouseContainer/Reducer';
import metadata from '../container/HouseContainer/MetadataReducer';
import social from '../container/SocialContainer/Reducer';
import { REDUCERS } from '../constants/COMMON';
import reducer from '../container/Common/GlobalErrorModal/reducer';
import notiReducer from '../container/ShellContainner/NotiReducer';
import notiPopupReducer from '../container/ShellContainner/NotificationPopup/Reducer';
import customerReducer from '../container/CustomerConntainer/Reducer';
import userReducer from '../container/UserConntainer/Reducer';
import projectReducer from '../container/ProjectContainer/Reducer';
import createModalReducer from '../helpers/createModalReducer';
import dashboardReducer from '../container/DashboardContainer/Reducer';
import confirmReducer from '../container/ShellContainner/ConfirmDialog/reducer';
import globalSearchReducer from '../container/ShellContainner/GlobalSearchReducer';
import blogReducer from '../container/BlogContainer/Reducer';
import requestReducer from '../container/RequestContainer/Reducer';
import typeKnowledgeReducer from '../container/KnowledgeContainer/Reducer';

import admin from '../container/AdminContainer/Reducer';
import {
  transactionReducer,
  transactionOldReducer,
  transactionBillReducer,
} from '../container/BussinessContainer/TransactionSuccess/reducer';
import { accumulationReducer } from '../container/BussinessContainer/Accumulation/reducer';
import { conditionReducer } from '../container/BussinessContainer/Condition/reducer';
import postReducer from '../container/PostManager/Post/reducer';
import {
  statisticReducer,
  targetReducer,
  houseQuantityReducer,
} from '../container/BussinessContainer/StatisticAccumulation/reducer';
import ChatRealtimeReducer from '../container/Chat/reducers/index';
import { chatReducer } from '../container/Firebase/reducer';
import AccountReducer from '../container/AccountContainer/reducer';

const rootReducer = {
  auth,
  alonhadat: postReducer,
  anotherAccount: AccountReducer,
  // chat,
  house: houseReducer,
  customer: customerReducer,
  user: userReducer,
  blog: blogReducer,
  project: projectReducer,
  dashboard: dashboardReducer,
  request: requestReducer,
  admin,
  metadata,
  social,
  chatRealtime: ChatRealtimeReducer,
  modal: combineReducers({
    [REDUCERS.GLOBAL_ERROR_MODAL]: reducer,
    [REDUCERS.NOTI_POPUP]: notiPopupReducer,
    [REDUCERS.GLOBAL_LOADING]: createModalReducer(REDUCERS.GLOBAL_LOADING),
    [REDUCERS.USER_PROFILE_MODAL]: createModalReducer(REDUCERS.USER_PROFILE_MODAL),
    [REDUCERS.NEW_CUSTOMER_MODAL]: createModalReducer(REDUCERS.NEW_CUSTOMER_MODAL),
    [REDUCERS.EDIT_CUSTOMER_MODAL]: createModalReducer(REDUCERS.EDIT_CUSTOMER_MODAL),
    [REDUCERS.NEW_USER_MODAL]: createModalReducer(REDUCERS.NEW_USER_MODAL),
    [REDUCERS.EDIT_USER_MODAL]: createModalReducer(REDUCERS.EDIT_USER_MODAL),
    [REDUCERS.CONFIRM_DIALOG]: confirmReducer,
    [REDUCERS.CHANGE_PASSWORD_MODAL]: createModalReducer(REDUCERS.CHANGE_PASSWORD_MODAL),
  }),
  notification: notiReducer,
  search: globalSearchReducer,
  transaction: transactionReducer,
  transactionOld: transactionOldReducer,
  accumulation: accumulationReducer,
  condition: conditionReducer,
  statistic: statisticReducer,
  target: targetReducer,
  chat: chatReducer,
  countHouse: houseQuantityReducer,
  transactionBill: transactionBillReducer,
  Knowledge: typeKnowledgeReducer,
};

export default rootReducer;
