import store from 'store';
import { USER_ID } from '../constants/STORAGE_KEYS';

const getCurrentUserId = () => {
  return store.get(USER_ID);
};
const buildUserStorageKey = key => {
  const userId = getCurrentUserId() || 'unknown';
  return `${userId}_${key}`;
};

export const set = (key, object) => {
  store.set(buildUserStorageKey(key), object);
};

export const get = key => {
  return store.get(buildUserStorageKey(key));
};

export const remove = key => {
  store.remove(buildUserStorageKey(key));
};

export const clearUserStoredInfo = () => {
  store.each((value, key) => {
    if (key.indexOf(getCurrentUserId()) === 0) {
      store.remove(key);
    }
  });
};

export const clearAll = () => {
  store.clearAll();
};

export default {
  set,
  get,
  remove,
  clearAll,
};
