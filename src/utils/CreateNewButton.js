import React from 'react';
import Button from '../components/Button/Button';

// eslint-disable-next-line react/prop-types
const CreateButton = ({ openModal }) => <Button text="+ Tạo mới" onClick={() => openModal()} />;

export default CreateButton;
