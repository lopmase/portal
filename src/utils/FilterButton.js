import React from 'react';
import { faFilter } from '@fortawesome/free-solid-svg-icons';
import Button from '../components/Button/Button';
import styles from '../styles/_Colors.scss';

const styleProps = {
  text: 'LỌC',
  small: true,
  icon: faFilter,
  backgroundColor: 'transparent',
  border: true,
  textColor: styles.primaryColor,
  iconColor: styles.primaryColor,
  containerStyle: {
    marginRight: 10,
    marginLeft: 10,
  },
};

// eslint-disable-next-line react/prop-types
const customerListFilterButton = () => <Button {...styleProps} />;

export default customerListFilterButton;
