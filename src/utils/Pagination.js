import React from 'react';

import { Paginator } from '../components';

// eslint-disable-next-line react/prop-types
const Pagination = ({ currentPage, pageNumbers, onNext, onPrev }) => (
  <Paginator currentPage={currentPage} pageNumbers={pageNumbers} onNext={onNext} onPrev={onPrev} />
);

export default Pagination;
